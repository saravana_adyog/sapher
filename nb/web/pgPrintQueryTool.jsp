<%-- 
    Document   : pgPrintQueryTool
    Created on : Dec 23, 2009, 3:14:43 PM
    Author     : Anoop
--%>

<%@page
    contentType="text/html"
    import="java.sql.ResultSet"
    import="bl.sapher.QueryTool"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.general.GenericException"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"

    pageEncoding="UTF-8"
    %>

<html>
    <head>
        <script type="text/javascript" src="libjs/gui.js"></script>
        
        <%!    private final String FORM_ID = "REP5";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
    private int nRepNum = 0;
    private int nValid = -1;
    private String strRepType = "";
    private String strProdCode = "";
    private String strGenericNm = "";
    private String strBrandNm = "";
    private String strReporter = "";
    private String strCountry = "";
    private String strTrial = "";
    private String strClasfCode = "";
    private String strDoseUnit = "";
    private float fDose = -1;
    private String strIndication = "";
    private String strLLTCode = "";
    private String strOutcomeCode = "";
    private String strPTCode = "";
    private String strCoAssCode = "";
    private String strBodySysNum = "";
    private String strSexCode = "";
    private String strPhyAssCode = "";
    private String strManufCode = "";
    private String strPatNum = "";
    private int nRepNumStart = -1;
    private int nRepNumEnd = -1;
    private int nAgeStart = -1;
    private int nAgeEnd = -1;
    private int nTreatStart = -1;
    private int nTreatEnd = -1;
    private int nEvtStart = -1;
    private int nEvtEnd = -1;
    private String strTimeSpanStart = "";
    private String strTimeSpanEnd = "";
    private String strRepNumCondition = "=";
    private String strAgeCondition = "=";
    private String strDurTreatmentCondition = "=";
    private String strDurEvtCondition = "=";
    private String strTimeSpanCondition = "=";
        %>

    </head>
    <body>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPrintWHO.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);

            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintWHO.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; RecordNotFoundException.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }

        // get all query string vars.
        ///////////////////
        if (request.getParameter("txtProdCode") != null) {
            strProdCode = request.getParameter("txtProdCode");
        }

        if (request.getParameter("txtGenericNm") != null) {
            strGenericNm = request.getParameter("txtGenericNm");
        }

        if (request.getParameter("txtBrandNm") != null) {
            strBrandNm = request.getParameter("txtBrandNm");
        }

        if (request.getParameter("txtReporter") != null) {
            strReporter = request.getParameter("txtReporter");
        }

        if (request.getParameter("txtCountry") != null) {
            strCountry = request.getParameter("txtCountry");
        }

        if (request.getParameter("txtTrial") != null) {
            strTrial = request.getParameter("txtTrial");
        }

        if (request.getParameter("txtClasfCode") != null) {
            strClasfCode = request.getParameter("txtClasfCode");
        }

        if (request.getParameter("txtDoseUnit") != null) {
            strDoseUnit = request.getParameter("txtDoseUnit");
        }

        if (request.getParameter("txtDose") != null) {
            if (!((String) request.getParameter("txtDose")).equals("")) {
                fDose = Float.parseFloat((String) request.getParameter("txtDose"));
            }
        }

        if (request.getParameter("txtIndication") != null) {
            strIndication = request.getParameter("txtIndication");
        }

        if (request.getParameter("txtLLTCode") != null) {
            strLLTCode = request.getParameter("txtLLTCode");
        }

        if (request.getParameter("txtOutcomeCode") != null) {
            strOutcomeCode = request.getParameter("txtOutcomeCode");
        }

        if (request.getParameter("txtLLTCode") != null) {
            strLLTCode = request.getParameter("txtLLTCode");
        }

        if (request.getParameter("txtCoAssCode") != null) {
            strCoAssCode = request.getParameter("txtCoAssCode");
        }

        if (request.getParameter("txtBodySysNum") != null) {
            strBodySysNum = request.getParameter("txtBodySysNum");
        }

        if (request.getParameter("txtSexCode") != null) {
            strSexCode = request.getParameter("txtSexCode");
        }

        if (request.getParameter("txtPhyAssCode") != null) {
            strPhyAssCode = request.getParameter("txtPhyAssCode");
        }

        if (request.getParameter("txtManufCode") != null) {
            strManufCode = request.getParameter("txtManufCode");
        }

        if (request.getParameter("txtPatNum") != null) {
            strPatNum = request.getParameter("txtPatNum");
        }

        if (request.getParameter("txtRepNumStart") != null) {
            if (!((String) request.getParameter("txtRepNumStart")).equals("")) {
                nRepNumStart = Integer.parseInt((String) request.getParameter("txtRepNumStart"));
            }
        }

        if (request.getParameter("txtRepNumEnd") != null) {
            if (!((String) request.getParameter("txtRepNumEnd")).equals("")) {
                nRepNumEnd = Integer.parseInt((String) request.getParameter("txtRepNumEnd"));
            }
        }

        if (request.getParameter("txtAgeStart") != null) {
            if (!((String) request.getParameter("txtAgeStart")).equals("")) {
                nAgeStart = Integer.parseInt((String) request.getParameter("txtAgeStart"));
            }
        }

        if (request.getParameter("txtAgeEnd") != null) {
            if (!((String) request.getParameter("txtAgeEnd")).equals("")) {
                nAgeEnd = Integer.parseInt((String) request.getParameter("txtAgeEnd"));
            }
        }

        if (request.getParameter("txtTreatStart") != null) {
            if (!((String) request.getParameter("txtTreatStart")).equals("")) {
                nTreatStart = Integer.parseInt((String) request.getParameter("txtTreatStart"));
            }
        }

        if (request.getParameter("txtTreatEnd") != null) {
            if (!((String) request.getParameter("txtTreatEnd")).equals("")) {
                nTreatEnd = Integer.parseInt((String) request.getParameter("txtTreatEnd"));
            }
        }

        if (request.getParameter("txtEvtStart") != null) {
            if (!((String) request.getParameter("txtEvtStart")).equals("")) {
                nEvtStart = Integer.parseInt((String) request.getParameter("txtEvtStart"));
            }
        }

        if (request.getParameter("txtEvtEnd") != null) {
            if (!((String) request.getParameter("txtEvtEnd")).equals("")) {
                nEvtEnd = Integer.parseInt((String) request.getParameter("txtEvtEnd"));
            }
        }

        if (request.getParameter("txtTimeSpanStart") != null) {
            strTimeSpanStart = request.getParameter("txtTimeSpanStart");
        }

        if (request.getParameter("txtTimeSpanEnd") != null) {
            strTimeSpanEnd = request.getParameter("txtTimeSpanEnd");
        }

        String strValid = "";
        if (request.getParameter("cmbSuspendedCase") == null) {
            strValid = "All";
        } else {
            strValid = request.getParameter("cmbSuspendedCase");
        }

        if (strValid.equals("Active")) {
            nValid = 1;
        } else if (strValid.equals("Inactive")) {
            nValid = 0;
        } else if (strValid.equals("All")) {
            nValid = -1;
        }
        ///////////////////

        if (request.getParameter("RepNum") != null) {
            if (!((String) request.getParameter("RepNum")).equals("")) {
                nRepNum = Integer.parseInt((String) request.getParameter("RepNum"));
            }
        }

        ResultSet rs = QueryTool.listQueryTool4Rep(
                (String) session.getAttribute("Company"),
                strProdCode,
                strReporter,
                strCountry,
                strTrial,
                strClasfCode,
                strDoseUnit,
                fDose,
                strIndication,
                strLLTCode,
                strOutcomeCode,
                strPTCode,
                strCoAssCode,
                strBodySysNum,
                strSexCode,
                strPhyAssCode,
                strManufCode,
                strPatNum,
                nRepNumStart,
                nRepNumEnd,
                nAgeStart,
                nAgeEnd,
                nTreatStart,
                nTreatEnd,
                nEvtStart,
                nEvtEnd,
                strTimeSpanStart,
                strTimeSpanEnd,
                nValid);

        String strPdfFileName = QueryTool.printPdf((String) session.getAttribute("Company"),
                application.getRealPath("/Reports"), rs);
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Report created.");
        %>
        <script type="text/javascript">
            popupWnd('_blank', 'yes','<%out.write("Reports/" + strPdfFileName);%>',800,600);
            document.location.replace("pgNavigate.jsp?Target=pgQueryTool.jsp");
        </script>
    </body>
</html>
