<!--
Form Id : CSD3
Date    : 18-09-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Concomitant Medication - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <script type="text/javascript" src="libjs/gui.js"></script>

    </head>
    <body>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='700' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Error...</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td class="form_group_title" colspan="10">Reason</td>
                        </tr>
                        <tr>
                            <td>
                                <%
        if (request.getParameter("ErrDetails") != null) {
            out.write(request.getParameter("ErrDetails"));
        } else {
            out.write("Invalid Operation...");
        }
                                %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>