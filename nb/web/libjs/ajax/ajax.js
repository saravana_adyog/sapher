function createRequestObject(){
    var req;
    if(window.XMLHttpRequest){
        //For Firefox, Safari, Opera
        req = new XMLHttpRequest();
    }
    else if(window.ActiveXObject){
        //For IE 5+
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else{
        //Error for an old browser
        alert('Your browser is not IE 5 or higher, or Firefox or Safari or Opera');
    }
    
    return req;
}

//Make the XMLHttpRequest Object
var http = createRequestObject();

function sendRequest(method, url, target_DIV){
    if(method == 'get' || method == 'GET'){
        http.open(method,url);
        http.onreadystatechange = function() {
            handleResponse(target_DIV);
        }
        http.send(null);
    }
}

function handleResponse(target) {
    if(http.readyState == 4 && http.status == 200){
        var response = http.responseText;
        
        if(response) {
            if(target != '') {
                document.getElementById(target).innerHTML = response;
            }
        }
    }
}

function getDynData(targetJsp, targetDIV) {
    sendRequest('GET',targetJsp,targetDIV);
}

function getMedDraDynData(targetJsp, targetDIV) {
    http.open('get',targetJsp);
    http.onreadystatechange = function() {
        handleMedDraResponse(targetDIV);
    }
    http.send(null);

}

function handleMedDraResponse(target) {
    if(http.readyState == 4 && http.status == 200){
        var response = http.responseText;

        if(response) {
            document.getElementById(target).innerHTML = response;
        }
    }
}