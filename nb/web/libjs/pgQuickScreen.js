var SAPHER_PARENT_NAME = "pgQuickScreen";

function confirmSave() {
    if(( /* AE related */
        (document.FrmQuickScreen.txtPTCode_ID.value == '') &&
        (document.FrmQuickScreen.txtLLTCode_ID.value == '') &&
        (document.FrmQuickScreen.txtBodySysNum_ID.value == '') &&
        (document.FrmQuickScreen.txtClasfCode_ID.value == '') &&
        (document.FrmQuickScreen.txtOutcomeCode_ID.value == '') &&
        /* others */
        (document.FrmQuickScreen.txtRecvDate.value == '') &&
        (document.FrmQuickScreen.txtClinRep.value == '') &&
        (document.FrmQuickScreen.txtCountryCode_ID.value == '') &&
        (document.FrmQuickScreen.txtProdCode_ID.value == '') &&
        (document.FrmQuickScreen.txtInitials.value == '') &&
        (document.FrmQuickScreen.txtAgeRep.value == '') &&
        (document.FrmQuickScreen.txtSexCode_ID.value == '') &&
        (document.FrmQuickScreen.txtTrial_ID.value == '') &&
        (document.FrmQuickScreen.txtPatNum.value == '') &&
        (document.FrmQuickScreen.txtPhyAssCode_ID.value == '') &&
        (document.FrmQuickScreen.txtCoAssCode_ID.value == '') &&
        (document.FrmQuickScreen.txtNarrative.value == '')
        )) {
        return confirm('Are you sure you want to save an empty entry?','Sapher');
    } else
        return true;
}

function validate() {
    // Value-Code integrity checks
    if( (document.FrmQuickScreen.txtPTCode.value != '') &&
        (document.FrmQuickScreen.txtPTCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtPTCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtLLTCode.value != '') &&
        (document.FrmQuickScreen.txtLLTCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtLLTCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtBodySysNum.value != '') &&
        (document.FrmQuickScreen.txtBodySysNum_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtBodySysNum.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtClasfCode.value != '') &&
        (document.FrmQuickScreen.txtClasfCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtClasfCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtOutcomeCode.value != '') &&
        (document.FrmQuickScreen.txtOutcomeCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtOutcomeCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtCountryCode.value != '') &&
        (document.FrmQuickScreen.txtCountryCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtCountryCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtProdCode.value != '') &&
        (document.FrmQuickScreen.txtProdCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtProdCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtSexCode.value != '') &&
        (document.FrmQuickScreen.txtSexCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtSexCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtTrial.value != '') &&
        (document.FrmQuickScreen.txtTrial_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtTrial.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtPhyAssCode.value != '') &&
        (document.FrmQuickScreen.txtPhyAssCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtPhyAssCode.focus();
        return false;
    }

    if( (document.FrmQuickScreen.txtCoAssCode.value != '') &&
        (document.FrmQuickScreen.txtCoAssCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQuickScreen.txtCoAssCode.focus();
        return false;
    }

    // AE data integrity checks
    if(( (document.FrmQuickScreen.txtPTCode_ID.value == '') ||
        (document.FrmQuickScreen.txtBodySysNum_ID.value != '') ||
        (document.FrmQuickScreen.txtClasfCode_ID.value != '') ||
        (document.FrmQuickScreen.txtOutcomeCode_ID.value != '') ) &&
    ( (document.FrmQuickScreen.txtLLTCode_ID.value != '') ) ) {
        window.alert('Please provide Adverse Event PT.');
        return false;
    }

    // Data lenght checks
    if (document.FrmQuickScreen.txtNarrative.value.length > 1000){
        window.alert('Narrative text must be less than 1000 characters.');
        document.FrmQuickScreen.txtNarrative.focus();
        return false;
    }
    if(!isAlphaNumericSpace('txtClinRep')) {
        window.alert('Invalid character(s) in Clinical reporter information');
        document.getElementById('txtClinRep').focus();
        return false;
    }
    if(!checkIsAlphaSpaceVar('txtPatNum')) {
        window.alert('Invalid character(s) in Patient Number');
        document.getElementById('txtPatNum').focus();
        return false;
    }
    if(checkIsIllegalChar('txtNarrative')) {
        window.alert('Invalid character(s) in Complementary Information');
        document.getElementById('txtNarrative').focus();
        return false;
    }
    return true;
}

function clearList() {
    document.getElementById('divDupCases').innerHTML="";
    document.getElementById('divDupCases').style.display="none";
    return true;
}

function populateList() {
    if(( /* AE related */
        (document.FrmQuickScreen.txtPTCode_ID.value == '') &&
        (document.FrmQuickScreen.txtLLTCode_ID.value == '') &&
        (document.FrmQuickScreen.txtBodySysNum_ID.value == '') &&
        (document.FrmQuickScreen.txtClasfCode_ID.value == '') &&
        (document.FrmQuickScreen.txtOutcomeCode_ID.value == '') &&
        /* others */
        (document.FrmQuickScreen.txtRecvDate.value == '') &&
        (document.FrmQuickScreen.txtClinRep.value == '') &&
        (document.FrmQuickScreen.txtCountryCode_ID.value == '') &&
        (document.FrmQuickScreen.txtProdCode_ID.value == '') &&
        (document.FrmQuickScreen.txtInitials.value == '') &&
        (document.FrmQuickScreen.txtAgeRep.value == '') &&
        (document.FrmQuickScreen.txtSexCode_ID.value == '') &&
        (document.FrmQuickScreen.txtTrial_ID.value == '') &&
        (document.FrmQuickScreen.txtPatNum.value == '') &&
        (document.FrmQuickScreen.txtPhyAssCode_ID.value == '') &&
        (document.FrmQuickScreen.txtCoAssCode_ID.value == '') &&
        (document.FrmQuickScreen.txtNarrative.value == '')
        )) {
        window.alert('Please provide atleast one value.');
        return false;
    }

    document.getElementById('divDupCases').innerHTML="";
    document.getElementById('divDupCases').style.display="block";

    var url = 'respDupCases.jsp?';
    // starts building the query string.
    if(document.getElementById('txtRecvDate').value != '')
        url += '&RcvDt=' + document.getElementById('txtRecvDate').value;
    if(document.getElementById('txtClinRep').value != '')
        url += '&CRI=' + document.getElementById('txtClinRep').value;
    if(document.getElementById('txtCountryCode_ID').value != '')
        url += '&CtryCd=' + document.getElementById('txtCountryCode_ID').value;
    if(document.getElementById('txtProdCode_ID').value != '')
        url += '&ProdCd=' + document.getElementById('txtProdCode_ID').value;
    if(document.getElementById('txtInitials').value != '')
        url += '&PInit=' + document.getElementById('txtInitials').value;
    if(document.getElementById('txtAgeRep').value != '')
        url += '&AgeRep=' + document.getElementById('txtAgeRep').value;
    if(document.getElementById('txtSexCode_ID').value != '')
        url += '&SxCd=' + document.getElementById('txtSexCode_ID').value;
    if(document.getElementById('txtTrial_ID').value != '')
        url += '&TrialNm=' + document.getElementById('txtTrial_ID').value;
    if(document.getElementById('txtPatNum').value != '')
        url += '&PatNum=' + document.getElementById('txtPatNum').value;
    if(document.getElementById('txtPTCode_ID').value != '')
        url += '&PTCod=' + document.getElementById('txtPTCode_ID').value;
    // If the checknox is ticked, set a value for 'AggrPT' in the query string
    if(document.getElementById('chkAggrPT').checked != false)
        url += '&AggrPT=Yes';
    if(document.getElementById('txtLLTCode_ID').value != '')
        url += '&LLTCd=' + document.getElementById('txtLLTCode_ID').value;
    // If the checknox is ticked, set a value for 'chkAggrLLT' in the query string
    if(document.getElementById('chkAggrLLT').checked != false)
        url += '&AggrLLT=Yes';
    if(document.getElementById('txtBodySysNum_ID').value != '')
        url += '&BSysNum=' + document.getElementById('txtBodySysNum_ID').value;
    if(document.getElementById('txtClasfCode_ID').value != '')
        url += '&ClasfCd=' + document.getElementById('txtClasfCode_ID').value;
    if(document.getElementById('txtOutcomeCode_ID').value != '')
        url += '&OutCCod=' + document.getElementById('txtOutcomeCode_ID').value;
    if(document.getElementById('txtPhyAssCode_ID').value != '')
        url += '&PhAssCd=' + document.getElementById('txtPhyAssCode_ID').value;
    if(document.getElementById('txtCoAssCode_ID').value != '')
        url += '&CoAssCd=' + document.getElementById('txtCoAssCode_ID').value;
    if(document.getElementById('txtNarrative').value != '')
        url += '&Nrr=' + document.getElementById('txtNarrative').value;

    return getDynData(url, 'divDupCases');
}