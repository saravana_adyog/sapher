var SAPHER_PARENT_NAME = "pgQueryTool";

function validateInputs() {
    // Value-Code integrity checks
    if( (document.FrmQueryTool.txtProdCode.value != '') &&
        (document.FrmQueryTool.txtProdCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtProdCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtReporter.value != '') &&
        (document.FrmQueryTool.txtReporter_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtReporter.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtCountry.value != '') &&
        (document.FrmQueryTool.txtCountry_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtCountry.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtTrial.value != '') &&
        (document.FrmQueryTool.txtTrial_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtTrial.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtClasfCode.value != '') &&
        (document.FrmQueryTool.txtClasfCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtClasfCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtDoseUnit.value != '') &&
        (document.FrmQueryTool.txtDoseUnit_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtDoseUnit.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtIndication.value != '') &&
        (document.FrmQueryTool.txtIndication_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtIndication.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtLLTCode.value != '') &&
        (document.FrmQueryTool.txtLLTCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtLLTCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtOutcomeCode.value != '') &&
        (document.FrmQueryTool.txtOutcomeCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtOutcomeCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtPTCode.value != '') &&
        (document.FrmQueryTool.txtPTCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtPTCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtSexCode.value != '') &&
        (document.FrmQueryTool.txtSexCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtSexCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtBodySysNum.value != '') &&
        (document.FrmQueryTool.txtBodySysNum_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtBodySysNum.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtCoAssCode.value != '') &&
        (document.FrmQueryTool.txtCoAssCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtCoAssCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtPhyAssCode.value != '') &&
        (document.FrmQueryTool.txtPhyAssCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtPhyAssCode.focus();
        return false;
    }

    if( (document.FrmQueryTool.txtManufCode.value != '') &&
        (document.FrmQueryTool.txtManufCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmQueryTool.txtManufCode.focus();
        return false;
    }

    if( ((document.FrmQueryTool.cmbRepNumCondition.value == 'Range') && (document.FrmQueryTool.txtRepNumEnd.value == '')) ||
        ((document.FrmQueryTool.cmbAgeCondition.value == 'Range') && (document.FrmQueryTool.txtAgeEnd.value == '')) ||
        ((document.FrmQueryTool.cmbDurTreatmentCondition.value == 'Range') && (document.FrmQueryTool.txtTreatEnd.value == '')) ||
        ((document.FrmQueryTool.cmbDurEvtCondition.value == 'Range') && (document.FrmQueryTool.txtEvtEnd.value == '')) ||
        ((document.FrmQueryTool.cmbTimeSpanCondition.value == 'Range') && (document.FrmQueryTool.txtTimeSpanEnd.value == ''))
        ) {
        return false;
    }
        
    return true;
}

function setupCtrlsWithCombo(cmbo, ctrl1, ctrl2,ctrl3,ctrl4) {
    if(document.getElementById(cmbo).value == '>=') {
        document.getElementById(ctrl1).disabled=false;
        document.getElementById(ctrl2).value='';
        document.getElementById(ctrl2).disabled=true;
        if(ctrl3!='' && ctrl4!='') {
            document.getElementById(ctrl3).style.visibility = 'visible';
            document.getElementById(ctrl4).style.visibility = 'hidden';
        }
    }
    if(document.getElementById(cmbo).value == '<=') {
        document.getElementById(ctrl1).value='';
        document.getElementById(ctrl1).disabled=true;
        document.getElementById(ctrl2).disabled=false;
        if(ctrl3!='' && ctrl4!='') {
            document.getElementById(ctrl3).style.visibility = 'hidden';
            document.getElementById(ctrl4).style.visibility = 'visible';
        }
    }
    if(document.getElementById(cmbo).value == 'Range') {
        document.getElementById(ctrl1).disabled=false;
        document.getElementById(ctrl2).disabled=false;
        if(ctrl3!='' && ctrl4!='') {
            document.getElementById(ctrl3).style.visibility = 'visible';
            document.getElementById(ctrl4).style.visibility = 'visible';
        }
    }
    return true;
}

function popupReport(urlToOpen) {
    var window_width = 800;
    var window_height = 600;
    var window_left = (screen.availWidth/2)-(window_width/2);
    var window_top = (screen.availHeight/2)-(window_height/2);
    var winParms = "Status=no" + ",resizable=yes" + ",scrollbars=yes" + ",height="+window_height+",width="+window_width + ",left="+window_left+",top="+window_top;
    var newwindow = window.open(urlToOpen,'_blank',winParms);

    if(!newwindow.opener)
        newwindow.opener = self;

    newwindow.focus()
}


function init() {
    //setupCtrlsWithCombo('cmbRepNumCondition', 'txtRepNumStart', 'txtRepNumEnd','','');
    setupCtrlsWithCombo('cmbTimeSpanCondition', 'txtTimeSpanStart', 'txtTimeSpanEnd','cal_trigger1','cal_trigger2');
    setupCtrlsWithCombo('cmbAgeCondition', 'txtAgeStart', 'txtAgeEnd','','');
    setupCtrlsWithCombo('cmbDurTreatmentCondition', 'txtTreatStart', 'txtTreatEnd','','');
    setupCtrlsWithCombo('cmbDurEvtCondition', 'txtEvtStart', 'txtEvtEnd','','');
}