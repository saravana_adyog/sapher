<!--
Form Id : CSD1
Date    : 22-06-2009.
Author  : Anoop Varma
-->

<%@page contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.Documents"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.DBConnectionException"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
        %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <%!    private final String FORM_ID = "CSD1";
    private boolean bView;
    private Inventory inv = null;
    private User currentUser = null;
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDownloadDoc.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
        bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp: bView=" + bView);
        try {
            if (request.getParameter("DocId") != null) {
                if (bView) {
                    Documents.downloadDocument((String) session.getAttribute("Company"),
                            application.getRealPath(Documents.DOCS_RELATIVE_PATH),
                            request.getParameter("RepNum"),
                            request.getParameter("DocId"));
        %>
        <script type="text/javascript">
            parent.document.location.replace('<%=request.getParameter("FullPath")%>');
        </script>
        <%
                //pageContext.forward();
                } else {
                    throw new ConstraintViolationException("4");
                }
            }
        } catch (ConstraintViolationException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; ConstraintViolationException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgCountrylist.jsp?DelStatus=" + ex.getMessage());
        } catch (DBConnectionException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; DBConnectionException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgCountrylist.jsp?DelStatus=5");
        } catch (GenericException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; GenericException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgCountrylist.jsp?DelStatus=6");
        } catch (RecordNotFoundException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDownloadDoc.jsp; Exception:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("content.jsp?Status=0");
        }
        %>
    </body>
</html>
