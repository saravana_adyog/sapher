<!--
Form Id : CDL11
Date    : 11-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.EthnicOrigin"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Ethnic Origin - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>
        <script>
            function validate(){
                if (document.FrmEOrigin.txtEOriginCode.value == ""){
                    alert( "Ethnic Code cannot be empty.");
                    document.FrmEOrigin.txtEOriginCode.focus();
                    return false;
                }
                if (trim(document.FrmEOrigin.txtEOriginDesc.value) == ""){
                    alert( "Ethnic Origin cannot be empty.");
                    document.FrmEOrigin.txtEOriginDesc.focus();
                    return false;
                }
                if(!checkIsAlphaVar(document.FrmEOrigin.txtEOriginCode.value)) {
                    window.alert('Invalid character(s) in ethnic origin code.');
                    document.getElementById('txtEOriginCode').focus();
                    return false;
                }
                if(!checkIsAlphaSpaceVar(document.FrmEOrigin.txtEOriginDesc.value)) {
                    window.alert('Invalid character(s) in ethnic origin.');
                    document.getElementById('txtEOriginDesc').focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <%!    EthnicOrigin eOrigin = null;
    private final String FORM_ID = "CDL11";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        eOrigin = new EthnicOrigin((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgEOrigin.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgEOrigin.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgEOrigin.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgEOrigin.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgEOrigin.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), ".jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgEOrigin.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("EOriginCode") != null) {
                    eOrigin = new EthnicOrigin((String) session.getAttribute("Company"), request.getParameter("EOriginCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                eOrigin = new EthnicOrigin();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgEOrigin.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmEOrigin" name="FrmEOrigin" method="POST" action="pgSavEOrigin.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>Ethnic Origin</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtEOriginCode">Ethnic Code</label></td>
                                <td class="field"><input type="text" name="txtEOriginCode" title="Ethnic Origin Code"
                                                             id="txtEOriginCode" size="3" maxlength="2"
                                                             value="<%=eOrigin.getEthnic_Code()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtEOriginDesc">Ethnic Origin</label></td>
                                <td class="field"><input type="text" name="txtEOriginDesc" id="txtEOriginDesc" size="50" title="Ethnic Origin"
                                                             maxlength="50" value="<%=eOrigin.getEthnic_Desc()%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                                             <%
        if (eOrigin.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgEOriginlist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>