<!--
Form Id : USM3
Date    : 13-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.User"
isThreadSafe="true"
import="bl.sapher.general.Logger"
    %>

<%!    private User currentUser = null;
%>

<%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgLogout.jsp");
        if (session.getAttribute("CurrentUser") != null) {
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
//De- Registration
            currentUser.unRegisterUser((String) session.getAttribute("Company"), (String) session.getAttribute("SapherSessionId"));

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLogout.jsp: De-Registration of session COMPLETED.");
            session.invalidate();
        }
        pageContext.forward("pgLogin.jsp");
        return;
%>