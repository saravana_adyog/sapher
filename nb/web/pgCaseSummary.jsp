
<!--
Form Id : CSD1
Date    : 29-2-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.CaseDetails" 
import="bl.sapher.Adverse_Event" 
import="bl.sapher.Conc_Medication" 
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="java.text.SimpleDateFormat" 
import="java.util.ArrayList" 
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Case Summary - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script src="libjs/gui.js"></script>

    </head>
    <body>
        <%!    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private boolean bView;
    private boolean bEdit;
    private User currentUser = null;
    private Inventory inv = null;
    private SimpleDateFormat dFormat = null;
    CaseDetails aCase = null;
    private String strRepNum = "";
    // Pagination
    private int nCMListRowCount = 5;
    private int nAEListRowCount = 5;
    private int nCMListPageCount = 1;
    private int nAEListPageCount = 1;
    private int nCMListCurPage = 1;
    private int nAEListCurPage = 1;
    private String strMessageInfo = "";
    // Variable taht decides thether to show the 'Back' button in the page
    private String strShowBackBtn = "";
        %>
        <%
        aCase = new CaseDetails((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaseSummary.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        boolean resetSession = false;
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Unknown reason.</font>";
            }
        } else {
            strMessageInfo = "";
        }

        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Update Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Unknown reason.</font>";
            }
        } else {
            strMessageInfo = "";
        }

        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp: E/V=" + bEdit + "/" + bView);
            if (!bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; Lack of permission. [View=" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }

            if (!bEdit) {
                saveFlag = "V";
            }
            dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);

            // Check whether the 'Back' button is to be shown
            if (request.getParameter("ShowBkBtn") != null) {
                strShowBackBtn = request.getParameter("ShowBkBtn");
            }

            if (request.getParameter("RepNum") != null) {
                strRepNum = request.getParameter("RepNum");
            }

            if (request.getParameter("AeListCurPage") == null) {
                nAEListCurPage = 1;
            } else {
                nAEListCurPage = Integer.parseInt(request.getParameter("AeListCurPage"));
            }

            if (request.getParameter("CmListCurPage") == null) {
                nCMListCurPage = 1;
            } else {
                nCMListCurPage = Integer.parseInt(request.getParameter("CmListCurPage"));
            }

            aCase = new CaseDetails((String) session.getAttribute("Company"), "y", Integer.parseInt(request.getParameter("RepNum")));

        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='750' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Case Summary</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmCaseSummary" name="FrmCaseSummary" method="POST" action="pgCaseDetails.jsp">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Case summary</td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtRepNum">Rep Num</label></td>
                                <td class="field"><input type="text" name="txtRepNum"
                                                             id="txtRepNum" size="12" maxlength="10"
                                                             value=<%=aCase.getRep_Num()%> readonly />
                                    <label class="fLabel" for="txtRepNum">
                                        <%
        if (aCase.getIs_Suspended() == 0) {
            out.write("ACTIVE");
        } else {
            out.write("<font color=\"RED\">SUSPENDED</font>");
        }
                                        %>
                                    </label>
                                </td>
                                <td colspan="8" align="right"><b><%=strMessageInfo%></b></td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtRecvDate">Received Date</label></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtRecvDate" id="txtRecvDate" size="12" maxlength="12"
                                                       readonly
                                                       value="<%=(aCase.getDate_Recv() != null ? dFormat.format(aCase.getDate_Recv()) : "")%>">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtCountryCode">Country</label></td>
                                <td class="field" colspan="4">
                                    <input type="text" name="txtCountryCode" id="txtCountryCode" size="3" maxlength="3"
                                           readonly
                                           value="<%=(aCase.getCntry() == null ? "" : aCase.getCntry())%>">
                                    <input type="text" name="txtCountryNm" id="txtCountryNm" size="30" maxlength="50" width=10
                                           readonly value="<%=(aCase.getCountry_Nm() == null ? "" : aCase.getCountry_Nm())%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtProdCode">Product</label></td>
                                <td class="field" colspan="9">
                                    <input type="text" name="txtProdCode" id="txtProdCode" size="13" maxlength="12"
                                           readonly
                                           value="<%=(aCase.getProdcode() == null ? "" : aCase.getProdcode())%>">
                                    <input type="text" name="txtGenericNm" id="txtGenericNm" size="28" maxlength="50"
                                           readonly value="<%=(aCase.getGeneric_Nm() == null ? "" : aCase.getGeneric_Nm())%>">
                                    <input type="text" name="txtBrandNm" id="txtBrandNm" size="29" maxlength="50"
                                           readonly value="<%=(aCase.getBrand_Nm() == null ? "" : aCase.getBrand_Nm())%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtInitials">Patient Initials</label></td>
                                <td >
                                    <input type="text" name="txtInitials" id="txtInitials" size="5" maxlength="4"
                                           value="<%=(aCase.getP_Initials() == null ? "" : aCase.getP_Initials())%>" readonly>
                                    <label class="fLabel" for="txtAgeRep">Age Reported</label>
                                    <input type="text" name="txtAgeRep" id="txtAgeRep" size="3" maxlength="3"
                                           value="<%="" + (aCase.getAge_Reported() > 1 ? aCase.getAge_Reported() : "")%>" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtSexCode">Sex</label></td>
                                <td>
                                    <input type="text" name="txtSexCode" id="txtSexCode" size="3" maxlength="1"
                                           readonly
                                           value="<%=(aCase.getSexcode() == null ? "" : aCase.getSexcode())%>">
                                    <input type="text" name="txtSex" id="txtSex" size="30" maxlength="20"
                                           readonly
                                           value="<%=(aCase.getSex_Desc() == null ? "" : aCase.getSex_Desc())%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtTrial">Source/Trial</label></td>
                                <td class="field">
                                    <input type="text" name="txtTrial" id="txtTrial" size="36" maxlength="20"
                                           readonly
                                           value="<%=(aCase.getTrialnum() == null ? "" : aCase.getTrialnum())%>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label></label></td>
                                <td class="field" colspan="2" >
                                    <input type="submit" name="cmdEdit_View" title="Details of this case" class="button" value="<%=(bEdit ? "Edit" : "View")%>" style="width: 60px;">
                                    <input type="button" name="cmdClone" title="Clone this case" class="button"
                                           value="Clone Case" <%=(bEdit ? "" : " Disabled ")%> style="width: 90px;"
                                           onclick="location.replace('pgSavCaseClone.jsp?RepNum=<%=strRepNum%>&user=<%=currentUser.getUsername()%>');" />
                                    <input type="hidden" id="Parent" name="Parent" value="0">
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=(bEdit ? "U" : "V")%>">
                                    <input type="hidden" id="RepNum" name="RepNum" value="<%=strRepNum%>">
                                </td>
                                <td class="field" colspan="2" align="right">
                                    <input type="button" name="cmdDocs" style="width: 80px;" class="button" value="Documents" title="Documents related to the case <%=strRepNum%>"
                                           onclick="location.replace('pgNavigate.jsp?Target=pgDocslist.jsp?SaveFlag=U&RepNum=<%=strRepNum%>');"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> />
                                    <input type="button" name="cmdNewAE" style="width: 60px;" <%=(bEdit ? "" : "DISABLED")%> class="<%=(bEdit ? "button" : "button disabled")%>" value="Add AE" title="Add new Adverse Event" onclick="javascript:location.replace('pgAE.jsp?AeSaveFlag=I&RepNum=<%=strRepNum%>&Parent=1');">
                                    <input type="button" name="cmdNewCM" style="width: 100px;" <%=(bEdit ? "" : "DISABLED")%> class="<%=(bEdit ? "button" : "button disabled")%>" value="Add Medication" title="Add new Medication" onclick="javascript:location.replace('pgConcMed.jsp?SaveFlag=I&RepNum=<%=strRepNum%>&Parent=1');">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                            <tr>
                                <%
        ArrayList alAE = Adverse_Event.listAEventFromView((String) session.getAttribute("Company"), Integer.parseInt(strRepNum));
                                %>
                                <td class="form_group_title" colspan="4">Adverse Event
                                    <%if (alAE != null) {
            out.write(" - " + alAE.size() + " record(s)");
        }%>
                                </td>

                                <td class="form_group_title" align="right" >
                                    <!-- AE Summary button -->
                                    <input type="button" name="cmdAeSummary" class="button" value="Narrative" style="width: 100px;"
                                           onclick="location.replace('pgAeSummary.jsp?SaveFlag=U&RepNum=<%=strRepNum%>');" title="Adverse Event Summary"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                                        <tr>
                                            <td>
                                                <% if (alAE.size() != 0) {%>
                                                <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table">
                                                    <tr class="grid_header">
                                                        <td width="16"></td>
                                                        <% if (bEdit) {%>
                                                        <td width="16"></td>
                                                        <% }%>
                                                        <td width="50">LLT Code</td>
                                                        <td width="200">Low Level Term</td>
                                                        <td width="75">Seriousness</td>
                                                        <td width="200">Sys Organ Desc</td>
                                                        <td></td>
                                                    </tr>
                                                    <%
     if (alAE != null) {
         Adverse_Event ae = null;

         nAEListPageCount = (int) Math.ceil((double) alAE.size() / nAEListRowCount);
         if (nAEListCurPage > nAEListPageCount) {
             nAEListCurPage = 1;
         }
         int nAEListStartPos = ((nAEListCurPage - 1) * nAEListRowCount);
         for (int i = nAEListStartPos;
                 i < ((nAEListStartPos + nAEListRowCount) > alAE.size() ? alAE.size() : (nAEListStartPos + nAEListRowCount));
                 i++) {
             ae = ((Adverse_Event) alAE.get(i));
             if (ae != null) {
                                                    %>
                                                    <tr class="
                                                        <%
                                                                if (i % 2 == 0) {
                                                                    out.write("even_row");
                                                                } else {
                                                                    out.write("odd_row");
                                                                }
                                                        %>
                                                        ">
                                                        <%
                                                                if (bEdit) {%>
                                                        <td width="16" align="center" title="Edit">
                                                            <a href="pgAE.jsp?RepNum=<%=strRepNum%>&LltCode=<%=ae.getLlt_Code()%>&AeSaveFlag=U">
                                                                <img src="images/icons/edit.gif" alt="Edit" title="Edit the Adverse Event with ID = <%=strRepNum%>" border='0'>
                                                            </a>
                                                        </td>
                                                        <% } else {%>
                                                        <td width="16" align="center" title="View">
                                                            <a href="pgAE.jsp?RepNum=<%=strRepNum%>&LltCode=<%=ae.getLlt_Code()%>&AeSaveFlag=V">
                                                                <img src="images/icons/view.gif" alt="View" title="View details of the Adverse Event with ID = <%=strRepNum%>" border='0'>
                                                            </a>
                                                        </td>
                                                        <% }%>
                                                        <% if (bEdit) {%>
                                                        <td width="16" align="center" title="Delete">
                                                            <a href="pgDelAE.jsp?RepNum=<%=strRepNum%>&LltCode=<%=ae.getLlt_Code()%>"
                                                               onclick="return confirm('Are you sure you want to delete?','Sapher')">
                                                                <img src="images/icons/delete.gif" alt="Delete" title="Delete the Adverse Event with ID = <%=strRepNum%>"  border='0'>
                                                            </a>
                                                        </td>
                                                        <% }%>
                                                        <td width="50" align='left' title="Diag code">
                                                            <%out.write(ae.getLlt_Code());%>
                                                        </td>
                                                        <td width="200" align='left'>
                                                            <%out.write(ae.getLlt_Desc());%>
                                                        </td>
                                                        <td width="75" align='left'>
                                                            <%out.write(ae.getClassification_Desc());%>
                                                        </td>
                                                        <td width="200">
                                                            <%out.write(ae.getBody_System());%>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <% }
         }
     }%>
                                                </table>
                                                <%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 5px;"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <% if (alAE.size() != 0) {%>
                                                <table border="0" cellspacing='0' cellpadding='5' width="100%" class="paginate_panel">
                                                    <tr>
                                                        <td>
                                                            <a href="pgCaseSummary.jsp?AeListCurPage=1&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-first.gif" alt="|<<" title="Go to first page" border="0"></a>
                                                        </td>
                                                        <td><% if (nAEListCurPage > 1) {%>
                                                            <a href="pgCaseSummary.jsp?AeListCurPage=<%=(nAEListCurPage - 1)%>&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-prev.gif" alt="<" title="Go to previous page" border="0"></a>
                                                            <% } else {%>
                                                            <img src="images/icons/page-prev.gif" alt="<" title="Go to previous page" border="0">
                                                            <% }%>
                                                        </td>
                                                        <td nowrap class="page_number">Page <%=nAEListCurPage%> of <%=nAEListPageCount%> </td>
                                                        <td><% if (nAEListCurPage < nAEListPageCount) {%>
                                                            <a href="pgCaseSummary.jsp?AeListCurPage=<%=nAEListCurPage + 1%>&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-next.gif" alt=">" title="Go to next page" border="0"></a>
                                                            <% } else {%>
                                                            <img src="images/icons/page-next.gif" alt=">" title="Go to next page" border="0">
                                                            <% }%>
                                                        </td>
                                                        <td>
                                                            <a href="pgCaseSummary.jsp?AeListCurPage=<%=nAEListPageCount%>&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-last.gif" alt=">>|" title="Go to last page" border="0"></a>
                                                        </td>
                                                        <td width="100%"></td>
                                                    </tr>
                                                </table>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                            <tr>
                                <%
        ArrayList alCM = Conc_Medication.listConc_MedicationFromView((String) session.getAttribute("Company"), Integer.parseInt(strRepNum));
                                %>
                                <td class="form_group_title" colspan="4">Concomitant Medication
                                    <%if (alCM != null) {
            out.write(" - " + alCM.size() + " record(s)");
        }%>
                                </td>

                                <td class="form_group_title" align="right" >
                                    <!-- CM Summary button -->
                                    <input type="button" name="cmdCmSummary" class="button" value="Medical History" style="width: 100px;"
                                           onclick="location.replace('pgConcMedSummary.jsp?SaveFlag=U&RepNum=<%=strRepNum%>');" title="Concomitant Medication Summary"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                                        <tr>
                                            <td colspan="10">
                                                <% if (alCM.size() != 0) {%>
                                                <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table">
                                                    <tr class="grid_header">
                                                        <td width="16"></td>
                                                        <% if (bEdit) {%>
                                                        <td width="16"></td>
                                                        <% }%>
                                                        <td width="150">Generic Name</td>
                                                        <td width="150">Brand Name</td>
                                                        <td width="100">Start Date</td>
                                                        <td width="100">End Date</td>
                                                        <td width="50">Duration(Days)</td>
                                                        <td></td>
                                                    </tr>
                                                    <%
     if ((alCM != null)) {
         Conc_Medication cm = null;

         if (alCM.size() != 0) {
             nCMListPageCount = (int) Math.ceil((double) alCM.size() / nCMListRowCount);
         }
         if (nCMListCurPage > nCMListPageCount) {
             nCMListCurPage = 1;
         }
         int nCMListStartPos = ((nCMListCurPage - 1) * nCMListRowCount);
         for (int i = nCMListStartPos;
                 i < ((nCMListStartPos + nCMListRowCount) > alCM.size() ? alCM.size() : (nCMListStartPos + nCMListRowCount));
                 i++) {
             cm = ((Conc_Medication) alCM.get(i));
             if (cm != null) {
                                                    %>
                                                    <tr class="
                                                        <%
                                                                if (i % 2 == 0) {
                                                                    out.write("even_row");
                                                                } else {
                                                                    out.write("odd_row");
                                                                }
                                                        %>
                                                        ">
                                                        <%
                                                                if (bEdit) {%>
                                                        <td width="16" align="center" title="Edit">
                                                            <a href="pgConcMed.jsp?RepNum=<%=strRepNum%>&ProdCode=<%=cm.getProduct_Code()%>&SaveFlag=U">
                                                                <img src="images/icons/edit.gif" alt="Edit" title="Edit the Concomitant Medication with ID = <%=strRepNum%>" border='0'>
                                                            </a>
                                                        </td>
                                                        <% } else {%>
                                                        <td width="16" align="center" title="View">
                                                            <a href="pgConcMed.jsp?RepNum=<%=strRepNum%>&ProdCode=<%=cm.getProduct_Code()%>&SaveFlag=V">
                                                                <img src="images/icons/view.gif" alt="View" title="View the Concomitant Medication with ID = <%=strRepNum%>" border='0'>
                                                            </a>
                                                        </td>
                                                        <% }%>
                                                        <% if (bEdit) {%>
                                                        <td width="16" align="center" title="Delete">
                                                            <a href="pgDelConcMed.jsp?RepNum=<%=strRepNum%>&ProdCode=<%=cm.getProduct_Code()%>"
                                                               onclick="return confirm('Are you sure you want to delete?','Sapher')">
                                                                <img src="images/icons/delete.gif" alt="Delete" title="Delete the Concomitant Medication with ID = <%=strRepNum%>" border='0'>
                                                            </a>
                                                        </td>
                                                        <% }%>
                                                        <td width="150" align='left' title="Generic name">
                                                            <%=cm.getGeneric_Nm()%>
                                                        </td>
                                                        <td width="150" align='left' title="Brand name">
                                                            <%=cm.getBrand_Nm()%>
                                                        </td>
                                                        <td title="Treatment start date">
                                                            <%
                                                                if (cm.getTreat_Start() != null) {
                                                                    out.write(dFormat.format(cm.getTreat_Start()));
                                                                }
                                                            %>
                                                        </td>
                                                        <td title="Treatment end date">
                                                            <%
                                                                if (cm.getTreat_Stop() != null) {
                                                                    out.write(dFormat.format(cm.getTreat_Stop()));
                                                                }
                                                            %>
                                                        </td>
                                                        <td width="100" align='left' title="Treatment duration">
                                                            <%="" + (cm.getDuration() == -1 ? "" : cm.getDuration())%>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <%}
         }
     }
                                                    %>
                                                </table>
                                                <%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 5px;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="10" class="formbody">
                                                <% if (alCM.size() != 0) {%>
                                                <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                                                    <tr>
                                                        <td>
                                                            <a href="pgCaseSummary.jsp?CmListCurPage=1&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-first.gif" alt="|<<" title="Go to first page" border="0"></a>
                                                        </td>
                                                        <td><% if (nCMListCurPage > 1) {%>
                                                            <a href="pgCaseSummary.jsp?CmListCurPage=<%=(nCMListCurPage - 1)%>&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-prev.gif" alt="<" title="Go to previous page" border="0"></a>
                                                            <% } else {%>
                                                            <img src="images/icons/page-prev.gif" alt="<" title="Go to previous page" border="0">
                                                            <% }%>
                                                        </td>
                                                        <td nowrap class="page_number">Page <%=nCMListCurPage%> of <%=nCMListPageCount%> </td>
                                                        <td><% if (nCMListCurPage < nCMListPageCount) {%>
                                                            <a href="pgCaseSummary.jsp?CmListCurPage=<%=nCMListCurPage + 1%>&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-next.gif" alt=">" title="Go to next page" border="0"></a>
                                                            <% } else {%>
                                                            <img src="images/icons/page-next.gif" alt=">" title="Go to next page" border="0">
                                                            <% }%>
                                                        </td>
                                                        <td>
                                                            <a href="pgCaseSummary.jsp?CmListCurPage=<%=nCMListPageCount%>&RepNum=<%=strRepNum%>">
                                                            <img src="images/icons/page-last.gif" alt=">>|" title="Go to last page"  border="0"></a>
                                                        </td>
                                                        <td width="100%"></td>
                                                    </tr>
                                                </table>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <input type="button" name="cmdCIOMS" style="width: 80px;" class="button" value="CIOMS I" title="CIOMS I PDF Report"
                                           onclick="popupWnd('bpopup', 'yes','pgPrintCIOMS1.jsp?RepNum=<%=strRepNum%>',800,600)"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> />
                                    <input type="button" name="cmdMedWatch" style="width: 80px;" class="button" value="MedWatch" title="MedWatch PDF Report"
                                           onclick="popupWnd('bpopup', 'yes','pgPrintMedWatch.jsp?RepNum=<%=strRepNum%>',800,600)"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> />
                                    <input type="button" name="cmdWHO" style="width: 80px;" class="button" value="WHO" title="WHO PDF Report"
                                           onclick="popupWnd('bpopup', 'yes','pgPrintWHO.jsp?RepNum=<%=strRepNum%>',800,600)"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> />
                                    <input type="button" name="cmdFullCase" style="width: 80px;" class="button" value="Full Case" title="Full Case PDF Report"
                                           onclick="popupWnd('bpopup', 'yes','pgPrintFullCase.jsp?RepNum=<%=strRepNum%>',800,600)"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> />

                                    <input type="button" name="cmdXML" style="width: 80px;" class="button" value="XML" title="XML Report"
                                           onclick="popupWnd('bpopup', 'yes','pgGenerateXml.jsp?RepNum=<%=strRepNum%>',500,300)"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> />
                                </td>
                                <td align="right">
                                    <input type="button" name="cmdBack" style="width: 60px;" class="button" value="Back" title="Back to Case listing"
                                           onclick="javascript:location.replace('pgNavigate.jsp?Cancellation=1&Target=pgCaselist.jsp');"
                                           <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
        if (strShowBackBtn.equalsIgnoreCase("0")) {
            out.write(" disabled");
        }%> />
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>