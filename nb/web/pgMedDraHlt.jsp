<%-- 
    Document   : pgMedDraHlt
    Created on : Oct 15, 2009, 11:00:14 AM
    Author     : Anoop Varma
--%>

<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="java.util.ArrayList"
    import="bl.sapher.MedDRABrowser"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"
    import="bl.sapher.general.Constants"
    isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>MedDRA Browser - [Sapher]</title>

        <script>var SAPHER_PARENT_NAME = "";</script>
        <script src="libjs/gui.js"></script>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <!-- jQuery treeview -->
        <link rel="stylesheet" href="libjs/jquery/TreeView/jquery.treeview.css" />
        <script src="libjs/jquery/TreeView/lib/jquery.js" type="text/javascript"></script>
        <script src="libjs/jquery/TreeView/lib/jquery.cookie.js" type="text/javascript"></script>
        <script src="libjs/jquery/TreeView/jquery.treeview.js" type="text/javascript"></script>
        <!-- jQuery treeview upto here -->

        <script type="text/javascript" src="libjs/ajax/ajax.js"></script>

        <script type="text/javascript" src="libjs/MedDra-Info-Dialog/dialogue_box.js"></script>
        <style type="text/css" media="all">@import "libjs/MedDra-Info-Dialog/dialogue_box.css";</style>

        <script type="text/javascript">
            $(function() {
                $("#tree").treeview({
                    collapsed: false,
                    animated: "medium",
                    control:"#sidetreecontrol",
                    persist: "location"
                });
            })

            function details(url) {
                var http = createRequestObject();
                http.open('GET',url,true);
                http.onreadystatechange = function() {if(http.readyState == 4){
                        var resp = http.responseText;
                        showDialog('Details',resp,'success');
                    }}
                http.send(null);
            }
        </script>

    </head>

    <body>

        <%!    private final String FORM_ID = "CDL24"; // Body System code
    private String strSOCId = "";
    private String strSOC = "";
    private String strHLGT = "";
    private String strHLGTId = "";
    private String strHLT = "";
    private String strHLTId = "";
    private boolean bView;
    private ArrayList<String> al = null;
    private String strToBeHighlighted;
    private User currentUser = null;
    private int nRowCount = 100;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;

        %>

        <%
        strToBeHighlighted = "";
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgMedDraHlt.jsp");

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            opener.parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
            window.close();
        </script>
        <%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp: V=" + bView);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            opener.parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
            window.close();
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            opener.parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
            window.close();
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            opener.parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
            window.close();
        </script>
        <%
            return;
        }

        if (request.getParameter("SOCID") != null) {
            strSOCId = request.getParameter("SOCID");
        }

        if (request.getParameter("SOC") != null) {
            strSOC = request.getParameter("SOC");
        }

        if (request.getParameter("HLGT") != null) {
            strHLGT = request.getParameter("HLGT");
        }

        if (request.getParameter("HLGTID") != null) {
            strHLGTId = request.getParameter("HLGTID");
        }

        if (request.getParameter("HiLit") != null) {
            strToBeHighlighted = request.getParameter("HiLit");
        }

        al = MedDRABrowser.list(1, (String) session.getAttribute("Company"),
                MedDRABrowser.MEDDRA_ITEMTYPE_HLT, strHLGTId, "");
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">MedDRA Browser</td>
            </tr>

            <tr>
                <td style="height: 1px;"></td>
            </tr>
            <!-- Grid View Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td colspan="10">
                                <div id="sidetree">
                                    <div class="treeheader"><b>Selected Term Hierarchy</b>
                                        <span style="float:right"><a href="pgNavigate.jsp?Cancellation=1&Target=pgMedDraSearch.jsp">Search&nbsp;</a></span>
                                    </div>
                                    <ul id="tree">
                                        <ul>
                                            <li>
                                                <img src="images/icons/16x16-SOC.gif"/>
                                                <a href="pgMedDraSoc.jsp?HiLit=<%=strSOCId%>">
                                                    <%out.write(strSOC + " [" + strSOCId + "]");%>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <img src="images/icons/16x16-HLGT.gif"/>
                                                        <a href="pgMedDraHlgt.jsp?HiLit=<%=strHLGTId%>&SOCID=<%=strSOCId%>&SOC=<%=strSOC%>">
                                                            <%out.write(strHLGT + " [" + strHLGTId + "]");%>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% if (al.size() != 0) {%>
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                        <tr class="grid_header">
                            <td width="16"></td>
                            <td width="100">Code</td>
                            <td>Description</td>
                            <td></td>
                        </tr>
                        <%
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     String[] strCols = null;
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         strCols = al.get(i).split("\\|");
                        %>
                        <tr class="
                            <%
                            if (strToBeHighlighted.equalsIgnoreCase(strCols[0])) {
                                out.write("hilit_row");
                            } else if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            %>
                            ">
                            <% if (bView) {%>
                            <td width="16" align="center" >
                                <a href="#">
                                    <img src="images/icons/16x16-HLT.gif" title="View the entry with ID = <%=strCols[0]%>" border='0'
                                         onclick="details('respMedDraDetails.jsp?Lvl=3&Cod=<%=strCols[0]%>')" />
                                </a>
                            </td>
                            <% }%>

                            <td width="100" align='center'>
                                <a href="pgMedDraPt.jsp?SOCID=<%=strSOCId%>&SOC=<%=strSOC%>&HLGTID=<%=strHLGTId%>&HLGT=<%=strHLGT%>&HLTID=<%=strCols[0]%>&HLT=<%=strCols[1]%>">
                                    <%=strCols[0]%>
                                </a>
                            </td>
                            <td align='left'>
                                <a href="pgMedDraPt.jsp?SOCID=<%=strSOCId%>&SOC=<%=strSOC%>&HLGTID=<%=strHLGTId%>&HLGT=<%=strHLGT%>&HLTID=<%=strCols[0]%>&HLT=<%=strCols[1]%>">
                                    <%=strCols[1]%>
                                </a>
                            </td>

                            <td></td>
                        </tr>
                        <% }%>
                    </table>
                </td>
                <tr>
                </tr>
            </tr>
            <!-- Grid View Ends -->

            <!-- Pagination Starts -->
            <tr>
                <td class="formbody">
                    <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                        <tr>
                            <td>
                                <a href="pgMedDraHlt.jsp?SOCID=<%=strSOCId%>&SOC=<%=strSOC%>&HLGTID=<%=strHLGTId%>&HLGT=<%=strHLGT%>&CurPage=1">
                                <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                            </td>
                            <td><% if (nCurPage > 1) {%>
                                <a href="pgMedDraHlt.jsp?SOCID=<%=strSOCId%>&SOC=<%=strSOC%>&HLGTID=<%=strHLGTId%>&HLGT=<%=strHLGT%>&CurPage=<%=(nCurPage - 1)%>">
                                <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></a>
                                <% } else {%>
                                <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                                <% }%>
                            </td>
                            <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                            <td><% if (nCurPage < nPageCount) {%>
                                <a href="pgMedDraHlt.jsp?SOCID=<%=strSOCId%>&SOC=<%=strSOC%>&HLGTID=<%=strHLGTId%>&HLGT=<%=strHLGT%>&CurPage=<%=nCurPage + 1%>">
                                <img src="images/icons/page-next.gif" title="Go to next page" border="0"></a>
                                <% } else {%>
                                <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                                <% }%>
                            </td>
                            <td>
                                <a href="pgMedDraHlt.jsp?SOCID=<%=strSOCId%>&SOC=<%=strSOC%>&HLGTID=<%=strHLGTId%>&HLGT=<%=strHLGT%>&CurPage=<%=nPageCount%>">
                                <img src="images/icons/page-last.gif" title="Go to last page" border="0"></a>
                            </td>
                            <td width="100%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Pagination Ends -->
            <tr>
            <%}%>
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>