<!--
Form Id : USM3
Date    : 11-12-2007.
Author  : Arun P. Jose
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.general.GenConf"
    import="bl.sapher.general.ClientConf"
    import="bl.sapher.general.Logger"
    import="bl.sapher.general.Constants"
    import="java.text.SimpleDateFormat"
    import="java.util.Calendar"
    isThreadSafe="true"
    %>

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all"> @import "css/master.css"; </style>
        <!--[if IE]> <style type="text/css"> .login_form label{ float: none; }  </style> <![endif]-->
        <link rel="shortcut icon" href="images/icons/sapher.ico" type="image/vnd.microsoft.icon" />
        <link rel="icon" href="images/icons/sapher.ico" type="image/vnd.microsoft.icon" />
    </head>
    <%
        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        GenConf.setLogPath(application.getRealPath("/Reports/logs"));
        String strVer = "";
        String strMessage = "";
        String strCRight = "";
        GenConf gConf = new GenConf();
        session.setAttribute("LogFileName", "sapher_startup_" +
                new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US).format(Calendar.getInstance().getTime()));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLogin; RemoteIP: " + request.getRemoteAddr());

        strVer = gConf.getSysVersion();
        strCRight = gConf.getSysCRight();
       
        if (request.getParameter("LoginStatus") != null) {
            if (request.getParameter("LoginStatus").equals(Constants.LOGIN_STATUS_INVALID_UN_PW)) {
                strMessage = "Invalid username or password support/pg_login";
            } else if (request.getParameter("LoginStatus").equals("2")) {
                strMessage = "Account Locked testram, Please contact administrator";
            } else if (request.getParameter("LoginStatus").equals("3")) {
                strMessage = "User already logged in.";
            } else if (request.getParameter("LoginStatus").equals("4")) {
                strMessage = "Cannot Login! Unknown Reason";
            } else if (request.getParameter("LoginStatus").equals("5")) {
                strMessage = "Login Blocked, DB Maintenance in progress";
            } else if (request.getParameter("LoginStatus").equals("6")) {
                strMessage = "Session timed out. Login again.";
            } else if (request.getParameter("LoginStatus").equals("7")) {
                strMessage = "No more login allowed, as per the license(s).";
            }
        }

        if (request.getParameter("AppStabilityStatus") != null) {
            if (request.getParameter("AppStabilityStatus").equals(Constants.APP_STABILITY_STATUS_ILLEGAL_OP)) {
                strMessage = "Illegal operation. Log in again.";
            }
        }
    %>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr>
                <td align="center" valign="middle" height="100%">
                    <table border="0" cellpadding="0" cellspacing="0" id="login_table">
                        <tr>
                            <td rowspan="2" align="left" width="8" class="bg_norepeat" background="images/login/login-left.jpg"></td>
                            <td valign="top" align="left"  height="158" class="bg_norepeat" background="images/login/loginpage-logo.jpg">
                                <div id="version"
                                     style="position: relative; font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#FFFFFF; left: 130px; top: 115px; width: 250px; ">
                                    <%=strVer%>
                                </div>
                            </td>
                            <td rowspan="2" align="left" width="8" class="bg_norepeat" background="images/login/login-right.jpg"></td>
                        </tr>
                        <tr>
                            <form name="login" method="post" action="pgValidateLogin.jsp">
                                <td align="right" valign="top" height="142">
                                    <div class="error" align="center" style="padding: 5px 0px 2px 0px; "><%=strMessage%></div>
                                    <div class="login_form">
                                        <div><label for="user">Company: </label>
                                            <select name="cmbCompany" style="width:160px">
                                                <%
        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        java.util.ArrayList al = ClientConf.getClients();
        for (int i = 0; i < al.size(); i++) {
            out.write("<option>" + ((ClientConf) al.get(i)).getClientName() + "</option>");
        }%>
                                            </select>
                                        </div>

                                        <div><label for="user">User Id: </label><input type="text" name="txtUsername" id="txtUsername" class="txt"></div>
                                        <div><label for="password">Password: </label><input type="password" name="txtPassword" id="txtPassword" class="txt"></div>
                                    </div>
                                    <div class="col2" style="padding-left: 248px; " align='left'><input type="submit" style="width: 60px;" value="Login" class="button" name="cmdLogin"/></div>
                                </td>
                            </form>
                        </tr>
                    </table>
                    <div align="center" class="copyright_1" ><%=strCRight%></div>
                </td>
            </tr>
        </table>
    </body>
</html>