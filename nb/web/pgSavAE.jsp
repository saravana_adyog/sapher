<!--
Form Id : CSD2
Date    : 04-01-2008.
Author  : Anoop Varma.
-->
<%@page
contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.Adverse_Event"
import="java.sql.Date"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.DBConnectionException"
import="bl.sapher.general.Logger"
import="java.text.SimpleDateFormat"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
        SimpleDateFormat dFormat = null;
        final String FORM_ID = "CSD1";
        String strSaveFlag = "I";
        int intRepNum = -1;
        Date dtEntry = null;
        // String strRecdBy = "";
        // Date dtRecv = null;
        // String strLclRpt = "";
        // String strXRef = "";
        // String strRTypeCode = "";
        int nSuspended = 0;
        // String saveFlag = "";
        // String strParent = "";
        // boolean bDirect = false;
        boolean bAdd = false;
        boolean bEdit = false;
        User currentUser = null;
        Adverse_Event anAdvEvt = null;
        // vars for Quick Screen
        String strPTCode = "";
        String strAggrPT = "N";
        String strLLTCode = "";
        String strAggrLLT = "N";
        String strBodySysNum = "";
        String strClasfCode = "";
        String strOutcomeCode = "";
        // vars for QS ends
        Date dtDateOfReport = null;
        Date dtStartDate = null;
        Date dtEndDate = null;
        String strLhaCode = "";
        String strLabelling = "U";
        String strPrevReportFlag = "";
        String strPrevReport = "";
        String strEvtTreatment = "";
        String strEvtAbateOnStop = "";
        String strSeverityCode = "";
        String StrReportedToLha = "N";

        nSuspended = 0;
        dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            pageContext.forward("pgLogin.jsp?LoginStatus=3");
        }
        session.removeAttribute("DirtyFlag");

        if (request.getParameter("SaveFlag") != null) {
            strSaveFlag = request.getParameter("SaveFlag");
        }

        if (request.getParameter("txtRepNum") != null) {
            if (!request.getParameter("txtRepNum").equals("")) {
                intRepNum = Integer.parseInt(request.getParameter("txtRepNum"));
            }
        }

        if (request.getParameter("chkSuspended") != null) {
            nSuspended = 1;
        }

        if (request.getParameter("txtEntryDate") != null) {
            if (!request.getParameter("txtEntryDate").equals("")) {
                dtEntry = new Date(dFormat.parse(request.getParameter("txtEntryDate")).getTime());
            }
        }

        if (request.getParameter("txtPTCode_ID") != null) {
            strPTCode = request.getParameter("txtPTCode_ID").trim();
        }

        if (request.getParameter("chkAggrPT") != null) {
            strAggrPT = "Y";
        } else {
            strAggrPT = "N";
        }

        if (request.getParameter("txtLLTCode_ID") != null) {
            strLLTCode = request.getParameter("txtLLTCode_ID").trim();
        }

        if (request.getParameter("chkAggrLLT") != null) {
            strAggrLLT = "Y";
        } else {
            strAggrLLT = "N";
        }

        if (request.getParameter("txtBodySysNum_ID") != null) {
            strBodySysNum = request.getParameter("txtBodySysNum_ID").trim();
        }

        if (request.getParameter("txtClasfCode_ID") != null) {
            strClasfCode = request.getParameter("txtClasfCode_ID").trim();
        }

        if (request.getParameter("txtOutcomeCode_ID") != null) {
            strOutcomeCode = request.getParameter("txtOutcomeCode_ID").trim();
        }

        if (request.getParameter("txtRepNum") != null) {
            intRepNum = Integer.parseInt(request.getParameter("txtRepNum"));
        }

        if (request.getParameter("txtDateOfReport") != null) {
            if (!request.getParameter("txtDateOfReport").equals("")) {
                dtDateOfReport = new Date(dFormat.parse(request.getParameter("txtDateOfReport")).getTime());
            }
        }

        if (request.getParameter("txtStartDate") != null) {
            if (!request.getParameter("txtStartDate").equals("")) {
                dtStartDate = new Date(dFormat.parse(request.getParameter("txtStartDate")).getTime());
            }
        }

        if (request.getParameter("txtEndDate") != null) {
            if (!request.getParameter("txtEndDate").equals("")) {
                dtEndDate = new Date(dFormat.parse(request.getParameter("txtEndDate")).getTime());
            }
        }

        if (request.getParameter("txtSeverityCode_ID") != null) {
            strSeverityCode = request.getParameter("txtSeverityCode_ID").trim();
        }

        if (request.getParameter("chkReportedToLha") != null) {
            StrReportedToLha = "Y";
        } else {
            StrReportedToLha = "N";
        }

        if (request.getParameter("txtLhaCode_ID") != null) {
            strLhaCode = request.getParameter("txtLhaCode_ID").trim();
        }

        if (request.getParameter("ChkLabelling") != null) {
            strLabelling = "E";
        } else {
            strLabelling = "U";
        }

        if (request.getParameter("chkPrevReportFlag") != null) {
            strPrevReportFlag = "Y";
        } else {
            strPrevReportFlag = "N";
        }

        if (request.getParameter("txtPrevReport") != null) {
            strPrevReport = request.getParameter("txtPrevReport");
        }

        if (request.getParameter("txtEvtTreatment") != null) {
            strEvtTreatment = request.getParameter("txtEvtTreatment");
        }

        if (request.getParameter("chkEvtAbateOnStop") != null) {
            strEvtAbateOnStop = "Y";
        } else {
            strEvtAbateOnStop = "N";
        }

        if (strSaveFlag.equals("I")) {
            try {
                if (!bAdd) {
                    throw new ConstraintViolationException("2");
                }
                anAdvEvt = new Adverse_Event();
                anAdvEvt.saveAdverse_Event((String) session.getAttribute("Company"), "I",
                        currentUser.getUsername(), intRepNum, strPTCode,
                        strLLTCode, strOutcomeCode, strBodySysNum, strClasfCode,
                        strSeverityCode, StrReportedToLha, strLhaCode, dtDateOfReport,
                        dtStartDate, dtEndDate, strLabelling, strPrevReportFlag,
                        strPrevReport, strEvtTreatment, strEvtAbateOnStop, strAggrPT,
                        strAggrLLT);

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; Insertion success.");
                //intRepNum = anAdvEvt.getRep_Num();
                pageContext.forward("pgCaseSummary.jsp?SavStatus=0&RepNum=" + intRepNum + "&SaveFlag=U");

            } catch (ConstraintViolationException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

                pageContext.forward("pgCaseSummary.jsp?SavStatus=" + ex.getMessage());
            } catch (DBConnectionException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

                pageContext.forward("pgCaseSummary.jsp?SavStatus=4");
            } catch (GenericException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

                pageContext.forward("pgCaseSummary.jsp?SavStatus=5");
            } catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
            }
        } else if (strSaveFlag.equals("U")) {
            try {
                if (!bEdit) {
                    throw new ConstraintViolationException("2");
                }
                anAdvEvt = new Adverse_Event((String) session.getAttribute("Company"), "y", intRepNum, strLLTCode);

                anAdvEvt.saveAdverse_Event((String) session.getAttribute("Company"), "U",
                        currentUser.getUsername(), intRepNum, strPTCode,
                        strLLTCode, strOutcomeCode, strBodySysNum, strClasfCode,
                        strSeverityCode, StrReportedToLha, strLhaCode, dtDateOfReport,
                        dtStartDate, dtEndDate, strLabelling, strPrevReportFlag,
                        strPrevReport, strEvtTreatment, strEvtAbateOnStop, strAggrPT,
                        strAggrLLT);

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; Updation success (ID:" + intRepNum + ").");
                pageContext.forward("pgCaseSummary.jsp?SavStatus=0&RepNum=" + intRepNum + "&SaveFlag=U");

            /*                if (request.getParameter("Parent").equals("0"))
            pageContext.forward("content.jsp?Status=2");
            else
            if (request.getParameter("Next") == null)
            pageContext.forward("pgCaseSummary.jsp?SavStatus=0");
            else if (request.getParameter("Next").equals("2"))
            pageContext.forward("pgCaseSummary.jsp?SavStatus=0");
             */                //pageContext.forward("pgCaseS.jsp?RepNum=" + intRepNum + "&&SaveFlag=U&&Parent=" + strParent);
            } catch (ConstraintViolationException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=" + ex.getMessage());
            } catch (DBConnectionException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=4");
            } catch (GenericException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=5");
            }
        }
        %>
    </body>
</html>
