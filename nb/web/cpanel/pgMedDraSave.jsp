<%-- 
    Document   : pgMedDraSave
    Created on : Nov 3, 2009, 2:48:04 PM
    Author     : anoop
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"
        import="java.util.List"
        import="java.util.Iterator"
        import="java.util.ArrayList"
        import="java.io.File"
        import="org.apache.commons.fileupload.servlet.ServletFileUpload"
        import="org.apache.commons.fileupload.FileItem"
        import="org.apache.commons.fileupload.disk.DiskFileItemFactory"
        import="bl.sapher.general.TimeoutException"
        import="bl.sapher.general.SimultaneousOpException"
        import="bl.sapher.general.GenericException"
        import="bl.sapher.general.Logger"
        import="bl.sapher.general.ClientConf"
        import="bl.sapher.MedDRABrowser"
        import="bl.sapher.User"
        %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title></title>
    </head>
    <body>
        <%
        String FILE_REPOSITORY_PATH = "";
        String strClient = "";

        String strUN = "";
        String strPW = "";
        %>

        <%
        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            if (request.getParameter("Client") != null) {
                strClient = (String) request.getParameter("Client");
            } else {
                strClient = (String) session.getAttribute("MedDra_Client");
            }

        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavDocs.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        }

        if (!strClient.equalsIgnoreCase("all clients")) { /* Case of a specific client */
            // Copy the MedDRA files to the client specific path
            FILE_REPOSITORY_PATH = ClientConf.getByClientName(strClient).getMedDraUpdatePath();
            try {
                // upload the .asc files to the specific location of the DB server
                MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).uploadMedDraFiles(request, FILE_REPOSITORY_PATH);

                // execute the procedure on external tables
                MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).updateDB(strUN, strPW, strClient);

                pageContext.forward("content.jsp?Status=4");
            } catch (Throwable error) {
                pageContext.forward("content.jsp?Status=5");
                error.printStackTrace();
                throw error;
            }
        } else { /* Case of all clients */
            ArrayList<ClientConf> al = ClientConf.getClients();
            ClientConf cc = null;
            FILE_REPOSITORY_PATH = "";
            for (int i = 0; i < al.size(); i++) {
                cc = al.get(i);

                FILE_REPOSITORY_PATH = cc.getMedDraUpdatePath();

                try {
                    // upload the .asc files to the specific location of the DB server
                    MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).uploadMedDraFiles(request, FILE_REPOSITORY_PATH);

                    // execute the procedure on external tables
                    MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).updateDB(strUN, strPW, cc.getClientName());

                    pageContext.forward("content.jsp?Status=4");
                } catch (SimultaneousOpException soe) {
                    pageContext.forward("content.jsp?Status=7");
                    soe.printStackTrace();
                } catch (GenericException e) {
                    pageContext.forward("content.jsp?Status=5");
                    e.printStackTrace();
                }
            }
        }
        %>
    </body>
</html>
