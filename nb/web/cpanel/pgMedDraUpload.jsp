<%-- 
    Document   : pgMedDraUpload
    Created on : Nov 2, 2009
    Author     : Anoop Varma
--%>

<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.CPanel"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.general.Logger"
    isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>MedDRA Data updation - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "../css/master.css";</style>
        <script type="text/javascript" src="../libjs/gui.js"></script>

        <script>
            function validate() {
                if(document.getElementById('fuFileUpload_SOC').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_SOC').focus();
                    return false;
                }
                if(!/(SOC.ASC)$/i.test(document.getElementById('fuFileUpload_SOC').value)) {
                    alert("Please select the SOC.ASC file");
                    document.getElementById('fuFileUpload_SOC').form.reset();
                    document.getElementById('fuFileUpload_SOC').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_HLGT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_HLGT').focus();
                    return false;
                }
                if(!/(HLGT.ASC)$/i.test(document.getElementById('fuFileUpload_HLGT').value)) {
                    alert("Please select the HLGT.ASC file");
                    document.getElementById('fuFileUpload_HLGT').form.reset();
                    document.getElementById('fuFileUpload_HLGT').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_HLT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_HLT').focus();
                    return false;
                }
                if(!/(HLT.ASC)$/i.test(document.getElementById('fuFileUpload_HLT').value)) {
                    alert("Please select the HLT.ASC file");
                    document.getElementById('fuFileUpload_HLT').form.reset();
                    document.getElementById('fuFileUpload_HLT').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_PT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_PT').focus();
                    return false;
                }
                if(!/(PT.ASC)$/i.test(document.getElementById('fuFileUpload_PT').value)) {
                    alert("Please select the PT.ASC file");
                    document.getElementById('fuFileUpload_PT').form.reset();
                    document.getElementById('fuFileUpload_PT').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_LLT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_LLT').focus();
                    return false;
                }
                if(!/(LLT.ASC)$/i.test(document.getElementById('fuFileUpload_LLT').value)) {
                    alert("Please select the LLT.ASC file");
                    document.getElementById('fuFileUpload_LLT').form.reset();
                    document.getElementById('fuFileUpload_LLT').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_SOC_HLGT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_SOC_HLGT').focus();
                    return false;
                }
                if(!/(SOC_HLGT.ASC)$/i.test(document.getElementById('fuFileUpload_SOC_HLGT').value)) {
                    alert("Please select the SOC_HLGT.ASC file");
                    document.getElementById('fuFileUpload_SOC_HLGT').form.reset();
                    document.getElementById('fuFileUpload_SOC_HLGT').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_HLGT_HLT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_HLGT_HLT').focus();
                    return false;
                }
                if(!/(HLGT_HLT.ASC)$/i.test(document.getElementById('fuFileUpload_HLGT_HLT').value)) {
                    alert("Please select the HLGT_HLT.ASC file");
                    document.getElementById('fuFileUpload_HLGT_HLT').form.reset();
                    document.getElementById('fuFileUpload_HLGT_HLT').focus();
                    return false;
                }
                if(document.getElementById('fuFileUpload_HLT_PT').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload_HLT_PT').focus();
                    return false;
                }
                if(!/(HLT_PT.ASC)$/i.test(document.getElementById('fuFileUpload_HLT_PT').value)) {
                    alert("Please select the HLT_PT.ASC file");
                    document.getElementById('fuFileUpload_HLT_PT').form.reset();
                    document.getElementById('fuFileUpload_HLT_PT').focus();
                    return false;
                }
                return true;
            }

        </script>

    </head>
    <body>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgMedDraUpload.jsp");

        String strUN = "";
        String strPW = "";
        String strClient = "";
        String strPath = "";

        try {

            strUN = (String) session.getAttribute("cpanelUser");
            strPW = (String) session.getAttribute("cpanelPwd");

            // If user name or password is null, The session timed out.
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }

            if (request.getParameter("cmbClientName") != null) {
                strClient = request.getParameter("cmbClientName");
                CPanel cpanel = CPanel.getInstance((String) session.getAttribute("LogFileName"));
                cpanel.updateMedDraFiles(strClient, strPath);
                session.setAttribute("MedDra_Client", strClient);
                out.write("0");
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraUpload.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle"></td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form action="pgMedDraSave.jsp" enctype="multipart/form-data" method="post">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Upload documents</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload SOC.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_SOC" id="fuFileUpload_SOC"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload HLGT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_HLGT" id="fuFileUpload_HLGT"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload HLT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_HLG" id="fuFileUpload_HLG"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload PT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_PT" id="fuFileUpload_PT"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload LLT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_LLT" id="fuFileUpload_LLT"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload SOC_HLGT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_SOC_HLGT" id="fuFileUpload_SOC_HLGT"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload HLGT_HLT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_HLGT_HLT" id="fuFileUpload_HLGT_HLT"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Upload HLT_PT.ASC:</td>
                                <td class="field">
                                    <input type="file" name="fuFileUpload_HLT_PT" id="fuFileUpload_HLT_PT"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td class="fLabel" width="200">Max File Size allowed is 10 MB.</td>
                            </tr>

                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="button" class="button" value="Upload" onclick="return validate();"/>
                                    <input type="hidden" name="Client" id="Client" value="<%=strClient%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('content.jsp?Status=6');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>