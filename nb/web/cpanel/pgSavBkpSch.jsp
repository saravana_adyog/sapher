
<!--
Form Id : CPL7.3
Date    : 14-02-2009
Author  : Arun P. Jose
-->

<%@page contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.BackupSchedule"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.DBConnectionException"
import="bl.sapher.general.GenericException"
import="java.text.SimpleDateFormat"
import="java.sql.Date"
        %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>

        <%!    private final String FORM_ID = "CPL7.3";
    private String strUN = "";
    private String strPW = "";
    private String saveFlag = "";
    SimpleDateFormat sdf;
    private Date dtStart;
    private Date dtEnd;
        %>

        <%
        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            saveFlag = ((String) request.getParameter("saveFlag"));

        } catch (TimeoutException toe) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (Exception e) {
            e.printStackTrace();
        %>
        <script type="text/javascript">
            document.location.replace("pgScheduleList.jsp?SavStatus=1");
        </script>
        <%
            return;
        }
        String repeat_interval = "";
        String days = "";
        if (!((String) request.getParameter("txtInterval")).equals("")) {
            repeat_interval = "freq=" + (String) request.getParameter("cmbFrequency") + ";" + "interval=" + (String) request.getParameter("txtInterval") + ";";
        }
        if (request.getParameter("chkSunday") != null) {
            days += "SUN,";
        }
        if (request.getParameter("chkMonday") != null) {
            days += "MON,";
        }
        if (request.getParameter("chkTuesday") != null) {
            days += "TUE,";
        }
        if (request.getParameter("chkWednesday") != null) {
            days += "WED,";
        }
        if (request.getParameter("chkThursday") != null) {
            days += "THU,";
        }
        if (request.getParameter("chkFriday") != null) {
            days += "FRI,";
        }
        if (request.getParameter("chkSaturday") != null) {
            days += "SAT,";
        }
        if (!days.equals("")) {
            repeat_interval += "byday=" + days.substring(0, days.length() - 1) + ";";
        }

        sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        if (request.getParameter("txtstartDate") != null) {
            if (!((String) request.getParameter("txtstartDate")).equals("")) {
                dtStart = new Date(sdf.parse(request.getParameter("txtstartDate")).getTime());//
            }
        }
        if (request.getParameter("txtendDate") != null) {
            if (!((String) request.getParameter("txtendDate")).equals("")) {
                dtEnd = new Date(sdf.parse(request.getParameter("txtendDate")).getTime());//
            }
        }

        try {
            System.out.print("AV> " + dtStart + ", " + dtEnd);
            BackupSchedule.saveBkpSchedule(strUN, strPW, saveFlag,
                    (String) request.getParameter("txtjobName"), (String) request.getParameter("cmbClient"),
                    dtStart,
                    dtEnd,
                    repeat_interval);

            pageContext.forward("pgScheduleList.jsp?SavStatus=0");
        } catch (DBConnectionException ex) {
            ex.printStackTrace();
            pageContext.forward("pgScheduleList.jsp?SavStatus=1");
        } catch (GenericException ex) {
            ex.printStackTrace();
            pageContext.forward("pgScheduleList.jsp?SavStatus=1&Msg=" + ex.getMessage());
        }
        %>
    </body>
</html>
