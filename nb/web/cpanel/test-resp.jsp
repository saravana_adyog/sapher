<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.general.DBCon"
    import="bl.sapher.general.License"
    import="bl.sapher.general.ClientConf"
    import="bl.sapher.general.DBConnectionException"
    import="bl.sapher.general.GenericException"
    isThreadSafe="true"
    %>

<%
        try {
            ClientConf cc = ClientConf.getByClientName(request.getParameter("cmbCompany"));

            out.write("<table  border = \"1\" >");
            out.write("  <tr>");
            out.write("    <td colspan=\"2\">" + "Connection Details" + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>Client</td>");
            out.write("    <td>" + cc.getClientName() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>Host</td>");
            out.write("    <td>" + cc.getHostName() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>Port</td>");
            out.write("    <td>" + cc.getPort() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>SID</td>");
            out.write("    <td>" + cc.getSID() + "</td>");
            out.write("  </tr>");
            out.write("    <td>Username</td>");
            out.write("    <td>" + cc.getUserName() + "</td>");
            out.write("  </tr>");
            out.write("    <td>Passwd</td>");
            out.write("    <td>" + cc.getPassword() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td colspan=\"2\"></td>");
            out.write("  </tr>");

            java.util.ArrayList<License> al = License.listLicense(cc.getClientName(), "");

            out.write("  <tr>");
            out.write("    <td colspan=\"2\">Total " + License.getNumOfUsers(cc.getClientName()) + " users in " + al.size() + " licenses</td>");
            out.write("  </tr>");
            for (int i = 0; i < al.size(); i++) {
                out.write("  <td colspan=\"2\">License " + (i + 1) + "</td>");
                out.write("  <tr>");
                out.write("    <td>" + "" + "</td>");
                out.write("    <td>" + al.get(i).getLicenseKey() + "</td>");
                out.write("  </tr>");
            }
            out.write("</table>");

            DBCon con = new DBCon(request.getParameter("cmbCompany"));
        } catch (DBConnectionException e) {
            StackTraceElement[] arr = e.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                out.write(arr[i].toString() + "\n");
            }
            return;
        } catch (GenericException uce) {
            StackTraceElement[] arr = uce.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                out.write(arr[i].toString() + "\n");
            }
            return;
        } catch (Exception e) {
            StackTraceElement[] arr = e.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                out.write(arr[i].toString() + "\n");
            }
            return;
        }
%>