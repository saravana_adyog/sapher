<!--
Form Id : NAV1.3
Author  : Ratheesh
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <script src="../../libjs/utils.js"></script>

        <!-- For Left Manu Starts -->
        <link rel="stylesheet" type="text/css" href="../../libjs/resources/css/ext-all.css" />
        <script type="text/javascript" src="../../libjs/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="../../libjs/ext-all.js"></script>
        <style type="text/css" media="all">@import "../../css/master.css";</style>
        <script>
            <!--
            Ext.onReady(function(){
                var ce = new Ext.Panel({
                    frame:true,
                    title: 'Control Panel',
                    collapsible:true,
                    renderTo: _el("panel-content"),
                    width:175,
                    contentEl:'cPanel',
                    titleCollapse: true,
                    collapsed: true
                });
            });
            //-->
        </script>
        <!-- For Left Manu Ends -->
    </head>

    <body>
        <div style="height: 100%; width:10px; z-index: 3000; position: absolute; vetical-align: middle; display: none;" id="showLeftFramePnael">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                <tr>
                    <td width="100%" align="center" valign="middle" background="../../images/letpanel/show-bg.gif"><a onclick="mostrarMenu('showLeftFramePnael');return false;" href="#"><img src="../../images/letpanel/show.gif" border="0"  ></a></td>
                </tr>
            </table>
        </div>

        <ul id="cPanel" class="x-hidden">
            <li>
                <img src="../../images/s.gif" style="background-image:url(../../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgBackupList.jsp" target="contentFrame">Backup</a>
            </li>
            <li>
                <img src="../../images/s.gif" style="background-image:url(../../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgScheduleList.jsp" target="contentFrame">Backup Schedule</a>
            </li>
            <li>
                <img src="../../images/s.gif" style="background-image:url(../../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgRestoreMain.jsp" target="contentFrame">Restore</a>
            </li>
            <li>
                <img src="../../images/s.gif" style="background-image:url(../../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgClearDBLanding.jsp" target="contentFrame">Clear Database</a>
            </li>
            <li>
                <img src="../../images/s.gif" style="background-image:url(../../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgDelCaseLanding.jsp" target="contentFrame">Delete All Cases</a>
            </li>
            <li>
                <img src="../../images/s.gif" style="background-image:url(../../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgMedDraUploadLanding.jsp" target="contentFrame">MedDRA Files Update</a>
            </li>
        </ul>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="left_menu_panel" height="100%">
            <tr>
                <td align="right" valign="top" style="padding-top: 15px">
                    <!-- Left Menu Box.1 Starts -->
                    <table width="99%"  border="0" cellspacing="0" cellpadding="0" align="right" height="100%">
                        <!-- Left Menux Box.1 Header Starts -->
                        <tr>
                            <td height="26"  background="../../images/letpanel/nav-header-bg.gif">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="7"><img src="../../images/letpanel/header-left-end.gif"></td>
                                        <td>
                                            <div style="float:left;" id="panel-head">NAVIGATION</div>
                                            <div style="float:left"><img src="../../images/letpanel/nav-header-curve.gif"></div>
                                        </td>
                                        <td width="7"><img src="../../images/letpanel/header-right-end.gif"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- Left Menux Box.1 Header Ends -->
          <!-- Left Menux Box.1 Content Area Starts -->
                        <tr>
                            <td id="panel-content" valign="top" align="center">
                            </td>
                        </tr>
                        <!-- Left Menux Box.1 Content Area Ends -->
                    </table>
                    <!-- Left Menu Box.1 Ends -->
                </td>
                <td width="5" align="center" valign="middle"><a onclick="mostrarMenu('showLeftFramePnael');return false;" href="#"><img src="../../images/letpanel/hide.gif" border="0"  ></a></td>
            </tr>
        </table>
    </body>
</html>