<!--
Form Id : CPL7.2
Date    : 14-02-2009
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.BackupSchedule"
import="bl.sapher.general.ClientConf"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="java.text.SimpleDateFormat"
import="java.util.Calendar"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Backup Schedule - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="../libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="../libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="../libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="../libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->
        <style type="text/css" media="all">@import "../css/master.css";</style>
        <script type="text/javascript" src="../libjs/gui.js"></script>
        <script>
            function validate(){
                if (document.FrmbkpSch.txtstartDate.value == ""){
                    alert( "Start Date & Time cannot be empty.");
                    document.FrmbkpSch.txtstartDate.focus();
                    return false;
                }
                if (document.FrmbkpSch.txtInterval.value == ""){
                    alert( "Frequency cannot be empty.");
                    document.FrmbkpSch.txtInterval.focus();
                    return false;
                }
                return true;
            }
            function kyStart(e){
                var keynum;
                if(window.event) // IE
                {
                    keynum = e.keyCode;
                }
                else if(e.which) // Netscape/Firefox/Opera
                {
                    keynum = e.which;
                }
                if (keynum == 27)
                    document.FrmbkpSch.txtstartDate.value = "";
            }
            function kyEnd(e){
                var keynum;
                if(window.event) // IE
                {
                    keynum = e.keyCode;
                }
                else if(e.which) // Netscape/Firefox/Opera
                {
                    keynum = e.which;
                }
                if (keynum == 27)
                    document.FrmbkpSch.txtendDate.value = "";
            }
        </script>
    </head>
    <body>
        <%!
    private BackupSchedule bkpSch = null;
    private final String FORM_ID = "CPL7.2";
    private String strUN = "";
    private String strPW = "";
    private String saveFlag = "";
    private String freq = "";
    private int interval = -1;
    private String days = "";
    private int index = -1;
        %>
        <%
        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));
            saveFlag = ((String) request.getParameter("saveFlag"));
            interval = -1;
            days = "";
            index = -1;
            freq = "";

            if (strUN==null || strPW==null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
        } catch (TimeoutException toe) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
            
            if (saveFlag.equals("U")) {
                bkpSch = new BackupSchedule(strUN, strPW, request.getParameter("jobName"));
                index = bkpSch.getRepeat_interval().indexOf("freq=");
                if (index != -1) {
                    freq = bkpSch.getRepeat_interval().substring(index + 5, bkpSch.getRepeat_interval().indexOf(";"));
                }
                index = bkpSch.getRepeat_interval().indexOf("interval=");
                if (index != -1) {
                    interval = Integer.parseInt(bkpSch.getRepeat_interval().substring(index + 9, bkpSch.getRepeat_interval().indexOf(";", index)));
                }
                index = bkpSch.getRepeat_interval().indexOf("byday=");
                if (index != -1) {
                    days = bkpSch.getRepeat_interval().substring(index + 6, bkpSch.getRepeat_interval().indexOf(";", index));
                }
            } else {
                bkpSch = new BackupSchedule();
            }

        } catch (RecordNotFoundException e) {
            e.printStackTrace();
            return;
        }
        %>
        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='600' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Control Panel</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmbkpSch" name="FrmbkpSch" method="POST" action="pgSavBkpSch.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("U") ? "Edit " : ""))%>Backup Schedule</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtjobName">Job Name</label></td>
                                <td class="field"><input type="text" name="txtjobName" title="Job Name"
                                                             id="txtjobName" size="50" maxlength="50"
                                                             value="<%=bkpSch.getJobName()%>"
                                                             readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="cmbClient">Client</label></td>
                                <td class="field">
                                    <select name="cmbClient" style="width:260px" class="comboclass">
                                        <%
        java.util.ArrayList al = ClientConf.getClients();
        ClientConf cc = null;
        for (int i = 0; i < al.size(); i++) {
            cc = (ClientConf) al.get(i);
            out.write("<option" + " value=\"" + cc.getUserName() + "\"" + (saveFlag.equals("U") && bkpSch.getSchemaName().equalsIgnoreCase(cc.getUserName()) ? " SELECTED " : "") + ">" + cc.getClientName() + "</option>");
        }%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtstartDate">Start Date &amp; Time</label></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtstartDate" title="Start Date &amp; Time"
                                                       id="txtstartDate" size="18" maxlength="18"
                                                       value="<%=(saveFlag.equals("I") ? "" : new SimpleDateFormat("dd-MMM-yyyy kk:mm", java.util.Locale.US).format(bkpSch.getStartDate()))%>"
                                                       readonly onkeydown="return kyStart(event);">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                            <a href="#" id="cal_trigger1"><img src="../images/icons/cal.gif" border='0' alt="Select" title="Select Date" name="imgCal"></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtstartDate">End Date</label></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtendDate" title="Start Date"
                                                       id="txtendDate" size="18" maxlength="18"
                                                       value="<%=(saveFlag.equals("I") ? "" : ((bkpSch.getEndDate() == null) ? "" : new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US).format(bkpSch.getEndDate())))%>"
                                                       readonly onkeydown="return kyEnd(event);">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                            <a href="#" id="cal_trigger2"><img src="../images/icons/cal.gif" border='0' alt="Select" title="Select Date" name="imgCal"></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="cmbFrequency">Frequency</label></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" id="txtInterval" name="txtInterval" size="5" maxlength="2" onkeydown="return checkIsNumeric(event);" value="<%=(interval != -1 ? interval : "")%>">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <select name="cmbFrequency" style="width:80px" class="comboclass">
                                                    <option value="daily" <%=(freq.equals("daily") ? "SELECTED" : "")%>>Days</option>
                                                    <option value="weekly" <%=(freq.equals("weekly") ? "SELECTED" : "")%>>Weeks</option>
                                                    <option value="monthly" <%=(freq.equals("monthly") ? "SELECTED" : "")%>>Months</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkSunday">Select Days</label></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                        <td class="fLabel">
                                            <label for="chkSunday">Sunday</label>
                                        </td>
                                        <td style="padding-right: 3px; padding-left: 0px;">
                                            <input type="checkbox" id="chkSunday" name="chkSunday" <%=(days.indexOf("SUN") != -1 ? "CHECKED" : "")%>>
                                        </td>
                                        <td class="fLabel">
                                            <label for="chkMonday">Monday</label>
                                        </td>
                                        <td style="padding-right: 3px; padding-left: 0px;">
                                            <input type="checkbox" id="chkMonday" name="chkMonday" <%=(days.indexOf("MON") != -1 ? "CHECKED" : "")%>>
                                        </td>
                                        <td class="fLabel">
                                            <label for="chkTuesday">Tuesday</label>
                                        </td>
                                        <td style="padding-right: 3px; padding-left: 0px;">
                                            <input type="checkbox" id="chkTuesday" name="chkTuesday" <%=(days.indexOf("TUE") != -1 ? "CHECKED" : "")%>>
                                        </td>
                                        <td class="fLabel">
                                            <label for="chkWednesday">Wednesday</label>
                                        </td>
                                        <td style="padding-right: 3px; padding-left: 0px;">
                                            <input type="checkbox" id="chkWednesday" name="chkWednesday" <%=(days.indexOf("WED") != -1 ? "CHECKED" : "")%>>
                                        </td>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td class="fLabel">
                                                <label for="chkThursday">Thursday</label>
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="checkbox" id="chkThursday" name="chkThursday" <%=(days.indexOf("THU") != -1 ? "CHECKED" : "")%>>
                                            </td>
                                            <td class="fLabel">
                                                <label for="chkFriday">Friday</label>
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="checkbox" id="chkFriday" name="chkFriday" <%=(days.indexOf("FRI") != -1 ? "CHECKED" : "")%>>
                                            </td>
                                            <td class="fLabel">
                                                <label for="chkSaturday">Saturday</label>
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="checkbox" id="chkSaturday" name="chkSaturday" <%=(days.indexOf("SAT") != -1 ? "CHECKED" : "")%>>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"></td>
                                <td>
                                    <input  type="hidden" name="saveFlag" id="saveFlag" value="<%=saveFlag%>">
                                    <input type="submit" class="button" id="cmdSubmit" name="cmdSubmit" value="Save" style="width: 60px;">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('pgScheduleList.jsp');">
                                </td>
                            </tr>
                            <script type="text/javascript">
                                Calendar.setup({
                                    inputField : "txtstartDate", //*
                                    ifFormat : "%d-%b-%Y",// %H:%M
                                    showsTime : true,
                                    button : "cal_trigger1", //*
                                    step : 1
                                });
                            </script>
                            <script type="text/javascript">
                                Calendar.setup({
                                    inputField : "txtendDate", //*
                                    ifFormat : "%d-%b-%Y",
                                    showsTime : false,
                                    button : "cal_trigger2", //*
                                    step : 1
                                });
                            </script>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>