<!--
Form Id : USM4.1
Date    : 11-12-2007.
Author  : Arun P. Jose
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.general.GenConf"
    import="bl.sapher.general.ClientConf"
    isThreadSafe="true"
    %>
    
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />
        
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />
        
        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />
        
        <meta name="author" content="Focal3" />
        
        <style type="text/css" media="all"> @import "../css/master.css"; </style>
        <!--[if IE]> <style type="text/css"> .login_form label{ float: none; }  </style> <![endif]-->
        <link rel="shortcut icon" href="../images/icons/sapher.ico" type="../image/vnd.microsoft.icon" />
        <link rel="icon" href="../images/icons/sapher.ico" type="../image/vnd.microsoft.icon" />
    </head>
    
    <%
            ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
            String strVer = "";
            String strMessage = "";
            String strCRight = "";
            GenConf gConf = new GenConf();

            strVer = "Control Panel";
            strCRight = gConf.getSysCRight();
            if (request.getParameter("LoginStatus") != null) {
                if (request.getParameter("LoginStatus").equals("1")) {
                    strMessage = "Invalid username or password";
                }
                if (request.getParameter("LoginStatus").equals("6")) {
                    strMessage = "Server timeout.Login again.";
                }
            }
    %>
    <body>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr>
                <td align="center" valign="middle" height="100%">
                    <table border="0" cellpadding="0" cellspacing="0" id="login_table">
                        <tr>
                            <td rowspan="2" align="left" width="8" class="bg_norepeat" background="../images/login/login-left.jpg"></td>
                            <td valign="top" align="left"  height="158" class="bg_norepeat" background="../images/login/loginpage-logo.jpg">
                            <div id="version" style="position: relative; font: Arial, Helvetica, sans-serif; font-size:11px; color:#FFFFFF; left: 130px; top: 115px; width: 200px; "><%=strVer%></div></td>
                            <td rowspan="2" align="left" width="8" class="bg_norepeat" background="../images/login/login-right.jpg"></td>
                        </tr>
                        <tr>
                            <form name="login" method="post" action="pgValidateLogin.jsp">
                                <td align="right" valign="top" height="142">
                                    <div class="error" align="center" style="padding: 5px 0px 2px 0px; "><%=strMessage%></div>
                                    <div class="login_form">
                                        <div><label for="user">User Id: </label><input type="text" name="txtUsername" id="user" class="txt"></div>
                                        <div><label for="password">Password: </label><input type="password" name="txtPassword" id="password"  class="txt"></div>
                                    </div>
                                    <div class="col2" style="padding-left: 248px; " align='left'><input type="submit" style="width: 60px;" value="Login" class="button" name="cmdLogin"/></div>
                                </td>
                            </form>
                        </tr>
                    </table>
                    <div align="center" class="copyright_1" ><%=strCRight%></div>
                </td>
            </tr>
        </table>
    </body>
</html>