<!--
Form Id : CPL3
Date    : 18-3-2008
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.CPanel"
import="bl.sapher.general.TimeoutException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%
        try {
            String strUN = "";
            String strPW = "";
            String strClient = "";

            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));
            strClient = ((String) request.getParameter("clientName"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }

            CPanel.ClearDB(strUN, strPW, strClient);
            pageContext.forward("content.jsp?Status=2");
        } catch (TimeoutException toe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                toe.printStackTrace();
            }
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            pageContext.forward("content.jsp?Status=3");
        }
%>