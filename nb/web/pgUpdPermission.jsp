<!--
Form Id : USM4
Date    : 11-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->
<%@page
contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.Permission"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.Logger"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%!    private final String FORM_ID = "USM4";
    private String strRoleName = "";
    private String strInvId = "";
    private int nView;
    private int nAdd;
    private int nEdit;
    private int nDelete;
    private boolean bEdit;
    private User currentUser = null;
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgUpdPermission.jsp");
        nView = 0;
        nAdd = 0;
        nEdit = 0;
        nDelete = 0;
        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUpdPermission.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUpdPermission.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUpdPermission.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUpdPermission.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (request.getParameter("txtRoleName") != null) {
            strRoleName = request.getParameter("txtRoleName");
        }

        if (request.getParameter("txtInvId") != null) {
            strInvId = request.getParameter("txtInvId");
        }

        if (request.getParameter("chkView") != null) {
            nView = 1;
        }

        if (request.getParameter("chkAdd") != null) {
            nAdd = 1;
        }

        if (request.getParameter("chkEdit") != null) {
            nEdit = 1;
        }

        if (request.getParameter("chkDelete") != null) {
            nDelete = 1;
        }
        try {
            if (!bEdit) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUpdPermission.jsp; No permission for editing.");
                throw new ConstraintViolationException("1");
            }
            Permission.setPermission((String) session.getAttribute("Company"), strInvId, strRoleName, nView, nAdd, nEdit, nDelete);
            pageContext.forward("pgPermissionlist.jsp?SavStatus=0");
        } catch (GenericException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUpdPermission.jsp; Updation failed: GenericException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgPermissionlist.jsp?SavStatus=1");
        }
        %>
    </body>
</html>
