<!--
Form Id : CDL6
Date    : 13-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.Manufacturer"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Manufacturer - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>
        <script>
            function validate(){
                if (document.FrmManufacturer.txtManuCode.value == ""){
                    alert( "Manufacturer Code cannot be empty.");
                    document.FrmManufacturer.txtManuCode.focus();
                    return false;
                }
                if (trim(document.FrmManufacturer.txtManuDesc.value) == ""){
                    alert( "Manufacturer name cannot be empty.");
                    document.FrmManufacturer.txtManuDesc.focus();
                    return false;
                }
                if(!checkIsAlphaVar(document.FrmManufacturer.txtManuCode.value)) {
                    window.alert('Invalid character(s) in manufacturer code.');
                    document.getElementById('txtManuCode').focus();
                    return false;
                }
                if(!checkIsAlphaSpaceVar(document.FrmManufacturer.txtManuDesc.value)) {
                    window.alert('Invalid character(s) in manufacturer name.');
                    document.getElementById('txtManuDesc').focus();
                    return false;
                }
                if(!checkIsPhoneVar(document.FrmManufacturer.txtManuPhone.value)) {
                    window.alert('Invalid character(s) in phone number.');
                    document.getElementById('txtManuPhone').focus();
                    return false;
                }
                if(!checkIsFaxVar(document.FrmManufacturer.txtManuFax.value)) {
                    window.alert('Invalid character(s) in fax.');
                    document.getElementById('txtManuFax').focus();
                    return false;
                }
                if(document.FrmManufacturer.txtManuEmail.value != "") {
                    if(!validateEMail(document.FrmManufacturer.txtManuEmail.value)) {
                        window.alert('Invalid e-mail.');
                        document.getElementById('txtManuEmail').focus();
                        return false;
                    }
                }
                return true;
            }
        </script>
    </head>
    <body>
        <%!    Manufacturer mfr = null;
    private final String FORM_ID = "CDL6";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgManufacturer.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("MfrCode") != null) {
                    mfr = new Manufacturer((String) session.getAttribute("Company"), request.getParameter("MfrCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                mfr = new Manufacturer();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmManufacturer" name="FrmManufacturer" method="POST" action="pgSavManufacturer.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>Manufacturer</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuCode">Mfr. Code</label></td>
                                <td class="field"><input type="text" name="txtManuCode" title="Manufacturer Code"
                                                             id="txtManuCode" size="6" maxlength="5"
                                                             value="<%=mfr.getM_Code()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuDesc">Name</label></td>
                                <td class="field"><input type="text" name="txtManuDesc" id="txtManuDesc" size="30" title="Manufacturer"
                                                             maxlength="50" value="<%=mfr.getM_Name()%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuAddr1">Address</label></td>
                                <td class="field"><input type="text" name="txtManuAddr1" id="txtManuAddr1" size="30"
                                                             maxlength="50" value="<%=(mfr.getM_Addr1() == null ? "" : mfr.getM_Addr1())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuAddr2"></label></td>
                                <td class="field"><input type="text" name="txtManuAddr2" id="txtManuAddr2" size="30"
                                                             maxlength="50" value="<%=(mfr.getM_Addr2() == null ? "" : mfr.getM_Addr2())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuAddr3"></label></td>
                                <td class="field"><input type="text" name="txtManuAddr3" id="txtManuAddr3" size="30"
                                                             maxlength="50" value="<%=(mfr.getM_Addr3() == null ? "" : mfr.getM_Addr3())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuAddr4"></label></td>
                                <td class="field"><input type="text" name="txtManuAddr4" id="txtManuAddr43" size="30"
                                                             maxlength="50" value="<%=(mfr.getM_Addr4() == null ? "" : mfr.getM_Addr4())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuPhone">Phone</label></td>
                                <td class="field"><input type="text" name="txtManuPhone" id="txtManuPhone" size="30"
                                                             maxlength="20" value="<%=(mfr.getM_Phone() == null ? "" : mfr.getM_Phone())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuFax">Fax</label></td>
                                <td class="field"><input type="text" name="txtManuFax" id="txtManuFax" size="30"
                                                             maxlength="20" value="<%=(mfr.getM_Fax() == null ? "" : mfr.getM_Fax())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtManuEmail">Email</label></td>
                                <td class="field"><input type="text" name="txtManuEmail" id="txtManuEmail" size="40"
                                                             maxlength="80" value="<%=(mfr.getM_Email() == null ? "" : mfr.getM_Email())%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                                             <%
        if (mfr.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgManufacturerlist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>