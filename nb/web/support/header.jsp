<!--
Form Id : NAV1.1
Author  : Ratheesh
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.general.GenConf"
import="bl.sapher.general.SessionCounter"
isThreadSafe="true"
%>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Sapher</title>
    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "../css/master.css";</style>
</head>
<%
    String strVer = "";
    String strUser = "";
    GenConf gConf = new GenConf();

    strVer = gConf.getSysVersion();
    if (session.getAttribute("CurrentUser") != null)
        strUser = (String)session.getAttribute("CurrentUser");
%>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
            <td id="header" align="left"><img src="../images/header/logo-inner.jpg"><div id="version"><%=strVer%></div></td>
	</tr>
	<tr>
		<td id="navbar">
			<table border='0' cellspacing='0' cellpadding='0' width='100%'>
				<tr>
					<td noWrap style="padding-left: 20px; color: #000; font-size: 11px;">
						Logged in As : <B><%=strUser%></B>
					</td>
					<td noWrap align="right" valign="top">
					<table border='0' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='20' align='center'><img src="../images/header/seperator.gif"></td>
							<td width='64' align='left'><a href="../pgNavigate.jsp?Target=content.jsp" target="contentFrame"><img src="../images/header/home.gif" border="0"></a></td>
							<td width='61' align='left'><a href="#"><img src="../images/header/help.gif" border="0"></a></td>
							<td width='99' align='left'><a href="../pgLogout.jsp" target="_top"><img src="../images/header/logout.gif" border="0"></a></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>