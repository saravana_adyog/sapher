<!--
Form Id : CDL18
Date    : 13-12-2007.
Author  : Jaikishan. S
-->
<%@page
contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.RepType"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.DBConnectionException"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%!    private final String FORM_ID = "CDL18";
    private String strTypeCode = null;
    private String strTypeDesc = null;
    private int nValid;
    private String strSaveFlag;
    private boolean bDirect = false;
    private boolean bAdd;
    private boolean bEdit;
    private User currentUser = null;
        %>

        <%
        nValid = 0;
        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        RepType reptype = null;
        session.removeAttribute("DirtyFlag");

        if (request.getParameter("SaveFlag") != null) {
            strSaveFlag = request.getParameter("SaveFlag");
        }

        if (request.getParameter("txtTypeCode") != null) {
            strTypeCode = request.getParameter("txtTypeCode");
        }

        if (request.getParameter("txtTypeDesc") != null) {
            strTypeDesc = request.getParameter("txtTypeDesc");
        }

        if (request.getParameter("chkValid") != null) {
            nValid = 1;
        }

        if (strSaveFlag.equals("I")) {
            try {
                if (!bAdd) {
                    throw new ConstraintViolationException("2");
                }
                reptype = new RepType();
                reptype.saveRepType((String) session.getAttribute("Company"), strSaveFlag, currentUser.getUsername(), strTypeCode, strTypeDesc, nValid);

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; Insertion success.");
                if (request.getParameter("Parent").equals("0")) {
                    pageContext.forward("content.jsp?Status=2");
                } else {
                    pageContext.forward("pgRepTypelist.jsp?SavStatus=0");
                }
            } catch (ConstraintViolationException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

                pageContext.forward("pgRepTypelist.jsp?SavStatus=" + ex.getMessage());
            } catch (DBConnectionException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

                pageContext.forward("pgRepTypelist.jsp?SavStatus=4");
            } catch (GenericException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

                pageContext.forward("pgRepTypelist.jsp?SavStatus=5");
            } catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
            }
        } else if (strSaveFlag.equals("U")) {
            try {
                if (!bEdit) {
                    throw new ConstraintViolationException("2");
                }
                reptype = new RepType((String) session.getAttribute("Company"), strTypeCode);
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; Updation success. (ID:" + strTypeCode + ").");
                reptype.saveRepType((String) session.getAttribute("Company"), strSaveFlag, currentUser.getUsername(), "", strTypeDesc, nValid);
                pageContext.forward("pgRepTypelist.jsp?SavStatus=0");
            } catch (ConstraintViolationException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgRepTypelist.jsp?SavStatus=" + ex.getMessage());
            } catch (DBConnectionException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgRepTypelist.jsp?SavStatus=4");
            } catch (GenericException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavRepType.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgRepTypelist.jsp?SavStatus=5");
            }
        }
        %>
    </body>
</html>
