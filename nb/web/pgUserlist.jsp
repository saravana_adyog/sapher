<!--
Form Id : USM1
Date    : 15-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>User Listing - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "pgUserlist";</script>
    <script src="libjs/gui.js"></script>
    <script src="libjs/ajax/ajax.js"></script>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>

    <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
    <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

</head>

<body>
<%!    private final String FORM_ID = "USM1";
    private String strUsername = "";
    private String strRolename = "";
    private int nUserLocked = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private User usr = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
%>

<%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgUserlist.jsp");
        boolean resetSession = false;
        boolean curUsr = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Audit reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Username already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Invalid Role Name.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Invalid License key.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUserlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
    bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
    bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUserlist.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUserlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUserlist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUserlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgUser.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions." + "[Add: " + bAdd + "View:" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }
        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtUsername") == null) {
            if (session.getAttribute("txtUsername") == null) {
                strUsername = "";
            } else {
                strUsername = (String) session.getAttribute("txtUsername");
            }
        } else {
            strUsername = request.getParameter("txtUsername");
        }
        session.setAttribute("txtUsername", strUsername);

        if (request.getParameter("txtRolename_ID") == null) {
            if (session.getAttribute("txtRolename_ID") == null) {
                strRolename = "";
            } else {
                strRolename = (String) session.getAttribute("txtRolename_ID");
            }
        } else {
            strRolename = request.getParameter("txtRolename_ID");
        }
        session.setAttribute("txtRolename_ID", strRolename);

        String strLocked = "";
        if (request.getParameter("cmbLockedUser") == null) {
            if (session.getAttribute("cmbLockedUser") == null) {
                strLocked = "All";
            } else {
                strLocked = (String) session.getAttribute("cmbLockedUser");
            }
        } else {
            strLocked = request.getParameter("cmbLockedUser");
        }
        session.setAttribute("cmbLockedUser", strLocked);

        if (strLocked.equals("Locked")) {
            nUserLocked = 1;
        } else if (strLocked.equals("Un-Locked")) {
            nUserLocked = 0;
        } else if (strLocked.equals("All")) {
            nUserLocked = -1;
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }


        if (resetSession) {
            strUsername = "";
            strRolename = "";
            nUserLocked = Constants.STATUS_ALL;
            session.removeAttribute("txtUsername");
            session.removeAttribute("txtRolename_ID");
            session.removeAttribute("cmbLockedUser");
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = User.listUser(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), "", "", Constants.STATUS_ALL);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strUsername + "," + strRolename + "," + nUserLocked);
            al = User.listUser(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), strUsername, strRolename, nUserLocked);
        }
%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">User Administration - User Listing</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <form name="FrmUserlist" METHOD="POST" ACTION="pgUserlist.jsp" >
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Criteria</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtUsername">User Name</label></td>
            <td class="field">
                <input type="text" name="txtUsername" id="txtUsername" size="12" maxlength="10" title="User Name"
                       value="<%=strUsername%>" />
            </td>
            <td width="500" align="right"><b><%=strMessageInfo%></b></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtRolename_ID">Role</label></td>
            <td class="field"  width="300">
                <input type="text" id="txtRolename_ID" name="txtRolename_ID" size="34" onKeyUp="ajax_showOptions('txtRolename_ID',this,'Role',event)"
                       <%
        if (!bEdit) {
            out.write(" readonly ");
        }
                       %> />
            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="cmbLockedUser">Status</label></td>
            <td class="field" colspan="2">
                <span style="float: left">
                    <select id="cmbLockedUser" NAME="cmbLockedUser" class="comboclass" style="width: 90px;" title="Status">
                        <option <%=(strLocked.equals("All") ? "SELECTED" : "")%>>All</option>
                        <option <%=(strLocked.equals("Locked") ? "SELECTED" : "")%>>Locked</option>
                        <option <%=(strLocked.equals("Un-Locked") ? "SELECTED" : "")%>>Un-Locked</option>
                    </select>

                </span>
            </td>
        </tr>
        <tr>
        <td>
            <td colspan="2">
                <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                       onclick="location.replace('pgNavigate.jsp?Target=pgUserlist.jsp');"/>
            </td>
        </td>
    </form>
    <form METHOD="POST" ACTION="pgUser.jsp">
        <td>
            <span style="float: right">
                <input type="hidden" name="SaveFlag" value="I">
                <input type="hidden" name="Parent" value="1">
                <input type="submit" name="cmdNew"  title="Adds a new entry"  class="<%=(bAdd ? "button" : "button disabled")%>"
                       value="New User" style="width: 110px;" <%=(bAdd ? "" : "DISABLED")%>>
            </span>
        </td>
    </form>
    </td>
</tr>
</table>
</td>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <% if (bDelete) {%>
                <td width="16"></td>
                <% }%>
                <td width="200">User Name</td>
                <td width="200">Role Name</td>
                <td width="100">Status</td>
                <td></td>
            </tr>
            <%
     usr = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         usr = ((User) al.get(i));
         curUsr = false;
         if (currentUser.getUsername().equals(usr.getUsername())) {
             curUsr = true;
         }
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <% if (bEdit && !curUsr) {%>
                <td width="16" align="center" >
                    <a href="pgUser.jsp?UserName=<% out.write(usr.getUsername());%>&SaveFlag=U&Parent=1">
                        <img src="images/icons/edit.gif" title="Edit the entry with ID = <%=usr.getUsername()%>" border='0'>
                    </a>
                </td>
                <% } else {%>
                <td width="16" align="center" >
                    <a href="pgUser.jsp?UserName=<% out.write(usr.getUsername());%>&SaveFlag=V&Parent=1">
                        <img src="images/icons/view.gif" title="View details of the entry with ID = <%=usr.getUsername()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <% if (bDelete) {%>
                <td width="16" align="center" >
                    <% if (!curUsr) {%>
                    <a href="pgDelUser.jsp?UserName=<% out.write(usr.getUsername());%>"
                       onclick="return confirm('Are you sure you want to delete?','Sapher')">
                        <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=usr.getUsername()%>" border='0'>
                    </a>
                    <%}%>
                </td>
                <% }%>
                <td width="200" align='left'>
                    <%
                if (usr.getIs_Locked() == 1) {
                    %>
                    <font color="red">
                        <%
                        out.write(usr.getUsername());
                        %>
                    </font>
                    <%
                } else {
                    out.write(usr.getUsername());
                }
                    %>
                </td>
                <td width="200" align='left'>
                    <%
                if (usr.getIs_Locked() == 1) {
                    %>
                    <font color="red"><% out.write(usr.getRole().getRole_Name());%></font>
                    <%
                } else {
                    out.write(usr.getRole().getRole_Name());
                }
                    %>
                </td>
                <td width="100" align='center'>
                    <%
                if (usr.getIs_Locked() == 1) {
                    %>
                    <font color="red"><% out.write("Locked");%></font>
                    <%
                } else {
                    out.write("Un-Locked");
                }
                    %>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgUserlist.jsp?CurPage=1">
                    <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <a href="pgUserlist.jsp?CurPage=<%=(nCurPage - 1)%>">
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></a>
                    <% } else {%>
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <A href="pgUserlist.jsp?CurPage=<%=nCurPage + 1%>">
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0"></A>
                    <% } else {%>
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                    <% }%>
                </td>
                <td>
                    <a href="pgUserlist.jsp?CurPage=<%=nPageCount%>">
                    <img src="images/icons/page-last.gif" title="Go to last page" border="0"></a>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>