<%-- 
    Document   : pgSavCaseAeSummary
    Created on : Aug 25, 2009, 10:17:41 AM
    Author     : anoop
--%>

<%@page
    contentType="text/html"
    pageEncoding="UTF-8"
    import="bl.sapher.CaseDetails"
    import="java.sql.Date"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.ConstraintViolationException"
    import="bl.sapher.general.GenericException"
    import="bl.sapher.general.DBConnectionException"
    import="bl.sapher.general.Logger"
    import="java.text.SimpleDateFormat"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%!    private final String FORM_ID = "CSD1";
    private int intRepNum = -1;
    private Date dtEntry = null;
    private String strParent = "";
    private boolean bDirect = false;
    private boolean bAdd;
    private boolean bEdit;
    private User currentUser = null;
    private String strVerbatim = "";
    private String strAeDesc = "";
        %>

        <%
        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
            pageContext.forward("pgLogin.jsp?LoginStatus=3");
        }

        CaseDetails aCase = null;
        session.removeAttribute("DirtyFlag");

        if (request.getParameter("txtRepNum") != null) {
            if (!request.getParameter("txtRepNum").equals("")) {
                intRepNum = Integer.parseInt(request.getParameter("txtRepNum"));
            }
        }

        // Large fields related to AE.
        if (request.getParameter("txtReactionDesc") != null) {
            strVerbatim = request.getParameter("txtReactionDesc");
        } else {
            strVerbatim = "";
        }

        if (request.getParameter("txtExtendedInfo") != null) {
            strAeDesc = request.getParameter("txtExtendedInfo");
        } else {
            strAeDesc = "";
        }

        /*        // Large fields related to CM.
        if (request.getParameter("txtRelevantHistory") != null) {
        strShortHistory = request.getParameter("txtRelevantHistory");
        } else {
        strShortHistory = "";
        }

        if (request.getParameter("txtShortComment") != null) {
        strShortComment = request.getParameter("txtShortComment");
        } else {
        strShortComment = "";
        }
         */

        try {
            if (!bEdit) {
                throw new ConstraintViolationException("2");
            }
            aCase = new CaseDetails((String) session.getAttribute("Company"), "y", intRepNum);

            aCase.saveCaseAeSummary((String) session.getAttribute("Company"), currentUser.getUsername(),
                    intRepNum, strVerbatim, strAeDesc);

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; Updation success (ID:" + intRepNum + ").");
            pageContext.forward("pgCaseSummary.jsp?SavStatus=0&RepNum=" + intRepNum + "&SaveFlag=U");

        } catch (ConstraintViolationException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
            pageContext.forward("pgCaseSummary.jsp?SavStatus=2&RepNum=" + intRepNum + "&SaveFlag=U");
        } catch (DBConnectionException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
            pageContext.forward("pgCaseSummary.jsp?SavStatus=4&RepNum=" + intRepNum + "&SaveFlag=U");
        } catch (GenericException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCase.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
            pageContext.forward("pgCaseSummary.jsp?SavStatus=5&RepNum=" + intRepNum + "&SaveFlag=U");
        }

        %>
    </body>
</html>
