<!--
Form Id : CSD1
Date    : 17-01-2008, Thu.
Author  : Anoop Varma.
-->

<%@page 
    contentType="text/html"
    pageEncoding="UTF-8"
    import="bl.sapher.CaseDetails"
    import="bl.sapher.Adverse_Event"
    import="bl.sapher.RepType"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.Conc_Medication"
    import="bl.sapher.Product"
    import="bl.sapher.AEDiag"
    import="bl.sapher.AELLT"
    import="bl.sapher.LHA"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.Logger"
    import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
    import="java.text.SimpleDateFormat"
    import="java.util.Date"
    import="java.util.Calendar"
    import="java.util.ArrayList"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Case details - [Sapher]</title>
    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>
    <link rel="stylesheet" href="css/tab-view.css" type="text/css" media="screen">
    <script type="text/javascript" src="libjs/tab/ajax.js"></script>
    <script type="text/javascript" src="libjs/tab/tab-view.js"></script>

    <script type="text/javascript" src="libjs/ajax/ajax.js"></script>
    <script type="text/javascript" src="libjs/pgCaseDetails.js"></script>
    <script type="text/javascript" src="libjs/gui.js"></script>

    <script>var SAPHER_PARENT_NAME = "pgCaseDetails";</script>

    <!-- Calendar Library Includes.. Start -->
    <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" >
    <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" >
    <!-- import the calendar script -->
    <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
    <!-- import the language module -->
    <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
    <!-- helper script that uses the calendar -->
    <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
    <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
    <!--Calendar Library Includes.. End -->

    <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
    <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

    <style type="text/css">
        input{height: 18px;}
    </style>
</head>
<body onload="setAllControls();">

<%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaseDetails.jsp");
        CaseDetails aCase = new CaseDetails((String) session.getAttribute("LogFileName"));
        final String FORM_ID = "CSD1";
        String strRepNum = "";
        String saveFlag = "";
        String strMessageInfo = "";
        String strParent = "";
        int nSuspended = 0;
        boolean bAdd;
        boolean bEdit;
        boolean bView;
        User currentUser = null;
        Inventory inv = null;
        SimpleDateFormat sdf = null;


        sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; Wrong username/password");
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; TimeoutException");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; User already logged in.");
            pageContext.forward("pgLogin.jsp?LoginStatus=1");
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");

                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            }
            if (request.getParameter("SaveFlag") != null) {
                if (!request.getParameter("SaveFlag").equals("I")) {
                    aCase = new CaseDetails((String) session.getAttribute("Company"), "y", Integer.parseInt(request.getParameter("RepNum")));

                    strRepNum = request.getParameter("RepNum");
                    strMessageInfo = (aCase.getIs_Suspended() == 1 ? "SUSPENDED" : "VALID");
                    nSuspended = (aCase.getIs_Suspended() == 1 ? 1 : 0);
                } else { // Insertion mode
                    aCase = new CaseDetails();
                }
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            return;
        }
%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
    <tr>
        <td id="formtitle">Case Detail</td>
    </tr>
    <!-- Form Body Starts -->
    <tr>
        <form id="FrmCaseDetails" name="FrmCaseDetails" method="psot" action="pgSavCase.jsp?Parent=1"
              onsubmit="return validate();">
        <td class="formbody">
            <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td class="fLabel"><label for="txtRepNum">Rep Num</label></td>
                    <td class="field"><input type="text" name="txtRepNum" title="Report Number" style="border:0;font-weight:bold"
                                                 id="txtRepNum" size="15" maxlength="10"
                                                 <%
        if (saveFlag.equals("U")) {
            out.write(" value=\"" + strRepNum + "\"  readonly");
        } else if (saveFlag.equals("I")) {
            out.write(" value=\"\" readonly");
        }
                                                 %> >
                    </td>

                    <td class="fLabelR"><label>Suspended</label></td>
                    <td class="field"><input type="checkbox" name="chkSuspended" id="chkSuspended" class="chkbox" title="Active/Suspended"
                                                 <%
        if (aCase.getIs_Suspended() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                 %> >
                    </td>
                </tr>
                <tr>
                    <td  colspan="9">
                        <div id="casedetailstab">
                        <!-- Tab 1.1 (1): Data Reception -->
                        <div id="tabDataRec" class="dhtmlgoodies_aTab" align="left">
                            <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                                <tr height="2px"></tr>
                                <tr>
                                    <td class="form_group_title" colspan="10">Data Reception</td>
                                </tr>
                                <tr height="2px"></tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td class="fLabel">
                                        <label for="txtEntryDate">Entry Date</label>
                                    </td>
                                    <td class="field" title="Entry date">
                                        <table border='0' cellspacing='0' cellpadding='0'>
                                            <tr>
                                                <td style="padding-right: 3px; padding-left: 0px;">
                                                    <input type="text" name="txtEntryDate" id="txtEntryDate" size="15" maxlength="11"
                                                           value="<%=(aCase.getDate_Entry() != null ? sdf.format(aCase.getDate_Entry()) : "")%>" readonly clear=50 title="Entry Date"
                                                           <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 50);\" ");
        }%> >
                                                </td>
                                                <td style="padding-right: 3px; padding-left: 0px;">
                                                    <%
        if (!saveFlag.equals("V")) {
                                                    %>
                                                    <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select Entry Date"></a>
                                                    <script type="text/javascript">
                                                        Calendar.setup({
                                                            inputField : "txtEntryDate", //*
                                                            ifFormat : "%d-%b-%Y",
                                                            showsTime : true,
                                                            button : "cal_trigger1", //*
                                                            step : 1
                                                        });
                                                    </script>
                                                    <%}%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel"><label for="txtRecdHq">Received By</label></td>
                                    <td class="field" title="Received by">
                                        <input type="text" name="txtRecdHq"
                                               id="txtRecdHq" size="36" maxlength="20" value="<%=(aCase.getRecv_By_At_Hq() == null ? "" : aCase.getRecv_By_At_Hq())%>"
                                               title="Received By whom at company"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel">
                                        <label for="txtRecvDate">Receipt Date</label>
                                    </td>
                                    <td class="field" title="Receipt date">
                                        <table border='0' cellspacing='0' cellpadding='0'>
                                            <tr>
                                                <td style="padding-right: 3px; padding-left: 0px;">
                                                    <input type="text" name="txtRecvDate" id="txtRecvDate" size="15" maxlength="12" title="Receipt Date"
                                                           onchange="if(!dateDifference_new('txtDoB', 'txtRecvDate', 'txtAgeOnset')) {
                                                               this.value=null;
                                                               document.FrmCaseDetails.txtAgeOnset.value = null;
                                                           }"
                                                           value="<%=(aCase.getDate_Recv() != null ? sdf.format(aCase.getDate_Recv()) : "")%>" readonly clear=51
                                                           <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 51);\" ");
        }%> />
                                                </td>
                                                <td style="padding-right: 3px; padding-left: 0px;">
                                                    <%
        if (!saveFlag.equals("V")) {
                                                    %>
                                                    <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" alt="Select" title="Select Receipt Date" border='0' name="imgCal"></a>
                                                    <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField : "txtRecvDate", //*
                                                        ifFormat : "%d-%b-%Y",
                                                        showsTime : true,
                                                        button : "cal_trigger2", //*
                                                        step : 1
                                                    });
                                                    </script>
                                                    <%}%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel"><label for="txtLocalRep">Local Report No.</label></td>
                                    <td class="field" title="Local report number">
                                        <input type="text" name="txtLocalRep" title="Local Report Number"
                                               id="txtLocalRep" size="36" maxlength="10"
                                               value="<%=(aCase.getRep_Num_Local() == null ? "" : aCase.getRep_Num_Local())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel"><label for="txtCrossRef">Cross Ref Rep No.</label></td>
                                    <td class="field" title="Cross reference report number">
                                        <input type="text" name="txtCrossRef" title="Cross Ref Rep Number"
                                               id="txtCrossRef" size="36" maxlength="10"
                                               value="<%=(aCase.getRep_Xref_Num() == null ? "" : aCase.getRep_Xref_Num())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel"><label for="txtCaseDRRTypeCode">Report Type</label></td>
                                    <td class="field" title="Report type">
                                        <input type="text" id="txtCaseDRRTypeCode" name="txtCaseDRRTypeCode" size="45" onkeyup="ajax_showOptions('txtCaseDRRTypeCode_ID',this,'ReportType',event)"
                                               value="<%=(aCase.getReportTypeDesc() == null ? "" : aCase.getReportTypeDesc())%>"
                                               title="Report Type description"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="3" id="txtCaseDRRTypeCode_ID" name="txtCaseDRRTypeCode_ID"
                                               value="<%=(aCase.getRtype() == null ? "" : aCase.getRtype())%>" />

                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                                <!-- Tab 1.2 (2): Source -->
                                <tr height="2px"></tr>
                                <tr>
                                    <td class="form_group_title" colspan="10">Source</td>
                                </tr>
                                <tr height="2px"></tr>
                                <tr>
                                    <td style="height: 10px;"></td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtCountryCode">Country Code</label></td>
                                    <td class="field">
                                        <div id="divCntryDynData">
                                            <input type="text" id="txtCountryCode" name="txtCountryCode" size="45" onkeyup="ajax_showOptions('txtCountryCode_ID',this,'Country',event)"
                                                   value="<%=(aCase.getCountry_Nm() == null ? "" : aCase.getCountry_Nm())%>" title="Country details"
                                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                   %>>
                                            <input type="text" readonly size="3" id="txtCountryCode_ID" name="txtCountryCode_ID"
                                                   value="<%=(aCase.getCntry() == null ? "" : aCase.getCntry())%>">

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="60"><label for="txtReporterStat">Reporter status</label></td>
                                    <td class="field">
                                        <input type="text" id="txtReporterStat" name="txtReporterStat" size="30" onkeyup="ajax_showOptions('txtReporterStat_ID',this,'ReporterStatus',event)"
                                               value="<%=(aCase.getRepstatDesc() == null ? "" : aCase.getRepstatDesc())%>" title="Reporter status details"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="4" id="txtReporterStat_ID" name="txtReporterStat_ID"
                                               value="<%=(aCase.getRepstat() == null ? "" : aCase.getRepstat())%>">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtClinRep">Clinical Reporter Info</label></td>
                                    <td class="field">
                                        <input type="text" name="txtClinRep" id="txtClinRep" size="37" maxlength="35" title="Clinical reporter information"
                                               value="<%=(aCase.getClin_Rep_Info() == null ? "" : aCase.getClin_Rep_Info())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtCpnyRepInfo">Com. Reporter Info</label></td>
                                    <td class="field">
                                        <input type="text" name="txtCpnyRepInfo" id="txtCpnyRepInfo" size="37" maxlength="50" title="Company reporter information"
                                               value="<%=(aCase.getComp_Rep_Info() == null ? "" : aCase.getComp_Rep_Info())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtPatNum">Patient Num</label></td>
                                    <td class="field">
                                        <input type="text" name="txtPatNum" id="txtPatNum" size="37" maxlength="20" title="Patient Number"
                                               value="<%=(aCase.getPatient_Num() == null ? "" : aCase.getPatient_Num())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtReporter">Reporter</label></td>
                                    <td class="field">
                                        <input type="text" id="txtReporter" name="txtReporter" size="30" onkeyup="ajax_showOptions('txtReporter_ID',this,'ReportSource',event)"
                                               value="<%=(aCase.getSource_Desc() == null ? "" : aCase.getSource_Desc())%>" title="Reporter code"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="4" id="txtReporter_ID" name="txtReporter_ID"
                                               value="<%=(aCase.getSrccode() == null ? "" : aCase.getSrccode())%>">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtTrialNum">Source/Trial Num</label></td>
                                    <td class="field">
                                    <input type="text" id="txtTrialNum" name="txtTrialNum" size="37" onkeyup="ajax_showOptions('txtTrialNum_ID',this,'TrialNo',event)"
                                           value="<%=(aCase.getTrialnum() == null ? "" : aCase.getTrialnum())%>" title="Source/Trial Num"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                    <input type="hidden" size="3" id="txtTrialNum_ID" name="txtTrialNum_ID"
                                           value="<%=(aCase.getTrialnum() == null ? "" : aCase.getTrialnum())%>">
                                </tr>
                                <tr>
                                    <td class="fLabel" width="125"><label for="txtHospitalName">Hospital name</label></td>
                                    <td class="field">
                                        <input type="text" name="txtHospitalName" id="txtHospitalName" size="37" maxlength="50" title="Hospital name"
                                               value="<%=(aCase.getHosp_Nm() == null ? "" : aCase.getHosp_Nm())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <!-- Tab 2.1 (3): Product Info -->
                        <div id="tab3" class="dhtmlgoodies_aTab">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <tr height="2px"></tr>
                            <tr>
                                <td class="form_group_title" colspan="10">Product Info</td>
                            </tr>
                            <tr height="2px"></tr>
                            <td class="fLabel" width="125"><label for="txtProdCode">Product</label></td>
                            <td class="field" colspan="9">

                                <input type="text" id="txtProdCode" name="txtProdCode" size="56" onkeyup="ajax_showOptions('txtProdCode_ID',this,'Product',event)"
                                       value="<%
        if ((aCase.getGeneric_Nm() != null && aCase.getBrand_Nm() != null) &&
                (aCase.getGeneric_Nm() != "" && aCase.getBrand_Nm() != "")) {
            out.write(aCase.getBrand_Nm() + " (" + aCase.getGeneric_Nm() + ")");
        } else {
            out.write("");
        }
                                       %>"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        } else {
            out.write(" onpress=\"return ClearInput(event, 'txtProdCode_ID');\" ");
        }
                                       %>>
                                <input type="text" readonly size="13" id="txtProdCode_ID" name="txtProdCode_ID"
                                       value="<%=(aCase.getProdcode() == null ? "" : aCase.getProdcode())%>">
                            </td>

                        </tr>
                        <tr>
                            <td class="fLabel" width="125"><label for="txtCpnyCode">Manufacturer</label></td>
                            <td class="field" width="33%">
                                <input type="text" id="txtCpnyCode" name="txtCpnyCode" size="35" onkeyup="ajax_showOptions('txtCpnyCode_ID', this,'Manufacturer',event);"
                                       value="<%=(aCase.getM_Name() == null ? "" : aCase.getM_Name())%>" title="Manufacturer"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %>>
                                <input type="text" readonly size="5" id="txtCpnyCode_ID" name="txtCpnyCode_ID"
                                       value="<%=(aCase.getMcode() == null ? "" : aCase.getMcode())%>">
                            </td>
                            <td class="fLabel" width="20%"><label for="txtBatchNum">Batch #</label></td>
                            <td class="field" width="30%"/>
                                <input type="text" name="txtBatchNum" id="txtBatchNum" size="7" maxlength="6" title="Batch number"
                                       value="<%=(aCase.getBatch_Num() == null ? "" : aCase.getBatch_Num())%>"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %> />
                            </td>
                        </tr>
                        <tr>
                            <td class="fLabel" width="125"><label for="txtFormulationCode">Formulation</label></td>
                            <td class="field">
                                <input type="text" id="txtFormulationCode" name="txtFormulationCode" size="15" onkeyup="ajax_showOptions('txtFormulationCode_ID',this,'Formulation',event);"
                                       value="<%=(aCase.getFormulation_Desc() == null ? "" : aCase.getFormulation_Desc())%>"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %>>
                                <input type="text" readonly size="5" id="txtFormulationCode_ID" name="txtFormulationCode_ID"
                                       value="<%=(aCase.getFormcode() == null ? "" : aCase.getFormcode())%>">
                            </td>
                            <td class="fLabel" width="125"><label for="txtDose">Dose</label></td>
                            <td class="field">
                                <input type="text" name="txtDose" id="txtDose" size="6" maxlength="6" title="Dose"
                                       onkeydown="if(!checkIsNumericDecVar(this.value))this.value='';"
                                       onkeyup="if(!checkIsNumericDecVar(this.value))this.value='';"
                                       value="<%if ((aCase.getDose() != -1f) && (aCase.getDose() != 0f)) {
            out.write("" + aCase.getDose());
        } else {
            out.write("");
        }%>"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %> />
                            </td>
                        </tr>
                        <tr>
                            <td class="fLabel" width="125"><label for="txtDoseUnit">Dose Unit</label></td>
                            <td class="field">
                                <input type="text" id="txtDoseUnit" name="txtDoseUnit" size="15" onkeyup="ajax_showOptions('txtDoseUnit_ID',this,'DoseUnits',event)"
                                       value="<%=(aCase.getUnit_Desc() == null ? "" : aCase.getUnit_Desc())%>" title="Dose unit"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        } else {
            out.write(" onpress=\"return ClearInput(event, 'txtFormulationCode_ID');\" ");
        }
                                       %>>
                                <input type="text" readonly size="3" id="txtDoseUnit_ID" name="txtDoseUnit_ID"
                                       value="<%=(aCase.getUnitcode() == null ? "" : aCase.getUnitcode())%>">
                            </td>
                            <td class="fLabel" width="125"><label for="txtDoseRegimen">Dose regimen</label></td>
                            <td class="field">
                                <input type="text" id="txtDRegimenCode" name="txtDRegimenCode" size="28" onkeyup="ajax_showOptions('txtDRegimenCode_ID',this,'DoseRgmn',event)"
                                       value="<%=(aCase.getDregimen_Desc() == null ? "" : aCase.getDregimen_Desc())%>" title="Dose regimen"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %>>
                                <input type="text" readonly size="6" id="txtDRegimenCode_ID" name="txtDRegimenCode_ID"
                                       value="<%=(aCase.getDregimen() == null ? "" : aCase.getDregimen())%>">
                            </td>
                        </tr>
                        <tr>
                            <td class="fLabel" width="125"><label for="txtDoseFreq">Dosing Period</label></td>
                            <td class="field" title="Dose frequency">
                                <input type="text" id="txtDoseFreq" name="txtDoseFreq" size="15" onkeyup="ajax_showOptions('txtDoseFreq_ID',this,'DoseFreq',event)"
                                       value="<%=(aCase.getDfreq_Desc() == null ? "" : aCase.getDfreq_Desc())%>"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %>>
                                <input type="text" readonly size="5" id="txtDoseFreq_ID" name="txtDoseFreq_ID"
                                       value="<%=(aCase.getDfreq() == null ? "" : aCase.getDfreq())%>">
                            </td>
                            <td class="fLabel" width="125"><label for="txtRoute">Route</label></td>
                            <td class="field">
                                <input type="text" id="txtRouteCode" name="txtRouteCode" size="28" onkeyup="ajax_showOptions('txtRouteCode_ID',this,'Route',event)"
                                       value="<%=(aCase.getRoute_Desc() == null ? "" : aCase.getRoute_Desc())%>" title="Route"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %>>
                                <input type="text" readonly size="6" id="txtRouteCode_ID" name="txtRouteCode_ID"
                                       value="<%=(aCase.getRoutecode() == null ? "" : aCase.getRoutecode())%>">
                            </td>
                        </tr>
                        <tr>
                        <td class="fLabel">
                            <label for="txtTreatmentStartDate">Treatment Start</label>
                        </td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Treatment start date">
                                        <input type="text" name="txtTreatmentStartDate" id="txtTreatmentStartDate" size="15" maxlength="11"
                                               clear=63 value="<%=(aCase.getTreat_Start() == null ? "" : sdf.format(aCase.getTreat_Start()))%>" readonly
                                               onchange="if(!dateDifference(this.value, document.FrmCaseDetails.txtTreatmentEndDate.value)){
                                               this.value=null;
                                               document.FrmCaseDetails.txtDuration.value = null;
                                           }"
                                               <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"ClearInput(event, 63);if(!dateDifference(this.value, document.FrmCaseDetails.txtTreatmentEndDate.value)){ this.value=null; document.FrmCaseDetails.txtDuration.value = null; }\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Treatment start date">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerTreatStart"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                                    Calendar.setup({
                                        inputField : "txtTreatmentStartDate", //*
                                        ifFormat : "%d-%b-%Y",
                                        showsTime : true,
                                        button : "cal_triggerTreatStart", //*
                                        step : 1
                                    });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="fLabel">
                            <label for="txtTreatmentEndDate">Treatment End</label>
                        </td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Treatment end date">
                                        <input type="text" name="txtTreatmentEndDate" id="txtTreatmentEndDate" size="15" maxlength="11"
                                               clear=64 value="<%=(aCase.getTreat_Stop() == null ? "" : sdf.format(aCase.getTreat_Stop()))%>" readonly
                                               onchange="if(!dateDifference(document.FrmCaseDetails.txtTreatmentStartDate.value, this.value)) {
                                           this.value=null;
                                           document.FrmCaseDetails.txtDuration.value = null;
                                       }"
                                               <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"ClearInput(event, 64);if(!dateDifference(document.FrmCaseDetails.txtTreatmentStartDate.value, this.value)) { this.value=null; document.FrmCaseDetails.txtDuration.value = null;}\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Treatment end date">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerTreatStop"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                                Calendar.setup({
                                    inputField : "txtTreatmentEndDate", //*
                                    ifFormat : "%d-%b-%Y",
                                    showsTime : true,
                                    button : "cal_triggerTreatStop", //*
                                    step : 1
                                });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtDuration">Duration (Days)</label></td>
                    <td class="field">
                        <input type="text" name="txtDuration" id="txtDuration" size="30" maxlength="30" title="Duration"
                               onkeydown="return checkIsNumeric(event);" value="<%
        if ((aCase.getDuration() > 0)) {
            out.write("" + aCase.getDuration());
        }
                               %>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %> />
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtTratmentIndicationCode">Treatment Indication</label></td>
                    <td class="field">
                        <input type="text" id="txtTratmentIndicationCode" name="txtTratmentIndicationCode" size="20" onkeyup="ajax_showOptions('txtTratmentIndicationCode_ID',this,'IndicationTreatment',event)"
                               value="<%=(aCase.getIndication() == null ? "" : aCase.getIndication())%>" title="Treatment indication"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %>>
                        <input type="text" readonly size="8" id="txtTratmentIndicationCode_ID" name="txtTratmentIndicationCode_ID"
                               value="<%=(aCase.getIndnum() == null ? "" : aCase.getIndnum())%>">
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtAuthNum">Authorization Number</label></td>
                    <td class="field">
                        <input type="text" name="txtAuthNum" id="txtAuthNum" size="30" maxlength="20" title="Authorization Number"
                               value="<%=(aCase.getAuthorization_No() == null ? "" : aCase.getAuthorization_No())%>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %> />
                    </td>
                </tr>
                <tr height="2px"></tr>
                <tr>
                    <td class="fLabel"><label for="chkRechallenge">Rechallenge</label></td>
                    <td class="field">
                        <input type="checkbox" name="chkRechallenge" id="chkRechallenge" class="chkbox" onclick="setRechallengeControls();"
                               title="Rechallenge"
                               <%
        if (aCase.getRechallenge() != null) {
            if (aCase.getRechallenge().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                               %> />
                    </td>
                </tr>
                <tr>
                    <td class="fLabel">
                        <label for="txtRechallengeDate">Rechallenge Date</label>
                    </td>
                    <td class="field">
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td style="padding-right: 3px; padding-left: 0px;" title="Rechallenge date">
                                    <input type="text" name="txtRechallengeDate" id="txtRechallengeDate" size="15" maxlength="11"
                                           clear=66 value="<%=(aCase.getRechallenge_Date() != null ? sdf.format(aCase.getRechallenge_Date()) : "")%>" readonly
                                           <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 66);\" ");
        }%> />
                                </td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <%
        if (!saveFlag.equals("V")) {
                                    %>
                                    <a href="#" id="calRechDate" >
                                        <img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' id="imgCalRechDate" name="imgCalRechDate">
                                    </a>
                                    <!--script type="text/javascript">
        /*    if (document.FrmCaseDetails.chkRechallenge.checked) {
                Calendar.setup({
                        inputField : "txtRechallengeDate", //*
                        ifFormat : "%d-%b-%Y",
                        button : "calRechDate", //*
                        step : 1
                        });
            } else {
                        Calendar.setup({
                        displayStat : "D",
                        inputField : "txtRechallengeDate", //*
                        ifFormat : "%d-%b-%Y",
                        button : "calRechDate", //*
                        step : 1
                        });
           }*/
        </script-->
                                    <%}%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtRechallengeDose">Rechallenge Dose</label></td>
                    <td class="field">
                        <input type="text" name="txtRechallengeDose" id="txtRechallengeDose" size="30" maxlength="10"
                               onkeydown="return checkIsNumericDec(event);" title="Rechallenge Dose"
                               value="<%=(aCase.getRechallenge_Dose() == null ? "" : aCase.getRechallenge_Dose())%>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %> />
                    </td>
                    <td class="fLabel"><label for="txtRechallengeResult">Rechallenge Result</label></td>
                    <td class="field" colspan="6">
                        <input type="text" id="txtRechallengeResult" name="txtRechallengeResult" size="24" onkeyup="ajax_showOptions('txtRechallengeResult_ID',this,'RechallengeResult',event)"
                               value="<%=(aCase.getResult_Desc() == null ? "" : aCase.getResult_Desc())%>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %>>
                        <input type="text" readonly size="3" id="txtRechallengeResult_ID" name="txtRechallengeResult_ID"
                               value="<%=(aCase.getRescode() == null ? "" : aCase.getRescode())%>">
                    </td>
                </tr>
                <!-- Tab 2.2 (4): Patient -->
                <tr height="2px"></tr>
                <tr>
                    <td class="form_group_title" colspan="10">Patient</td>
                </tr>
                <tr height="2px"></tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtPatientInitials">Patient Initials</label></td>
                    <td class="field">
                        <input type="text" name="txtPatientInitials" id="txtPatientInitials" size="5" maxlength="4" title="Patient initials"
                               value="<%=(aCase.getP_Initials() == null ? "" : aCase.getP_Initials())%>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %> />
                    </td>
                    <td class="fLabel"><label for="txtSexCode">Sex</label></td>
                    <td class="field" colspan="6">
                        <div id="divSexDynData">
                            <input type="text" id="txtSexCode" name="txtSexCode" size="20" onkeyup="ajax_showOptions('txtSexCode_ID',this,'Sex',event)"
                                   value="<%=(aCase.getSex_Desc() == null ? "" : aCase.getSex_Desc())%>" title="Sex code"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %>>
                            <input type="text" readonly size="3" id="txtSexCode_ID" name="txtSexCode_ID"
                                   value="<%=(aCase.getSexcode() == null ? "" : aCase.getSexcode())%>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtDoB">Date of Birth</label></td>
                    <td class="field">
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td style="padding-right: 3px; padding-left: 0px;" title="Birth date">
                                    <input type="text" name="txtDoB" id="txtDoB" size="15" maxlength="11"
                                           onchange="if(!dateDifference_new('txtDoB', 'txtRecvDate', 'txtAgeOnset')){
                                   this.value=null;
                                   document.FrmCaseDetails.txtAgeOnset.value = null;
                               }"
                                           value="<%=(aCase.getDate_Of_Birth() == null ? "" : sdf.format(aCase.getDate_Of_Birth()))%>" readonly clear=69
                                           <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 69);\" ");
        }%> />
                                </td>
                                <td style="padding-right: 3px;padding-left: 0px;">
                                    <%
        if (!saveFlag.equals("V")) {
                                    %>
                                    <a href="#" id="cal_triggerDoB"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                    <script type="text/javascript">
                        Calendar.setup({
                            inputField : "txtDoB", //*
                            ifFormat : "%d-%b-%Y",
                            showsTime : true,
                            button : "cal_triggerDoB", //*
                            step : 1
                        });
                                    </script>
                                    <%}%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="fLabel" width="125"><label for="txtAgeOnset">Age on Set (yrs)</label></td>
                    <td class="field">
                        <input type="text" name="txtAgeOnset" id="txtAgeOnset" size="4" maxlength="3" title="Age on set"
                               readonly value="" />
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtAgeRep">Age Reported</label></td>
                    <td class="field">
                        <input type="text" name="txtAgeRep" id="txtAgeRep" size="4" maxlength="3" title="Age reported"
                               onkeydown="return checkIsNumeric(event);" value="<%
        if ((aCase.getAge_Reported() != -1) && (aCase.getAge_Reported() != 0)) {
            out.write("" + aCase.getAge_Reported());
        } else {
            out.write("");
        }
                               %>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %> />
                    </td>
                    <td class="fLabel" width="125"><label for="txtWeight">Weight</label></td>
                    <td class="field">
                        <input type="text" name="txtWeight" id="txtWeight" size="4" maxlength="3" title="Weight"
                               onkeydown="return checkIsNumeric(event);" value="<%=((aCase.getWeight() != -1) && (aCase.getWeight() != 0)) ? "" + aCase.getWeight() : ""%>"
                               <% if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }%> >
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtHeight">Height</label></td>
                    <td class="field">
                        <input type="text" name="txtHeight" id="txtHeight" size="4" maxlength="3" title="Height"
                               onkeydown="return checkIsNumeric(event);" value="<%=((aCase.getHeight() != -1) && (aCase.getHeight() != 0)) ? "" + aCase.getHeight() : ""%>"
                               <% if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }%> />
                    </td>
                    <td class="fLabel" width="125"><label for="txtEthnicCode">Ethnic Origin</label></td>
                    <td class="field">
                        <input type="text" id="txtEthnicCode" name="txtEthnicCode" size="30" onkeyup="ajax_showOptions('txtEthnicCode_ID',this,'EthnicOrigin',event)"
                               value="<%=(aCase.getEthnic_Desc() == null ? "" : aCase.getEthnic_Desc())%>"
                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               %>>
                        <input type="text" readonly size="3" id="txtEthnicCode_ID" name="txtEthnicCode_ID"
                               value="<%=(aCase.getEtnicorig() == null ? "" : aCase.getEtnicorig())%>">
                    </td>
                </tr>
                <tr>
                    <td class="fLabel"><label for="chkPregnant">Pregnant</label></td>
                    <td class="field">
                        <input type="checkbox" name="chkPregnant" id="chkPregnant" class="chkbox" onclick="setPregControls();"
                               title="Pregnancy"
                               <%
        if (aCase.getPregnancy() != null) {
            if (aCase.getPregnancy().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                               %> />
                    </td>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtGestWeek">Gestation (week)</label></td>
                        <td class="field">
                            <input type="text" name="txtGestWeek" id="txtGestWeek" size="4" maxlength="3" title="Gestation week at onset"
                                   onkeydown="return checkIsNumeric(event);"
                                   value="<%if ((aCase.getPregnancy_Week() != -1) && (aCase.getPregnancy_Week() != 0)) {
            out.write("" + aCase.getPregnancy_Week());
        } else {
            out.write("");
        }%>"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %> />
                        </td>
                    </tr>
                </tr>
            </table>
            </div>

            <!-- Tab 3.1 (6): Outcome & Assessment -->
            <div id="tab6" class="dhtmlgoodies_aTab">
                <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr height="2px"></tr>
                    <tr>
                        <td class="form_group_title" colspan="10">Outcome & Assessment</td>
                    </tr>
                    <tr height="2px"></tr>
                    <tr>
                        <td class="fLabel" colspan="1"><label for="txtOutcomeCode">Outcome</label></td>
                        <td class="field">
                            <div id="divOCDynData">
                                <input type="text" id="txtOutcomeCode" name="txtOutcomeCode" size="24" onkeyup="ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)"
                                       value="<%=(aCase.getOutcome() == null ? "" : aCase.getOutcome())%>"
                                       <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       %>>
                                <input type="text" readonly size="3" id="txtOutcomeCode_ID" name="txtOutcomeCode_ID"
                                       value="<%=(aCase.getOutcomecode() == null ? "" : aCase.getOutcomecode())%>">
                            </div>
                        </td>
                        <td colspan="7">
                            <input style="border:0" type="text" name="txtAssConclusion" id="txtAssConclusion" size="25" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" colspan="1">
                            <label for="txtDateOfOutcome">Date of Outcome</label>
                        </td>
                        <td class="field" colspan="3">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Date of outcome">
                                        <input type="text" name="txtDateOfOutcome" id="txtDateOfOutcome" size="15" maxlength="11"
                                               clear=72 value="<%=(aCase.getOutcome_Rep_Date() == null ? "" : sdf.format(aCase.getOutcome_Rep_Date()))%>" readonly
                                               <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 72);\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerDoOC"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtDateOfOutcome", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_triggerDoOC", //*
                                step : 1
                            });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtPhyAssCode">Physician Assessment</label></td>
                        <td class="field">
                            <input type="text" id="txtPhyAssCode" name="txtPhyAssCode" size="22" onkeyup="ajax_showOptions('txtPhyAssCode_ID',this,'AssCausality',event)"
                                   value="<%=(aCase.getPhy_Assessment_Code() == null ? "" : aCase.getPhy_Assessment())%>"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %>>
                            <input type="text" readonly size="3" id="txtPhyAssCode_ID" name="txtPhyAssCode_ID"
                                   value="<%=(aCase.getPhy_Assessment_Code() == null ? "" : aCase.getPhy_Assessment_Code())%>">
                        </td>

                        <td class="fLabelR">
                            <label for="txtDateOfPhyAssessment">Date of Assessment</label>
                        </td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Date of Assessment">
                                        <input type="text" name="txtDateOfPhyAssessment" id="txtDateOfPhyAssessment" size="15" maxlength="11"
                                               clear=74 readonly
                                               value="<%=(aCase.getPhys_Assessment_Date() == null ? "" : sdf.format(aCase.getPhys_Assessment_Date()))%>"
                                               <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 74);\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Date of Assessment">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerDoPA"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtDateOfPhyAssessment", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_triggerDoPA", //*
                                step : 1
                            });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtPhyAssReason">Physician's Reason</label></td>
                        <td colspan="9">
                            <textarea name="txtPhyAssReason" id="txtPhyAssReason" cols="110" wrap="physical" title="Physician's Reason for assessment"
                                      <% if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }%>
                                      ><%if (aCase.getPhys_Assessment_Reason() == null) {
            out.write("");
        } else {
            out.write(aCase.getPhys_Assessment_Reason());
        }
                            %></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtContribFactor">Contributing factors</label></td>
                        <td class="field">
                            <input type="text" name="txtContribFactor" id="txtContribFactor" size="50" maxlength="50" title="Contributing factors"
                                   value="<%=(aCase.getContributing_Factors() == null ? "" : aCase.getContributing_Factors())%>"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %> />
                        </td>
                    </tr>

                    <tr>
                        <td class="fLabel" width="125"><label for="txtCoAssCode">Company Assessment</label></td>
                        <td class="field">
                            <input type="text" id="txtCoAssCode" name="txtCoAssCode" size="22" onkeyup="ajax_showOptions('txtCoAssCode_ID',this,'AssCausality',event)"
                                   value="<%=(aCase.getCo_Assessment() == null ? "" : aCase.getCo_Assessment())%>"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %>>
                            <input type="text" readonly size="3" id="txtCoAssCode_ID" name="txtCoAssCode_ID"
                                   value="<%=(aCase.getCo_Assessment_Code() == null ? "" : aCase.getCo_Assessment_Code())%>">
                        </td>

                        <td class="fLabelR">
                            <label for="txtDateOfCoAssessment">Date of Assessment</label>
                        </td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;" title="Date of Assessment">
                                        <input type="text" name="txtDateOfCoAssessment" id="txtDateOfCoAssessment" size="15" maxlength="11"
                                               clear=76 value="<%=(aCase.getCo_Assessment_Date() == null ? "" : sdf.format(aCase.getCo_Assessment_Date()))%>" readonly
                                               <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 76);\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerDoCA"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtDateOfCoAssessment", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_triggerDoCA", //*
                                step : 1
                            });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <tr>
                            <td class="fLabel" width="125"><label for="txtCoAssReason">Company's Reason</label></td>
                            <td colspan="9">
                                <textarea name="txtCoAssReason" id="txtCoAssReason" cols="110" wrap="physical" title="Company's Reason for assessment"
                                          <% if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }%>
                                          ><%if (aCase.getCo_Assessment_Reason() == null) {
            out.write("");
        } else {
            out.write(aCase.getCo_Assessment_Reason());
        }
                                %></textarea>
                            </td>
                        </tr>
                    </tr>

                    <!-- Tab 3.2 (9): Distribution -->
                    <tr height="2px"></tr>
                    <tr>
                        <td class="form_group_title" colspan="10">Distribution</td>
                    </tr>
                    <tr height="2px"></tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtDistribnCpny">Distributed to</label></td>
                        <td class="field">
                            <input type="text" id="txtDistribnCpny" name="txtDistribnCpny" size="50" onkeyup="ajax_showOptions('txtDistribnCpny_ID',this,'SubmCo',event)"
                                   value="<%=(aCase.getDcompany() == null ? "" : aCase.getDcompany())%>"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %>>
                            <input type="hidden" readonly size="3" id="txtDistribnCpny_ID" name="txtDistribnCpny_ID"
                                   value="<%=(aCase.getDcompany() == null ? "" : aCase.getDcompany())%>">
                        </td>

                        <td class="fLabelR">
                            <label for="txtDistribnDate">On</label>
                        </td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <input type="text" name="txtDistribnDate" id="txtDistribnDate" size="15" maxlength="11" title="Distribution Date"
                                               clear=78 value="<%=(aCase.getDistbn_Date() == null ? "" : sdf.format(aCase.getDistbn_Date()))%>" readonly
                                               <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 78);\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerDD"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtDistribnDate", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_triggerDD", //*
                                step : 1
                            });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtLHACode">LHA</label></td>
                        <td class="field">
                            <input type="text" id="txtLHACode" name="txtLHACode" size="30" onkeyup="ajax_showOptions('txtLHACode_ID',this,'LHA',event)"
                                   value="<%=(aCase.getLha_Desc() == null ? "" : aCase.getLha_Desc())%>" title="LHA"
                                   <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   %>>
                            <input type="text" readonly size="12" id="txtLHACode_ID" name="txtLHACode_ID"
                                   value="<%=(aCase.getSublha() == null ? "" : aCase.getSublha())%>">
                        </td>

                        <td class="fLabelR">
                            <label for="txtSubmissionDate">Submission On</label>
                        </td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;" >
                                        <input type="text" name="txtSubmissionDate" id="txtSubmissionDate" size="15" maxlength="11"
                                               clear=79 value="<%=(aCase.getSubmission_Date() == null ? "" : sdf.format(aCase.getSubmission_Date()))%>" readonly
                                               title="Submission date" <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 79);\" ");
        }%> />
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <%
        if (!saveFlag.equals("V")) {
                                        %>
                                        <a href="#" id="cal_triggerSubmD"><img src="images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a>
                                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtSubmissionDate", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_triggerSubmD", //*
                                step : 1
                            });
                                        </script>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtFurtherCommunication">Further Communication</label></td>
                        <td title="Verbatim" colspan="9">
                            <textarea name="txtFurtherCommunication" id="txtFurtherCommunication" cols="110" rows="3" wrap="physical" title="Further Communication"
                                      <% if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }%>
                                      ><%=(aCase.getFurther_Commn() == null ? "" : aCase.getFurther_Commn())%></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_group_title" colspan="10">Autopsy</td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="chkAutopsy">Autopsy</label></td>
                        <td class="field" colspan="1">
                            <input type="checkbox" name="chkAutopsy" id="chkAutopsy" class="chkbox" onclick="setAutopsyControls();"
                                   title="Autopsy" <%
        if (aCase != null) {
            if (aCase.getAutopsy() != null) {
                if (aCase.getAutopsy().equalsIgnoreCase("Y")) {
                    out.write("CHECKED ");
                }
            }
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                   %>
                                   />
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtAutopsyResult">Autopsy Result</label></td>
                        <td colspan="9">
                            <textarea name="txtAutopsyResult" id="txtAutopsyResult" cols="110" wrap="physical" title="Autopsy Result"
                                      <% if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }%>
                                      ><%if (aCase.getAutopsy_Result() == null) {
            out.write("");
        } else {
            out.write(aCase.getAutopsy_Result());
        }
                            %></textarea>
                        </td>
                    </tr>
                </table>
            </div>

            </div>
        </td>
    </tr>
    <tr>
        <td colspan='3'>&nbsp;  </td>
    </tr>
    <tr>
        <td colspan='3'>&nbsp;  </td>
    </tr>
    <tr>
        <td class="fLabel" width="125"></td>
        <td class="field" align="right" colspan="4">
            <form id="FrmCaseDetails" name="FrmCaseDetails" method="POST" action="pgSavCase.jsp?Parent=1&SaveFlag=<%=saveFlag%>"
                  onsubmit="if(confirmSave()) { return validate(); } return false;">

                <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>"
                       value="Save" style="width: 60px;" <% if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }%> onclick="if(confirmSave()) { return validate(); } return false;" title="Save"/>
                <input type="reset" align="right" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                       onclick="javascript:location.replace('<%=strParent.equals("0") ? "pgCaseSummary.jsp?RepNum=" + strRepNum : "pgCaselist.jsp"%>');" title="Cancel"/>
                <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>" />
                <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>" />
                <input type="hidden" id="hfPhyAssFlag" name="hfPhyAssFlag" value=""/>
                <input type="hidden" id="hfCoAssFlag" name="hfCoAssFlag" value=""/>
            </form>
        </td>
    </tr>

    <script type="text/javascript"><!-- Outcome & Assessment / Distribution -->
initTabs('casedetailstab', Array(
'Data Reception / Source',
'Product Info / Patient',
'O&amp;A / Distribution'
), 0, 750, 480, Array(false,false,false));
    </script>
    </td>
</table>
</td>
</form>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>