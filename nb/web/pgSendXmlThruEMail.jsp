<%--
    Document   : pgSendXmlViaEMail
    Created on : Jan 30, 2009, 11:55:53 AM
    Author     : anoop
--%>

<%@page
    contentType="text/html"
    pageEncoding="windows-1252"
    import="bl.sapher.general.GenConf"
    import="bl.sapher.general.Logger"
    %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Send XML as e-mail</title>
    </head>
    <body>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgSendXmlThruEMail.jsp");
        String strXmlFile = request.getParameter("XmlFile");
        String str = application.getRealPath("/XML");
        GenConf gc = new GenConf((String) session.getAttribute("Company"));

        try {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Preparing and sending the e-mail.");
            com.f3.edx.ich_icsr_v2_1.comm.email.EMailSender ems = new com.f3.edx.ich_icsr_v2_1.comm.email.EMailSender(
                    gc.getEmailHost(),
                    gc.getEmailFrom(),
                    gc.getEmailTo(),
                    gc.getSmtpPort(),
                    gc.getDebugMode(),
                    str + "/" + strXmlFile,
                    "The SGM file for submission",
                    "The SGM file generated by SaPhER is included here with.");
            if (ems.send()) {
                pageContext.forward("pgSendXmlStatus.jsp");
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSendXmlThruEMail.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Mail sent successfully");
            }
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSendXmlThruEMail.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception");
        }
        %>
    </body>
</html>