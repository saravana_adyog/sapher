<!--
Form Id : CSD1
Date    : 19-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="java.text.SimpleDateFormat"
import="bl.sapher.CaseDetails"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Case Listing - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "pgCaselist";</script>
    <script src="libjs/gui.js"></script>
    <script src="libjs/ajax/ajax.js"></script>
    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>
    <!--Calendar Library Includes.. S => -->
    <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
    <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
    <!-- import the calendar script -->
    <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
    <!-- import the language module -->
    <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
    <!-- helper script that uses the calendar -->
    <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
    <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
    <!--Calendar Library Includes.. E <= -->

    <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
    <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

</head>

<body>

<%!    private final String FORM_ID = "CSD1";
    private int intRepNum = -1;
    private String strRepNum = "";
    private String strPTCode = "";
    private String strPTDesc = "";
    private String strTrialNum = "";
    private String strProdCode = "";
    private String strGenericNm = "";
    private String strBrandNm = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private int nCaseSuspended = 0;
    private boolean bAdd;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";

    public void jspDestroy() {
    }
%>

<%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaselist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
    bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp: A/V/D=" + bAdd + "/" + bView + "/" + bDelete);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDetails.jsp?Parent=0&SaveFlag=I");
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtRepNum") == null) {
            if (session.getAttribute("txtRepNum") == null) {
                strRepNum = "";
            } else {
                strRepNum = (String) session.getAttribute("txtRepNum");
            }
        } else {
            strRepNum = request.getParameter("txtRepNum");
        }
        session.setAttribute("txtRepNum", strRepNum);

        if (strRepNum.equals("")) {
            intRepNum = -1;
        } else {
            intRepNum = Integer.parseInt(strRepNum);
        }

        if (request.getParameter("txtPTCode_ID") == null) {
            if (session.getAttribute("txtPTCode_ID") == null) {
                strPTCode = "";
            } else {
                strPTCode = (String) session.getAttribute("txtPTCode_ID");
            }
        } else {
            strPTCode = request.getParameter("txtPTCode_ID");
        }
        session.setAttribute("txtPTCode_ID", strPTCode);

        if (request.getParameter("txtPTDesc") == null) {
            if (session.getAttribute("txtPTDesc") == null) {
                strPTDesc = "";
            } else {
                strPTDesc = (String) session.getAttribute("txtPTDesc");
            }
        } else {
            strPTDesc = request.getParameter("txtPTDesc");
        }
        session.setAttribute("txtPTDesc", strPTDesc);

        if (request.getParameter("txtCaseTrial") == null) {
            if (session.getAttribute("txtCaseTrial") == null) {
                strTrialNum = "";
            } else {
                strTrialNum = (String) session.getAttribute("txtCaseTrial");
            }
        } else {
            strTrialNum = request.getParameter("txtCaseTrial");
        }
        session.setAttribute("txtCaseTrial", strTrialNum);

        if (request.getParameter("txtProdCode_ID") == null) {
            if (session.getAttribute("txtProdCode_ID") == null) {
                strProdCode = "";
            } else {
                strProdCode = (String) session.getAttribute("txtProdCode_ID");
            }
        } else {
            strProdCode = request.getParameter("txtProdCode_ID");
        }
        session.setAttribute("txtProdCode_ID", strProdCode);

        if (request.getParameter("txtGenericNm") == null) {
            if (session.getAttribute("txtGenericNm") == null) {
                strGenericNm = "";
            } else {
                strGenericNm = (String) session.getAttribute("txtGenericNm");
            }
        } else {
            strGenericNm = request.getParameter("txtGenericNm");
        }
        session.setAttribute("txtGenericNm", strGenericNm);

        if (request.getParameter("txtBrandNm") == null) {
            if (session.getAttribute("txtBrandNm") == null) {
                strBrandNm = "";
            } else {
                strBrandNm = (String) session.getAttribute("txtBrandNm");
            }
        } else {
            strBrandNm = request.getParameter("txtBrandNm");
        }
        session.setAttribute("txtBrandNm", strBrandNm);

        if (request.getParameter("txtCaseDtFrom") == null) {
            if (session.getAttribute("txtCaseDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtCaseDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtCaseDtFrom");
        }
        session.setAttribute("txtCaseDtFrom", strDtFrom);

        if (request.getParameter("txtCaseDtTo") == null) {
            if (session.getAttribute("txtCaseDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtCaseDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtCaseDtTo");
        }
        session.setAttribute("txtCaseDtTo", strDtTo);

        String strSuspended = "";
        if (request.getParameter("cmbSuspendedCase") == null) {
            if (session.getAttribute("cmbSuspendedCase") == null) {
                strSuspended = "All";
            } else {
                strSuspended = (String) session.getAttribute("cmbSuspendedCase");
            }
        } else {
            strSuspended = request.getParameter("cmbSuspendedCase");
        }
        session.setAttribute("cmbSuspendedCase", strSuspended);

        if (strSuspended.equals("Active")) {
            nCaseSuspended = 0;
        } else if (strSuspended.equals("Suspended")) {
            nCaseSuspended = 1;
        } else if (strSuspended.equals("All")) {
            nCaseSuspended = -1;
        }

        if (resetSession) {
            intRepNum = -1;
            strRepNum = "";
            strPTCode = "";
            strTrialNum = "";
            strProdCode = "";
            strGenericNm = "";
            strBrandNm = "";
            strDtFrom = "";
            strDtTo = "";
            nCaseSuspended = -1;
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = CaseDetails.listCaseFromView((String) session.getAttribute("Company"), -1, "", "", "", "", "", -1);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + intRepNum + "," + strPTCode + "," + strTrialNum + "," + strProdCode +
                    "," + strDtFrom + "," + strDtTo + "," + nCaseSuspended);
            al = CaseDetails.listCaseFromView((String) session.getAttribute("Company"), intRepNum, strPTCode, strTrialNum, strProdCode,
                    strDtFrom, strDtTo, nCaseSuspended);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">Case Listing</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <form name="FrmCaseList" METHOD="POST" ACTION="pgCaselist.jsp" >
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Criteria</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtRepNum">Rep Num</label></td>
            <td class="field">
                <input type="text" name="txtRepNum" id="txtRepNum" size="10" maxlength="9" title="Report Number"
                       value="<%=strRepNum%>" />
            </td>
            <td width="500" align="right"><b><%=strMessageInfo%></b></td>
        </tr>
        <tr>
            <td class="fLabel" width="150"><label for="txtPTCode">Preferred Term</label></td>
            <td class="field" width="300" colspan="6">

                <input type="text" id="txtPTCode" name="txtPTCode" size="30"
                       onKeyUp="ajax_showOptions('txtPTCode_ID',this,'AEPT',event)" />
                <input type="text" readonly size="10" id="txtPTCode_ID" name="txtPTCode_ID">
            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtCaseTrial">Source/Trail</label></td>
            <td class="field" width="300">
                <input type="text" id="txtCaseTrial" name="txtCaseTrial" size="36"
                       onKeyUp="ajax_showOptions('txtCaseTrial_ID',this,'TrialNo',event)" />
                <input type="hidden" id="txtCaseTrial_ID" name="txtCaseTrial_ID" />

            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtProdCode">Product</label></td>
            <td class="field" width="500" colspan="9">
                <input type="text" id="txtProdCode" name="txtProdCode" size="57"
                       onKeyUp="ajax_showOptions('txtProdCode_ID',this,'Product',event)"/>
                <input type="text" readonly size="13" id="txtProdCode_ID" name="txtProdCode_ID">
            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtCaseDtFrom">Date Range</label></td>
            <td class="field">
                <table border='0' cellspacing='0' cellpadding='0'>
                    <tr>
                        <td style="padding-right: 3px; padding-left: 0px;">
                            <input type="text" name="txtCaseDtFrom" id="txtCaseDtFrom" size="12" maxlength="12" clear=3 title="From Date"
                                   value="<%=strDtFrom%>" readonly onKeyDown="return ClearInput(event,3);">
                        </td>
                        <td style="padding-right: 3px; padding-left: 0px;">
                        <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select From Date"></a></td>
                        <td class="fLabel" style="padding-right: 3px; padding-left: 3px;">TO</td>
                        <td style="padding-right: 3px; padding-left: 0px;">
                            <input type="text" name="txtCaseDtTo" id="txtCaseDtTo" size="13" maxlength="12" clear=4 title="To Date"
                                   value="<%=strDtTo%>" readonly  onkeydown="return ClearInput(event,4);">
                        </td>
                        <td style="padding-right: 3px; padding-left: 0px;">
                        <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select To Date"></a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <script type="text/javascript">
            Calendar.setup({
                inputField : "txtCaseDtFrom", //*
                ifFormat : "%d-%b-%Y",
                showsTime : true,
                button : "cal_trigger1", //*
                step : 1
            });
        </script>
        <script type="text/javascript">
            Calendar.setup({
                inputField : "txtCaseDtTo", //*
                ifFormat : "%d-%b-%Y",
                showsTime : true,
                button : "cal_trigger2", //*
                step : 1
            });
        </script>
        <tr>
            <td class="fLabel" width="125"><label for="cmbSuspendedCase">Status</label></td>
            <td class="field" colspan="2">
                <span style="float: left">
                    <select id="cmbSuspendedCase" NAME="cmbSuspendedCase" class="comboclass" style="width: 100px;"
                            title="Select Status">
                        <option <%=(strSuspended.equals("All") ? "SELECTED" : "")%>>All</option>
                        <option <%=(strSuspended.equals("Active") ? "SELECTED" : "")%>>Active</option>
                        <option <%=(strSuspended.equals("Suspended") ? "SELECTED" : "")%>>Suspended</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr>
        <td>
            <td colspan="2">
                <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                       onclick="location.replace('pgNavigate.jsp?Target=pgCaselist.jsp');"/>
            </td>
        </td>
    </form>
    <td>
        <form METHOD="POST" ACTION="pgCaseDetails.jsp?SaveFlag=I">
            <span style="float: right">
                <input type="hidden" name="SaveFlag" value="I">
                <input type="hidden" name="Parent" value="1">
                <input type="submit" name="cmdNew"  title="Adds a new entry"  class="<%=(bAdd ? "button" : "button disabled")%>"
                       value="New Case" style="width: 85px;" <%=(bAdd ? "" : " DISABLED")%>>
            </span>
        </form>
    </td>
    </td>
</tr>
</table>
</td>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {
%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <% if (bDelete) {%>
                <td width="16"></td>
                <% }%>
                <td width="200">Rep Num</td>
                <td width="100">Source/Trail Num</td>
                <td width="100">Rec. Date</td>
                <td width="50">Status</td>
                <td></td>
            </tr>
            <%
     CaseDetails cd = null;
     SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
     String strStatusFlag = "";

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         cd = ((CaseDetails) al.get(i));
         strStatusFlag = "" + cd.getIs_Suspended();
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <td width="16" align="center" >
                    <a href="pgCaseSummary.jsp?RepNum=<%=cd.getRep_Num()%>">
                        <img src="images/icons/view.gif" title="View details of the entry with ID = <%=cd.getRep_Num()%>" border='0' alt="View">
                    </a>
                </td>
                <% if (bDelete) {%>
                <td width="16" align="center" >
                    <a href="pgDelCase.jsp?RepNum=<%=cd.getRep_Num()%>"
                       onclick="return confirm('Are you sure you want to delete?','Sapher')">
                        <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=cd.getRep_Num()%>" border='0' alt="Delete">
                    </a>
                </td>
                <% }%>
                <td width="200" align='left'>
                    <%
                if (strStatusFlag.equals("1")) {
                    %>
                    <font color="red"><%=cd.getRep_Num()%></font>
                    <%
                } else {
                    out.write("" + cd.getRep_Num());
                }
                    %>
                </td>
                <td width="100" align='left'>
                    <%
                if (cd.getTrialnum() != null) {
                    if (strStatusFlag.equals("1")) {
                    %>
                    <font color="red"><%=cd.getTrialnum()%></font>
                    <%
                    } else {
                        out.write("" + cd.getTrialnum());
                    }
                }
                    %>
                </td>
                <td width="100" align='left'>
                    <%
                if (cd.getDate_Recv() != null) {
                    if (strStatusFlag.equals("1")) {
                    %>
                    <font color="red"><%=sdf.format(cd.getDate_Recv())%></font>
                    <%
                    } else {
                        out.write(sdf.format(cd.getDate_Recv()));
                    }
                }
                    %>
                </td>
                <td width="50">
                    <%=(strStatusFlag.equals("1") ? "<font color=\"RED\">Suspended</font>" : "Active")%>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgCaselist.jsp?CurPage=1">
                    <img src="images/icons/page-first.gif" title="Go to first page" border="0" alt="First"></a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <A href="pgCaselist.jsp?CurPage=<%=(nCurPage - 1)%>">
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0" alt="Prev"></A>
                    <% } else {%>
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0" alt="Prev">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <A href="pgCaselist.jsp?CurPage=<%=nCurPage + 1%>">
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0" alt="Next"></A>
                    <% } else {%>
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0" alt="Next">
                    <% }%>
                </td>
                <td>
                    <a href="pgCaselist.jsp?CurPage=<%=nPageCount%>">
                    <img src="images/icons/page-last.gif" title="Go to last page" border="0" alt="Last"></a>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>