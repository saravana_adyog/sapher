<!--
Form Id : -
Date    : 23-04-2008.
Author  : Anoop Varma
-->

<%@page
contentType="text/html"
pageEncoding="UTF-8"
    %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sapher</title>
    </head>
    <body>
        <%!    private String strTarget = "[NO PAGE]";
        %>
        <%
        if (request.getParameter("Target") != null) {
            strTarget = request.getParameter("Target");
        }

        session.removeAttribute("txtInvId");
        session.removeAttribute("txtInvDesc");
        session.removeAttribute("cmbInvType");

        session.removeAttribute("txtCountryCode");
        session.removeAttribute("txtCountryNm");
        session.removeAttribute("cmbValidCountry");

        session.removeAttribute("txtOutcomeCode");
        session.removeAttribute("txtOutcome");
        session.removeAttribute("cmbValidOutcome");

        session.removeAttribute("txtAELltCode");
        session.removeAttribute("txtAELltDesc");
        session.removeAttribute("cmbValidLlt");

        session.removeAttribute("txtAEDiagCode");
        session.removeAttribute("txtAEDiagDesc");
        session.removeAttribute("cmbValidDiag");

        session.removeAttribute("txtSrsnsCode");
        session.removeAttribute("txtSrsnsDesc");
        session.removeAttribute("cmbValidSrsns");

        session.removeAttribute("txtDFreqCode");
        session.removeAttribute("txtDFreqDesc");
        session.removeAttribute("cmbValidDFreq");

        session.removeAttribute("txtDRgmnCode");
        session.removeAttribute("txtDRgmnDesc");
        session.removeAttribute("cmbValidDRgmn");

        session.removeAttribute("txtUnitCode");
        session.removeAttribute("txtUnitDesc");
        session.removeAttribute("cmbValidUnit");

        session.removeAttribute("txtEOriginCode");
        session.removeAttribute("txtEOriginDesc");
        session.removeAttribute("cmbValidEOrigin");

        session.removeAttribute("txtSvtyCode");
        session.removeAttribute("txtSvtyDesc");
        //session.removeAttribute("cmbValidESvty"); //?
        session.removeAttribute("cmbValidSvty");

        session.removeAttribute("txtFormCode");
        session.removeAttribute("txtFormDesc");
        session.removeAttribute("cmbValidForm");

        //session.removeAttribute("txtRoleName"); //?
        session.removeAttribute("txtRole");
        session.removeAttribute("cmbLockRole");

        session.removeAttribute("txtSexCode");
        session.removeAttribute("txtSexDesc");
        session.removeAttribute("cmbValidSex");

        session.removeAttribute("txtIndicationNum");
        session.removeAttribute("txtIndication");
        session.removeAttribute("cmbValidInd");

        session.removeAttribute("txtManuCode");
        session.removeAttribute("txtManuDesc");
        session.removeAttribute("cmbValidManu");

        session.removeAttribute("txtResultCode");
        session.removeAttribute("txtResultDesc");
        session.removeAttribute("cmbValidRechResult");

        session.removeAttribute("txtPermRoleName");
        session.removeAttribute("txtPermInvId");
        session.removeAttribute("txtPermInvName");
        session.removeAttribute("cmbPermInvType");

        session.removeAttribute("txtLHACode");
        session.removeAttribute("txtCountryCode");//txtLHACountryCode
        session.removeAttribute("txtCountryCode_ID");
        //session.removeAttribute("txtCountryNm");txtLHACountryNm
        session.removeAttribute("txtLHANm");
        session.removeAttribute("cmbValidLHA");

        session.removeAttribute("txtSCoName");
        session.removeAttribute("txtSCoCountryId");
        session.removeAttribute("txtSCoCountryId_ID");
        //session.removeAttribute("txtSCoCountryNm");
        session.removeAttribute("cmbValidSCo");

        session.removeAttribute("txtResultCode");
        session.removeAttribute("txtResultDesc");
        session.removeAttribute("cmbValidResult");

        session.removeAttribute("txtTypeCode");
        session.removeAttribute("txtTypeDesc");
        session.removeAttribute("cmbValidRType");

        session.removeAttribute("txtSourceCode");
        session.removeAttribute("txtSourceDesc");
        session.removeAttribute("cmbValidSource");

        session.removeAttribute("txtStatusCode");
        session.removeAttribute("txtStatusDesc");
        session.removeAttribute("cmbValidRepStatus");

        session.removeAttribute("txtTrialNum");
        session.removeAttribute("cmbValidTrial");

        session.removeAttribute("txtBodySysNum");
        session.removeAttribute("txtBodySystem");
        session.removeAttribute("cmbValidBodySys");

        session.removeAttribute("txtRouteCode");
        session.removeAttribute("txtRouteDesc");
        session.removeAttribute("cmbValidRoute");

        session.removeAttribute("txtUsername");
        session.removeAttribute("txtUserRoleName");
        session.removeAttribute("cmbLockedUser");

        session.removeAttribute("txtAssCode");
        session.removeAttribute("txtAssDesc");
        session.removeAttribute("cmbAssFlag");
        session.removeAttribute("cmbValidAss");

        session.removeAttribute("txtProdCode_ID");
        session.removeAttribute("txtGenericNm");
        session.removeAttribute("txtBrandNm");
        session.removeAttribute("cmbValidPord");

        session.removeAttribute("txtAudUsername");
        session.removeAttribute("txtAudInv");
        session.removeAttribute("txtAudInvDesc");
        session.removeAttribute("cmbTxnType");
        session.removeAttribute("cmbTgtTyptxtAudInve");
        session.removeAttribute("txtDtFrom");
        session.removeAttribute("txtDtTo");
        session.removeAttribute("txtRefVal");

        session.removeAttribute("txtRepNum");
        //AV session.removeAttribute("txtCaseRTypeCode");
        //AV session.removeAttribute("txtCaseRType");
        session.removeAttribute("txtPTCode_ID");
        session.removeAttribute("txtPTCode");
        session.removeAttribute("txtCaseTrial");
        session.removeAttribute("txtCaseTrial_ID");
        session.removeAttribute("txtProdCode");
        session.removeAttribute("txtProdCode_ID");
        session.removeAttribute("txtCaseDtFrom");
        session.removeAttribute("txtCaseDtTo");
        session.removeAttribute("cmbSuspendedCase");

        // Clear Code list print related data from session.
        session.removeAttribute("AeLltPrintData");
        session.removeAttribute("SubmCoPrintData");
        session.removeAttribute("SexPrintData");
        session.removeAttribute("SeriousPrintData");
        session.removeAttribute("RoutePrintData");
        session.removeAttribute("ReprStatPrintData");
        session.removeAttribute("RepSrcPrintData");
        session.removeAttribute("RepTypePrintData");
        session.removeAttribute("RechPrintData");
        session.removeAttribute("ProdPrintData");
        session.removeAttribute("ManufPrintData");
        session.removeAttribute("LhaPrintData");
        session.removeAttribute("IndTreatPrintData");
        session.removeAttribute("FormlnPrintData");
        session.removeAttribute("EvtSvPrintData");
        session.removeAttribute("EthOrgPrintData");
        session.removeAttribute("DoseRegPrintData");
        session.removeAttribute("UnitPrintData");
        session.removeAttribute("DoseFrPrintData");
        session.removeAttribute("CountryPrintData");
        session.removeAttribute("BodySysPrintData");
        session.removeAttribute("AuditPrintData");
        session.removeAttribute("PrintData");
        session.removeAttribute("AeDiagPrintData");
        session.removeAttribute("AeoPrintData");
        session.removeAttribute("TrialNumPrintData");

        session.removeAttribute("txtCaseDRRTypeCode");
        session.removeAttribute("txtEntryDate");
        session.removeAttribute("txtTreatmentStartDate");
        session.removeAttribute("txtDose");
        session.removeAttribute("txtTreatmentEndDate");
        session.removeAttribute("txtWeight");
        session.removeAttribute("txtHeight");

        //if(session.getAttribute("txtInvId"))
        session.removeAttribute("txtInvId");

        // for Doc list page
        session.removeAttribute("txtDocName");

        // for case narrative page (pgAeSummary.jsp)
        session.removeAttribute("txtReactionDesc");
        session.removeAttribute("txtExtendedInfo");

        //
        session.removeAttribute("txtRelevantHistory");
        session.removeAttribute("txtShortComment");

        // Clear MedDRA Browser Search session variables
        session.removeAttribute("txtSrchKey");
        session.removeAttribute("chkSoc");
        session.removeAttribute("chkHlgt");
        session.removeAttribute("chkHlt");
        session.removeAttribute("chkPt");
        session.removeAttribute("chkLlt");

        try {
            System.gc();
        } catch (Throwable t) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                t.printStackTrace();
            }
        }

        if (request.getParameter("Cancellation") == null) {
            if (session.getAttribute("DirtyFlag") != null) {
                if (strTarget.substring(strTarget.length() - 3, strTarget.length()).equalsIgnoreCase("jsp")) {
                    strTarget += "?Cancellation=1";
                } else {
                    strTarget += "&Cancellation=1";
                }
                out.write("<script type=\"text/javascript\">");
                out.write("if(!confirm('Unsaved Data. Do you want to proceed?','Sapher')) { history.go(-1); } else document.location.replace('" + strTarget + "');");
                out.write("</script>");
                return;
            } else {
                pageContext.forward(strTarget);
            }
            session.removeAttribute("DirtyFlag");
        } else {
            // After cleaning up the sessions, forward to the target page.
            pageContext.forward(strTarget);
        }
        %>
    </body>
</html>
