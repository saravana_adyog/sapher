<%-- 
    Document   : pgSavDoc
    Created on : Jun 25, 2009, 3:50:11 PM
    Author     : Anoop Varma
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"
        import="java.util.List"
        import="java.util.Iterator"
        import="java.io.File"
        import="org.apache.commons.fileupload.servlet.ServletFileUpload"
        import="org.apache.commons.fileupload.FileItem"
        import="org.apache.commons.fileupload.disk.DiskFileItemFactory"
        import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
        import="bl.sapher.general.Logger"
        import="bl.sapher.User"
        import="bl.sapher.Documents"
        import="bl.sapher.general.ClientConf"
        %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title></title>
    </head>
    <body>
        <%
        String FILE_REPOSITORY_PATH = "";// application.getRealPath("/Reports/Docs");//"e:\\Sapher_docs\\";
        final int FILE_SIZE_THRESHOLD = 40960;
        final long FILE_SIZE_MAX = 10485760; // 10 MB
        User currentUser;
        String strFileName = "";
        String strRepNum = "";
        String strDocDesc = "";
        %>

        <%
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavDocs.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavDocs.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavDocs.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavDocs.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        if (request.getParameter("txtDocDesc") != null) {
            strDocDesc = request.getParameter("txtDocDesc");
        } else {
            strDocDesc = "eMpTy sTrInG";
        }

        FILE_REPOSITORY_PATH = ClientConf.getByClientName((String) session.getAttribute("Company")).getDocUploadPath();

        try {
            DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
            diskFileItemFactory.setSizeThreshold(FILE_SIZE_THRESHOLD); /* the unit is bytes */

            File repositoryPath = new File(FILE_REPOSITORY_PATH);
            diskFileItemFactory.setRepository(repositoryPath);

            ServletFileUpload servletFileUpload = new ServletFileUpload(diskFileItemFactory);
            servletFileUpload.setSizeMax(FILE_SIZE_MAX); /* the unit is bytes */
            List fileItemsList = servletFileUpload.parseRequest(request);

            Iterator it = fileItemsList.iterator();

            while (it.hasNext()) {
                FileItem fileItem = (FileItem) it.next();
                if (!fileItem.isFormField()) {/* The file item contains an uploaded file */
                    strFileName = fileItem.getName();
                    File uploadedFile = new File(FILE_REPOSITORY_PATH, strFileName);
                    fileItem.write(uploadedFile);
                }
            }

            Documents.uploadDoc2Db((String) session.getAttribute("Company"),
                    (String) session.getAttribute("CurrentUser"),
                    strRepNum, strFileName, strDocDesc, 1);
            pageContext.forward("pgDocslist.jsp?SavStatus=0");
        } catch (Throwable error) {
            //pageContext.forward("pgDocslist.jsp?SavStatus=4");
            error.printStackTrace();
            throw error;
        }
        %>
    </body>
</html>
