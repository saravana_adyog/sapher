<!--
Form Id : USM4
Date    : 11-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.User"
import="bl.sapher.Role"
import="bl.sapher.Inventory"
import="bl.sapher.Permission"
import="bl.sapher.general.RecordNotFoundException" import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Permission - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script>
            function viewClick(){
                if (!document.FrmPerm.chkView.checked){
                    if (document.FrmPerm.chkEdit.checked)
                        return false;
                    if (document.FrmPerm.chkDelete.checked)
                        return false;
                }
                return true;
            }
            function changeView(){
                document.FrmPerm.chkView.checked = true;
            }
            function noChangeEdit(){
                document.FrmPerm.chkEdit.checked = true;
                return true;
            }
        </script>

    </head>
    <body>
        <%!    private final String FORM_ID = "USM4";
    private String strRoleName = "";
    private String strInvId = "";
    private boolean bEdit;
    private User currentUser = null;
    private Inventory inv = null;
    private Permission perm = null;
        %>
        <%
        perm = new Permission((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPermission.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (request.getParameter("InvId") != null) {
            strInvId = request.getParameter("InvId");
        }

        if (request.getParameter("Role") != null) {
            strRoleName = request.getParameter("Role");
        }

        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp: bEdit=" + bEdit);
            if (!bEdit) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; No permission." + "[Edit: " + bEdit + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }

            perm = new Permission((String) session.getAttribute("Company"), strInvId, strRoleName);

        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">User Administration</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmPerm" name="FrmPerm" method="POST" action="pgUpdPermission.jsp">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Edit Permissions</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtRoleName">Role Name</label></td>
                                <td class="field"><input type="text" name="txtRoleName" title="Role Name"
                                                             id="txtRoleName" size="40" maxlength="30"
                                                             value="<%=strRoleName%>" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtInvId">Inventory</label></td>
                                <td class="field"><input type="text" name="txtInvId" id="txtInvId" size="7" title="Inventory"
                                                         maxlength="10" value="<%=perm.getInv().getInv_Id()%>" readonly>
                                <input type="text" name="txtInvDesc" id="txtInvDesc" size="30"
                                       maxlength="50" value="<%=perm.getInv().getInv_Desc()%>" readonly>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkView">View</label></td>
                                <td class="field"><input type="checkbox" name="chkView" id="chkView" class="chkbox" title="View" onclick="return viewClick();"
                                                             <%
        if (perm.getPView() == 1) {
            out.write("CHECKED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkAdd">Add</label></td>
                                <td class="field"><input type="checkbox" name="chkAdd" id="chkAdd" class="chkbox"
                                                             <%
        if (perm.getPAdd() == 1) {
            out.write("CHECKED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkEdit">Edit</label></td>
                                <td class="field"><input type="checkbox" name="chkEdit" id="chkEdit" class="chkbox" onclick="changeView();
                                                             <%if ((currentUser.getRole().getRole_Name().equals(strRoleName)) && perm.getInv().getInv_Id().equals("USM4")) {
            out.write("return noChangeEdit();");
        }%>"
                                                             <%
        if (perm.getPEdit() == 1) {
            out.write("CHECKED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkDelete">Delete</label></td>
                                <td class="field"><input type="checkbox" name="chkDelete" id="chkDelete" class="chkbox" onclick="return changeView();"
                                                             <%
        if (perm.getPDelete() == 1) {
            out.write("CHECKED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="button" value="Save" style="width: 60px;">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('pgNavigate.jsp?Target=pgPermissionlist.jsp');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>