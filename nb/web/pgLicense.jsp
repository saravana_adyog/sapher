<!--
Form Id : 
Date    : 13-08-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.general.License"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>License - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>
        
        <script>
            function validate(){
                if (trim(document.getElementById('txtLicense').value) == ""){
                    alert( "License Code cannot be empty.");
                    document.getElementById('txtLicense').focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <%!    private final String FORM_ID = "";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
    private License license = null;
        %>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgLicense.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                    return;
                }
            } catch (TimeoutException toe) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("LicenseKey") != null) {
                    license = new License(request.getParameter("LicenseKey"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                license = new License();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLicense.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">License List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmLicense" name="FrmLicense" method="POST" action="pgSavLicense.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>License</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>

                            <tr>
                                <td class="fLabel" width="125"><label for="txtLicense">License</label></td>
                                <td class="field"><input type="text" name="txtLicense" id="txtLicense" size="100" title="License key"
                                                             maxlength="50" onchange="return checkIsAlphaSpaceVar(event);" value=""
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>

                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgLicenselist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>