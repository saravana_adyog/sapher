<!--
Form Id : CSD1
Date    : 
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="java.text.SimpleDateFormat"
import="bl.sapher.Documents"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Case Listing - [Sapher]</title>
        <script>var SAPHER_PARENT_NAME = "pgDocslist";</script>
        <script src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>
    </head>

    <body>
        <%!    private final String FORM_ID = "CSD1";
    private int nRepNum = -1;
    private String strRepNum = "";
    private String strDocName = "";
    private boolean bAdd;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDocslist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";

        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Unknown reason.</font>";
            }
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), ".jsp: A/V/D=" + bAdd + "/" + bView + "/" + bDelete);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDetails.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("RepNum") == null) {
            if (session.getAttribute("txtRepNum") == null) {
                strRepNum = "";
            } else {
                strRepNum = (String) session.getAttribute("txtRepNum");
            }
        } else {
            strRepNum = request.getParameter("RepNum");
        }
        session.setAttribute("txtRepNum", strRepNum);

        if (strRepNum.equals("")) {
            nRepNum = -1;
        } else {
            nRepNum = Integer.parseInt(strRepNum);
        }

        if (request.getParameter("txtDocName") == null) {
            if (session.getAttribute("txtDocName") == null) {
                strDocName = "";
            } else {
                strDocName = (String) session.getAttribute("txtDocName");
            }
        } else {
            strDocName = request.getParameter("txtDocName");
        }
        session.setAttribute("txtDocName", strDocName);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strRepNum + "," + strDocName);
        al = Documents.listDocuments(
                Constants.DOCS_PARTIAL_DOWNLOAD, Constants.SEARCH_STRICT,
                (String) session.getAttribute("Company"),
                application.getRealPath(Documents.DOCS_RELATIVE_PATH),
                strRepNum, Constants.SEARCH_ANY_VAL/*doc id*/,
                strDocName, Constants.SEARCH_ANY_VAL/*doc type*/,
                Constants.SEARCH_ANY_VAL/*doc desc*/,
                Constants.STATUS_ALL);

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Documents Listing</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form name="FrmCaseList" method="POST" action="pgDocslist.jsp" >
                    <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
                        <td class="form_group_title" colspan="10">Search Criteria</td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtRepNum">Rep Num</label></td>
                        <td class="field">
                            <input type="text" name="txtRepNum" id="txtRepNum" size="10" maxlength="9" title="Report Number"
                                   value="<%=strRepNum%>" readonly />
                        </td>
                        <td width="500" align="right"><b><%=strMessageInfo%></b></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="cmbSuspendedCase">Document Name</label></td>
                        <td class="field" colspan="2">
                            <input type="text" name="txtDocName" id="txtDocName" size="25" maxlength="24" title="Document name"
                                   value="<%=strDocName%>" />
                        </td>
                    </tr>
                    <tr>
                    <td>
                        <td colspan="2">
                            <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                            <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                                   onclick="location.replace('pgNavigate.jsp?Target=pgDocslist.jsp?SaveFlag=U&RepNum=<%=strRepNum%>');"/>
                        </td>
                    </td>
                </form>
                <td>
                    <form METHOD="POST" ACTION="pgSelDoc.jsp?RepNum=<%=strRepNum%>">
                        <span style="float: right">
                            <input type="hidden" name="SaveFlag" value="I"/>
                            <input type="hidden" name="Parent" value="1"/>
                            <input type="submit" name="cmdNew"  title="Upload a new document"  class="<%=(bAdd ? "button" : "button disabled")%>"
                                   value="Upload new Document" style="width: 140px;" <%=(bAdd ? "" : " DISABLED")%> />
                        </span>
                    </form>
                </td>

            </tr>
        </table>

        <!-- Form Body Ends -->
        <tr>
            <td style="height: 1px;"></td>
        </tr>
        <!-- Grid View Starts -->
        <tr>
            <td class="formbody">
                <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
                        <td class="form_group_title" colspan="10">Search Results
                            <%=al.size() + " - " + " record(s) found"%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <% if (al.size() != 0) {
        %>
        <tr>
            <td class="formbody">
                <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                    <tr class="grid_header">
                        <td width="16"></td>
                        <% if (bDelete) {%>
                        <td width="16"></td>
                        <% }%>
                        <td width="200">Document name</td>
                        <td width="100">Size</td>
                        <td width="100">Type</td>
                        <td width="50">Status</td>
                        <td></td>
                    </tr>
                    <%
     Documents doc = null;
     SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
     String strStatusFlag = "";

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         doc = ((Documents) al.get(i));
         strStatusFlag = "" /*+ doc.getIs_Suspended()*/;
                    %>
                    <tr class="
                        <%
                        if (i % 2 == 0) {
                            out.write("even_row");
                        } else {
                            out.write("odd_row");
                        }
                        %>
                        ">
                        <td width="16" align="center">
                            <a href="#"
                               onclick="popupWnd('_blank', 'yes','<%=("pgDownloadDoc.jsp?RepNum=" + strRepNum + "&DocId=" + doc.getDocId() + "&FullPath=" + Documents.DOCS_RELATIVE_PATH + "/" + doc.getDocName())%>',800,600)">
                                <img src="<%
                        if (doc.getType().equalsIgnoreCase("doc")) {
                            out.write("images/icons/icon-doc-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("txt")) {
                            out.write("images/icons/icon-txt-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("jpg")) {
                            out.write("images/icons/icon-jpg-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("pdf")) {
                            out.write("images/icons/icon-pdf-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("rtf")) {
                            out.write("images/icons/icon-doc-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("zip")) {
                            out.write("images/icons/icon-zip-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("xls")) {
                            out.write("images/icons/icon-exel-16x16.gif");
                        } else {
                            out.write("images/icons/icon-all-16x16.gif");
                        }
                                     %>"
                                     title="Download/See the attached file: <%=doc.getDocName()%>" border='0' alt="View">
                            </a>
                        </td>
                        <% if (bDelete) {%>
                        <td width="16" align="center" >
                            <a href="pgDelDoc.jsp?RepNum=<%=doc.getRepNum()%>&DocId=<%=doc.getDocId()%>"
                               onclick="return confirm('Are you sure you want to delete?','Sapher')">
                                <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=doc.getRepNum()%>" border='0' alt="Delete">
                            </a>
                        </td>
                        <% }%>
                        <td width="200" align='left'>
                            <%=doc.getDocName()%>
                        </td>
                        <td width="100" align='left'>
                            <%=doc.getDocSize()%>
                        </td>
                        <td width="100" align='left'>
                            <%=(doc.getType() + " File")%>
                        </td>
                        <td width="50">
                        </td>
                        <td></td>
                    </tr>
                    <% }%>
                </table>
            </td>
            <tr>
            </tr>
        </tr>
        <!-- Grid View Ends -->

        <!-- Pagination Starts -->
        <tr>
            <td class="formbody">
                <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                    <tr>
                        <td>
                            <a href="pgDocslist.jsp?CurPage=1">
                            <img src="images/icons/page-first.gif" title="Go to first page" border="0" alt="First"></a>
                        </td>
                        <td><% if (nCurPage > 1) {%>
                            <A href="pgDocslist.jsp?CurPage=<%=(nCurPage - 1)%>">
                            <img src="images/icons/page-prev.gif" title="Go to previous page" border="0" alt="Prev"></A>
                            <% } else {%>
                            <img src="images/icons/page-prev.gif" title="Go to previous page" border="0" alt="Prev">
                            <% }%>
                        </td>
                        <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                        <td><% if (nCurPage < nPageCount) {%>
                            <A href="pgDocslist.jsp?CurPage=<%=nCurPage + 1%>">
                            <img src="images/icons/page-next.gif" title="Go to next page" border="0" alt="Next"></A>
                            <% } else {%>
                            <img src="images/icons/page-next.gif" title="Go to next page" border="0" alt="Next">
                            <% }%>
                        </td>
                        <td>
                            <a href="pgDocslist.jsp?CurPage=<%=nPageCount%>">
                            <img src="images/icons/page-last.gif" title="Go to last page" border="0" alt="Last"></a>
                        </td>
                        <td width="100%"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- Pagination Ends -->
        <%}%>

        <tr>
            <td class="formbody" align="right">
                <input type="reset" id="cmdBack" name="cmdBack" class="button" value="Back"
                       style="width: 60px;" title="Go back to Case Summary"
                       onclick="javascript:location.replace('<%="pgCaseSummary.jsp?RepNum=" + strRepNum%>')"/>
            </td>
        </tr>

        <div style="height: 25px;"></div>
    </body>
</html>