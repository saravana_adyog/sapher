<%@page 
    import="bl.sapher.CaseDetails"
    import="java.util.ArrayList"
    import="java.text.SimpleDateFormat"
    %>
<%    ArrayList<CaseDetails> al = null;
        String strReceiveDate = "";
        String strClinRepInfo = "";
        String strCntryCode = "";
        String strProdCode = "";
        String strInitials = "";
        int nAgeReported = -1;
        String strSexCode = "";
        String strTrialNum = "";
        String strPatientNum = "";
        String strPTCode = "";
        String strAggrPT = "N";
        String strLLTCode = "";
        String strAggrLLT = "N";
        String strBodySys = "";
        String Seriousness = "";
        String strOutcomeCode = "";
        String strPhyAssCode = "";
        String strCoAssCode = "";
        String strComplInfo = "";

        if (request.getParameter("RcvDt") != null) {
            strReceiveDate = request.getParameter("RcvDt");
        }
        if (request.getParameter("CRI") != null) {
            strClinRepInfo = request.getParameter("CRI");
        }
        if (request.getParameter("CtryCd") != null) {
            strCntryCode = request.getParameter("CtryCd");
        }
        if (request.getParameter("ProdCd") != null) {
            strProdCode = request.getParameter("ProdCd");
        }
        if (request.getParameter("PInit") != null) {
            strInitials = request.getParameter("PInit");
        }
        if (request.getParameter("AgeRep") != null) {
            if (request.getParameter("AgeRep") != "") {
                nAgeReported = Integer.parseInt(request.getParameter("AgeRep"));
            }
        }
        if (request.getParameter("SxCd") != null) {
            strSexCode = request.getParameter("SxCd");
        }
        if (request.getParameter("TrialNm") != null) {
            strTrialNum = request.getParameter("TrialNm");
        }
        if (request.getParameter("PatNum") != null) {
            strPatientNum = request.getParameter("PatNum");
        }
        if (request.getParameter("PTCod") != null) {
            strPTCode = request.getParameter("PTCod");
        }
        if (request.getParameter("AggrPT") != null) {
            strAggrPT = "Y";
        } else {
            strAggrPT = "N";
        }
        if (request.getParameter("LLTCd") != null) {
            strLLTCode = request.getParameter("LLTCd");
        }
        if (request.getParameter("AggrLLT") != null) {
            strAggrLLT = "Y";
        } else {
            strAggrLLT = "N";
        }
        if (request.getParameter("BSysNum") != null) {
            strBodySys = request.getParameter("BSysNum");
        }
        if (request.getParameter("ClasfCd") != null) {
            Seriousness = request.getParameter("ClasfCd");
        }
        if (request.getParameter("OutCCod") != null) {
            strOutcomeCode = request.getParameter("OutCCod");
        }
        if (request.getParameter("PhAssCd") != null) {
            strPhyAssCode = request.getParameter("PhAssCd");
        }
        if (request.getParameter("CoAssCd") != null) {
            strCoAssCode = request.getParameter("CoAssCd");
        }
        if (request.getParameter("Nrr") != null) {
            strComplInfo = request.getParameter("Nrr");
        }

        CaseDetails aCase = new CaseDetails();
        al = aCase.listDuplicateCase((String) session.getAttribute("Company"),
                strReceiveDate, strClinRepInfo, strCntryCode,
                strProdCode, strInitials, nAgeReported,
                strSexCode, strTrialNum, strPatientNum,
                strPTCode, strAggrPT, strLLTCode,
                strAggrLLT, strBodySys, Seriousness,
                strOutcomeCode, strPhyAssCode, strCoAssCode,
                strComplInfo);
%>
<table width="100%">
    <tr>
        <td class="formbody">
            <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td class="form_group_title" colspan="10">Search Results
                        <%=" - " + al.size() + " record(s) found"%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="formbody">
            <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                <tr class="grid_header">
                    <td width="16"></td>
                    <td width="200">Rep Num</td>
                    <td width="100">Source/Trail Num</td>
                    <td width="100">Rec. Date</td>
                    <td width="50">Status</td>
                    <td></td>
                </tr>
                <%
        for (int i = 0; i < al.size(); i++) {
            aCase = al.get(i);
                %>
                <tr class="
                    <%
                    if (i % 2 == 0) {
                        out.write("even_row");
                    } else {
                        out.write("odd_row");
                    }
                    %>
                    ">
                    <td width="16" align="center" >
                        <a href="#" onclick="popupWnd('_blank', 'no','pgCaseSummary.jsp?ShowBkBtn=0&RepNum=<%=aCase.getRep_Num()%>',800,600)">
                            <img src="images/icons/view.gif" title="" border='0' alt="View"/>
                        </a>
                    </td>
                    <td width="200" align='left'>
                        <%=aCase.getRep_Num()%>
                    </td>
                    <td width="100" align='left'></td>
                    <td width="100" align='left'></td>
                    <td width="50"></td>
                    <td></td>
                </tr>
                <% }%>
            </table>
        </td>
        <tr></tr>
    </tr>
</table>