<!--
Form Id : CDL24
Date    : 14-12-2007.
Author  : Jaikishan. S, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.Body_System"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Body System - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>
        <script>
            function validate(){
                if (document.FrmForm.txtBodySysNum.value == ""){
                    alert("Body System Number cannot be empty.");
                    document.FrmForm.txtBodySysNum.focus();
                    return false;
                }
                if (trim(document.FrmForm.txtBodySystem.value) == ""){
                    alert("Body System cannot be empty.");
                    document.FrmForm.txtBodySystem.focus();
                    return false;
                }
                //if(!checkIsAlphaVar(document.FrmForm.txtBodySysNum.value)) {
                //    window.alert('Invalid character(s) in body system number');
                //    document.getElementById('txtBodySysNum').focus();
                //    return false;
                //}
                if(!isAlphaNumericSpace(document.FrmForm.txtBodySystem.value)) {
                    window.alert('Invalid character(s) in body system');
                    document.getElementById('txtBodySystem').focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <%!    Body_System bodysystem = null;
    private final String FORM_ID = "CDL24";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        bodysystem = new Body_System((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgBodySystem.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp; Invalid username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                    return;
                }
            } catch (TimeoutException toe) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("BodySysNum") != null) {
                    bodysystem = new Body_System((String) session.getAttribute("Company"), request.getParameter("BodySysNum"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                bodysystem = new Body_System();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgBodySystem.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmForm" name="FrmForm" method="POST" action="pgSavBodySystem.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>Body System</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtBodySysNum">System Number</label></td>
                                <td class="field"><input type="text" name="txtBodySysNum"
                                                             id="txtBodySysNum" size="12" maxlength="12" title="Body System Number"
                                                             onkeydown="return checkIsNumeric(event);"
                                                             value="<%=bodysystem.getBody_Sys_Num()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtBodySystem">Body System</label></td>
                                <td class="field"><input type="text" name="txtBodySystem" id="txtBodySystem" size="30" title="Body System"
                                                             maxlength="50" value="<%=bodysystem.getBody_System()%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %> onkeydown="return checkIsAlphaSpaceVar('txtBodySystem');" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                                             <%
        if (bodysystem.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgBodySystemlist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>