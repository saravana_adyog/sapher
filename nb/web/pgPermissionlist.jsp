<!--
Form Id : USM4
Date    : 13-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.Permission"
import="bl.sapher.Inventory"
import="bl.sapher.User"
import="bl.sapher.Role"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Permissions - [Sapher]</title>
        <script>var SAPHER_PARENT_NAME = "pgPermissionlist";</script>
        <script src="libjs/gui.js"></script>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

    </head>

    <body>
        <%!    private final String FORM_ID = "USM4";
    private String strRoleName = "";
    private String strInvType = "";
    private String strInvId = "";
    private String strInvName = "";
    private boolean bEdit;
    private ArrayList al = null;
    private User currentUser = null;
    private Role role = null;
    private Permission aPermission = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
        %>

        <%
        aPermission = new Permission((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPermissionlist.jsp");
        strMessageInfo = "";
        if (request.getParameter("SavStatus") != null) {
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed!</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp: E=" + bEdit);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (!bEdit) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [Edit=" + bEdit + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }
        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtPermRoleName") == null) {
            if (session.getAttribute("txtPermRoleName") == null) {
                strRoleName = "";
            } else {
                strRoleName = (String) session.getAttribute("txtPermRoleName");
            }
        } else {
            strRoleName = request.getParameter("txtPermRoleName");
        }
        session.setAttribute("txtPermRoleName", strRoleName);

        if (strRoleName.equals("")) {
            role = currentUser.getRole();
            strRoleName = currentUser.getRole().getRole_Name();
        } else {
            role = new Role((String) session.getAttribute("Company"), strRoleName);
        }

        if (request.getParameter("txtPermInvId_ID") == null) {
            if (session.getAttribute("txtPermInvId_ID") == null) {
                strInvId = "";
            } else {
                strInvId = (String) session.getAttribute("txtPermInvId_ID");
            }
        } else {
            strInvId = request.getParameter("txtPermInvId_ID");
        }
        session.setAttribute("txtPermInvId_ID", strInvId);

        if (request.getParameter("txtPermInvName") == null) {
            if (session.getAttribute("txtPermInvName") == null) {
                strInvName = "";
            } else {
                strInvName = (String) session.getAttribute("txtPermInvName");
            }
        } else {
            strInvName = request.getParameter("txtPermInvName");
        }
        session.setAttribute("txtPermInvName", strInvName);

        if (request.getParameter("cmbPermInvType") == null) {
            if (session.getAttribute("cmbPermInvType") == null) {
                strInvType = "";
            } else {
                strInvType = (String) session.getAttribute("cmbPermInvType");
            }
        } else {
            strInvType = request.getParameter("cmbPermInvType");
        }
        strInvType = (strInvType.equals("") ? "All" : strInvType);
        session.setAttribute("cmbPermInvType", strInvType);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strInvId + "," + strRoleName + "," + (strInvType.equals("All") ? "" : (strInvType.equals("Screen") ? "SCR" : "REP")));
        al = Permission.listPermissions((String) session.getAttribute("Company"), strInvId, strRoleName, (strInvType.equals("All") ? "" : (strInvType.equals("Screen") ? "SCR" : "REP")));

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

        %>
        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">User Administration - Role Permissions</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form name="FrmPermList" METHOD="POST" ACTION="pgPermissionlist.jsp" >
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
                        <td class="form_group_title" colspan="10">Search Criteria</td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                    <tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtPermRoleName">Role Name</label></td>
                        <td class="field" width="200">

                            <input type="text" id="txtPermRoleName" name="txtPermRoleName" size="43" onKeyUp="ajax_showOptions('txtPermRoleName_ID',this,'Role',event)">
                            <input type="hidden" id="txtPermRoleName_ID" name="txtPermRoleName_ID" value="">

                        </td>
                        <td width="500" align="right"><b><%=strMessageInfo%></b></td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtPermInvId">Inventory Id</label></td>
                        <td class="field" width="300">

                            <input type="text" id="txtPermInvId" name="txtPermInvId" size="30" onKeyUp="ajax_showOptions('txtPermInvId_ID',this,'Inventory',event)">
                            <input type="text" readonly size="10" id="txtPermInvId_ID" name="txtPermInvId_ID" value="">

                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="cmbPermInvType">Inventory Type</label></td>
                        <td class="field" colspan="2">
                            <span style="float: left">
                                <select id="cmbPermInvType" NAME="cmbPermInvType" class="comboclass" style="width: 70px;" title="Inventory Type">
                                    <option <%=(strInvType.equals("All") ? "SELECTED" : "")%>>All</option>
                                    <option <%=(strInvType.equals("Report") ? "SELECTED" : "")%>>Report</option>
                                    <option <%=(strInvType.equals("Screen") ? "SELECTED" : "")%>>Screen</option>
                                </select>

                            </span>
                        </td>
                    </tr>
                    <tr>
                    <td>
                        <td colspan="2">
                            <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                            <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                                   onclick="location.replace('pgNavigate.jsp?Target=pgPermissionlist.jsp');"/>
                        </td>
                    </td>
                </td>
            </tr>
        </table>
        </td>
        </form>
        </tr>
        <!-- Form Body Ends -->
        <tr>
            <td style="height: 1px;"></td>
        </tr>
        <!-- Grid View Starts -->
        <tr>
            <td class="formbody">
                <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
                        <td class="form_group_title" colspan="10">Search Results
                            <%=" - " + al.size() + " record(s) found"%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <% if (al.size() != 0) {%>
        <tr>
            <td class="formbody">
                <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                    <tr class="grid_header">
                        <td width="16"></td>
                        <td width="100">Inventory Id</td>
                        <td>Inventory Desc</td>
                        <td width="100">Inv Type</td>
                        <td width="25">View</td>
                        <td width="25">Add</td>
                        <td width="25">Edit</td>
                        <td width="25">Delete</td>
                        <td></td>
                    </tr>
                    <%
     aPermission = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aPermission = ((Permission) al.get(i));
                    %>
                    <tr class="
                        <%
                        if (i % 2 == 0) {
                            out.write("even_row");
                        } else {
                            out.write("odd_row");
                        }
                        %>
                        ">
                        <td width="16" align='center'>
                            <a href="pgPermission.jsp?InvId=<% out.write(aPermission.getInv().getInv_Id());%>&Role=<%=strRoleName%>">
                            <img src="images/icons/edit.gif" title="Edit the entry with ID = <%=aPermission.getInv().getInv_Id()%>" border='0'>
                        </td>
                        <td width="100" align='center'>
                            <%
                        out.write(aPermission.getInv().getInv_Id());
                            %>
                        </td>
                        <td  align='left'>
                            <%
                        out.write(aPermission.getInv().getInv_Desc());
                            %>
                        </td>
                        <td width="100" align='center'>
                            <%
                        if (aPermission.getInv().getInv_Type().equals("SCR")) {
                            out.write("Screen");
                        } else {
                            out.write("Report");
                        }
                            %>
                        </td>
                        <td width="25" align='center'>
                            <% if (aPermission.getPView() == 1) {%>
                            <img src="images/icons/tick.gif" border='0'>
                            <%}%>
                        </td>
                        <td width="25" align='center'>
                            <% if (aPermission.getPAdd() == 1) {%>
                            <img src="images/icons/tick.gif" border='0'>
                            <%}%>
                        </td>
                        <td width="25" align='center'>
                            <% if (aPermission.getPEdit() == 1) {%>
                            <img src="images/icons/tick.gif" border='0'>
                            <%}%>
                        </td>
                        <td width="25" align='center'>
                            <% if (aPermission.getPDelete() == 1) {%>
                            <img src="images/icons/tick.gif" border='0'>
                            <%}%>
                        </td>
                        <td></td>
                    </tr>
                    <% }%>
                </table>
            </td>
            <tr>
            </tr>
        </tr>
        <!-- Grid View Ends -->

        <!-- Pagination Starts -->
        <tr>
            <td class="formbody">
                <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                    <tr>
                        <td>
                            <a href="pgPermissionlist.jsp?CurPage=1">
                            <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                        </td>
                        <td><% if (nCurPage > 1) {%>
                            <a href="pgPermissionlist.jsp?CurPage=<%=(nCurPage - 1)%>">
                            <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></a>
                            <% } else {%>
                            <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                            <% }%>
                        </td>
                        <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                        <td><% if (nCurPage < nPageCount) {%>
                            <a href="pgPermissionlist.jsp?CurPage=<%=nCurPage + 1%>">
                            <img src="images/icons/page-next.gif" title="Go to next page" border="0"></a>
                            <% } else {%>
                            <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                            <% }%>
                        </td>
                        <td>
                            <a href="pgPermissionlist.jsp?CurPage=<%=nPageCount%>">
                            <img src="images/icons/page-last.gif" title="Go to last page" border="0"></a>
                        </td>
                        <td width="100%"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- Pagination Ends -->
        <%}%>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>