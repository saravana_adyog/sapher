<%-- 
    Document   : pgGenerateXml
    Created on : Jul 1, 2008, 12:58:15 PM
    Author     : Anoop Varma
--%>

<%@page
    contentType="text/html"
    pageEncoding="windows-1252"
    import="com.f3.edx.ich_icsr_v2_1.e2bm.tag.IcsrMessage"
    import="com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException"
    import="com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException"
    import="bl.sapher.general.GenConf"
    import="bl.sapher.general.Logger"
    import="java.text.SimpleDateFormat"
    import="java.util.Calendar"
    import="java.io.BufferedWriter"
    import="java.io.File"
    import="java.io.FileWriter"
    import="java.io.Writer"
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <script type="text/javascript" src="libjs/gui.js"></script>
        <title></title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <script type="text/javascript">

            function sendXml(xml_file) {
                var sel= document.getElementById('cmbMedium').value;

                if(sel == 'e-Mail') {
                    document.location.replace('pgSendXmlThruEMail.jsp?XmlFile='+xml_file);
                } else if(sel == 'HTTP') {
                    document.location.replace('pgSendXmlThruHttp.jsp?XmlFile='+xml_file);
                }else if(sel == 'FTP') {
                    document.location.replace('pgSendXmlThruFtp.jsp?XmlFile='+xml_file);
                }
                return;
            }
        </script>
    </head>
    <body>
        <%!    private String strXmlFile;
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgGenerateXml.jsp");
        String strRepNum = "";

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }
        IcsrMessage icsrMsg = new IcsrMessage((String) session.getAttribute("Company"), strRepNum);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US);
        String str = application.getRealPath("/XML");

        String strXmlFile =
                sdf.format(Calendar.getInstance().getTime()) +
                "-" + strRepNum +
                ".sgm";

        Writer output = null;
        File file = null;
        try {
            String strXmlData = icsrMsg.getTag().toString();
            if (strXmlData.length() == 0) {
                throw new Exception("IcsrMessage::getTag() did'nt provoded data.");
            }
            file = new File(str + "/" + strXmlFile);
            output = new BufferedWriter(new FileWriter(file));
            output.write(strXmlData);
            output.close();

            out.write("<script type=\"text/javascript\">");
            //out.write("     document.location.replace('XML/" + strXmlFile + "');");
            out.write("</script>");

        } catch (InvalidDataException ide) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgGenerateXml.jsp; Failed to create XML. This is due to absence of mandatory tags or presence too lengthy values.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ide.getMessage());

            String s = "Failed to create XML. This is due to &lt;b&gt;" + ide.getMessage() + "&lt;/b&gt;";
            System.out.println(s);
            pageContext.forward("pgError.jsp?ErrDetails=" + s);
        } catch (InvalidTagException ite) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgGenerateXml.jsp; Failed to create XML. This is due to absence of mandatory tags or presence too lengthy values.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ite.getMessage());

            String s = "Failed to create XML. This is due to &lt;b&gt;" + ite.getMessage() + "&lt;/b&gt;";
            System.out.println(s);
            pageContext.forward("pgError.jsp?ErrDetails=" + s);
        }

        //pageContext.forward("XML/" + strXmlFile);
%>

        <div style="height: 15px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">XML Submission Options</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <td class="formbody">
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="fLabel">Send XML via :</td>
                                <td><select name="cmbMedium" id="cmbMedium">
                                        <option>e-Mail</option>
                                        <option>HTTP</option>
                                        <option>FTP</option>
                                </select></td>
                                <td>
                                    <input type="button" value="Send" class="button" name="cmdSend" onclick="sendXml('<%=strXmlFile%>');" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><a href="#" onclick="document.location.replace('pgDisplyXML.jsp?XmlFile='+'<%=strXmlFile%>');">Show generated XML file.</a></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 1px;"></td>
            </tr>
            <!-- Grid View Starts -->
            <tr>
                <td class="formbody">

                </td>
            </tr>

            <tr>
                <td class="formbody">

                </td>
            </tr>
            <!-- Grid View Ends -->


            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>

    </body>
    <script type="text/javascript">

    </script>
</html>