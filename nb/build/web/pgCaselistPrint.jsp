<!--
Form Id : CSD1.1
Date    : 09-01-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.CaseDetails"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Case Details Print - [Sapher]</title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <script type="text/javascript" src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>

        <style type="text/css" media="all">@import "css/master.css";</style>
        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

        <script>
            var SAPHER_PARENT_NAME = "pgCaselistPrint";
        </script>

    </head>

    <body>
        <%!    private final String FORM_ID = "CSD1";
    private int intRepNum = -1;
    private String strRepNum = "";
    /**
     * This variable holds the type of report [CIOMS1/FullCase/WHO/MedWatch]
     * obtained from the query string.
     */
    private String RepType = "";
    private String strPTCode = "";
    private String strPTDesc = "";
    //AV private String strRepType = "";
    private String strTrialNum = "";
    private String strProdCode = "";
    private String strProdDesc = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private int nCaseSuspended = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private CaseDetails cDet = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
        %>

        <%
        cDet = new CaseDetails((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaselistPrint.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Update Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
        bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
        bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
        bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
        bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistPrint.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDR.jsp?Parent=0&SaveFlag=I");
        }

        if (!bAdd && !bEdit && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistPrint.jsp; Lack of permissions. [Add=" + bAdd + ", Edit=" + bEdit + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }

        /* type of PDF. CIOMS-I, WHO, FullCase or MedWatch*/
        if (request.getParameter("ReportType") != null) {
            RepType = request.getParameter("ReportType");
        }
        
        if (request.getParameter("txtRepNum") == null) {
            if (session.getAttribute("txtRepNum") == null) {
                strRepNum = "";
            } else {
                strRepNum = (String) session.getAttribute("txtRepNum");
            }
        } else {
            strRepNum = request.getParameter("txtRepNum");
        }
        session.setAttribute("txtRepNum", strRepNum);

        if (strRepNum.equals("")) {
            intRepNum = -1;
        } else {
            intRepNum = Integer.parseInt(strRepNum);
        }

        if (request.getParameter("txtPTCode_ID") == null) {
            if (session.getAttribute("txtPTCode_ID") == null) {
                strPTCode = "";
            } else {
                strPTCode = (String) session.getAttribute("txtPTCode_ID");
            }
        } else {
            strPTCode = request.getParameter("txtPTCode_ID");
        }
        session.setAttribute("txtPTCode_ID", strPTCode);

        if (request.getParameter("txtPTCode") == null) {
            if (session.getAttribute("txtPTCode") == null) {
                strPTDesc = "";
            } else {
                strPTDesc = (String) session.getAttribute("txtPTCode");
            }
        } else {
            strPTDesc = request.getParameter("txtPTCode");
        }
        session.setAttribute("txtPTCode", strPTDesc);

//            if (request.getParameter("txtCaseRType") == null) {
//                if (session.getAttribute("txtCaseRType") == null) {
//                    strRepType = "";
//                } else {
//                    strRepType = (String) session.getAttribute("txtCaseRType");
//                }
//            } else {
//                strRepType = request.getParameter("txtCaseRType");
//            }
//            session.setAttribute("txtCaseRType", strRepType);

        if (request.getParameter("txtCaseTrial") == null) {
            if (session.getAttribute("txtCaseTrial") == null) {
                strTrialNum = "";
            } else {
                strTrialNum = (String) session.getAttribute("txtCaseTrial");
            }
        } else {
            strTrialNum = request.getParameter("txtCaseTrial");
        }
        session.setAttribute("txtCaseTrial", strTrialNum);

        if (request.getParameter("txtProdCode") == null) {
            if (session.getAttribute("txtProdCode") == null) {
                strProdDesc = "";
            } else {
                strProdDesc = (String) session.getAttribute("txtProdCode");
            }
        } else {
            strProdDesc = request.getParameter("txtProdCode");
        }
        session.setAttribute("txtProdCode", strProdDesc);

        if (request.getParameter("txtProdCode_ID") == null) {
            if (session.getAttribute("txtProdCode_ID") == null) {
                strProdCode = "";
            } else {
                strProdCode = (String) session.getAttribute("txtProdCode_ID");
            }
        } else {
            strProdCode = request.getParameter("txtProdCode_ID");
        }
        session.setAttribute("txtProdCode_ID", strProdCode);

        if (request.getParameter("txtCaseDtFrom") == null) {
            if (session.getAttribute("txtCaseDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtCaseDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtCaseDtFrom");
        }
        session.setAttribute("txtCaseDtFrom", strDtFrom);

        if (request.getParameter("txtCaseDtTo") == null) {
            if (session.getAttribute("txtCaseDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtCaseDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtCaseDtTo");
        }
        session.setAttribute("txtCaseDtTo", strDtTo);

        String strSuspended = "";
        if (request.getParameter("cmbSuspendedCase") == null) {
            if (session.getAttribute("cmbSuspendedCase") == null) {
                strSuspended = "All";
            } else {
                strSuspended = (String) session.getAttribute("cmbSuspendedCase");
            }
        } else {
            strSuspended = request.getParameter("cmbSuspendedCase");
        }
        session.setAttribute("cmbSuspendedCase", strSuspended);

        if (strSuspended.equals("Valid")) {
            nCaseSuspended = 0;
        } else if (strSuspended.equals("Suspended")) {
            nCaseSuspended = 1;
        } else if (strSuspended.equals("All")) {
            nCaseSuspended = -1;
        }

        if (resetSession) {
            intRepNum = -1;
            strRepNum = "";
            strPTCode = "";
            strPTCode = "";
            strTrialNum = "";
            strProdCode = "";
            strDtFrom = "";
            strDtTo = "";
            nCaseSuspended = -1;
            al = CaseDetails.listCaseFromView((String) session.getAttribute("Company"), -1, "", "", "", "", "", -1);
        } else {
            al = CaseDetails.listCaseFromView((String) session.getAttribute("Company"), intRepNum, strPTCode, strTrialNum, strProdCode,
                    strDtFrom, strDtTo, nCaseSuspended);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Case Listing for <%
        if (RepType.equals("CIOMS1")) {
            out.write("CIOMS I");
        } else if (RepType.equals("MedWatch")) {
            out.write("MedWatch");
        } else if (RepType.equals("FullCase")) {
            out.write("FullCase");
        } else if (RepType.equals("WHO")) {
            out.write("WHO");
        }
                %></td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form name="FrmCaseListPrint" METHOD="POST" ACTION="pgCaselistPrint.jsp" >
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Search Criteria</td>
                            </tr>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtRepNum">Rep Num</label></td>
                                <td class="field">
                                    <input type="text" name="txtRepNum" id="txtRepNum" size="10" maxlength="9" title="Repoprt Number"
                                           onkeydown="return checkIsNumeric(event);" value="<%=strRepNum%>" clear=1 onkeydown="return ClearInput(event, 1);">
                                </td>
                                <td width="500" align="right"><b><%=strMessageInfo%></b></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="150"><label for="txtPTCode">Preferred Term</label></td>
                                <td class="field" width="300" colspan="6">

                                    <input type="text" id="txtPTCode" name="txtPTCode" size="30"
                                    value="<%=strPTDesc%>" onKeyUp="ajax_showOptions('txtPTCode_ID',this,'AEPT',event)" />
                                    <input type="text" readonly size="10" id="txtPTCode_ID" name="txtPTCode_ID" value="<%=strPTCode%>" />

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtCaseTrial">Source/Trial</label></td>
                                <td class="field" width="300">

                                    <input type="text" id="txtCaseTrial" name="txtCaseTrial" size="36"
                                    value="<%=strTrialNum%>" onKeyUp="ajax_showOptions('txtCaseTrial_ID',this,'TrialNo',event)">
                                    <input type="hidden" readonly size="3" id="txtCaseTrial_ID" name="txtCaseTrial_ID" value="<%=strTrialNum%>">

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtProdCode">Product</label></td>
                                <td class="field" width="1000">
                                    <div id="divProdDynData">

                                        <input type="text" id="txtProdCode" name="txtProdCode" size="57"
                                        value="<%=strProdDesc%>" onKeyUp="ajax_showOptions('txtProdCode_ID',this,'Product',event)" />
                                        <input type="text" readonly size="13" id="txtProdCode_ID" name="txtProdCode_ID" value="<%=strProdCode%>">

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtCaseDtFrom">Date Range</label></td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtCaseDtFrom" id="txtCaseDtFrom" size="12" maxlength="12"
                                                       value="<%=strDtFrom%>" readonly title="From Date"
                                                       clear=4 onKeyDown="return ClearInput(event, 4);">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                            <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select From date"></a></td>
                                            <script type="text/javascript">
                                                Calendar.setup({
                                                    inputField : "txtCaseDtFrom", //*
                                                    ifFormat : "%d-%b-%Y",
                                                    showsTime : true,
                                                    button : "cal_trigger1", //*
                                                    step : 1
                                                });
                                            </script>
                                            <td class="fLabel" style="padding-right: 3px; padding-left: 3px;">TO</td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtCaseDtTo" id="txtCaseDtTo" size="13" maxlength="12"
                                                       value="<%=strDtTo%>" readonly title="To Date"
                                                       clear=5 onKeyDown="return ClearInput(event, 5);">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                            <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select To date"></a></td>
                                            <script type="text/javascript">
                                                Calendar.setup({
                                                    inputField : "txtCaseDtTo", //*
                                                    ifFormat : "%d-%b-%Y",
                                                    showsTime : true,
                                                    button : "cal_trigger2", //*
                                                    step : 1
                                                });
                                            </script>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="cmbSuspendedCase">Validity</label></td>
                                <td class="field" colspan="2">
                                    <span style="float: left">
                                        <select id="cmbSuspendedCase" NAME="cmbSuspendedCase" class="comboclass" style="width: 100px;" title="Select Validity">
                                            <option <%=(strSuspended.equals("All") ? "SELECTED" : "")%>>All</option>
                                            <option <%=(strSuspended.equals("Valid") ? "SELECTED" : "")%>>Valid</option>
                                            <option <%=(strSuspended.equals("Suspended") ? "SELECTED" : "")%>>Suspended</option>
                                        </select>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2">
                                    <input type="hidden" id="hfRepType" value="<%=RepType%>" />
                                    <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;"
                                           <%if (RepType.equals("TabularRT")) {
            out.write(" disabled ");
        }%>>
                                    <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                                           onclick="location.replace('pgNavigate.jsp?Target=pgCaselistPrint.jsp');"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 1px;"></td>
            </tr>
            <!-- Grid View Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td class="form_group_title" colspan="10">Search Results
                                <%=" - " + al.size() + " record(s) found"%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% if (al.size() == 0) {
            return;
        }
            %>
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                        <tr class="grid_header">
                            <td width="16"></td>
                            <td width="200">Rep Num</td>
                            <td></td>
                        </tr>
                        <%
        String strRNum = "";
        String strStatusFlag = "";

        cDet = null;
        nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
        if (nCurPage > nPageCount) {
            nCurPage = 1;
        }
        int nStartPos = ((nCurPage - 1) * nRowCount);
        for (int i = nStartPos;
                i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
                i++) {
            cDet = (CaseDetails) al.get(i);
            strRNum = "" + cDet.getRep_Num();
            strStatusFlag = "" + cDet.getIs_Suspended();
                        %>
                        <tr class="
                            <%
                            if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            %>
                            ">
                            <td width="16" align="center" >
                                <%
                            if (RepType.equals("CIOMS1")) {
                                %>
                                <a href="#"  onclick="popupWnd('_blank', 'yes','pgPrintCIOMS1.jsp?RepNum=<%=strRNum%>',800,600)">
                                    <img src="images/icons/view.gif" title="Start generating PDF report" border='0'>
                                </a>
                                <%} else if (RepType.equals("FullCase")) {%>
                                <a href="#"  onclick="popupWnd('_blank', 'yes','pgPrintFullCase.jsp?RepNum=<%=strRNum%>',800,600)">
                                    <img src="images/icons/view.gif" title="Start generating PDF report" border='0'>
                                </a>
                                <%} else if (RepType.equals("WHO")) {%>
                                <a href="#"  onclick="popupWnd('_blank', 'yes','pgPrintWHO.jsp?RepNum=<%=strRNum%>',800,600)">
                                    <img src="images/icons/view.gif" title="Start generating PDF report" title="Start generating PDF report" border='0'>
                                </a>
                                <%} else if (RepType.equals("MedWatch")) {%>
                                <a href="#"  onclick="popupWnd('_blank', 'yes','pgPrintMedWatch.jsp?RepNum=<%=strRNum%>',800,600)">
                                    <img src="images/icons/view.gif" title="Start generating PDF report" border='0'>
                                </a>
                                <%}%>
                            </td>

                            <td width="200" align='left'>
                                <%
                            if (strStatusFlag.equals("1")) {
                                %>
                                <font color="red"><%out.write(strRNum);%></font>
                                <%
                            } else {
                                out.write(strRNum);
                            }
                                %>
                            </td>
                            <td></td>
                        </tr>
                        <% }%>
                    </table>
                </td>
                <tr>
                </tr>
            </tr>
            <!-- Grid View Ends -->

            <!-- Pagination Starts -->
            <tr>
                <td class="formbody">
                    <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                        <tr>
                            <td>
                                <a href="pgCaselistPrint.jsp?ReportType=<%=RepType%>&CurPage=1">
                                <img src="images/icons/page-first.gif" border="0" alt="First" title="Go to first page"/></a>
                            </td>
                            <td><% if (nCurPage > 1) {%>
                                <a href="pgCaselistPrint.jsp?ReportType=<%=RepType%>&CurPage=<%=(nCurPage - 1)%>">
                                <img src="images/icons/page-prev.gif" border="0" alt="Prev" title="Go to previous page"/></a>
                                <% } else {%>
                                <img src="images/icons/page-prev.gif" border="0" alt="Prev" title="Go to previous page"/>
                                <% }%>
                            </td>
                            <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                            <td><% if (nCurPage < nPageCount) {%>
                                <a href="pgCaselistPrint.jsp?ReportType=<%=RepType%>&CurPage=<%=nCurPage + 1%>">
                                <img src="images/icons/page-next.gif" border="0" alt="Next" title="Go to next page"/></a>
                                <% } else {%>
                                <img src="images/icons/page-next.gif" border="0" alt="Next" title="Go to next page"/>
                                <% }%>
                            </td>
                            <td>
                                <a href="pgCaselistPrint.jsp?ReportType=<%=RepType%>&CurPage=<%=nPageCount%>">
                                <img src="images/icons/page-last.gif" border="0" alt="Last" title="Go to last page"/></a>
                            </td>
                            <td width="100%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Pagination Ends -->
            <tr>
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>