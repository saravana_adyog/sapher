<%-- 
    Document   : pgAeSummary
    Created on : Jul 31, 2008, 11:19:22 AM
    Author     : Anoop Varma
--%>

<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.CaseDetails"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"
    import="bl.sapher.general.RecordNotFoundException"
    isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Medical History - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <script type="text/javascript" src="libjs/gui.js"></script>
        <script type="text/javascript" src="libjs/case-details.js"></script>
    </head>
    <body>
        <%!    CaseDetails aCase = null;
    private final String FORM_ID = "CSD1";
    private String strSaveFlag = "";
    private String strParent = "";
    private String strRepNum = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        aCase = new CaseDetails((String) session.getAttribute("LogFileName"));
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            strSaveFlag = request.getParameter("SaveFlag");
        }
        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMedSummary.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMedSummary.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMedSummary.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMedSummary.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                strSaveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            strSaveFlag = "U";
            if (!strSaveFlag.equals("U")) {
            } else {
                if (request.getParameter("RepNum") != null) {
                    if (request.getParameter("RepNum") != "") {
                        aCase = new CaseDetails((String) session.getAttribute("Company"), "Y", Integer.parseInt(request.getParameter("RepNum")));
                    }
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMedSummary.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Medical History</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmCaseDetails" name="FrmCaseDetails" method="POST" action="pgSavCaseCmSummary.jsp"
                      onsubmit="return validateCmFields();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(strSaveFlag.equals("I") ? "New " : (strSaveFlag.equals("V") ? "View " : "Edit "))%>Medical History</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>


                            <tr>
                                <td class="fLabel" width="125"><label for="txtRelevantHistory">Other Relevant History</label></td>
                            </tr>
                            <tr>
                                <td class="field">
                                    <textarea name="txtRelevantHistory" id="txtRelevantHistory" cols="140" rows="9" wrap="physical" title="Other relevant history"
                                              <% if (strSaveFlag.equals("V")) {
            out.write(" readonly ");
        }%>
                                              ><%if (aCase.getShort_History() == null) {
            out.write("");
        } else {
            out.write(aCase.getShort_History());
        }
                                    %></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtShortComment">Short Comment</label></td>
                            </tr>
                            <tr>
                                <td class="field">
                                    <textarea name="txtShortComment" id="txtShortComment" cols="140" rows="9" wrap="physical" title="Short comment"
                                              <% if (strSaveFlag.equals("V")) {
            out.write(" readonly ");
        }%>
                                              ><%if (aCase.getShort_Comment() == null) {
            out.write("");
        } else {
            out.write(aCase.getShort_Comment());
        }
                                    %></textarea>
                                </td>
                            </tr>


                            <tr>
                                <td class="field" align="left">
                                    <input type="submit" name="cmdSave" class="<%=(strSaveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (strSaveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=strSaveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="hidden" id="txtRepNum" name="txtRepNum" value="<%=strRepNum%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%="pgCaseSummary.jsp?RepNum=" + strRepNum%>')">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>