<!--
Form Id : CSD1.1
Date    : 12-04-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.CaseDetails"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException" import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Reports - [Sapher]</title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <script type="text/javascript" src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>

        <style type="text/css" media="all">@import "css/master.css";</style>
        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

        <script>
            var SAPHER_PARENT_NAME = "pgRep";
        </script>

    </head>

    <body onUnload="nProdPopupCount=0;">

        <script>var nProdPopupCount = 0;</script>

        <%!    private final String FORM_ID = "CSD1";
    //    private int intRepNum = -1;
    //    private String strRepNum = "";
    /**
     * This variable holds the type of report [CIOMS1I/ICH/Sum Tab-S/Sum Tab-RT]
     * obtained from the query string.
     */
    private String RepType = "";

    //    private String strRepTypeCode = "";
    //    private String strRepType = "";
    //    private String strTrialNum = "";
    //    private String strGenericNm = "";
    //    private String strBrandNm = "";
    //    private String strDtFrom = "";
    //    private String strDtTo = "";
    //    private int nCaseSuspended = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    //    private ArrayList al = null;
    private User currentUser = null;
    //    private CaseDetails cDet = null;
    //    private int nRowCount = 10;
    //    private int nPageCount;
    //    private int nCurPage;
    private Inventory inv = null;
    //    private String strMessageInfo = "";
%>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgRep.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                    return;
                }
                inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
                bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
                bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
                bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
                bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
            } catch (TimeoutException toe) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDR.jsp?Parent=0&SaveFlag=I");
        }

        if (!bAdd && !bEdit && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("ReportType") != null) {
            RepType = request.getParameter("ReportType");
        }

        %>
        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Report Parameters</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <FORM name="FrmRep" METHOD="POST" ACTION="<%
        if (RepType.equals("CIOMS2")) {
            out.write("pgPrintCIOMS2.jsp");
        } else if (RepType.equals("ICH")) {
            out.write("pgPrintICH.jsp");
        } else if (RepType.equals("TabularS")) {
            out.write("pgPrintSumTabS.jsp");
        } else if (RepType.equals("TabularRT")) {
            out.write("pgPrintSumTabRT.jsp");
        }
                      %>" >
                    <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
                        <td class="form_group_title" colspan="10"><%
        if (RepType.equals("CIOMS2")) {
            out.write("CIOMS II");
        } else if (RepType.equals("ICH")) {
            out.write("ICH");
        } else if (RepType.equals("TabularS")) {
            out.write("Summary Tabulation - Seriousness");
        } else if (RepType.equals("TabularRT")) {
            out.write("Summary Tabulation - Report Type");
        }
                        %></td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtProdCode_ID">Product</label></td>
                        <td class="field" width="1000">

                            <input type="text" id="txtProdCode" name="txtProdCode" size="57" onKeyUp="ajax_showOptions('txtProdCode_ID',this,'Product',event)">
                            <input type="text" readonly size="13" id="txtProdCode_ID" name="txtProdCode_ID" value="">

                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtCaseDtFrom">Date Range</label></td>
                        <td class="field">
                            <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <input type="text" name="txtCaseDtFrom" id="txtCaseDtFrom" size="12" maxlength="12"
                                               title="From Date" readonly clear=2 onKeyDown="return ClearInput(event, 2);">
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select From Date"></a>
                                    </td>
                                    <td class="fLabel" style="padding-right: 3px; padding-left: 3px;">TO</td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <input type="text" name="txtCaseDtTo" id="txtCaseDtTo" size="13" maxlength="12"
                                               title="To Date" readonly clear=3 onKeyDown="return ClearInput(event, 3);">
                                    </td>
                                    <td style="padding-right: 3px; padding-left: 0px;">
                                        <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select To Date"></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <script type="text/javascript">
                        Calendar.setup({
                            inputField : "txtCaseDtFrom", //*
                            ifFormat : "%d-%b-%Y",
                            showsTime : true,
                            button : "cal_trigger1", //*
                            step : 1
                        });
                    </script>
                    <script type="text/javascript">
                        Calendar.setup({
                            inputField : "txtCaseDtTo", //*
                            ifFormat : "%d-%b-%Y",
                            showsTime : true,
                            button : "cal_trigger2", //*
                            step : 1
                        });
                    </script>
                    <tr>
                        <td class="fLabel" width="125"><label for="cmbSuspendedCase">Validity</label></td>
                        <td class="field" colspan="2">
                            <span style="float: left">
                                <SELECT id="cmbSuspendedCase" title="Select Validity" name="cmbSuspendedCase" class="comboclass" style="width: 100px;">
                                    <option selected>All</option>
                                    <option>Valid</option>
                                    <option>Suspended</option>
                                </SELECT>
                                <input type="submit" name="cmdGenerate" title="Start generating PDF report" class="button" value="Generate" style="width: 87px;"/>
                            </span>
                        </td>
                    </tr>
                </form>
            </tr>
        </table>

        <!-- Form Body Ends -->
        <tr>
            <td style="height: 1px;"></td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>

        <div style="height: 25px;"></div>
    </body>
</html>