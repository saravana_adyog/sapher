<!--
Form Id : USM3
Date    : 13-12-2007.
Author  : Arun P. Jose
-->
<%@page
contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.GenConf"
import="bl.sapher.general.RecordNotFoundException" import="bl.sapher.general.Logger"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.UserCountException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.DBConnectionException"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%!    private final String FORM_ID = "USM1";
    private String strPassword = "";
    private int nPwdMistakeCount = -1;
    private int nPwdChgFlag = -1;
    private int nLocked = -1;
    private int next = -1;
    private User currentUser = null;
    private GenConf gC = null;
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgShortSavUser.jsp");
        strPassword = "";
        nPwdMistakeCount = -1;
        nLocked = -1;
        next = -1;
        try {
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"));
            gC = new GenConf((String) session.getAttribute("Company"));
        } catch (UserCountException e) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=7");
        </script>
        <%
                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
            return;
        }

        if (request.getParameter("txtPassword") != null) {
            strPassword = request.getParameter("txtPassword");
            nPwdChgFlag = 0;
        }

        if (request.getParameter("PwdMistake") != null) {
            if (Integer.parseInt(request.getParameter("PwdMistake")) == 1) {
                nPwdMistakeCount = currentUser.getPwd_Mistake_Count() + 1;
                if (nPwdMistakeCount >= gC.getSysLoginMistakeCount()) {
                    nLocked = 1;
                }
            } else {
                nPwdMistakeCount = 0;
            }
        }

        if (request.getParameter("Next") != null) {
            next = Integer.parseInt(request.getParameter("Next"));
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser.saveUser((String) session.getAttribute("Company"), "U", currentUser.getUsername(), "", strPassword, nPwdChgFlag, nPwdMistakeCount, nLocked);
            if (!strPassword.equals("")) {
                session.setAttribute("CurrentUserPW", strPassword);
            }
            if (next == 1) { // pgLogin.jsp
%>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%        } else if (next == 0) { //content.jsp
            pageContext.forward("content.jsp?Status=2");
        } else if (next == 2) {//pgSapher
            //Register User
            
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp: Registering User/session");
            String sessId = currentUser.registerUser((String) session.getAttribute("Company"), request.getRemoteAddr());
            session.setAttribute("SapherSessionId", sessId);
            session.setAttribute("LogFileName", sessId);
            
            pageContext.forward("pgSapher.jsp");
            
        }
    } catch (TimeoutException toe) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp: Timeout Exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
        return;
    } catch (Exception ex) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp: Exception" +"1"+ ex.toString());
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
        }
        %>
    </body>
</html>
