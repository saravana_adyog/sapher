<!--
Form Id : CSD2
Date    : 08-02-2008.
Author  : Anoop Varma
-->

<%@page contentType="text/html"
pageEncoding="UTF-8"
import="java.util.ArrayList"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.Adverse_Event"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
        %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>

        <%!    private String FORM_ID = "CSD1";
    private boolean bDelete;
    private Inventory inv = null;
    private User currentUser = null;
    private int nRepNum;
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDelAE.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        try {
            FORM_ID = "CSD1";
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp: bDelete=" + bDelete);

            Adverse_Event anAE = null;

            if (request.getParameter("RepNum") != null && request.getParameter("LltCode") != null) {
                nRepNum = Integer.parseInt((String) request.getParameter("RepNum"));
                anAE = new Adverse_Event((String) session.getAttribute("Company"), "y", nRepNum, (String) request.getParameter("LltCode"));
                if (bDelete) {
                    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp: Deleting the entry with RepNum/LLT=" + request.getParameter("RepNum") + "/" + request.getParameter("LltCode"));
                    anAE.deleteAdverse_Event((String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"));
                    pageContext.forward("pgCaseSummary.jsp?DelStatus=0&RepNum=" + nRepNum + "&SaveFlag=U");
                } else {
                    throw new ConstraintViolationException("2");
                }
            }

        } catch (ConstraintViolationException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("pgCaseSummary.jsp?DelStatus=2&RepNum=" + nRepNum + "&SaveFlag=U");
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("pgCaseSummary.jsp?DelStatus=4&RepNum=" + nRepNum + "&SaveFlag=U");
        }
        %>
    </body>
</html>
