<!--
Form Id : USM3
Date    : 13-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.GenConf"
import="bl.sapher.general.Logger"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!    private User currentUser = null;
    private GenConf gC = null;
    private String strNext = "";
%>

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Reset Password - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgResetPwd.jsp");
        gC = new GenConf((String) session.getAttribute("Company"));
        %>
        <script>
            function validate(){
                if (document.FrmPassword.txtPassword.value.length < <%=gC.getSysMinPwdLength()%> ){
                    alert("Minimum password length is <%=gC.getSysMinPwdLength()%>");
                    document.FrmPassword.txtPassword.focus();
                    return false;
                }
                if (document.FrmPassword.txtPassword.value != document.FrmPassword.txtConfirmPassword.value){
                    alert( "Passwords do not match.");
                    document.FrmPassword.txtPassword.focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <%
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"));
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgResetPwd.jsp; Timeout exception.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgResetPwd.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgResetPwd.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        if (request.getParameter("Next") != null) {
            strNext = request.getParameter("Next");
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">User Administration</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmPassword" name="FrmPassword" method="POST" action="pgShortSavUser.jsp" onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Change Password</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtPassword">New Password</label></td>
                                <td class="field">
                                    <input type="password" name="txtPassword" id="txtPassword" size="30" maxlength="25" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtConfirmPassword">Confirm Password</label></td>
                                <td class="field">
                                    <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" size="30" maxlength="25" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="hidden" id="Next" name="Next" value="<%=strNext%>">
                                    <input type="submit" name="cmdSave" class="button" value="Save" style="width: 60px;">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=strNext.equals("0") ? "content.jsp" : "pgLogin.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>