<!--
Form Id : CSD2
Date    : 22-01-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="java.text.SimpleDateFormat"
import="bl.sapher.Adverse_Event"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Adverse Event - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <script src="libjs/ajax/ajax.js"></script>

        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

        <script type="text/javascript" src="libjs/gui.js"></script>


        <script>
            var SAPHER_PARENT_NAME = "pgAE";

            function validate(){
                if(!checkIsAlphaSpaceVar(document.getElementById('txtPrevReport').value)) {
                    window.alert('Invalid character(s) in previous report');
                    document.getElementById('txtPrevReport').focus();
                    return false;
                }
                if(!checkIsAlphaSpaceVar(document.getElementById('txtEvtTreatment').value)) {
                    window.alert('Invalid character(s) in event treatment');
                    document.getElementById('txtEvtTreatment').focus();
                    return false;
                }
                return true;
            }

            function setRepFlagControls() {
                if (document.FrmAE.chkPrevReportFlag.checked) {
                    document.FrmAE.txtPrevReport.disabled      = false;
                }
                else {
                    document.FrmAE.txtPrevReport.value = "";
                    document.FrmAE.txtPrevReport.disabled      = true;
                }
            }

            function setLhaControls() {
                if (document.FrmAE.chkReportedToLha.checked) {
                    document.FrmAE.txtLhaCode.disabled      = false;
                    document.FrmAE.txtLhaCode_ID.disabled   = false;
                    document.FrmAE.txtDateOfReport.disabled = false;
                }
                else {
                    document.FrmAE.txtLhaCode.value = "";
                    document.FrmAE.txtLhaCode_ID.value = "";
                    document.FrmAE.txtDateOfReport.value = "";
                    document.FrmAE.txtLhaCode.disabled = true;
                    document.FrmAE.txtLhaCode_ID.disabled = true;
                    document.FrmAE.txtDateOfReport.disabled = true;
                }
            }

            function setControls() {
                setRepFlagControls();
                setLhaControls();
            }

            function validateAEFields(){
                if(document.FrmAE.txtLLTCode.value == '') {
                    window.alert('Please provide Adverse Event LLT.');
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body onLoad="setControls()">
        <%    Adverse_Event anAE = new Adverse_Event((String) session.getAttribute("LogFileName"));
        final String FORM_ID = "CSD1";
        String saveFlag = "";
        String strParent = "";
        boolean bAdd;
        boolean bEdit;
        //private boolean bDelete;
        boolean bView;
        User currentUser = null;
        Inventory inv = null;
        String strRepNum = "";
        String strLltCode = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        %>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgAE.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }

        if (request.getParameter("AeSaveFlag") != null) {
            saveFlag = request.getParameter("AeSaveFlag");
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        if (request.getParameter("LltCode") != null) {
            strLltCode = request.getParameter("LltCode");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp: Edit mode. Rep#:" + strRepNum + ", LLT=" + strLltCode);
                if ((strRepNum != null) && strLltCode != null) {
                    anAE = new Adverse_Event((String) session.getAttribute("Company"), "y", Integer.parseInt(strRepNum), strLltCode);
                } else {
                    //pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp: Insertion mode.");
                anAE = new Adverse_Event();
            }

        } catch (RecordNotFoundException e) {
            //pageContext.forward("content.jsp?Status=0");
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='700' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Adverse Event</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmAE" name="FrmAE" method="POST" action="pgSavAE.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Adverse Event</td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtRepNum">Rep Num</label></td>
                                <td class="field"><input type="text" name="txtRepNum"
                                                             id="txtRepNum" size="15" maxlength="10"
                                                             <%
        out.write(" value=\"" + strRepNum + "\" readonly ");
                                                             %> title="Report number"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtLLTCode">Adverse Event LLT</label></td>
                                <td class="field" colspan="6">
                                    <div id="divLLTDynData">

                                        <input type="text" id="txtLLTCode" name="txtLLTCode" size="45" onKeyUp="ajax_showOptions('txtLLTCode_ID',this,'AELLT',event)"
                                               value="<%=(anAE.getLlt_Desc() == null ? "" : anAE.getLlt_Desc())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="10" id="txtLLTCode_ID" name="txtLLTCode_ID"
                                               value="<%=(anAE.getLlt_Code() == null ? "" : anAE.getLlt_Code())%>">
                                        <a href="#" onclick="popupWnd('spopup','no','pgMedDraSoc.jsp', 800, 600)">
                                            <img src="images/icons/star_on-16x16.gif" alt="Select" title="Popup MedDRA Browser" border='0' name="imgSearch">
                                        </a>

                                    </div>
                                </td>
                                <td class="fLabelR"><label for="chkAggrLLT">Aggravation of</label></td>
                                <td class="field" colspan="">
                                    <input type="checkbox" name="chkAggrLLT" id="chkAggrLLT" class="chkbox" title="Aggravation of Adverse Event Low Level Term"
                                           <%
        if (anAE.getAggr_Of_Llt() != null) {
            if (anAE.getAggr_Of_Llt().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           %> />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtPTCode">Adverse Event PT</label></td>
                                <td class="field" colspan="6">
                                    <div id="divDiagDynData">

                                        <input type="text" id="txtPTCode" name="txtPTCode" size="45" onKeyUp="ajax_showOptions('txtPTCode_ID',this,'AEPT',event)"
                                               value="<%=(anAE.getDiag_Desc() == null ? "" : anAE.getDiag_Desc())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="10" id="txtPTCode_ID" name="txtPTCode_ID"
                                               value="<%=(anAE.getDiag_Code() == null ? "" : anAE.getDiag_Code())%>">

                                    </div>
                                </td>
                                <td class="fLabelR"><label for="chkAggrPT">Aggravation of</label></td>
                                <td class="field" colspan="">
                                    <input type="checkbox" name="chkAggrPT" id="chkAggrPT" class="chkbox" title="Aggravation of Adverse Event Preferred Term"
                                           <%
        if (anAE.getAggr_Of_Diag() != null) {
            if (anAE.getAggr_Of_Diag().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           %> />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtBodySysNum">System Organ Class</label></td>
                                <td class="field" colspan="9">
                                    <div id="divBodSysDynData">

                                        <input type="text" id="txtBodySysNum" name="txtBodySysNum" size="45" onKeyUp="ajax_showOptions('txtBodySysNum_ID',this,'BodySys',event)"
                                               value="<%=(anAE.getBody_System() == null ? "" : anAE.getBody_System())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="10" id="txtBodySysNum_ID" name="txtBodySysNum_ID"
                                               value="<%=(anAE.getBody_Sys_Num() == null ? "" : anAE.getBody_Sys_Num())%>">

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtClasfCode">Seriousness</label></td>
                                <td class="field" colspan="9">

                                    <input type="text" id="txtClasfCode" name="txtClasfCode" size="50" onKeyUp="ajax_showOptions('txtClasfCode_ID',this,'Seriousness',event)"
                                           value="<%=(anAE.getClassification_Desc() == null ? "" : anAE.getClassification_Desc())%>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                    <input type="text" readonly size="5" id="txtClasfCode_ID" name="txtClasfCode_ID"
                                           value="<%=(anAE.getClassification_Code() == null ? "" : anAE.getClassification_Code())%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtOutcomeCode">Total AE Outcome</label></td>
                                <td class="field" colspan="9">
                                    <div id="divOCDynData">

                                        <input type="text" id="txtOutcomeCode" name="txtOutcomeCode" size="50" onKeyUp="ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)"
                                               value="<%=(anAE.getOutcome() == null ? "" : anAE.getOutcome())%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="5" id="txtOutcomeCode_ID" name="txtOutcomeCode_ID"
                                               value="<%=(anAE.getOutcome_Code() == null ? "" : anAE.getOutcome_Code())%>">

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtSeverityCode">Severity</label></td>
                                <td class="field" colspan="9">

                                    <input type="text" id="txtSeverityCode" name="txtSeverityCode" size="50" onKeyUp="ajax_showOptions('txtSeverityCode_ID',this,'EventSeverity',event)"
                                           value="<%=(anAE.getESeverity_Desc() == null ? "" : anAE.getESeverity_Desc())%>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                    <input type="text" readonly size="5" id="txtSeverityCode_ID" name="txtSeverityCode_ID"
                                           value="<%=(anAE.getESeverity_Code() == null ? "" : anAE.getESeverity_Code())%>">

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="chkReportedToLha">Reported to LHA</label></td>
                                <td class="field">
                                    <input type="checkbox" name="chkReportedToLha" id="chkReportedToLha" size="1" class="chkbox" title="Whether the event is reported to LHA?"
                                           onclick="setLhaControls();" <%
        if (anAE.getReported_To_LHA() != null) {
            if (anAE.getReported_To_LHA().equalsIgnoreCase("Y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           %> />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtLhaCode">LHA</label></td>
                                <td class="field" colspan="9">

                                    <input type="text" id="txtLhaCode" name="txtLhaCode" size="50" onKeyUp="ajax_showOptions('txtLhaCode_ID',this,'LHA',event)"
                                           value="<%=(anAE.getLha_Desc() == null ? "" : anAE.getLha_Desc())%>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                    <input type="text" readonly size="12" id="txtLhaCode_ID" name="txtLhaCode_ID"
                                           value="<%=(anAE.getLha_Code() == null ? "" : anAE.getLha_Code())%>">

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel">
                                    <label for="txtDateOfReport">Date of Report</label>
                                </td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtDateOfReport" id="txtDateOfReport" size="15" maxlength="11" title="Date of report"
                                                       clear=107 value="<%=(anAE.getReport_Date() == null ? "" : sdf.format(anAE.getReport_Date()))%>" readonly
                                                       <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 107);\" ");
        }%> />
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <%
        if (!saveFlag.equals("V")) {
                                                %>
                                                <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" title="Select Date of report"></a>
                                                <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField : "txtDateOfReport", //*
                                                        ifFormat : "%d-%b-%Y",
                                                        showsTime : true,
                                                        button : "cal_trigger1", //*
                                                        step : 1
                                                    });
                                                </script>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel">
                                    <label for="txtStartDate">Start date</label>
                                </td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtStartDate" id="txtStartDate" size="15" maxlength="11" title="Event start date"
                                                       clear=108 value="<%=(anAE.getDate_Start() == null ? "" : sdf.format(anAE.getDate_Start()))%>" readonly
                                                       <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 108);\" ");
        }%> />
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <%
        if (!saveFlag.equals("V")) {
                                                %>
                                                <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" border='0' name="imgCal" title="Select Event start date"></a>
                                                <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField : "txtStartDate", //*
                                                        ifFormat : "%d-%b-%Y",
                                                        showsTime : true,
                                                        button : "cal_trigger2", //*
                                                        step : 1
                                                    });
                                                </script>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel">
                                    <label for="txtEndDate">End date</label>
                                </td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtEndDate" id="txtEndDate" size="15" maxlength="11" title="Event end date"
                                                       clear=109 value="<%=(anAE.getDate_Stop() == null ? "" : sdf.format(anAE.getDate_Stop()))%>" readonly
                                                       <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 109);\" ");
        }%>
                                                   </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <%
        if (!saveFlag.equals("V")) {
                                                %>
                                                <a href="#" id="cal_trigger3"><img src="images/icons/cal.gif" border='0' name="imgCal" title="Select Event end date"></a>
                                                <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField : "txtEndDate", //*
                                                        ifFormat : "%d-%b-%Y",
                                                        showsTime : true,
                                                        button : "cal_trigger3", //*
                                                        step : 1
                                                    });
                                                </script>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="chkLabelling">Expected</label></td>
                                <td class="field">
                                    <input type="checkbox" name="ChkLabelling" id="ChkLabelling" size="1" class="chkbox" title="Whether it is labelled?"
                                           <%
        if (anAE.getLabelling() != null) {
            if (anAE.getLabelling().equalsIgnoreCase("E")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           %> >
                                    <label class="fLabel">&nbsp;&nbsp;(Labelling)</label>
                                </td>

                            </tr>
                            <tr>
                                <td class="fLabel"><label for="chkPrevReportFlag">Prev. reporting flag</label></td>
                                <td class="field">
                                    <input type="checkbox" class="chkbox" name="chkPrevReportFlag" id="chkPrevReportFlag" size="2" maxlength="1"
                                           title="Previous reporting flag"
                                           onclick="setRepFlagControls();" <%
        if (anAE.getPrev_Report_Flg() != null) {
            if (anAE.getPrev_Report_Flg().equalsIgnoreCase("Y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtPrevReport">Previous report</label></td>
                                <td class="field" colspan="9">
                                    <input type="text" name="txtPrevReport" id="txtPrevReport" size="50" maxlength="50"  title="Previous report"
                                           value="<%=(anAE.getPrev_Report() == null ? "" : anAE.getPrev_Report())%>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtEvtTreatment">Event treatment</label></td>
                                <td class="field" colspan="9">
                                    <input type="text" name="txtEvtTreatment" id="txtEvtTreatment" size="100" maxlength="100" title="Event treatment"
                                           value="<%=(anAE.getEvent_Treatment() == null ? "" : anAE.getEvent_Treatment())%>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="chkEvtAbateOnStop">Event abate on stop</label></td>
                                <td class="field">
                                    <input type="checkbox" name="chkEvtAbateOnStop" id="chkEvtAbateOnStop" size="1" class="chkbox"
                                           title="Event abate on stop"
                                           <%
        if (anAE.getEvent_Abate_On_Stop() != null) {
            if (anAE.getEvent_Abate_On_Stop().equalsIgnoreCase("Y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave"  onclick="return validateAEFields();" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           title="Save the Adverse Event"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="hidden" id="RepNum" name="RepNum" value="<%=strRepNum%>">
                                    <input type="hidden" id="DiagNum" name="DiagNum" value="<%=strLltCode%>">
                                    <input type="reset" id="cmdCancel" name="cmdCancel" class="button" value="Cancel" style="width: 60px;" title="Cancel the operation"
                                           onclick="javascript:location.replace('<%="pgCaseSummary.jsp?RepNum=" + strRepNum%>')"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>