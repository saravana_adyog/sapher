<!--
Form Id : CSD1
Date    : 11-12-2007.
Author  : Arun P. Jose, Anoop Varma.
-->
<%@page
contentType="text/html"
pageEncoding="UTF-8"

import="bl.sapher.CaseDetails"
import="bl.sapher.Adverse_Event"
import="bl.sapher.Conc_Medication"
import="bl.sapher.User"
import="bl.sapher.Inventory"

import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.DBConnectionException"

import="java.sql.Date"
import="java.util.ArrayList"
import="java.text.SimpleDateFormat"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
        SimpleDateFormat dFormat = null;
        final String FORM_ID = "CSD1";
        int intRepNum;
        Date dtEntry = null;
        Date dtRecv = null;
        boolean bAdd;
        boolean bEdit;
        User currentUser = null;
        String strClinRep = "";
        String strCountryCode = "";
        String strProdCode = "";
        String strInitials = "";
        String strOutcomeCode = "";
        int nAgeRep = -1;
        String strSexCode = "";
        String strTrial = "";
        String strPatNum = "";
        String strPTCode = "";
        String strAggrPT = "N";
        String strLLTCode = "";
        String strAggrLLT = "N";
        String strBodySysNum = "";
        String strClasfCode = "";
        String strPhyAssCode = "";
        String strCoAssCode = "";
        String strVerbatim = "";

        dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseQS.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseQS.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseQS.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseQS.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        CaseDetails cDet = null;
        session.removeAttribute("DirtyFlag");

        if (request.getParameter("txtEntryDate") != null) {
            if (!request.getParameter("txtEntryDate").equals("")) {
                dtEntry = new Date(dFormat.parse(request.getParameter("txtEntryDate")).getTime());
            }
        }

        if (request.getParameter("txtRecvDate") != null) {
            if (!request.getParameter("txtRecvDate").equals("")) {
                dtRecv = new Date(dFormat.parse(request.getParameter("txtRecvDate")).getTime());
            }
        }

        if (request.getParameter("txtClinRep") != null) {
            strClinRep = request.getParameter("txtClinRep");
        }

        if (request.getParameter("txtCountryCode_ID") != null) {
            strCountryCode = request.getParameter("txtCountryCode_ID").trim();
        }

        if (request.getParameter("txtProdCode_ID") != null) {
            strProdCode = request.getParameter("txtProdCode_ID").trim();
        }

        if (request.getParameter("txtInitials") != null) {
            strInitials = request.getParameter("txtInitials");
        }

        if (request.getParameter("txtAgeRep") != null) {
            if (request.getParameter("txtAgeRep") != "") {
                nAgeRep = Integer.parseInt(request.getParameter("txtAgeRep"));
            }
        }

        if (request.getParameter("txtSexCode_ID") != null) {
            strSexCode = request.getParameter("txtSexCode_ID").trim();
        }

        if (request.getParameter("txtTrial") != null) {
            strTrial = request.getParameter("txtTrial");
        }

        if (request.getParameter("txtPatNum") != null) {
            strPatNum = request.getParameter("txtPatNum");
        }

        if (request.getParameter("txtPTCode_ID") != null) {
            strPTCode = request.getParameter("txtPTCode_ID").trim();
        }

        if (request.getParameter("chkAggrPT") != null) {
            strAggrPT = request.getParameter("chkAggrPT");
        }

        if (request.getParameter("txtLLTCode_ID") != null) {
            strLLTCode = request.getParameter("txtLLTCode_ID").trim();
        }

        if (request.getParameter("chkAggrLLT") != null) {
            strAggrLLT = request.getParameter("chkAggrLLT");
        }

        if (request.getParameter("txtBodySysNum_ID") != null) {
            strBodySysNum = request.getParameter("txtBodySysNum_ID").trim();
        }

        if (request.getParameter("txtClasfCode_ID") != null) {
            strClasfCode = request.getParameter("txtClasfCode_ID").trim();
        }

        if (request.getParameter("txtOutcomeCode_ID") != null) {
            strOutcomeCode = request.getParameter("txtOutcomeCode_ID").trim();
        }

        if (request.getParameter("txtPhyAssCode_ID") != null) {
            strPhyAssCode = request.getParameter("txtPhyAssCode_ID").trim();
        }

        if (request.getParameter("txtCoAssCode_ID") != null) {
            strCoAssCode = request.getParameter("txtCoAssCode_ID").trim();
        }

        if (request.getParameter("txtNarrative") != null) {
            strVerbatim = request.getParameter("txtNarrative");
        }

        try {
            if (!bAdd) {
                throw new ConstraintViolationException("2");
            }
            cDet = new CaseDetails();
            cDet.saveCaseDetails((String) session.getAttribute("Company"), "I", currentUser.getUsername(), -1, "",
                    strTrial, strCountryCode, "", "",
                    "", "", "", "", "", "", "", strSexCode,
                    "", "", strProdCode, strPhyAssCode,
                    strCoAssCode, "", "", "",
                    dtEntry, "", dtRecv, "",
                    "", strClinRep, "", strPatNum,
                    "", "", -1, null,
                    null, "", "", strInitials,
                    null, nAgeRep, -1, -1,
                    "", -1, "", null,
                    "", "", null, null,
                    "", "", "", null,
                    "", strVerbatim, "", null,
                    null, "", "", 0);

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseQS.jsp; Insertion success.");
            intRepNum = cDet.getRep_Num();

            if (!strPTCode.equals("")) {
                Adverse_Event ae = new Adverse_Event();
                ae.saveAdverse_Event((String) session.getAttribute("Company"), "I", currentUser.getUsername(), intRepNum, strPTCode, strLLTCode, strOutcomeCode, strBodySysNum,
                        strClasfCode, "", "", "", null, null, null, "", "", "", "", "", "", "");
            }
            pageContext.forward("content.jsp?Status=2&RepNum=" + cDet.getRep_Num());
        } catch (Exception ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseQS.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
        }
        %>
    </body>
</html>
