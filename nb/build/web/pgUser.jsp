<!--
Form Id : USM1
Date    : 15-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.User"
import="bl.sapher.Role"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException" import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%!    User usr = null;
    private final String FORM_ID = "USM1";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
%>

<%
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
%>

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>User - [Sapher]</title>
        <script>var SAPHER_PARENT_NAME = "pgUser";</script>
        <script src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script>
            function validate(){
                if (trim(document.FrmUser.txtUsername.value) == ""){
                    alert( "Username cannot be empty.");
                    document.FrmUser.txtUsername.focus();
                    return false;
                }
                if (document.FrmUser.txtRolename_ID.value == ""){
                    alert( "Role cannot be empty.");
                    document.FrmUser.txtRolename.focus();
                    return false;
                }
                if(!checkIsAlphaVar(document.FrmUser.txtUsername.value)) {
                    window.alert('Invalid character(s) in user name');
                    document.getElementById('txtUsername').focus();
                    return false;
                }
                return true;
            }
            function noChangePwdChange(){
                document.FrmUser.chkPwdChange.checked = true;
                return true;
            }
        </script>

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

    </head>
    <body>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgUser.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("UserName") != null) {
                    usr = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), request.getParameter("UserName"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                usr = new User();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgUser.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">User Administration</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmUser" name="FrmUser" method="POST" action="pgSavUser.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>User</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtUsername">Username</label></td>
                                <td class="field"><input type="text" name="txtUsername" title="Username"
                                                             id="txtUsername" size="12" maxlength="10"
                                                             value="<%=usr.getUsername()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>

                            <tr>
                                <td class="fLabel" width="125"><label for="txtRolename_ID">Role</label></td>
                                <td class="field">
                                    <input type="text" id="txtRolename_ID" name="txtRolename_ID" size="30" onKeyUp="ajax_showOptions('txtRolename_ID',this,'Role',event)"
                                           value="<%
        if (usr.getRole() != null) {
            out.write(usr.getRole().getRole_Name());
        } else {
            out.write("");
        }
                                           %>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %> />

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkPwdChange">Password Reset</label></td>
                                <td class="field"><input type="checkbox" name="chkPwdChange" id="chkPwdChange" class="chkbox" title="Password Reset"
                                                             <%
        if (saveFlag.equals("I")) {
            out.write("onclick=\"return noChangePwdChange();\" ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
        if (usr.getPwd_Change_Flag() == 1) {
            out.write("CHECKED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkLocked">Locked</label></td>
                                <td class="field"><input type="checkbox" name="chkLocked" id="chkLocked" class="chkbox" title="Locked"
                                                             <%
        if (usr.getIs_User_Locked() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgUserlist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>