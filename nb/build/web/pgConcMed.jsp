<!--
Form Id : CSD3
Date    : 22-01-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="java.text.SimpleDateFormat" 
import="bl.sapher.Conc_Medication"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Concomitant Medication - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <script src="libjs/ajax/ajax.js"></script>

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

        <script type="text/javascript" src="libjs/gui.js"></script>

        <script>
            var SAPHER_PARENT_NAME = "pgConcMed";

            function validateCMFields(){
                if(document.FrmConcMed.txtProdCode.value == '') {
                    window.alert('Please provide Product code for concomitant medication.');
                    return false;
                }
                return true;
            }

            function validate(){
                return true;
            }

        </script>
    </head>
    <body>
        <%!    Conc_Medication aCM = null;
    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
    private String strRepNum = "";
    private String strProdCode = "";
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        %>
        <%
        aCM = new Conc_Medication((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgConcMed.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }

        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; Invalid username/password.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            }
            if (!saveFlag.equals("I")) {
                if ((request.getParameter("RepNum") != null) && (request.getParameter("ProdCode") != null)) {
                    strRepNum = request.getParameter("RepNum");
                    strProdCode = request.getParameter("ProdCode");
                    aCM = new Conc_Medication((String) session.getAttribute("Company"), "y", Integer.parseInt(request.getParameter("RepNum")), request.getParameter("ProdCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                aCM = new Conc_Medication();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; RecordNotFoundException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='700' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Concomitant Medication</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmConcMed" name="FrmConcMed" method="POST" action="pgSavConcMed.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Concomitant Medication</td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtRepNum">Rep Num</label></td>
                                <td class="field"><input type="text" name="txtRepNum" title="Report Number"
                                                             id="txtRepNum" size="15" maxlength="9"
                                                             <%out.write(" value=\"" + strRepNum + "\"  readonly ");%>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtProdCode">Product Details</label></td>
                                <td class="field" colspan="9">
                                    <div id="divProdDynData">

                                        <input type="text" id="txtProdCode" name="txtProdCode" size="57" onKeyUp="ajax_showOptions('txtProdCode_ID',this,'Product',event)"
                                               value="<%=((aCM.getBrand_Nm() == null && aCM.getGeneric_Nm() == null) ? "" : (aCM.getBrand_Nm() + " (" + aCM.getGeneric_Nm() + ")"))%>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %>>
                                        <input type="text" readonly size="13" id="txtProdCode_ID" name="txtProdCode_ID"
                                               value="<%=(aCM.getProduct_Code() == null ? "" : aCM.getProduct_Code())%>">

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtIndicationNum">Indication Num</label></td>
                                <td class="field">

                                    <input type="text" id="txtIndicationNum" name="txtIndicationNum" size="30" onKeyUp="ajax_showOptions('txtIndicationNum_ID',this,'IndicationTreatment',event)"
                                           value="<%=(aCM.getIndication() == null ? "" : aCM.getIndication())%>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %>>
                                    <input type="text" readonly size="8" id="txtIndicationNum_ID" name="txtIndicationNum_ID"
                                           value="<%=(aCM.getIndication_Num() == null ? "" : aCM.getIndication_Num())%>">

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel">
                                    <label for="txtStartDate">Start date</label>
                                </td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtStartDate" id="txtStartDate" size="15" maxlength="11" title="Start date"
                                                       clear=202 value="<%=(aCM.getTreat_Start() == null ? "" : (sdf.format(aCM.getTreat_Start())))%>" readonly
                                                       <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 202);\" ");
        }%>
                                                   </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <%
        if (!saveFlag.equals("V")) {
                                                %>
                                                <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" title="Select Start date"></a>
                                                <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField : "txtStartDate", //*
                                                        ifFormat : "%d-%b-%Y",
                                                        showsTime : true,
                                                        button : "cal_trigger1", //*
                                                        step : 1
                                                    });
                                                </script>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel">
                                    <label for="txtEndDate">End date</label>
                                </td>
                                <td class="field">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtEndDate" id="txtEndDate" size="15" maxlength="11" title="End date"
                                                       clear=203 value="<%=(aCM.getTreat_Stop() == null ? "" : (sdf.format(aCM.getTreat_Stop())))%>" readonly
                                                       <% if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 203);\" ");
        }%>
                                                   </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <%
        if (!saveFlag.equals("V")) {
                                                %>
                                                <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" border='0' name="imgCal" title="Select End date"></a>
                                                <script type="text/javascript">
                                                    Calendar.setup({
                                                        inputField : "txtEndDate", //*
                                                        ifFormat : "%d-%b-%Y",
                                                        showsTime : true,
                                                        button : "cal_trigger2", //*
                                                        step : 1
                                                    });
                                                </script>
                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" onClick="return validateCMFields();" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" title="Save" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="hidden" id="RepNum" name="RepNum" value="<%=strRepNum%>">
                                    <input type="hidden" id="DiagNum" name="DiagNum" value="<%=strProdCode%>">
                                    <input type="reset" name="cmdNew" class="button" value="Cancel" title="Cancel the operation" style="width: 60px;"
                                           title="Cancel" onclick="javascript:location.replace('<%="pgCaseSummary.jsp?RepNum=" + strRepNum%>')"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>