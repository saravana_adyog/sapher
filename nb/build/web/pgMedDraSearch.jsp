<%-- 
    Document   : pgMEdDraSearch
    Created on : Oct 29, 2009, 11:41:06 AM
    Author     : Anoop Varma
--%>

<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="java.util.ArrayList"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.MedDRABrowser"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"
    import="bl.sapher.general.Constants"
    isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>MedDRA code - [Sapher]</title>

    <script>var SAPHER_PARENT_NAME = "";</script>
    <script src="libjs/gui.js"></script>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>

    <link rel="stylesheet" href="css/tab-view.css" type="text/css" media="screen">
    <script type="text/javascript" src="libjs/tab/ajax.js"></script>
    <script type="text/javascript" src="libjs/tab/tab-view.js"></script>

    <script type="text/javascript">
        function validateThisPage() {
            return true;
        }
    </script>

</head>

<body>

<%!    private final String FORM_ID = "CDL24"; // Body System code
    private String strSearchString = "";
    private String strSoc = "0";
    private String strHlgt = "0";
    private String strHlt = "0";
    private String strPt = "0";
    private String strLlt = "0";
    String[] strVals = null;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 100;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
%>

<%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgMedDRASearch.jsp");
        boolean resetSession = false;

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp: V=" + bView);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }

        if (request.getParameter("txtSrchKey") == null) {
            if (session.getAttribute("txtSrchKey") == null) {
                strSearchString = "";
            } else {
                strSearchString = (String) session.getAttribute("txtSrchKey");
            }
        } else {
            strSearchString = request.getParameter("txtSrchKey");
        }
        session.setAttribute("txtSrchKey", strSearchString);
        
        if (request.getParameter("chkSoc") == null) {
            if (session.getAttribute("chkSoc") == null) {
                strSoc = "0";
            } else {
                strSoc = (String)session.getAttribute("chkSoc");
            }
        } else {
            strSoc = "1";
        }
        session.setAttribute("chkSoc", strSoc);

        if (request.getParameter("chkHlgt") == null) {
            if (session.getAttribute("chkHlgt") == null) {
                strHlgt = "0";
            } else {
                strHlgt = (String)session.getAttribute("chkHlgt");
            }
        } else {
            strHlgt = "1";
        }
        session.setAttribute("chkHlgt", strHlgt);

        if (request.getParameter("chkHlt") == null) {
            if (session.getAttribute("chkHlt") == null) {
                strHlt = "0";
            } else {
                strHlt = (String)session.getAttribute("chkHlt");
            }
        } else {
            strHlt = "1";
        }
        session.setAttribute("chkHlt", strHlt);

        if (request.getParameter("chkPt") == null) {
            if (session.getAttribute("chkPt") == null) {
                strPt = "0";
            } else {
                strPt = (String)session.getAttribute("chkPt");
            }
        } else {
            strPt = "1";
        }
        session.setAttribute("chkPt", strPt);
        
        if (request.getParameter("chkLlt") == null) {
            if (session.getAttribute("chkLlt") == null) {
                strLlt = "0";
            } else {
                strLlt = (String)session.getAttribute("chkLlt");
            }
        } else {
            strLlt = "1";
        }
        session.setAttribute("chkLlt", strLlt);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strSearchString + "," + strSoc + strHlgt + strHlt + strPt + strLlt);
        System.out.println("AV> Loading list with val:" + strSearchString + "," + strSoc + strHlgt + strHlt + strPt + strLlt);
        al = MedDRABrowser.search(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"),
                strSoc + strHlgt + strHlt + strPt + strLlt, strSearchString);

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">MedDRA code search</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <form name="FrmMedDRA codeList" METHOD="POST" ACTION="pgMedDraSearch.jsp" >
        <td class="formbody">
            <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Criteria</td>
            </tr>
            <tr>
                <td style="height: 10px;"></td>
            </tr>
            <!--
        <tr>
            <td class="fLabel" width="125"><label for="txtMedDRA codeCode">MedDRA code</label></td>
            <td class="field">
                <input type="text" name="txtMedDRA codeCode" id="txtMedDRA codeCode" size="3" maxlength="2" title="MedDRA code Code"
                       value="<%--=strCtryId--%>">
            </td>
            <td width="500" align="right"><b><%--=strMessageInfo--%></b></td>
        </tr>
        -->
            <tr>
                <td class="fLabel" width="125"><label for="cmbSrchType code">Condition</label></td>
                <td class="field" colspan="2">
                <span style="float: left">
                    <SELECT id="cmbCondition" NAME="cmbCondition" class="comboclass" style="width: 100px;" title="Status">
                        <option value="0">Begins with</option>
                        <option value="1">Contains</option>
                        <option value="2">Ends with</option>
                    </SELECT>
                </span>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label for="txtSrchKey">MedDRA Description</label></td>
                <td class="field">
                    <input type="text" name="txtSrchKey" id="txtSrchKey" size="30" maxlength="50" title="Search Key"
                           value="<%=strSearchString%>">
                </td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label for="chkValid">Search in</label></td>
                <td class="field">
                    <table border="0">
                        <tr>
                            <td>SOC</td>
                            <td><input type="checkbox" name="chkSoc" id="chkSoc" title="SOC" class="chkbox"
                                           <%if (strSoc.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }%></td>
                            <td>HLGT</td>
                            <td><input type="checkbox" name="chkHlgt" id="chkHlgt" title="HLGT" class="chkbox"
                                           <%if (strHlgt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }%>></td>
                            <td>HLT</td>
                            <td><input type="checkbox" name="chkHlt" id="chkHlt" title="HLT" class="chkbox"
                                           <%if (strHlt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }%>></td>
                            <td>PT</td>
                            <td><input type="checkbox" name="chkPt" id="chkPt" title="PT" class="chkbox"
                                           <%if (strPt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }%>></td>
                            <td>LLT</td>
                            <td><input type="checkbox" name="chkLlt" id="chkLlt" title="LLT" class="chkbox"
                                           <%if (strLlt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }%>></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td>
                <td colspan="2">
                    <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;"
                           onclick="return validateThisPage();">
                    <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                           onclick="location.replace('pgNavigate.jsp?Cancellation=1&Target=pgMedDraSearch.jsp');"/>
                </td>
            </td>

        </td>
    </form>

    </td>
</tr>
</table>
</td>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {%>
<tr>
    <!--___________________________________-->

    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
        <tr class="grid_header">
            <td width="16"></td>

            <td width="100">Body System Number</td>
            <td>Body System (SOC)</td>
            <td width="100">Status</td>
            <td></td>
        </tr>
        <%
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         strVals = ((String) al.get(i)).split("\\|");
        %>
        <tr class="
            <%
            if (i % 2 == 0) {
                out.write("even_row");
            } else {
                out.write("odd_row");
            }
            %>
            ">

        <%switch (Integer.parseInt(strVals[0])) {
                case 1:%>
        <td width="16" align="center" >
            <a href="pgMedDraSoc.jsp?HiLit=<%=strVals[11]%>">
                <img src="images/icons/16x16-SOC.gif" title="View details of the entry with ID = <%=strVals[11]%>" border='0'>
            </a>
        </td>
        <td width="100" align='center'>
            <%=(strVals[11].equalsIgnoreCase("null")?"":strVals[11])%>
        </td>
        <td align='left'>
            <%=(strVals[12].equalsIgnoreCase("null")?"":strVals[12])%>
        </td>
        <td width="100" align='center'>

        </td>
        <td></td>
        <%break;
    case 2:%>
        <td width="16" align="center" >
            <a href="pgMedDraHlgt.jsp?HiLit=<%=strVals[11]%>&SOCID=<%=strVals[1]%>&SOC=<%=strVals[2]%>">
                <img src="images/icons/16x16-HLGT.gif" title="View details of the entry with ID = <%=strVals[11]%>" border='0'>
            </a>
        </td>
        <td width="100" align='center'>
            <%=(strVals[11].equalsIgnoreCase("null")?"":strVals[11])%>
        </td>
        <td align='left'>
            <%=(strVals[12].equalsIgnoreCase("null")?"":strVals[12])%>
        </td>
        <td width="100" align='center'>

        </td>
        <td></td>
        <%break;
    case 3:%>
        <td width="16" align="center" >
            <a href="pgMedDraHlt.jsp?HiLit=<%=strVals[11]%>&SOCID=<%=strVals[1]%>&SOC=<%=strVals[2]%>&HLGTID=<%=strVals[3]%>&HLGT=<%=strVals[4]%>">
                <img src="images/icons/16x16-HLT.gif" title="View details of the entry with ID = <%=strVals[11]%>" border='0'>
            </a>
        </td>
        <td width="100" align='center'>
            <%=(strVals[11].equalsIgnoreCase("null")?"":strVals[11])%>
        </td>
        <td align='left'>
            <%=(strVals[12].equalsIgnoreCase("null")?"":strVals[12])%>
        </td>
        <td width="100" align='center'>

        </td>
        <td></td>
        <%break;
    case 4:%>
        <td width="16" align="center" >
            <a href="pgMedDraPt.jsp?HiLit=<%=strVals[11]%>&SOCID=<%=strVals[1]%>&SOC=<%=strVals[2]%>&HLGTID=<%=strVals[3]%>&HLGT=<%=strVals[4]%>&HLTID=<%=strVals[5]%>&HLT=<%=strVals[6]%>">
                <img src="images/icons/16x16-PT-1.gif" title="View details of the entry with ID = <%=strVals[11]%>" border='0'>
            </a>
        </td>
        <td width="100" align='center'>
            <%=(strVals[11].equalsIgnoreCase("null")?"":strVals[11])%>
        </td>
        <td align='left'>
            <%=(strVals[12].equalsIgnoreCase("null")?"":strVals[12])%>
        </td>
        <td width="100" align='center'>

        </td>
        <td></td>
        <%break;
    case 5:%>
        <td width="16" align="center" >
            <a href="pgMedDraLlt.jsp?HiLit=<%=strVals[11]%>&SOCID=<%=strVals[1]%>&SOC=<%=strVals[2]%>&HLGTID=<%=strVals[3]%>&HLGT=<%=strVals[4]%>&HLTID=<%=strVals[5]%>&HLT=<%=strVals[6]%>&PTID=<%=strVals[7]%>&PT=<%=strVals[8]%>">
                <img src="images/icons/16x16-LLT-1.gif" title="View details of the entry with ID = <%=strVals[11]%>" border='0'>
            </a>
        </td>
        <td width="100" align='center'>
            <%=(strVals[11].equalsIgnoreCase("null")?"":strVals[11])%>
        </td>
        <td align='left'>
            <%=(strVals[12].equalsIgnoreCase("null")?"":strVals[12])%>
        </td>
        <td width="100" align='center'>

        </td>
        <td></td>
        <%break;
            }%>
    </td>
</tr>
<% }%>
</table>
</td>
</tr>
<tr>
    <!-- Pagination Starts -->
    <tr>
        <td class="formbody">
            <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                <tr>
                    <td>
                        <a href="pgMedDraSearch.jsp?CurPage=1">
                        <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                    </td>
                    <td><% if (nCurPage > 1) {%>
                        <a href="pgMedDraSearch.jsp?CurPage=<%=(nCurPage - 1)%>">
                        <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></a>
                        <% } else {%>
                        <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                        <% }%>
                    </td>
                    <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                    <td><% if (nCurPage < nPageCount) {%>
                        <a href="pgMedDraSearch.jsp?CurPage=<%=nCurPage + 1%>">
                        <img src="images/icons/page-next.gif" title="Go to next page" border="0"></a>
                        <% } else {%>
                        <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                        <% }%>
                    </td>
                    <td>
                        <a href="pgMedDraSearch.jsp?CurPage=<%=nPageCount%>">
                        <img src="images/icons/page-last.gif" title="Go to last page" border="0"></a>
                    </td>
                    <td width="100%"></td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- Pagination Ends -->
</tr>
<!--___________________________________-->


<tr></tr>
<tr></tr>
</tr>
<!-- Grid View Ends -->

<tr>
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>

</body>
</html>