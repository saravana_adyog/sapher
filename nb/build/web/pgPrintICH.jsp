<%-- 
    Document   : pgPrintICH
    Created on : Nov 17, 2009, 7:36:32 PM
    Author     : Anoop
--%>

<%@page
    contentType="text/html"
    import="bl.sapher.Reports_Listing"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.general.GenericException"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"
    import="java.text.SimpleDateFormat"
    pageEncoding="UTF-8"
    %>

<html>
    <head>
        <script type="text/javascript" src="libjs/gui.js"></script>

        <%!    private final String FORM_ID = "REP2";
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
    private String strProdCode;
    private String strProdDesc;
    private String strDtFrom;
    private String strDtTo;
    private int nStatus = -1;
    java.text.SimpleDateFormat sdf;
        %>

    </head>
    <body>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPrintCIOMS2.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp: V=" + bView);

            if (!bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; No permission." + "[View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; RecordNotFoundException.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }

        if (request.getParameter("txtProdCode_ID") != null) {
            strProdCode = request.getParameter("txtProdCode_ID");
        } else {
            strProdCode = "";
        }

        if (request.getParameter("txtProdCode") != null) {
            strProdDesc = request.getParameter("txtProdCode");
        } else {
            strProdDesc = "";
        }

        sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        if (request.getParameter("txtCaseDtFrom") != null) {
            strDtFrom = (String) request.getParameter("txtCaseDtFrom");
        } else {
            strDtFrom = "";
        }

        if (request.getParameter("txtCaseDtTo") != null) {
            strDtTo = (String) request.getParameter("txtCaseDtTo");
        } else {
            strDtTo = "";
        }

        String strSuspended = "";
        if (request.getParameter("cmbSuspendedCase") != null) {
            strSuspended = request.getParameter("cmbSuspendedCase");
        }

        if (strSuspended.equals("Valid")) {
            nStatus = 0;
        } else if (strSuspended.equals("Suspended")) {
            nStatus = 1;
        } else if (strSuspended.equals("All")) {
            nStatus = -1;
        }

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Report created.");
        %>
        <script type="text/javascript">
            popupWnd('_blank', 'yes','<%out.write("Reports/" +
                Reports_Listing.printPdf(bl.sapher.Reports_Listing.REP_ICH,
                (String) session.getAttribute("Company"),
                application.getRealPath("/Reports"),
                strProdCode, strProdDesc, strDtFrom, strDtTo, nStatus));%>',800,600);
            document.location.replace("pgRep.jsp?ReportType=ICH");
        </script>
    </body>
</html>
