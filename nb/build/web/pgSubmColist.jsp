<!--
Form Id : CDL23
Date    : 13-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.SubmCompany"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Submission Company - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "pgSubmColist";</script>

    <script src="libjs/gui.js"></script>
    <script src="libjs/ajax/ajax.js"></script>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>

    <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
    <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

</head>

<body onUnload="nCntryPopupCount = 0;">
<script>
    var nCntryPopupCount = 0;
</script>
<%!    private final String FORM_ID = "CDL23";
    private String strCoName = "";
    private String strSCoCountryId = "";
    private String strSCoCountryNm = "";
    private int nSCoValid = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private SubmCompany aSCompany = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
%>

<%
        aSCompany = new SubmCompany((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgSubmColist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! SubmCompany reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Updation Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmColist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmColist.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmColist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmColist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmColist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgSubmCo.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmColist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }
        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtSCoName") == null) {
            if (session.getAttribute("txtSCoName") == null) {
                strCoName = "";
            } else {
                strCoName = (String) session.getAttribute("txtSCoName");
            }
        } else {
            strCoName = request.getParameter("txtSCoName");
        }
        session.setAttribute("txtSCoName", strCoName);

        if (request.getParameter("txtSCoCountryId_ID") == null) {
            if (session.getAttribute("txtSCoCountryId_ID") == null) {
                strSCoCountryId = "";
            } else {
                strSCoCountryId = (String) session.getAttribute("txtSCoCountryId_ID");
            }
        } else {
            strSCoCountryId = request.getParameter("txtSCoCountryId_ID");
        }
        session.setAttribute("txtSCoCountryId_ID", strSCoCountryId);

        if (request.getParameter("txtSCoCountryId") == null) {
            if (session.getAttribute("txtSCoCountryId") == null) {
                strSCoCountryNm = "";
            } else {
                strSCoCountryNm = (String) session.getAttribute("txtSCoCountryId");
            }
        } else {
            strSCoCountryNm = request.getParameter("txtSCoCountryId");
        }
        session.setAttribute("txtSCoCountryId", strSCoCountryNm);

        String strValid = "";
        if (request.getParameter("cmbValidSCo") == null) {
            if (session.getAttribute("cmbValidSCo") == null) {
                strValid = "All";
            } else {
                strValid = (String) session.getAttribute("cmbValidSCo");
            }
        } else {
            strValid = request.getParameter("cmbValidSCo");
        }
        session.setAttribute("cmbValidSCo", strValid);

        if (strValid.equals("Active")) {
            nSCoValid = 1;
        } else if (strValid.equals("Inactive")) {
            nSCoValid = 0;
        } else if (strValid.equals("All")) {
            nSCoValid = -1;
        }

        if (resetSession) {
            strCoName = "";
            strSCoCountryId = "";
            strSCoCountryNm = "";
            nSCoValid = -1;
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = SubmCompany.listSubmCompany(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), "", "", -1);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strCoName + "," + strSCoCountryId + "," + nSCoValid);
            al = SubmCompany.listSubmCompany(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), strCoName, strSCoCountryId, nSCoValid);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">Code List - Submission Company Listing</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <FORM name="FrmSColist" METHOD="POST" ACTION="pgSubmColist.jsp" >
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Criteria</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtSCoName">Company</label></td>
            <td class="field">
                <input type="text" name="txtSCoName" id="txtSCoName" size="30" maxlength="50" title="Company"
                       value="<%=strCoName%>">
            </td>
            <td width="500" align="right"><b><%=strMessageInfo%></b></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtSCoCountryId">Country</label></td>
            <td class="field"  width="300">

                <input type="text" id="txtSCoCountryId" name="txtSCoCountryId" size="24"
                value="<%=strSCoCountryNm%>" onKeyUp="ajax_showOptions('txtSCoCountryId_ID',this,'Country',event)"
                       <%
        if (!bView) {
            out.write(" readonly ");
        }
                       %> />
                <input type="text" readonly size="3" id="txtSCoCountryId_ID" name="txtSCoCountryId_ID"
                value="<%=strSCoCountryId%>"/>
            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="cmbValidSCo">Status</label></td>
            <td class="field" colspan="2">
                <span style="float: left">
                    <select id="cmbValidSCo" NAME="cmbValidSCo" class="comboclass" style="width: 70px;" title="Status">
                        <option <%=(strValid.equals("All") ? "SELECTED" : "")%>>All</option>
                        <option <%=(strValid.equals("Active") ? "SELECTED" : "")%>>Active</option>
                        <option <%=(strValid.equals("Inactive") ? "SELECTED" : "")%>>Inactive</option>
                    </select>

                </span>
            </td>
        </tr>
        <tr>
        <td>
            <td colspan="2">
                <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                       onclick="location.replace('pgNavigate.jsp?Target=pgSubmColist.jsp');"/>
            </td>
        </td>
    </form>
    <td width="16" align="right" >
        <a href="#" title="Print"
           <%
        if (al.size() > 0) {
            session.setAttribute("SubmCoPrintData", al);
            out.write("onclick=\"popupWnd('_blank', 'yes','pgPrintSubmCo.jsp?cod=" + strCoName +
                    "&desc=" + strSCoCountryNm + "&valid=" + nSCoValid + "',800,600)\"");
        }
           %>>
            <img src="images/icons/print.gif" border='0'>
        </a>
    </td>
    <td>
        <form METHOD="POST" ACTION="pgSubmCo.jsp">
            <span style="float: right">
                <input type="hidden" name="SaveFlag" value="I">
                <input type="hidden" name="Parent" value="1">
                <input type="submit" name="cmdNew"  title="Adds a new entry"  class="<%=(bAdd ? "button" : "button disabled")%>"
                       value="New Company" style="width: 110px;" <%=(bAdd ? "" : " DISABLED")%>>
            </span>
        </form>
    </td>
    </td>
</tr>
</table>
</td>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <% if (bDelete) {%>
                <td width="16"></td>
                <% }%>
                <td>Submission Company</td>
                <td width="100">Country</td>
                <td width="100">Status</td>
                <td></td>
            </tr>
            <%
     aSCompany = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aSCompany = ((SubmCompany) al.get(i));
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <% if (bEdit) {%>
                <td width="16" align="center" >
                    <a href="pgSubmCo.jsp?SubmCo=<% out.write(aSCompany.getCompany_Nm());%>&SaveFlag=U&Parent=1">
                        <img src="images/icons/edit.gif" title="Edit the entry with ID = <%=aSCompany.getCompany_Nm()%>" border='0'>
                    </a>
                </td>
                <% } else {%>
                <td width="16" align="center" >
                    <a href="pgSubmCo.jsp?SubmCo=<% out.write(aSCompany.getCompany_Nm());%>&SaveFlag=V&Parent=1">
                        <img src="images/icons/view.gif" title="View details of the entry with ID = <%=aSCompany.getCompany_Nm()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <% if (bDelete) {%>
                <td width="16" align="center" >
                    <a href="pgDelSubmCo.jsp?SubmCo=<% out.write(aSCompany.getCompany_Nm());%>"
                       onclick="return confirm('Are you sure you want to delete?','Sapher')">
                        <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=aSCompany.getCompany_Nm()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <td align='center'>
                    <%
                if (aSCompany.getIs_Valid() == 0) {
                    %>
                    <font color="red"> <% out.write(aSCompany.getCompany_Nm());%> </font>
                    <%
                } else {
                    out.write(aSCompany.getCompany_Nm());
                }
                    %>
                </td>
                <td width="100" align='left'>
                    <%
                if (aSCompany.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(aSCompany.getCtry().getCountry_Nm());%></font>
                    <%
                } else {
                    out.write(aSCompany.getCtry().getCountry_Nm());
                }
                    %>
                </td>
                <td width="100" align='center'>
                    <%
                if (aSCompany.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write("Inactive");%></font>
                    <%
                } else {
                    out.write("Active");
                }
                    %>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgSubmColist.jsp?CurPage=1">
                    <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <A href="pgSubmColist.jsp?CurPage=<%=(nCurPage - 1)%>">
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></A>
                    <% } else {%>
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <A href="pgSubmColist.jsp?CurPage=<%=nCurPage + 1%>">
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0"></A>
                    <% } else {%>
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                    <% }%>
                </td>
                <td>
                    <a href="pgSubmColist.jsp?CurPage=<%=nPageCount%>">
                    <img src="images/icons/page-last.gif" title="Go to last page" border="0"></A>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>