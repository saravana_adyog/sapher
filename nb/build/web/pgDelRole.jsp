<!--
Form Id : USM2
Date    : 12-12-2007.
Author  : Arun P. Jose
-->

<%@page
contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.Role"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.DBConnectionException"
import="bl.sapher.general.ConstraintViolationException"
import="bl.sapher.general.GenericException"
import="bl.sapher.general.Logger"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>

        <%!    private final String FORM_ID = "USM2";
    private boolean bDelete;
    private Inventory inv = null;
    private User currentUser = null;
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDelRole.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
        bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');

        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp: bDelete=" + bDelete);

        Role role = null;
        try {
            if (request.getParameter("RoleName") != null) {
                role = new Role((String) session.getAttribute("Company"), request.getParameter("RoleName"));
                if (bDelete) {
                    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp: Deleting the entry with ID=" + request.getParameter("RoleName"));
                    role.deleteRole((String) session.getAttribute("Company"));
                    pageContext.forward("pgRolelist.jsp?DelStatus=0");
                } else {
                    throw new ConstraintViolationException("2");
                }
            }
        } catch (ConstraintViolationException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgRolelist.jsp?DelStatus=" + ex.getMessage());
        } catch (DBConnectionException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgRolelist.jsp?DelStatus=3");
        } catch (GenericException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("pgRolelist.jsp?DelStatus=4");
        } catch (RecordNotFoundException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDelRole.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());

            pageContext.forward("content.jsp?Status=0");
        }
        %>
    </body>
</html>
