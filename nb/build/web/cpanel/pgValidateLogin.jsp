<!--
Form Id : USM4.2
Date    : 11-12-2007.
Author  : Arun P. Jose
-->

<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.general.DBCon"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.UserBlockedException"
    isThreadSafe="true"
    %>

<%!
    private DBCon dbCon = null;
%>

<%
            try {
                dbCon = new DBCon(request.getParameter("txtUsername"), request.getParameter("txtPassword"));
                session.setAttribute("cpanelUser", request.getParameter("txtUsername"));
                session.setAttribute("cpanelPwd", request.getParameter("txtPassword"));
                pageContext.forward("pgSapher.jsp");
            } catch (Exception e) {
                if (bl.sapher.general.GenConf.isDebugMode()) {
                    e.printStackTrace();
                    e.getMessage();
                }
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
            }
%>