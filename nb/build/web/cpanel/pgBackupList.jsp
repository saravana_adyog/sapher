<!--
Form Id : CPL1
Date    : 18-3-2008
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.CPanel"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.ClientConf"
import="java.util.ArrayList"
import="java.text.DateFormatSymbols"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Backup - [Sapher]</title>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "../css/master.css";</style>
    <!--Calendar Library Includes.. S => -->
    <link rel="stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/aqua/theme.css" title="Aqua" />
    <link rel="alternate stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/calendar-green.css" title="green" />
    <!-- import the calendar script -->
    <script type="text/javascript" src="../libjs/calendar/calendar.js"></script>
    <!-- import the language module -->
    <script type="text/javascript" src="../libjs/calendar/lang/calendar-en.js"></script>
    <!-- helper script that uses the calendar -->
    <script type="text/javascript" src="../libjs/calendar/calendar-helper.js"></script>
    <script type="text/javascript" src="../libjs/calendar/calendar-setup.js"></script>
    <!--Calendar Library Includes.. E <= -->

    <script src="../libjs/ajax/ajax.js" type="text/javascript"></script>
    <script src="../libjs/jquery/jquery.js" type="text/javascript"></script>
    <script src="../libjs/jquery/query.ui.draggable.js" type="text/javascript"></script>
    <!-- Core files -->
    <script src="../libjs/jquery/AlertBox/jquery.alerts.js" type="text/javascript"></script>
    <link href="../libjs/jquery/AlertBox/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />

    <script>
        $(document).ready( function() {
            var text='';
            $("#dyncompo > div").each(function (i) {
                text +='<option value="'+ $(this).html() +'">'+ $(this).html() +'</option>';
            });

            $("#cmdBackup").click( function() {
                jPrompt('Select a Client: ', text, 'Sapher', function(r) {
                    if( r ) {
                        waitPreloadPage();
                        var http = createRequestObject();
                        http.open('GET','pgBackup.jsp?clientName=' + r,true);
                        http.onreadystatechange = function() {if(http.readyState == 4){
                                var resp = http.responseText;
                                window.location.href='pgBackupList.jsp?BkpStatus=' + resp;}}
                        http.send(null);
                    }
                });
            });

        });

        <!-- PreLoad Wait - Script -->
        <!-- This script and more from http://www.rainbow.arch.scriptmania.com
        function waitPreloadPage() { //DOM
            if (document.getElementById){
                document.getElementById('prepage').style.display='block';
            }
        }

        function popupSearchWnd(urlToOpen,window_width,window_height)
        {
            var window_left = (screen.availWidth/2)-(window_width/2);
            var window_top = (screen.availHeight/2)-(window_height/2);
            var winParms = "Status=no" + ",resizable=no" + ",scrollbars=yes" + ",height="+window_height+",width="+window_width + ",left="+window_left+",top="+window_top;
            var newwindow = window.open(urlToOpen,'_blank',winParms);

            if(!newwindow.opener)
                newwindow.opener = self;

            newwindow.focus()
        }

        function validate(){
            return true;
        }

        function kyFrom(e){
            var keynum;
            if(window.event) // IE
            {
                keynum = e.keyCode;
            }
            else if(e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
            }
            if (keynum == 27)
                document.FrmBkpList.txtBkpDtFrom.value = "";
        }
        function kyTo(e){
            var keynum;
            if(window.event) // IE
            {
                keynum = e.keyCode;
            }
            else if(e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
            }
            if (keynum == 27)
                document.FrmBkpList.txtBkpDtTo.value = "";
        }
    </script>
</head>

<body>

<%!    private final String FORM_ID = "CPL1";
    private ArrayList al = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private String strMessageInfo = "";
    private String strVal = "";
    private String strDtFrom = "";
    private String strDtTo = "";
%>

<%
        strMessageInfo = "";
        if (request.getParameter("BkpStatus") != null) {
            if (((String) request.getParameter("BkpStatus")).equals("0")) {
                strMessageInfo = "Backup Success.";
            } else if (((String) request.getParameter("BkpStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Backup Failed! Please investigate.</font>";
            } else if (((String) request.getParameter("BkpStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Backup Failed! A backup/Restore operation already running. Try later.</font>";
            }
        }

        if (request.getParameter("txtBkpDtFrom") == null) {
            if (session.getAttribute("txtBkpDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtBkpDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtBkpDtFrom");
        }
        session.setAttribute("txtBkpDtFrom", strDtFrom);

        if (request.getParameter("txtBkpDtTo") == null) {
            if (session.getAttribute("txtBkpDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtBkpDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtBkpDtTo");
        }
        session.setAttribute("txtBkpDtTo", strDtTo);

        String strUN = "";
        String strPW = "";

        strUN = ((String) session.getAttribute("cpanelUser"));
        strPW = ((String) session.getAttribute("cpanelPwd"));

        try {
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            al = CPanel.lstFiles(strUN, strPW, "DMP", strDtFrom, strDtTo);
        } catch (TimeoutException toe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%        }
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">Backup</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <form name="FrmBkpList" METHOD="POST" ACTION="pgBackupList.jsp" >
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Backup</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtBkpDtFrom">Date Range</label></td>
            <td class="field">
                <table border='0' cellspacing='0' cellpadding='0'>
                    <tr>
                        <td style="padding-right: 3px; padding-left: 0px;">
                            <input type="text" name="txtBkpDtFrom" id="txtBkpDtFrom" size="18" maxlength="18" title="From Date"
                                   value="<%=strDtFrom%>" readonly  onkeydown="return kyFrom(event);">
                        </td>
                        <td style="padding-right: 3px; padding-left: 0px;">
                        <a href="#" id="cal_trigger1"><img src="../images/icons/cal.gif" border='0' alt="Select" title="Select Date" name="imgCal"></a></td>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtBkpDtFrom",
                                ifFormat : "%d-%b-%Y %H:%M",
                                showsTime : true,
                                button : "cal_trigger1",
                                step : 1
                            });
                        </script>

                        <td class="fLabel" style="padding-right: 3px; padding-left: 3px;">TO</td>
                        <td style="padding-right: 3px; padding-left: 0px;">
                            <input type="text" name="txtBkpDtTo" id="txtBkpDtTo" size="18" maxlength="18" title="To Date"
                                   value="<%=strDtTo%>" readonly  onkeydown="return kyTo(event);">
                        </td>
                        <td style="padding-right: 3px; padding-left: 0px;">
                        <a href="#" id="cal_trigger2"><img src="../images/icons/cal.gif" alt="Select" title="Select Date" border='0' name="imgCal"></a></td>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtBkpDtTo",
                                ifFormat : "%d-%b-%Y %H:%M",
                                showsTime : true,
                                button : "cal_trigger2",
                                step : 1
                            });
                        </script>
                    </tr>
                </table>
            </td>
            <td colspan="3" width="600" align="right" valign="top">
                <b><%=strMessageInfo%></b>
                <span  id="prepage" style="display:none; float:right;">
                    Processing, Please wait...
                    <img src="../images/indicator.gif" />
                </span>
            </td>
        </tr>
        

        <tr>
        <td class="fLabel" width="125"></td>
        <td class="field" colspan="1">
        <input type="submit" name="cmdSearch" class="button" value="Search" title="Search with specified criteria" style="width: 87px;">
    </form>
    </td>
    <td>
        <form METHOD="POST" ACTION="" >
            <span style="float: right">
                <input type="button" name="cmdBackup" id="cmdBackup" class="button"
                       value="Backup Now" style="width: 85px;">
            </span>
            <div id="dyncompo" style="display:none; border:1px;" >
                <%
        java.util.ArrayList alClientslist = ClientConf.getClients();
        for (int i = 0; i < alClientslist.size(); i++) {
            out.write("<div>" + ((ClientConf) alClientslist.get(i)).getClientName() + "</div>");
        }
        alClientslist = null;
                %>
            </div>
        </form>
    </td>
</tr>
</table>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " file(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() == 0) {
            return;
        }
%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <td width="150">Schema</td>
                <td width="100">Backup Date</td>
                <td width="20">Time</td>
                <td width="300">File Name</td>
                <td></td>
            </tr>
            <%
        nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
        if (nCurPage > nPageCount) {
            nCurPage = 1;
        }
        int nStartPos = ((nCurPage - 1) * nRowCount);
        for (int i = nStartPos;
                i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
                i++) {
            strVal = ((String) al.get(i));
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <td width="16" align="center" >
                    <a href="#" onclick="popupSearchWnd('pgLogFile.jsp?LogFile=<%=strVal.replace("DMP", "LOG")%>',800,600)">
                        <img src="../images/icons/edit.gif" border='0'>
                    </a>
                </td>
                <td width="150" align='center'>
                    <%
                out.write(strVal.substring(15, strVal.length() - 4));
                    %>
                </td>
                <td width="100" align='center'>
                    <%
                out.write(strVal.substring(6, 8) + "-" + new DateFormatSymbols().getShortMonths()[Integer.parseInt(strVal.substring(4, 6)) - 1] + "-" + strVal.substring(0, 4));
                    %>
                </td>
                <td width="20" align='left' >
                    <%
                out.write(strVal.substring(8, 10) + ":" + strVal.substring(10, 12));
                    %>
                </td>
                <td width="300" align='center'>
                    <%=strVal%>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgBackupList.jsp?CurPage=1">
                    <img src="../images/icons/page-first.gif" border="0"></a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <A href="pgBackupList.jsp?CurPage=<%=(nCurPage - 1)%>">
                    <img src="../images/icons/page-prev.gif" border="0"></A>
                    <% } else {%>
                    <img src="../images/icons/page-prev.gif" border="0">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <A href="pgBackupList.jsp?CurPage=<%=nCurPage + 1%>">
                    <img src="../images/icons/page-next.gif" border="0"></A>
                    <% } else {%>
                    <img src="../images/icons/page-next.gif" border="0">
                    <% }%>
                </td>
                <td>
                    <a href="pgBackupList.jsp?CurPage=<%=nPageCount%>">
                        <img src="../images/icons/page-last.gif" border="0">
                    </a>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<tr>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>