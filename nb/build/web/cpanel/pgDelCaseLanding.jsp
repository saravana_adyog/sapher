<%-- 
    Document   : pgDelCaseLanding
    Created on : Oct 6, 2009, 4:14:29 PM
    Author     : Anoop Varma
--%>

<%@page
    contentType="text/html"
    pageEncoding="windows-1252"
    import="bl.sapher.general.ClientConf"
    import="bl.sapher.general.GenConf"
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <script type="text/javascript" src="libjs/gui.js"></script>
        <title></title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "../css/master.css";</style>

        <script src="../libjs/jquery/jquery.js" type="text/javascript"></script>
        <script src="../libjs/jquery/query.ui.draggable.js" type="text/javascript"></script>
        <!-- Core files -->
        <script src="../libjs/jquery/AlertBox/jquery.alerts.js" type="text/javascript"></script>
        <link href="../libjs/jquery/AlertBox/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />

        <script>
            function proceedToDelCase() {
                var resp = confirm('Are you sure to Delete all Cases?','Sapher')
                if(resp) {
                    window.location='pgDelCases.jsp?clientName=' + document.getElementById("cmbClientName").value;
                }
            }
        </script>
    </head>
    <body>

        <div style="height: 15px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Delete all Cases</td>
            </tr>
            <!-- Form Body Starts -->
            <form name="FrmClearDB" METHOD="POST" ACTION="" >
                <tr>
                    <td class="formbody">
                        <span>
                            Client&nbsp;&nbsp;&nbsp;&nbsp;
                        </span>
                        <span>
                            <select name="cmbClientName" id="cmbClientName">
                                <%
        java.util.ArrayList alClientslist = ClientConf.getClients();
        for (int i = 0; i < alClientslist.size(); i++) {
            out.write("<option>" + ((ClientConf) alClientslist.get(i)).getClientName() + "</option>");
        }
        alClientslist = null;
                                %>
                            </select>
                        </span>
                        <span style="float: right">
                            <input type="button" name="cmdDelCase" id="cmdDelCase" class="button" value="Delete all Cases"
                                   style="width: 100px;"
                                   onclick="return proceedToDelCase();"/>
                        </span>
                    </td>
                </tr>
            </form>
            <!-- Form Body Ends -->
        </table>
        <div style="height: 25px;"></div>

    </body>
    <script type="text/javascript">

    </script>
</html>