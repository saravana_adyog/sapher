<!--
Form Id : CPL7
Date    : 11-2-2009
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.BackupSchedule"
import="bl.sapher.general.TimeoutException"
import="java.util.ArrayList"
import="java.text.DateFormatSymbols"
import="java.text.SimpleDateFormat"
import="java.util.Calendar"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Backup Schedule - [Sapher]</title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "../css/master.css";</style>
        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="../libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="../libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="../libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="../libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->
    </head>

    <body>
        <%!    private final String FORM_ID = "CPL7";
    private ArrayList<BackupSchedule> al = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private String strMessageInfo = "";
    private BackupSchedule bkpSch = null;
    private String freq = "";
    private int interval = -1;
    private String days = "";
    private int index = -1;
        %>

        <%
        strMessageInfo = "";
        if (request.getParameter("SavStatus") != null) {
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Update Failed! " +  (String) request.getParameter("Msg") + " </font>";
            }
        }

        if (request.getParameter("DelStatus") != null) {
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Delete Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Please investigate.</font>";
            }
        }

        String strUN = "";
        String strPW = "";

        strUN = ((String) session.getAttribute("cpanelUser"));
        strPW = ((String) session.getAttribute("cpanelPwd"));

        try {
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            al = BackupSchedule.listBkpSchedule(strUN, strPW);
        } catch (TimeoutException toe) {
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%        }
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Backup Schedule</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form name="FrmBkpScheduleList" METHOD="POST" ACTION="pgBkpSchedule.jsp?saveFlag=I" >
                    <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td>
                                <input type="submit" name="cmdCreateSchedule" class="button" value="Create New Schedule">
                            </td>
                            <td width="500" align="right"><b><%=strMessageInfo%></b></td>
                        </tr>
                    </table>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 1px;"></td>
            </tr>
            <!-- Grid View Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td class="form_group_title" colspan="10">
                                <%=al.size() + " record(s) found"%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% if (al.size() == 0) {
            return;
        }
            %>
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                        <tr class="grid_header">
                            <td width="10"></td>
                            <td width="10"></td>
                            <td width="50">Schema</td>
                            <td width="200">Schedule Name</td>
                            <td width="40">Schedule Start</td>
                            <td width="40">Schedule End</td>
                            <td width="40">Schedule Time</td>
                            <td width="100">Frequency</td>
                            <td width="100">Days</td>
                            <td width="40">Last Run</td>
                            <td width="40">Next Run</td>
                            <td width="50">State</td>
                            <td></td>
                        </tr>
                        <%
        nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
        if (nCurPage > nPageCount) {
            nCurPage = 1;
        }
        int nStartPos = ((nCurPage - 1) * nRowCount);
        for (int i = nStartPos;
                i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
                i++) {
            bkpSch = al.get(i);
            interval = -1;
            days = "";
            index = -1;
            freq = "";
            index = bkpSch.getRepeat_interval().indexOf("freq=");
            if (index != -1) {
                freq = bkpSch.getRepeat_interval().substring(index + 5, bkpSch.getRepeat_interval().indexOf(";"));
            }
            index = bkpSch.getRepeat_interval().indexOf("interval=");
            if (index != -1) {
                interval = Integer.parseInt(bkpSch.getRepeat_interval().substring(index + 9, bkpSch.getRepeat_interval().indexOf(";", index)));
            }
            index = bkpSch.getRepeat_interval().indexOf("byday=");
            if (index != -1) {
                days = bkpSch.getRepeat_interval().substring(index + 6, bkpSch.getRepeat_interval().indexOf(";", index));
            }

                        %>
                        <tr class="
                            <%
                            if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            %>
                            ">
                            <td width="10" align="center" >
                                <a href="pgBkpSchedule.jsp?saveFlag=U&jobName=<%=bkpSch.getJobName()%>">
                                    <img src="../images/icons/edit.gif" border='0'>
                                </a>
                            </td>
                            <td width="10" align="center">
                                <a href="pgDelBkpSchedule.jsp?jobName=<%=bkpSch.getJobName()%>"
                                   onclick="return confirm('Are you sure you want to delete?','Sapher')">
                                    <img src="../images/icons/delete.gif" border='0' title="Delete the schedule entry">
                                </a>
                            </td>
                            <td width="50"><%=bkpSch.getSchemaName()%></td>
                            <td width="200"><%=bkpSch.getJobName()%></td>
                            <td width="40">
                                <%=new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US).format(bkpSch.getStartDate())%>
                            </td>
                            <td width = "40" >
                                <%=((bkpSch.getEndDate() == null) ? "" : new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US).format(bkpSch.getEndDate()))%>
                            </td>
                            <td width="40">
                                <%=new SimpleDateFormat("kk:mm:ss", java.util.Locale.US).format(bkpSch.getStartDate())%>
                            </td>
                            <td width="100"><%=interval + " " + freq%></td>
                            <td width="100"><%=days%></td>
                            <td width="40"><%=(bkpSch.getLastRun() == null ? "" : bkpSch.getLastRun())%></td>
                            <td width="40"><%=(bkpSch.getNextRun() == null ? "" : bkpSch.getNextRun())%></td>
                            <td width="50"><%=bkpSch.getState()%></td>
                            <td></td>

                        </tr>
                        <% }%>
                    </table>
                </td>
                <tr>
                </tr>
            </tr>
            <!-- Grid View Ends -->

            <!-- Pagination Starts -->
            <tr>
                <td class="formbody">
                    <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                        <tr>
                            <td>
                                <a href="pgScheduleList.jsp?CurPage=1">
                                <img src="../images/icons/page-first.gif" border="0"></a>
                            </td>
                            <td><% if (nCurPage > 1) {%>
                                <A href="pgScheduleList.jsp?CurPage=<%=(nCurPage - 1)%>">
                                <img src="../images/icons/page-prev.gif" border="0"></A>
                                <% } else {%>
                                <img src="../images/icons/page-prev.gif" border="0">
                                <% }%>
                            </td>
                            <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                            <td><% if (nCurPage < nPageCount) {%>
                                <A href="pgScheduleList.jsp?CurPage=<%=nCurPage + 1%>">
                                <img src="../images/icons/page-next.gif" border="0"></A>
                                <% } else {%>
                                <img src="../images/icons/page-next.gif" border="0">
                                <% }%>
                            </td>
                            <td>
                                <a href="pgScheduleList.jsp?CurPage=<%=nPageCount%>">
                                <img src="../images/icons/page-last.gif" border="0"></A>
                            </td>
                            <td width="100%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Pagination Ends -->
            <tr>
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>