<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.CPanel"
    import="bl.sapher.general.SimultaneousOpException"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.User"
    isThreadSafe="true"
    %>
<%
//<!--
//    Form Id : CPL5
//    Date    : 19-3-2008
//    Author  : Arun P. Jose
//-->

        try {
            if (request.getParameter("FileName") != null) {
                User.blockUsers();
            }

            String strUN = "";
            String strPW = "";

            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }

            CPanel cpanel = CPanel.getInstance((String) session.getAttribute("LogFileName"));
            System.out.println("AV> UN=" + strUN + ",PW=" + strPW + ",file=" + request.getParameter("FileName") + ",schema="+ request.getParameter("SchemaName") + ",new user" + request.getParameter("NewUser"));
            cpanel.doRestore(strUN, strPW, request.getParameter("FileName"),
                    request.getParameter("SchemaName"),
                    request.getParameter("NewUser"));
            User.unBlockUsers();
            out.write("0");
            //pageContext.forward("pgRestoreMain.jsp?RstrStatus=0");
        } catch (SimultaneousOpException soe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                soe.printStackTrace();
            }
            out.write("2");
            //pageContext.forward("pgRestoreMain.jsp?RstrStatus=2");
        } catch (TimeoutException toe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                toe.printStackTrace();
            }
%>
<script>
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<% } catch (Exception ex) {

            ex.printStackTrace();
            out.write("1");
            //pageContext.forward("pgRestoreMain.jsp?RstrStatus=1");
        }
%>