
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.general.GenConf"
    import="bl.sapher.general.ClientConf"
    import="bl.sapher.general.Logger"
    import="java.text.SimpleDateFormat"
    import="java.util.Calendar"
    isThreadSafe="true"
    %>

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <!--[if IE]> <style type="text/css"> .login_form label{ float: none; }  </style> <![endif]-->
        <link rel="shortcut icon" href="images/icons/sapher.ico" type="image/vnd.microsoft.icon" />
        <link rel="icon" href="images/icons/sapher.ico" type="image/vnd.microsoft.icon" />

        <script>
            function startTest() {
                var url = 'test-resp.jsp?';
                url += 'cmbCompany=' + document.getElementById('cmbCompany').value;
                console.log("TEST");

                return getDynData(url, 'divConsole');
            }

            function createRequestObject(){
                var req;
                if(window.XMLHttpRequest){
                    //For Firefox, Safari, Opera
                    req = new XMLHttpRequest();
                }
                else if(window.ActiveXObject){
                    //For IE 5+
                    req = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else{
                    //Error for an old browser
                    alert('Your browser is not IE 5 or higher, or Firefox or Safari or Opera');
                }

                return req;
            }

            //Make the XMLHttpRequest Object
            var http = createRequestObject();

            function sendRequest(method, url, target_DIV){
                if(method == 'get' || method == 'GET'){
                    http.open(method,url);
                    http.onreadystatechange = function() {handleResponse(target_DIV);}
                    http.send(null);
                }
            }

            function handleResponse(target) {
                if(http.readyState == 4 && http.status == 200){
                    var response = http.responseText;

                    if(response) {
                        if(target != '') {
                            document.getElementById(target).innerHTML = response;
                        }
                    }
                }
            }

            function getDynData(targetJsp, targetDIV) {
                sendRequest('GET',targetJsp,targetDIV);
            }

        </script>
    </head>

    <%
        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        GenConf.setLogPath(application.getRealPath("/Reports/logs"));
        String strVer = "";
        String strMessage = "";
        String strCRight = "";
        GenConf gConf = new GenConf();
        session.setAttribute("LogFileName", "sapher_startup_" +
                new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US).format(Calendar.getInstance().getTime()));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLogin; RemoteIP: " + request.getRemoteAddr());

        strVer = gConf.getSysVersion();
        strCRight = gConf.getSysCRight();
        if (request.getParameter("LoginStatus") != null) {
            if (request.getParameter("LoginStatus").equals("1")) {
                strMessage = "Invalid username or password";
            } else if (request.getParameter("LoginStatus").equals("2")) {
                strMessage = "Account Locked, Please contact administrator";
            } else if (request.getParameter("LoginStatus").equals("3")) {
                strMessage = "User already logged in.";
            } else if (request.getParameter("LoginStatus").equals("4")) {
                strMessage = "Cannot Login! Unknown Reason";
            } else if (request.getParameter("LoginStatus").equals("5")) {
                strMessage = "Login Blocked, DB Maintenance in progress";
            } else if (request.getParameter("LoginStatus").equals("6")) {
                strMessage = "Session timed out. Login again.";
            } else if (request.getParameter("LoginStatus").equals("7")) {
                strMessage = "No more login allowed, as per the license(s).";
            }
        }
    %>
    <body onload="return startTest();">
        <form name="login" method="post" action="">
            <td align="right" valign="top" height="142">
                <div class="error" align="center" style="padding: 5px 0px 2px 0px; "><%=strMessage%></div>
                <div class="login_form">
                    <div>
                        <label for="user">Company: </label>
                        <select id="cmbCompany" name="cmbCompany" style="width:160px" onchange="return startTest();">
                            <%
        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        java.util.ArrayList al = ClientConf.getClients();
        for (int i = 0; i < al.size(); i++) {
            out.write("<option>" + ((ClientConf) al.get(i)).getClientName() + "</option>");
        }%>
                        </select>
                    </div>
                </div>
                <div style="height: 10px;"></div>
                <div id="divConsole"></div>
            </td>
        </form>
    </body>
</html>