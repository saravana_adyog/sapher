<!--
Form Id : CPL4
Date    : 18-3-2008
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.CPanel"
import="bl.sapher.general.GenConf"
import="bl.sapher.general.ClientConf"
import="bl.sapher.general.TimeoutException"
import="java.util.ArrayList"
import="java.text.DateFormatSymbols"
import="java.text.SimpleDateFormat"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Restore Wizard - [Sapher]</title>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "../css/master.css";</style>
    <!--Calendar Library Includes.. S => -->
    <link rel="stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/aqua/theme.css" title="Aqua" />
    <link rel="alternate stylesheet" type="text/css" media="all" href="../libjs/calendar/skins/calendar-green.css" title="green" />
    <!-- import the calendar script -->
    <script type="text/javascript" src="../libjs/calendar/calendar.js"></script>
    <!-- import the language module -->
    <script type="text/javascript" src="../libjs/calendar/lang/calendar-en.js"></script>
    <!-- helper script that uses the calendar -->
    <script type="text/javascript" src="../libjs/calendar/calendar-helper.js"></script>
    <script type="text/javascript" src="../libjs/calendar/calendar-setup.js"></script>
    <!--Calendar Library Includes.. E <= -->

    <script src="../libjs/ajax/ajax.js" type="text/javascript"></script>

    <script>
        function startSubmission() {
            waitPreloadPage();
            var http = createRequestObject();

            var url = 'pgRestore.jsp?' + 'FileName=' + document.getElementById('FileName').value +
                '&SchemaName=' + document.getElementById('SchemaName').value +
                '&NewUser=' + document.getElementById('NewUser').value;
            http.open('GET',url,true);
            http.onreadystatechange = function() {if(http.readyState == 4){
                    var resp = http.responseText;
                    window.location.href='pgRestoreMain.jsp?RstrStatus=' + resp;}}
            http.send(null);
        }

        <!-- PreLoad Wait - Script -->
        <!-- This script and more from http://www.rainbow.arch.scriptmania.com
        function waitPreloadPage() { //DOM
            if (document.getElementById){
                document.getElementById('prepage').style.display='block';
            }
        }

        function validate(){
            if (document.FrmRestore.FileName.value == ""){
                alert("Please select a Backup File from the list.");
                return false;
            }
            if (document.FrmRestore.SchemaName.value == ""){
                alert("Please select a Client.");
                return false;
            }
            ans = confirm("Do you want to restore to a new Schema?");

            if (ans) {
                toUser = prompt("New Schema to which the data to be imported");
                document.FrmRestore.NewUser.value=toUser;
            }
            startSubmission();

            return true;
        }
        function kyFrom(e){
            var keynum;
            if(window.event) // IE
            {
                keynum = e.keyCode;
            }
            else if(e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
            }
            if (keynum == 27)
                document.FrmBkpList.txtRstrDtFrom.value = "";
        }
        function kyTo(e){
            var keynum;
            if(window.event) // IE
            {
                keynum = e.keyCode;
            }
            else if(e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
            }
            if (keynum == 27)
                document.FrmBkpList.txtRstrDtTo.value = "";
        }
    </script>
</head>

<body>

    <%!    private final String FORM_ID = "CPL4";
    private ArrayList al = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private String strMessageInfo = "";
    private String strVal = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private String strClient = "";
    private String strSchema = "";
    %>

    <%
        if (request.getParameter("txtRstrDtFrom") == null) {
            if (session.getAttribute("txtRstrDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtRstrDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtRstrDtFrom");
        }
        session.setAttribute("txtRstrDtFrom", strDtFrom);

        if (request.getParameter("clientName") == null) {
            if (session.getAttribute("clientName") == null) {
                strClient = "";
                strSchema = "";
            } else {
                strClient = (String) session.getAttribute("clientName");
                strSchema = new GenConf(strClient).getAppUser().toUpperCase();
            }
        } else {
            strClient = (String) request.getParameter("clientName");
            strSchema = new GenConf(strClient).getAppUser().toUpperCase();
        }
        session.setAttribute("clientName", strClient);

        if (request.getParameter("txtRstrDtTo") == null) {
            if (session.getAttribute("txtRstrDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtRstrDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtRstrDtTo");
        }
        session.setAttribute("txtRstrDtTo", strDtTo);

        String strUN = "";
        String strPW = "";

        strUN = ((String) session.getAttribute("cpanelUser"));
        strPW = ((String) session.getAttribute("cpanelPwd"));

        try {
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            al = CPanel.lstFiles(strUN, strPW, strSchema + ".DMP", strDtFrom, strDtTo);
        } catch (TimeoutException toe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                toe.printStackTrace();
            }
    %>
    <script type="text/javascript">
        parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
    </script>
    <%
        }
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

    %>

    <div style="height: 25px;"></div>
    <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
        <tr>
            <td id="formtitle">Select a Backup File</td>
        </tr>
        <!-- Form Body Starts -->
        <tr>
            <form name="FrmBkpList" METHOD="POST" ACTION="pgRestoreList.jsp" >
                <td class="formbody">
                <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td class="form_group_title" colspan="10">Search Backup</td>
                </tr>
                <tr>
                    <td style="height: 10px;"></td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtRstrDtFrom">Date Range</label></td>
                    <td class="field">
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <input type="text" name="txtRstrDtFrom" id="txtRstrDtFrom" size="18" maxlength="18"
                                           value="<%=strDtFrom%>" readonly  onkeydown="return kyFrom(event);">
                                </td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <a href="#" id="cal_trigger1"><img src="../images/icons/cal.gif" border='0' name="imgCal"></a>
                                </td>
                                <td class="fLabel" style="padding-right: 3px; padding-left: 3px;">TO</td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <input type="text" name="txtRstrDtTo" id="txtRstrDtTo" size="18" maxlength="18"
                                           value="<%=strDtTo%>" readonly  onkeydown="return kyTo(event);">
                                </td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <a href="#" id="cal_trigger2"><img src="../images/icons/cal.gif" border='0' name="imgCal"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="2" width="500" align="right">
                        <b><%=strMessageInfo%></b>
                        <span  id="prepage" style="display:none; float:right;">
                            Processing, Please wait...
                            <img src="../images/indicator.gif" />
                        </span>
                    </td>
                </tr>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField : "txtRstrDtFrom", //*
                        ifFormat : "%d-%b-%Y %H:%M",
                        showsTime : true,
                        button : "cal_trigger1", //*
                        step : 1
                    });
                </script>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField : "txtRstrDtTo", //*
                        ifFormat : "%d-%b-%Y %H:%M",
                        showsTime : true,
                        button : "cal_trigger2", //*
                        step : 1
                    });
                </script>
                <tr>
                <td class="fLabel" width="125"></td>
                <td class="field" colspan="2">
                    <input type="submit" name="cmdSearch" class="button" value="Search" style="width: 87px;">
                </td>
            </form>

            <td>
                <form name="FrmRestore" METHOD="POST" ACTION="pgRestore.jsp" onsubmit="return validate();">
                    <input type="hidden" id="FileName" name="FileName">
                    <input type="hidden" id="SchemaName" name="SchemaName" value="<%=strSchema%>">
                    <input type="hidden" id="ClientName" name="SchemaName" value="<%=strClient%>">
                    <input type="hidden" id="NewUser" name="NewUser" value="<%=strSchema%>">
                    <span style="float: right">
                        <input type="button" name="cmdRestore" id="cmdRestore" class="button"
                               value="Restore Now" style="width: 85px;" onclick="return validate();">
                    </span>
                    <div id="dyncompo" style="display:none; border:1px;" >
                        <%
        java.util.ArrayList alClientslist = ClientConf.getClients();
        for (int i = 0; i < alClientslist.size(); i++) {
            out.write("<div>" + ((ClientConf) alClientslist.get(i)).getClientName() + "</div>");
        }
        alClientslist = null;
                        %>
                    </div>
                </form>
            </td>
        </tr>
    </table>

    <!-- Form Body Ends -->
    <tr>
        <td style="height: 1px;"></td>
    </tr>
    <!-- Grid View Starts -->
    <tr>
        <td class="formbody">
            <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td class="form_group_title" colspan="10">Search Results
                        <%=" - " + al.size() + " file(s) found"%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <% if (al.size() == 0) {
            return;
        }
    %>
    <tr>
        <td class="formbody">
            <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                <tr class="grid_header">
                    <td width="16"></td>
                    <td width="150">Schema</td>
                    <td width="100">Backup Date</td>
                    <td width="20">Time</td>
                    <td width="300">File Name</td>
                    <td></td>
                </tr>
                <form action="">
                    <%
        nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
        if (nCurPage > nPageCount) {
            nCurPage = 1;
        }
        int nStartPos = ((nCurPage - 1) * nRowCount);
        for (int i = nStartPos;
                i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
                i++) {
            strVal = ((String) al.get(i));
                    %>
                    <tr class="
                        <%
                        if (i % 2 == 0) {
                            out.write("even_row");
                        } else {
                            out.write("odd_row");
                        }
                        %>
                        ">
                        <td width="16" align="center" >
                            <input type="radio" id="FileSelect" name="FileSelect" onclick="document.FrmRestore.FileName.value='<%=strVal%>'">
                        </td>
                        <td width="150" align="center">
                            <%
                        out.write(strVal.substring(15, strVal.length() - 4));
                            %>
                        </td>
                        <td width="100" align='center'>
                            <%
                        out.write(strVal.substring(6, 8) + "-" + new DateFormatSymbols().getShortMonths()[Integer.parseInt(strVal.substring(4, 6))] + "-" + strVal.substring(0, 4));
                            %>
                        </td>
                        <td width="20" align='left' >
                            <%
                        out.write(strVal.substring(8, 10) + ":" + strVal.substring(10, 12));
                            %>
                        </td>
                        <td width="300" align='center'>
                            <%=strVal%>
                        </td>
                        <td></td>
                    </tr>
                    <% }%>
                </form>
            </table>
        </td>
        <tr>
        </tr>
    </tr>
    <!-- Grid View Ends -->

    <!-- Pagination Starts -->
    <tr>
        <td class="formbody">
            <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                <tr>
                    <td>
                        <a href="pgRestoreList.jsp?CurPage=1">
                        <img src="../images/icons/page-first.gif" border="0"></a>
                    </td>
                    <td><% if (nCurPage > 1) {%>
                        <A href="pgRestoreList.jsp?CurPage=<%=(nCurPage - 1)%>">
                        <img src="../images/icons/page-prev.gif" border="0"></A>
                        <% } else {%>
                        <img src="../images/icons/page-prev.gif" border="0">
                        <% }%>
                    </td>
                    <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                    <td><% if (nCurPage < nPageCount) {%>
                        <A href="pgRestoreList.jsp?CurPage=<%=nCurPage + 1%>">
                        <img src="../images/icons/page-next.gif" border="0"></A>
                        <% } else {%>
                        <img src="../images/icons/page-next.gif" border="0">
                        <% }%>
                    </td>
                    <td>
                        <a href="pgRestoreList.jsp?CurPage=<%=nPageCount%>">
                        <img src="../images/icons/page-last.gif" border="0"></a>
                    </td>
                    <td width="100%"></td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- Pagination Ends -->

    <tr>
        <td style="height: 10px;"></td>
    </tr>
    </table>
    <div style="height: 25px;"></div>
</body>
</html>