<!--
Form Id : CPL2
Date    : 18-3-2008
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.CPanel"
import="java.util.ArrayList"
isThreadSafe="true"
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%!
    private final String FORM_ID = "CPL2";
    private ArrayList al = null;
    private String strVal = "";
%>

<html>
    <head>
        <title>Log File</title>
    </head>
    <body>
        <font face="Courier New" size="2">
            <%
            try {
                if (request.getParameter("LogFile") != null) {
                    al = CPanel.readLogFile((String) session.getAttribute("cpanelUser"),
                            (String) session.getAttribute("cpanelPwd"),
                            request.getParameter("LogFile"));
                    for (int i = 1;
                            i < al.size(); i++) {
                        out.write((String) al.get(i) + "<br>");
                    }
                }
            } catch (Exception ex) {
                if (bl.sapher.general.GenConf.isDebugMode()) {
                    out.write("Log file not present!!!");
                }
            }
            %>
        </font>
    </body>
</html>