<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.CPanel"
import="bl.sapher.general.SimultaneousOpException"
import="bl.sapher.general.TimeoutException"
isThreadSafe="true"
    %>

<%
//Form Id : CPL3
//Date    : 18-3-2008
//Author  : Arun P. Jose

        try {
            String strUN = "";
            String strPW = "";
            String strClient = "";
            strUN = (String) session.getAttribute("cpanelUser");
            strPW = (String) session.getAttribute("cpanelPwd");

            // If user name or password is null, The session timed out.
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }

            if (request.getParameter("clientName") != null) {
                strClient = request.getParameter("clientName");
                CPanel cpanel = CPanel.getInstance((String) session.getAttribute("LogFileName"));
                cpanel.doBackup(strUN, strPW, strClient);
                out.write("0");
                //pageContext.forward("pgBackupList.jsp?BkpStatus=0");
            } else {
                out.write("1");
                //pageContext.forward("pgBackupList.jsp?BkpStatus=1");
            }
        } catch (SimultaneousOpException soe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                soe.printStackTrace();
            }
            out.write("2");
            //pageContext.forward("pgBackupList.jsp?BkpStatus=2");
        } catch (TimeoutException toe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%        } catch (Exception e) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                e.printStackTrace();
            }
            out.write("1");
            //pageContext.forward("pgBackupList.jsp?BkpStatus=1");
        }
%>