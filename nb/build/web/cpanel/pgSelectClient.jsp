<%-- 
    Document   : pgSelectClient
    Created on : Feb 6, 2009, 12:09:19 PM
    Author     : Arun P Jose
--%>

<%@page
    contentType="text/html"
    pageEncoding="windows-1252"
    import="bl.sapher.general.ClientConf"
    import="bl.sapher.general.GenConf"
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <script type="text/javascript" src="libjs/gui.js"></script>
        <title></title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "../css/master.css";</style>

        <script>
            function nextWindow(next)
            {
                if (next==1) {
                    opener.location='pgBackup.jsp?clientName=' + document.getElementById('cmbClients').value;
                } else if (next==2) {
                    opener.location='pgRestoreList.jsp?clientName=' + document.getElementById('cmbClients').value;
                } else if (next==3) {
                    this.location='pgClearDB.jsp?clientName=' + document.getElementById('cmbClients').value;
                } else if (next==4) {
                    this.location='pgDelCases.jsp?clientName=' + document.getElementById('cmbClients').value
                }
                window.close();
            }
        </script>
    </head>
    <body>

        <div style="height: 15px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Select a Client</td>
            </tr>
            <!-- Form Body Starts -->
            <form name="FrmClientList" METHOD="POST" ACTION="" >
                <tr>
                    <td class="formbody">
                        <table border="0">
                            <tbody>
                                <tr>
                                    <td class="fLabel">Clients :</td>
                                    <td><select name="cmbClients" id="cmbClients">
                                            <%
        java.util.ArrayList al = ClientConf.getClients();
        for (int i = 0; i < al.size(); i++) {
            out.write("<option>" + ((ClientConf) al.get(i)).getClientName() + "</option>");
        }%>
                                    </select></td>
                                </tr>
                                <tr>
                                    <td class="fLabel"></td>
                                    <td>
                                        <input type="button" value="Select" class="button" name="cmdSelect"
                                               onclick="nextWindow(<%=request.getParameter("Next")%>)" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </form>
            <!-- Form Body Ends -->
        </table>
        <div style="height: 25px;"></div>

    </body>
    <script type="text/javascript">

    </script>
</html>