<!--
Form Id : NAV1.5
Author  : Ratheesh
-->
<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">
            @import "../css/master.css";
            body{
                text-align: center;
                background: #588A89 url(../images/bg/BG-effect.jpg);
                background-repeat:  no-repeat;
                background-position: center;
                margin: 0; 	padding: 0;
            }
        </style>
    </head>

    <%!    private String strMessage;
    private String strHdr;
    %>

    <body>
        <%
        strMessage = "";
        strHdr = "";

        if (request.getParameter("Status") != null) {
            if (request.getParameter("Status").equals("1")) {
                strHdr = "Error:";
                strMessage = "Sorry, Permission Denied!";
            } else if (request.getParameter("Status").equals("2")) {
                strHdr = "Status:";
                strMessage = "Update Success!";
            } else if (request.getParameter("Status").equals("3")) {
                strHdr = "Status:";
                strMessage = "Update Failed!";
            } else if (request.getParameter("Status").equals("4")) {
                strHdr = "Status:";
                strMessage = "MedDRA Datebase updated successfully.";
            } else if (request.getParameter("Status").equals("5")) {
                strHdr = "Status:";
                strMessage = "MedDRA Datebase update failed!";
            } else if (request.getParameter("Status").equals("6")) {
                strHdr = "Status:";
                strMessage = "MedDRA Datebase update cancelled by user.";
            } else if (request.getParameter("Status").equals("7")) {
                strHdr = "Status:";
                strMessage = "Other instance of MedDRA update in progress.";
            }
        }

        session.removeAttribute("txtRstrMainDtFrom");
        session.removeAttribute("txtRstrMainDtTo");

        session.removeAttribute("txtRstrDtFrom");
        session.removeAttribute("txtRstrDtTo");

        session.removeAttribute("txtBkpDtTo");
        session.removeAttribute("txtBkpDtFrom");


        if (!strMessage.equals("")) {
        %>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" height="50%">
            <tr>
                <td height="200" colspan="2">
                    <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
                        <tr>
                            <td id="formtitle"><%=strHdr%></td>
                        </tr>
                        <!-- Form Body Starts -->
                        <tr>
                            <td class="formbody">
                                <div class="error"><%=strMessage%></div>
                            </td>
                        </tr>
                        <!-- Form Body Ends -->
                        <tr>
                            <td style="height: 10px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%}%>
    </body>
</html>
