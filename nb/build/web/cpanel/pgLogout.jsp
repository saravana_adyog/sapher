<!--
Form Id : USM4.5
Date    : 13-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
isThreadSafe="true"
%>

<%
    session.removeAttribute("cpanelUser");
    session.removeAttribute("cpanelPwd");

    pageContext.forward("pgLogin.jsp");
    return;
%>