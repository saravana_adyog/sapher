package cpanel;

<!--
Form Id : CPL7.1
Date    : 14-02-2009
Author  : Arun P. Jose
-->

<%@page contentType="text/html"
pageEncoding="UTF-8"
import="bl.sapher.BackupSchedule"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.DBConnectionException"
import="bl.sapher.general.GenericException"
        %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        
        <%!    private final String FORM_ID = "CPL7.1";
    private String strUN = "";
    private String strPW = "";
        %>

        <%
        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));
            
            if (strUN==null || strPW==null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
         } catch (TimeoutException toe) {
        %>
        <script type="text/javascript">
            document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
       } catch (Exception e) {
            e.printStackTrace();
        %>
        <script type="text/javascript">
            document.location.replace("pgScheduleList.jsp?DelStatus=1");
        </script>
        <%
            return;
        }

        try {
            BackupSchedule.deleteBkpSchedule(strUN, strPW, (String) request.getParameter("jobName"));
            pageContext.forward("pgScheduleList.jsp?DelStatus=0");
        } catch (DBConnectionException ex) {
            ex.printStackTrace();
            pageContext.forward("pgScheduleList.jsp?DelStatus=1");
        } catch (GenericException ex) {
            ex.printStackTrace();
            pageContext.forward("pgScheduleList.jsp?DelStatus=1");
        }
        %>
    </body>
</html>
