<!--
Form Id : NAV1.3
Author  : Ratheesh
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />
        
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />
        
        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />
        
        <meta name="author" content="Focal3" />
        
        <SCRIPT src="../libjs/utils.js"></SCRIPT>
        
        <!-- For Left Manu Starts -->
        <link rel="stylesheet" type="text/css" href="../libjs/resources/css/ext-all.css" />
        <script type="text/javascript" src="../libjs/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="../libjs/ext-all.js"></script>
        <style type="text/css" media="all">@import "../css/master.css";</style>
        <SCRIPT>
	<!--
	Ext.onReady(function(){
		var ce = new Ext.Panel({
			frame:true,
			title: 'Case Entry',
			collapsible:true,
			renderTo: _el("panel-content"),
			width:175,
			contentEl:'case-entry',
			titleCollapse: true,
			collapsed: true
		});
		var cl = new Ext.Panel({
			frame:true,
			title: 'Code List',
			collapsible:true,
			renderTo: _el("panel-content"),
			width:175,
			contentEl:'code-list',
			titleCollapse: true,
			collapsed: true
		});
		var re = new Ext.Panel({
			frame:true,
			title: 'Reports',
			collapsible:true,
			renderTo: _el("panel-content"),
			width:175,
			contentEl:'report',
			titleCollapse: true,
			collapsed: true
		});
		var re = new Ext.Panel({
			frame:true,
			title: 'Audit',
			collapsible:true,
			renderTo: _el("panel-content"),
			width:175,
			contentEl:'audit',
			titleCollapse: true,
			collapsed: true
		});
		var us = new Ext.Panel({
			frame:true,
			title: 'User Administration',
			collapsible:true,
			renderTo: _el("panel-content"),
			width:175,
			contentEl:'user-admn',
			titleCollapse: true,
			collapsed: true
		});
	});
	//-->
        </SCRIPT>
        <!-- For Left Manu Ends -->
    </head>
    
    <body>
        <div style="height: 100%; width:10px; z-index: 3000; position: absolute; vetical-align: middle; display: none;" id="showLeftFramePnael">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                <tr>
                    <td width="100%" align="center" valign="middle" background="../images/letpanel/show-bg.gif"><a onclick="mostrarMenu('showLeftFramePnael');return false;" href="#"><img src="../images/letpanel/show.gif" border="0"  ></a></td>
                </tr>
            </table>
        </div>
        
        <ul id="case-entry" class="x-hidden">
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgQuickScreen.jsp" target="contentFrame">Quick Screen</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCaselist.jsp" target="contentFrame">Case Details</a>
            </li>
        </ul>
        
        <ul id="code-list" class="x-hidden">
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCountrylist.jsp"  target="contentFrame">Country</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgOutcomelist.jsp"  target="contentFrame">AE Outcome</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgAELLTlist.jsp"  target="contentFrame">AE Low Level Term</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgAEDiaglist.jsp"  target="contentFrame">AE Preferred Term</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgBodySystemlist.jsp"  target="contentFrame">Body System</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCausalitylist.jsp"  target="contentFrame">Assessment of Causality</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgSeriousnesslist.jsp"  target="contentFrame">Seriousness</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgDFreqlist.jsp"  target="contentFrame">Dose Frequency</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgDRgmnlist.jsp"  target="contentFrame">Dose Regimen</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgUnitlist.jsp"  target="contentFrame">Dose Units</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgEOriginlist.jsp"  target="contentFrame">Ethnic Origin</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgSeveritylist.jsp"  target="contentFrame">Event Severity</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgFormulationlist.jsp"  target="contentFrame">Formulation</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgProductlist.jsp"  target="contentFrame">Product</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgSexlist.jsp"  target="contentFrame">Sex</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgIndicationTreatmentlist.jsp"  target="contentFrame">Indication for Treatment</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgManufacturerlist.jsp"  target="contentFrame">Manufacturer</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRechallengeResultlist.jsp"  target="contentFrame">Rechallenge Result</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRepTypelist.jsp"  target="contentFrame">Report Type</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgSourceInfolist.jsp"  target="contentFrame">Report Source</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgReporterStatuslist.jsp"  target="contentFrame">Reporter Status</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgLHAlist.jsp"  target="contentFrame">Health Authority</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgSubmColist.jsp"  target="contentFrame">Submission Company</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgTrialNumlist.jsp"  target="contentFrame">Trial No</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRoutelist.jsp"  target="contentFrame">Treatment Route</a>
            </li>
        </ul>
        
        <ul id="report" class="x-hidden">
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCaselistPrint.jsp?ReportType=CIOMS1"  target="contentFrame">CIOMS I</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRep.jsp?ReportType=CIOMS2"  target="contentFrame">CIOMS II</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRep.jsp?ReportType=ICH"  target="contentFrame">ICH</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCaselistPrint.jsp?ReportType=MedWatch"  target="contentFrame">MEDWATCH</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRep.jsp?ReportType=TabularS"  target="contentFrame">SUMTAB - SERIOUSNESS</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRep.jsp?ReportType=TabularRT"  target="contentFrame">SUMTAB - REP TYPE</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCaselistPrint.jsp?ReportType=FullCase"  target="contentFrame">FULL CASE</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgCaselistPrint.jsp?ReportType=WHO"  target="contentFrame">WHO</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgQueryTool.jsp"  target="contentFrame">Query Tool</a>
            </li>        
        </ul>
        
        <ul id="audit" class="x-hidden">
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgInventorylist.jsp"  target="contentFrame">Audit Control</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgAuditlist.jsp"  target="contentFrame">Audit Trail</a>
            </li>
        </ul>
        <ul id="user-admn" class="x-hidden">
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgRolelist.jsp"  target="contentFrame">Roles</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgPermissionlist.jsp"  target="contentFrame">Role Permissions</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgUserlist.jsp?Next=0"  target="contentFrame">Users</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgResetPwd.jsp?Next=0"  target="contentFrame">Change Password</a>
            </li>
            <li>
                <img src="../images/s.gif" style="background-image:url(../images/icon-by-date.gif);"/>
                <a id="group-date" href="../pgNavigate.jsp?Target=pgLicenselist.jsp" target="contentFrame">License</a>
            </li>
        </ul>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="left_menu_panel" height="100%">
            <tr>
                <td align="right" valign="top" style="padding-top: 15px">
                    <!-- Left Menu Box.1 Starts -->
                    <table width="99%"  border="0" cellspacing="0" cellpadding="0" align="right" height="100%">
                        <!-- Left Menux Box.1 Header Starts -->
                        <tr>
                            <td height="26"  background="../images/letpanel/nav-header-bg.gif">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="7"><img src="../images/letpanel/header-left-end.gif"></td>
                                        <td>
                                            <div style="float:left;" id="panel-head">NAVIGATION</div>
                                            <div style="float:left"><img src="../images/letpanel/nav-header-curve.gif"></div>
                                        </td>
                                        <td width="7"><img src="../images/letpanel/header-right-end.gif"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- Left Menux Box.1 Header Ends -->
                        <!-- Left Menux Box.1 Content Area Starts -->
                        <tr>
                            <td id="panel-content" valign="top" align="center">
                            </td>
                        </tr>
                        <!-- Left Menux Box.1 Content Area Ends -->
                    </table>
                    <!-- Left Menu Box.1 Ends -->
                </td>
                <td width="5" align="center" valign="middle"><a onclick="mostrarMenu('showLeftFramePnael');return false;" href="#"><img src="../images/letpanel/hide.gif" border="0"  ></a></td>
            </tr>
        </table>
    </body>
</html>