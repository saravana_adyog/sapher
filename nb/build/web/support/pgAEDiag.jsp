<!--
Form Id : CDL3
Date    : 10-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.AEDiag"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>AE Preferred Term - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>
        <script language="Javascript">
            function validate(){
                if (document.FrmAEDiag.txtDiagCode.value == ""){
                    alert( "AE PT Code cannot be empty.");
                    document.FrmAEDiag.txtDiagCode.focus();
                    return false;
                }
                if (document.FrmAEDiag.txtDiagDesc.value == ""){
                    alert( "AE PT Desc cannot be empty.");
                    document.FrmAEDiag.txtDiagDesc.focus();
                    return false;
                }
                //if(!checkIsAlphaVar(document.FrmAEDiag.txtDiagCode.value)) {
                //    window.alert('Invalid character(s) in diag code');
                //    document.getElementById('txtDiagCode').focus();
                //    return false;
                //}
                if(!isAlphaNumericSpace(document.FrmAEDiag.txtDiagDesc.value)) {
                    window.alert('Invalid character(s) in diag description');
                    document.getElementById('txtDiagDesc').focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
    <%!
        AEDiag aeDiag = null;
        private final String FORM_ID = "CDL3";
        private String saveFlag = "";
        private String strParent = "";
        private boolean bAdd;
        private boolean bEdit;
        private boolean bDelete;
        private boolean bView;
        private User currentUser = null;
        private Inventory inv = null;
    %>
    <%
        if (request.getParameter("Parent") != null)
            strParent = request.getParameter("Parent");
        else
            strParent = "0";
        if (request.getParameter("SaveFlag") != null)
            saveFlag = request.getParameter("SaveFlag");
        try {
            if(session.getAttribute("CurrentUser")==null && session.getAttribute("SapherSessionId")==null) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiag.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
                 throw new TimeoutException("Server timeout.Login again.");
            }
        currentUser = new User((String)session.getAttribute("Company"), (String)session.getAttribute("CurrentUser"),(String)session.getAttribute("SapherSessionId"));
            String strPW = (String)session.getAttribute("CurrentUserPW");
            if(!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiag.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Invalid username/password.");
        %>
                    <script type="text/javascript">
                        parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
                    </script>
        <%
                    return;
            }
        } catch(TimeoutException toe){
    %>
    <script type="text/javascript">
        parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
    </script>
    <%
        return;
    } catch(Exception e) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Other Exception. User already logged in.");
        %>
                    <script type="text/javascript">
                        parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
                    </script>
        <%
                    return;
        }
        try{
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String)session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission( (String)session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission( (String)session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission( (String)session.getAttribute("Company"), inv, 'V');

            if (!bAdd && !bEdit && !bView){
	            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiag.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit){
                saveFlag = "V";
            } else {
                    session.setAttribute("DirtyFlag", "true");
                }
            if (!saveFlag.equals("I"))
                if (request.getParameter("DiagCode") != null)
                    aeDiag = new AEDiag( (String)session.getAttribute("Company"), (String)(request.getParameter("DiagCode")));
                else{
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            else
                aeDiag = new AEDiag();
        } catch(RecordNotFoundException e){
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiag.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; RecordNotFoundException.");
            pageContext.forward("content.jsp?Status=0");
            return;
        }
    %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmAEDiag" name="FrmAEDiag" method="POST" action="pgSavAEDiag.jsp"
                onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I")?"New ":(saveFlag.equals("V")?"View ":"Edit "))%>AE Preferred Term</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtDiagCode">PT Code</label></td>
                                <td class="field"><input type="text" name="txtDiagCode" onkeydown="return checkIsNumeric(event);"
                                     id="txtDiagCode" size="10" maxlength="8" title="Adverse event Preferred Term code" 
                                     value="<%=aeDiag.getDiag_Code()%>"
                                     <%
                                        if(!saveFlag.equals("I")) {
                                            out.write(" readonly ");
                                        }
                                     %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtDiagDesc">Preferred Term</label></td>
                                <td class="field"><input type="text" name="txtDiagDesc" id="txtDiagDesc" size="50" title="Adverse event Preferred Term description" 
                                     maxlength="50" value="<%=aeDiag.getDiag_Desc()%>"
                                     <%
                                          if(saveFlag.equals("V")) {
                                              out.write(" readonly ");
                                          }
                                      %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                     <%
                                         if(aeDiag.getIs_Valid() == 1) {
                                             out.write("CHECKED ");
                                         }
                                          if(saveFlag.equals("V")) {
                                             out.write("DISABLED ");
                                         }
                                     %>>
                                 </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V")?"button disabled":"button")%>" value="Save" style="width: 60px;"
                                         title="Save the preferred term entry" 
                                           <%
                                                if(saveFlag.equals("V")) {
                                                    out.write(" DISABLED ");
                                                }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;" title="Cancel the operation" 
                                        onclick="javascript:location.replace('<%=!strParent.equals("0")?"pgNavigate.jsp?Cancellation=1&Target=pgAEDiaglist.jsp":"content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>