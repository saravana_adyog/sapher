<!--
Form Id : NAV1.2
Author  : Ratheesh
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page 
contentType="text/html; charset=iso-8859-1" 
language="java" 
import="bl.sapher.general.GenConf"
isThreadSafe="true" 
%>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
	<title>Sapher</title>
	<meta http-equiv="Content-Language" content="en-us" />
	
	<meta http-equiv="imagetoolbar" content="no" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	
	<meta name="description" content="Description" />
	<meta name="keywords" content="Keywords" />
	
	<meta name="author" content="Focal3" />
	
	<style type="text/css" media="all">@import "../css/master.css";</style>
</head>
<%
    String strClient = "";
    String strCRight = "";
    GenConf gConf = new GenConf();
    
    strClient = gConf.getSysOrgName();
    strCRight = gConf.getSysCRight();
%>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
	<tr>
		<td id="footer">
			<div style="float:left; padding-left: 20px;">The product is Licensed to <%=(String)session.getAttribute("Company")%></div>
			<div style="float:right; padding-right: 20px; "><%=strCRight%></div>
		</td>
	</tr>
</table>
</body>
</html>