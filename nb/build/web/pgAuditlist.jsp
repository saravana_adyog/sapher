<!--
Form Id : AUD1
Date    : 15-12-2007.
Author  : Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.Date"
import="java.util.ArrayList"
import="bl.sapher.AuditTrial"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
import="java.text.SimpleDateFormat"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Audit Trail - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "pgAuditlist";</script>
    <script src="libjs/gui.js"></script>
    <script src="libjs/ajax/ajax.js"></script>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>

    <!--Calendar Library Includes.. S => -->
    <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
    <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
    <!-- import the calendar script -->
    <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
    <!-- import the language module -->
    <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
    <!-- helper script that uses the calendar -->
    <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
    <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
    <!--Calendar Library Includes.. E <= -->

    <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
    <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

</head>
<body>
    <%!    private final String FORM_ID = "AUD1";
    private String strUsername = "";
    private String strInvId = "";
    private String strInvDesc = "";
    private String strTxnType = "";
    private String strTgtType = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private String strRefVal = "";
    private Date dtFrom = null;
    private java.util.Date dtTo = null;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private AuditTrial aTrial = null;
    private int nRowCount = 5;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
    private SimpleDateFormat dFormat = null;
    %>

    <%
    aTrial = new AuditTrial((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgAuditlist.jsp");
        boolean resetSession = false;
        boolean currRole = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! No Permission.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {

                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Invalid username/password.");
    %>
    <script type="text/javascript">
        parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
    </script>
    <%
            return;
        }
        inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
        bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
        bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp: V/D=" + bView + "/" + bDelete);
    } catch (TimeoutException toe) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
    %>
    <script type="text/javascript">
        parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
    </script>
    <%
        return;
    } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
    %>
    <script type="text/javascript">
        parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
    </script>
    <%
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("txtAudUsername") == null) {
            if (session.getAttribute("txtAudUsername") == null) {
                strUsername = "";
            } else {
                strUsername = (String) session.getAttribute("txtAudUsername");
            }
        } else {
            strUsername = request.getParameter("txtAudUsername");
        }
        session.setAttribute("txtAudUsername", strUsername);

        if (request.getParameter("txtAudInv") == null) {
            if (session.getAttribute("txtAudInv") == null) {
                strInvId = "";
            } else {
                strInvId = (String) session.getAttribute("txtAudInv");
            }
        } else {
            strInvId = request.getParameter("txtAudInv");
        }
        session.setAttribute("txtAudInv", strInvId);

        if (request.getParameter("txtAudInvDesc") == null) {
            if (session.getAttribute("txtAudInvDesc") == null) {
                strInvDesc = "";
            } else {
                strInvDesc = (String) session.getAttribute("txtAudInvDesc");
            }
        } else {
            strInvDesc = request.getParameter("txtAudInvDesc");
        }
        session.setAttribute("txtAudInvDesc", strInvDesc);

        if (request.getParameter("txtRefVal") == null) {
            if (session.getAttribute("txtRefVal") == null) {
                strRefVal = "";
            } else {
                strRefVal = (String) session.getAttribute("txtRefVal");
            }
        } else {
            strRefVal = request.getParameter("txtRefVal");
        }
        session.setAttribute("txtRefVal", strRefVal);

        if (request.getParameter("cmbTxnType") == null) {
            if (session.getAttribute("cmbTxnType") == null) {
                strTxnType = "All";
            } else {
                strTxnType = (String) session.getAttribute("cmbTxnType");
            }
        } else {
            strTxnType = request.getParameter("cmbTxnType");
        }
        session.setAttribute("cmbTxnType", strTxnType);

        if (strTxnType.equals("Insert")) {
            strTxnType = "I";
        } else if (strTxnType.equals("Update")) {
            strTxnType = "U";
        } else if (strTxnType.equals("Delete")) {
            strTxnType = "D";
        } else if (strTxnType.equals("All")) {
            strTxnType = "";
        }

        if (request.getParameter("cmbTgtType") == null) {
            if (session.getAttribute("cmbTgtType") == null) {
                strTgtType = "All";
            } else {
                strTgtType = (String) session.getAttribute("cmbTgtType");
            }
        } else {
            strTgtType = request.getParameter("cmbTgtType");
        }
        session.setAttribute("cmbTgtType", strTgtType);

        if (strTgtType.equals("All")) {
            strTgtType = "";
        }

        if (request.getParameter("txtDtFrom") == null) {
            if (session.getAttribute("txtDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtDtFrom");
        }
        session.setAttribute("txtDtFrom", strDtFrom);

        if (request.getParameter("txtDtTo") == null) {
            if (session.getAttribute("txtDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtDtTo");
        }
        session.setAttribute("txtDtTo", strDtTo);

        if (resetSession) {
            strUsername = "";
            strInvDesc = "";
            strInvId = "";
            strTgtType = "";
            strTxnType = "";
            strDtFrom = "";
            strDtTo = "";
            strRefVal = "";
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = AuditTrial.listAudit((String) session.getAttribute("Company"), "", "", "", "", "", "", "");
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strUsername + "," + strInvId + "," + strTxnType + "," + strTgtType + "," + strDtFrom + "," + strDtTo + "," + strRefVal);
            al = AuditTrial.listAudit((String) session.getAttribute("Company"), strUsername, strInvId, strTxnType, strTgtType, strDtFrom, strDtTo, strRefVal);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

    %>

    <div style="height: 25px;"></div>
    <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
        <tr>
            <td id="formtitle">Audit Trail - Audit Listing</td>
        </tr>
        <!-- Form Body Starts -->
        <tr>
            <form name="FrmAuditList" METHOD="POST" ACTION="pgAuditlist.jsp" >
                <td class="formbody">
                <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td class="form_group_title" colspan="10">Search Criteria</td>
                </tr>
                <tr>
                    <td style="height: 10px;"></td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtAudInv">Inventory</label></td>
                    <td class="field" width="300">
                        <input type="text" id="txtAudInv" name="txtAudInv" size="30" onkeyup="ajax_showOptions('txtAudInv_ID',this,'Inventory',event);"
                               value="<%=strInvId%>">
                        <input type="text" readonly size="5" id="txtAudInv_ID" name="txtAudInv_ID"
                               value="<%=strInvDesc%>">
                    </td>
                    <td width="500" align="right"><b><%=strMessageInfo%></b></td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtAudUsername">Username</label></td>
                    <td class="field" colspan="9">
                        <input type="text" name="txtAudUsername" id="txtAudUsername" size="11" maxlength="10" title="Username"
                               value="<%=strUsername%>">
                        <label style="margin-right: 2px;" class="fLabel">Ref Value</label>
                        <input style="padding-right: 0px; padding-left: 0px;" type="text" name="txtRefVal" id="txtRefVal" title="Ref Value"
                               size="23" maxlength="30" value="<%=strRefVal%>">
                    </td>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="txtDtFrom">Date Range</label></td>
                    <td class="field">
                        <table border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <input type="text" name="txtDtFrom" id="txtDtFrom" size="18" maxlength="18" clear=2 title="From Date"
                                           value="<%=strDtFrom%>" readonly  onkeydown="return ClearInput(event,2);">
                                </td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select Date"></a>
                                </td>
                                <td class="fLabel" style="padding-right: 3px; padding-left: 3px;">TO</td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <input type="text" name="txtDtTo" id="txtDtTo" size="18" maxlength="18" clear=3 title="To Date"
                                           value="<%=strDtTo%>" readonly  onkeydown="return ClearInput(event,3);">
                                </td>
                                <td style="padding-right: 3px; padding-left: 0px;">
                                    <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" border='0' name="imgCal" alt="Select" title="Select Date"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField : "txtDtFrom", //*
                        ifFormat : "%d-%b-%Y %H:%M",
                        showsTime : true,
                        button : "cal_trigger1", //*
                        step : 1
                    });
                </script>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField : "txtDtTo", //*
                        ifFormat : "%d-%b-%Y %H:%M",
                        showsTime : true,
                        button : "cal_trigger2", //*
                        step : 1
                    });
                </script>
                <tr>
                    <td class="fLabel" width="125"><label for="cmbTxnType">Transaction</label></td>
                    <td class="field">
                    <span style="float: left">
                        <select id="cmbTxnType" NAME="cmbTxnType" class="comboclass" style="width: 100px;" title="Transaction">
                            <option <%=(strTxnType.equals("") ? "SELECTED" : "")%>>All</option>
                            <option <%=(strTxnType.equals("I") ? "SELECTED" : "")%>>Insert</option>
                            <option <%=(strTxnType.equals("U") ? "SELECTED" : "")%>>Update</option>
                            <option <%=(strTxnType.equals("D") ? "SELECTED" : "")%>>Delete</option>
                        </select>
                    </span>
                </tr>
                <tr>
                    <td class="fLabel" width="125"><label for="cmbTgtType">Screen Type</label></td>
                    <td class="field" colspan="2">
                        <span style="float: left">
                            <select id="cmbTgtType" NAME="cmbTgtType" class="comboclass" style="width: 100px;" title="Screen Type">
                                <option <%=(strTgtType.equals("") ? "SELECTED" : "")%>>All</option>
                                <option <%=(strTgtType.equals("CODE") ? "SELECTED" : "")%>>CODE</option>
                                <option <%=(strTgtType.equals("CASE") ? "SELECTED" : "")%>>CASE</option>
                            </select>
                        </span>
                    </td>
                </tr>
                <tr>
                <td>
                    <td colspan="2">
                        <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                        <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                               onclick="location.replace('pgNavigate.jsp?Target=pgAuditlist.jsp');"/>
                    </td>
                </td>
            </form>
            <td width="16" align="right" >
                <a href="#" title="Print"
                   <%
        if (al.size() > 0) {
            session.setAttribute("AuditPrintData", al);
            out.write("onclick=\"popupWnd('_blank', 'yes','pgPrintAuditTrial.jsp?usr=" + strUsername +
                    "&invId=" + strInvId + "&transTyp=" + strTxnType + "&tgtTyp=" + strTgtType +
                    "&frmDt=" + strDtFrom +"&toDt=" + strDtTo +"&refVal=" + strRefVal + "',800,600)\"");
        }
                   %>>
                    <img src="images/icons/print.gif" border='0'>
                </a>
            </td>

        </tr>
    </table>
    </td>
    </tr>
    <!-- Form Body Ends -->
    <tr>
        <td style="height: 1px;"></td>
    </tr>

    <!-- Grid View Starts -->
    <tr>
        <td class="formbody">
            <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                <tr>
                    <td class="form_group_title" colspan="10">Search Results
                        <%=" - " + al.size() + " record(s) found"%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <% if (al.size() != 0) {%>
    <tr>
        <td class="formbody">
            <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                <tr class="grid_header">
                    <% if (bDelete) {%>
                    <td width="16"></td>
                    <% }%>
                    <td width="30">User</td>
                    <td width="45">Date</td>
                    <td width="45">Time</td>
                    <td width="30">Type</td>
                    <td width="40">Txn</td>
                    <td width="30">Inv Id</td>
                    <td width="100">Inv Desc</td>
                    <td width="50">Ref Val</td>
                    <td width="100">Field Name</td>
                    <td width="100">Old Value</td>
                    <td width="100">New Value</td>
                    <td></td>
                </tr>
                <%
     aTrial = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aTrial = ((AuditTrial) al.get(i));
                %>
                <tr class="
                    <%
                    if (i % 2 == 0) {
                        out.write("even_row");
                    } else {
                        out.write("odd_row");
                    }
                    %>
                    ">
                    <% if (bDelete) {%>
                    <td width="16" align="center" >
                        <a href="pgDelAudit.jsp?ChgNo=<%=aTrial.getChg_No()%>"
                           onclick="return confirm('Are you sure you want to delete?','Sapher')">
                            <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=aTrial.getChg_No()%>" border='0'>
                        </a>
                    </td>
                    <% }%>
                    <td align='center' width="30"><%=aTrial.getUsername()%></td>
                    <td align='center' width="45">
                        <%
                    dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
                    out.write(dFormat.format(aTrial.getChg_Date()));
                        %>
                    </td>
                    <td align='center' width="45">
                        <%
                    dFormat = new SimpleDateFormat("HH:mm:ss");
                    out.write(dFormat.format(aTrial.getChg_Time()));
                        %>
                    </td>
                    <td align='center' width="30"><%=aTrial.getTgt_Type()%></td>
                    <td align='center' width="40">
                        <%
                    if (aTrial.getTxn_Type().equals("I")) {
                        out.write("Insert");
                    }
                    if (aTrial.getTxn_Type().equals("U")) {
                        out.write("Update");
                    }
                    if (aTrial.getTxn_Type().equals("D")) {
                        out.write("Delete");
                    }
                        %>
                    </td>
                    <td align='center' width="30"><%=aTrial.getInventory().getInv_Id()%></td>
                    <td align='center' width="100"><%=aTrial.getInventory().getInv_Desc()%></td>
                    <td align='center' width="50"><%=aTrial.getPk_Col_Value()%></td>
                    <td align='center' width="100"><%=(aTrial.getInventoryCols() == null ? "" : aTrial.getInventoryCols().getField_Name())%></td>
                    <td align='center' width="100"><%=(aTrial.getOld_Val() == null ? "" : aTrial.getOld_Val())%></td>
                    <td align='center' width="100"><%=(aTrial.getNew_Val() == null ? "" : aTrial.getNew_Val())%></td>
                    <td></td>
                </tr>
                <% }%>
            </table>
        </td>
        <tr>
        </tr>
    </tr>
    <!-- Grid View Ends -->

    <!-- Pagination Starts -->
    <tr>
        <td class="formbody">
            <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                <tr>
                    <td>
                        <a href="pgAuditlist.jsp?CurPage=1">
                        <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                    </td>
                    <td><% if (nCurPage > 1) {%>
                        <A href="pgAuditlist.jsp?CurPage=<%=(nCurPage - 1)%>">
                        <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></A>
                        <% } else {%>
                        <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                        <% }%>
                    </td>
                    <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                    <td><% if (nCurPage < nPageCount) {%>
                        <a href="pgAuditlist.jsp?CurPage=<%=nCurPage + 1%>">
                        <img src="images/icons/page-next.gif" title="Go to next page" border="0"></a>
                        <% } else {%>
                        <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                        <% }%>
                    </td>
                    <td>
                        <a href="pgAuditlist.jsp?CurPage=<%=nPageCount%>">
                        <img src="images/icons/page-last.gif" title="Go to last page" border="0"></a>
                    </td>
                    <td width="100%"></td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- Pagination Ends -->
    <%}%>
    <tr>
        <td style="height: 10px;"></td>
    </tr>
    </table>
    <div style="height: 25px;"></div>
</body>
</html>