<!--
Form Id : REP1
Date    : 09-01-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.CaseDetails"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException" import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Query Tool - [Sapher]</title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <!--Calendar Library Includes.. S -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E -->

        <script type="text/javascript" src="libjs/gui.js"></script>
        <script type="text/javascript" src="libjs/ajax/ajax.js"></script>

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

        <script type="text/javascript" src="libjs/pgQueryTool.js"></script>

    </head>

    <body onload="init();">

        <%!    private final String FORM_ID = "REP1";
    private int intRepNum = -1;
    private String strRepNum = "";
    private int nCaseSuspended = 0;
    private String strRepType = "";
    private String strProdCode = "";
    private String strGenericNm = "";
    private String strBrandNm = "";
    private String strReporter = "";
    private String strCountry = "";
    private String strTrial = "";
    private String strClasfCode = "";
    private String strDoseUnit = "";
    private String strDose = "";
    private String strIndication = "";
    private String strLLTCode = "";
    private String strOutcomeCode = "";
    private String strPTCode = "";
    private String strCoAssCode = "";
    private String strBodySysNum = "";
    private String strSexCode = "";
    private String strPhyAssCode = "";
    private String strManufCode = "";
    private String strPatNum = "";
    private String strRepNumStart = "";
    private String strRepNumEnd = "";
    private String strAgeStart = "";
    private String strAgeEnd = "";
    private String strTreatStart = "";
    private String strTreatEnd = "";
    private String strEvtStart = "";
    private String strEvtEnd = "";
    private String strTimeSpanStart = "";
    private String strTimeSpanEnd = "";
    private String strRepNumCondition = "=";
    private String strAgeCondition = "=";
    private String strDurTreatmentCondition = "=";
    private String strDurEvtCondition = "=";
    private String strTimeSpanCondition = "=";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    //private int nRowCount = 10;
    //private int nPageCount;
    //private int nCurPage;
    private Inventory inv = null;
    //private String strMessageInfo ="";
%>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgQueryTool.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }

            if (request.getParameter("Cancellation") != null) {
                session.removeAttribute("DirtyFlag");
            }

            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        String strSuspended = "";

        if (strSuspended.equals("Valid")) {
            nCaseSuspended = 0;
        } else if (strSuspended.equals("Suspended")) {
            nCaseSuspended = 1;
        } else if (strSuspended.equals("All")) {
            nCaseSuspended = -1;
        }

        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Query Tool</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form name="FrmQueryTool" METHOD="POST" ACTION="pgPrintQueryTool.jsp">
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                    <tr>
                        <td class="form_group_title" colspan="10">Value parameters</td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="txtProdCode">Prod Details</label></td>
                        <td class="field" colspan="9">

                            <input type="text" id="txtProdCode" name="txtProdCode" size="70" onkeyup="ajax_showOptions('txtProdCode_ID',this,'Product',event)">
                            <input type="hidden" id="txtProdCode_ID" name="txtProdCode_ID" value="">

                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtReporter">Reporter</label></td>
                        <td class="field" colspan="3">

                            <input type="text" id="txtReporter" name="txtReporter" size="30" onkeyup="ajax_showOptions('txtReporter_ID',this,'ReportSource',event)">
                            <input type="hidden" id="txtReporter_ID" name="txtReporter_ID" value="">

                        </td>
                        <td class="fLabelR" ><label for="txtCountry">Country</label></td>
                        <td class="field" colspan="4">
                            <input type="text" id="txtCountry" name="txtCountry" size="30" onkeyup="ajax_showOptions('txtCountry_ID',this,'Country',event)">
                            <input type="hidden" id="txtCountry_ID" name="txtCountry_ID" value="">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtTrial">Source/Trial ID</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtTrial" name="txtTrial" size="30" onkeyup="ajax_showOptions('txtTrial_ID',this,'TrialNo',event)">
                            <input type="hidden" id="txtTrial_ID" name="txtTrial_ID" value="">

                        </td>
                        <td class="fLabelR" ><label for="txtClasfCode">Seriousness</label></td>
                        <td class="field" colspan="4">
                            <input type="text" id="txtClasfCode" name="txtClasfCode" size="30" onkeyup="ajax_showOptions('txtClasfCode_ID',this,'Seriousness',event)">
                            <input type="hidden" id="txtClasfCode_ID" name="txtClasfCode_ID" value="">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtDoseUnit">Dose Unit</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtDoseUnit" name="txtDoseUnit" size="30" onkeyup="ajax_showOptions('txtDoseUnit_ID',this,'DoseUnits',event)">
                            <input type="hidden" id="txtDoseUnit_ID" name="txtDoseUnit_ID" value="">
                        </td>
                        <td class="fLabelR" ><label for="txtDose">Dose</label></td>
                        <td class="field" colspan="4">
                            <input type="text" name="txtDose" id="txtDose" size="30" maxlength="20" title="Dose"
                                   clear=6 onkeydown="return ClearInput(event,6);">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtIndication">Indication</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtIndication" name="txtIndication" size="30" onkeyup="ajax_showOptions('txtIndication_ID',this,'IndicationTreatment',event)">
                            <input type="hidden" id="txtIndication_ID" name="txtIndication_ID" value="">
                        </td>
                        <td class="fLabelR" ><label for="txtLLTCode">Adverse Reaction - LLT</label></td>
                        <td class="field" colspan="4">
                            <input type="text" id="txtLLTCode" name="txtLLTCode" size="30" onkeyup="ajax_showOptions('txtLLTCode_ID',this,'AELLT',event)">
                            <input type="hidden" id="txtLLTCode_ID" name="txtLLTCode_ID" value="">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtOutcomeCode">Outcome</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtOutcomeCode" name="txtOutcomeCode" size="30" onkeyup="ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)">
                            <input type="hidden" id="txtOutcomeCode_ID" name="txtOutcomeCode_ID" value="">
                        </td>
                        <td class="fLabelR" ><label for="txtPTCode">Adverse Reaction - PT</label></td>
                        <td class="field" colspan="4">
                            <input type="text" id="txtPTCode" name="txtPTCode" size="30" onkeyup="ajax_showOptions('txtPTCode_ID',this,'AEPT',event)">
                            <input type="hidden" id="txtPTCode_ID" name="txtPTCode_ID" value="">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtSexCode">Sex</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtSexCode" name="txtSexCode" size="30" onkeyup="ajax_showOptions('txtSexCode_ID',this,'Sex',event)">
                            <input type="hidden" id="txtSexCode_ID" name="txtSexCode_ID" value="">
                        </td>
                        <td class="fLabelR" ><label for="txtBodySysNum">System Organ Class</label></td>
                        <td class="field" colspan="4">
                            <input type="text" id="txtBodySysNum" name="txtBodySysNum" size="30" onkeyup="ajax_showOptions('txtBodySysNum_ID',this,'BodySys',event)">
                            <input type="hidden" id="txtBodySysNum_ID" name="txtBodySysNum_ID" value="">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtCoAssCode">Company Assessment</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtCoAssCode" name="txtCoAssCode" size="30" onkeyup="ajax_showOptions('txtCoAssCode_ID',this,'AssCausality',event)">
                            <input type="hidden" id="txtCoAssCode_ID" name="txtCoAssCode_ID" value="">
                        </td>
                        <td class="fLabelR"><label for="txtPhyAssCode">Physician Assessment</label></td>
                        <td class="field" colspan="4">
                            <input type="text" id="txtPhyAssCode" name="txtPhyAssCode" size="30" onkeyup="ajax_showOptions('txtPhyAssCode_ID',this,'AssCausality',event)">
                            <input type="hidden" id="txtPhyAssCode_ID" name="txtPhyAssCode_ID" value="">
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel"><label for="txtManufCode">Manufacturer Code</label></td>
                        <td class="field" colspan="3">
                            <input type="text" id="txtManufCode" name="txtManufCode" size="30" onkeyup="ajax_showOptions('txtManufCode_ID',this,'Manufacturer',event)">
                            <input type="hidden" id="txtManufCode_ID" name="txtManufCode_ID" value="">
                        </td>
                        <td class="fLabelR" ><label for="txtPatNum">Patient Num/Center</label></td>
                        <td class="field" colspan="4">
                            <input type="text" name="txtPatNum" id="txtPatNum" size="30" maxlength="20" title="Patient Num/Center"
                                   clear=16 readonly onkeydown="return ClearInput(event,16);">
                            <a href="#" onclick="popupWnd('spopup','no','pgSearchPatientNum.jsp?FormId=<%=FORM_ID%>', 280, 475)">
                                <img src="images/window.gif" alt="Select" title="Select Patient Num/Center" border='0' name="imgSearch">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="fLabel" width="125"><label for="cmbSuspendedCase">Validity</label></td>
                        <td class="field" colspan="2">
                            <span style="float: left">
                                <select id="cmbSuspendedCase" NAME="cmbSuspendedCase" class="comboclass" style="width: 100px;" title="Status">
                                    <option <%=(strSuspended.equals("All") ? "SELECTED" : "")%>>All</option>
                                    <option <%=(strSuspended.equals("Valid") ? "SELECTED" : "")%>>Active</option>
                                    <option <%=(strSuspended.equals("Suspended") ? "SELECTED" : "")%>>Inactive</option>
                                </select>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 5px;"></td>
                    </tr>
                    <tr>
                        <td class="form_group_title" colspan="10">Range parameters</td>
                    </tr>
                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>
                    <!--tr>
                        <td class="fLabel" width="125"><label>Report Num</label></td>
                        <td class="field" colspan="9">
                            <select id="cmbRepNumCondition" name="cmbRepNumCondition" class="comboclass" style="width: 60px;" title="Report Num Criteria" onchange="setupCtrlsWithCombo('cmbRepNumCondition', 'txtRepNumStart', 'txtRepNumEnd','','');" ONFOCUS="setupCtrlsWithCombo('cmbRepNumCondition', 'txtRepNumStart', 'txtRepNumEnd','','');">
                                <option <!=(strRepNumCondition.equals(">=") ? "SELECTED" : "")%>>&gt;=</option>
                                <option <!=(strRepNumCondition.equals("<=") ? "SELECTED" : "")%>>&lt;=</option>
                                <option <!=(strRepNumCondition.equals("Range") ? "SELECTED" : "")%>>Range</option>
                            </select>
                            <input type="text" name="txtRepNumStart" id="txtRepNumStart" size="28" maxlength="9" title="Report Number"
                                   readonly/>
                            <a href="#" onclick="if(!document.getElementById('txtRepNumStart').disabled) {click_root='S';popupWnd('spopup','no','pgSearchRepNum.jsp?FormId=<%=FORM_ID%>', 280, 475);}">
                                <img src="images/window.gif" alt="Select" title="Select Report Number" border='0' name="imgSearch">
                            </a>
                            <label for="txtRepNumEnd">To</label>
                            <input type="text" name="txtRepNumEnd" id="txtRepNumEnd" size="29" maxlength="9" title="Report Number"
                                   readonly />
                            <a href="#" onclick="if(!document.getElementById('txtRepNumEnd').disabled) {click_root='E';popupWnd('spopup','no','pgSearchRepNum.jsp?FormId=<%=FORM_ID%>', 280, 475);}">
                                <img src="images/window.gif" alt="Select" title="Select Report Number" border='0' name="imgSearch">
                            </a>
                        </td>
                    </tr-->
                    <tr>
                    <td class="fLabel" width="125"><label>Time span</label></td>
                    <td class="field" colspan="9">
                        <select id="cmbTimeSpanCondition" name="cmbTimeSpanCondition" class="comboclass" style="width: 60px;" title="Time span Criteria" onchange="setupCtrlsWithCombo('cmbTimeSpanCondition', 'txtTimeSpanStart', 'txtTimeSpanEnd','cal_trigger1','cal_trigger2');" ONFOCUS="setupCtrlsWithCombo('cmbTimeSpanCondition', 'txtTimeSpanStart', 'txtTimeSpanEnd','cal_trigger1','cal_trigger2');">
                            <option <%=(strTimeSpanCondition.equals(">=") ? "SELECTED" : "")%>>>=</option>
                            <option <%=(strTimeSpanCondition.equals("<=") ? "SELECTED" : "")%>><=</option>
                            <option <%=(strTimeSpanCondition.equals("Range") ? "SELECTED" : "")%>>Range</option>
                        </select>
                        <input type="text" name="txtTimeSpanStart" id="txtTimeSpanStart" size="28" maxlength="11" title="Time span Start"
                               clear=17 value="<% %>" readonly onkeydown="ClearInput(event,17);">
                        <img src="images/icons/cal.gif" alt="Select" title="Select" border='0' name="imgCal" id="cal_trigger1" style="cursor:pointer;visibility:hidden;">
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtTimeSpanStart", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_trigger1", //*
                                step : 1
                            });
                        </script>
                        <label>To</label>
                        <input type="text" name="txtTimeSpanEnd" id="txtTimeSpanEnd" size="29" maxlength="11" title="Time span End"
                               value="<% %>" readonly
                               clear=18 onkeydown="ClearInput(event,18);">
                        <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" alt="Select" title="Select" border='0' name="imgCal"></a>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField : "txtTimeSpanEnd", //*
                                ifFormat : "%d-%b-%Y",
                                showsTime : true,
                                button : "cal_trigger2", //*
                                step : 1
                            });
                        </script>
                    </td>
                </td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label>Patient Age</label></td>
                <td class="field" colspan="9">
                    <select id="cmbAgeCondition" NAME="cmbAgeCondition" class="comboclass" style="width: 60px;" title="Age Criteria" onchange="setupCtrlsWithCombo('cmbAgeCondition', 'txtAgeStart', 'txtAgeEnd','','');" ONFOCUS="setupCtrlsWithCombo('cmbAgeCondition', 'txtAgeStart', 'txtAgeEnd','','');">
                        <option <%=(strAgeCondition.equals(">=") ? "SELECTED" : "")%>>>=</option>
                        <option <%=(strAgeCondition.equals("<=") ? "SELECTED" : "")%>><=</option>
                        <option <%=(strAgeCondition.equals("Range") ? "SELECTED" : "")%>>Range</option>
                    </select>
                    <input type="text" name="txtAgeStart" id="txtAgeStart" size="6" maxlength="5" title="Age"/>
                    <label for="txtProdCode">To</label>
                    <input type="text" name="txtAgeEnd" id="txtAgeEnd" size="6" maxlength="5" title="Age"/>
                </td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label>Duration for Treatment</label></td>
                <td class="field" colspan="9">
                    <select id="cmbDurTreatmentCondition" NAME="cmbDurTreatmentCondition" class="comboclass" title="Duration Criteria" style="width: 60px;" onchange="setupCtrlsWithCombo('cmbDurTreatmentCondition', 'txtTreatStart', 'txtTreatEnd','','');" ONFOCUS="setupCtrlsWithCombo('cmbDurTreatmentCondition', 'txtTreatStart', 'txtTreatEnd','','');">
                        <option <%=(strDurTreatmentCondition.equals(">=") ? "SELECTED" : "")%>>>=</option>
                        <option <%=(strDurTreatmentCondition.equals("<=") ? "SELECTED" : "")%>><=</option>
                        <option <%=(strDurTreatmentCondition.equals("Range") ? "SELECTED" : "")%>>Range</option>
                    </select>
                    <input type="text" name="txtTreatStart" id="txtTreatStart" size="6" maxlength="5" title="Duration"/>
                    <label for="txtProdCode">To</label>
                    <input type="text" name="txtTreatEnd" id="txtTreatEnd" size="6" maxlength="5" title="Duration"
                           readonly>
                </td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label>Duration of Event</label></td>
                <td class="field" colspan="9">
                    <select id="cmbDurEvtCondition" NAME="cmbDurEvtCondition" class="comboclass" title="Duration Criteria" style="width: 60px;" onchange="setupCtrlsWithCombo('cmbDurEvtCondition', 'txtEvtStart', 'txtEvtEnd','','');" ONFOCUS="setupCtrlsWithCombo('cmbDurEvtCondition', 'txtEvtStart', 'txtEvtEnd','','');">
                        <option <%=(strDurEvtCondition.equals(">=") ? "SELECTED" : "")%>>>=</option>
                        <option <%=(strDurEvtCondition.equals("<=") ? "SELECTED" : "")%>><=</option>
                        <option <%=(strDurEvtCondition.equals("Range") ? "SELECTED" : "")%>>Range</option>
                    </select>
                    <input type="text" name="txtEvtStart" id="txtEvtStart" size="6" maxlength="5" title="Duration"/>
                    <label for="txtProdCode">To</label>
                    <input type="text" name="txtEvtEnd" id="txtEvtEnd" size="6" maxlength="5" />
                </td>
            </tr>
            <tr><td style="height: 5px;"></td></tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="cmdGenerate" class="button" value="Generate" title="Generate PDF report with given criteria"
                           onclick="return validateInputs();" style="width: 87px;">
                </td>
            </tr>
        </table>
        </td>
        </form>
        </tr>
        <!-- Form Body Ends -->

        </table>
        <div style="height: 10px;"></div>
    </body>
</html>