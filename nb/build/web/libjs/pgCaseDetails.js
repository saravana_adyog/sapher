var SAPHER_PARENT_NAME = "pgCaseDetails";

function validate() {
    // Value-Code integrity checks
    if( (document.FrmCaseDetails.txtCaseDRRTypeCode.value != '') &&
        (document.FrmCaseDetails.txtCaseDRRTypeCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtCaseDRRTypeCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtCountryCode.value != '') &&
        (document.FrmCaseDetails.txtCountryCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtCountryCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtReporterStat.value != '') &&
        (document.FrmCaseDetails.txtReporterStat_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtReporterStat.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtReporter.value != '') &&
        (document.FrmCaseDetails.txtReporter_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtReporter.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtProdCode.value != '') &&
        (document.FrmCaseDetails.txtProdCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtProdCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtCpnyCode.value != '') &&
        (document.FrmCaseDetails.txtCpnyCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtCpnyCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtFormulationCode.value != '') &&
        (document.FrmCaseDetails.txtFormulationCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtFormulationCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtDoseUnit.value != '') &&
        (document.FrmCaseDetails.txtDoseUnit_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtDoseUnit.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtDRegimenCode.value != '') &&
        (document.FrmCaseDetails.txtDRegimenCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtDRegimenCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtDoseFreq.value != '') &&
        (document.FrmCaseDetails.txtDoseFreq_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtDoseFreq.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtRouteCode.value != '') &&
        (document.FrmCaseDetails.txtRouteCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtRouteCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtTratmentIndicationCode.value != '') &&
        (document.FrmCaseDetails.txtTratmentIndicationCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtTratmentIndicationCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtRechallengeResult.value != '') &&
        (document.FrmCaseDetails.txtRechallengeResult_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtRechallengeResult.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtSexCode.value != '') &&
        (document.FrmCaseDetails.txtSexCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtSexCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtEthnicCode.value != '') &&
        (document.FrmCaseDetails.txtEthnicCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtEthnicCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtOutcomeCode.value != '') &&
        (document.FrmCaseDetails.txtOutcomeCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtOutcomeCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtPhyAssCode.value != '') &&
        (document.FrmCaseDetails.txtPhyAssCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtPhyAssCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtCoAssCode.value != '') &&
        (document.FrmCaseDetails.txtCoAssCode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtCoAssCode.focus();
        return false;
    }
    
    if( (document.FrmCaseDetails.txtLHACode.value != '') &&
        (document.FrmCaseDetails.txtLHACode_ID.value == '')) {
        window.alert('Please select a valid data.');
        document.FrmCaseDetails.txtLHACode.focus();
        return false;
    }
    
    // text field validations
    if(checkIsIllegalChar(document.FrmCaseDetails.txtRecdHq.value)) {
        window.alert('Invalid character in Received by field');
        document.getElementById('txtRecdHq').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtLocalRep.value)) {
        window.alert('Invalid character in Local Report number');
        document.getElementById('txtLocalRep').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtCrossRef.value)) {
        window.alert('Invalid character in cross Ref. number');
        document.getElementById('txtCrossRef').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtClinRep.value)) {
        window.alert('Invalid character in clinical reporter information');
        document.getElementById('txtClinRep').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtCpnyRepInfo.value)) {
        window.alert('Invalid character in Company Reporter Information');
        document.getElementById('txtCpnyRepInfo').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtPatNum.value)) {
        window.alert('Invalid character in Patient Number');
        document.getElementById('txtPatNum').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtHospitalName.value)) {
        window.alert('Invalid character in Hospital Name');
        document.getElementById('txtHospitalName').focus();
        return false;
    }
    if(!isAlphaNumeric(document.FrmCaseDetails.txtBatchNum.value)) {
        window.alert('Invalid character in Batch Number');
        document.getElementById('txtBatchNum').focus();
        return false;
    }
    if(!checkIsNumericDecVar(document.FrmCaseDetails.txtDose.value)) {
        window.alert('Invalid character in dose');
        document.getElementById('txtDose').focus();
        return false;
    }
    if(!checkIsNumericDecVar(document.FrmCaseDetails.txtDuration.value)) {
        window.alert('Invalid character in Duration');
        document.getElementById('txtDuration').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtAuthNum.value)) {
        window.alert('Invalid character in Authorization Num');
        document.getElementById('txtAuthNum').focus();
        return false;
    }
    if(!isAlphaNumericSpace(document.FrmCaseDetails.txtRechallengeDose.value)) {
        window.alert('Invalid character in Rechallenge Dose');
        document.getElementById('txtRechallengeDose').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtPatientInitials.value)) {
        window.alert('Invalid character in Patient Initials');
        document.getElementById('txtPatientInitials').focus();
        return false;
    }
    if(!checkIsNumericOnlyVar(document.FrmCaseDetails.txtAgeOnset.value)) {
        window.alert('Invalid character in Age Onset');
        document.getElementById('txtAgeOnset').focus();
        return false;
    }
    if(!checkIsNumericOnlyVar(document.FrmCaseDetails.txtAgeRep.value)) {
        window.alert('Invalid character in Age Reported');
        document.getElementById('txtAgeRep').focus();
        return false;
    }
    if(!checkIsNumericDecVar(document.FrmCaseDetails.txtWeight.value)) {
        window.alert('Invalid character in Weight');
        document.getElementById('txtWeight').focus();
        return false;
    }
    if(!checkIsNumericDecVar(document.FrmCaseDetails.txtHeight.value)) {
        window.alert('Invalid character in Height');
        document.getElementById('txtHeight').focus();
        return false;
    }
    if(!checkIsNumericOnlyVar(document.FrmCaseDetails.txtGestWeek.value)) {
        window.alert('Invalid character in Gestation Week');
        document.getElementById('txtGestWeek').focus();
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtContribFactor.value)) {
        window.alert('Invalid character in Contributing Factor');
        document.getElementById('txtContribFactor').focus();
        return false;
    }
    
    // validate long txt fields
    if(!validateAll())
        return false;
    //\\
    if (document.FrmCaseDetails.chkPregnant.checked) {
        if(document.FrmCaseDetails.txtSexCode_ID.value=="M" || document.FrmCaseDetails.txtSexCode_ID.value=="m") {
            alert('Invalid Sex-Pregnancy selection.');
            return false;
        }
        if( !(document.FrmCaseDetails.txtGestWeek.value>0 && document.FrmCaseDetails.txtGestWeek.value<42) ) {
            alert('Invalid gestation value');
            return false;
        }
    }
    return true;
}

function validateAeFields() {
    if (!checkCharCountVerbatim())
    { return false; }
    if (!checkCharCountAEDesc())
    { return false; }
    return true;
}

function validateCmFields() {
    if (!checkCharCountRelevantHistory())
    { return false; }
    if (!checkCharCountShortComment())
    { return false; }
    return true;
}

function checkCharCountPhyAssReason() {
    var error = "";
    var maxlength = 1000;
    if (document.FrmCaseDetails.txtPhyAssReason.value.length > maxlength) {
        error = "Physician's assessment reason must be less than " + maxlength + " characters.";
        document.FrmCaseDetails.txtPhyAssReason.focus();
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtPhyAssReason.value)) {
        window.alert('Invalid character in Physician assessment reason');
        document.getElementById('txtPhyAssReason').focus();
        return false;
    }
    return true;
}

function checkCharCountCoAssReason() {
    var error = "";
    var maxlength = 1000;
    if (document.FrmCaseDetails.txtCoAssReason.value.length > maxlength) {
        error = "Company's assessment reason must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtCoAssReason.value)) {
        window.alert('Invalid character in Company assessment reason');
        document.getElementById('txtCoAssReason').focus();
        return false;
    }
    return true;
}

function checkCharCountVerbatim() {
    var error = "";
    var maxlength = 1000;
    if (document.FrmCaseDetails.txtReactionDesc.value.length > maxlength) {
        error = "Reaction Description must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtReactionDesc.value)) {
        window.alert('Invalid character in Reaction Description');
        document.getElementById('txtReactionDesc').focus();
        return false;
    }
    return true;
}

function checkCharCountAEDesc() {
    var error = "";
    var maxlength = 4000;
    if (document.FrmCaseDetails.txtExtendedInfo.value.length > maxlength) {
        error = "Extended Information must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtExtendedInfo.value)) {
        window.alert('Invalid character in Extended Information');
        document.getElementById('txtExtendedInfo').focus();
        return false;
    }
    return true;
}

function checkCharCountFurtherComm() {
    var error = "";
    var maxlength = 500;
    if (document.FrmCaseDetails.txtFurtherCommunication.value.length > maxlength) {
        error = "Further Communication must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtFurtherCommunication.value)) {
        window.alert('Invalid character in Further Communication');
        document.getElementById('txtFurtherCommunication').focus();
        return false;
    }
    return true;
}

function checkCharCountAutopsyResult() {
    var error = "";
    var maxlength = 500;
    if (document.FrmCaseDetails.txtAutopsyResult.value.length > maxlength) {
        error = "Autopsy Result must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtAutopsyResult.value)) {
        window.alert('Invalid character in Autopsy Result');
        document.getElementById('txtAutopsyResult').focus();
        return false;
    }
    return true;
}

function checkCharCountRelevantHistory() {
    var error = "";
    var maxlength = 1000;
    if (document.FrmCaseDetails.txtRelevantHistory.value.length > maxlength) {
        error = "Relevant History must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtRelevantHistory.value)) {
        window.alert('Invalid character in Relevant History');
        document.getElementById('txtRelevantHistory').focus();
        return false;
    }
    return true;
}

function checkCharCountShortComment() {
    var error = "";
    var maxlength = 500;
    if (document.FrmCaseDetails.txtShortComment.value.length > maxlength) {
        error = "Short Comment must be less than " + maxlength + " characters.";
    }
    if (error) {
        alert(error);
        return false;
    }
    if(checkIsIllegalChar(document.FrmCaseDetails.txtShortComment.value)) {
        window.alert('Invalid character in Short Comment');
        document.getElementById('txtShortComment').focus();
        return false;
    }
    return true;
}


function validateAll() {
    if (!checkCharCountPhyAssReason())
    {
        return false;
    }
    if (!checkCharCountCoAssReason())
    {
        return false;
    }
    
    if (!checkCharCountFurtherComm())
    {
        return false;
    }
    if (!checkCharCountAutopsyResult())
    {
        return false;
    }
    
    return true;
}

/////////////////////////////////////////////////////////////////

function setRechallengeControls() {
    if (document.FrmCaseDetails.chkRechallenge.checked) {
        document.FrmCaseDetails.txtRechallengeDate.disabled = false;
        //document.FrmCaseDetails.imgCalRechDate.style.display = 'hidden';
        Calendar.setup({
            inputField : "txtRechallengeDate", //*
            ifFormat : "%d-%b-%Y",
            button : "calRechDate", //*
            step : 1
        });
        document.FrmCaseDetails.txtRechallengeDose.disabled = false;
        document.FrmCaseDetails.txtRechallengeResult_ID.disabled = false;
    //document.FrmCaseDetails.txtRechallengeResultDesc.disabled = false;
    }
    else {
        document.FrmCaseDetails.txtRechallengeDate.value = "";
        document.FrmCaseDetails.txtRechallengeDate.disabled = true;
        //document.FrmCaseDetails.imgCalRechDate.style.display = true;
        Calendar.setup({
            displayStat : "D",
            inputField : "txtRechallengeDate", //*
            ifFormat : "%d-%b-%Y",
            button : "calRechDate", //*
            step : 1
        });
        document.FrmCaseDetails.txtRechallengeDose.value = "";
        document.FrmCaseDetails.txtRechallengeDose.disabled = true;
        document.FrmCaseDetails.txtRechallengeResult_ID.value = "";
        //document.FrmCaseDetails.txtRechallengeResultDesc.value = "";
        document.FrmCaseDetails.txtRechallengeResult_ID.disabled = true;
    //document.FrmCaseDetails.txtRechallengeResultDesc.disabled = true;
    //document.FrmCaseDetails.calRechDate.onclick = funciton(event){return false;};
    }
}

function setAutopsyControls() {
    if (document.FrmCaseDetails.chkAutopsy.checked) {
        document.FrmCaseDetails.txtAutopsyResult.disabled = false;
    }
    else {
        document.FrmCaseDetails.txtAutopsyResult.value = "";
        document.FrmCaseDetails.txtAutopsyResult.disabled = true;
    }
}

function setPregControls() {
    if (document.FrmCaseDetails.chkPregnant.checked) {
        document.FrmCaseDetails.txtGestWeek.disabled = false;
    }
    else {
        document.FrmCaseDetails.txtGestWeek.value = "";
        document.FrmCaseDetails.txtGestWeek.disabled = true;
    }
}

function setAllControls() {
    setRechallengeControls();
    setAutopsyControls();
    setPregControls();
    dateDifference_new('txtDoB', 'txtRecvDate', 'txtAgeOnset');
}

function confirmSave() {
    if(( (document.FrmCaseDetails.txtEntryDate.value == '') &&
        (document.FrmCaseDetails.txtRecdHq.value == '') &&
        (document.FrmCaseDetails.txtRecvDate.value == '') &&
        (document.FrmCaseDetails.txtLocalRep.value == '') &&
        (document.FrmCaseDetails.txtCrossRef.value == '') &&
        
        (document.FrmCaseDetails.txtCaseDRRTypeCode_ID.value == '') &&
        (document.FrmCaseDetails.txtCountryCode_ID.value == '') &&
        (document.FrmCaseDetails.txtReporter_IDStat_ID.value == '') &&
        (document.FrmCaseDetails.txtClinRep.value == '') &&
        (document.FrmCaseDetails.txtPatNum.value == '') &&
        
        (document.FrmCaseDetails.txtReporter_ID.value == '') &&
        (document.FrmCaseDetails.txtTrialNum.value == '') &&
        (document.FrmCaseDetails.txtHospitalName.value == '') &&
        (document.FrmCaseDetails.txtReporter_ID.value == '') &&
        (document.FrmCaseDetails.txtProdCode_ID.value == '') &&
        
        (document.FrmCaseDetails.txtCpnyCode_ID.value == '') &&
        (document.FrmCaseDetails.txtFormulationCode_ID.value == '') &&
        (document.FrmCaseDetails.txtDoseUnit_ID.value == '') &&
        (document.FrmCaseDetails.txtTreatmentStartDate.value == '') &&
        (document.FrmCaseDetails.txtDuration.value == '') &&
        
        (document.FrmCaseDetails.txtDoseFreq_ID.value == '') &&
        (document.FrmCaseDetails.txtTratmentIndicationCode_ID.value == '') &&
        (document.FrmCaseDetails.txtAuthNum.value == '') &&
        /* (document.FrmCaseDetails.txtRechallengeDate.value == '') && */
        /* (document.FrmCaseDetails.txtRechallengeDose.value == '') && */
        
        /* (document.FrmCaseDetails.txtRechallengeResult_ID.value == '') && */
        (document.FrmCaseDetails.txtBatchNum.value == '') &&
        (document.FrmCaseDetails.txtDose.value == '') &&
        (document.FrmCaseDetails.txtDRegimenCode_ID.value == '') &&
        (document.FrmCaseDetails.txtRouteCode_ID.value == '') &&
        
        (document.FrmCaseDetails.txtTreatmentEndDate.value == '') &&
        (document.FrmCaseDetails.txtPatientInitials.value == '') &&
        (document.FrmCaseDetails.txtDoB.value == '') &&
        (document.FrmCaseDetails.txtAgeRep.value == '') &&
        (document.FrmCaseDetails.txtHeight.value == '') &&
        
        /* (document.FrmCaseDetails.txtGestWeek.value == '') && */
        (document.FrmCaseDetails.txtSexCode_ID.value == '') &&
        (document.FrmCaseDetails.txtAgeOnset.value == '') &&
        (document.FrmCaseDetails.txtWeight.value == '') &&
        (document.FrmCaseDetails.txtEthnicCode_ID.value == '') &&
        
        (document.FrmCaseDetails.txtOutcomeCode_ID.value == '') &&
        (document.FrmCaseDetails.txtDateOfOutcome.value == '') &&
        (document.FrmCaseDetails.txtPhyAssCode_ID.value == '') &&
        (document.FrmCaseDetails.txtPhyAssReason.value == '') &&
        (document.FrmCaseDetails.txtContribFactor.value == '') &&
        
        (document.FrmCaseDetails.txtCoAssCode_ID.value == '') &&
        (document.FrmCaseDetails.txtCoAssReason.value == '') &&
        (document.FrmCaseDetails.txtDateOfPhyAssessment.value == '') &&
        (document.FrmCaseDetails.txtDateOfCoAssessment.value == '') &&
        (document.FrmCaseDetails.txtDistribnCpny.value == '') &&
        
        (document.FrmCaseDetails.txtSubmissionDate.value == '') &&
        (document.FrmCaseDetails.txtLHACode_ID.value == '') &&
        (document.FrmCaseDetails.txtFurtherCommunication.value == '') &&
        (document.FrmCaseDetails.txtDistribnDate.value == '') &&
        /* (document.FrmCaseDetails.txtAutopsyResult.value == '') && */
        
        (document.FrmCaseDetails.txtRelevantHistory.value == '') &&
        (document.FrmCaseDetails.txtShortComment.value == '') &&
        (document.FrmCaseDetails.txtReactionDesc.value == '') &&
        (document.FrmCaseDetails.txtExtendedInfo.value == '') 
        )) {
        return confirm('Are you sure you want to save an empty entry?','Sapher');
    } else
        return true;
}

/**
 * startDt, endDt MUST BE IN DD-MMM-YYYY FORMAT.
 */
function dateDifference(startDt, endDt) {
    /* startDt, endDt MUST BE IN DD-MMM-YYYY FORMAT. */
    document.FrmCaseDetails.txtDuration.value = null;
    if(startDt!="" && endDt!="") {
        var start_day = startDt.substring(0,2);
        var start_month = '';
        if(startDt.substring(3,6) == "Jan")
            start_month = 0;
        else if(startDt.substring(3,6) == "Feb")
            start_month = 1;
        else if(startDt.substring(3,6) == "Mar")
            start_month = 2;
        else if(startDt.substring(3,6) == "Apr")
            start_month = 3;
        else if(startDt.substring(3,6) == "May")
            start_month = 4;
        else if(startDt.substring(3,6) == "Jun")
            start_month = 5;
        else if(startDt.substring(3,6) == "Jul")
            start_month = 6;
        else if(startDt.substring(3,6) == "Aug")
            start_month = 7;
        else if(startDt.substring(3,6) == "Sep")
            start_month = 8;
        else if(startDt.substring(3,6) == "Oct")
            start_month = 9;
        else if(startDt.substring(3,6) == "Nov")
            start_month = 10;
        else if(startDt.substring(3,6) == "Dec")
            start_month = 11;
        var start_year = startDt.substring(7,11);
        
        var end_day = endDt.substring(0,2);
        var end_month = '';
        if(endDt.substring(3,6) == "Jan")
            end_month = 0;
        else if(endDt.substring(3,6) == "Feb")
            end_month = 1;
        else if(endDt.substring(3,6) == "Mar")
            end_month = 2;
        else if(endDt.substring(3,6) == "Apr")
            end_month = 3;
        else if(endDt.substring(3,6) == "May")
            end_month = 4;
        else if(endDt.substring(3,6) == "Jun")
            end_month = 5;
        else if(endDt.substring(3,6) == "Jul")
            end_month = 6;
        else if(endDt.substring(3,6) == "Aug")
            end_month = 7;
        else if(endDt.substring(3,6) == "Sep")
            end_month = 8;
        else if(endDt.substring(3,6) == "Oct")
            end_month = 9;
        else if(endDt.substring(3,6) == "Nov")
            end_month = 10;
        else if(endDt.substring(3,6) == "Dec")
            end_month = 11;
        var end_year = endDt.substring(7,11);
        
        var d1 = new Date(start_year,start_month,start_day);
        var d2= new Date(end_year,end_month,end_day);
        
        //Set 1 day in milliseconds
        var one_day=1000*60*60*24;
        
        if(d2.getTime()<d1.getTime())
            return false;
        
        //Calculate difference btw the two dates, and convert to days
        document.FrmCaseDetails.txtDuration.value = (Math.ceil( (d2.getTime()-d1.getTime())/(one_day)) );
        return true;
    } else {
        document.FrmCaseDetails.txtDuration.value = null;
        return true;
    }
}

/**
 * startDt, endDt MUST BE IN DD-MMM-YYYY FORMAT.
 */
function dateDifference_new(ctrl1, ctrl2, targetCtrl) {
    /* startDt, endDt MUST BE IN DD-MMM-YYYY FORMAT. */
    document.getElementById(targetCtrl).value = null;
    
    var startDt = document.getElementById(ctrl1).value;
    var endDt = document.getElementById(ctrl2).value;
    
    if(startDt!="" && endDt!="") {
        var start_day = startDt.substring(0,2);
        var start_month = '';
        if(startDt.substring(3,6) == "Jan")
            start_month = 0;
        else if(startDt.substring(3,6) == "Feb")
            start_month = 1;
        else if(startDt.substring(3,6) == "Mar")
            start_month = 2;
        else if(startDt.substring(3,6) == "Apr")
            start_month = 3;
        else if(startDt.substring(3,6) == "May")
            start_month = 4;
        else if(startDt.substring(3,6) == "Jun")
            start_month = 5;
        else if(startDt.substring(3,6) == "Jul")
            start_month = 6;
        else if(startDt.substring(3,6) == "Aug")
            start_month = 7;
        else if(startDt.substring(3,6) == "Sep")
            start_month = 8;
        else if(startDt.substring(3,6) == "Oct")
            start_month = 9;
        else if(startDt.substring(3,6) == "Nov")
            start_month = 10;
        else if(startDt.substring(3,6) == "Dec")
            start_month = 11;
        var start_year = startDt.substring(7,11);
        
        var end_day = endDt.substring(0,2);
        var end_month = '';
        if(endDt.substring(3,6) == "Jan")
            end_month = 0;
        else if(endDt.substring(3,6) == "Feb")
            end_month = 1;
        else if(endDt.substring(3,6) == "Mar")
            end_month = 2;
        else if(endDt.substring(3,6) == "Apr")
            end_month = 3;
        else if(endDt.substring(3,6) == "May")
            end_month = 4;
        else if(endDt.substring(3,6) == "Jun")
            end_month = 5;
        else if(endDt.substring(3,6) == "Jul")
            end_month = 6;
        else if(endDt.substring(3,6) == "Aug")
            end_month = 7;
        else if(endDt.substring(3,6) == "Sep")
            end_month = 8;
        else if(endDt.substring(3,6) == "Oct")
            end_month = 9;
        else if(endDt.substring(3,6) == "Nov")
            end_month = 10;
        else if(endDt.substring(3,6) == "Dec")
            end_month = 11;
        var end_year = endDt.substring(7,11);
        
        var d1 = new Date(start_year,start_month,start_day);
        var d2= new Date(end_year,end_month,end_day);
        
        //Set 1 year in milliseconds
        var one_year=1000*60*60*24*365;
        
        if(d2.getTime()<d1.getTime())
            return false;
        
        //Calculate difference btw the two dates, and convert to days
        document.getElementById(targetCtrl).value = (Math.ceil( (d2.getTime()-d1.getTime())/(one_year)) );
        return true;
    } else {
        document.getElementById(targetCtrl).value = null;
        return true;
    }
}
