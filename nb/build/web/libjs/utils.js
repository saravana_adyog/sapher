function _el(id){
	return document.getElementById(id);
}

/**
* @name mostrarMenu
* @format function mostrarMenu()
* @comment Muestra u oculta el menu de la ventana de la aplicación.
*
* @return Devuelve un booleano que indica si ha podido procesar la operación o no.
* @example onClick="mostrarMenu();return false;"
*/
function mostrarMenu(id) {
  if (!top.leftNavFrame) window.open("frame.html", "_blank");
  else {
    var frame = top.document;
    var frameset = frame.getElementById("framesetMenu");
    if (!frameset) return false;
      if (frameset.cols.substring(0,1)=="1") {
		frameset.cols = "205,*";
		  //frame.getElementById("leftNavFrame").src="support/leftNav.html";
	  }
      else  {
		  frameset.cols = "10,*";
		  //frame.getElementById("leftNavFrame").src="support/leftNav-show.html";
	  }
    try {
      swapVisibility(id);
    } catch (e) {}
    return true;
  }
}


function swapVisibility(id) {
  var element = _el(id);
  if (element.style.display=="none")
	  element.style.display = "block";
  else{
	  element.style.display =  "none";
  }
 
  return true;
}
