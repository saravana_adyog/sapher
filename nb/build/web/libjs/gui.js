function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function validateEMail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if(reg.test(email) == false) {
        window.alert('fail');
        return false;
    }else {
        window.alert('success');
        return true;
    }
}

function ClearInput(e, strId){
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum == 27) {//ky
        var inp_to_clear = document.getElementsByTagName("input");
        for (var i = 0; i < inp_to_clear.length; i++) {
            if (inp_to_clear[i].getAttribute('clear') == strId) {
                inp_to_clear[i].value = "";
            }
        }
        //Special case for Quick Screen
        if (strId == "10")
            document.FrmQuickScreen.txtClasfType.value="NON SERIOUS";
    }
}

function popupWnd(popupname, resize, urlToOpen, window_width, window_height)
{
    var window_left = (screen.availWidth/2)-(window_width/2);
    var window_top = (screen.availHeight/2)-(window_height/2);
    var winParms = "Status=no" + ",resizable=" + resize + ",scrollbars=yes" + ",height="+window_height+",width="+window_width + ",left="+window_left+",top="+window_top;
    var newwindow = window.open(urlToOpen, popupname, winParms);

    if(!newwindow.opener)
        newwindow.opener = self;

    newwindow.focus();
}
        
function checkCharCount(inp, e, maxlength) {
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) {// Netscape/Firefox/Opera
        keynum = e.which;
    }
    if (!(keynum==37||keynum==39||keynum==46||keynum==36||keynum==35||keynum==8||keynum==38||keynum==40))
        if (inp.value.length > maxlength -1) {
            return false;
        }
    return true;
}
                
function checkIsNumeric(e){
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if ((keynum >= 48 && keynum <= 57)||keynum==8 ||(keynum >= 96 && keynum <= 105)
        ||keynum==37||keynum==39||keynum==46||keynum==36||keynum==35){
        return true;
    }
    return false;
}
                    
function checkIsNumericDec(e){
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if ((keynum >= 48 && keynum <= 57)||(keynum >= 96 && keynum <= 105)
        ||keynum==37||keynum==39||keynum==46||keynum==36||keynum==35||keynum==190||keynum==8){
        return true;
    }
    return false;
}
                        
function checkIsAlpha(e){
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if ((keynum >= 65 && keynum <= 90)||(keynum >= 97 && keynum <= 122)
        ||keynum==8 ||keynum==37||keynum==39||keynum==46||keynum==36||keynum==35){
        return true;
    }
    return false;
}

/* Deprecated. */
function checkIsAlphaSpace(e){
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if ((keynum >= 65 && keynum <= 90)||(keynum >= 97 && keynum <= 122)||keynum==61
        ||keynum==8 ||keynum==37||keynum==39||keynum==46||keynum==36||keynum==35){
        return true;
    }
    return false;
}
                                
/* Deprecated. */
function checkIsAlphaNumeric(e) {
    var keynum;
    if(window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if (keynum!=34||keynum!=39||keynum!=60||keynum!=62){
        return true;
    }
    return false;
}

function checkIsPhoneVar(sText) {
    var ValidChars = "0123456789(+) ";
    var Char;

    for (i = 0; i<sText.length; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            return false;
        }
    }
    return true;
}

function checkIsFaxVar(sText) {
    var ValidChars = "0123456789(+) ";
    var Char;

    for (i = 0; i<sText.length; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            return false;
        }
    }
    return true;
}

function checkIsAlphaVar(sText) {
    var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var Char;

    for (i = 0; i<sText.length; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            return false;
        }
    }
    return true;
}

function checkIsAlphaSpaceVar(sText) {
    var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    var Char;

    for (i = 0; i<sText.length; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            return false;
        }
    }
    return true;
}

function isAlphaNumericSpace(sText) {
    var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 (.,)/%-";
    var Char;

    if(sText.length == 0) {
        return true;
    } else {
        for (i = 0; i<sText.length; i++) { 
            Char = sText.charAt(i); 
            if (ValidChars.indexOf(Char) == -1) {
                return false;
            }
        }
    }
    return true;
}

function isAlphaNumeric(sText) {
    var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-";
    var Char;

    if(sText.length == 0) {
        return true;
    } else {
        for (i = 0; i<sText.length; i++) { 
            Char = sText.charAt(i); 
            if (ValidChars.indexOf(Char) == -1) {
                return false;
            }
        }
    }
    return true;
}

function checkIsNumericOnlyVar(sText) {
    var ValidChars = "0123456789";
    var Char;

    for (i = 0; i < sText.length; i++) { 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            return false;
        }
    }
    return true;
}

function checkIsNumericDecVar(sText) {
    var ValidChars = "0123456789.";
    var Char;
    var dotCount = 0;

    if(sText.length == 0) {
        return true;
    } else {
        for (i = 0; i<sText.length; i++) {
            Char = sText.charAt(i);
            if(Char=='.') {
                dotCount++;
            }
            if (ValidChars.indexOf(Char) == -1 || dotCount>1) {
                return false;
            } 
        }
    }
    return true;
}

function checkIsIllegalChar(sText) {
    var ValidChars = "><'\"";
    var Char;
    if(sText.length == 0) {
        return false;
    } else {
        for (i = 0; i < sText.length; i++) {
            Char = sText.charAt(i); 
            if (ValidChars.indexOf(Char) != -1) {
                return true;
            }
        }
    }
    return false;
}

function validateEMail(sText) {
    var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-@.";
    var Char;
    if(sText.length == 0) {
        return false;
    } else {
        for (i = 0; i < sText.length; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) != -1) {
                return true;
            }
        }
    }
    return false;
}
