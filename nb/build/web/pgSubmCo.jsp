<!--
Form Id : CDL23
Date    : 12-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.SubmCompany"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Submission Company - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />
        <script>var SAPHER_PARENT_NAME = "pgSubmCo";</script>

        <script src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script>
            var nCntryPopupCount = 0;

            function validate(){
                if (trim(document.FrmSCo.txtSCoName.value) == ""){
                    alert( "Company cannot be empty.");
                    document.FrmSCo.txtSCoName.focus();
                    return false;
                }
                if (document.FrmSCo.txtSCoCountryId_ID.value == ""){
                    alert( "Country Code cannot be empty.");
                    document.FrmSCo.txtSCoCountryId.focus();
                    return false;
                }
                if(!checkIsAlphaSpaceVar(document.FrmForm.txtSCoName.value)) {
                    window.alert('Invalid character(s) in submission company.');
                    document.getElementById('txtSCoName').focus();
                    return false;
                }
                return true;
            }
        </script>

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

    </head>
    <body onUnload="nCntryPopupCount = 0;">
        <%!    SubmCompany sCompany = null;
    private final String FORM_ID = "CDL23";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        sCompany = new SubmCompany((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgSubmCo.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("SubmCo") != null) {
                    sCompany = new SubmCompany((String) session.getAttribute("Company"), request.getParameter("SubmCo"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                sCompany = new SubmCompany();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSubmCo.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmSCo" name="FrmSCo" method="POST" action="pgSavSubmCo.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>Submission Company</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtSCoName">Company</label></td>
                                <td class="field"><input type="text" name="txtSCoName" title="Company"
                                                             id="txtSCoName" size="36" maxlength="50"
                                                             value="<%=sCompany.getCompany_Nm()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtSCoCountryId">Country</label></td>
                                <td class="field">

                                    <input type="text" id="txtSCoCountryId" name="txtSCoCountryId" size="30" onKeyUp="ajax_showOptions('txtSCoCountryId_ID',this,'Country',event)"
                                           value="<%
        if (sCompany.getCtry() != null) {
            out.write(sCompany.getCtry().getCountry_Nm());
        } else {
            out.write("");
        }
                                           %>"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           %> />
                                    <input type="text" readonly size="3" id="txtSCoCountryId_ID" name="txtSCoCountryId_ID"
                                           value="<%
        if (sCompany.getCtry() != null) {
            out.write(sCompany.getCtry().getCountry_Code());
        } else {
            out.write("");
        }
                                           %>" />

                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                                             <%
        if (sCompany.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgSubmColist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>