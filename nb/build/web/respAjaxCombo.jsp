<%@page
    import="bl.sapher.general.Constants"
    import="bl.sapher.Country"
    import="bl.sapher.AEOutcome"
    import="bl.sapher.AELLT"
    import="bl.sapher.AEDiag"
    import="bl.sapher.Causality"
    import="bl.sapher.Classification_Seriousness"
    import="bl.sapher.DoseFrequency"
    import="bl.sapher.DoseRegimen"
    import="bl.sapher.DoseUnits"
    import="bl.sapher.EthnicOrigin"
    import="bl.sapher.Event_Severity"
    import="bl.sapher.Formulation"
    import="bl.sapher.Product"
    import="bl.sapher.Sex"
    import="bl.sapher.IndicationTreatment"
    import="bl.sapher.Manufacturer"
    import="bl.sapher.RechallengeResult"
    import="bl.sapher.RepType"
    import="bl.sapher.SourceInfo"
    import="bl.sapher.ReporterStatus"
    import="bl.sapher.LHA"
    import="bl.sapher.SubmCompany"
    import="bl.sapher.TrialNum"
    import="bl.sapher.Body_System"
    import="bl.sapher.Route"

    import="bl.sapher.Role"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"

    import="bl.sapher.general.Constants"
    import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"

    import="java.util.ArrayList"
    %>
<%!    private User currentUser = null;
%>
<%
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));


        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        String strCode = "";
        String strUtilVar = "";

        if (request.getParameter("letters") == null) {
            strCode = "";
        } else {
            strCode = request.getParameter("letters");
        }

// Country
        if (request.getParameter("Country") != null) {
            ArrayList<Country> al = Country.listCountry(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getCountry_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getCountry_Nm() + "|");
                }
                strUtilVar = null;
            }
        } // AE Outcome
        else if (request.getParameter("AEOutcome") != null) {
            ArrayList<AEOutcome> al = AEOutcome.listOutcome(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getOutcome_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getOutcome() + "|");
                }
                strUtilVar = null;
            }
        } // AE Low Level Term
        else if (request.getParameter("AELLT") != null) {
            ArrayList<AELLT> al = AELLT.listLLT(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getLlt_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getLlt_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // AE Preferred Term
        else if (request.getParameter("AEPT") != null) {
            ArrayList<AEDiag> al = AEDiag.listDiag(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getDiag_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getDiag_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Assessment of Causality
        else if (request.getParameter("AssCausality") != null) {
            ArrayList<Causality> al = Causality.listCausality(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, "", Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getAssessment_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getAssessment() + "|");
                }
                strUtilVar = null;
            }
        } // Seriousness
        else if (request.getParameter("Seriousness") != null) {
            ArrayList<Classification_Seriousness> al = Classification_Seriousness.listClasfSeriousness(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getClasf_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getClasf_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Dose Frequency
        else if (request.getParameter("DoseFreq") != null) {
            ArrayList<DoseFrequency> al = DoseFrequency.listDoseFrequency(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getDfreq_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getDfreq_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Dose Regimen
        else if (request.getParameter("DoseRgmn") != null) {
            ArrayList<DoseRegimen> al = DoseRegimen.listDoseRegimen(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, -1, -1, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getDregimen_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getDregimen_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Dose Units
        else if (request.getParameter("DoseUnits") != null) {
            ArrayList<DoseUnits> al = DoseUnits.listDoseUnits(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getUnit_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getUnit_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Ethnic Origin
        else if (request.getParameter("EthnicOrigin") != null) {
            ArrayList<EthnicOrigin> al = EthnicOrigin.listEthnicOrigin(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getEthnic_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getEthnic_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Event Severity
        else if (request.getParameter("EventSeverity") != null) {
            ArrayList<Event_Severity> al = Event_Severity.listESeverity(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getEseverity_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getEseverity_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Formulation
        else if (request.getParameter("Formulation") != null) {
            ArrayList<Formulation> al = Formulation.listFormulation(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getFormulation_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getFormulation_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Product
        else if (request.getParameter("Product") != null) {
            ArrayList<Product> al = Product.listProduct(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getProduct_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getBrand_Nm() + "(" + al.get(i).getGeneric_Nm() + ")" + "|");
                }
                strUtilVar = null;
            }
        } // Sex
        else if (request.getParameter("Sex") != null) {
            ArrayList<Sex> al = Sex.listSex(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getSex_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getSex_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Indication for Treatment
        else if (request.getParameter("IndicationTreatment") != null) {
            ArrayList<IndicationTreatment> al = IndicationTreatment.listIndicationTreatment(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getIndication_Num();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getIndication() + "|");
                }
                strUtilVar = null;
            }
        } // Manufacturer
        else if (request.getParameter("Manufacturer") != null) {
            ArrayList<Manufacturer> al = Manufacturer.listManufacturer(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getM_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getM_Name() + "|");
                }
                strUtilVar = null;
            }
        } // Rechallenge Result
        else if (request.getParameter("RechallengeResult") != null) {
            ArrayList<RechallengeResult> al = RechallengeResult.listRechallengeResult(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getResult_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getResult_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Report Type
        else if (request.getParameter("ReportType") != null) {
            ArrayList<RepType> al = RepType.listRepType(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getType_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getType_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Report Source
        else if (request.getParameter("ReportSource") != null) {
            ArrayList<SourceInfo> al = SourceInfo.listSourceInfo(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getSource_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getSource_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Reporter Status
        else if (request.getParameter("ReporterStatus") != null) {
            ArrayList<ReporterStatus> al = ReporterStatus.listReporterStatus(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getStatus_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getStatus_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Health Authority
        else if (request.getParameter("LHA") != null) {
            ArrayList<LHA> al = LHA.listLHA(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, "", strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getLha_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getLha_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // Submission Company
        else if (request.getParameter("SubmCo") != null) {
            ArrayList<SubmCompany> al = SubmCompany.listSubmCompany(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, "", Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getCompany_Nm();
                    out.write(strUtilVar + "###" + strUtilVar + "|");
                }
                strUtilVar = null;
            }
        } //Trial No
        else if (request.getParameter("TrialNo") != null) {
            ArrayList<TrialNum> al = TrialNum.listTrialNum(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getTrial_Num();
                    out.write(strUtilVar + "###" + strUtilVar + "|");
                }
                strUtilVar = null;
            }
        } // Body System
        else if (request.getParameter("BodySys") != null) {
            ArrayList<Body_System> al = Body_System.listBodySystem(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getBody_Sys_Num();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getBody_System() + "|");
                }
                strUtilVar = null;
            }
        } // Treatment Route
        else if (request.getParameter("Route") != null) {
            ArrayList<Route> al = Route.listRoute(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getRoute_Code();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getRoute_Desc() + "|");
                }
                strUtilVar = null;
            }
        } // User
        else if (request.getParameter("User") != null) {
            ArrayList<User> al = User.listUser(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, Constants.STATUS_ACTIVE);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getUsername();
                    out.write(strUtilVar + "###" + strUtilVar + " - " /*+ al.get(i).get()*/ + "|");
                }
                strUtilVar = null;
            }
        } // Role
        else if (request.getParameter("Role") != null) {
            ArrayList<Role> al = Role.listRole(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, Constants.STATUS_ALL);

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getRole_Name();
                    out.write(strUtilVar + "###" + strUtilVar + "|");
                }
                strUtilVar = null;
            }
        } // Inventory
        else if (request.getParameter("Inventory") != null) {
            ArrayList<Inventory> al = Inventory.listInventory(Constants.SEARCH_LIBERAL, (String) session.getAttribute("Company"), strCode, strCode, "");

            if (al != null) {
                for (int i = 0; i < al.size(); i++) {
                    strUtilVar = al.get(i).getInv_Id();
                    out.write(strUtilVar + "###" + strUtilVar + " - " + al.get(i).getInv_Desc() + " (" + al.get(i).getInv_Type() + ")|");
                }
                strUtilVar = null;
            }
        }


%>