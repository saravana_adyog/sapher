<!--
Form Id : AUD2
Date    : 06-12-2007.
Author  : Jaikishan. S
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.Inventory"
import="bl.sapher.User"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Inventory List - [Sapher]</title>

        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
    </head>

    <body>

        <%!    private final String FORM_ID = "AUD2";
    private String strInvId = "";
    private String strInvName = "";
    private String strInvType = "";
    private boolean bView;
    private boolean bEdit;
    private ArrayList al = null;
    private User currentUser = null;
    private Inventory anInventory = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
        %>

        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgInventorylist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! LHA reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Submission Company reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("6")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Successfully Updated.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventorylist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventorylist.jsp: E/V=" + bEdit + "/" + bView);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventorylist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventorylist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventorylist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventorylist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }

        if (request.getParameter("txtInvId") == null) {
            /*if (session.getAttribute("txtInvId") == null) {
            strInvId = "";
            } else {
            strInvId = (String) session.getAttribute("txtInvId");
            }*/
            strInvId = "";
        } else {
            strInvId = request.getParameter("txtInvId");
        }
        //session.setAttribute("txtInvId", strInvId);

        if (request.getParameter("txtInvDesc") == null) {
            if (session.getAttribute("txtInvDesc") == null) {
                strInvName = "";
            } else {
                strInvName = (String) session.getAttribute("txtInvDesc");
            }
        } else {
            strInvName = request.getParameter("txtInvDesc");
        }
        session.setAttribute("txtInvDesc", strInvName);

        if (request.getParameter("cmbInvType") == null) {
            if (session.getAttribute("cmbInvType") == null) {
                strInvType = "";
            } else {
                strInvType = (String) session.getAttribute("cmbInvType");
            }
        } else {
            strInvType = request.getParameter("cmbInvType");
        }
        strInvType = (strInvType.equals("") ? "All" : strInvType);
        session.setAttribute("cmbInvType", strInvType);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strInvId + "," + strInvName + "," + (strInvType.equals("All") ? "" : (strInvType.equals("Screen") ? "SCR" : "REP")));
        al = Inventory.listInventory(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), strInvId, strInvName, (strInvType.equals("All") ? "" : (strInvType.equals("Screen") ? "SCR" : "REP")));

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

        %>
        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Audit Trial - Inventory Listing</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form name="FrmInventoryList" METHOD="POST" ACTION="pgInventorylist.jsp" >
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Search Criteria</td>
                            </tr>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtInvId">Inventory Id</label></td>
                                <td class="field">
                                    <input type="text" name="txtInvId" id="txtInvId" size="10" maxlength="10" title="Inventory ID"
                                           value="<%=strInvId%>">
                                </td>
                                <td width="500" align="right"><b><%=strMessageInfo%></b></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtInvDesc">Inventory Desc</label></td>
                                <td class="field">
                                    <input type="text" name="txtInvDesc" id="txtInvDesc" size="30" maxlength="50" title="Inventory"
                                           value="<%=strInvName%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="cmbInvType">Inventory Type</label></td>
                                <td class="field" colspan="2">
                                    <span style="float: left">
                                        <SELECT id="cmbInvType" NAME="cmbInvType" class="comboclass" style="width: 70px;" title="Inventory Type">
                                            <option <%=(strInvType.equals("All") ? "SELECTED" : "")%>>All</option>
                                            <option <%=(strInvType.equals("Report") ? "SELECTED" : "")%>>Report</option>
                                            <option <%=(strInvType.equals("Screen") ? "SELECTED" : "")%>>Screen</option>
                                        </SELECT>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <td colspan="2">
                                    <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                                    <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                                           onclick="location.replace('pgNavigate.jsp?Target=pgInventorylist.jsp');"/>
                                </td>
                            </td>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 1px;"></td>
            </tr>
            <!-- Grid View Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td class="form_group_title" colspan="10">Search Results
                                <%=" - " + al.size() + " record(s) found"%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% if (al.size() != 0) {%>
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                        <tr class="grid_header">
                            <td width="16"></td>
                            <td width="16"></td>
                            <td width="100">Inventory Id</td>
                            <td>Inventory Desc</td>
                            <td width="100">Inv Type</td>
                            <td width="16">Ins Flag</td>
                            <td width="16">Del Flag</td>
                            <td></td>
                        </tr>
                        <%
     anInventory = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         anInventory = ((Inventory) al.get(i));
                        %>
                        <tr class="
                            <%
                            if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            %>
                            ">
                            <%-- ------------------------------------------ --%>
                            <% if (bEdit) {%>
                            <td width="16" align="center" >
                                <a href="pgInventory.jsp?SaveFlag=U&txtInvId=<%=anInventory.getInv_Id()%>">
                                    <img src="images/icons/16x16_report.png" title="Edit the inventory with ID = <%=anInventory.getInv_Id()%>" border='0'>
                                </a>
                            </td>
                            <% } else {%>
                            <td width="16" align="center" >
                                <a href="pgInventory.jsp?SaveFlag=U&txtInvId=<%=anInventory.getInv_Id()%>">
                                    <img src="images/icons/16x16_report.png" title="View details of the inventory with ID = <%=anInventory.getInv_Id()%>" border='0'>
                                </a>
                            </td>
                            <% }%>
                            <%-- ------------------------------------------ --%>
                            <% if (bEdit) {%>
                            <td width="16" align="center" >
                                <a href="pgInventoryColList.jsp?txtInvId=<%=anInventory.getInv_Id()%>">
                                    <img src="images/icons/edit.gif" title="Edit the column specific settings for the inventory with ID = <%=anInventory.getInv_Id()%>" border='0'>
                                </a>
                            </td>
                            <% } else {%>
                            <td width="16" align="center" >
                                <a href="pgInventoryColList.jsp?txtInvId=<%=anInventory.getInv_Id()%>">
                                    <img src="images/icons/view.gif" title="View the column specific settings for the inventory with ID = <%=anInventory.getInv_Id()%>" border='0'>
                                </a>
                            </td>
                            <% }%>
                            <td width="100" align='center'>
                                <%
                            out.write(anInventory.getInv_Id());
                                %>
                            </td>
                            <td  align='left'>
                                <%
                            out.write(anInventory.getInv_Desc());
                                %>
                            </td>
                            <td width="100" align='center'>
                                <%
                            if (anInventory.getInv_Type().equals("SCR")) {
                                out.write("Screen");
                            } else {
                                out.write("Report");
                            }
                                %>
                            </td>

                            <td width="25" align='center'>
                                <% if (anInventory.getInsAuditFlag() == 1) {%>
                                <img src="images/icons/tick.gif" border='0'>
                                <%}%>
                            </td>
                            <td width="25" align='center'>
                                <% if (anInventory.getDelAuditFlag() == 1) {%>
                                <img src="images/icons/tick.gif" border='0'>
                                <%}%>
                            </td>

                            <td></td>
                        </tr>
                        <% }%>
                    </table>
                </td>
                <tr>
                </tr>
            </tr>
            <!-- Grid View Ends -->

            <!-- Pagination Starts -->
            <tr>
                <td class="formbody">
                    <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
                        <tr>
                            <td>
                                <a href="pgInventorylist.jsp?CurPage=1">
                                <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                            </td>
                            <td><% if (nCurPage > 1) {%>
                                <A href="pgInventorylist.jsp?CurPage=<%=(nCurPage - 1)%>">
                                <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></A>
                                <% } else {%>
                                <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                                <% }%>
                            </td>
                            <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                            <td><% if (nCurPage < nPageCount) {%>
                                <A href="pgInventorylist.jsp?CurPage=<%=nCurPage + 1%>">
                                <img src="images/icons/page-next.gif" title="Go to next page" border="0"></A>
                                <% } else {%>
                                <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                                <% }%>
                            </td>
                            <td>
                                <a href="pgInventorylist.jsp?CurPage=<%=nPageCount%>">
                                <img src="images/icons/page-last.gif" title="Go to last page" border="0"></A>
                            </td>
                            <td width="100%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Pagination Ends -->
            <%}%>
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>