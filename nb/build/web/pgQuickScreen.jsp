<!--
Form Id : CSD1
Date    : 27-12-2007.
Author  : Anoop Varma, Arun P. Jose
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.Country"

import="java.text.SimpleDateFormat"
import="java.util.ArrayList"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Quick Screen - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <script src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>
        <script src="libjs/combobox/combobox.js"></script>

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>

        <!--Calendar Library Includes.. S => -->
        <link rel="stylesheet" type="text/css" media="all" href="libjs/calendar/skins/aqua/theme.css" title="Aqua" />
        <link rel="alternate stylesheet" type="text/css" media="all" href="libjs/calendar/skins/calendar-green.css" title="green" />
        <!-- import the calendar script -->
        <script type="text/javascript" src="libjs/calendar/calendar.js"></script>
        <!-- import the language module -->
        <script type="text/javascript" src="libjs/calendar/lang/calendar-en.js"></script>
        <!-- helper script that uses the calendar -->
        <script type="text/javascript" src="libjs/calendar/calendar-helper.js"></script>
        <script type="text/javascript" src="libjs/calendar/calendar-setup.js"></script>
        <!--Calendar Library Includes.. E <= -->

        <script type="text/javascript" src="libjs/Ajax/ajax.js"></script>

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

        <script type="text/javascript" src="libjs/pgQuickScreen.js"></script>
    </head>
    <body>
        <%!    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private String strParent = "";
    private String strCurDate = "";
    private boolean bAdd;
    private User currentUser = null;
    private Inventory inv = null;
    private SimpleDateFormat dFormat = null;
        %>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgQuickScreen.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; Invalid username/password.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp: bAdd=" + bAdd);
            if (!bAdd) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; No Add permission.");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
            strCurDate = dFormat.format(new java.util.Date());
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='750' align="center" id="formwindow"><!-- width='710' -->
            <tr>
                <td id="formtitle">Quick Screen </td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmQuickScreen" name="FrmQuickScreen" method="POST" action="pgSavCaseQS.jsp"
                      onsubmit="if(confirmSave()) { return validate(); } return false;">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Reporter</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtEntryDate">Entry date</label></td>
                                <td class="field" colspan="3">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtEntryDate" id="txtEntryDate" size="12" maxlength="12" clear=1 title="Entry date"
                                                       value="<%=strCurDate%>" readonly  onkeydown="return ClearInput(event,1);">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                            <a href="#" id="cal_trigger1"><img src="images/icons/cal.gif" alt="Select" title="Select" border='0' name="imgCal"></a></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="fLabelR" colspan="2"><label for="txtRecvDate">Received Date</label></td>
                                <td class="field" colspan="4">
                                    <table border='0' cellspacing='0' cellpadding='0'>
                                        <tr>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                                <input type="text" name="txtRecvDate" id="txtRecvDate" size="12" maxlength="12" clear=2 title="Received Date"
                                                       readonly  onkeydown="return ClearInput(event,2);">
                                            </td>
                                            <td style="padding-right: 3px; padding-left: 0px;">
                                            <a href="#" id="cal_trigger2"><img src="images/icons/cal.gif" alt="Select" title="Select" border='0' name="imgCal"></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <script type="text/javascript">
                                Calendar.setup({
                                    inputField : "txtEntryDate", //*
                                    ifFormat : "%d-%b-%Y",
                                    showsTime : true,
                                    button : "cal_trigger1", //*
                                    step : 1
                                });
                                Calendar.setup({
                                    inputField : "txtRecvDate", //*
                                    ifFormat : "%d-%b-%Y",
                                    showsTime : true,
                                    button : "cal_trigger2", //*
                                    step : 1
                                });
                            </script>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtClinRep">Clinical Reporter</label></td>
                                <td class="field" colspan="9">
                                    <input type="text" name="txtClinRep" id="txtClinRep" size="98" maxlength="100" title="Clinical Reporter Information" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtCountryCode">Country</label></td>
                                <td colspan="9">
                                    <input type="text" id="txtCountryCode" name="txtCountryCode" size="45" onkeyup="ajax_showOptions('txtCountryCode_ID',this,'Country',event)">
                                    <input type="text" readonly size="3" id="txtCountryCode_ID" name="txtCountryCode_ID" value="">
                                </td>
                            </tr>
                            <tr>
                                <td class="form_group_title" colspan="10">Product</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtProdCode">Details</label></td>
                                <td class="field" colspan="9">
                                    <input type="text" id="txtProdCode" name="txtProdCode" size="70" onkeyup="ajax_showOptions('txtProdCode_ID',this,'Product',event)">
                                    <input type="text" readonly size="6" id="txtProdCode_ID" name="txtProdCode_ID" value="">
                                </td>
                            </tr>
                            <tr>
                                <td class="form_group_title" colspan="10">Patient</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtInitials">Initials</label></td>
                                <td >
                                    <input type="text" name="txtInitials" id="txtInitials" size="5" maxlength="4" title="Patient Initials">
                                </td>
                                <td class="fLabel"><label for="txtAgeRep">Age Reported</label></td>
                                <td class="field" colspan="2">
                                    <input type="text" name="txtAgeRep" id="txtAgeRep" size="3" maxlength="3" title="Age Reported"
                                           onkeydown="return checkIsNumeric(event);">
                                </td>
                                <td class="fLabelR"><label for="txtSexCode">Sex</label></td>
                                <td class="field" colspan="6">
                                    <input type="text" id="txtSexCode" name="txtSexCode" value="" onkeyup="ajax_showOptions('txtSexCode_ID',this,'Sex',event)">
                                    <input size="3" type="text" readonly id="txtSexCode_ID" name="txtSexCode_ID" value="" >
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel"><label for="txtTrial">Source/Trail</label></td>
                                <td class="field" colspan="4">
                                    <input type="text" id="txtTrial" name="txtTrial" size="30" onkeyup="ajax_showOptions('txtTrial_ID',this,'TrialNo',event)">
                                    <input type="text" readonly size="3" id="txtTrial_ID" name="txtTrial_ID" value="">
                                </td>
                                <td class="fLabelR"><label for="txtPatNum">Patient Number/Centre</label></td>
                                <td class="field" colspan="4">
                                    <input type="text" name="txtPatNum" id="txtPatNum" size="30" maxlength="20" title="Patient Number/Centre">
                                </td>
                            </tr>
                            <tr>
                                <td class="form_group_title" colspan="10">Adverse Event</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtLLTCode">Low Level Term</label></td>
                                <td class="field" colspan="6">
                                    <div id="divLLTDynData">
                                        <input type="text" id="txtLLTCode" name="txtLLTCode" size="45" onkeyup="ajax_showOptions('txtLLTCode_ID',this,'AELLT',event)">
                                        <input type="text" readonly size="10" id="txtLLTCode_ID" name="txtLLTCode_ID" value="">
                                    </div>
                                </td>
                                <td class="fLabelR"><label for="chkAggrLLT">Aggravation of</label></td>
                                <td class="field" colspan="">
                                    <input type="checkbox" name="chkAggrLLT" id="chkAggrLLT" class="chkbox">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtPTCode">Preferred Term</label></td>
                                <td class="field" colspan="6">
                                    <div id="divDiagDynData">
                                        <input type="text" id="txtPTCode" name="txtPTCode" size="45" onkeyup="ajax_showOptions('txtPTCode_ID',this,'AEPT',event)">
                                        <input type="text" readonly size="10" id="txtPTCode_ID" name="txtPTCode_ID" value="">
                                    </div>
                                </td>
                                <td class="fLabelR"><label for="chkAggrPT">Aggravation of</label></td>
                                <td class="field" colspan="">
                                    <input type="checkbox" name="chkAggrPT" id="chkAggrPT" title="Aggravation of" class="chkbox">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtBodySysNum">System Organ Class</label></td>
                                <td class="field" colspan="9">
                                    <div id="divBodySysDynData">
                                        <input type="text" id="txtBodySysNum" name="txtBodySysNum" size="45" onkeyup="ajax_showOptions('txtBodySysNum_ID',this,'BodySys',event)">
                                        <input type="text" readonly size="10" id="txtBodySysNum_ID" name="txtBodySysNum_ID" value="">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtClasfCode">Seriousness</label></td>
                                <td class="field" colspan="9">
                                    <div id="divClasfDynData">
                                        <input type="text" id="txtClasfCode" name="txtClasfCode" size="50" onkeyup="ajax_showOptions('txtClasfCode_ID',this,'Seriousness',event)">
                                        <input type="text" readonly size="5" id="txtClasfCode_ID" name="txtClasfCode_ID" value="">

                                        <input style="border: 0; font-color: red" type="text" name="txtClasfType" id="txtClasfType" size="12" maxlength="12"
                                               readonly value="NON SERIOUS">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtOutcomeCode">AE Outcome</label></td>
                                <td class="field" colspan="9">
                                    <div id="divOCDynData">
                                        <input type="text" id="txtOutcomeCode" name="txtOutcomeCode" size="50" onkeyup="ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)">
                                        <input type="text" readonly size="5" id="txtOutcomeCode_ID" name="txtOutcomeCode_ID" value="">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="form_group_title" colspan="10">Assessment</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtPhyAssCode">Physician Assessment</label></td>
                                <td class="field" colspan="4">
                                    <input type="text" id="txtPhyAssCode" name="txtPhyAssCode" size="22" onkeyup="ajax_showOptions('txtPhyAssCode_ID',this,'AssCausality',event)">
                                    <input type="text" readonly size="3" id="txtPhyAssCode_ID" name="txtPhyAssCode_ID" value="">
                                </td>
                                <td class="fLabelR"><label for="txtCoAssCode">Company Assessment</label></td>
                                <td class="field" colspan="4">
                                    <input type="text" id="txtCoAssCode" name="txtCoAssCode" size="22" onkeyup="ajax_showOptions('txtCoAssCode_ID',this,'AssCausality',event)">
                                    <input type="text" readonly size="3" id="txtCoAssCode_ID" name="txtCoAssCode_ID" value="">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtNarrative">Complementary Information</label></td>
                                <td colspan="9">
                                    <textarea name="txtNarrative" id="txtNarrative" cols="110" wrap="physical" title="Complementary Information"
                                              onkeydown="return checkCharCount(document.FrmQuickScreen.txtNarrative,event, 1000);"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field" colspan="6">
                                    <input type="button" name="cmdList" class="button" value="List similar Cases" style="width: 120px;" title="List"
                                           onclick="return populateList();">
                                    <input type="button" name="cmdClearList" class="button" value="Clear List" style="width: 120px;" title="Clear List"
                                           onclick="return clearList();">
                                </td>
                                <td class="field" colspan="3" align="right">
                                    <input type="submit" name="cmdSave" class="button" value="Save" style="width: 60px;" title="Save">
                                    <input type="hidden" id="Parent" name="Parent" value="0">
                                    <input type="reset" name="cmdCancel" class="button"
                                           value="Cancel" title="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('content.jsp');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>


                <script language="JavaScript" type="text/JavaScript">
                    function shiftTo(x,y) {
                        var id = "cb1_scroll_div";
                        var el = (document.getElementById)? document.getElementById(id): (document.all)? document.all[id]: (document.layers)? getLyrRef(id,document): null;

                        this.doc = (document.layers)? this.el.document: this.el;

                        var css = (el.style)? el.style: el;
                        var px = (document.layers||window.opera)? "": "px";

                        if (css.moveTo) {
                            alert("1");
                            css.moveTo(Math.round(x),Math.round(y));
                        } else { alert("2");
                            css.left=Math.round(x)+"px";
                            css.top=Math.round(y)+"px";
                        }
                    }

                    function siv(nm,top){
                        if(document.getElementById){
                            var element=document.getElementById(nm);
                            if(element.scrollIntoView){
                                element.scrollIntoView(top);
                            }
                            else { notSupported(); }
                        }
                        else { notSupported(); }
                    }
                </script>
            </tr>

            <tr>
                <td>
                    <div id="divDupCases" style="display:none" >
                    </div>
                </td>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>