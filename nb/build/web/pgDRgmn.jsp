<!--
Form Id : CDL9
Date    : 11-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.sql.*"
import="bl.sapher.DoseRegimen"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.Interval"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Dose Regimen - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>
        <script>
            function validate() {
                if (document.FrmDRgmn.txtDRgmnCode.value == ""){
                    alert( "Regimen Code cannot be empty.");
                    document.FrmDRgmn.txtDRgmnCode.focus();
                    return false;
                }
                if (trim(document.FrmDRgmn.txtDRgmnDesc.value) == ""){
                    alert( "Dose Regimen cannot be empty.");
                    document.FrmDRgmn.txtDRgmnDesc.focus();
                    return false;
                }
                if (document.FrmDRgmn.txtNoOfDosage.value == ""){
                    alert( "Number of dosage cannot be empty.");
                    document.FrmDRgmn.txtNoOfDosage.focus();
                    return false;
                }
                if(!checkIsAlphaVar(document.FrmDRgmn.txtDRgmnCode.value)) {
                    window.alert('Invalid character(s) in dose regimen code.');
                    document.getElementById('txtDRgmnCode').focus();
                    return false;
                }
                if(!checkIsAlphaSpaceVar(document.FrmDRgmn.txtDRgmnDesc.value)) {
                    window.alert('Invalid character(s) dose regimen.');
                    document.getElementById('txtDRgmnDesc').focus();
                    return false;
                }
                if(!checkIsNumericOnlyVar(document.FrmDRgmn.txtNoOfDosage.value)) {
                    window.alert('Mumber of dose should be numeric.');
                    document.getElementById('txtDRgmnCode').focus();
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>
        <%!    DoseRegimen dRgmn = null;
    private final String FORM_ID = "CDL9";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        dRgmn = new DoseRegimen((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDRgmn.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("DRgmnCode") != null) {
                    dRgmn = new DoseRegimen((String) session.getAttribute("Company"), request.getParameter("DRgmnCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                dRgmn = new DoseRegimen();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmDRgmn" name="FrmDRgmn" method="POST" action="pgSavDRgmn.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>Dose Regimen</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtDRgmnCode">Rgmn Code</label></td>
                                <td class="field"><input type="text" name="txtDRgmnCode" title="Dose Regimen Code"
                                                             id="txtDRgmnCode" size="3" maxlength="2"
                                                             value="<%=dRgmn.getDregimen_Code()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtDRgmnDesc">Dose Regimen</label></td>
                                <td class="field"><input type="text" name="txtDRgmnDesc" id="txtDRgmnDesc" size="50" title="Dose Regimen"
                                                             maxlength="50" value="<%=dRgmn.getDregimen_Desc()%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <!-- New fields STARTS -->
                            <tr>
                                <td class="fLabel" width="125"><label for="txtNoOfDosage">No. of Dosage</label></td>
                                <td class="field"><input type="text" name="txtNoOfDosage" id="txtNoOfDosage" size="4" title="No. of Dosage"
                                                             onkeydown="return checkIsNumeric(event);"
                                                             maxlength="3" value="<%

        if (dRgmn.getNoOfDosage().equals("-1") || dRgmn.getNoOfDosage().equals("0")) {
            out.write("");
        } else {
            out.write("" + dRgmn.getNoOfDosage());
        }

                                                             %>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtDosageInterval">Dosage Interval</label></td>
                                <td class="field">

                                    <select name="cmboDosageInterval" id="cmboDosageInterval">
                                        <%
        java.util.ArrayList<Interval> alIntervals = Interval.listDosageInterval((String) session.getAttribute("Company"), "", "", 1);
        Interval i = null;
        for (int count = 0; count < alIntervals.size(); count++) {
            i = (alIntervals.get(count));
                                        %>
                                        <option value="<%=i.getIntervalCode()%>"
                                                <%if (dRgmn.getDosageInterval().equals("" + i.getIntervalCode())) {
                                                out.write(" SELECTED ");
                                            }%>><%=i.getInterval()%></option>
                                        <%

        }
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                        %>
                                    </select>
                                </td>
                            </tr>
                            <!-- New fields ENDS -->
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                                             <%
        if (dRgmn.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgDRgmnlist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>