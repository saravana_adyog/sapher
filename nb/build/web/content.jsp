<!--
Form Id : NAV1.5
Author  : Ratheesh
-->
<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.general.GenConf"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Sapher</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">
            @import "css/master.css";
            body{
                text-align: center;
                background: #588A89 url(images/bg/BG-effect.jpg);
                background-repeat:  no-repeat;
                background-position: center;
                margin: 0; 	padding: 0;
            }
        </style>
    </head>

    <%!    private String strMessage;
    private String strHdr;
    %>

    <body>
        <%
        //GenConf gConf = new GenConf();
        //String strVer = gConf.getSysVersion();

        strMessage = "";
        strHdr = "";

        if (request.getParameter("Status") != null) {
            if (request.getParameter("Status").equals("0")) {
                strHdr = "Error:";
                strMessage = "Sorry, Specified Record not found!";
            } else if (request.getParameter("Status").equals("1")) {
                strHdr = "Error:";
                strMessage = "Sorry, Permission Denied!";
            } else if (request.getParameter("Status").equals("2")) {
                strHdr = "Status:";
                strMessage = "Update Success!";
                if (request.getParameter("RepNum") != null) {
                    strMessage = "Update Success, Report Number - " + request.getParameter("RepNum");
                }
            } else if (request.getParameter("Status").equals("3")) {
                strHdr = "Status:";
                strMessage = "Update Failed!";
            }
        }

        if (!strMessage.equals("")) {
        %>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" height="50%">
            <tr>
                <td height="200" colspan="2">
                    <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
                        <tr>
                            <td id="formtitle"><%=strHdr%></td>
                        </tr>
                        <!-- Form Body Starts -->
                        <tr>
                            <td class="formbody">
                                <div class="error"><%=strMessage%></div>
                            </td>
                        </tr>
                        <!-- Form Body Ends -->
                        <tr>
                            <td style="height: 10px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%}%>
    </body>
</html>
