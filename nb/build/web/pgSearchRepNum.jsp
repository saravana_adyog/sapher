<!--
Form Id : 
Date    : 12-04-2008.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.CaseDetails"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>
    
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>Patient number Filter- [Sapher]</title>
        
        <meta http-equiv="Content-Language" content="en-us" />
        
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />
        
        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />
        
        <meta name="author" content="Focal3" />
        
        <style type="text/css" media="all">@import "css/master.css";</style>
    </head>
    
    <body>
        <script>
            function doClose(id, name) {
                var strId = id;
                var strName = name;
                
                var tmp = (opener.SAPHER_PARENT_NAME.localeCompare('pgQueryTool'));
                if(tmp == '0') {
                    tmp = (opener.click_root.localeCompare('S'));
                    if(tmp == '0') {
                        opener.document.FrmQueryTool.txtRepNumStart.style.color = "#000000";
                        opener.document.FrmQueryTool.txtRepNumStart.style.fontWeight= "normal";
                        opener.document.FrmQueryTool.txtRepNumStart.value = strId;
                    }
                    
                    tmp = (opener.click_root.localeCompare('E'));
                    if(tmp == '0') {
                        opener.document.FrmQueryTool.txtRepNumEnd.style.color = "#000000";
                        opener.document.FrmQueryTool.txtRepNumEnd.style.fontWeight= "normal";
                        opener.document.FrmQueryTool.txtRepNumEnd.value = strId;
                    }
                }
                
                window.close();
            }
        </script>
        <%!    private String FORM_ID = "";
    private String strRNum = "";
    private boolean bAdd;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private Inventory inv = null;
    private String strPNum;
        %>
        
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgSearchRepNum.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Invalid username/password");
        %>
        <script type="text/javascript">
            opener.parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
            window.close();
        </script>
        <%
                return;
            }
            if (request.getParameter("FormId") == null) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Permission denied.");
        %>
        <script type="text/javascript">
            opener.document.location.replace("content.jsp?Status=1");
            window.close();
        </script>
        <%
                return;
            } else {
                FORM_ID = request.getParameter("FormId");
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp: A/V=" + bAdd + "/" + bView);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            opener.parent.document.location.replace("pgLogin.jsp?LoginStatus=6");window.close();
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
            window.close();
        </script>
        <%
            return;
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchRepNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Permission denied. [Add: " + bAdd + ",View: " + bView + "]");
        %>
        <script type="text/javascript">
            opener.document.location.replace("content.jsp?Status=1");
            window.close();
        </script>
        <%
            return;
        }

        if (request.getParameter("txtSrchRepNum") == null) {
            strRNum = "";
        } else {
            strRNum = request.getParameter("txtSrchRepNum");
        }

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + (String) session.getAttribute("Company"));
        al = CaseDetails.listReportNum((String) session.getAttribute("Company"));

        %>
        
        <div style="height: 15px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Filter - Report Number</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <form name="FrmRepNumList" METHOD="POST" ACTION="pgSearchRepNum.jsp" >
                            <tr>
                                <td class="form_group_title" colspan="10">Filter Criteria</td>
                            </tr>
                            <tr>
                                <td style="height: 10px;"></td>
                            </tr>
                            <input type="hidden" id="FormId" name="FormId" value="<%=FORM_ID%>">
                            <input type="hidden" id="ValidSrchRNum" name="ValidSrchRNum" value="<%%>">
                            <tr>
                                <td class="fLabel" width="125"><label for="txtSrchRepNum">Name</label></td>
                                <td class="field">
                                    <input type="text" name="txtSrchRepNum" id="txtSrchRepNum" size="30" maxlength="50"
                                           value="<%=strRNum%>" title="Report Number">
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label></label></td>
                                <td class="field" colspan="2">
                                    <span style="float: left">
                                        <input type="submit" name="cmdFilter" title="Start filtering with specified criteria" class="button" value="Filter" style="width: 87px;">
                                    </span>
                                </td>
                            </tr>
                        </form>
                    </table>
                </td>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 1px;"></td>
            </tr>
            <!-- Grid View Starts -->
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td class="form_group_title" colspan="10">Filter Results
                                <%=" - " + al.size() + " record(s) found"%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% if (al.size() != 0) {%>
            <tr>
                <td class="formbody">
                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
                        <tr class="grid_header">
                            <td width="100">Patient Num</td>
                            <td></td>
                        </tr>
                        <tr class="even_row">
                            <td width="16" align="center" >
                                <a  onclick="doClose('');" href="#">NONE</a>
                            </td>
                            <td width="100" align='center'></td>
                        </tr>
                        <%
     for (int i = 0;
             i < al.size();
             i++) {
         strPNum = ((String) al.get(i));
                        %>
                        <tr class="
                            <%
                            if (i % 2 != 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            %>
                            ">
                            <td width="100" align='center'>
                                <font>
                                    <a onclick="doClose('<%=strPNum%>');"
                                       href="#"><%=strPNum%></a>
                                </font>
                            </td>
                            <td></td>
                        </tr>
                        <% }%>
                    </table>
                </td>
                <tr>
                </tr>
            </tr>
            <!-- Grid View Ends -->
            <%}%>
            
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>