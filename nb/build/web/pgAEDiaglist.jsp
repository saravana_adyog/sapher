<!--
Form Id : CDL3
Date    : 10-12-2007.
Author  : Arun P. Jose, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.AEDiag"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>AE Preferred Term - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "";</script>
    <script src="libjs/gui.js"></script>
    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>

    <script type="text/javascript">
        function validateThisPage() {
            return true;
        }
    </script>
</head>

<body>

<%!    private final String FORM_ID = "CDL3";
    private String strDiagCode = "";
    private String strDiagDesc = "";
    private int nDiagValid = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private AEDiag aeDiag = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
%>

<%
        aeDiag = new AEDiag((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgAEDiaglist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Adverse Event reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Successfully Updated.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Description already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
    bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
    bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgAEDiag.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtAEDiagCode") == null) {
            if (session.getAttribute("txtAEDiagCode") == null) {
                strDiagCode = "";
            } else {
                strDiagCode = (String) session.getAttribute("txtAEDiagCode");
            }
        } else {
            strDiagCode = request.getParameter("txtAEDiagCode");
        }
        session.setAttribute("txtAEDiagCode", strDiagCode);

        if (request.getParameter("txtAEDiagDesc") == null) {
            if (session.getAttribute("txtAEDiagDesc") == null) {
                strDiagDesc = "";
            } else {
                strDiagDesc = (String) session.getAttribute("txtAEDiagDesc");
            }
        } else {
            strDiagDesc = request.getParameter("txtAEDiagDesc");
        }
        session.setAttribute("txtAEDiagDesc", strDiagDesc);

        String strValid = "";
        if (request.getParameter("cmbValidDiag") == null) {
            if (session.getAttribute("cmbValidDiag") == null) {
                strValid = "All";
            } else {
                strValid = (String) session.getAttribute("cmbValidDiag");
            }
        } else {
            strValid = request.getParameter("cmbValidDiag");
        }
        session.setAttribute("cmbValidDiag", strValid);

        if (strValid.equals("Active")) {
            nDiagValid = 1;
        } else if (strValid.equals("Inactive")) {
            nDiagValid = 0;
        } else if (strValid.equals("All")) {
            nDiagValid = -1;
        }

        if (resetSession) {
            strDiagCode = "";
            strDiagDesc = "";
            nDiagValid = -1;
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = AEDiag.listDiag(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), "", "", -1);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strDiagCode + "," + strDiagDesc + "," + nDiagValid);
            al = AEDiag.listDiag(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), strDiagCode, strDiagDesc, nDiagValid);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">Code List - AE Preferred Term Listing</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <form name="FrmDiagList" METHOD="POST" ACTION="pgAEDiaglist.jsp" >
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Criteria</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtAEDiagCode">PT Code</label></td>
            <td class="field">
                <input type="text" name="txtAEDiagCode" id="txtAEDiagCode" size="12" maxlength="12" title="Preferred Term Code"
                       value="<%=strDiagCode%>">
            </td>
            <td width="500" align="right"><b><%=strMessageInfo%></b></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtAEDiagDesc">Preferred Term</label></td>
            <td class="field">
                <input type="text" name="txtAEDiagDesc" id="txtAEDiagDesc" size="30" maxlength="50" title="Preferred Term"
                       value="<%=strDiagDesc%>">
            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="cmbValidDiag">Status</label></td>
            <td class="field" colspan="2">
                <span style="float: left">
                    <select id="cmbValidDiag" NAME="cmbValidDiag" class="comboclass" style="width: 70px;" title="Status" >
                        <option <%=(strValid.equals("All") ? "SELECTED" : "")%>>All</option>
                        <option <%=(strValid.equals("Active") ? "SELECTED" : "")%>>Active</option>
                        <option <%=(strValid.equals("Inactive") ? "SELECTED" : "")%>>Inactive</option>
                    </select>
                </span>
            </td>
        </tr>
        <tr>
        <td>
        <td colspan="2">
            <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;"
                   onclick="return validateThisPage();">
            <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                   onclick="location.replace('pgNavigate.jsp?Target=pgAEDiaglist.jsp');"/>
        </td>
    </form>
    <td width="16" align="right" >
        <a href="#" title="Print"
           <%
        if (al.size() > 0) {
            out.write("onclick=\"popupWnd('_blank', 'yes','pgPrintAEPT.jsp?cod=" + strDiagCode +
                    "&desc=" + strDiagDesc + "&valid=" + nDiagValid + "',800,600)\"");
        }
           %>>
            <img src="images/icons/print.gif" border='0'>
        </a>
    </td>
    <td>
        <form METHOD="POST" ACTION="pgAEDiag.jsp">
            <span style="float: right">
                <input type="hidden" name="SaveFlag" value="I">
                <input type="hidden" name="Parent" value="1">
                <input type="submit" name="cmdNew"  title="Adds a new entry"  class="<%=(bAdd ? "button" : "button disabled")%>"
                       value="New Pref. Term" style="width: 110px;" <%=(bAdd ? "" : " DISABLED")%>>
            </span>
        </form>
    </td>
    </td>
</tr>
</table>
</td>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <% if (bDelete) {%>
                <td width="16"></td>
                <% }%>
                <td width="100">PT Code</td>
                <td>Preferred Term</td>
                <td width="100">Status</td>
                <td></td>
            </tr>
            <%
     aeDiag = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aeDiag = ((AEDiag) al.get(i));
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <% if (bEdit) {%>
                <td width="16" align="center" >
                    <a href="pgAEDiag.jsp?DiagCode=<% out.write(aeDiag.getDiag_Code());%>&SaveFlag=U&Parent=1">
                        <img src="images/icons/edit.gif" border='0' title="Edit the entry with ID = <%=aeDiag.getDiag_Code()%>">
                    </a>
                </td>
                <% } else {%>
                <td width="16" align="center" >
                    <a href="pgAEDiag.jsp?DiagCode=<% out.write(aeDiag.getDiag_Code());%>&SaveFlag=V&Parent=1">
                        <img src="images/icons/view.gif" border='0' title="View details of the entry with ID = <%=aeDiag.getDiag_Code()%>">
                    </a>
                </td>
                <% }%>
                <% if (bDelete) {%>
                <td width="16" align="center" >
                    <a href="pgDelAEDiag.jsp?DiagCode=<% out.write(aeDiag.getDiag_Code());%>"
                       onclick="return confirm('Are you sure you want to delete?','Sapher')">
                        <img src="images/icons/delete.gif" border='0' title="Delete the entry with ID = <%=aeDiag.getDiag_Code()%>">
                    </a>
                </td>
                <% }%>
                <td width="100" align='center'>
                    <%
                if (aeDiag.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(aeDiag.getDiag_Code());%></font>
                    <%
                } else {
                    out.write(aeDiag.getDiag_Code());
                }
                    %>
                </td>
                <td align='left'>
                    <%
                if (aeDiag.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(aeDiag.getDiag_Desc());%></font>
                    <%
                } else {
                    out.write(aeDiag.getDiag_Desc());
                }
                    %>
                </td>
                <td width="100" align='center'>
                    <%
                if (aeDiag.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write("Inactive");%></font>
                    <%
                } else {
                    out.write("Active");
                }
                    %>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgAEDiaglist.jsp?CurPage=1">
                    <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <A href="pgAEDiaglist.jsp?CurPage=<%=(nCurPage - 1)%>">
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></A>
                    <% } else {%>
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <a href="pgAEDiaglist.jsp?CurPage=<%=nCurPage + 1%>">
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0"></a>
                    <% } else {%>
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                    <% }%>
                </td>
                <td>
                    <a href="pgAEDiaglist.jsp?CurPage=<%=nPageCount%>">
                    <img src="images/icons/page-last.gif" title="Go to last page" border="0"></a>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>