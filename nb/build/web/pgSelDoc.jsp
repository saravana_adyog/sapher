<!--
Form Id : CSD1
Date    : Jun 25, 2009, 3:49:47 PM
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.Logger"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title> - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script type="text/javascript" src="libjs/gui.js"></script>

        <script>
            function validate() {
                if(document.getElementById('fuFileUpload').value == '') {
                    window.alert('Please select a file to upload.');
                    document.getElementById('fuFileUpload').focus();
                    return false;
                }
                
                return true;
            }

        </script>

    </head>
    <body>
        <%!    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private String strParent = "";
    private String strRepNum = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgSelDoc.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSelDoc.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle"></td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form action="pgSavDoc.jsp?RepNum=<%=strRepNum%>" enctype="multipart/form-data" method="post">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10">Upload document</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">Select File :</td>
                                <td class="field">
                                    <input type="hidden" id="hvRepNum" name="hvRepNum" value="<%=strRepNum%>" />
                                    <input type="file" name="fuFileUpload" id="fuFileUpload"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="field"></td>
                                <td class="fLabel" width="200">Max File Size allowed is 10 MB.</td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="200">File Description:</td>
                                <td class="field">
                                    <input type="text" id="txtDocDesc" name="txtDocDesc"
                                    maxlength="100" size="40"/>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="button" class="button" value="Upload" onclick="return validate();"/>
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('pgNavigate.jsp?Cancellation=1&Target=pgDocslist.jsp?RepNum=<%=strRepNum%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>