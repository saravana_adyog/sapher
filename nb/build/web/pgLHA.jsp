<!--
Form Id : CDL15
Date    : 12-12-2007.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="bl.sapher.LHA"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException" import="bl.sapher.general.Logger"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title>LHA - [Sapher]</title>
        <meta http-equiv="Content-Language" content="en-us" />
        <script>var SAPHER_PARENT_NAME = "pgLHA";</script>
        <script src="libjs/gui.js"></script>
        <script src="libjs/ajax/ajax.js"></script>

        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="MSSmartTagsPreventParsing" content="true" />

        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />

        <meta name="author" content="Focal3" />

        <style type="text/css" media="all">@import "css/master.css";</style>
        <script>
            var nCntryPopupCount = 0;

            function validate(){
                if (document.FrmLHA.txtLHACode.value == ""){
                    alert( "LHA Code cannot be empty.");
                    document.FrmLHA.txtLHACode.focus();
                    return false;
                }
                if (document.FrmLHA.txtCountryCode_ID.value == ""){
                    alert( "Country Code cannot be empty.");
                    document.FrmLHA.txtCountryCode.focus();
                    return false;
                }
                if (trim(document.FrmLHA.txtLHADesc.value) == ""){
                    alert( "LHA cannot be empty.");
                    document.FrmLHA.txtLHADesc.focus();
                    return false;
                }
                if(!checkIsAlphaVar(document.FrmLHA.txtLHACode.value)) {
                    window.alert('Invalid character(s) in LHA code.');
                    document.getElementById('txtLHACode').focus();
                    return false;
                }
                if(!isAlphaNumericSpace(document.FrmLHA.txtLHADesc.value)) {
                    window.alert('Invalid character(s) in LHA description.');
                    document.getElementById('txtLHADesc').focus();
                    return false;
                }
                return true;
            }

        </script>

        <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
        <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

    </head>
    <body onUnload="nCntryPopupCount = 0;">
        <%!    LHA aLHA = null;
    private final String FORM_ID = "CDL15";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        %>
        <%
        aLHA = new LHA((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgLHA.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp; Wrong username/password");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
        </script>
        <%
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp; User already logged in.");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
        </script>
        <%
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("LHACode") != null) {
                    aLHA = new LHA((String) session.getAttribute("Company"), request.getParameter("LHACode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                aLHA = new LHA();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHA.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        %>

        <div style="height: 25px;"></div>
        <table border='0' cellspacing='0' cellpadding='0' width='400' align="center" id="formwindow">
            <tr>
                <td id="formtitle">Code List</td>
            </tr>
            <!-- Form Body Starts -->
            <tr>
                <form id="FrmLHA" name="FrmLHA" method="POST" action="pgSavLHA.jsp"
                      onsubmit="return validate();">
                    <td class="formbody">
                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
                            <tr>
                                <td class="form_group_title" colspan="10"><%=(saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit "))%>LHA</td>
                            </tr>
                            <tr>
                                <td style="height: 5px;"></td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtLHACode">LHA Code</label></td>
                                <td class="field"><input type="text" name="txtLHACode" title="Health Authority Code"
                                                             id="txtLHACode" size="12" maxlength="12"
                                                             value="<%=aLHA.getLha_Code()%>"
                                                             <%
        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtCountryCode">Country</label></td>
                                <td class="field">
                                    <div id="divCntryDynData" >

                                        <input type="text" id="txtCountryCode" name="txtCountryCode" size="30" onKeyUp="ajax_showOptions('txtCountryCode_ID',this,'Country',event)"
                                               value="<%
        if (aLHA.getCtry() != null) {
            out.write(aLHA.getCtry().getCountry_Nm());
        } else {
            out.write("");
        }
                                               %>"
                                               <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               %> />
                                        <input type="text" readonly size="3" id="txtCountryCode_ID" name="txtCountryCode_ID"
                                               value="<%
        if (aLHA.getCtry() != null) {
            out.write(aLHA.getCtry().getCountry_Code());
        } else {
            out.write("");
        }
                                               %>" />

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="txtLHADesc">Description</label></td>
                                <td class="field"><input type="text" name="txtLHADesc" id="txtLHADesc" size="50" title="Health Authority"
                                                             maxlength="50" value="<%=aLHA.getLha_Desc()%>"
                                                             <%
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"><label for="chkValid">Active</label></td>
                                <td class="field"><input type="checkbox" name="chkValid" id="chkValid" title="Status" class="chkbox"
                                                             <%
        if (aLHA.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             %>>
                                </td>
                            </tr>
                            <tr>
                                <td class="fLabel" width="125"></td>
                                <td class="field">
                                    <input type="submit" name="cmdSave" class="<%=(saveFlag.equals("V") ? "button disabled" : "button")%>" value="Save" style="width: 60px;"
                                           <%
        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           %>>
                                    <input type="hidden" id="SaveFlag" name="SaveFlag" value="<%=saveFlag%>">
                                    <input type="hidden" id="Parent" name="Parent" value="<%=strParent%>">
                                    <input type="reset" name="cmdCancel" class="button" value="Cancel" style="width: 60px;"
                                           onclick="javascript:location.replace('<%=!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgLHAlist.jsp" : "content.jsp"%>');">
                                </td>
                            </tr>
                        </table>
                    </td>
                </form>
            </tr>
            <!-- Form Body Ends -->
            <tr>
                <td style="height: 10px;"></td>
            </tr>
        </table>
        <div style="height: 25px;"></div>
    </body>
</html>