<!--
Form Id : CDL15
Date    : 13-12-2007.
Author  : Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.LHA"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>LHA List - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "pgLHAlist";</script>
    <script src="libjs/gui.js"></script>
    <script src="libjs/ajax/ajax.js"></script>

    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>

    <script type="text/javascript" src="libjs/Ajax2/ajax.js"></script>
    <script type="text/javascript" src="libjs/Ajax2/ajax-dynamic-list.js"></script>

</head>

<body>
<%!    private final String FORM_ID = "CDL15";
    private String strLHAId = "";
    private String strLHAName = "";
    private String strLHACountryId = "";
    private String strLHACountryNm = "";
    private int nLHAValid = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private LHA aLHA = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
%>

<%
        aLHA = new LHA((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgLHAlist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Adverse Event reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Updation Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! LHA Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Invalid Country.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("6")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }

        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHAlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHAlist.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHAlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
            toe.printStackTrace();
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHAlist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHAlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgLHA.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLHAlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtLHACode") == null) {
            if (session.getAttribute("txtLHACode") == null) {
                strLHAId = "";
            } else {
                strLHAId = (String) session.getAttribute("txtLHACode");
            }
        } else {
            strLHAId = request.getParameter("txtLHACode");
        }
        session.setAttribute("txtLHACode", strLHAId);

        if (request.getParameter("txtCountryCode_ID") == null) {
            if (session.getAttribute("txtCountryCode_ID") == null) {
                strLHACountryId = "";
            } else {
                strLHACountryId = (String) session.getAttribute("txtCountryCode_ID");
            }
        } else {
            strLHACountryId = request.getParameter("txtCountryCode_ID");
        }
        session.setAttribute("txtCountryCode_ID", strLHACountryId);

        if (request.getParameter("txtCountryCode") == null) {
            if (session.getAttribute("txtCountryCode") == null) {
                strLHACountryNm = "";
            } else {
                strLHACountryNm = (String) session.getAttribute("txtCountryCode");
            }
        } else {
            strLHACountryNm = request.getParameter("txtCountryCode");
        }
        session.setAttribute("txtCountryCode", strLHACountryNm);

        if (request.getParameter("txtLHANm") == null) {
            if (session.getAttribute("txtLHANm") == null) {
                strLHAName = "";
            } else {
                strLHAName = (String) session.getAttribute("txtLHANm");
            }
        } else {
            strLHAName = request.getParameter("txtLHANm");
        }
        session.setAttribute("txtLHANm", strLHAName);

        String strValid = "";
        if (request.getParameter("cmbValidLHA") == null) {
            if (session.getAttribute("cmbValidLHA") == null) {
                strValid = "All";
            } else {
                strValid = (String) session.getAttribute("cmbValidLHA");
            }
        } else {
            strValid = request.getParameter("cmbValidLHA");
        }
        session.setAttribute("cmbValidLHA", strValid);

        if (strValid.equals("Active")) {
            nLHAValid = 1;
        } else if (strValid.equals("Inactive")) {
            nLHAValid = 0;
        } else if (strValid.equals("All")) {
            nLHAValid = -1;
        }

        if (resetSession) {
            strLHAId = "";
            strLHACountryId = "";
            strLHACountryNm = "";
            strLHAName = "";
            nLHAValid = -1;
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = LHA.listLHA(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), "", "", "", -1);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strLHAId + "," + strLHACountryId + "," + strLHAName + "," + nLHAValid);
            al = LHA.listLHA(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), strLHAId, strLHACountryId, strLHAName, nLHAValid);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
    <tr>
        <td id="formtitle">Code List - LHA Listing</td>
    </tr>
    <!-- Form Body Starts -->
    <tr>
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Criteria</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <FORM name="FrmLHAlist" METHOD="POST" ACTION="pgLHAlist.jsp" >
            <tr>
                <td class="fLabel" width="125"><label for="txtLHACode">LHA Code</label></td>
                <td class="field">
                    <input type="text" name="txtLHACode" id="txtLHACode" size="12" maxlength="12" title="Local Health Authority Code"
                           value="<%=strLHAId%>">
                </td>
                <td width="500" align="right"><b><%=strMessageInfo%></b></td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label for="txtCountryCode">Country</label></td>
                <td class="field"  width="300">
                    <div id=divDynData>
                        <input type="text" id="txtCountryCode" name="txtCountryCode" size="24"
                        <%=strLHACountryNm%> onKeyUp="ajax_showOptions('txtCountryCode_ID',this,'Country',event)" />
                        <input type="text" readonly size="3" id="txtCountryCode_ID" name="txtCountryCode_ID"
                        value="<%=strLHACountryId%>" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label for="txtLHANm">LHA Desc.</label></td>
                <td class="field">
                    <input type="text" name="txtLHANm" id="txtLHANm" size="30" maxlength="50" title="Local Health Authority"
                           value="<%=strLHAName%>"/>
                </td>
            </tr>
            <tr>
                <td class="fLabel" width="125"><label for="cmbValidLHA">Status</label></td>
                <td class="field">
                    <span style="float: left">
                        <SELECT id="cmbValidLHA" NAME="cmbValidLHA" class="comboclass" style="width: 70px;" title="Status">
                            <option <%=(strValid.equals("All") ? "SELECTED" : "")%>>All</option>
                            <option <%=(strValid.equals("Active") ? "SELECTED" : "")%>>Active</option>
                            <option <%=(strValid.equals("Inactive") ? "SELECTED" : "")%>>Inactive</option>
                        </SELECT>
                    </span>
                </td>
            </tr>
            <tr>
            <td></td>
            <td>
                <span style="float: left">
                    <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                    <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                           onclick="location.replace('pgNavigate.jsp?Target=pgLHAlist.jsp');"/>
                </span>
            </td>
        </form>
        <td  width="16" align="right"><!--colspan="4"-->
            <a href="#" title="Print"
               <%
        if (al.size() > 0) {
            session.setAttribute("LhaPrintData", al);
            out.write("onclick=\"popupWnd('_blank', 'yes','pgPrintLHA.jsp?cod=" + strLHAId +
                    "&desc=" + strLHAName + "&ctry=" + strLHACountryNm + "&valid=" + nLHAValid + "',800,600)\"");
        }
               %>>
                <img src="images/icons/print.gif"  border='0'>
            </a>
        </td>
        <td>
            <form METHOD="POST" ACTION="pgLHA.jsp">
                <span style="float: right">
                    <input type="hidden" name="SaveFlag" value="I">
                    <input type="hidden" name="Parent" value="1">
                    <input type="submit" name="cmdNew"  title="Adds a new entry" title="Adds a new entry" class="<%=(bAdd ? "button" : "button disabled")%>"
                           value="New LHA" style="width: 110px;" <%=(bAdd ? "" : " DISABLED")%>>
                </span>
            </form>
        </td>
    </tr>
    </tr>
</table>
</td>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <% if (bDelete) {%>
                <td width="16"></td>
                <% }%>
                <td width="100">LHA Code</td>
                <td>LHA Description</td>
                <td width="100">Country</td>
                <td width="100">Status</td>
                <td></td>
            </tr>
            <%
     aLHA = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aLHA = ((LHA) al.get(i));
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <% if (bEdit) {%>
                <td width="16" align="center" >
                    <a href="pgLHA.jsp?LHACode=<% out.write(aLHA.getLha_Code());%>&SaveFlag=U&Parent=1">
                        <img src="images/icons/edit.gif" title="Edit the entry with ID = <%=aLHA.getLha_Code()%>" border='0'>
                    </a>
                </td>
                <% } else {%>
                <td width="16" align="center" >
                    <a href="pgLHA.jsp?LHACode=<% out.write(aLHA.getLha_Code());%>&SaveFlag=V&Parent=1">
                        <img src="images/icons/view.gif" title="View details of the entry with ID = <%=aLHA.getLha_Code()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <% if (bDelete) {%>
                <td width="16" align="center" >
                    <a href="pgDelLHA.jsp?LHACode=<% out.write(aLHA.getLha_Code());%>"
                       onclick="return confirm('Are you sure you want to delete?','Sapher')">
                        <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=aLHA.getLha_Code()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <td width="100" align='center'>
                    <%
                if (aLHA.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(aLHA.getLha_Code());%></font>
                    <%
                } else {
                    out.write(aLHA.getLha_Code());
                }
                    %>
                </td>
                <td align='left'>
                    <%
                if (aLHA.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(aLHA.getLha_Desc());%></font>
                    <%
                } else {
                    out.write(aLHA.getLha_Desc());
                }
                    %>
                </td>
                <td width="100" align='left'>
                    <%
                if (aLHA.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(aLHA.getCtry().getCountry_Nm());%></font>
                    <%
                } else {
                    out.write(aLHA.getCtry().getCountry_Nm());
                }
                    %>
                </td>
                <td width="100" align='center'>
                    <%
                if (aLHA.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write("Inactive");%></font>
                    <%
                } else {
                    out.write("Active");
                }
                    %>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgLHAlist.jsp?CurPage=1">
                        <img src="images/icons/page-first.gif" title="Go to first page" border="0">
                    </a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <a href="pgLHAlist.jsp?CurPage=<%=(nCurPage - 1)%>">
                        <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                    </a>
                    <% } else {%>
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <a href="pgLHAlist.jsp?CurPage=<%=nCurPage + 1%>">
                        <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                    </a>
                    <% } else {%>
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                    <% }%>
                </td>
                <td>
                    <a href="pgLHAlist.jsp?CurPage=<%=nPageCount%>">
                        <img src="images/icons/page-last.gif" title="Go to last page" border="0">
                    </a>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>