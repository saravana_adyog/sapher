<%-- 
    Document   : pgSavCaseClone
    Created on : Nov 17, 2009, 5:13:36 PM
    Author     : Anoop
--%>
<%@page
    contentType="text/html"
    pageEncoding="UTF-8"
    import="bl.sapher.CaseDetails"
    import="java.sql.Date"
    import="bl.sapher.User"
    import="bl.sapher.Inventory"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.TimeoutException"
    import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.ConstraintViolationException"
    import="bl.sapher.general.GenericException"
    import="bl.sapher.general.DBConnectionException"
    import="bl.sapher.general.Logger"
    import="java.text.SimpleDateFormat"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
        final String FORM_ID = "CSD1";
        int nRepNum = -1;
        boolean bAdd = false;
        User currentUser = null;

        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
        </script>
        <%
            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; UserBlocked exception");
        %>
        <script type="text/javascript">
            parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
        </script>
        <%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
            pageContext.forward("pgLogin.jsp?LoginStatus=3");
        }

        if (request.getParameter("RepNum") != null) {
            if (!request.getParameter("RepNum").equals("")) {
                nRepNum = Integer.parseInt(request.getParameter("RepNum"));
            }
        }
        try {
            if (!bAdd) {
                throw new ConstraintViolationException("2");
            }

            CaseDetails aCase = new CaseDetails();

            aCase.saveCloneCaseDetails((String) session.getAttribute("Company"),currentUser.getUsername(), nRepNum);
            nRepNum = aCase.getRep_Num();

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; Insertion success.");
            pageContext.forward("pgCaseSummary.jsp?SavStatus=0&RepNum=" + nRepNum + "&SaveFlag=U");

        } catch (ConstraintViolationException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
            pageContext.forward("pgCaseSummary.jsp?SavStatus=2&RepNum=" + nRepNum + "&SaveFlag=U");
        } catch (DBConnectionException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
            pageContext.forward("pgCaseSummary.jsp?SavStatus=4&RepNum=" + nRepNum + "&SaveFlag=U");
        } catch (GenericException ex) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
            pageContext.forward("pgCaseSummary.jsp?SavStatus=5&RepNum=" + nRepNum + "&SaveFlag=U");
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavCaseClone.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
        }

        %>
    </body>
</html>
