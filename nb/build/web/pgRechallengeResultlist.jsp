<!--
Form Id : CDL17
Date    : 13-12-2007.
Author  : Jaikishan. S, Anoop Varma
-->

<%@ page
contentType="text/html; charset=iso-8859-1"
language="java"
import="java.util.ArrayList"
import="bl.sapher.RechallengeResult"
import="bl.sapher.User"
import="bl.sapher.Inventory"
import="bl.sapher.general.RecordNotFoundException"
import="bl.sapher.general.TimeoutException"
import="bl.sapher.general.UserBlockedException"
import="bl.sapher.general.Logger"
import="bl.sapher.general.Constants"
isThreadSafe="true"
    %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
    <title>Rechallenge Result - [Sapher]</title>
    <script>var SAPHER_PARENT_NAME = "";</script>
    <script src="libjs/gui.js"></script>
    <meta http-equiv="Content-Language" content="en-us" />

    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta name="description" content="Description" />
    <meta name="keywords" content="Keywords" />

    <meta name="author" content="Focal3" />

    <style type="text/css" media="all">@import "css/master.css";</style>
</head>

<body>

<%!    private final String FORM_ID = "CDL17";
    private String strResultCode = "";
    private String strResultDesc = "";
    private int nFormValid = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private RechallengeResult rechallengeresult = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
%>

<%
        rechallengeresult = new RechallengeResult((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgRechallengeResultlist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Description already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRechallengeResultlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=1");
</script>
<%
                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRechallengeResultlist.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRechallengeResultlist.jsp; UserBlocked exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=5");
</script>
<%
            return;
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRechallengeResultlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=6");
</script>
<%
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRechallengeResultlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?LoginStatus=3");
</script>
<%
            return;
        }
        if (bAdd && !bView) {
            pageContext.forward("pgRechallengeResult.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRechallengeResultlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }
        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtResultCode") == null) {
            if (session.getAttribute("txtResultCode") == null) {
                strResultCode = "";
            } else {
                strResultCode = (String) session.getAttribute("txtResultCode");
            }
        } else {
            strResultCode = request.getParameter("txtResultCode");
        }
        session.setAttribute("txtResultCode", strResultCode);

        if (request.getParameter("txtResultDesc") == null) {
            if (session.getAttribute("txtResultDesc") == null) {
                strResultDesc = "";
            } else {
                strResultDesc = (String) session.getAttribute("txtResultDesc");
            }
        } else {
            strResultDesc = request.getParameter("txtResultDesc");
        }
        session.setAttribute("txtResultDesc", strResultDesc);

        String strValid = "";
        if (request.getParameter("cmbValidResult") == null) {
            if (session.getAttribute("cmbValidResult") == null) {
                strValid = "All";
            } else {
                strValid = (String) session.getAttribute("cmbValidResult");
            }
        } else {
            strValid = request.getParameter("cmbValidResult");
        }
        session.setAttribute("cmbValidResult", strValid);

        if (strValid.equals("Active")) {
            nFormValid = 1;
        } else if (strValid.equals("Inactive")) {
            nFormValid = 0;
        } else if (strValid.equals("All")) {
            nFormValid = -1;
        }

        if (resetSession) {
            strResultCode = "";
            strResultDesc = "";
            nFormValid = -1;
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = RechallengeResult.listRechallengeResult(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), "", "", -1);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strResultCode + "," + strResultDesc + "," + nFormValid);
            al = RechallengeResult.listRechallengeResult(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"), strResultCode, strResultDesc, nFormValid);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {
%>
<script type="text/javascript">
    parent.document.location.replace("pgLogin.jsp?AppStabilityStatus=" + <%=Constants.APP_STABILITY_STATUS_ILLEGAL_OP%>);
</script>
<%            return;
            }
        }

%>

<div style="height: 25px;"></div>
<table border='0' cellspacing='0' cellpadding='0' width='90%' align="center" id="formwindow">
<tr>
    <td id="formtitle">Code List - Rechallenge Result Listing</td>
</tr>
<!-- Form Body Starts -->
<tr>
    <FORM name="FrmRechallengeResultList" METHOD="POST" ACTION="pgRechallengeResultlist.jsp" >
        <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
        <tr>
            <td class="form_group_title" colspan="10">Search Criteria</td>
        </tr>
        <tr>
            <td style="height: 10px;"></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtResultCode">Result Code</label></td>
            <td class="field">
                <input type="text" name="txtResultCode" id="txtResultCode" size="3" maxlength="2" title="Result Code"
                       value="<%=strResultCode%>">
            </td>
            <td width="500" align="right"><b><%=strMessageInfo%></b></td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="txtResultDesc">Rechallenge Result</label></td>
            <td class="field">
                <input type="text" name="txtResultDesc" id="txtResultDesc" size="30" maxlength="50" title="Rechallenge Result"
                       value="<%=strResultDesc%>">
            </td>
        </tr>
        <tr>
            <td class="fLabel" width="125"><label for="cmbValidResult">Status</label></td>
            <td class="field" colspan="2">
                <span style="float: left">
                    <SELECT id="cmbValidResult" NAME="cmbValidResult" class="comboclass" style="width: 70px;" title="Status">
                        <option <%=(strValid.equals("All") ? "SELECTED" : "")%>>All</option>
                        <option <%=(strValid.equals("Active") ? "SELECTED" : "")%>>Active</option>
                        <option <%=(strValid.equals("Inactive") ? "SELECTED" : "")%>>Inactive</option>
                    </SELECT>

                </span>
            </td>
        </tr>
        <tr>
        <td>
            <td colspan="2">
                <input type="submit" name="cmdSearch" title="Start searching with specified criteria" class="button" value="Search" style="width: 87px;">
                <input type="button" name="cmdClear" title="Clear searching criteria" class="button" value="Clear Search" style="width: 87px;"
                       onclick="location.replace('pgNavigate.jsp?Target=pgRechallengeResultlist.jsp');"/>
            </td>
        </td>
    </form>
    <td width="16" align="right" >
        <a href="#" title="Print"
           <%
        if (al.size() > 0) {
            out.write("onclick=\"popupWnd('_blank', 'yes','pgPrintRechallenge.jsp?cod=" + strResultCode +
                    "&desc=" + strResultDesc + "&valid=" + nFormValid + "',800,600)\"");
        }
           %>>
            <img src="images/icons/print.gif" border='0'>
        </a>
    </td>
    <td>
        <form METHOD="POST" ACTION="pgRechallengeResult.jsp">
            <span style="float: right">
                <input type="hidden" name="SaveFlag" value="I">
                <input type="hidden" name="Parent" value="1">
                <input type="submit" name="cmdNew"  title="Adds a new entry"  class="<%=(bAdd ? "button" : "button disabled")%>"
                       value="New Rechallenge result" style="width: 130px;" <%=(bAdd ? "" : " DISABLED")%>>
            </span>
        </form>
    </td>
    </td>
</tr>
</table>
</td>
</tr>
<!-- Form Body Ends -->
<tr>
    <td style="height: 1px;"></td>
</tr>
<!-- Grid View Starts -->
<tr>
    <td class="formbody">
        <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
                <td class="form_group_title" colspan="10">Search Results
                    <%=" - " + al.size() + " record(s) found"%>
                </td>
            </tr>
        </table>
    </td>
</tr>
<% if (al.size() != 0) {%>
<tr>
    <td class="formbody">
        <table border='0' cellspacing='1' cellpadding='0' width='100%' class="grid_table" >
            <tr class="grid_header">
                <td width="16"></td>
                <% if (bDelete) {%>
                <td width="16"></td>
                <% }%>
                <td width="100">Result Code</td>
                <td>Rechallenge Result</td>
                <td width="100">Status</td>
                <td></td>
            </tr>
            <%
     rechallengeresult = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         rechallengeresult = ((RechallengeResult) al.get(i));
            %>
            <tr class="
                <%
                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                %>
                ">
                <% if (bEdit) {%>
                <td width="16" align="center" >
                    <a href="pgRechallengeResult.jsp?ResultCode=<% out.write(rechallengeresult.getResult_Code());%>&SaveFlag=U&Parent=1">
                        <img src="images/icons/edit.gif" title="Edit the entry with ID = <%=rechallengeresult.getResult_Code()%>" border='0'>
                    </a>
                </td>
                <% } else {%>
                <td width="16" align="center" >
                    <a href="pgRechallengeResult.jsp?ResultCode=<% out.write(rechallengeresult.getResult_Code());%>&SaveFlag=V&Parent=1">
                        <img src="images/icons/view.gif" title="View details of the entry with ID = <%=rechallengeresult.getResult_Code()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <% if (bDelete) {%>
                <td width="16" align="center" >
                    <a href="pgDelRechallengeResult.jsp?ResultCode=<% out.write(rechallengeresult.getResult_Code());%>"
                       onclick="return confirm('Are you sure you want to delete?','Sapher')">
                        <img src="images/icons/delete.gif" title="Delete the entry with ID = <%=rechallengeresult.getResult_Code()%>" border='0'>
                    </a>
                </td>
                <% }%>
                <td width="100" align='center'>
                    <%
                if (rechallengeresult.getIs_Valid() == 0) {
                    %>
                    <font color="red"> <% out.write(rechallengeresult.getResult_Code());%> </font>
                    <%
                } else {
                    out.write(rechallengeresult.getResult_Code());
                }
                    %>
                </td>
                <td align='left'>
                    <%
                if (rechallengeresult.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write(rechallengeresult.getResult_Desc());%></font>
                    <%
                } else {
                    out.write(rechallengeresult.getResult_Desc());
                }
                    %>
                </td>
                <td width="100" align='center'>
                    <%
                if (rechallengeresult.getIs_Valid() == 0) {
                    %>
                    <font color="red"><% out.write("Inactive");%></font>
                    <%
                } else {
                    out.write("Active");
                }
                    %>
                </td>
                <td></td>
            </tr>
            <% }%>
        </table>
    </td>
    <tr>
    </tr>
</tr>
<!-- Grid View Ends -->

<!-- Pagination Starts -->
<tr>
    <td class="formbody">
        <table border="0" cellspacing='0' cellpadding='0' width="100%" class="paginate_panel">
            <tr>
                <td>
                    <a href="pgRechallengeResultlist.jsp?CurPage=1">
                    <img src="images/icons/page-first.gif" title="Go to first page" border="0"></a>
                </td>
                <td><% if (nCurPage > 1) {%>
                    <A href="pgRechallengeResultlist.jsp?CurPage=<%=(nCurPage - 1)%>">
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0"></A>
                    <% } else {%>
                    <img src="images/icons/page-prev.gif" title="Go to previous page" border="0">
                    <% }%>
                </td>
                <td nowrap class="page_number">Page <%=nCurPage%> of <%=nPageCount%> </td>
                <td><% if (nCurPage < nPageCount) {%>
                    <A href="pgRechallengeResultlist.jsp?CurPage=<%=nCurPage + 1%>">
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0"></A>
                    <% } else {%>
                    <img src="images/icons/page-next.gif" title="Go to next page" border="0">
                    <% }%>
                </td>
                <td>
                    <a href="pgRechallengeResultlist.jsp?CurPage=<%=nPageCount%>">
                    <img src="images/icons/page-last.gif" title="Go to last page" border="0"></A>
                </td>
                <td width="100%"></td>
            </tr>
        </table>
    </td>
</tr>
<!-- Pagination Ends -->
<%}%>
<tr>
    <td style="height: 10px;"></td>
</tr>
</table>
<div style="height: 25px;"></div>
</body>
</html>