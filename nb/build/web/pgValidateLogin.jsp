<!--
Form Id : USM3
Date    : 11-12-2007.
Author  : Arun P. Jose
-->

<%@ page
    contentType="text/html; charset=iso-8859-1"
    language="java"
    import="bl.sapher.User"
    import="bl.sapher.general.RecordNotFoundException"
    import="bl.sapher.general.UserCountException"
    import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.TimeoutException" import="bl.sapher.general.UserBlockedException"
    import="bl.sapher.general.Logger"
    isThreadSafe="true"
    %>

<%!    private User currentUser = null;
%>

<%
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgValidateLogin.jsp");
        try {
            session.setAttribute("Company", request.getParameter("cmbCompany"));
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Creating user with params: " + (String) session.getAttribute("Company") + "," + request.getParameter("txtUsername"));
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), request.getParameter("txtUsername"));

        } catch (UserBlockedException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; User blocked exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getLocalizedMessage());

            pageContext.forward("pgLogin.jsp?LoginStatus=5");
            return;
        } catch (UserCountException uce) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; User count exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), uce.getLocalizedMessage());

            pageContext.forward("pgLogin.jsp?LoginStatus=7");
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Other exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getLocalizedMessage());
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
            

            pageContext.forward("pgLogin.jsp?LoginStatus=1");
            return;
        }
        try {
            session.setAttribute("CurrentUser", currentUser.getUsername());
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Username validate success");

            if (currentUser.getIs_Locked() == 1) {
                 Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin;");
               
                

                pageContext.forward("pgLogin.jsp?LoginStatus=2");
                return;
            }
            if (currentUser.getPwd_Change_Flag() == 1) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Reset Password");

                pageContext.forward("pgResetPwd.jsp?Next=2");
                return;
            }
            if (!currentUser.getPassword().equals(request.getParameter("txtPassword"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Password wrong" );
                
                pageContext.forward("pgShortSavUser.jsp?CurrentUser=" + currentUser.getUsername() + "&PwdMistake=1&Next=1");
                return;
            }
            session.setAttribute("CurrentUserPW", currentUser.getPassword());
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Login success");
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Password : " + currentUser.getPassword());

            pageContext.forward("pgShortSavUser.jsp?CurrentUser=" + currentUser.getUsername() + "&PwdMistake=0&Next=2");
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Other exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getLocalizedMessage());

            pageContext.forward("pgLogin.jsp?LoginStatus=4");
            return;
        }
%>