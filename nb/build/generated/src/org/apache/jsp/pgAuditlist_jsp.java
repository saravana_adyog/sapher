package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.Date;
import java.util.ArrayList;
import bl.sapher.AuditTrial;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;
import java.text.SimpleDateFormat;

public final class pgAuditlist_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "AUD1";
    private String strUsername = "";
    private String strInvId = "";
    private String strInvDesc = "";
    private String strTxnType = "";
    private String strTgtType = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private String strRefVal = "";
    private Date dtFrom = null;
    private java.util.Date dtTo = null;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private AuditTrial aTrial = null;
    private int nRowCount = 5;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
    private SimpleDateFormat dFormat = null;
    
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : AUD1\r\n");
      out.write("Date    : 15-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>Audit Trail - [Sapher]</title>\r\n");
      out.write("    <script>var SAPHER_PARENT_NAME = \"pgAuditlist\";</script>\r\n");
      out.write("    <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("    <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("    <!--Calendar Library Includes.. S => -->\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("    <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("    <!-- import the calendar script -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("    <!-- import the language module -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("    <!-- helper script that uses the calendar -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("    <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("    ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    ");

    aTrial = new AuditTrial((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgAuditlist.jsp");
        boolean resetSession = false;
        boolean currRole = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! No Permission.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {

                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Invalid username/password.");
    
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("    </script>\r\n");
      out.write("    ");

            return;
        }
        inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
        bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
        bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp: V/D=" + bView + "/" + bDelete);
    } catch (TimeoutException toe) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
    
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("    </script>\r\n");
      out.write("    ");

        return;
    } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
    
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("    </script>\r\n");
      out.write("    ");

            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAuditlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("txtAudUsername") == null) {
            if (session.getAttribute("txtAudUsername") == null) {
                strUsername = "";
            } else {
                strUsername = (String) session.getAttribute("txtAudUsername");
            }
        } else {
            strUsername = request.getParameter("txtAudUsername");
        }
        session.setAttribute("txtAudUsername", strUsername);

        if (request.getParameter("txtAudInv") == null) {
            if (session.getAttribute("txtAudInv") == null) {
                strInvId = "";
            } else {
                strInvId = (String) session.getAttribute("txtAudInv");
            }
        } else {
            strInvId = request.getParameter("txtAudInv");
        }
        session.setAttribute("txtAudInv", strInvId);

        if (request.getParameter("txtAudInvDesc") == null) {
            if (session.getAttribute("txtAudInvDesc") == null) {
                strInvDesc = "";
            } else {
                strInvDesc = (String) session.getAttribute("txtAudInvDesc");
            }
        } else {
            strInvDesc = request.getParameter("txtAudInvDesc");
        }
        session.setAttribute("txtAudInvDesc", strInvDesc);

        if (request.getParameter("txtRefVal") == null) {
            if (session.getAttribute("txtRefVal") == null) {
                strRefVal = "";
            } else {
                strRefVal = (String) session.getAttribute("txtRefVal");
            }
        } else {
            strRefVal = request.getParameter("txtRefVal");
        }
        session.setAttribute("txtRefVal", strRefVal);

        if (request.getParameter("cmbTxnType") == null) {
            if (session.getAttribute("cmbTxnType") == null) {
                strTxnType = "All";
            } else {
                strTxnType = (String) session.getAttribute("cmbTxnType");
            }
        } else {
            strTxnType = request.getParameter("cmbTxnType");
        }
        session.setAttribute("cmbTxnType", strTxnType);

        if (strTxnType.equals("Insert")) {
            strTxnType = "I";
        } else if (strTxnType.equals("Update")) {
            strTxnType = "U";
        } else if (strTxnType.equals("Delete")) {
            strTxnType = "D";
        } else if (strTxnType.equals("All")) {
            strTxnType = "";
        }

        if (request.getParameter("cmbTgtType") == null) {
            if (session.getAttribute("cmbTgtType") == null) {
                strTgtType = "All";
            } else {
                strTgtType = (String) session.getAttribute("cmbTgtType");
            }
        } else {
            strTgtType = request.getParameter("cmbTgtType");
        }
        session.setAttribute("cmbTgtType", strTgtType);

        if (strTgtType.equals("All")) {
            strTgtType = "";
        }

        if (request.getParameter("txtDtFrom") == null) {
            if (session.getAttribute("txtDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtDtFrom");
        }
        session.setAttribute("txtDtFrom", strDtFrom);

        if (request.getParameter("txtDtTo") == null) {
            if (session.getAttribute("txtDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtDtTo");
        }
        session.setAttribute("txtDtTo", strDtTo);

        if (resetSession) {
            strUsername = "";
            strInvDesc = "";
            strInvId = "";
            strTgtType = "";
            strTxnType = "";
            strDtFrom = "";
            strDtTo = "";
            strRefVal = "";
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = AuditTrial.listAudit((String) session.getAttribute("Company"), "", "", "", "", "", "", "");
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strUsername + "," + strInvId + "," + strTxnType + "," + strTgtType + "," + strDtFrom + "," + strDtTo + "," + strRefVal);
            al = AuditTrial.listAudit((String) session.getAttribute("Company"), strUsername, strInvId, strTxnType, strTgtType, strDtFrom, strDtTo, strRefVal);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
            }
        }

    
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <div style=\"height: 25px;\"></div>\r\n");
      out.write("    <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td id=\"formtitle\">Audit Trail - Audit Listing</td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Form Body Starts -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <form name=\"FrmAuditList\" METHOD=\"POST\" ACTION=\"pgAuditlist.jsp\" >\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td style=\"height: 10px;\"></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtAudInv\">Inventory</label></td>\r\n");
      out.write("                    <td class=\"field\" width=\"300\">\r\n");
      out.write("                        <input type=\"text\" id=\"txtAudInv\" name=\"txtAudInv\" size=\"30\" onkeyup=\"ajax_showOptions('txtAudInv_ID',this,'Inventory',event);\"\r\n");
      out.write("                               value=\"");
      out.print(strInvId);
      out.write("\">\r\n");
      out.write("                        <input type=\"text\" readonly size=\"5\" id=\"txtAudInv_ID\" name=\"txtAudInv_ID\"\r\n");
      out.write("                               value=\"");
      out.print(strInvDesc);
      out.write("\">\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtAudUsername\">Username</label></td>\r\n");
      out.write("                    <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtAudUsername\" id=\"txtAudUsername\" size=\"11\" maxlength=\"10\" title=\"Username\"\r\n");
      out.write("                               value=\"");
      out.print(strUsername);
      out.write("\">\r\n");
      out.write("                        <label style=\"margin-right: 2px;\" class=\"fLabel\">Ref Value</label>\r\n");
      out.write("                        <input style=\"padding-right: 0px; padding-left: 0px;\" type=\"text\" name=\"txtRefVal\" id=\"txtRefVal\" title=\"Ref Value\"\r\n");
      out.write("                               size=\"23\" maxlength=\"30\" value=\"");
      out.print(strRefVal);
      out.write("\">\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtDtFrom\">Date Range</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtDtFrom\" id=\"txtDtFrom\" size=\"18\" maxlength=\"18\" clear=2 title=\"From Date\"\r\n");
      out.write("                                           value=\"");
      out.print(strDtFrom);
      out.write("\" readonly  onkeydown=\"return ClearInput(event,2);\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select Date\"></a>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabel\" style=\"padding-right: 3px; padding-left: 3px;\">TO</td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtDtTo\" id=\"txtDtTo\" size=\"18\" maxlength=\"18\" clear=3 title=\"To Date\"\r\n");
      out.write("                                           value=\"");
      out.print(strDtTo);
      out.write("\" readonly  onkeydown=\"return ClearInput(event,3);\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select Date\"></a>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    Calendar.setup({\r\n");
      out.write("                        inputField : \"txtDtFrom\", //*\r\n");
      out.write("                        ifFormat : \"%d-%b-%Y %H:%M\",\r\n");
      out.write("                        showsTime : true,\r\n");
      out.write("                        button : \"cal_trigger1\", //*\r\n");
      out.write("                        step : 1\r\n");
      out.write("                    });\r\n");
      out.write("                </script>\r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    Calendar.setup({\r\n");
      out.write("                        inputField : \"txtDtTo\", //*\r\n");
      out.write("                        ifFormat : \"%d-%b-%Y %H:%M\",\r\n");
      out.write("                        showsTime : true,\r\n");
      out.write("                        button : \"cal_trigger2\", //*\r\n");
      out.write("                        step : 1\r\n");
      out.write("                    });\r\n");
      out.write("                </script>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"cmbTxnType\">Transaction</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                    <span style=\"float: left\">\r\n");
      out.write("                        <select id=\"cmbTxnType\" NAME=\"cmbTxnType\" class=\"comboclass\" style=\"width: 100px;\" title=\"Transaction\">\r\n");
      out.write("                            <option ");
      out.print((strTxnType.equals("") ? "SELECTED" : ""));
      out.write(">All</option>\r\n");
      out.write("                            <option ");
      out.print((strTxnType.equals("I") ? "SELECTED" : ""));
      out.write(">Insert</option>\r\n");
      out.write("                            <option ");
      out.print((strTxnType.equals("U") ? "SELECTED" : ""));
      out.write(">Update</option>\r\n");
      out.write("                            <option ");
      out.print((strTxnType.equals("D") ? "SELECTED" : ""));
      out.write(">Delete</option>\r\n");
      out.write("                        </select>\r\n");
      out.write("                    </span>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"cmbTgtType\">Screen Type</label></td>\r\n");
      out.write("                    <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                        <span style=\"float: left\">\r\n");
      out.write("                            <select id=\"cmbTgtType\" NAME=\"cmbTgtType\" class=\"comboclass\" style=\"width: 100px;\" title=\"Screen Type\">\r\n");
      out.write("                                <option ");
      out.print((strTgtType.equals("") ? "SELECTED" : ""));
      out.write(">All</option>\r\n");
      out.write("                                <option ");
      out.print((strTgtType.equals("CODE") ? "SELECTED" : ""));
      out.write(">CODE</option>\r\n");
      out.write("                                <option ");
      out.print((strTgtType.equals("CASE") ? "SELECTED" : ""));
      out.write(">CASE</option>\r\n");
      out.write("                            </select>\r\n");
      out.write("                        </span>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <td colspan=\"2\">\r\n");
      out.write("                        <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                        <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                               onclick=\"location.replace('pgNavigate.jsp?Target=pgAuditlist.jsp');\"/>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </td>\r\n");
      out.write("            </form>\r\n");
      out.write("            <td width=\"16\" align=\"right\" >\r\n");
      out.write("                <a href=\"#\" title=\"Print\"\r\n");
      out.write("                   ");

        if (al.size() > 0) {
            session.setAttribute("AuditPrintData", al);
            out.write("onclick=\"popupWnd('_blank', 'yes','pgPrintAuditTrial.jsp?usr=" + strUsername +
                    "&invId=" + strInvId + "&transTyp=" + strTxnType + "&tgtTyp=" + strTgtType +
                    "&frmDt=" + strDtFrom +"&toDt=" + strDtTo +"&refVal=" + strRefVal + "',800,600)\"");
        }
                   
      out.write(">\r\n");
      out.write("                    <img src=\"images/icons/print.gif\" border='0'>\r\n");
      out.write("                </a>\r\n");
      out.write("            </td>\r\n");
      out.write("\r\n");
      out.write("        </tr>\r\n");
      out.write("    </table>\r\n");
      out.write("    </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Form Body Ends -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td style=\"height: 1px;\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("\r\n");
      out.write("    <!-- Grid View Starts -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                        ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    ");
 if (al.size() != 0) {
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                <tr class=\"grid_header\">\r\n");
      out.write("                    ");
 if (bDelete) {
      out.write("\r\n");
      out.write("                    <td width=\"16\"></td>\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                    <td width=\"30\">User</td>\r\n");
      out.write("                    <td width=\"45\">Date</td>\r\n");
      out.write("                    <td width=\"45\">Time</td>\r\n");
      out.write("                    <td width=\"30\">Type</td>\r\n");
      out.write("                    <td width=\"40\">Txn</td>\r\n");
      out.write("                    <td width=\"30\">Inv Id</td>\r\n");
      out.write("                    <td width=\"100\">Inv Desc</td>\r\n");
      out.write("                    <td width=\"50\">Ref Val</td>\r\n");
      out.write("                    <td width=\"100\">Field Name</td>\r\n");
      out.write("                    <td width=\"100\">Old Value</td>\r\n");
      out.write("                    <td width=\"100\">New Value</td>\r\n");
      out.write("                    <td></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                ");

     aTrial = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aTrial = ((AuditTrial) al.get(i));
                
      out.write("\r\n");
      out.write("                <tr class=\"\r\n");
      out.write("                    ");

                    if (i % 2 == 0) {
                        out.write("even_row");
                    } else {
                        out.write("odd_row");
                    }
                    
      out.write("\r\n");
      out.write("                    \">\r\n");
      out.write("                    ");
 if (bDelete) {
      out.write("\r\n");
      out.write("                    <td width=\"16\" align=\"center\" >\r\n");
      out.write("                        <a href=\"pgDelAudit.jsp?ChgNo=");
      out.print(aTrial.getChg_No());
      out.write("\"\r\n");
      out.write("                           onclick=\"return confirm('Are you sure you want to delete?','Sapher')\">\r\n");
      out.write("                            <img src=\"images/icons/delete.gif\" title=\"Delete the entry with ID = ");
      out.print(aTrial.getChg_No());
      out.write("\" border='0'>\r\n");
      out.write("                        </a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                    <td align='center' width=\"30\">");
      out.print(aTrial.getUsername());
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"45\">\r\n");
      out.write("                        ");

                    dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
                    out.write(dFormat.format(aTrial.getChg_Date()));
                        
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td align='center' width=\"45\">\r\n");
      out.write("                        ");

                    dFormat = new SimpleDateFormat("HH:mm:ss");
                    out.write(dFormat.format(aTrial.getChg_Time()));
                        
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td align='center' width=\"30\">");
      out.print(aTrial.getTgt_Type());
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"40\">\r\n");
      out.write("                        ");

                    if (aTrial.getTxn_Type().equals("I")) {
                        out.write("Insert");
                    }
                    if (aTrial.getTxn_Type().equals("U")) {
                        out.write("Update");
                    }
                    if (aTrial.getTxn_Type().equals("D")) {
                        out.write("Delete");
                    }
                        
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td align='center' width=\"30\">");
      out.print(aTrial.getInventory().getInv_Id());
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"100\">");
      out.print(aTrial.getInventory().getInv_Desc());
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"50\">");
      out.print(aTrial.getPk_Col_Value());
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"100\">");
      out.print((aTrial.getInventoryCols() == null ? "" : aTrial.getInventoryCols().getField_Name()));
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"100\">");
      out.print((aTrial.getOld_Val() == null ? "" : aTrial.getOld_Val()));
      out.write("</td>\r\n");
      out.write("                    <td align='center' width=\"100\">");
      out.print((aTrial.getNew_Val() == null ? "" : aTrial.getNew_Val()));
      out.write("</td>\r\n");
      out.write("                    <td></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                ");
 }
      out.write("\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("        <tr>\r\n");
      out.write("        </tr>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("    <!-- Pagination Starts -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <a href=\"pgAuditlist.jsp?CurPage=1\">\r\n");
      out.write("                        <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                        <A href=\"pgAuditlist.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\"></A>\r\n");
      out.write("                        ");
 } else {
      out.write("\r\n");
      out.write("                        <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                    <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                        <a href=\"pgAuditlist.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\"></a>\r\n");
      out.write("                        ");
 } else {
      out.write("\r\n");
      out.write("                        <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <a href=\"pgAuditlist.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\"></a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td width=\"100%\"></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Pagination Ends -->\r\n");
      out.write("    ");
}
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td style=\"height: 10px;\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    </table>\r\n");
      out.write("    <div style=\"height: 25px;\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
