package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.InventoryCols;
import bl.sapher.Inventory;
import bl.sapher.User;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgInventoryColList_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "AUD2";
    private String strInvId = "";
    private String strInvColName = "";
    //    private String strInvType = "";
    private boolean bView;
    private boolean bEdit;
    private ArrayList al = null;
    private User currentUser = null;
    private InventoryCols anInventoryCol = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : AUD2\r\n");
      out.write("Date    : 06-12-2007.\r\n");
      out.write("Author  : Jaikishan. S\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Inventory List - [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        anInventoryCol = new InventoryCols((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgInventoryColList.jsp");
        boolean resetSession = false;
        String strMessageInfo = "";

        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Successfully Updated.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Description already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventoryColList.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');

            Logger.writeLog((String) session.getAttribute("LogFileName"), ".jsp: E/V=" + bEdit + "/" + bView);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventoryColList.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventoryColList.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventoryColList.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgInventoryColList.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }

        if (request.getParameter("txtInvId") == null) {
            if (session.getAttribute("txtInvId") == null) {
                strInvId = "";
            } else {
                strInvId = (String) session.getAttribute("txtInvId");
            }
        } else {
            strInvId = request.getParameter("txtInvId");
        }
        session.setAttribute("txtInvId", strInvId);

        if (request.getParameter("txtInvDesc") == null) {
            if (session.getAttribute("txtInvDesc") == null) {
                strInvColName = "";
            } else {
                strInvColName = (String) session.getAttribute("txtInvDesc");
            }
        } else {
            strInvColName = request.getParameter("txtInvDesc");
        }
        session.setAttribute("txtInvDesc", strInvColName);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strInvId + "," + strInvColName);
        al = InventoryCols.listInvcol((String) session.getAttribute("Company"), "", strInvId, strInvColName);

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
            }
        }

        
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Audit Trail - Inventory Column Listing</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form name=\"FrmInvColList\" METHOD=\"POST\" ACTION=\"pgInventoryColList.jsp\" >\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 10px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtInvId\">Inventory Id</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtInvId\" id=\"txtInvId\" size=\"10\" maxlength=\"10\" title=\"Inventory ID\" readonly\r\n");
      out.write("                                           value=\"");
      out.print(strInvId);
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtInvDesc\">Column Desc</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtInvDesc\" id=\"txtInvDesc\" size=\"30\" maxlength=\"50\" title=\"Inventory\"\r\n");
      out.write("                                           value=\"");
      out.print(strInvColName);
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("\r\n");
      out.write("                            <tr>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <td colspan=\"2\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                                           onclick=\"location.replace('pgNavigate.jsp?Target=pgInventoryColList.jsp');\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 1px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                                ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");
 if (al.size() != 0) {
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                        <tr class=\"grid_header\">\r\n");
      out.write("                            <td width=\"16\"></td>\r\n");
      out.write("                            <td width=\"100\">Inventory Column Id</td>\r\n");
      out.write("                            <td>Inventory Column Desc</td>\r\n");
      out.write("                            <td>Audit Flag</td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");

     anInventoryCol = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         anInventoryCol = ((InventoryCols) al.get(i));
                        
      out.write("\r\n");
      out.write("                        <tr class=\"\r\n");
      out.write("                            ");

                            if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            
      out.write("\r\n");
      out.write("                            \">\r\n");
      out.write("                            ");
 if (bEdit) {
      out.write("\r\n");
      out.write("                            <td width=\"16\" align=\"center\" >\r\n");
      out.write("                                <a href=\"pgInventoryCol.jsp?InvCId=");
      out.print(anInventoryCol.getInv_Col_Id());
      out.write("&SaveFlag=U&Parent=1\">\r\n");
      out.write("                                    <img src=\"images/icons/edit.gif\" title=\"Edit the entry with ID = ");
      out.print(anInventoryCol.getInv_Col_Id());
      out.write("\" border='0'>\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            ");
 } else {
      out.write("\r\n");
      out.write("                            <td width=\"16\" align=\"center\" >\r\n");
      out.write("                                <a href=\"pgInventoryCol.jsp?InvCId=");
      out.print(anInventoryCol.getInv_Col_Id());
      out.write("&SaveFlag=V&Parent=1\">\r\n");
      out.write("                                    <img src=\"images/icons/view.gif\" title=\"View details of the entry with ID = ");
      out.print(anInventoryCol.getInv_Col_Id());
      out.write("\" border='0'>\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            ");
 }
      out.write("\r\n");
      out.write("                            <td width=\"100\" align='center'>\r\n");
      out.write("                                ");

                            out.write(anInventoryCol.getInv_Col_Id());
                                
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td  align='left'>\r\n");
      out.write("                                ");

                            out.write(anInventoryCol.getField_Name());
                                
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"25\" align='center'>\r\n");
      out.write("                                ");
 if (anInventoryCol.getAudit_Flag() == 1) {
      out.write("\r\n");
      out.write("                                <img src=\"images/icons/tick.gif\" border='0'>\r\n");
      out.write("                                ");
}
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("                <tr>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("            <!-- Pagination Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"pgInventoryColList.jsp?CurPage=1\">\r\n");
      out.write("                                <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                                <A href=\"pgInventoryColList.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                                <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\"></A>\r\n");
      out.write("                                ");
 } else {
      out.write("\r\n");
      out.write("                                <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                                ");
 }
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                            <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                                <A href=\"pgInventoryColList.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                                <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\"></A>\r\n");
      out.write("                                ");
 } else {
      out.write("\r\n");
      out.write("                                <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                                ");
 }
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"pgInventoryColList.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                                <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\"></a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"100%\"></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Pagination Ends -->\r\n");
      out.write("            ");
}
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
