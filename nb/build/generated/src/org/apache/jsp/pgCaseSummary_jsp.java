package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.CaseDetails;
import bl.sapher.Adverse_Event;
import bl.sapher.Conc_Medication;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public final class pgCaseSummary_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private boolean bView;
    private boolean bEdit;
    private User currentUser = null;
    private Inventory inv = null;
    private SimpleDateFormat dFormat = null;
    CaseDetails aCase = null;
    private String strRepNum = "";
    // Pagination
    private int nCMListRowCount = 5;
    private int nAEListRowCount = 5;
    private int nCMListPageCount = 1;
    private int nAEListPageCount = 1;
    private int nCMListCurPage = 1;
    private int nAEListCurPage = 1;
    private String strMessageInfo = "";
    // Variable taht decides thether to show the 'Back' button in the page
    private String strShowBackBtn = "";
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!--\r\n");
      out.write("Form Id : CSD1\r\n");
      out.write("Date    : 29-2-2008.\r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Case Summary - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        aCase = new CaseDetails((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaseSummary.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; UserBlocked exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        boolean resetSession = false;
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Unknown reason.</font>";
            }
        } else {
            strMessageInfo = "";
        }

        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Update Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Update Failed! Unknown reason.</font>";
            }
        } else {
            strMessageInfo = "";
        }

        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp: E/V=" + bEdit + "/" + bView);
            if (!bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; Lack of permission. [View=" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }

            if (!bEdit) {
                saveFlag = "V";
            }
            dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);

            // Check whether the 'Back' button is to be shown
            if (request.getParameter("ShowBkBtn") != null) {
                strShowBackBtn = request.getParameter("ShowBkBtn");
            }

            if (request.getParameter("RepNum") != null) {
                strRepNum = request.getParameter("RepNum");
            }

            if (request.getParameter("AeListCurPage") == null) {
                nAEListCurPage = 1;
            } else {
                nAEListCurPage = Integer.parseInt(request.getParameter("AeListCurPage"));
            }

            if (request.getParameter("CmListCurPage") == null) {
                nCMListCurPage = 1;
            } else {
                nCMListCurPage = Integer.parseInt(request.getParameter("CmListCurPage"));
            }

            aCase = new CaseDetails((String) session.getAttribute("Company"), "y", Integer.parseInt(request.getParameter("RepNum")));

        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseSummary.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='750' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Case Summary</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmCaseSummary\" name=\"FrmCaseSummary\" method=\"POST\" action=\"pgCaseDetails.jsp\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Case summary</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtRepNum\">Rep Num</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtRepNum\"\r\n");
      out.write("                                                             id=\"txtRepNum\" size=\"12\" maxlength=\"10\"\r\n");
      out.write("                                                             value=");
      out.print(aCase.getRep_Num());
      out.write(" readonly />\r\n");
      out.write("                                    <label class=\"fLabel\" for=\"txtRepNum\">\r\n");
      out.write("                                        ");

        if (aCase.getIs_Suspended() == 0) {
            out.write("ACTIVE");
        } else {
            out.write("<font color=\"RED\">SUSPENDED</font>");
        }
                                        
      out.write("\r\n");
      out.write("                                    </label>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td colspan=\"8\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtRecvDate\">Received Date</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtRecvDate\" id=\"txtRecvDate\" size=\"12\" maxlength=\"12\"\r\n");
      out.write("                                                       readonly\r\n");
      out.write("                                                       value=\"");
      out.print((aCase.getDate_Recv() != null ? dFormat.format(aCase.getDate_Recv()) : ""));
      out.write("\">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtCountryCode\">Country</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtCountryCode\" id=\"txtCountryCode\" size=\"3\" maxlength=\"3\"\r\n");
      out.write("                                           readonly\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getCntry() == null ? "" : aCase.getCntry()));
      out.write("\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtCountryNm\" id=\"txtCountryNm\" size=\"30\" maxlength=\"50\" width=10\r\n");
      out.write("                                           readonly value=\"");
      out.print((aCase.getCountry_Nm() == null ? "" : aCase.getCountry_Nm()));
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode\">Product</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtProdCode\" id=\"txtProdCode\" size=\"13\" maxlength=\"12\"\r\n");
      out.write("                                           readonly\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getProdcode() == null ? "" : aCase.getProdcode()));
      out.write("\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtGenericNm\" id=\"txtGenericNm\" size=\"28\" maxlength=\"50\"\r\n");
      out.write("                                           readonly value=\"");
      out.print((aCase.getGeneric_Nm() == null ? "" : aCase.getGeneric_Nm()));
      out.write("\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtBrandNm\" id=\"txtBrandNm\" size=\"29\" maxlength=\"50\"\r\n");
      out.write("                                           readonly value=\"");
      out.print((aCase.getBrand_Nm() == null ? "" : aCase.getBrand_Nm()));
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtInitials\">Patient Initials</label></td>\r\n");
      out.write("                                <td >\r\n");
      out.write("                                    <input type=\"text\" name=\"txtInitials\" id=\"txtInitials\" size=\"5\" maxlength=\"4\"\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getP_Initials() == null ? "" : aCase.getP_Initials()));
      out.write("\" readonly>\r\n");
      out.write("                                    <label class=\"fLabel\" for=\"txtAgeRep\">Age Reported</label>\r\n");
      out.write("                                    <input type=\"text\" name=\"txtAgeRep\" id=\"txtAgeRep\" size=\"3\" maxlength=\"3\"\r\n");
      out.write("                                           value=\"");
      out.print("" + (aCase.getAge_Reported() > 1 ? aCase.getAge_Reported() : ""));
      out.write("\" readonly>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtSexCode\">Sex</label></td>\r\n");
      out.write("                                <td>\r\n");
      out.write("                                    <input type=\"text\" name=\"txtSexCode\" id=\"txtSexCode\" size=\"3\" maxlength=\"1\"\r\n");
      out.write("                                           readonly\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getSexcode() == null ? "" : aCase.getSexcode()));
      out.write("\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtSex\" id=\"txtSex\" size=\"30\" maxlength=\"20\"\r\n");
      out.write("                                           readonly\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getSex_Desc() == null ? "" : aCase.getSex_Desc()));
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtTrial\">Source/Trial</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtTrial\" id=\"txtTrial\" size=\"36\" maxlength=\"20\"\r\n");
      out.write("                                           readonly\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getTrialnum() == null ? "" : aCase.getTrialnum()));
      out.write("\" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label></label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"2\" >\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdEdit_View\" title=\"Details of this case\" class=\"button\" value=\"");
      out.print((bEdit ? "Edit" : "View"));
      out.write("\" style=\"width: 60px;\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdClone\" title=\"Clone this case\" class=\"button\"\r\n");
      out.write("                                           value=\"Clone Case\" ");
      out.print((bEdit ? "" : " Disabled "));
      out.write(" style=\"width: 90px;\"\r\n");
      out.write("                                           onclick=\"location.replace('pgSavCaseClone.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&user=");
      out.print(currentUser.getUsername());
      out.write("');\" />\r\n");
      out.write("                                    <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"0\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"SaveFlag\" name=\"SaveFlag\" value=\"");
      out.print((bEdit ? "U" : "V"));
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"RepNum\" name=\"RepNum\" value=\"");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"2\" align=\"right\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdDocs\" style=\"width: 80px;\" class=\"button\" value=\"Documents\" title=\"Documents related to the case ");
      out.print(strRepNum);
      out.write("\"\r\n");
      out.write("                                           onclick=\"location.replace('pgNavigate.jsp?Target=pgDocslist.jsp?SaveFlag=U&RepNum=");
      out.print(strRepNum);
      out.write("');\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" />\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdNewAE\" style=\"width: 60px;\" ");
      out.print((bEdit ? "" : "DISABLED"));
      out.write(" class=\"");
      out.print((bEdit ? "button" : "button disabled"));
      out.write("\" value=\"Add AE\" title=\"Add new Adverse Event\" onclick=\"javascript:location.replace('pgAE.jsp?AeSaveFlag=I&RepNum=");
      out.print(strRepNum);
      out.write("&Parent=1');\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdNewCM\" style=\"width: 100px;\" ");
      out.print((bEdit ? "" : "DISABLED"));
      out.write(" class=\"");
      out.print((bEdit ? "button" : "button disabled"));
      out.write("\" value=\"Add Medication\" title=\"Add new Medication\" onclick=\"javascript:location.replace('pgConcMed.jsp?SaveFlag=I&RepNum=");
      out.print(strRepNum);
      out.write("&Parent=1');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 10px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                ");

        ArrayList alAE = Adverse_Event.listAEventFromView((String) session.getAttribute("Company"), Integer.parseInt(strRepNum));
                                
      out.write("\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"4\">Adverse Event\r\n");
      out.write("                                    ");
if (alAE != null) {
            out.write(" - " + alAE.size() + " record(s)");
        }
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("\r\n");
      out.write("                                <td class=\"form_group_title\" align=\"right\" >\r\n");
      out.write("                                    <!-- AE Summary button -->\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdAeSummary\" class=\"button\" value=\"Narrative\" style=\"width: 100px;\"\r\n");
      out.write("                                           onclick=\"location.replace('pgAeSummary.jsp?SaveFlag=U&RepNum=");
      out.print(strRepNum);
      out.write("');\" title=\"Adverse Event Summary\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td colspan=\"10\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td>\r\n");
      out.write("                                                ");
 if (alAE.size() != 0) {
      out.write("\r\n");
      out.write("                                                <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\">\r\n");
      out.write("                                                    <tr class=\"grid_header\">\r\n");
      out.write("                                                        <td width=\"16\"></td>\r\n");
      out.write("                                                        ");
 if (bEdit) {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\"></td>\r\n");
      out.write("                                                        ");
 }
      out.write("\r\n");
      out.write("                                                        <td width=\"50\">LLT Code</td>\r\n");
      out.write("                                                        <td width=\"200\">Low Level Term</td>\r\n");
      out.write("                                                        <td width=\"75\">Seriousness</td>\r\n");
      out.write("                                                        <td width=\"200\">Sys Organ Desc</td>\r\n");
      out.write("                                                        <td></td>\r\n");
      out.write("                                                    </tr>\r\n");
      out.write("                                                    ");

     if (alAE != null) {
         Adverse_Event ae = null;

         nAEListPageCount = (int) Math.ceil((double) alAE.size() / nAEListRowCount);
         if (nAEListCurPage > nAEListPageCount) {
             nAEListCurPage = 1;
         }
         int nAEListStartPos = ((nAEListCurPage - 1) * nAEListRowCount);
         for (int i = nAEListStartPos;
                 i < ((nAEListStartPos + nAEListRowCount) > alAE.size() ? alAE.size() : (nAEListStartPos + nAEListRowCount));
                 i++) {
             ae = ((Adverse_Event) alAE.get(i));
             if (ae != null) {
                                                    
      out.write("\r\n");
      out.write("                                                    <tr class=\"\r\n");
      out.write("                                                        ");

                                                                if (i % 2 == 0) {
                                                                    out.write("even_row");
                                                                } else {
                                                                    out.write("odd_row");
                                                                }
                                                        
      out.write("\r\n");
      out.write("                                                        \">\r\n");
      out.write("                                                        ");

                                                                if (bEdit) {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\" align=\"center\" title=\"Edit\">\r\n");
      out.write("                                                            <a href=\"pgAE.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&LltCode=");
      out.print(ae.getLlt_Code());
      out.write("&AeSaveFlag=U\">\r\n");
      out.write("                                                                <img src=\"images/icons/edit.gif\" alt=\"Edit\" title=\"Edit the Adverse Event with ID = ");
      out.print(strRepNum);
      out.write("\" border='0'>\r\n");
      out.write("                                                            </a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        ");
 } else {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\" align=\"center\" title=\"View\">\r\n");
      out.write("                                                            <a href=\"pgAE.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&LltCode=");
      out.print(ae.getLlt_Code());
      out.write("&AeSaveFlag=V\">\r\n");
      out.write("                                                                <img src=\"images/icons/view.gif\" alt=\"View\" title=\"View details of the Adverse Event with ID = ");
      out.print(strRepNum);
      out.write("\" border='0'>\r\n");
      out.write("                                                            </a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        ");
 }
      out.write("\r\n");
      out.write("                                                        ");
 if (bEdit) {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\" align=\"center\" title=\"Delete\">\r\n");
      out.write("                                                            <a href=\"pgDelAE.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&LltCode=");
      out.print(ae.getLlt_Code());
      out.write("\"\r\n");
      out.write("                                                               onclick=\"return confirm('Are you sure you want to delete?','Sapher')\">\r\n");
      out.write("                                                                <img src=\"images/icons/delete.gif\" alt=\"Delete\" title=\"Delete the Adverse Event with ID = ");
      out.print(strRepNum);
      out.write("\"  border='0'>\r\n");
      out.write("                                                            </a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        ");
 }
      out.write("\r\n");
      out.write("                                                        <td width=\"50\" align='left' title=\"Diag code\">\r\n");
      out.write("                                                            ");
out.write(ae.getLlt_Code());
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"200\" align='left'>\r\n");
      out.write("                                                            ");
out.write(ae.getLlt_Desc());
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"75\" align='left'>\r\n");
      out.write("                                                            ");
out.write(ae.getClassification_Desc());
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"200\">\r\n");
      out.write("                                                            ");
out.write(ae.getBody_System());
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td></td>\r\n");
      out.write("                                                    </tr>\r\n");
      out.write("                                                    ");
 }
         }
     }
      out.write("\r\n");
      out.write("                                                </table>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"height: 5px;\"></td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td>\r\n");
      out.write("                                                ");
 if (alAE.size() != 0) {
      out.write("\r\n");
      out.write("                                                <table border=\"0\" cellspacing='0' cellpadding='5' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                                                    <tr>\r\n");
      out.write("                                                        <td>\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?AeListCurPage=1&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-first.gif\" alt=\"|<<\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td>");
 if (nAEListCurPage > 1) {
      out.write("\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?AeListCurPage=");
      out.print((nAEListCurPage - 1));
      out.write("&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-prev.gif\" alt=\"<\" title=\"Go to previous page\" border=\"0\"></a>\r\n");
      out.write("                                                            ");
 } else {
      out.write("\r\n");
      out.write("                                                            <img src=\"images/icons/page-prev.gif\" alt=\"<\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                                                            ");
 }
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td nowrap class=\"page_number\">Page ");
      out.print(nAEListCurPage);
      out.write(" of ");
      out.print(nAEListPageCount);
      out.write(" </td>\r\n");
      out.write("                                                        <td>");
 if (nAEListCurPage < nAEListPageCount) {
      out.write("\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?AeListCurPage=");
      out.print(nAEListCurPage + 1);
      out.write("&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-next.gif\" alt=\">\" title=\"Go to next page\" border=\"0\"></a>\r\n");
      out.write("                                                            ");
 } else {
      out.write("\r\n");
      out.write("                                                            <img src=\"images/icons/page-next.gif\" alt=\">\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                                                            ");
 }
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td>\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?AeListCurPage=");
      out.print(nAEListPageCount);
      out.write("&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-last.gif\" alt=\">>|\" title=\"Go to last page\" border=\"0\"></a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"100%\"></td>\r\n");
      out.write("                                                    </tr>\r\n");
      out.write("                                                </table>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 10px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                ");

        ArrayList alCM = Conc_Medication.listConc_MedicationFromView((String) session.getAttribute("Company"), Integer.parseInt(strRepNum));
                                
      out.write("\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"4\">Concomitant Medication\r\n");
      out.write("                                    ");
if (alCM != null) {
            out.write(" - " + alCM.size() + " record(s)");
        }
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("\r\n");
      out.write("                                <td class=\"form_group_title\" align=\"right\" >\r\n");
      out.write("                                    <!-- CM Summary button -->\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdCmSummary\" class=\"button\" value=\"Medical History\" style=\"width: 100px;\"\r\n");
      out.write("                                           onclick=\"location.replace('pgConcMedSummary.jsp?SaveFlag=U&RepNum=");
      out.print(strRepNum);
      out.write("');\" title=\"Concomitant Medication Summary\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td colspan=\"10\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td colspan=\"10\">\r\n");
      out.write("                                                ");
 if (alCM.size() != 0) {
      out.write("\r\n");
      out.write("                                                <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\">\r\n");
      out.write("                                                    <tr class=\"grid_header\">\r\n");
      out.write("                                                        <td width=\"16\"></td>\r\n");
      out.write("                                                        ");
 if (bEdit) {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\"></td>\r\n");
      out.write("                                                        ");
 }
      out.write("\r\n");
      out.write("                                                        <td width=\"150\">Generic Name</td>\r\n");
      out.write("                                                        <td width=\"150\">Brand Name</td>\r\n");
      out.write("                                                        <td width=\"100\">Start Date</td>\r\n");
      out.write("                                                        <td width=\"100\">End Date</td>\r\n");
      out.write("                                                        <td width=\"50\">Duration(Days)</td>\r\n");
      out.write("                                                        <td></td>\r\n");
      out.write("                                                    </tr>\r\n");
      out.write("                                                    ");

     if ((alCM != null)) {
         Conc_Medication cm = null;

         if (alCM.size() != 0) {
             nCMListPageCount = (int) Math.ceil((double) alCM.size() / nCMListRowCount);
         }
         if (nCMListCurPage > nCMListPageCount) {
             nCMListCurPage = 1;
         }
         int nCMListStartPos = ((nCMListCurPage - 1) * nCMListRowCount);
         for (int i = nCMListStartPos;
                 i < ((nCMListStartPos + nCMListRowCount) > alCM.size() ? alCM.size() : (nCMListStartPos + nCMListRowCount));
                 i++) {
             cm = ((Conc_Medication) alCM.get(i));
             if (cm != null) {
                                                    
      out.write("\r\n");
      out.write("                                                    <tr class=\"\r\n");
      out.write("                                                        ");

                                                                if (i % 2 == 0) {
                                                                    out.write("even_row");
                                                                } else {
                                                                    out.write("odd_row");
                                                                }
                                                        
      out.write("\r\n");
      out.write("                                                        \">\r\n");
      out.write("                                                        ");

                                                                if (bEdit) {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\" align=\"center\" title=\"Edit\">\r\n");
      out.write("                                                            <a href=\"pgConcMed.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&ProdCode=");
      out.print(cm.getProduct_Code());
      out.write("&SaveFlag=U\">\r\n");
      out.write("                                                                <img src=\"images/icons/edit.gif\" alt=\"Edit\" title=\"Edit the Concomitant Medication with ID = ");
      out.print(strRepNum);
      out.write("\" border='0'>\r\n");
      out.write("                                                            </a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        ");
 } else {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\" align=\"center\" title=\"View\">\r\n");
      out.write("                                                            <a href=\"pgConcMed.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&ProdCode=");
      out.print(cm.getProduct_Code());
      out.write("&SaveFlag=V\">\r\n");
      out.write("                                                                <img src=\"images/icons/view.gif\" alt=\"View\" title=\"View the Concomitant Medication with ID = ");
      out.print(strRepNum);
      out.write("\" border='0'>\r\n");
      out.write("                                                            </a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        ");
 }
      out.write("\r\n");
      out.write("                                                        ");
 if (bEdit) {
      out.write("\r\n");
      out.write("                                                        <td width=\"16\" align=\"center\" title=\"Delete\">\r\n");
      out.write("                                                            <a href=\"pgDelConcMed.jsp?RepNum=");
      out.print(strRepNum);
      out.write("&ProdCode=");
      out.print(cm.getProduct_Code());
      out.write("\"\r\n");
      out.write("                                                               onclick=\"return confirm('Are you sure you want to delete?','Sapher')\">\r\n");
      out.write("                                                                <img src=\"images/icons/delete.gif\" alt=\"Delete\" title=\"Delete the Concomitant Medication with ID = ");
      out.print(strRepNum);
      out.write("\" border='0'>\r\n");
      out.write("                                                            </a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        ");
 }
      out.write("\r\n");
      out.write("                                                        <td width=\"150\" align='left' title=\"Generic name\">\r\n");
      out.write("                                                            ");
      out.print(cm.getGeneric_Nm());
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"150\" align='left' title=\"Brand name\">\r\n");
      out.write("                                                            ");
      out.print(cm.getBrand_Nm());
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td title=\"Treatment start date\">\r\n");
      out.write("                                                            ");

                                                                if (cm.getTreat_Start() != null) {
                                                                    out.write(dFormat.format(cm.getTreat_Start()));
                                                                }
                                                            
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td title=\"Treatment end date\">\r\n");
      out.write("                                                            ");

                                                                if (cm.getTreat_Stop() != null) {
                                                                    out.write(dFormat.format(cm.getTreat_Stop()));
                                                                }
                                                            
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"100\" align='left' title=\"Treatment duration\">\r\n");
      out.write("                                                            ");
      out.print("" + (cm.getDuration() == -1 ? "" : cm.getDuration()));
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td></td>\r\n");
      out.write("                                                    </tr>\r\n");
      out.write("                                                    ");
}
         }
     }
                                                    
      out.write("\r\n");
      out.write("                                                </table>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"height: 5px;\"></td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td colspan=\"10\" class=\"formbody\">\r\n");
      out.write("                                                ");
 if (alCM.size() != 0) {
      out.write("\r\n");
      out.write("                                                <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                                                    <tr>\r\n");
      out.write("                                                        <td>\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?CmListCurPage=1&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-first.gif\" alt=\"|<<\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td>");
 if (nCMListCurPage > 1) {
      out.write("\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?CmListCurPage=");
      out.print((nCMListCurPage - 1));
      out.write("&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-prev.gif\" alt=\"<\" title=\"Go to previous page\" border=\"0\"></a>\r\n");
      out.write("                                                            ");
 } else {
      out.write("\r\n");
      out.write("                                                            <img src=\"images/icons/page-prev.gif\" alt=\"<\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                                                            ");
 }
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td nowrap class=\"page_number\">Page ");
      out.print(nCMListCurPage);
      out.write(" of ");
      out.print(nCMListPageCount);
      out.write(" </td>\r\n");
      out.write("                                                        <td>");
 if (nCMListCurPage < nCMListPageCount) {
      out.write("\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?CmListCurPage=");
      out.print(nCMListCurPage + 1);
      out.write("&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-next.gif\" alt=\">\" title=\"Go to next page\" border=\"0\"></a>\r\n");
      out.write("                                                            ");
 } else {
      out.write("\r\n");
      out.write("                                                            <img src=\"images/icons/page-next.gif\" alt=\">\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                                                            ");
 }
      out.write("\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td>\r\n");
      out.write("                                                            <a href=\"pgCaseSummary.jsp?CmListCurPage=");
      out.print(nCMListPageCount);
      out.write("&RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                                            <img src=\"images/icons/page-last.gif\" alt=\">>|\" title=\"Go to last page\"  border=\"0\"></a>\r\n");
      out.write("                                                        </td>\r\n");
      out.write("                                                        <td width=\"100%\"></td>\r\n");
      out.write("                                                    </tr>\r\n");
      out.write("                                                </table>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 10px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td colspan=\"4\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdCIOMS\" style=\"width: 80px;\" class=\"button\" value=\"CIOMS I\" title=\"CIOMS I PDF Report\"\r\n");
      out.write("                                           onclick=\"popupWnd('bpopup', 'yes','pgPrintCIOMS1.jsp?RepNum=");
      out.print(strRepNum);
      out.write("',800,600)\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" />\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdMedWatch\" style=\"width: 80px;\" class=\"button\" value=\"MedWatch\" title=\"MedWatch PDF Report\"\r\n");
      out.write("                                           onclick=\"popupWnd('bpopup', 'yes','pgPrintMedWatch.jsp?RepNum=");
      out.print(strRepNum);
      out.write("',800,600)\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" />\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdWHO\" style=\"width: 80px;\" class=\"button\" value=\"WHO\" title=\"WHO PDF Report\"\r\n");
      out.write("                                           onclick=\"popupWnd('bpopup', 'yes','pgPrintWHO.jsp?RepNum=");
      out.print(strRepNum);
      out.write("',800,600)\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" />\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdFullCase\" style=\"width: 80px;\" class=\"button\" value=\"Full Case\" title=\"Full Case PDF Report\"\r\n");
      out.write("                                           onclick=\"popupWnd('bpopup', 'yes','pgPrintFullCase.jsp?RepNum=");
      out.print(strRepNum);
      out.write("',800,600)\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" />\r\n");
      out.write("\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdXML\" style=\"width: 80px;\" class=\"button\" value=\"XML\" title=\"XML Report\"\r\n");
      out.write("                                           onclick=\"popupWnd('bpopup', 'yes','pgGenerateXml.jsp?RepNum=");
      out.print(strRepNum);
      out.write("',500,300)\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td align=\"right\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdBack\" style=\"width: 60px;\" class=\"button\" value=\"Back\" title=\"Back to Case listing\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('pgNavigate.jsp?Cancellation=1&Target=pgCaselist.jsp');\"\r\n");
      out.write("                                           ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
        if (strShowBackBtn.equalsIgnoreCase("0")) {
            out.write(" disabled");
        }
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
