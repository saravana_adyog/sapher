package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.Reports_Listing;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.GenericException;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import java.text.SimpleDateFormat;

public final class pgPrintSumTabS_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "REP2";
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
    private String strProdCode;
    private String strProdDesc;
    private String strDtFrom;
    private String strDtTo;
    private int nStatus = -1;
    java.text.SimpleDateFormat sdf;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPrintCIOMS2.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; UserBlocked exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp: V=" + bView);

            if (!bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; No permission." + "[View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPrintCIOMS2.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; RecordNotFoundException.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }

        if (request.getParameter("txtProdCode_ID") != null) {
            strProdCode = request.getParameter("txtProdCode_ID");
        } else {
            strProdCode = "";
        }

        if (request.getParameter("txtProdCode") != null) {
            strProdDesc = request.getParameter("txtProdCode");
        } else {
            strProdDesc = "";
        }

        sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        if (request.getParameter("txtCaseDtFrom") != null) {
            strDtFrom = (String) request.getParameter("txtCaseDtFrom");
        } else {
            strDtFrom = "";
        }

        if (request.getParameter("txtCaseDtTo") != null) {
            strDtTo = (String) request.getParameter("txtCaseDtTo");
        } else {
            strDtTo = "";
        }

        String strSuspended = "";
        if (request.getParameter("cmbSuspendedCase") != null) {
            strSuspended = request.getParameter("cmbSuspendedCase");
        }

        if (strSuspended.equals("Valid")) {
            nStatus = 0;
        } else if (strSuspended.equals("Suspended")) {
            nStatus = 1;
        } else if (strSuspended.equals("All")) {
            nStatus = -1;
        }

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Report created.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            popupWnd('_blank', 'yes','");
out.write("Reports/" +
                Reports_Listing.printPdf(bl.sapher.Reports_Listing.REP_SUMTAB_SERIOUSNESS,
                (String) session.getAttribute("Company"),
                application.getRealPath("/Reports"),
                strProdCode, strProdDesc, strDtFrom, strDtTo, nStatus));
      out.write("',800,600);\r\n");
      out.write("            document.location.replace(\"pgRep.jsp?ReportType=SumTabS\");\r\n");
      out.write("        </script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
