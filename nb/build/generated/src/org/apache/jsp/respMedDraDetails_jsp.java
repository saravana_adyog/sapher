package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.MedDRABrowser;

public final class respMedDraDetails_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=windows-1252");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");


        //    Document   : respMedDraDetails
        //    Created on : Oct 27, 2009, 10:40:49 AM
        //    Author     : Anoop Varma

        int nLevel = Integer.parseInt(request.getParameter("Lvl"));
        String strCode = request.getParameter("Cod");
        String strResult = null;
        String[] strVals = null;

        switch (nLevel) {
            case 1:
                strResult = MedDRABrowser.getSocDetails((String) session.getAttribute("Company"), strCode);
                strVals = strResult.split("\\|");
                out.write("<table border=\"0\" width=\"100%\">");

                out.write("<tr>");
                out.write("<td width=\"30%\">SOC Num</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + strVals[0] + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">SOC</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + strVals[1] + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">Status</td>");
                if (strVals[2].equalsIgnoreCase("1")) {
                    out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"Active\"></td>");
                } else {
                    out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"Inactive\"></td>");
                }
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">WHO ART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[3].equalsIgnoreCase("null") ? "" : strVals[3]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HARTS Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[4].equalsIgnoreCase("null") ? "" : strVals[4]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">COSTART Sym</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[5].equalsIgnoreCase("null") ? "" : strVals[5]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[6].equalsIgnoreCase("null") ? "" : strVals[6]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 CM Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[7].equalsIgnoreCase("null") ? "" : strVals[7]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 10 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[8].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">JART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[9].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");

                out.write("</table>");
                break;
            case 2:
                strResult = MedDRABrowser.getHlgtDetails((String) session.getAttribute("Company"), strCode);
                strVals = strResult.split("\\|");
                out.write("<table border=\"0\" width=\"100%\">");

                out.write("<tr>");
                out.write("<td width=\"30%\">HLGT Num</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[0].equalsIgnoreCase("null") ? "" : strVals[0]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HLGT</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[1].equalsIgnoreCase("null") ? "" : strVals[1]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">WHO ART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[2].equalsIgnoreCase("null") ? "" : strVals[2]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HARTS Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[3].equalsIgnoreCase("null") ? "" : strVals[3]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">COSTART Sym</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[4].equalsIgnoreCase("null") ? "" : strVals[4]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[5].equalsIgnoreCase("null") ? "" : strVals[5]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 CM Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[6].equalsIgnoreCase("null") ? "" : strVals[6]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 10 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[7].equalsIgnoreCase("null") ? "" : strVals[7]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">JART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[8].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");

                out.write("</table>");
                break;
            case 3:
                strResult = MedDRABrowser.getHltDetails((String) session.getAttribute("Company"), strCode);
                strVals = strResult.split("\\|");
                out.write("<table border=\"0\" width=\"100%\">");

                out.write("<tr>");
                out.write("<td width=\"30%\">HLT Num</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[0].equalsIgnoreCase("null") ? "" : strVals[0]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HLT</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[1].equalsIgnoreCase("null") ? "" : strVals[1]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">WHO ART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[2].equalsIgnoreCase("null") ? "" : strVals[2]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HARTS Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[3].equalsIgnoreCase("null") ? "" : strVals[3]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">COSTART Sym</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[4].equalsIgnoreCase("null") ? "" : strVals[4]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[5].equalsIgnoreCase("null") ? "" : strVals[5]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 CM Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[6].equalsIgnoreCase("null") ? "" : strVals[6]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 10 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[7].equalsIgnoreCase("null") ? "" : strVals[7]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">JART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[8].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");

                out.write("</table>");
                break;
            case 4:
                strResult = MedDRABrowser.getPtDetails((String) session.getAttribute("Company"), strCode);
                strVals = strResult.split("\\|");
                out.write("<table border=\"0\" width=\"100%\">");

                out.write("<tr>");
                out.write("<td width=\"30%\">PT Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[0].equalsIgnoreCase("null") ? "" : strVals[0]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">Preferred Term</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[1].equalsIgnoreCase("null") ? "" : strVals[1]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">Status</td>");
                if (strVals[2].equalsIgnoreCase("1")) {
                    out.write("<td width=\"30%\"><input type=\"input\" readonly size=\"45\" value=\"Active\"></td>");
                } else {
                    out.write("<td width=\"30%\"><input type=\"input\" readonly size=\"45\" value=\"Inactive\"></td>");
                }
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">Primary SOC</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[3].equalsIgnoreCase("null") ? "" : strVals[3]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">WHO ART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[4].equalsIgnoreCase("null") ? "" : strVals[4]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HARTS Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[5].equalsIgnoreCase("null") ? "" : strVals[5]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">COSTART Sym</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[6].equalsIgnoreCase("null") ? "" : strVals[6]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[7].equalsIgnoreCase("null") ? "" : strVals[7]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 CM Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[8].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 10 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[9].equalsIgnoreCase("null") ? "" : strVals[9]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">JART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[10].equalsIgnoreCase("null") ? "" : strVals[10]) + "\"></td>");
                out.write("</tr>");

                out.write("</table>");
                break;
            case 5:
                strResult = MedDRABrowser.getLltDetails((String) session.getAttribute("Company"), strCode);
                strVals = strResult.split("\\|");
                out.write("<table border=\"0\" width=\"100%\">");
                out.write("<tr>");
                out.write("<td width=\"30%\">LLT Num</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[0].equalsIgnoreCase("null") ? "" : strVals[0]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">LLT</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[1].equalsIgnoreCase("null") ? "" : strVals[1]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">Status</td>");
                if (strVals[2].equalsIgnoreCase("1")) {
                    out.write("<td width=\"30%\"><input type=\"input\" readonly size=\"45\" value=\"Active\"></td>");
                } else {
                    out.write("<td width=\"30%\"><input type=\"input\" readonly size=\"45\" value=\"Inactive\"></td>");
                }
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">Preferred Term</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[3].equalsIgnoreCase("null") ? "" : strVals[3]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">WHO ART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[4].equalsIgnoreCase("null") ? "" : strVals[4]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">HARTS Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[5].equalsIgnoreCase("null") ? "" : strVals[5]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">COSTART Sym</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[6].equalsIgnoreCase("null") ? "" : strVals[6]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[7].equalsIgnoreCase("null") ? "" : strVals[7]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 9 CM Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[8].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">IDC 10 Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[9].equalsIgnoreCase("null") ? "" : strVals[9]) + "\"></td>");
                out.write("</tr>");
                out.write("<tr>");
                out.write("<td width=\"30%\">JART Code</td>");
                out.write("<td width=\"70%\"><input type=\"input\" readonly size=\"45\" value=\"" + (strVals[10].equalsIgnoreCase("null") ? "" : strVals[8]) + "\"></td>");
                out.write("</tr>");
                out.write("</table>");
                break;
        }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
