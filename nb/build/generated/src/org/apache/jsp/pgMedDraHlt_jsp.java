package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.MedDRABrowser;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgMedDraHlt_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CDL24"; // Body System code
    private String strSOCId = "";
    private String strSOC = "";
    private String strHLGT = "";
    private String strHLGTId = "";
    private String strHLT = "";
    private String strHLTId = "";
    private boolean bView;
    private ArrayList<String> al = null;
    private String strToBeHighlighted;
    private User currentUser = null;
    private int nRowCount = 100;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;

        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>MedDRA Browser - [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("        <script>var SAPHER_PARENT_NAME = \"\";</script>\r\n");
      out.write("        <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <!-- jQuery treeview -->\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"libjs/jquery/TreeView/jquery.treeview.css\" />\r\n");
      out.write("        <script src=\"libjs/jquery/TreeView/lib/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("        <script src=\"libjs/jquery/TreeView/lib/jquery.cookie.js\" type=\"text/javascript\"></script>\r\n");
      out.write("        <script src=\"libjs/jquery/TreeView/jquery.treeview.js\" type=\"text/javascript\"></script>\r\n");
      out.write("        <!-- jQuery treeview upto here -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/MedDra-Info-Dialog/dialogue_box.js\"></script>\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"libjs/MedDra-Info-Dialog/dialogue_box.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            $(function() {\r\n");
      out.write("                $(\"#tree\").treeview({\r\n");
      out.write("                    collapsed: false,\r\n");
      out.write("                    animated: \"medium\",\r\n");
      out.write("                    control:\"#sidetreecontrol\",\r\n");
      out.write("                    persist: \"location\"\r\n");
      out.write("                });\r\n");
      out.write("            })\r\n");
      out.write("\r\n");
      out.write("            function details(url) {\r\n");
      out.write("                var http = createRequestObject();\r\n");
      out.write("                http.open('GET',url,true);\r\n");
      out.write("                http.onreadystatechange = function() {if(http.readyState == 4){\r\n");
      out.write("                        var resp = http.responseText;\r\n");
      out.write("                        showDialog('Details',resp,'success');\r\n");
      out.write("                    }}\r\n");
      out.write("                http.send(null);\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        strToBeHighlighted = "";
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgMedDraHlt.jsp");

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp: V=" + bView);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; UserBlocked exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraHlt.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (request.getParameter("SOCID") != null) {
            strSOCId = request.getParameter("SOCID");
        }

        if (request.getParameter("SOC") != null) {
            strSOC = request.getParameter("SOC");
        }

        if (request.getParameter("HLGT") != null) {
            strHLGT = request.getParameter("HLGT");
        }

        if (request.getParameter("HLGTID") != null) {
            strHLGTId = request.getParameter("HLGTID");
        }

        if (request.getParameter("HiLit") != null) {
            strToBeHighlighted = request.getParameter("HiLit");
        }

        al = MedDRABrowser.list(1, (String) session.getAttribute("Company"),
                MedDRABrowser.MEDDRA_ITEMTYPE_HLT, strHLGTId, "");
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">MedDRA Browser</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 1px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td colspan=\"10\">\r\n");
      out.write("                                <div id=\"sidetree\">\r\n");
      out.write("                                    <div class=\"treeheader\"><b>Selected Term Hierarchy</b>\r\n");
      out.write("                                        <span style=\"float:right\"><a href=\"pgNavigate.jsp?Cancellation=1&Target=pgMedDraSearch.jsp\">Search&nbsp;</a></span>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <ul id=\"tree\">\r\n");
      out.write("                                        <ul>\r\n");
      out.write("                                            <li>\r\n");
      out.write("                                                <img src=\"images/icons/16x16-SOC.gif\"/>\r\n");
      out.write("                                                <a href=\"pgMedDraSoc.jsp?HiLit=");
      out.print(strSOCId);
      out.write("\">\r\n");
      out.write("                                                    ");
out.write(strSOC + " [" + strSOCId + "]");
      out.write("\r\n");
      out.write("                                                </a>\r\n");
      out.write("                                                <ul>\r\n");
      out.write("                                                    <li>\r\n");
      out.write("                                                        <img src=\"images/icons/16x16-HLGT.gif\"/>\r\n");
      out.write("                                                        <a href=\"pgMedDraHlgt.jsp?HiLit=");
      out.print(strHLGTId);
      out.write("&SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("\">\r\n");
      out.write("                                                            ");
out.write(strHLGT + " [" + strHLGTId + "]");
      out.write("\r\n");
      out.write("                                                        </a>\r\n");
      out.write("                                                    </li>\r\n");
      out.write("                                                </ul>\r\n");
      out.write("                                            </li>\r\n");
      out.write("                                        </ul>\r\n");
      out.write("                                    </ul>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");
 if (al.size() != 0) {
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                        <tr class=\"grid_header\">\r\n");
      out.write("                            <td width=\"16\"></td>\r\n");
      out.write("                            <td width=\"100\">Code</td>\r\n");
      out.write("                            <td>Description</td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     String[] strCols = null;
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         strCols = al.get(i).split("\\|");
                        
      out.write("\r\n");
      out.write("                        <tr class=\"\r\n");
      out.write("                            ");

                            if (strToBeHighlighted.equalsIgnoreCase(strCols[0])) {
                                out.write("hilit_row");
                            } else if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            
      out.write("\r\n");
      out.write("                            \">\r\n");
      out.write("                            ");
 if (bView) {
      out.write("\r\n");
      out.write("                            <td width=\"16\" align=\"center\" >\r\n");
      out.write("                                <a href=\"#\">\r\n");
      out.write("                                    <img src=\"images/icons/16x16-HLT.gif\" title=\"View the entry with ID = ");
      out.print(strCols[0]);
      out.write("\" border='0'\r\n");
      out.write("                                         onclick=\"details('respMedDraDetails.jsp?Lvl=3&Cod=");
      out.print(strCols[0]);
      out.write("')\" />\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            ");
 }
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <td width=\"100\" align='center'>\r\n");
      out.write("                                <a href=\"pgMedDraPt.jsp?SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("&HLGTID=");
      out.print(strHLGTId);
      out.write("&HLGT=");
      out.print(strHLGT);
      out.write("&HLTID=");
      out.print(strCols[0]);
      out.write("&HLT=");
      out.print(strCols[1]);
      out.write("\">\r\n");
      out.write("                                    ");
      out.print(strCols[0]);
      out.write("\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td align='left'>\r\n");
      out.write("                                <a href=\"pgMedDraPt.jsp?SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("&HLGTID=");
      out.print(strHLGTId);
      out.write("&HLGT=");
      out.print(strHLGT);
      out.write("&HLTID=");
      out.print(strCols[0]);
      out.write("&HLT=");
      out.print(strCols[1]);
      out.write("\">\r\n");
      out.write("                                    ");
      out.print(strCols[1]);
      out.write("\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("                <tr>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("            <!-- Pagination Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"pgMedDraHlt.jsp?SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("&HLGTID=");
      out.print(strHLGTId);
      out.write("&HLGT=");
      out.print(strHLGT);
      out.write("&CurPage=1\">\r\n");
      out.write("                                <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                                <a href=\"pgMedDraHlt.jsp?SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("&HLGTID=");
      out.print(strHLGTId);
      out.write("&HLGT=");
      out.print(strHLGT);
      out.write("&CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                                <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\"></a>\r\n");
      out.write("                                ");
 } else {
      out.write("\r\n");
      out.write("                                <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                                ");
 }
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                            <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                                <a href=\"pgMedDraHlt.jsp?SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("&HLGTID=");
      out.print(strHLGTId);
      out.write("&HLGT=");
      out.print(strHLGT);
      out.write("&CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                                <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\"></a>\r\n");
      out.write("                                ");
 } else {
      out.write("\r\n");
      out.write("                                <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                                ");
 }
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"pgMedDraHlt.jsp?SOCID=");
      out.print(strSOCId);
      out.write("&SOC=");
      out.print(strSOC);
      out.write("&HLGTID=");
      out.print(strHLGTId);
      out.write("&HLGT=");
      out.print(strHLGT);
      out.write("&CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                                <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\"></a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"100%\"></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Pagination Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("            ");
}
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
