package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.GenConf;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.UserCountException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.GenericException;
import bl.sapher.general.DBConnectionException;

public final class pgShortSavUser_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "USM1";
    private String strPassword = "";
    private int nPwdMistakeCount = -1;
    private int nPwdChgFlag = -1;
    private int nLocked = -1;
    private int next = -1;
    private User currentUser = null;
    private GenConf gC = null;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : USM3\r\n");
      out.write("Date    : 13-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgShortSavUser.jsp");
        strPassword = "";
        nPwdMistakeCount = -1;
        nLocked = -1;
        next = -1;
        try {
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"));
            gC = new GenConf((String) session.getAttribute("Company"));
        } catch (UserCountException e) {
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=7\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (request.getParameter("txtPassword") != null) {
            strPassword = request.getParameter("txtPassword");
            nPwdChgFlag = 0;
        }

        if (request.getParameter("PwdMistake") != null) {
            if (Integer.parseInt(request.getParameter("PwdMistake")) == 1) {
                nPwdMistakeCount = currentUser.getPwd_Mistake_Count() + 1;
                if (nPwdMistakeCount >= gC.getSysLoginMistakeCount()) {
                    nLocked = 1;
                }
            } else {
                nPwdMistakeCount = 0;
            }
        }

        if (request.getParameter("Next") != null) {
            next = Integer.parseInt(request.getParameter("Next"));
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser.saveUser((String) session.getAttribute("Company"), "U", currentUser.getUsername(), "", strPassword, nPwdChgFlag, nPwdMistakeCount, nLocked);
            if (!strPassword.equals("")) {
                session.setAttribute("CurrentUserPW", strPassword);
            }
            if (next == 1) { // pgLogin.jsp

      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");
        } else if (next == 0) { //content.jsp
            pageContext.forward("content.jsp?Status=2");
        } else if (next == 2) {//pgSapher
            //Register User
            
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp: Registering User/session");
            String sessId = currentUser.registerUser((String) session.getAttribute("Company"), request.getRemoteAddr());
            session.setAttribute("SapherSessionId", sessId);
            session.setAttribute("LogFileName", sessId);
            
            pageContext.forward("pgSapher.jsp");
            
        }
    } catch (TimeoutException toe) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp: Timeout Exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

        return;
    } catch (Exception ex) {
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgShortSavUser.jsp: Exception" +"1"+ ex.toString());
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

        }
        
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
