package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import bl.sapher.DoseRegimen;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.Interval;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;

public final class pgDRgmn_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    DoseRegimen dRgmn = null;
    private final String FORM_ID = "CDL9";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CDL9\r\n");
      out.write("Date    : 11-12-2007.\r\n");
      out.write("Author  : Arun P. Jose, Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Dose Regimen - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <script>\r\n");
      out.write("            function validate() {\r\n");
      out.write("                if (document.FrmDRgmn.txtDRgmnCode.value == \"\"){\r\n");
      out.write("                    alert( \"Regimen Code cannot be empty.\");\r\n");
      out.write("                    document.FrmDRgmn.txtDRgmnCode.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if (trim(document.FrmDRgmn.txtDRgmnDesc.value) == \"\"){\r\n");
      out.write("                    alert( \"Dose Regimen cannot be empty.\");\r\n");
      out.write("                    document.FrmDRgmn.txtDRgmnDesc.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if (document.FrmDRgmn.txtNoOfDosage.value == \"\"){\r\n");
      out.write("                    alert( \"Number of dosage cannot be empty.\");\r\n");
      out.write("                    document.FrmDRgmn.txtNoOfDosage.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsAlphaVar(document.FrmDRgmn.txtDRgmnCode.value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in dose regimen code.');\r\n");
      out.write("                    document.getElementById('txtDRgmnCode').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsAlphaSpaceVar(document.FrmDRgmn.txtDRgmnDesc.value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) dose regimen.');\r\n");
      out.write("                    document.getElementById('txtDRgmnDesc').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsNumericOnlyVar(document.FrmDRgmn.txtNoOfDosage.value)) {\r\n");
      out.write("                    window.alert('Mumber of dose should be numeric.');\r\n");
      out.write("                    document.getElementById('txtDRgmnCode').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        dRgmn = new DoseRegimen((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDRgmn.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("DRgmnCode") != null) {
                    dRgmn = new DoseRegimen((String) session.getAttribute("Company"), request.getParameter("DRgmnCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                dRgmn = new DoseRegimen();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDRgmn.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='400' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Code List</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmDRgmn\" name=\"FrmDRgmn\" method=\"POST\" action=\"pgSavDRgmn.jsp\"\r\n");
      out.write("                      onsubmit=\"return validate();\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">");
      out.print((saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit ")));
      out.write("Dose Regimen</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtDRgmnCode\">Rgmn Code</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtDRgmnCode\" title=\"Dose Regimen Code\"\r\n");
      out.write("                                                             id=\"txtDRgmnCode\" size=\"3\" maxlength=\"2\"\r\n");
      out.write("                                                             value=\"");
      out.print(dRgmn.getDregimen_Code());
      out.write("\"\r\n");
      out.write("                                                             ");

        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtDRgmnDesc\">Dose Regimen</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtDRgmnDesc\" id=\"txtDRgmnDesc\" size=\"50\" title=\"Dose Regimen\"\r\n");
      out.write("                                                             maxlength=\"50\" value=\"");
      out.print(dRgmn.getDregimen_Desc());
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <!-- New fields STARTS -->\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtNoOfDosage\">No. of Dosage</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtNoOfDosage\" id=\"txtNoOfDosage\" size=\"4\" title=\"No. of Dosage\"\r\n");
      out.write("                                                             onkeydown=\"return checkIsNumeric(event);\"\r\n");
      out.write("                                                             maxlength=\"3\" value=\"");


        if (dRgmn.getNoOfDosage().equals("-1") || dRgmn.getNoOfDosage().equals("0")) {
            out.write("");
        } else {
            out.write("" + dRgmn.getNoOfDosage());
        }

                                                             
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtDosageInterval\">Dosage Interval</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("\r\n");
      out.write("                                    <select name=\"cmboDosageInterval\" id=\"cmboDosageInterval\">\r\n");
      out.write("                                        ");

        java.util.ArrayList<Interval> alIntervals = Interval.listDosageInterval((String) session.getAttribute("Company"), "", "", 1);
        Interval i = null;
        for (int count = 0; count < alIntervals.size(); count++) {
            i = (alIntervals.get(count));
                                        
      out.write("\r\n");
      out.write("                                        <option value=\"");
      out.print(i.getIntervalCode());
      out.write("\"\r\n");
      out.write("                                                ");
if (dRgmn.getDosageInterval().equals("" + i.getIntervalCode())) {
                                                out.write(" SELECTED ");
                                            }
      out.write('>');
      out.print(i.getInterval());
      out.write("</option>\r\n");
      out.write("                                        ");


        }
        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                        
      out.write("\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <!-- New fields ENDS -->\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkValid\">Active</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"checkbox\" name=\"chkValid\" id=\"chkValid\" title=\"Status\" class=\"chkbox\"\r\n");
      out.write("                                                             ");

        if (dRgmn.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSave\" class=\"");
      out.print((saveFlag.equals("V") ? "button disabled" : "button"));
      out.write("\" value=\"Save\" style=\"width: 60px;\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"SaveFlag\" name=\"SaveFlag\" value=\"");
      out.print(saveFlag);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"");
      out.print(strParent);
      out.write("\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('");
      out.print(!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgDRgmnlist.jsp" : "content.jsp");
      out.write("');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
