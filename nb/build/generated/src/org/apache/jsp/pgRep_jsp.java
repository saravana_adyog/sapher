package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.CaseDetails;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;

public final class pgRep_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CSD1";
    //    private int intRepNum = -1;
    //    private String strRepNum = "";
    /**
     * This variable holds the type of report [CIOMS1I/ICH/Sum Tab-S/Sum Tab-RT]
     * obtained from the query string.
     */
    private String RepType = "";

    //    private String strRepTypeCode = "";
    //    private String strRepType = "";
    //    private String strTrialNum = "";
    //    private String strGenericNm = "";
    //    private String strBrandNm = "";
    //    private String strDtFrom = "";
    //    private String strDtTo = "";
    //    private int nCaseSuspended = 0;
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    //    private ArrayList al = null;
    private User currentUser = null;
    //    private CaseDetails cDet = null;
    //    private int nRowCount = 10;
    //    private int nPageCount;
    //    private int nCurPage;
    private Inventory inv = null;
    //    private String strMessageInfo = "";

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD1.1\r\n");
      out.write("Date    : 12-04-2008.\r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Reports - [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            var SAPHER_PARENT_NAME = \"pgRep\";\r\n");
      out.write("        </script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body onUnload=\"nProdPopupCount=0;\">\r\n");
      out.write("\r\n");
      out.write("        <script>var nProdPopupCount = 0;</script>\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgRep.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                    return;
                }
                inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
                bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
                bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
                bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
                bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
            } catch (TimeoutException toe) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDR.jsp?Parent=0&SaveFlag=I");
        }

        if (!bAdd && !bEdit && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgRep.jsp; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("ReportType") != null) {
            RepType = request.getParameter("ReportType");
        }

        
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Report Parameters</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <FORM name=\"FrmRep\" METHOD=\"POST\" ACTION=\"");

        if (RepType.equals("CIOMS2")) {
            out.write("pgPrintCIOMS2.jsp");
        } else if (RepType.equals("ICH")) {
            out.write("pgPrintICH.jsp");
        } else if (RepType.equals("TabularS")) {
            out.write("pgPrintSumTabS.jsp");
        } else if (RepType.equals("TabularRT")) {
            out.write("pgPrintSumTabRT.jsp");
        }
                      
      out.write("\" >\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">");

        if (RepType.equals("CIOMS2")) {
            out.write("CIOMS II");
        } else if (RepType.equals("ICH")) {
            out.write("ICH");
        } else if (RepType.equals("TabularS")) {
            out.write("Summary Tabulation - Seriousness");
        } else if (RepType.equals("TabularRT")) {
            out.write("Summary Tabulation - Report Type");
        }
                        
      out.write("</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"height: 10px;\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode_ID\">Product</label></td>\r\n");
      out.write("                        <td class=\"field\" width=\"1000\">\r\n");
      out.write("\r\n");
      out.write("                            <input type=\"text\" id=\"txtProdCode\" name=\"txtProdCode\" size=\"57\" onKeyUp=\"ajax_showOptions('txtProdCode_ID',this,'Product',event)\">\r\n");
      out.write("                            <input type=\"text\" readonly size=\"13\" id=\"txtProdCode_ID\" name=\"txtProdCode_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtCaseDtFrom\">Date Range</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtCaseDtFrom\" id=\"txtCaseDtFrom\" size=\"12\" maxlength=\"12\"\r\n");
      out.write("                                               title=\"From Date\" readonly clear=2 onKeyDown=\"return ClearInput(event, 2);\">\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select From Date\"></a>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td class=\"fLabel\" style=\"padding-right: 3px; padding-left: 3px;\">TO</td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtCaseDtTo\" id=\"txtCaseDtTo\" size=\"13\" maxlength=\"12\"\r\n");
      out.write("                                               title=\"To Date\" readonly clear=3 onKeyDown=\"return ClearInput(event, 3);\">\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select To Date\"></a>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <script type=\"text/javascript\">\r\n");
      out.write("                        Calendar.setup({\r\n");
      out.write("                            inputField : \"txtCaseDtFrom\", //*\r\n");
      out.write("                            ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                            showsTime : true,\r\n");
      out.write("                            button : \"cal_trigger1\", //*\r\n");
      out.write("                            step : 1\r\n");
      out.write("                        });\r\n");
      out.write("                    </script>\r\n");
      out.write("                    <script type=\"text/javascript\">\r\n");
      out.write("                        Calendar.setup({\r\n");
      out.write("                            inputField : \"txtCaseDtTo\", //*\r\n");
      out.write("                            ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                            showsTime : true,\r\n");
      out.write("                            button : \"cal_trigger2\", //*\r\n");
      out.write("                            step : 1\r\n");
      out.write("                        });\r\n");
      out.write("                    </script>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"cmbSuspendedCase\">Validity</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                            <span style=\"float: left\">\r\n");
      out.write("                                <SELECT id=\"cmbSuspendedCase\" title=\"Select Validity\" name=\"cmbSuspendedCase\" class=\"comboclass\" style=\"width: 100px;\">\r\n");
      out.write("                                    <option selected>All</option>\r\n");
      out.write("                                    <option>Valid</option>\r\n");
      out.write("                                    <option>Suspended</option>\r\n");
      out.write("                                </SELECT>\r\n");
      out.write("                                <input type=\"submit\" name=\"cmdGenerate\" title=\"Start generating PDF report\" class=\"button\" value=\"Generate\" style=\"width: 87px;\"/>\r\n");
      out.write("                            </span>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("\r\n");
      out.write("        <!-- Form Body Ends -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 1px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 10px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
