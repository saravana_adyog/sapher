package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import bl.sapher.User;
import bl.sapher.Role;
import bl.sapher.Inventory;
import bl.sapher.Permission;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;

public final class pgPermission_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "USM4";
    private String strRoleName = "";
    private String strInvId = "";
    private boolean bEdit;
    private User currentUser = null;
    private Inventory inv = null;
    private Permission perm = null;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : USM4\r\n");
      out.write("Date    : 11-12-2007.\r\n");
      out.write("Author  : Arun P. Jose, Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Permission - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <script>\r\n");
      out.write("            function viewClick(){\r\n");
      out.write("                if (!document.FrmPerm.chkView.checked){\r\n");
      out.write("                    if (document.FrmPerm.chkEdit.checked)\r\n");
      out.write("                        return false;\r\n");
      out.write("                    if (document.FrmPerm.chkDelete.checked)\r\n");
      out.write("                        return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("            function changeView(){\r\n");
      out.write("                document.FrmPerm.chkView.checked = true;\r\n");
      out.write("            }\r\n");
      out.write("            function noChangeEdit(){\r\n");
      out.write("                document.FrmPerm.chkEdit.checked = true;\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        perm = new Permission((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPermission.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (request.getParameter("InvId") != null) {
            strInvId = request.getParameter("InvId");
        }

        if (request.getParameter("Role") != null) {
            strRoleName = request.getParameter("Role");
        }

        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp: bEdit=" + bEdit);
            if (!bEdit) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; No permission." + "[Edit: " + bEdit + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }

            perm = new Permission((String) session.getAttribute("Company"), strInvId, strRoleName);

        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermission.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='400' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">User Administration</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmPerm\" name=\"FrmPerm\" method=\"POST\" action=\"pgUpdPermission.jsp\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Edit Permissions</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtRoleName\">Role Name</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtRoleName\" title=\"Role Name\"\r\n");
      out.write("                                                             id=\"txtRoleName\" size=\"40\" maxlength=\"30\"\r\n");
      out.write("                                                             value=\"");
      out.print(strRoleName);
      out.write("\" readonly>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtInvId\">Inventory</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtInvId\" id=\"txtInvId\" size=\"7\" title=\"Inventory\"\r\n");
      out.write("                                                         maxlength=\"10\" value=\"");
      out.print(perm.getInv().getInv_Id());
      out.write("\" readonly>\r\n");
      out.write("                                <input type=\"text\" name=\"txtInvDesc\" id=\"txtInvDesc\" size=\"30\"\r\n");
      out.write("                                       maxlength=\"50\" value=\"");
      out.print(perm.getInv().getInv_Desc());
      out.write("\" readonly>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkView\">View</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"checkbox\" name=\"chkView\" id=\"chkView\" class=\"chkbox\" title=\"View\" onclick=\"return viewClick();\"\r\n");
      out.write("                                                             ");

        if (perm.getPView() == 1) {
            out.write("CHECKED ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkAdd\">Add</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"checkbox\" name=\"chkAdd\" id=\"chkAdd\" class=\"chkbox\"\r\n");
      out.write("                                                             ");

        if (perm.getPAdd() == 1) {
            out.write("CHECKED ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkEdit\">Edit</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"checkbox\" name=\"chkEdit\" id=\"chkEdit\" class=\"chkbox\" onclick=\"changeView();\r\n");
      out.write("                                                             ");
if ((currentUser.getRole().getRole_Name().equals(strRoleName)) && perm.getInv().getInv_Id().equals("USM4")) {
            out.write("return noChangeEdit();");
        }
      out.write("\"\r\n");
      out.write("                                                             ");

        if (perm.getPEdit() == 1) {
            out.write("CHECKED ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkDelete\">Delete</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"checkbox\" name=\"chkDelete\" id=\"chkDelete\" class=\"chkbox\" onclick=\"return changeView();\"\r\n");
      out.write("                                                             ");

        if (perm.getPDelete() == 1) {
            out.write("CHECKED ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSave\" class=\"button\" value=\"Save\" style=\"width: 60px;\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('pgNavigate.jsp?Target=pgPermissionlist.jsp');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
