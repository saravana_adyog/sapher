package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.text.SimpleDateFormat;
import bl.sapher.CaseDetails;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgCaselistDup_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CSD1";
    private int intRepNum = -1;
    private String strRepNum = "";
    private String strPTCode = "";
    private String strPTDesc = "";
    private String strTrialNum = "";
    private String strProdCode = "";
    private String strGenericNm = "";
    private String strBrandNm = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private int nCaseSuspended = 0;
    private boolean bAdd;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
    private String strClinRep = "";
    private String strCountryCode = "";
//private String strProdCode = "";
    private String strInitials = "";
    private String strOutcomeCode = "";
    private int nAgeRep = -1;
    private String strSexCode = "";
    private String strTrial = "";
    private String strPatNum = "";
//private String strPTCode = "";
    private String strAggrPT = "N";
    private String strLLTCode = "";
    private String strAggrLLT = "N";
    private String strBodySysNum = "";
    private String strClasfCode = "";
    private String strPhyAssCode = "";
    private String strCoAssCode = "";
    private String strVerbatim = "";
    private Date dtRecv = null;
    private SimpleDateFormat dFormat = null;

    public void jspDestroy() {
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD1\r\n");
      out.write("Date    : 19-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>Case Listing - [Sapher]</title>\r\n");
      out.write("    <script>var SAPHER_PARENT_NAME = \"pgCaselistDup\";</script>\r\n");
      out.write("    <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("    <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("    <!--Calendar Library Includes.. S => -->\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("    <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("    <!-- import the calendar script -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("    <!-- import the language module -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("    <!-- helper script that uses the calendar -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("    <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaselistDup.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistDup.jsp; Wrong username/password");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("</script>\r\n");

        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
    bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistDup.jsp: A/D/V=" + bAdd + "/" + bDelete + "/" + bView);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistDup.jsp; Timeout exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistDup.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistDup.jsp; User already logged in.");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("</script>\r\n");

            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDetails.jsp?Parent=0&SaveFlag=I");
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselistDup.jsp; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }

        if (request.getParameter("txtRecvDate") != null) {
            if (!request.getParameter("txtRecvDate").equals("")) {
                dtRecv = new Date(dFormat.parse(request.getParameter("txtRecvDate")).getTime());
            }
        }

        if (request.getParameter("txtClinRep") != null) {
            strClinRep = request.getParameter("txtClinRep");
        }

        if (request.getParameter("txtCountryCode_ID") != null) {
            strCountryCode = request.getParameter("txtCountryCode_ID").trim();
        }

        if (request.getParameter("txtProdCode_ID") != null) {
            strProdCode = request.getParameter("txtProdCode_ID").trim();
        }

        if (request.getParameter("txtInitials") != null) {
            strInitials = request.getParameter("txtInitials");
        }

        if (request.getParameter("txtAgeRep") != null) {
            if (request.getParameter("txtAgeRep") != "") {
                nAgeRep = Integer.parseInt(request.getParameter("txtAgeRep"));
            }
        }

        if (request.getParameter("txtSexCode_ID") != null) {
            strSexCode = request.getParameter("txtSexCode_ID").trim();
        }

        if (request.getParameter("txtTrial") != null) {
            strTrial = request.getParameter("txtTrial");
        }

        if (request.getParameter("txtPatNum") != null) {
            strPatNum = request.getParameter("txtPatNum");
        }

        if (request.getParameter("txtPTCode_ID") != null) {
            strPTCode = request.getParameter("txtPTCode_ID").trim();
        }

        if (request.getParameter("chkAggrPT") != null) {
            strAggrPT = request.getParameter("chkAggrPT");
        }

        if (request.getParameter("txtLLTCode_ID") != null) {
            strLLTCode = request.getParameter("txtLLTCode_ID").trim();
        }

        if (request.getParameter("chkAggrLLT") != null) {
            strAggrLLT = request.getParameter("chkAggrLLT");
        }

        if (request.getParameter("txtBodySysNum_ID") != null) {
            strBodySysNum = request.getParameter("txtBodySysNum_ID").trim();
        }

        if (request.getParameter("txtClasfCode_ID") != null) {
            strClasfCode = request.getParameter("txtClasfCode_ID").trim();
        }

        if (request.getParameter("txtOutcomeCode_ID") != null) {
            strOutcomeCode = request.getParameter("txtOutcomeCode_ID").trim();
        }

        if (request.getParameter("txtPhyAssCode_ID") != null) {
            strPhyAssCode = request.getParameter("txtPhyAssCode_ID").trim();
        }

        if (request.getParameter("txtCoAssCode_ID") != null) {
            strCoAssCode = request.getParameter("txtCoAssCode_ID").trim();
        }

        if (request.getParameter("txtNarrative") != null) {
            strVerbatim = request.getParameter("txtNarrative");
        }

        if (request.getParameter("txtRepNum") == null) {
            if (session.getAttribute("txtRepNum") == null) {
                strRepNum = "";
            } else {
                strRepNum = (String) session.getAttribute("txtRepNum");
            }
        } else {
            strRepNum = request.getParameter("txtRepNum");
        }
        session.setAttribute("txtRepNum", strRepNum);

        if (strRepNum.equals("")) {
            intRepNum = -1;
        } else {
            intRepNum = Integer.parseInt(strRepNum);
        }

        al = CaseDetails.listCaseFromView(
                (String) session.getAttribute("Company"), -1,
                Constants.SEARCH_ANY_VAL, Constants.SEARCH_ANY_VAL,
                Constants.SEARCH_ANY_VAL, Constants.SEARCH_ANY_VAL,
                Constants.SEARCH_ANY_VAL, -1);

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }


      out.write("\r\n");
      out.write("\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("<table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("<tr>\r\n");
      out.write("    <td id=\"formtitle\">Case Listing</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <form name=\"FrmCaseList\" METHOD=\"POST\" ACTION=\"pgCaselistDup.jsp\" >\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 10px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("        <td>\r\n");
      out.write("            <td colspan=\"2\">\r\n");
      out.write("                <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                       onclick=\"location.replace('pgNavigate.jsp?Target=pgCaselistDup.jsp');\"/>\r\n");
      out.write("            </td>\r\n");
      out.write("        </td>\r\n");
      out.write("    </form>\r\n");
      out.write("    <td>\r\n");
      out.write("        <form METHOD=\"POST\" ACTION=\"pgCaseDetails.jsp?SaveFlag=I\">\r\n");
      out.write("            <span style=\"float: right\">\r\n");
      out.write("                <input type=\"hidden\" name=\"SaveFlag\" value=\"I\">\r\n");
      out.write("                <input type=\"hidden\" name=\"Parent\" value=\"1\">\r\n");
      out.write("                <input type=\"submit\" name=\"cmdNew\"  title=\"Adds a new entry\" class=\"");
      out.print((bAdd ? "button" : "button disabled"));
      out.write("\"\r\n");
      out.write("                       value=\"New Case\" style=\"width: 85px;\" ");
      out.print((bAdd ? "" : " DISABLED"));
      out.write(">\r\n");
      out.write("            </span>\r\n");
      out.write("        </form>\r\n");
      out.write("    </td>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Ends -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 1px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Grid View Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                    ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
 if (al.size() != 0) {

      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("            <tr class=\"grid_header\">\r\n");
      out.write("                <td width=\"16\"></td>\r\n");
      out.write("                <td width=\"200\">Rep Num</td>\r\n");
      out.write("                <td width=\"100\">Source/Trail Num</td>\r\n");
      out.write("                <td width=\"100\">Rec. Date</td>\r\n");
      out.write("                <td width=\"50\">Status</td>\r\n");
      out.write("                <td></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");

     CaseDetails cd = null;
     SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
     String strStatusFlag = "";

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         cd = ((CaseDetails) al.get(i));
         strStatusFlag = "" + cd.getIs_Suspended();
            
      out.write("\r\n");
      out.write("            <tr class=\"\r\n");
      out.write("                ");

                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                
      out.write("\r\n");
      out.write("                \">\r\n");
      out.write("                <td width=\"16\" align=\"center\" >\r\n");
      out.write("                    <a href=\".jsp?RepNum=");
      out.print(cd.getRep_Num());
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/view.gif\" title=\"View details of the entry with ID = ");
      out.print(cd.getRep_Num());
      out.write("\" border='0' alt=\"View\">\r\n");
      out.write("                    </a>\r\n");
      out.write("                </td>\r\n");
      out.write("\r\n");
      out.write("                <td width=\"200\" align='left'>\r\n");
      out.write("                    ");

                if (strStatusFlag.equals("1")) {
                    
      out.write("\r\n");
      out.write("                    <font color=\"red\">");
      out.print(cd.getRep_Num());
      out.write("</font>\r\n");
      out.write("                    ");

                } else {
                    out.write("" + cd.getRep_Num());
                }
                    
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"100\" align='left'>\r\n");
      out.write("                    ");

                if (cd.getTrialnum() != null) {
                    if (strStatusFlag.equals("1")) {
                    
      out.write("\r\n");
      out.write("                    <font color=\"red\">");
      out.print(cd.getTrialnum());
      out.write("</font>\r\n");
      out.write("                    ");

                    } else {
                        out.write("" + cd.getTrialnum());
                    }
                }
                    
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"100\" align='left'>\r\n");
      out.write("                    ");

                if (cd.getDate_Recv() != null) {
                    if (strStatusFlag.equals("1")) {
                    
      out.write("\r\n");
      out.write("                    <font color=\"red\">");
      out.print(sdf.format(cd.getDate_Recv()));
      out.write("</font>\r\n");
      out.write("                    ");

                    } else {
                        out.write(sdf.format(cd.getDate_Recv()));
                    }
                }
                    
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"50\">\r\n");
      out.write("                    ");
      out.print((strStatusFlag.equals("1") ? "<font color=\"RED\">Suspended</font>" : "Active"));
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");
 }
      out.write("\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("    <tr>\r\n");
      out.write("    </tr>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("<!-- Pagination Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <a href=\"pgCaselistDup.jsp?CurPage=1\">\r\n");
      out.write("                    <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\" alt=\"First\"></a>\r\n");
      out.write("                </td>\r\n");
      out.write("                <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                    <A href=\"pgCaselistDup.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                    <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\" alt=\"Prev\"></A>\r\n");
      out.write("                    ");
 } else {
      out.write("\r\n");
      out.write("                    <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\" alt=\"Prev\">\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                    <A href=\"pgCaselistDup.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                    <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\" alt=\"Next\"></A>\r\n");
      out.write("                    ");
 } else {
      out.write("\r\n");
      out.write("                    <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\" alt=\"Next\">\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <a href=\"pgCaselistDup.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                    <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\" alt=\"Last\"></a>\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"100%\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Pagination Ends -->\r\n");
}
      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 10px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
