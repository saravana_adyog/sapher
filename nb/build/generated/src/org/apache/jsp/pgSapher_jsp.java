package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class pgSapher_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : NAV1\r\n");
      out.write("Author  : Ratheesh\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/jsp; charset=UTF-8\" />\r\n");
      out.write("        <title>Sapher</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <link rel=\"shortcut icon\" href=\"../images/icons/sapher.ico\" type=\"../image/vnd.microsoft.icon\" />\r\n");
      out.write("        <link rel=\"icon\" href=\"../images/icons/sapher.ico\" type=\"../image/vnd.microsoft.icon\" />\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <frameset rows=\"85,*,33\" cols=\"*\" frameborder=\"NO\" border=\"0\" framespacing=\"0\">\r\n");
      out.write("        <frame src=\"support/header.jsp\" name=\"headerFrame\" scrolling=\"NO\" noresize />\r\n");
      out.write("        <frameset cols=\"205,*\" frameborder=\"NO\" border=\"0\" framespacing=\"0\" id=\"framesetMenu\">\r\n");
      out.write("            <frame src=\"support/leftNav.jsp\" name=\"leftNavFrame\" scrolling=\"YES\" noresize  id=\"leftNavFrame\"/>\r\n");
      out.write("            <frame src=\"content.jsp\" name=\"contentFrame\"/>\r\n");
      out.write("        </frameset>\r\n");
      out.write("        <frame src=\"support/footer.jsp\" name=\"footerFrame\" scrolling=\"NO\" noresize />\r\n");
      out.write("    </frameset>\r\n");
      out.write("\r\n");
      out.write("    <noframes>\r\n");
      out.write("        <body bgcolor=\"#FFFFFF\">\r\n");
      out.write("            <CENTER>Frames not available</CENTER>\r\n");
      out.write("        </body>\r\n");
      out.write("    </noframes>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
