package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.Permission;
import bl.sapher.Inventory;
import bl.sapher.User;
import bl.sapher.Role;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgPermissionlist_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "USM4";
    private String strRoleName = "";
    private String strInvType = "";
    private String strInvId = "";
    private String strInvName = "";
    private boolean bEdit;
    private ArrayList al = null;
    private User currentUser = null;
    private Role role = null;
    private Permission aPermission = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : USM4\r\n");
      out.write("Date    : 13-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Permissions - [Sapher]</title>\r\n");
      out.write("        <script>var SAPHER_PARENT_NAME = \"pgPermissionlist\";</script>\r\n");
      out.write("        <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        aPermission = new Permission((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgPermissionlist.jsp");
        strMessageInfo = "";
        if (request.getParameter("SavStatus") != null) {
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed!</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp: E=" + bEdit);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgPermissionlist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (!bEdit) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAEDiaglist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [Edit=" + bEdit + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }
        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtPermRoleName") == null) {
            if (session.getAttribute("txtPermRoleName") == null) {
                strRoleName = "";
            } else {
                strRoleName = (String) session.getAttribute("txtPermRoleName");
            }
        } else {
            strRoleName = request.getParameter("txtPermRoleName");
        }
        session.setAttribute("txtPermRoleName", strRoleName);

        if (strRoleName.equals("")) {
            role = currentUser.getRole();
            strRoleName = currentUser.getRole().getRole_Name();
        } else {
            role = new Role((String) session.getAttribute("Company"), strRoleName);
        }

        if (request.getParameter("txtPermInvId_ID") == null) {
            if (session.getAttribute("txtPermInvId_ID") == null) {
                strInvId = "";
            } else {
                strInvId = (String) session.getAttribute("txtPermInvId_ID");
            }
        } else {
            strInvId = request.getParameter("txtPermInvId_ID");
        }
        session.setAttribute("txtPermInvId_ID", strInvId);

        if (request.getParameter("txtPermInvName") == null) {
            if (session.getAttribute("txtPermInvName") == null) {
                strInvName = "";
            } else {
                strInvName = (String) session.getAttribute("txtPermInvName");
            }
        } else {
            strInvName = request.getParameter("txtPermInvName");
        }
        session.setAttribute("txtPermInvName", strInvName);

        if (request.getParameter("cmbPermInvType") == null) {
            if (session.getAttribute("cmbPermInvType") == null) {
                strInvType = "";
            } else {
                strInvType = (String) session.getAttribute("cmbPermInvType");
            }
        } else {
            strInvType = request.getParameter("cmbPermInvType");
        }
        strInvType = (strInvType.equals("") ? "All" : strInvType);
        session.setAttribute("cmbPermInvType", strInvType);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strInvId + "," + strRoleName + "," + (strInvType.equals("All") ? "" : (strInvType.equals("Screen") ? "SCR" : "REP")));
        al = Permission.listPermissions((String) session.getAttribute("Company"), strInvId, strRoleName, (strInvType.equals("All") ? "" : (strInvType.equals("Screen") ? "SCR" : "REP")));

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
            }
        }

        
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">User Administration - Role Permissions</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form name=\"FrmPermList\" METHOD=\"POST\" ACTION=\"pgPermissionlist.jsp\" >\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"height: 10px;\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtPermRoleName\">Role Name</label></td>\r\n");
      out.write("                        <td class=\"field\" width=\"200\">\r\n");
      out.write("\r\n");
      out.write("                            <input type=\"text\" id=\"txtPermRoleName\" name=\"txtPermRoleName\" size=\"43\" onKeyUp=\"ajax_showOptions('txtPermRoleName_ID',this,'Role',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtPermRoleName_ID\" name=\"txtPermRoleName_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtPermInvId\">Inventory Id</label></td>\r\n");
      out.write("                        <td class=\"field\" width=\"300\">\r\n");
      out.write("\r\n");
      out.write("                            <input type=\"text\" id=\"txtPermInvId\" name=\"txtPermInvId\" size=\"30\" onKeyUp=\"ajax_showOptions('txtPermInvId_ID',this,'Inventory',event)\">\r\n");
      out.write("                            <input type=\"text\" readonly size=\"10\" id=\"txtPermInvId_ID\" name=\"txtPermInvId_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"cmbPermInvType\">Inventory Type</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                            <span style=\"float: left\">\r\n");
      out.write("                                <select id=\"cmbPermInvType\" NAME=\"cmbPermInvType\" class=\"comboclass\" style=\"width: 70px;\" title=\"Inventory Type\">\r\n");
      out.write("                                    <option ");
      out.print((strInvType.equals("All") ? "SELECTED" : ""));
      out.write(">All</option>\r\n");
      out.write("                                    <option ");
      out.print((strInvType.equals("Report") ? "SELECTED" : ""));
      out.write(">Report</option>\r\n");
      out.write("                                    <option ");
      out.print((strInvType.equals("Screen") ? "SELECTED" : ""));
      out.write(">Screen</option>\r\n");
      out.write("                                </select>\r\n");
      out.write("\r\n");
      out.write("                            </span>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <td colspan=\"2\">\r\n");
      out.write("                            <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                            <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                                   onclick=\"location.replace('pgNavigate.jsp?Target=pgPermissionlist.jsp');\"/>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        </td>\r\n");
      out.write("        </form>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Form Body Ends -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 1px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Grid View Starts -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\">\r\n");
      out.write("                <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                            ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        ");
 if (al.size() != 0) {
      out.write("\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\">\r\n");
      out.write("                <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                    <tr class=\"grid_header\">\r\n");
      out.write("                        <td width=\"16\"></td>\r\n");
      out.write("                        <td width=\"100\">Inventory Id</td>\r\n");
      out.write("                        <td>Inventory Desc</td>\r\n");
      out.write("                        <td width=\"100\">Inv Type</td>\r\n");
      out.write("                        <td width=\"25\">View</td>\r\n");
      out.write("                        <td width=\"25\">Add</td>\r\n");
      out.write("                        <td width=\"25\">Edit</td>\r\n");
      out.write("                        <td width=\"25\">Delete</td>\r\n");
      out.write("                        <td></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    ");

     aPermission = null;
     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         aPermission = ((Permission) al.get(i));
                    
      out.write("\r\n");
      out.write("                    <tr class=\"\r\n");
      out.write("                        ");

                        if (i % 2 == 0) {
                            out.write("even_row");
                        } else {
                            out.write("odd_row");
                        }
                        
      out.write("\r\n");
      out.write("                        \">\r\n");
      out.write("                        <td width=\"16\" align='center'>\r\n");
      out.write("                            <a href=\"pgPermission.jsp?InvId=");
 out.write(aPermission.getInv().getInv_Id());
      out.write("&Role=");
      out.print(strRoleName);
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/edit.gif\" title=\"Edit the entry with ID = ");
      out.print(aPermission.getInv().getInv_Id());
      out.write("\" border='0'>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100\" align='center'>\r\n");
      out.write("                            ");

                        out.write(aPermission.getInv().getInv_Id());
                            
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td  align='left'>\r\n");
      out.write("                            ");

                        out.write(aPermission.getInv().getInv_Desc());
                            
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100\" align='center'>\r\n");
      out.write("                            ");

                        if (aPermission.getInv().getInv_Type().equals("SCR")) {
                            out.write("Screen");
                        } else {
                            out.write("Report");
                        }
                            
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"25\" align='center'>\r\n");
      out.write("                            ");
 if (aPermission.getPView() == 1) {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/tick.gif\" border='0'>\r\n");
      out.write("                            ");
}
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"25\" align='center'>\r\n");
      out.write("                            ");
 if (aPermission.getPAdd() == 1) {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/tick.gif\" border='0'>\r\n");
      out.write("                            ");
}
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"25\" align='center'>\r\n");
      out.write("                            ");
 if (aPermission.getPEdit() == 1) {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/tick.gif\" border='0'>\r\n");
      out.write("                            ");
}
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"25\" align='center'>\r\n");
      out.write("                            ");
 if (aPermission.getPDelete() == 1) {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/tick.gif\" border='0'>\r\n");
      out.write("                            ");
}
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("            <tr>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("        <!-- Pagination Starts -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\">\r\n");
      out.write("                <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td>\r\n");
      out.write("                            <a href=\"pgPermissionlist.jsp?CurPage=1\">\r\n");
      out.write("                            <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                            <a href=\"pgPermissionlist.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\"></a>\r\n");
      out.write("                            ");
 } else {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                            ");
 }
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                        <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                            <a href=\"pgPermissionlist.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\"></a>\r\n");
      out.write("                            ");
 } else {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                            ");
 }
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td>\r\n");
      out.write("                            <a href=\"pgPermissionlist.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\"></a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100%\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Pagination Ends -->\r\n");
      out.write("        ");
}
      out.write("\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 10px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
