package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.User;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.UserCountException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;

public final class pgValidateLogin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private User currentUser = null;

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : USM3\r\n");
      out.write("Date    : 11-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgValidateLogin.jsp");
        try {
            session.setAttribute("Company", request.getParameter("cmbCompany"));
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Creating user with params: " + (String) session.getAttribute("Company") + "," + request.getParameter("txtUsername"));
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), request.getParameter("txtUsername"));

        } catch (UserBlockedException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; User blocked exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getLocalizedMessage());

            pageContext.forward("pgLogin.jsp?LoginStatus=5");
            return;
        } catch (UserCountException uce) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; User count exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), uce.getLocalizedMessage());

            pageContext.forward("pgLogin.jsp?LoginStatus=7");
            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Other exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getLocalizedMessage());
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
            

            pageContext.forward("pgLogin.jsp?LoginStatus=1");
            return;
        }
        try {
            session.setAttribute("CurrentUser", currentUser.getUsername());
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Username validate success");

            if (currentUser.getIs_Locked() == 1) {
                 Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin;");
               
                

                pageContext.forward("pgLogin.jsp?LoginStatus=2");
                return;
            }
            if (currentUser.getPwd_Change_Flag() == 1) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Reset Password");

                pageContext.forward("pgResetPwd.jsp?Next=2");
                return;
            }
            if (!currentUser.getPassword().equals(request.getParameter("txtPassword"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Password wrong" );
                
                pageContext.forward("pgShortSavUser.jsp?CurrentUser=" + currentUser.getUsername() + "&PwdMistake=1&Next=1");
                return;
            }
            session.setAttribute("CurrentUserPW", currentUser.getPassword());
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Login success");
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Password : " + currentUser.getPassword());

            pageContext.forward("pgShortSavUser.jsp?CurrentUser=" + currentUser.getUsername() + "&PwdMistake=0&Next=2");
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgValidateLogin; Other exception");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getLocalizedMessage());

            pageContext.forward("pgLogin.jsp?LoginStatus=4");
            return;
        }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
