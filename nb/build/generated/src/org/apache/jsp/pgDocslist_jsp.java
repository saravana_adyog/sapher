package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import bl.sapher.Documents;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgDocslist_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CSD1";
    private int nRepNum = -1;
    private String strRepNum = "";
    private String strDocName = "";
    private boolean bAdd;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD1\r\n");
      out.write("Date    : \r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Case Listing - [Sapher]</title>\r\n");
      out.write("        <script>var SAPHER_PARENT_NAME = \"pgDocslist\";</script>\r\n");
      out.write("        <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgDocslist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";

        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">File upload Failed! Unknown reason.</font>";
            }
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), ".jsp: A/V/D=" + bAdd + "/" + bView + "/" + bDelete);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDetails.jsp?Parent=0&SaveFlag=I");
            return;
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgDocslist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("RepNum") == null) {
            if (session.getAttribute("txtRepNum") == null) {
                strRepNum = "";
            } else {
                strRepNum = (String) session.getAttribute("txtRepNum");
            }
        } else {
            strRepNum = request.getParameter("RepNum");
        }
        session.setAttribute("txtRepNum", strRepNum);

        if (strRepNum.equals("")) {
            nRepNum = -1;
        } else {
            nRepNum = Integer.parseInt(strRepNum);
        }

        if (request.getParameter("txtDocName") == null) {
            if (session.getAttribute("txtDocName") == null) {
                strDocName = "";
            } else {
                strDocName = (String) session.getAttribute("txtDocName");
            }
        } else {
            strDocName = request.getParameter("txtDocName");
        }
        session.setAttribute("txtDocName", strDocName);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strRepNum + "," + strDocName);
        al = Documents.listDocuments(
                Constants.DOCS_PARTIAL_DOWNLOAD, Constants.SEARCH_STRICT,
                (String) session.getAttribute("Company"),
                application.getRealPath(Documents.DOCS_RELATIVE_PATH),
                strRepNum, Constants.SEARCH_ANY_VAL/*doc id*/,
                strDocName, Constants.SEARCH_ANY_VAL/*doc type*/,
                Constants.SEARCH_ANY_VAL/*doc desc*/,
                Constants.STATUS_ALL);

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
            }
        }

        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Documents Listing</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form name=\"FrmCaseList\" method=\"POST\" action=\"pgDocslist.jsp\" >\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"height: 10px;\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtRepNum\">Rep Num</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtRepNum\" id=\"txtRepNum\" size=\"10\" maxlength=\"9\" title=\"Report Number\"\r\n");
      out.write("                                   value=\"");
      out.print(strRepNum);
      out.write("\" readonly />\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"cmbSuspendedCase\">Document Name</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtDocName\" id=\"txtDocName\" size=\"25\" maxlength=\"24\" title=\"Document name\"\r\n");
      out.write("                                   value=\"");
      out.print(strDocName);
      out.write("\" />\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <td colspan=\"2\">\r\n");
      out.write("                            <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                            <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                                   onclick=\"location.replace('pgNavigate.jsp?Target=pgDocslist.jsp?SaveFlag=U&RepNum=");
      out.print(strRepNum);
      out.write("');\"/>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <form METHOD=\"POST\" ACTION=\"pgSelDoc.jsp?RepNum=");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                        <span style=\"float: right\">\r\n");
      out.write("                            <input type=\"hidden\" name=\"SaveFlag\" value=\"I\"/>\r\n");
      out.write("                            <input type=\"hidden\" name=\"Parent\" value=\"1\"/>\r\n");
      out.write("                            <input type=\"submit\" name=\"cmdNew\"  title=\"Upload a new document\"  class=\"");
      out.print((bAdd ? "button" : "button disabled"));
      out.write("\"\r\n");
      out.write("                                   value=\"Upload new Document\" style=\"width: 140px;\" ");
      out.print((bAdd ? "" : " DISABLED"));
      out.write(" />\r\n");
      out.write("                        </span>\r\n");
      out.write("                    </form>\r\n");
      out.write("                </td>\r\n");
      out.write("\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("\r\n");
      out.write("        <!-- Form Body Ends -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 1px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Grid View Starts -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\">\r\n");
      out.write("                <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                            ");
      out.print(al.size() + " - " + " record(s) found");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        ");
 if (al.size() != 0) {
        
      out.write("\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\">\r\n");
      out.write("                <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                    <tr class=\"grid_header\">\r\n");
      out.write("                        <td width=\"16\"></td>\r\n");
      out.write("                        ");
 if (bDelete) {
      out.write("\r\n");
      out.write("                        <td width=\"16\"></td>\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                        <td width=\"200\">Document name</td>\r\n");
      out.write("                        <td width=\"100\">Size</td>\r\n");
      out.write("                        <td width=\"100\">Type</td>\r\n");
      out.write("                        <td width=\"50\">Status</td>\r\n");
      out.write("                        <td></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    ");

     Documents doc = null;
     SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
     String strStatusFlag = "";

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         doc = ((Documents) al.get(i));
         strStatusFlag = "" /*+ doc.getIs_Suspended()*/;
                    
      out.write("\r\n");
      out.write("                    <tr class=\"\r\n");
      out.write("                        ");

                        if (i % 2 == 0) {
                            out.write("even_row");
                        } else {
                            out.write("odd_row");
                        }
                        
      out.write("\r\n");
      out.write("                        \">\r\n");
      out.write("                        <td width=\"16\" align=\"center\">\r\n");
      out.write("                            <a href=\"#\"\r\n");
      out.write("                               onclick=\"popupWnd('_blank', 'yes','");
      out.print(("pgDownloadDoc.jsp?RepNum=" + strRepNum + "&DocId=" + doc.getDocId() + "&FullPath=" + Documents.DOCS_RELATIVE_PATH + "/" + doc.getDocName()));
      out.write("',800,600)\">\r\n");
      out.write("                                <img src=\"");

                        if (doc.getType().equalsIgnoreCase("doc")) {
                            out.write("images/icons/icon-doc-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("txt")) {
                            out.write("images/icons/icon-txt-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("jpg")) {
                            out.write("images/icons/icon-jpg-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("pdf")) {
                            out.write("images/icons/icon-pdf-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("rtf")) {
                            out.write("images/icons/icon-doc-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("zip")) {
                            out.write("images/icons/icon-zip-16x16.gif");
                        } else if (doc.getType().equalsIgnoreCase("xls")) {
                            out.write("images/icons/icon-exel-16x16.gif");
                        } else {
                            out.write("images/icons/icon-all-16x16.gif");
                        }
                                     
      out.write("\"\r\n");
      out.write("                                     title=\"Download/See the attached file: ");
      out.print(doc.getDocName());
      out.write("\" border='0' alt=\"View\">\r\n");
      out.write("                            </a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        ");
 if (bDelete) {
      out.write("\r\n");
      out.write("                        <td width=\"16\" align=\"center\" >\r\n");
      out.write("                            <a href=\"pgDelDoc.jsp?RepNum=");
      out.print(doc.getRepNum());
      out.write("&DocId=");
      out.print(doc.getDocId());
      out.write("\"\r\n");
      out.write("                               onclick=\"return confirm('Are you sure you want to delete?','Sapher')\">\r\n");
      out.write("                                <img src=\"images/icons/delete.gif\" title=\"Delete the entry with ID = ");
      out.print(doc.getRepNum());
      out.write("\" border='0' alt=\"Delete\">\r\n");
      out.write("                            </a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                        <td width=\"200\" align='left'>\r\n");
      out.write("                            ");
      out.print(doc.getDocName());
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100\" align='left'>\r\n");
      out.write("                            ");
      out.print(doc.getDocSize());
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100\" align='left'>\r\n");
      out.write("                            ");
      out.print((doc.getType() + " File"));
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"50\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("            <tr>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("        <!-- Pagination Starts -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\">\r\n");
      out.write("                <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td>\r\n");
      out.write("                            <a href=\"pgDocslist.jsp?CurPage=1\">\r\n");
      out.write("                            <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\" alt=\"First\"></a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                            <A href=\"pgDocslist.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\" alt=\"Prev\"></A>\r\n");
      out.write("                            ");
 } else {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\" alt=\"Prev\">\r\n");
      out.write("                            ");
 }
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                        <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                            <A href=\"pgDocslist.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\" alt=\"Next\"></A>\r\n");
      out.write("                            ");
 } else {
      out.write("\r\n");
      out.write("                            <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\" alt=\"Next\">\r\n");
      out.write("                            ");
 }
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td>\r\n");
      out.write("                            <a href=\"pgDocslist.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                            <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\" alt=\"Last\"></a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100%\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Pagination Ends -->\r\n");
      out.write("        ");
}
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"formbody\" align=\"right\">\r\n");
      out.write("                <input type=\"reset\" id=\"cmdBack\" name=\"cmdBack\" class=\"button\" value=\"Back\"\r\n");
      out.write("                       style=\"width: 60px;\" title=\"Go back to Case Summary\"\r\n");
      out.write("                       onclick=\"javascript:location.replace('");
      out.print("pgCaseSummary.jsp?RepNum=" + strRepNum);
      out.write("')\"/>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
