package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.Conc_Medication;
import java.sql.Date;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.DBConnectionException;
import java.text.SimpleDateFormat;

public final class pgSavConcMed_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : \r\n");
      out.write("Date    : 22-01-2008.\r\n");
      out.write("Author  : Anoop Varma.\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");

        SimpleDateFormat dFormat = null;
        final String FORM_ID = "CSD1";
        String strSaveFlag = "I";
        int intRepNum = -1;
        String strRTypeCode = "";
        int nSuspended = 0;
        String saveFlag = "";
        String strParent = "";
        boolean bDirect = false;
        boolean bAdd = false;
        boolean bEdit = false;
        User currentUser = null;
        Conc_Medication aCM = null;
        Date dtStartDate = null;
        Date dtEndDate = null;
        String strProdCode = "";
        String strIndicationNum = "";

        nSuspended = 0;
        dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        try {
            Inventory inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            pageContext.forward("pgLogin.jsp?LoginStatus=3");
        }
        session.removeAttribute("DirtyFlag");

        if (request.getParameter("SaveFlag") != null) {
            strSaveFlag = request.getParameter("SaveFlag");
        }

        if (request.getParameter("txtRepNum") != null) {
            if (!request.getParameter("txtRepNum").equals("")) {
                intRepNum = Integer.parseInt(request.getParameter("txtRepNum"));
            }
        }

        if (request.getParameter("chkSuspended") != null) {
            nSuspended = 1;
        }

        if (request.getParameter("txtStartDate") != null) {
            if (!request.getParameter("txtStartDate").equals("")) {
                dtStartDate = new Date(dFormat.parse(request.getParameter("txtStartDate")).getTime());
            }
        }

        if (request.getParameter("txtEndDate") != null) {
            if (!request.getParameter("txtEndDate").equals("")) {
                dtEndDate = new Date(dFormat.parse(request.getParameter("txtEndDate")).getTime());
            }
        }

        if (request.getParameter("txtProdCode_ID") != null) {
            strProdCode = request.getParameter("txtProdCode_ID").trim();
        }

        if (request.getParameter("txtIndicationNum_ID") != null) {
            strIndicationNum = request.getParameter("txtIndicationNum_ID").trim();
        }

        if (strSaveFlag.equals("I")) {
            try {
                if (!bAdd) {
                    throw new ConstraintViolationException("2");
                }
                aCM = new Conc_Medication();

                aCM.saveConc_Medication((String) session.getAttribute("Company"), "I", currentUser.getUsername(), intRepNum, strProdCode,
                        strIndicationNum, dtStartDate, dtEndDate);

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; Insertion success.");
                //intRepNum = aCM.getRep_Num();
                pageContext.forward("pgCaseSummary.jsp?SavStatus=0&RepNum=" + intRepNum + "&SaveFlag=U");

            } catch (ConstraintViolationException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=3" + ex.getMessage());
            } catch (DBConnectionException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=4");
            } catch (GenericException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=5");
            } catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Exception:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
                System.out.println(e);
            }
        } else if (strSaveFlag.equals("U")) {
            try {
                if (!bEdit) {
                    throw new ConstraintViolationException("2");
                }
                aCM = new Conc_Medication((String) session.getAttribute("Company"), "y", intRepNum, strProdCode);

                aCM.saveConc_Medication((String) session.getAttribute("Company"), "U", currentUser.getUsername(), intRepNum, strProdCode,
                        strIndicationNum, dtStartDate, dtEndDate);
                pageContext.forward("pgCaseSummary.jsp?SavStatus=0&RepNum=" + intRepNum + "&SaveFlag=U");

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; Updation success (ID:" + intRepNum + ").");

            /*      if (request.getParameter("Parent").equals("0"))
            pageContext.forward("content.jsp?Status=2");
            else
            if (request.getParameter("Next") == null)
            pageContext.forward("pgCaseSummary.jsp?SavStatus=0");
            else if (request.getParameter("Next").equals("2"))
            pageContext.forward("pgCaseSummary.jsp?SavStatus=0");
             */
            } catch (ConstraintViolationException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; ConstraintViolationException");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=3" + ex.getMessage());
            } catch (DBConnectionException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; DBConnectionException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=4");
            } catch (GenericException ex) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavConcMed.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; GenericException:");
                Logger.writeLog((String) session.getAttribute("LogFileName"), ex.getMessage());
                pageContext.forward("pgCaseSummary.jsp?SavStatus=5");
            }
        }
        
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
