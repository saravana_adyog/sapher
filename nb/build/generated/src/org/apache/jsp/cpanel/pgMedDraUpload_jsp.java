package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.CPanel;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.Logger;

public final class pgMedDraUpload_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>MedDRA Data updation - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            function validate() {\r\n");
      out.write("                if(document.getElementById('fuFileUpload_SOC').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_SOC').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(SOC.ASC)$/i.test(document.getElementById('fuFileUpload_SOC').value)) {\r\n");
      out.write("                    alert(\"Please select the SOC.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_SOC').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_SOC').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_HLGT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLGT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(HLGT.ASC)$/i.test(document.getElementById('fuFileUpload_HLGT').value)) {\r\n");
      out.write("                    alert(\"Please select the HLGT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLGT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLGT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_HLT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(HLT.ASC)$/i.test(document.getElementById('fuFileUpload_HLT').value)) {\r\n");
      out.write("                    alert(\"Please select the HLT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_PT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_PT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(PT.ASC)$/i.test(document.getElementById('fuFileUpload_PT').value)) {\r\n");
      out.write("                    alert(\"Please select the PT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_PT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_PT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_LLT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_LLT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(LLT.ASC)$/i.test(document.getElementById('fuFileUpload_LLT').value)) {\r\n");
      out.write("                    alert(\"Please select the LLT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_LLT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_LLT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_SOC_HLGT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_SOC_HLGT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(SOC_HLGT.ASC)$/i.test(document.getElementById('fuFileUpload_SOC_HLGT').value)) {\r\n");
      out.write("                    alert(\"Please select the SOC_HLGT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_SOC_HLGT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_SOC_HLGT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_HLGT_HLT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLGT_HLT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(HLGT_HLT.ASC)$/i.test(document.getElementById('fuFileUpload_HLGT_HLT').value)) {\r\n");
      out.write("                    alert(\"Please select the HLGT_HLT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLGT_HLT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLGT_HLT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.getElementById('fuFileUpload_HLT_PT').value == '') {\r\n");
      out.write("                    window.alert('Please select a file to upload.');\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLT_PT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!/(HLT_PT.ASC)$/i.test(document.getElementById('fuFileUpload_HLT_PT').value)) {\r\n");
      out.write("                    alert(\"Please select the HLT_PT.ASC file\");\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLT_PT').form.reset();\r\n");
      out.write("                    document.getElementById('fuFileUpload_HLT_PT').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("        </script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgMedDraUpload.jsp");

        String strUN = "";
        String strPW = "";
        String strClient = "";
        String strPath = "";

        try {

            strUN = (String) session.getAttribute("cpanelUser");
            strPW = (String) session.getAttribute("cpanelPwd");

            // If user name or password is null, The session timed out.
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }

            if (request.getParameter("cmbClientName") != null) {
                strClient = request.getParameter("cmbClientName");
                CPanel cpanel = CPanel.getInstance((String) session.getAttribute("LogFileName"));
                cpanel.updateMedDraFiles(strClient, strPath);
                session.setAttribute("MedDra_Client", strClient);
                out.write("0");
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraUpload.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='400' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form action=\"pgMedDraSave.jsp\" enctype=\"multipart/form-data\" method=\"post\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Upload documents</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload SOC.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_SOC\" id=\"fuFileUpload_SOC\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload HLGT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_HLGT\" id=\"fuFileUpload_HLGT\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload HLT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_HLG\" id=\"fuFileUpload_HLG\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload PT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_PT\" id=\"fuFileUpload_PT\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload LLT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_LLT\" id=\"fuFileUpload_LLT\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload SOC_HLGT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_SOC_HLGT\" id=\"fuFileUpload_SOC_HLGT\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload HLGT_HLT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_HLGT_HLT\" id=\"fuFileUpload_HLGT_HLT\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Upload HLT_PT.ASC:</td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"file\" name=\"fuFileUpload_HLT_PT\" id=\"fuFileUpload_HLT_PT\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"field\"></td>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"200\">Max File Size allowed is 10 MB.</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td></td>\r\n");
      out.write("                                <td>\r\n");
      out.write("                                    <input type=\"submit\" name=\"button\" class=\"button\" value=\"Upload\" onclick=\"return validate();\"/>\r\n");
      out.write("                                    <input type=\"hidden\" name=\"Client\" id=\"Client\" value=\"");
      out.print(strClient);
      out.write("\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('content.jsp?Status=6');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
