package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.ClientConf;
import bl.sapher.general.GenConf;

public final class pgSelectClient_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=windows-1252");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <title></title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            function nextWindow(next)\r\n");
      out.write("            {\r\n");
      out.write("                if (next==1) {\r\n");
      out.write("                    opener.location='pgBackup.jsp?clientName=' + document.getElementById('cmbClients').value;\r\n");
      out.write("                } else if (next==2) {\r\n");
      out.write("                    opener.location='pgRestoreList.jsp?clientName=' + document.getElementById('cmbClients').value;\r\n");
      out.write("                } else if (next==3) {\r\n");
      out.write("                    this.location='pgClearDB.jsp?clientName=' + document.getElementById('cmbClients').value;\r\n");
      out.write("                } else if (next==4) {\r\n");
      out.write("                    this.location='pgDelCases.jsp?clientName=' + document.getElementById('cmbClients').value\r\n");
      out.write("                }\r\n");
      out.write("                window.close();\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 15px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Select a Client</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <form name=\"FrmClientList\" METHOD=\"POST\" ACTION=\"\" >\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border=\"0\">\r\n");
      out.write("                            <tbody>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\">Clients :</td>\r\n");
      out.write("                                    <td><select name=\"cmbClients\" id=\"cmbClients\">\r\n");
      out.write("                                            ");

        java.util.ArrayList al = ClientConf.getClients();
        for (int i = 0; i < al.size(); i++) {
            out.write("<option>" + ((ClientConf) al.get(i)).getClientName() + "</option>");
        }
      out.write("\r\n");
      out.write("                                    </select></td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\"></td>\r\n");
      out.write("                                    <td>\r\n");
      out.write("                                        <input type=\"button\" value=\"Select\" class=\"button\" name=\"cmdSelect\"\r\n");
      out.write("                                               onclick=\"nextWindow(");
      out.print(request.getParameter("Next"));
      out.write(")\" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </tbody>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </form>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("\r\n");
      out.write("    </script>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
