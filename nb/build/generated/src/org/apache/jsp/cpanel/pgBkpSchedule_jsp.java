package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import bl.sapher.BackupSchedule;
import bl.sapher.general.ClientConf;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class pgBkpSchedule_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


    private BackupSchedule bkpSch = null;
    private final String FORM_ID = "CPL7.2";
    private String strUN = "";
    private String strPW = "";
    private String saveFlag = "";
    private String freq = "";
    private int interval = -1;
    private String days = "";
    private int index = -1;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CPL7.2\r\n");
      out.write("Date    : 14-02-2009\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Backup Schedule - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"../libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"../libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/gui.js\"></script>\r\n");
      out.write("        <script>\r\n");
      out.write("            function validate(){\r\n");
      out.write("                if (document.FrmbkpSch.txtstartDate.value == \"\"){\r\n");
      out.write("                    alert( \"Start Date & Time cannot be empty.\");\r\n");
      out.write("                    document.FrmbkpSch.txtstartDate.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if (document.FrmbkpSch.txtInterval.value == \"\"){\r\n");
      out.write("                    alert( \"Frequency cannot be empty.\");\r\n");
      out.write("                    document.FrmbkpSch.txtInterval.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("            function kyStart(e){\r\n");
      out.write("                var keynum;\r\n");
      out.write("                if(window.event) // IE\r\n");
      out.write("                {\r\n");
      out.write("                    keynum = e.keyCode;\r\n");
      out.write("                }\r\n");
      out.write("                else if(e.which) // Netscape/Firefox/Opera\r\n");
      out.write("                {\r\n");
      out.write("                    keynum = e.which;\r\n");
      out.write("                }\r\n");
      out.write("                if (keynum == 27)\r\n");
      out.write("                    document.FrmbkpSch.txtstartDate.value = \"\";\r\n");
      out.write("            }\r\n");
      out.write("            function kyEnd(e){\r\n");
      out.write("                var keynum;\r\n");
      out.write("                if(window.event) // IE\r\n");
      out.write("                {\r\n");
      out.write("                    keynum = e.keyCode;\r\n");
      out.write("                }\r\n");
      out.write("                else if(e.which) // Netscape/Firefox/Opera\r\n");
      out.write("                {\r\n");
      out.write("                    keynum = e.which;\r\n");
      out.write("                }\r\n");
      out.write("                if (keynum == 27)\r\n");
      out.write("                    document.FrmbkpSch.txtendDate.value = \"\";\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));
            saveFlag = ((String) request.getParameter("saveFlag"));
            interval = -1;
            days = "";
            index = -1;
            freq = "";

            if (strUN==null || strPW==null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
        } catch (TimeoutException toe) {
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
            
            if (saveFlag.equals("U")) {
                bkpSch = new BackupSchedule(strUN, strPW, request.getParameter("jobName"));
                index = bkpSch.getRepeat_interval().indexOf("freq=");
                if (index != -1) {
                    freq = bkpSch.getRepeat_interval().substring(index + 5, bkpSch.getRepeat_interval().indexOf(";"));
                }
                index = bkpSch.getRepeat_interval().indexOf("interval=");
                if (index != -1) {
                    interval = Integer.parseInt(bkpSch.getRepeat_interval().substring(index + 9, bkpSch.getRepeat_interval().indexOf(";", index)));
                }
                index = bkpSch.getRepeat_interval().indexOf("byday=");
                if (index != -1) {
                    days = bkpSch.getRepeat_interval().substring(index + 6, bkpSch.getRepeat_interval().indexOf(";", index));
                }
            } else {
                bkpSch = new BackupSchedule();
            }

        } catch (RecordNotFoundException e) {
            e.printStackTrace();
            return;
        }
        
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='600' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Control Panel</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmbkpSch\" name=\"FrmbkpSch\" method=\"POST\" action=\"pgSavBkpSch.jsp\"\r\n");
      out.write("                      onsubmit=\"return validate();\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">");
      out.print((saveFlag.equals("I") ? "New " : (saveFlag.equals("U") ? "Edit " : "")));
      out.write("Backup Schedule</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtjobName\">Job Name</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtjobName\" title=\"Job Name\"\r\n");
      out.write("                                                             id=\"txtjobName\" size=\"50\" maxlength=\"50\"\r\n");
      out.write("                                                             value=\"");
      out.print(bkpSch.getJobName());
      out.write("\"\r\n");
      out.write("                                                             readonly>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"cmbClient\">Client</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <select name=\"cmbClient\" style=\"width:260px\" class=\"comboclass\">\r\n");
      out.write("                                        ");

        java.util.ArrayList al = ClientConf.getClients();
        ClientConf cc = null;
        for (int i = 0; i < al.size(); i++) {
            cc = (ClientConf) al.get(i);
            out.write("<option" + " value=\"" + cc.getUserName() + "\"" + (saveFlag.equals("U") && bkpSch.getSchemaName().equalsIgnoreCase(cc.getUserName()) ? " SELECTED " : "") + ">" + cc.getClientName() + "</option>");
        }
      out.write("\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtstartDate\">Start Date &amp; Time</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtstartDate\" title=\"Start Date &amp; Time\"\r\n");
      out.write("                                                       id=\"txtstartDate\" size=\"18\" maxlength=\"18\"\r\n");
      out.write("                                                       value=\"");
      out.print((saveFlag.equals("I") ? "" : new SimpleDateFormat("dd-MMM-yyyy kk:mm", java.util.Locale.US).format(bkpSch.getStartDate())));
      out.write("\"\r\n");
      out.write("                                                       readonly onkeydown=\"return kyStart(event);\">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <a href=\"#\" id=\"cal_trigger1\"><img src=\"../images/icons/cal.gif\" border='0' alt=\"Select\" title=\"Select Date\" name=\"imgCal\"></a></td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtstartDate\">End Date</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtendDate\" title=\"Start Date\"\r\n");
      out.write("                                                       id=\"txtendDate\" size=\"18\" maxlength=\"18\"\r\n");
      out.write("                                                       value=\"");
      out.print((saveFlag.equals("I") ? "" : ((bkpSch.getEndDate() == null) ? "" : new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US).format(bkpSch.getEndDate()))));
      out.write("\"\r\n");
      out.write("                                                       readonly onkeydown=\"return kyEnd(event);\">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <a href=\"#\" id=\"cal_trigger2\"><img src=\"../images/icons/cal.gif\" border='0' alt=\"Select\" title=\"Select Date\" name=\"imgCal\"></a></td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"cmbFrequency\">Frequency</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" id=\"txtInterval\" name=\"txtInterval\" size=\"5\" maxlength=\"2\" onkeydown=\"return checkIsNumeric(event);\" value=\"");
      out.print((interval != -1 ? interval : ""));
      out.write("\">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <select name=\"cmbFrequency\" style=\"width:80px\" class=\"comboclass\">\r\n");
      out.write("                                                    <option value=\"daily\" ");
      out.print((freq.equals("daily") ? "SELECTED" : ""));
      out.write(">Days</option>\r\n");
      out.write("                                                    <option value=\"weekly\" ");
      out.print((freq.equals("weekly") ? "SELECTED" : ""));
      out.write(">Weeks</option>\r\n");
      out.write("                                                    <option value=\"monthly\" ");
      out.print((freq.equals("monthly") ? "SELECTED" : ""));
      out.write(">Months</option>\r\n");
      out.write("                                                </select>\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkSunday\">Select Days</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                        <td class=\"fLabel\">\r\n");
      out.write("                                            <label for=\"chkSunday\">Sunday</label>\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <input type=\"checkbox\" id=\"chkSunday\" name=\"chkSunday\" ");
      out.print((days.indexOf("SUN") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td class=\"fLabel\">\r\n");
      out.write("                                            <label for=\"chkMonday\">Monday</label>\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <input type=\"checkbox\" id=\"chkMonday\" name=\"chkMonday\" ");
      out.print((days.indexOf("MON") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td class=\"fLabel\">\r\n");
      out.write("                                            <label for=\"chkTuesday\">Tuesday</label>\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <input type=\"checkbox\" id=\"chkTuesday\" name=\"chkTuesday\" ");
      out.print((days.indexOf("TUE") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td class=\"fLabel\">\r\n");
      out.write("                                            <label for=\"chkWednesday\">Wednesday</label>\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <input type=\"checkbox\" id=\"chkWednesday\" name=\"chkWednesday\" ");
      out.print((days.indexOf("WED") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td class=\"fLabel\">\r\n");
      out.write("                                                <label for=\"chkThursday\">Thursday</label>\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"checkbox\" id=\"chkThursday\" name=\"chkThursday\" ");
      out.print((days.indexOf("THU") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td class=\"fLabel\">\r\n");
      out.write("                                                <label for=\"chkFriday\">Friday</label>\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"checkbox\" id=\"chkFriday\" name=\"chkFriday\" ");
      out.print((days.indexOf("FRI") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td class=\"fLabel\">\r\n");
      out.write("                                                <label for=\"chkSaturday\">Saturday</label>\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"checkbox\" id=\"chkSaturday\" name=\"chkSaturday\" ");
      out.print((days.indexOf("SAT") != -1 ? "CHECKED" : ""));
      out.write(">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"></td>\r\n");
      out.write("                                <td>\r\n");
      out.write("                                    <input  type=\"hidden\" name=\"saveFlag\" id=\"saveFlag\" value=\"");
      out.print(saveFlag);
      out.write("\">\r\n");
      out.write("                                    <input type=\"submit\" class=\"button\" id=\"cmdSubmit\" name=\"cmdSubmit\" value=\"Save\" style=\"width: 60px;\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('pgScheduleList.jsp');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <script type=\"text/javascript\">\r\n");
      out.write("                                Calendar.setup({\r\n");
      out.write("                                    inputField : \"txtstartDate\", //*\r\n");
      out.write("                                    ifFormat : \"%d-%b-%Y\",// %H:%M\r\n");
      out.write("                                    showsTime : true,\r\n");
      out.write("                                    button : \"cal_trigger1\", //*\r\n");
      out.write("                                    step : 1\r\n");
      out.write("                                });\r\n");
      out.write("                            </script>\r\n");
      out.write("                            <script type=\"text/javascript\">\r\n");
      out.write("                                Calendar.setup({\r\n");
      out.write("                                    inputField : \"txtendDate\", //*\r\n");
      out.write("                                    ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                    showsTime : false,\r\n");
      out.write("                                    button : \"cal_trigger2\", //*\r\n");
      out.write("                                    step : 1\r\n");
      out.write("                                });\r\n");
      out.write("                            </script>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 10px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
