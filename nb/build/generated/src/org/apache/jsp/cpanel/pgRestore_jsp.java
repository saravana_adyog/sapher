package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.CPanel;
import bl.sapher.general.SimultaneousOpException;
import bl.sapher.general.TimeoutException;
import bl.sapher.User;

public final class pgRestore_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\r');
      out.write('\n');

//<!--
//    Form Id : CPL5
//    Date    : 19-3-2008
//    Author  : Arun P. Jose
//-->

        try {
            if (request.getParameter("FileName") != null) {
                User.blockUsers();
            }

            String strUN = "";
            String strPW = "";

            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }

            CPanel cpanel = CPanel.getInstance((String) session.getAttribute("LogFileName"));
            System.out.println("AV> UN=" + strUN + ",PW=" + strPW + ",file=" + request.getParameter("FileName") + ",schema="+ request.getParameter("SchemaName") + ",new user" + request.getParameter("NewUser"));
            cpanel.doRestore(strUN, strPW, request.getParameter("FileName"),
                    request.getParameter("SchemaName"),
                    request.getParameter("NewUser"));
            User.unBlockUsers();
            out.write("0");
            //pageContext.forward("pgRestoreMain.jsp?RstrStatus=0");
        } catch (SimultaneousOpException soe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                soe.printStackTrace();
            }
            out.write("2");
            //pageContext.forward("pgRestoreMain.jsp?RstrStatus=2");
        } catch (TimeoutException toe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                toe.printStackTrace();
            }

      out.write("\r\n");
      out.write("<script>\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("</script>\r\n");
 } catch (Exception ex) {

            ex.printStackTrace();
            out.write("1");
            //pageContext.forward("pgRestoreMain.jsp?RstrStatus=1");
        }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
