package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.GenConf;
import bl.sapher.general.ClientConf;
import bl.sapher.general.Logger;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class test_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Sapher</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <!--[if IE]> <style type=\"text/css\"> .login_form label{ float: none; }  </style> <![endif]-->\r\n");
      out.write("        <link rel=\"shortcut icon\" href=\"images/icons/sapher.ico\" type=\"image/vnd.microsoft.icon\" />\r\n");
      out.write("        <link rel=\"icon\" href=\"images/icons/sapher.ico\" type=\"image/vnd.microsoft.icon\" />\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            function startTest() {\r\n");
      out.write("                var url = 'test-resp.jsp?';\r\n");
      out.write("                url += 'cmbCompany=' + document.getElementById('cmbCompany').value;\r\n");
      out.write("                console.log(\"TEST\");\r\n");
      out.write("\r\n");
      out.write("                return getDynData(url, 'divConsole');\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function createRequestObject(){\r\n");
      out.write("                var req;\r\n");
      out.write("                if(window.XMLHttpRequest){\r\n");
      out.write("                    //For Firefox, Safari, Opera\r\n");
      out.write("                    req = new XMLHttpRequest();\r\n");
      out.write("                }\r\n");
      out.write("                else if(window.ActiveXObject){\r\n");
      out.write("                    //For IE 5+\r\n");
      out.write("                    req = new ActiveXObject(\"Microsoft.XMLHTTP\");\r\n");
      out.write("                }\r\n");
      out.write("                else{\r\n");
      out.write("                    //Error for an old browser\r\n");
      out.write("                    alert('Your browser is not IE 5 or higher, or Firefox or Safari or Opera');\r\n");
      out.write("                }\r\n");
      out.write("\r\n");
      out.write("                return req;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            //Make the XMLHttpRequest Object\r\n");
      out.write("            var http = createRequestObject();\r\n");
      out.write("\r\n");
      out.write("            function sendRequest(method, url, target_DIV){\r\n");
      out.write("                if(method == 'get' || method == 'GET'){\r\n");
      out.write("                    http.open(method,url);\r\n");
      out.write("                    http.onreadystatechange = function() {handleResponse(target_DIV);}\r\n");
      out.write("                    http.send(null);\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function handleResponse(target) {\r\n");
      out.write("                if(http.readyState == 4 && http.status == 200){\r\n");
      out.write("                    var response = http.responseText;\r\n");
      out.write("\r\n");
      out.write("                    if(response) {\r\n");
      out.write("                        if(target != '') {\r\n");
      out.write("                            document.getElementById(target).innerHTML = response;\r\n");
      out.write("                        }\r\n");
      out.write("                    }\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function getDynData(targetJsp, targetDIV) {\r\n");
      out.write("                sendRequest('GET',targetJsp,targetDIV);\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    ");

        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        GenConf.setLogPath(application.getRealPath("/Reports/logs"));
        String strVer = "";
        String strMessage = "";
        String strCRight = "";
        GenConf gConf = new GenConf();
        session.setAttribute("LogFileName", "sapher_startup_" +
                new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US).format(Calendar.getInstance().getTime()));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLogin; RemoteIP: " + request.getRemoteAddr());

        strVer = gConf.getSysVersion();
        strCRight = gConf.getSysCRight();
        if (request.getParameter("LoginStatus") != null) {
            if (request.getParameter("LoginStatus").equals("1")) {
                strMessage = "Invalid username or password";
            } else if (request.getParameter("LoginStatus").equals("2")) {
                strMessage = "Account Locked, Please contact administrator";
            } else if (request.getParameter("LoginStatus").equals("3")) {
                strMessage = "User already logged in.";
            } else if (request.getParameter("LoginStatus").equals("4")) {
                strMessage = "Cannot Login! Unknown Reason";
            } else if (request.getParameter("LoginStatus").equals("5")) {
                strMessage = "Login Blocked, DB Maintenance in progress";
            } else if (request.getParameter("LoginStatus").equals("6")) {
                strMessage = "Session timed out. Login again.";
            } else if (request.getParameter("LoginStatus").equals("7")) {
                strMessage = "No more login allowed, as per the license(s).";
            }
        }
    
      out.write("\r\n");
      out.write("    <body onload=\"return startTest();\">\r\n");
      out.write("        <form name=\"login\" method=\"post\" action=\"\">\r\n");
      out.write("            <td align=\"right\" valign=\"top\" height=\"142\">\r\n");
      out.write("                <div class=\"error\" align=\"center\" style=\"padding: 5px 0px 2px 0px; \">");
      out.print(strMessage);
      out.write("</div>\r\n");
      out.write("                <div class=\"login_form\">\r\n");
      out.write("                    <div>\r\n");
      out.write("                        <label for=\"user\">Company: </label>\r\n");
      out.write("                        <select id=\"cmbCompany\" name=\"cmbCompany\" style=\"width:160px\" onchange=\"return startTest();\">\r\n");
      out.write("                            ");

        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        java.util.ArrayList al = ClientConf.getClients();
        for (int i = 0; i < al.size(); i++) {
            out.write("<option>" + ((ClientConf) al.get(i)).getClientName() + "</option>");
        }
      out.write("\r\n");
      out.write("                        </select>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div style=\"height: 10px;\"></div>\r\n");
      out.write("                <div id=\"divConsole\"></div>\r\n");
      out.write("            </td>\r\n");
      out.write("        </form>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
