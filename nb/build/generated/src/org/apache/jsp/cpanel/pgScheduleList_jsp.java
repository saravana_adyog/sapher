package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.BackupSchedule;
import bl.sapher.general.TimeoutException;
import java.util.ArrayList;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class pgScheduleList_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CPL7";
    private ArrayList<BackupSchedule> al = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private String strMessageInfo = "";
    private BackupSchedule bkpSch = null;
    private String freq = "";
    private int interval = -1;
    private String days = "";
    private int index = -1;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CPL7\r\n");
      out.write("Date    : 11-2-2009\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Backup Schedule - [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"../libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"../libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        strMessageInfo = "";
        if (request.getParameter("SavStatus") != null) {
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Update Failed! " +  (String) request.getParameter("Msg") + " </font>";
            }
        }

        if (request.getParameter("DelStatus") != null) {
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Delete Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Delete Failed! Please investigate.</font>";
            }
        }

        String strUN = "";
        String strPW = "";

        strUN = ((String) session.getAttribute("cpanelUser"));
        strPW = ((String) session.getAttribute("cpanelPwd"));

        try {
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            al = BackupSchedule.listBkpSchedule(strUN, strPW);
        } catch (TimeoutException toe) {
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");
        }
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Backup Schedule</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form name=\"FrmBkpScheduleList\" METHOD=\"POST\" ACTION=\"pgBkpSchedule.jsp?saveFlag=I\" >\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <input type=\"submit\" name=\"cmdCreateSchedule\" class=\"button\" value=\"Create New Schedule\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 1px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"form_group_title\" colspan=\"10\">\r\n");
      out.write("                                ");
      out.print(al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");
 if (al.size() == 0) {
            return;
        }
            
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                        <tr class=\"grid_header\">\r\n");
      out.write("                            <td width=\"10\"></td>\r\n");
      out.write("                            <td width=\"10\"></td>\r\n");
      out.write("                            <td width=\"50\">Schema</td>\r\n");
      out.write("                            <td width=\"200\">Schedule Name</td>\r\n");
      out.write("                            <td width=\"40\">Schedule Start</td>\r\n");
      out.write("                            <td width=\"40\">Schedule End</td>\r\n");
      out.write("                            <td width=\"40\">Schedule Time</td>\r\n");
      out.write("                            <td width=\"100\">Frequency</td>\r\n");
      out.write("                            <td width=\"100\">Days</td>\r\n");
      out.write("                            <td width=\"40\">Last Run</td>\r\n");
      out.write("                            <td width=\"40\">Next Run</td>\r\n");
      out.write("                            <td width=\"50\">State</td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");

        nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
        if (nCurPage > nPageCount) {
            nCurPage = 1;
        }
        int nStartPos = ((nCurPage - 1) * nRowCount);
        for (int i = nStartPos;
                i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
                i++) {
            bkpSch = al.get(i);
            interval = -1;
            days = "";
            index = -1;
            freq = "";
            index = bkpSch.getRepeat_interval().indexOf("freq=");
            if (index != -1) {
                freq = bkpSch.getRepeat_interval().substring(index + 5, bkpSch.getRepeat_interval().indexOf(";"));
            }
            index = bkpSch.getRepeat_interval().indexOf("interval=");
            if (index != -1) {
                interval = Integer.parseInt(bkpSch.getRepeat_interval().substring(index + 9, bkpSch.getRepeat_interval().indexOf(";", index)));
            }
            index = bkpSch.getRepeat_interval().indexOf("byday=");
            if (index != -1) {
                days = bkpSch.getRepeat_interval().substring(index + 6, bkpSch.getRepeat_interval().indexOf(";", index));
            }

                        
      out.write("\r\n");
      out.write("                        <tr class=\"\r\n");
      out.write("                            ");

                            if (i % 2 == 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            
      out.write("\r\n");
      out.write("                            \">\r\n");
      out.write("                            <td width=\"10\" align=\"center\" >\r\n");
      out.write("                                <a href=\"pgBkpSchedule.jsp?saveFlag=U&jobName=");
      out.print(bkpSch.getJobName());
      out.write("\">\r\n");
      out.write("                                    <img src=\"../images/icons/edit.gif\" border='0'>\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"10\" align=\"center\">\r\n");
      out.write("                                <a href=\"pgDelBkpSchedule.jsp?jobName=");
      out.print(bkpSch.getJobName());
      out.write("\"\r\n");
      out.write("                                   onclick=\"return confirm('Are you sure you want to delete?','Sapher')\">\r\n");
      out.write("                                    <img src=\"../images/icons/delete.gif\" border='0' title=\"Delete the schedule entry\">\r\n");
      out.write("                                </a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"50\">");
      out.print(bkpSch.getSchemaName());
      out.write("</td>\r\n");
      out.write("                            <td width=\"200\">");
      out.print(bkpSch.getJobName());
      out.write("</td>\r\n");
      out.write("                            <td width=\"40\">\r\n");
      out.write("                                ");
      out.print(new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US).format(bkpSch.getStartDate()));
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width = \"40\" >\r\n");
      out.write("                                ");
      out.print(((bkpSch.getEndDate() == null) ? "" : new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US).format(bkpSch.getEndDate())));
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"40\">\r\n");
      out.write("                                ");
      out.print(new SimpleDateFormat("kk:mm:ss", java.util.Locale.US).format(bkpSch.getStartDate()));
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"100\">");
      out.print(interval + " " + freq);
      out.write("</td>\r\n");
      out.write("                            <td width=\"100\">");
      out.print(days);
      out.write("</td>\r\n");
      out.write("                            <td width=\"40\">");
      out.print((bkpSch.getLastRun() == null ? "" : bkpSch.getLastRun()));
      out.write("</td>\r\n");
      out.write("                            <td width=\"40\">");
      out.print((bkpSch.getNextRun() == null ? "" : bkpSch.getNextRun()));
      out.write("</td>\r\n");
      out.write("                            <td width=\"50\">");
      out.print(bkpSch.getState());
      out.write("</td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("                <tr>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("            <!-- Pagination Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"pgScheduleList.jsp?CurPage=1\">\r\n");
      out.write("                                <img src=\"../images/icons/page-first.gif\" border=\"0\"></a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                                <A href=\"pgScheduleList.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                                <img src=\"../images/icons/page-prev.gif\" border=\"0\"></A>\r\n");
      out.write("                                ");
 } else {
      out.write("\r\n");
      out.write("                                <img src=\"../images/icons/page-prev.gif\" border=\"0\">\r\n");
      out.write("                                ");
 }
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                            <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                                <A href=\"pgScheduleList.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                                <img src=\"../images/icons/page-next.gif\" border=\"0\"></A>\r\n");
      out.write("                                ");
 } else {
      out.write("\r\n");
      out.write("                                <img src=\"../images/icons/page-next.gif\" border=\"0\">\r\n");
      out.write("                                ");
 }
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td>\r\n");
      out.write("                                <a href=\"pgScheduleList.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                                <img src=\"../images/icons/page-last.gif\" border=\"0\"></A>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"100%\"></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Pagination Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
