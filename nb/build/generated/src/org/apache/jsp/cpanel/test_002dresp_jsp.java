package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.DBCon;
import bl.sapher.general.License;
import bl.sapher.general.ClientConf;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;

public final class test_002dresp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");

        try {
            ClientConf cc = ClientConf.getByClientName(request.getParameter("cmbCompany"));

            out.write("<table  border = \"1\" >");
            out.write("  <tr>");
            out.write("    <td colspan=\"2\">" + "Connection Details" + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>Client</td>");
            out.write("    <td>" + cc.getClientName() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>Host</td>");
            out.write("    <td>" + cc.getHostName() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>Port</td>");
            out.write("    <td>" + cc.getPort() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td>SID</td>");
            out.write("    <td>" + cc.getSID() + "</td>");
            out.write("  </tr>");
            out.write("    <td>Username</td>");
            out.write("    <td>" + cc.getUserName() + "</td>");
            out.write("  </tr>");
            out.write("    <td>Passwd</td>");
            out.write("    <td>" + cc.getPassword() + "</td>");
            out.write("  </tr>");
            out.write("  <tr>");
            out.write("    <td colspan=\"2\"></td>");
            out.write("  </tr>");

            java.util.ArrayList<License> al = License.listLicense(cc.getClientName(), "");

            out.write("  <tr>");
            out.write("    <td colspan=\"2\">Total " + License.getNumOfUsers(cc.getClientName()) + " users in " + al.size() + " licenses</td>");
            out.write("  </tr>");
            for (int i = 0; i < al.size(); i++) {
                out.write("  <td colspan=\"2\">License " + (i + 1) + "</td>");
                out.write("  <tr>");
                out.write("    <td>" + "" + "</td>");
                out.write("    <td>" + al.get(i).getLicenseKey() + "</td>");
                out.write("  </tr>");
            }
            out.write("</table>");

            DBCon con = new DBCon(request.getParameter("cmbCompany"));
        } catch (DBConnectionException e) {
        
            StackTraceElement[] arr = e.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                out.write(arr[i].toString() + "\n");
                
            }
            return;
        } catch (GenericException uce) {
            StackTraceElement[] arr = uce.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                out.write(arr[i].toString() + "\n");
            }
            return;
        } catch (Exception e) {
            StackTraceElement[] arr = e.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                out.write(arr[i].toString() + "\n");
            }
            return;
        }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
