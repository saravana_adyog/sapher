package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.CPanel;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.ClientConf;
import java.util.ArrayList;
import java.text.DateFormatSymbols;

public final class pgRestoreMain_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CPL5";
    private ArrayList al = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private String strVal = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private String strMessageInfo = "";
    
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CPL6\r\n");
      out.write("Date    : 19-3-2008\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>Restore Wizard - [Sapher]</title>\r\n");
      out.write("    <script language=\"JavaScript\">\r\n");
      out.write("    </script>\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("    <!--Calendar Library Includes.. S => -->\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"../libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("    <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"../libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("    <!-- import the calendar script -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"../libjs/calendar/calendar.js\"></script>\r\n");
      out.write("    <!-- import the language module -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"../libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("    <!-- helper script that uses the calendar -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"../libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"../libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("    <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("    <script src=\"../libjs/jquery/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("    <script src=\"../libjs/jquery/query.ui.draggable.js\" type=\"text/javascript\"></script>\r\n");
      out.write("    <!-- Core files -->\r\n");
      out.write("    <script src=\"../libjs/jquery/AlertBox/jquery.alerts.js\" type=\"text/javascript\"></script>\r\n");
      out.write("    <link href=\"../libjs/jquery/AlertBox/jquery.alerts.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\r\n");
      out.write("\r\n");
      out.write("    <script>\r\n");
      out.write("        $(document).ready( function() {\r\n");
      out.write("            var text='';\r\n");
      out.write("            $(\"#dyncompo > div\").each(function (i) {\r\n");
      out.write("                text +='<option value=\"'+ $(this).html() +'\">'+ $(this).html() +'</option>';\r\n");
      out.write("            });\r\n");
      out.write("\r\n");
      out.write("            $(\"#prompt_button\").click( function() {\r\n");
      out.write("                jPrompt('Select a Client: ', text, 'Sapher', function(r) {\r\n");
      out.write("                    if( r ) {\r\n");
      out.write("                        window.location='pgRestoreList.jsp?clientName=' + r;\r\n");
      out.write("                    }\r\n");
      out.write("                });\r\n");
      out.write("            });\r\n");
      out.write("\r\n");
      out.write("        });\r\n");
      out.write("\r\n");
      out.write("        function popupSearchWnd(urlToOpen,window_width,window_height)\r\n");
      out.write("        {\r\n");
      out.write("            var window_left = (screen.availWidth/2)-(window_width/2);\r\n");
      out.write("            var window_top = (screen.availHeight/2)-(window_height/2);\r\n");
      out.write("            var winParms = \"Status=no\" + \",resizable=no\" + \",scrollbars=yes\" + \",height=\"+window_height+\",width=\"+window_width + \",left=\"+window_left+\",top=\"+window_top;\r\n");
      out.write("            var newwindow = window.open(urlToOpen,'_blank',winParms);\r\n");
      out.write("\r\n");
      out.write("            if(!newwindow.opener)\r\n");
      out.write("                newwindow.opener = self;\r\n");
      out.write("\r\n");
      out.write("            newwindow.focus()\r\n");
      out.write("        }\r\n");
      out.write("        function kyFrom(e){\r\n");
      out.write("            var keynum;\r\n");
      out.write("            if(window.event) // IE\r\n");
      out.write("            {\r\n");
      out.write("                keynum = e.keyCode;\r\n");
      out.write("            }\r\n");
      out.write("            else if(e.which) // Netscape/Firefox/Opera\r\n");
      out.write("            {\r\n");
      out.write("                keynum = e.which;\r\n");
      out.write("            }\r\n");
      out.write("            if (keynum == 27)\r\n");
      out.write("                document.FrmRstrList.txtRstrMainDtFrom.value = \"\";\r\n");
      out.write("        }\r\n");
      out.write("        function kyTo(e){\r\n");
      out.write("            var keynum;\r\n");
      out.write("            if(window.event) // IE\r\n");
      out.write("            {\r\n");
      out.write("                keynum = e.keyCode;\r\n");
      out.write("            }\r\n");
      out.write("            else if(e.which) // Netscape/Firefox/Opera\r\n");
      out.write("            {\r\n");
      out.write("                keynum = e.which;\r\n");
      out.write("            }\r\n");
      out.write("            if (keynum == 27)\r\n");
      out.write("                document.FrmRstrList.txtRstrMainDtTo.value = \"\";\r\n");
      out.write("        }\r\n");
      out.write("    </script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("    ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    ");

        strMessageInfo = "";
        if (request.getParameter("RstrStatus") != null) {
            if (((String) request.getParameter("RstrStatus")).equals("0")) {
                strMessageInfo = "Restore Success.";
            } else if (((String) request.getParameter("RstrStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Restore Failed! Please investigate.</font>";
            } else if (((String) request.getParameter("RstrStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Restore Failed! A backup/Restore operation already running. Try later.</font>";
            }
        }

        if (request.getParameter("txtRstrMainDtFrom") == null) {
            if (session.getAttribute("txtRstrMainDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtRstrMainDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtRstrMainDtFrom");
        }
        session.setAttribute("txtRstrMainDtFrom", strDtFrom);

        if (request.getParameter("txtRstrMainDtTo") == null) {
            if (session.getAttribute("txtRstrMainDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtRstrMainDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtRstrMainDtTo");
        }
        session.setAttribute("txtRstrMainDtTo", strDtTo);


        String strUN = null;
        String strPW = null;

        strUN = ((String) session.getAttribute("cpanelUser"));
        strPW = ((String) session.getAttribute("cpanelPwd"));

        try {
            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            al = CPanel.lstFiles(strUN, strPW, "RESTORE.LOG", strDtFrom, strDtTo);
        } catch (TimeoutException toe) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                toe.printStackTrace();
            }
    
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("    </script>\r\n");
      out.write("    ");

        }
        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

    
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <div style=\"height: 25px;\"></div>\r\n");
      out.write("    <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td id=\"formtitle\" height=\"21\">Restore</td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Form Body Starts -->\r\n");
      out.write("        <tr>\r\n");
      out.write("            <form name=\"FrmRstrList\" METHOD=\"POST\" ACTION=\"pgRestoreMain.jsp\" >\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"form_group_title\" colspan=\"10\">Search Restore Logs</td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td style=\"height: 10px;\">&nbsp;</td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtRstrMainDtFrom\">Date Range</label></td>\r\n");
      out.write("                    <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtRstrMainDtFrom\" id=\"txtRstrMainDtFrom\" size=\"18\" maxlength=\"18\" title=\"From Date\"\r\n");
      out.write("                                           value=\"");
      out.print(strDtFrom);
      out.write("\" readonly  onkeydown=\"return kyFrom(event);\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                <a href=\"#\" id=\"cal_trigger1\"><img src=\"../images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a></td>\r\n");
      out.write("                                <td class=\"fLabel\" style=\"padding-right: 3px; padding-left: 3px;\">TO</td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtRstrMainDtTo\" id=\"txtRstrMainDtTo\" size=\"18\" maxlength=\"18\" title=\"To Date\"\r\n");
      out.write("                                           value=\"");
      out.print(strDtTo);
      out.write("\" readonly  onkeydown=\"return kyTo(event);\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    <a href=\"#\" id=\"cal_trigger2\"><img src=\"../images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td colspan=\"2\" width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    Calendar.setup({\r\n");
      out.write("                        inputField : \"txtRstrMainDtFrom\", //*\r\n");
      out.write("                        ifFormat : \"%d-%b-%Y %H:%M\",\r\n");
      out.write("                        showsTime : true,\r\n");
      out.write("                        button : \"cal_trigger1\", //*\r\n");
      out.write("                        step : 1\r\n");
      out.write("                    });\r\n");
      out.write("                </script>\r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    Calendar.setup({\r\n");
      out.write("                        inputField : \"txtRstrMainDtTo\", //*\r\n");
      out.write("                        ifFormat : \"%d-%b-%Y %H:%M\",\r\n");
      out.write("                        showsTime : true,\r\n");
      out.write("                        button : \"cal_trigger2\", //*\r\n");
      out.write("                        step : 1\r\n");
      out.write("                    });\r\n");
      out.write("                </script>\r\n");
      out.write("                <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\">&nbsp;</td>\r\n");
      out.write("                <td class=\"field\" >\r\n");
      out.write("                    <input type=\"submit\" name=\"cmdSearch\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                </td>\r\n");
      out.write("            </form>\r\n");
      out.write("\r\n");
      out.write("            <td>\r\n");
      out.write("                <form name=\"FrmRestore\" action=\"#\">\r\n");
      out.write("                    <span style=\"float: right\">\r\n");
      out.write("                        <input type=\"button\" name=\"prompt_button\" id=\"prompt_button\" class=\"button\" value=\"Restore\"\r\n");
      out.write("                               style=\"width: 85px;\"/>\r\n");
      out.write("                    </span>\r\n");
      out.write("                    <div id=\"dyncompo\" style=\"display:none;\">\r\n");
      out.write("                        ");

        java.util.ArrayList alClientslist = ClientConf.getClients();
        for (int i = 0; i < alClientslist.size(); i++) {
            out.write("<div>" + ((ClientConf) alClientslist.get(i)).getClientName() + "</div>");
        }
        alClientslist = null;
                        
      out.write("\r\n");
      out.write("                    </div>\r\n");
      out.write("                </form>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("    </table>\r\n");
      out.write("    </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Form Body Ends -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td style=\"height: 1px;\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Grid View Starts -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                        ");
      out.print(" - " + (al == null ? "" : al.size()) + " file(s) found");
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    ");
 if (al.size() == 0) {
            return;
        }
    
      out.write("\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                <tr class=\"grid_header\">\r\n");
      out.write("                    <td width=\"16\"></td>\r\n");
      out.write("                    <td width=\"100\">Restore Date</td>\r\n");
      out.write("                    <td width=\"20\">Time</td>\r\n");
      out.write("                    <td width=\"300\">File Name</td>\r\n");
      out.write("                    <td></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <form action=\"\">\r\n");
      out.write("                    ");

        nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
        if (nCurPage > nPageCount) {
            nCurPage = 1;
        }
        int nStartPos = ((nCurPage - 1) * nRowCount);
        for (int i = nStartPos;
                i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
                i++) {
            strVal = ((String) al.get(i));
                    
      out.write("\r\n");
      out.write("                    <tr class=\"\r\n");
      out.write("                        ");

                        if (i % 2 == 0) {
                            out.write("even_row");
                        } else {
                            out.write("odd_row");
                        }
                        
      out.write("\r\n");
      out.write("                        \">\r\n");
      out.write("                        <td width=\"16\" align=\"center\" >\r\n");
      out.write("                            <a href=\"#\" onclick=\"popupSearchWnd('pgLogFile.jsp?LogFile=");
      out.print(strVal);
      out.write("',800,600)\">\r\n");
      out.write("                            <img src=\"../images/icons/edit.gif\" border='0'></a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"100\" align='center'>\r\n");
      out.write("                            ");

                        out.write(strVal.substring(6, 8) + "-" + new DateFormatSymbols().getShortMonths()[Integer.parseInt(strVal.substring(4, 6))] + "-" + strVal.substring(0, 4));
                            
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"20\" align='left' >\r\n");
      out.write("                            ");

                        out.write(strVal.substring(8, 10) + ":" + strVal.substring(10, 12));
                            
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td width=\"300\" align='center'>\r\n");
      out.write("                            ");
      out.print(strVal);
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </form>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("        <tr>\r\n");
      out.write("        </tr>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("    <!-- Pagination Starts -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <a href=\"pgRestoreMain.jsp?CurPage=1\">\r\n");
      out.write("                        <img src=\"../images/icons/page-first.gif\" border=\"0\"></a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                        <A href=\"pgRestoreMain.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                        <img src=\"../images/icons/page-prev.gif\" border=\"0\"></A>\r\n");
      out.write("                        ");
 } else {
      out.write("\r\n");
      out.write("                        <img src=\"../images/icons/page-prev.gif\" border=\"0\">\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                    <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                        <A href=\"pgRestoreMain.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                        <img src=\"../images/icons/page-next.gif\" border=\"0\"></A>\r\n");
      out.write("                        ");
 } else {
      out.write("\r\n");
      out.write("                        <img src=\"../images/icons/page-next.gif\" border=\"0\">\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <a href=\"pgRestoreMain.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\" >\r\n");
      out.write("                        <img src=\"../images/icons/page-last.gif\" border=\"0\"></a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td width=\"100%\"></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Pagination Ends -->\r\n");
      out.write("    <tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td style=\"height: 10px;\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    </table>\r\n");
      out.write("    <div style=\"height: 25px;\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
