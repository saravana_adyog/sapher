package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.BackupSchedule;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import java.text.SimpleDateFormat;
import java.sql.Date;

public final class pgSavBkpSch_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CPL7.3";
    private String strUN = "";
    private String strPW = "";
    private String saveFlag = "";
    SimpleDateFormat sdf;
    private Date dtStart;
    private Date dtEnd;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!--\n");
      out.write("Form Id : CPL7.3\n");
      out.write("Date    : 14-02-2009\n");
      out.write("Author  : Arun P. Jose\n");
      out.write("-->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title></title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        ");
      out.write("\n");
      out.write("\n");
      out.write("        ");

        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            saveFlag = ((String) request.getParameter("saveFlag"));

        } catch (TimeoutException toe) {
        
      out.write("\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\n");
      out.write("        </script>\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            e.printStackTrace();
        
      out.write("\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("            document.location.replace(\"pgScheduleList.jsp?SavStatus=1\");\n");
      out.write("        </script>\n");
      out.write("        ");

            return;
        }
        String repeat_interval = "";
        String days = "";
        if (!((String) request.getParameter("txtInterval")).equals("")) {
            repeat_interval = "freq=" + (String) request.getParameter("cmbFrequency") + ";" + "interval=" + (String) request.getParameter("txtInterval") + ";";
        }
        if (request.getParameter("chkSunday") != null) {
            days += "SUN,";
        }
        if (request.getParameter("chkMonday") != null) {
            days += "MON,";
        }
        if (request.getParameter("chkTuesday") != null) {
            days += "TUE,";
        }
        if (request.getParameter("chkWednesday") != null) {
            days += "WED,";
        }
        if (request.getParameter("chkThursday") != null) {
            days += "THU,";
        }
        if (request.getParameter("chkFriday") != null) {
            days += "FRI,";
        }
        if (request.getParameter("chkSaturday") != null) {
            days += "SAT,";
        }
        if (!days.equals("")) {
            repeat_interval += "byday=" + days.substring(0, days.length() - 1) + ";";
        }

        sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        if (request.getParameter("txtstartDate") != null) {
            if (!((String) request.getParameter("txtstartDate")).equals("")) {
                dtStart = new Date(sdf.parse(request.getParameter("txtstartDate")).getTime());//
            }
        }
        if (request.getParameter("txtendDate") != null) {
            if (!((String) request.getParameter("txtendDate")).equals("")) {
                dtEnd = new Date(sdf.parse(request.getParameter("txtendDate")).getTime());//
            }
        }

        try {
            System.out.print("AV> " + dtStart + ", " + dtEnd);
            BackupSchedule.saveBkpSchedule(strUN, strPW, saveFlag,
                    (String) request.getParameter("txtjobName"), (String) request.getParameter("cmbClient"),
                    dtStart,
                    dtEnd,
                    repeat_interval);

            pageContext.forward("pgScheduleList.jsp?SavStatus=0");
        } catch (DBConnectionException ex) {
            ex.printStackTrace();
            pageContext.forward("pgScheduleList.jsp?SavStatus=1");
        } catch (GenericException ex) {
            ex.printStackTrace();
            pageContext.forward("pgScheduleList.jsp?SavStatus=1&Msg=" + ex.getMessage());
        }
        
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
