package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.ClientConf;
import bl.sapher.general.GenConf;

public final class pgMedDraUploadLanding_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=windows-1252");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <title></title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <script src=\"../libjs/jquery/jquery.js\" type=\"text/javascript\"></script>\r\n");
      out.write("        <script src=\"../libjs/jquery/query.ui.draggable.js\" type=\"text/javascript\"></script>\r\n");
      out.write("        <!-- Core files -->\r\n");
      out.write("        <script src=\"../libjs/jquery/AlertBox/jquery.alerts.js\" type=\"text/javascript\"></script>\r\n");
      out.write("        <link href=\"../libjs/jquery/AlertBox/jquery.alerts.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 15px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">MedDRA data updation</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <form name=\"FrmCMedDRAUpdate\" METHOD=\"POST\" ACTION=\"pgMedDraUpload.jsp\" >\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <span>\r\n");
      out.write("                            Client&nbsp;&nbsp;&nbsp;&nbsp;\r\n");
      out.write("                        </span>\r\n");
      out.write("                        <span>\r\n");
      out.write("                            <select name=\"cmbClientName\" id=\"cmbClientName\">\r\n");
      out.write("                                ");

        java.util.ArrayList alClientslist = ClientConf.getClients();
        for (int i = 0; i < alClientslist.size(); i++) {
            out.write("<option>" + ((ClientConf) alClientslist.get(i)).getClientName() + "</option>");
        }
        out.write("<option>All Clients</option>");
        alClientslist = null;
                                
      out.write("\r\n");
      out.write("                            </select>\r\n");
      out.write("                        </span>\r\n");
      out.write("                        <span style=\"float: right\">\r\n");
      out.write("                            <input type=\"submit\" name=\"cmdUpdate\" id=\"cmdUpdate\" class=\"button\" value=\"Update\"\r\n");
      out.write("                                   style=\"width: 100px;\" />\r\n");
      out.write("                        </span>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </form>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("\r\n");
      out.write("    </script>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
