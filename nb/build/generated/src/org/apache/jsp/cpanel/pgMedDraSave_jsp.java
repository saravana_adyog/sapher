package org.apache.jsp.cpanel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.File;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.SimultaneousOpException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.ClientConf;
import bl.sapher.MedDRABrowser;
import bl.sapher.User;

public final class pgMedDraSave_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=windows-1252");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
      out.write("\"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\r\n");
      out.write("        <title></title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");

        String FILE_REPOSITORY_PATH = "";
        String strClient = "";

        String strUN = "";
        String strPW = "";
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        try {
            strUN = ((String) session.getAttribute("cpanelUser"));
            strPW = ((String) session.getAttribute("cpanelPwd"));

            if (strUN == null || strPW == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            if (request.getParameter("Client") != null) {
                strClient = (String) request.getParameter("Client");
            } else {
                strClient = (String) session.getAttribute("MedDra_Client");
            }

        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSavDocs.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (!strClient.equalsIgnoreCase("all clients")) { /* Case of a specific client */
            // Copy the MedDRA files to the client specific path
            FILE_REPOSITORY_PATH = ClientConf.getByClientName(strClient).getMedDraUpdatePath();
            try {
                // upload the .asc files to the specific location of the DB server
                MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).uploadMedDraFiles(request, FILE_REPOSITORY_PATH);

                // execute the procedure on external tables
                MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).updateDB(strUN, strPW, strClient);

                pageContext.forward("content.jsp?Status=4");
            } catch (Throwable error) {
                pageContext.forward("content.jsp?Status=5");
                error.printStackTrace();
                throw error;
            }
        } else { /* Case of all clients */
            ArrayList<ClientConf> al = ClientConf.getClients();
            ClientConf cc = null;
            FILE_REPOSITORY_PATH = "";
            for (int i = 0; i < al.size(); i++) {
                cc = al.get(i);

                FILE_REPOSITORY_PATH = cc.getMedDraUpdatePath();

                try {
                    // upload the .asc files to the specific location of the DB server
                    MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).uploadMedDraFiles(request, FILE_REPOSITORY_PATH);

                    // execute the procedure on external tables
                    MedDRABrowser.getInstance((String) session.getAttribute("LogFileName")).updateDB(strUN, strPW, cc.getClientName());

                    pageContext.forward("content.jsp?Status=4");
                } catch (SimultaneousOpException soe) {
                    pageContext.forward("content.jsp?Status=7");
                    soe.printStackTrace();
                } catch (GenericException e) {
                    pageContext.forward("content.jsp?Status=5");
                    e.printStackTrace();
                }
            }
        }
        
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
