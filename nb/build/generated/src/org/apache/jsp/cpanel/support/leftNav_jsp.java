package org.apache.jsp.cpanel.support;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class leftNav_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : NAV1.3\r\n");
      out.write("Author  : Ratheesh\r\n");
      out.write("-->\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Sapher</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <script src=\"../../libjs/utils.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!-- For Left Manu Starts -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"../../libjs/resources/css/ext-all.css\" />\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../../libjs/adapter/ext/ext-base.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../../libjs/ext-all.js\"></script>\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"../../css/master.css\";</style>\r\n");
      out.write("        <script>\r\n");
      out.write("            <!--\r\n");
      out.write("            Ext.onReady(function(){\r\n");
      out.write("                var ce = new Ext.Panel({\r\n");
      out.write("                    frame:true,\r\n");
      out.write("                    title: 'Control Panel',\r\n");
      out.write("                    collapsible:true,\r\n");
      out.write("                    renderTo: _el(\"panel-content\"),\r\n");
      out.write("                    width:175,\r\n");
      out.write("                    contentEl:'cPanel',\r\n");
      out.write("                    titleCollapse: true,\r\n");
      out.write("                    collapsed: true\r\n");
      out.write("                });\r\n");
      out.write("            });\r\n");
      out.write("            //-->\r\n");
      out.write("        </script>\r\n");
      out.write("        <!-- For Left Manu Ends -->\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        <div style=\"height: 100%; width:10px; z-index: 3000; position: absolute; vetical-align: middle; display: none;\" id=\"showLeftFramePnael\">\r\n");
      out.write("            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"100%\">\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td width=\"100%\" align=\"center\" valign=\"middle\" background=\"../../images/letpanel/show-bg.gif\"><a onclick=\"mostrarMenu('showLeftFramePnael');return false;\" href=\"#\"><img src=\"../../images/letpanel/show.gif\" border=\"0\"  ></a></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("        <ul id=\"cPanel\" class=\"x-hidden\">\r\n");
      out.write("            <li>\r\n");
      out.write("                <img src=\"../../images/s.gif\" style=\"background-image:url(../../images/icon-by-date.gif);\"/>\r\n");
      out.write("                <a id=\"group-date\" href=\"../pgBackupList.jsp\" target=\"contentFrame\">Backup</a>\r\n");
      out.write("            </li>\r\n");
      out.write("            <li>\r\n");
      out.write("                <img src=\"../../images/s.gif\" style=\"background-image:url(../../images/icon-by-date.gif);\"/>\r\n");
      out.write("                <a id=\"group-date\" href=\"../pgScheduleList.jsp\" target=\"contentFrame\">Backup Schedule</a>\r\n");
      out.write("            </li>\r\n");
      out.write("            <li>\r\n");
      out.write("                <img src=\"../../images/s.gif\" style=\"background-image:url(../../images/icon-by-date.gif);\"/>\r\n");
      out.write("                <a id=\"group-date\" href=\"../pgRestoreMain.jsp\" target=\"contentFrame\">Restore</a>\r\n");
      out.write("            </li>\r\n");
      out.write("            <li>\r\n");
      out.write("                <img src=\"../../images/s.gif\" style=\"background-image:url(../../images/icon-by-date.gif);\"/>\r\n");
      out.write("                <a id=\"group-date\" href=\"../pgClearDBLanding.jsp\" target=\"contentFrame\">Clear Database</a>\r\n");
      out.write("            </li>\r\n");
      out.write("            <li>\r\n");
      out.write("                <img src=\"../../images/s.gif\" style=\"background-image:url(../../images/icon-by-date.gif);\"/>\r\n");
      out.write("                <a id=\"group-date\" href=\"../pgDelCaseLanding.jsp\" target=\"contentFrame\">Delete All Cases</a>\r\n");
      out.write("            </li>\r\n");
      out.write("            <li>\r\n");
      out.write("                <img src=\"../../images/s.gif\" style=\"background-image:url(../../images/icon-by-date.gif);\"/>\r\n");
      out.write("                <a id=\"group-date\" href=\"../pgMedDraUploadLanding.jsp\" target=\"contentFrame\">MedDRA Files Update</a>\r\n");
      out.write("            </li>\r\n");
      out.write("        </ul>\r\n");
      out.write("\r\n");
      out.write("        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" id=\"left_menu_panel\" height=\"100%\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td align=\"right\" valign=\"top\" style=\"padding-top: 15px\">\r\n");
      out.write("                    <!-- Left Menu Box.1 Starts -->\r\n");
      out.write("                    <table width=\"99%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" height=\"100%\">\r\n");
      out.write("                        <!-- Left Menux Box.1 Header Starts -->\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td height=\"26\"  background=\"../../images/letpanel/nav-header-bg.gif\">\r\n");
      out.write("                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n");
      out.write("                                    <tr>\r\n");
      out.write("                                        <td width=\"7\"><img src=\"../../images/letpanel/header-left-end.gif\"></td>\r\n");
      out.write("                                        <td>\r\n");
      out.write("                                            <div style=\"float:left;\" id=\"panel-head\">NAVIGATION</div>\r\n");
      out.write("                                            <div style=\"float:left\"><img src=\"../../images/letpanel/nav-header-curve.gif\"></div>\r\n");
      out.write("                                        </td>\r\n");
      out.write("                                        <td width=\"7\"><img src=\"../../images/letpanel/header-right-end.gif\"></td>\r\n");
      out.write("                                    </tr>\r\n");
      out.write("                                </table>\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <!-- Left Menux Box.1 Header Ends -->\r\n");
      out.write("          <!-- Left Menux Box.1 Content Area Starts -->\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td id=\"panel-content\" valign=\"top\" align=\"center\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <!-- Left Menux Box.1 Content Area Ends -->\r\n");
      out.write("                    </table>\r\n");
      out.write("                    <!-- Left Menu Box.1 Ends -->\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"5\" align=\"center\" valign=\"middle\"><a onclick=\"mostrarMenu('showLeftFramePnael');return false;\" href=\"#\"><img src=\"../../images/letpanel/hide.gif\" border=\"0\"  ></a></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
