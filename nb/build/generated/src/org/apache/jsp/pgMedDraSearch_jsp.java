package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.MedDRABrowser;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgMedDraSearch_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CDL24"; // Body System code
    private String strSearchString = "";
    private String strSoc = "0";
    private String strHlgt = "0";
    private String strHlt = "0";
    private String strPt = "0";
    private String strLlt = "0";
    String[] strVals = null;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 100;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>MedDRA code - [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("    <script>var SAPHER_PARENT_NAME = \"\";</script>\r\n");
      out.write("    <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/tab-view.css\" type=\"text/css\" media=\"screen\">\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/tab/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/tab/tab-view.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        function validateThisPage() {\r\n");
      out.write("            return true;\r\n");
      out.write("        }\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgMedDRASearch.jsp");
        boolean resetSession = false;

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("</script>\r\n");

        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp: V=" + bView);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("</script>\r\n");

            return;
        }

        if (!bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgMedDraSearch.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }

        if (request.getParameter("txtSrchKey") == null) {
            if (session.getAttribute("txtSrchKey") == null) {
                strSearchString = "";
            } else {
                strSearchString = (String) session.getAttribute("txtSrchKey");
            }
        } else {
            strSearchString = request.getParameter("txtSrchKey");
        }
        session.setAttribute("txtSrchKey", strSearchString);
        
        if (request.getParameter("chkSoc") == null) {
            if (session.getAttribute("chkSoc") == null) {
                strSoc = "0";
            } else {
                strSoc = (String)session.getAttribute("chkSoc");
            }
        } else {
            strSoc = "1";
        }
        session.setAttribute("chkSoc", strSoc);

        if (request.getParameter("chkHlgt") == null) {
            if (session.getAttribute("chkHlgt") == null) {
                strHlgt = "0";
            } else {
                strHlgt = (String)session.getAttribute("chkHlgt");
            }
        } else {
            strHlgt = "1";
        }
        session.setAttribute("chkHlgt", strHlgt);

        if (request.getParameter("chkHlt") == null) {
            if (session.getAttribute("chkHlt") == null) {
                strHlt = "0";
            } else {
                strHlt = (String)session.getAttribute("chkHlt");
            }
        } else {
            strHlt = "1";
        }
        session.setAttribute("chkHlt", strHlt);

        if (request.getParameter("chkPt") == null) {
            if (session.getAttribute("chkPt") == null) {
                strPt = "0";
            } else {
                strPt = (String)session.getAttribute("chkPt");
            }
        } else {
            strPt = "1";
        }
        session.setAttribute("chkPt", strPt);
        
        if (request.getParameter("chkLlt") == null) {
            if (session.getAttribute("chkLlt") == null) {
                strLlt = "0";
            } else {
                strLlt = (String)session.getAttribute("chkLlt");
            }
        } else {
            strLlt = "1";
        }
        session.setAttribute("chkLlt", strLlt);

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + strSearchString + "," + strSoc + strHlgt + strHlt + strPt + strLlt);
        System.out.println("AV> Loading list with val:" + strSearchString + "," + strSoc + strHlgt + strHlt + strPt + strLlt);
        al = MedDRABrowser.search(Constants.SEARCH_STRICT, (String) session.getAttribute("Company"),
                strSoc + strHlgt + strHlt + strPt + strLlt, strSearchString);

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }


      out.write("\r\n");
      out.write("\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("<table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("<tr>\r\n");
      out.write("    <td id=\"formtitle\">MedDRA code search</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <form name=\"FrmMedDRA codeList\" METHOD=\"POST\" ACTION=\"pgMedDraSearch.jsp\" >\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!--\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"125\"><label for=\"txtMedDRA codeCode\">MedDRA code</label></td>\r\n");
      out.write("            <td class=\"field\">\r\n");
      out.write("                <input type=\"text\" name=\"txtMedDRA codeCode\" id=\"txtMedDRA codeCode\" size=\"3\" maxlength=\"2\" title=\"MedDRA code Code\"\r\n");
      out.write("                       value=\"");
      out.write("\">\r\n");
      out.write("            </td>\r\n");
      out.write("            <td width=\"500\" align=\"right\"><b>");
      out.write("</b></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\"><label for=\"cmbSrchType code\">Condition</label></td>\r\n");
      out.write("                <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                <span style=\"float: left\">\r\n");
      out.write("                    <SELECT id=\"cmbCondition\" NAME=\"cmbCondition\" class=\"comboclass\" style=\"width: 100px;\" title=\"Status\">\r\n");
      out.write("                        <option value=\"0\">Begins with</option>\r\n");
      out.write("                        <option value=\"1\">Contains</option>\r\n");
      out.write("                        <option value=\"2\">Ends with</option>\r\n");
      out.write("                    </SELECT>\r\n");
      out.write("                </span>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\"><label for=\"txtSrchKey\">MedDRA Description</label></td>\r\n");
      out.write("                <td class=\"field\">\r\n");
      out.write("                    <input type=\"text\" name=\"txtSrchKey\" id=\"txtSrchKey\" size=\"30\" maxlength=\"50\" title=\"Search Key\"\r\n");
      out.write("                           value=\"");
      out.print(strSearchString);
      out.write("\">\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\"><label for=\"chkValid\">Search in</label></td>\r\n");
      out.write("                <td class=\"field\">\r\n");
      out.write("                    <table border=\"0\">\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td>SOC</td>\r\n");
      out.write("                            <td><input type=\"checkbox\" name=\"chkSoc\" id=\"chkSoc\" title=\"SOC\" class=\"chkbox\"\r\n");
      out.write("                                           ");
if (strSoc.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }
      out.write("</td>\r\n");
      out.write("                            <td>HLGT</td>\r\n");
      out.write("                            <td><input type=\"checkbox\" name=\"chkHlgt\" id=\"chkHlgt\" title=\"HLGT\" class=\"chkbox\"\r\n");
      out.write("                                           ");
if (strHlgt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }
      out.write("></td>\r\n");
      out.write("                            <td>HLT</td>\r\n");
      out.write("                            <td><input type=\"checkbox\" name=\"chkHlt\" id=\"chkHlt\" title=\"HLT\" class=\"chkbox\"\r\n");
      out.write("                                           ");
if (strHlt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }
      out.write("></td>\r\n");
      out.write("                            <td>PT</td>\r\n");
      out.write("                            <td><input type=\"checkbox\" name=\"chkPt\" id=\"chkPt\" title=\"PT\" class=\"chkbox\"\r\n");
      out.write("                                           ");
if (strPt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }
      out.write("></td>\r\n");
      out.write("                            <td>LLT</td>\r\n");
      out.write("                            <td><input type=\"checkbox\" name=\"chkLlt\" id=\"chkLlt\" title=\"LLT\" class=\"chkbox\"\r\n");
      out.write("                                           ");
if (strLlt.equalsIgnoreCase("1")) {
            out.write(" checked ");
        }
      out.write("></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("            <td>\r\n");
      out.write("                <td colspan=\"2\">\r\n");
      out.write("                    <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\"\r\n");
      out.write("                           onclick=\"return validateThisPage();\">\r\n");
      out.write("                    <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                           onclick=\"location.replace('pgNavigate.jsp?Cancellation=1&Target=pgMedDraSearch.jsp');\"/>\r\n");
      out.write("                </td>\r\n");
      out.write("            </td>\r\n");
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("    </form>\r\n");
      out.write("\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Ends -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 1px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Grid View Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                    ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
 if (al.size() != 0) {
      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("    <!--___________________________________-->\r\n");
      out.write("\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("        <tr class=\"grid_header\">\r\n");
      out.write("            <td width=\"16\"></td>\r\n");
      out.write("\r\n");
      out.write("            <td width=\"100\">Body System Number</td>\r\n");
      out.write("            <td>Body System (SOC)</td>\r\n");
      out.write("            <td width=\"100\">Status</td>\r\n");
      out.write("            <td></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        ");

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         strVals = ((String) al.get(i)).split("\\|");
        
      out.write("\r\n");
      out.write("        <tr class=\"\r\n");
      out.write("            ");

            if (i % 2 == 0) {
                out.write("even_row");
            } else {
                out.write("odd_row");
            }
            
      out.write("\r\n");
      out.write("            \">\r\n");
      out.write("\r\n");
      out.write("        ");
switch (Integer.parseInt(strVals[0])) {
                case 1:
      out.write("\r\n");
      out.write("        <td width=\"16\" align=\"center\" >\r\n");
      out.write("            <a href=\"pgMedDraSoc.jsp?HiLit=");
      out.print(strVals[11]);
      out.write("\">\r\n");
      out.write("                <img src=\"images/icons/16x16-SOC.gif\" title=\"View details of the entry with ID = ");
      out.print(strVals[11]);
      out.write("\" border='0'>\r\n");
      out.write("            </a>\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("            ");
      out.print((strVals[11].equalsIgnoreCase("null")?"":strVals[11]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td align='left'>\r\n");
      out.write("            ");
      out.print((strVals[12].equalsIgnoreCase("null")?"":strVals[12]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td></td>\r\n");
      out.write("        ");
break;
    case 2:
      out.write("\r\n");
      out.write("        <td width=\"16\" align=\"center\" >\r\n");
      out.write("            <a href=\"pgMedDraHlgt.jsp?HiLit=");
      out.print(strVals[11]);
      out.write("&SOCID=");
      out.print(strVals[1]);
      out.write("&SOC=");
      out.print(strVals[2]);
      out.write("\">\r\n");
      out.write("                <img src=\"images/icons/16x16-HLGT.gif\" title=\"View details of the entry with ID = ");
      out.print(strVals[11]);
      out.write("\" border='0'>\r\n");
      out.write("            </a>\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("            ");
      out.print((strVals[11].equalsIgnoreCase("null")?"":strVals[11]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td align='left'>\r\n");
      out.write("            ");
      out.print((strVals[12].equalsIgnoreCase("null")?"":strVals[12]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td></td>\r\n");
      out.write("        ");
break;
    case 3:
      out.write("\r\n");
      out.write("        <td width=\"16\" align=\"center\" >\r\n");
      out.write("            <a href=\"pgMedDraHlt.jsp?HiLit=");
      out.print(strVals[11]);
      out.write("&SOCID=");
      out.print(strVals[1]);
      out.write("&SOC=");
      out.print(strVals[2]);
      out.write("&HLGTID=");
      out.print(strVals[3]);
      out.write("&HLGT=");
      out.print(strVals[4]);
      out.write("\">\r\n");
      out.write("                <img src=\"images/icons/16x16-HLT.gif\" title=\"View details of the entry with ID = ");
      out.print(strVals[11]);
      out.write("\" border='0'>\r\n");
      out.write("            </a>\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("            ");
      out.print((strVals[11].equalsIgnoreCase("null")?"":strVals[11]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td align='left'>\r\n");
      out.write("            ");
      out.print((strVals[12].equalsIgnoreCase("null")?"":strVals[12]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td></td>\r\n");
      out.write("        ");
break;
    case 4:
      out.write("\r\n");
      out.write("        <td width=\"16\" align=\"center\" >\r\n");
      out.write("            <a href=\"pgMedDraPt.jsp?HiLit=");
      out.print(strVals[11]);
      out.write("&SOCID=");
      out.print(strVals[1]);
      out.write("&SOC=");
      out.print(strVals[2]);
      out.write("&HLGTID=");
      out.print(strVals[3]);
      out.write("&HLGT=");
      out.print(strVals[4]);
      out.write("&HLTID=");
      out.print(strVals[5]);
      out.write("&HLT=");
      out.print(strVals[6]);
      out.write("\">\r\n");
      out.write("                <img src=\"images/icons/16x16-PT-1.gif\" title=\"View details of the entry with ID = ");
      out.print(strVals[11]);
      out.write("\" border='0'>\r\n");
      out.write("            </a>\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("            ");
      out.print((strVals[11].equalsIgnoreCase("null")?"":strVals[11]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td align='left'>\r\n");
      out.write("            ");
      out.print((strVals[12].equalsIgnoreCase("null")?"":strVals[12]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td></td>\r\n");
      out.write("        ");
break;
    case 5:
      out.write("\r\n");
      out.write("        <td width=\"16\" align=\"center\" >\r\n");
      out.write("            <a href=\"pgMedDraLlt.jsp?HiLit=");
      out.print(strVals[11]);
      out.write("&SOCID=");
      out.print(strVals[1]);
      out.write("&SOC=");
      out.print(strVals[2]);
      out.write("&HLGTID=");
      out.print(strVals[3]);
      out.write("&HLGT=");
      out.print(strVals[4]);
      out.write("&HLTID=");
      out.print(strVals[5]);
      out.write("&HLT=");
      out.print(strVals[6]);
      out.write("&PTID=");
      out.print(strVals[7]);
      out.write("&PT=");
      out.print(strVals[8]);
      out.write("\">\r\n");
      out.write("                <img src=\"images/icons/16x16-LLT-1.gif\" title=\"View details of the entry with ID = ");
      out.print(strVals[11]);
      out.write("\" border='0'>\r\n");
      out.write("            </a>\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("            ");
      out.print((strVals[11].equalsIgnoreCase("null")?"":strVals[11]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td align='left'>\r\n");
      out.write("            ");
      out.print((strVals[12].equalsIgnoreCase("null")?"":strVals[12]));
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td width=\"100\" align='center'>\r\n");
      out.write("\r\n");
      out.write("        </td>\r\n");
      out.write("        <td></td>\r\n");
      out.write("        ");
break;
            }
      out.write("\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
 }
      out.write("\r\n");
      out.write("</table>\r\n");
      out.write("</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<tr>\r\n");
      out.write("    <!-- Pagination Starts -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <a href=\"pgMedDraSearch.jsp?CurPage=1\">\r\n");
      out.write("                        <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\"></a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                        <a href=\"pgMedDraSearch.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\"></a>\r\n");
      out.write("                        ");
 } else {
      out.write("\r\n");
      out.write("                        <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\">\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                    <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                        <a href=\"pgMedDraSearch.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\"></a>\r\n");
      out.write("                        ");
 } else {
      out.write("\r\n");
      out.write("                        <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\">\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <a href=\"pgMedDraSearch.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\"></a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td width=\"100%\"></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Pagination Ends -->\r\n");
      out.write("</tr>\r\n");
      out.write("<!--___________________________________-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<tr></tr>\r\n");
      out.write("<tr></tr>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("<tr>\r\n");
}
      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 10px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
