package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.CaseDetails;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;

public final class pgQueryTool_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "REP1";
    private int intRepNum = -1;
    private String strRepNum = "";
    private int nCaseSuspended = 0;
    private String strRepType = "";
    private String strProdCode = "";
    private String strGenericNm = "";
    private String strBrandNm = "";
    private String strReporter = "";
    private String strCountry = "";
    private String strTrial = "";
    private String strClasfCode = "";
    private String strDoseUnit = "";
    private String strDose = "";
    private String strIndication = "";
    private String strLLTCode = "";
    private String strOutcomeCode = "";
    private String strPTCode = "";
    private String strCoAssCode = "";
    private String strBodySysNum = "";
    private String strSexCode = "";
    private String strPhyAssCode = "";
    private String strManufCode = "";
    private String strPatNum = "";
    private String strRepNumStart = "";
    private String strRepNumEnd = "";
    private String strAgeStart = "";
    private String strAgeEnd = "";
    private String strTreatStart = "";
    private String strTreatEnd = "";
    private String strEvtStart = "";
    private String strEvtEnd = "";
    private String strTimeSpanStart = "";
    private String strTimeSpanEnd = "";
    private String strRepNumCondition = "=";
    private String strAgeCondition = "=";
    private String strDurTreatmentCondition = "=";
    private String strDurEvtCondition = "=";
    private String strTimeSpanCondition = "=";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    //private int nRowCount = 10;
    //private int nPageCount;
    //private int nCurPage;
    private Inventory inv = null;
    //private String strMessageInfo ="";

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : REP1\r\n");
      out.write("Date    : 09-01-2008.\r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Query Tool - [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <!--Calendar Library Includes.. S -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/pgQueryTool.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body onload=\"init();\">\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgQueryTool.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }

            if (request.getParameter("Cancellation") != null) {
                session.removeAttribute("DirtyFlag");
            }

            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp: A/E/V/D=" + bAdd + "/" + bEdit + "/" + bView + "/" + bDelete);
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQueryTool.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        String strSuspended = "";

        if (strSuspended.equals("Valid")) {
            nCaseSuspended = 0;
        } else if (strSuspended.equals("Suspended")) {
            nCaseSuspended = 1;
        } else if (strSuspended.equals("All")) {
            nCaseSuspended = -1;
        }

        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Query Tool</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form name=\"FrmQueryTool\" METHOD=\"POST\" ACTION=\"pgPrintQueryTool.jsp\">\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Value parameters</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"height: 10px;\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode\">Prod Details</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"9\">\r\n");
      out.write("\r\n");
      out.write("                            <input type=\"text\" id=\"txtProdCode\" name=\"txtProdCode\" size=\"70\" onkeyup=\"ajax_showOptions('txtProdCode_ID',this,'Product',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtProdCode_ID\" name=\"txtProdCode_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtReporter\">Reporter</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("\r\n");
      out.write("                            <input type=\"text\" id=\"txtReporter\" name=\"txtReporter\" size=\"30\" onkeyup=\"ajax_showOptions('txtReporter_ID',this,'ReportSource',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtReporter_ID\" name=\"txtReporter_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtCountry\">Country</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtCountry\" name=\"txtCountry\" size=\"30\" onkeyup=\"ajax_showOptions('txtCountry_ID',this,'Country',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtCountry_ID\" name=\"txtCountry_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtTrial\">Source/Trial ID</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtTrial\" name=\"txtTrial\" size=\"30\" onkeyup=\"ajax_showOptions('txtTrial_ID',this,'TrialNo',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtTrial_ID\" name=\"txtTrial_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtClasfCode\">Seriousness</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtClasfCode\" name=\"txtClasfCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtClasfCode_ID',this,'Seriousness',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtClasfCode_ID\" name=\"txtClasfCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtDoseUnit\">Dose Unit</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtDoseUnit\" name=\"txtDoseUnit\" size=\"30\" onkeyup=\"ajax_showOptions('txtDoseUnit_ID',this,'DoseUnits',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtDoseUnit_ID\" name=\"txtDoseUnit_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtDose\">Dose</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtDose\" id=\"txtDose\" size=\"30\" maxlength=\"20\" title=\"Dose\"\r\n");
      out.write("                                   clear=6 onkeydown=\"return ClearInput(event,6);\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtIndication\">Indication</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtIndication\" name=\"txtIndication\" size=\"30\" onkeyup=\"ajax_showOptions('txtIndication_ID',this,'IndicationTreatment',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtIndication_ID\" name=\"txtIndication_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtLLTCode\">Adverse Reaction - LLT</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtLLTCode\" name=\"txtLLTCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtLLTCode_ID',this,'AELLT',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtLLTCode_ID\" name=\"txtLLTCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtOutcomeCode\">Outcome</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtOutcomeCode\" name=\"txtOutcomeCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtOutcomeCode_ID\" name=\"txtOutcomeCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtPTCode\">Adverse Reaction - PT</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtPTCode\" name=\"txtPTCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtPTCode_ID',this,'AEPT',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtPTCode_ID\" name=\"txtPTCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtSexCode\">Sex</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtSexCode\" name=\"txtSexCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtSexCode_ID',this,'Sex',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtSexCode_ID\" name=\"txtSexCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtBodySysNum\">System Organ Class</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtBodySysNum\" name=\"txtBodySysNum\" size=\"30\" onkeyup=\"ajax_showOptions('txtBodySysNum_ID',this,'BodySys',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtBodySysNum_ID\" name=\"txtBodySysNum_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtCoAssCode\">Company Assessment</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtCoAssCode\" name=\"txtCoAssCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtCoAssCode_ID',this,'AssCausality',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtCoAssCode_ID\" name=\"txtCoAssCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\"><label for=\"txtPhyAssCode\">Physician Assessment</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtPhyAssCode\" name=\"txtPhyAssCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtPhyAssCode_ID',this,'AssCausality',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtPhyAssCode_ID\" name=\"txtPhyAssCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"txtManufCode\">Manufacturer Code</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtManufCode\" name=\"txtManufCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtManufCode_ID',this,'Manufacturer',event)\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"txtManufCode_ID\" name=\"txtManufCode_ID\" value=\"\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabelR\" ><label for=\"txtPatNum\">Patient Num/Center</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtPatNum\" id=\"txtPatNum\" size=\"30\" maxlength=\"20\" title=\"Patient Num/Center\"\r\n");
      out.write("                                   clear=16 readonly onkeydown=\"return ClearInput(event,16);\">\r\n");
      out.write("                            <a href=\"#\" onclick=\"popupWnd('spopup','no','pgSearchPatientNum.jsp?FormId=");
      out.print(FORM_ID);
      out.write("', 280, 475)\">\r\n");
      out.write("                                <img src=\"images/window.gif\" alt=\"Select\" title=\"Select Patient Num/Center\" border='0' name=\"imgSearch\">\r\n");
      out.write("                            </a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"cmbSuspendedCase\">Validity</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                            <span style=\"float: left\">\r\n");
      out.write("                                <select id=\"cmbSuspendedCase\" NAME=\"cmbSuspendedCase\" class=\"comboclass\" style=\"width: 100px;\" title=\"Status\">\r\n");
      out.write("                                    <option ");
      out.print((strSuspended.equals("All") ? "SELECTED" : ""));
      out.write(">All</option>\r\n");
      out.write("                                    <option ");
      out.print((strSuspended.equals("Valid") ? "SELECTED" : ""));
      out.write(">Active</option>\r\n");
      out.write("                                    <option ");
      out.print((strSuspended.equals("Suspended") ? "SELECTED" : ""));
      out.write(">Inactive</option>\r\n");
      out.write("                                </select>\r\n");
      out.write("                            </span>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"height: 5px;\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Range parameters</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"height: 10px;\"></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <!--tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label>Report Num</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                            <select id=\"cmbRepNumCondition\" name=\"cmbRepNumCondition\" class=\"comboclass\" style=\"width: 60px;\" title=\"Report Num Criteria\" onchange=\"setupCtrlsWithCombo('cmbRepNumCondition', 'txtRepNumStart', 'txtRepNumEnd','','');\" ONFOCUS=\"setupCtrlsWithCombo('cmbRepNumCondition', 'txtRepNumStart', 'txtRepNumEnd','','');\">\r\n");
      out.write("                                <option <!=(strRepNumCondition.equals(\">=\") ? \"SELECTED\" : \"\")%>>&gt;=</option>\r\n");
      out.write("                                <option <!=(strRepNumCondition.equals(\"<=\") ? \"SELECTED\" : \"\")%>>&lt;=</option>\r\n");
      out.write("                                <option <!=(strRepNumCondition.equals(\"Range\") ? \"SELECTED\" : \"\")%>>Range</option>\r\n");
      out.write("                            </select>\r\n");
      out.write("                            <input type=\"text\" name=\"txtRepNumStart\" id=\"txtRepNumStart\" size=\"28\" maxlength=\"9\" title=\"Report Number\"\r\n");
      out.write("                                   readonly/>\r\n");
      out.write("                            <a href=\"#\" onclick=\"if(!document.getElementById('txtRepNumStart').disabled) {click_root='S';popupWnd('spopup','no','pgSearchRepNum.jsp?FormId=");
      out.print(FORM_ID);
      out.write("', 280, 475);}\">\r\n");
      out.write("                                <img src=\"images/window.gif\" alt=\"Select\" title=\"Select Report Number\" border='0' name=\"imgSearch\">\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <label for=\"txtRepNumEnd\">To</label>\r\n");
      out.write("                            <input type=\"text\" name=\"txtRepNumEnd\" id=\"txtRepNumEnd\" size=\"29\" maxlength=\"9\" title=\"Report Number\"\r\n");
      out.write("                                   readonly />\r\n");
      out.write("                            <a href=\"#\" onclick=\"if(!document.getElementById('txtRepNumEnd').disabled) {click_root='E';popupWnd('spopup','no','pgSearchRepNum.jsp?FormId=");
      out.print(FORM_ID);
      out.write("', 280, 475);}\">\r\n");
      out.write("                                <img src=\"images/window.gif\" alt=\"Select\" title=\"Select Report Number\" border='0' name=\"imgSearch\">\r\n");
      out.write("                            </a>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr-->\r\n");
      out.write("                    <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label>Time span</label></td>\r\n");
      out.write("                    <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                        <select id=\"cmbTimeSpanCondition\" name=\"cmbTimeSpanCondition\" class=\"comboclass\" style=\"width: 60px;\" title=\"Time span Criteria\" onchange=\"setupCtrlsWithCombo('cmbTimeSpanCondition', 'txtTimeSpanStart', 'txtTimeSpanEnd','cal_trigger1','cal_trigger2');\" ONFOCUS=\"setupCtrlsWithCombo('cmbTimeSpanCondition', 'txtTimeSpanStart', 'txtTimeSpanEnd','cal_trigger1','cal_trigger2');\">\r\n");
      out.write("                            <option ");
      out.print((strTimeSpanCondition.equals(">=") ? "SELECTED" : ""));
      out.write(">>=</option>\r\n");
      out.write("                            <option ");
      out.print((strTimeSpanCondition.equals("<=") ? "SELECTED" : ""));
      out.write("><=</option>\r\n");
      out.write("                            <option ");
      out.print((strTimeSpanCondition.equals("Range") ? "SELECTED" : ""));
      out.write(">Range</option>\r\n");
      out.write("                        </select>\r\n");
      out.write("                        <input type=\"text\" name=\"txtTimeSpanStart\" id=\"txtTimeSpanStart\" size=\"28\" maxlength=\"11\" title=\"Time span Start\"\r\n");
      out.write("                               clear=17 value=\"");
 
      out.write("\" readonly onkeydown=\"ClearInput(event,17);\">\r\n");
      out.write("                        <img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select\" border='0' name=\"imgCal\" id=\"cal_trigger1\" style=\"cursor:pointer;visibility:hidden;\">\r\n");
      out.write("                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtTimeSpanStart\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_trigger1\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                        </script>\r\n");
      out.write("                        <label>To</label>\r\n");
      out.write("                        <input type=\"text\" name=\"txtTimeSpanEnd\" id=\"txtTimeSpanEnd\" size=\"29\" maxlength=\"11\" title=\"Time span End\"\r\n");
      out.write("                               value=\"");
 
      out.write("\" readonly\r\n");
      out.write("                               clear=18 onkeydown=\"ClearInput(event,18);\">\r\n");
      out.write("                        <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtTimeSpanEnd\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_trigger2\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                        </script>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\"><label>Patient Age</label></td>\r\n");
      out.write("                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                    <select id=\"cmbAgeCondition\" NAME=\"cmbAgeCondition\" class=\"comboclass\" style=\"width: 60px;\" title=\"Age Criteria\" onchange=\"setupCtrlsWithCombo('cmbAgeCondition', 'txtAgeStart', 'txtAgeEnd','','');\" ONFOCUS=\"setupCtrlsWithCombo('cmbAgeCondition', 'txtAgeStart', 'txtAgeEnd','','');\">\r\n");
      out.write("                        <option ");
      out.print((strAgeCondition.equals(">=") ? "SELECTED" : ""));
      out.write(">>=</option>\r\n");
      out.write("                        <option ");
      out.print((strAgeCondition.equals("<=") ? "SELECTED" : ""));
      out.write("><=</option>\r\n");
      out.write("                        <option ");
      out.print((strAgeCondition.equals("Range") ? "SELECTED" : ""));
      out.write(">Range</option>\r\n");
      out.write("                    </select>\r\n");
      out.write("                    <input type=\"text\" name=\"txtAgeStart\" id=\"txtAgeStart\" size=\"6\" maxlength=\"5\" title=\"Age\"/>\r\n");
      out.write("                    <label for=\"txtProdCode\">To</label>\r\n");
      out.write("                    <input type=\"text\" name=\"txtAgeEnd\" id=\"txtAgeEnd\" size=\"6\" maxlength=\"5\" title=\"Age\"/>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\"><label>Duration for Treatment</label></td>\r\n");
      out.write("                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                    <select id=\"cmbDurTreatmentCondition\" NAME=\"cmbDurTreatmentCondition\" class=\"comboclass\" title=\"Duration Criteria\" style=\"width: 60px;\" onchange=\"setupCtrlsWithCombo('cmbDurTreatmentCondition', 'txtTreatStart', 'txtTreatEnd','','');\" ONFOCUS=\"setupCtrlsWithCombo('cmbDurTreatmentCondition', 'txtTreatStart', 'txtTreatEnd','','');\">\r\n");
      out.write("                        <option ");
      out.print((strDurTreatmentCondition.equals(">=") ? "SELECTED" : ""));
      out.write(">>=</option>\r\n");
      out.write("                        <option ");
      out.print((strDurTreatmentCondition.equals("<=") ? "SELECTED" : ""));
      out.write("><=</option>\r\n");
      out.write("                        <option ");
      out.print((strDurTreatmentCondition.equals("Range") ? "SELECTED" : ""));
      out.write(">Range</option>\r\n");
      out.write("                    </select>\r\n");
      out.write("                    <input type=\"text\" name=\"txtTreatStart\" id=\"txtTreatStart\" size=\"6\" maxlength=\"5\" title=\"Duration\"/>\r\n");
      out.write("                    <label for=\"txtProdCode\">To</label>\r\n");
      out.write("                    <input type=\"text\" name=\"txtTreatEnd\" id=\"txtTreatEnd\" size=\"6\" maxlength=\"5\" title=\"Duration\"\r\n");
      out.write("                           readonly>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"fLabel\" width=\"125\"><label>Duration of Event</label></td>\r\n");
      out.write("                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                    <select id=\"cmbDurEvtCondition\" NAME=\"cmbDurEvtCondition\" class=\"comboclass\" title=\"Duration Criteria\" style=\"width: 60px;\" onchange=\"setupCtrlsWithCombo('cmbDurEvtCondition', 'txtEvtStart', 'txtEvtEnd','','');\" ONFOCUS=\"setupCtrlsWithCombo('cmbDurEvtCondition', 'txtEvtStart', 'txtEvtEnd','','');\">\r\n");
      out.write("                        <option ");
      out.print((strDurEvtCondition.equals(">=") ? "SELECTED" : ""));
      out.write(">>=</option>\r\n");
      out.write("                        <option ");
      out.print((strDurEvtCondition.equals("<=") ? "SELECTED" : ""));
      out.write("><=</option>\r\n");
      out.write("                        <option ");
      out.print((strDurEvtCondition.equals("Range") ? "SELECTED" : ""));
      out.write(">Range</option>\r\n");
      out.write("                    </select>\r\n");
      out.write("                    <input type=\"text\" name=\"txtEvtStart\" id=\"txtEvtStart\" size=\"6\" maxlength=\"5\" title=\"Duration\"/>\r\n");
      out.write("                    <label for=\"txtProdCode\">To</label>\r\n");
      out.write("                    <input type=\"text\" name=\"txtEvtEnd\" id=\"txtEvtEnd\" size=\"6\" maxlength=\"5\" />\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <tr><td style=\"height: 5px;\"></td></tr>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td></td>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <input type=\"submit\" name=\"cmdGenerate\" class=\"button\" value=\"Generate\" title=\"Generate PDF report with given criteria\"\r\n");
      out.write("                           onclick=\"return validateInputs();\" style=\"width: 87px;\">\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        </td>\r\n");
      out.write("        </form>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!-- Form Body Ends -->\r\n");
      out.write("\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 10px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
