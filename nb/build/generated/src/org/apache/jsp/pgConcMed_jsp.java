package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import bl.sapher.Conc_Medication;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;

public final class pgConcMed_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    Conc_Medication aCM = null;
    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
    private String strRepNum = "";
    private String strProdCode = "";
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD3\r\n");
      out.write("Date    : 22-01-2008.\r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Concomitant Medication - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            var SAPHER_PARENT_NAME = \"pgConcMed\";\r\n");
      out.write("\r\n");
      out.write("            function validateCMFields(){\r\n");
      out.write("                if(document.FrmConcMed.txtProdCode.value == '') {\r\n");
      out.write("                    window.alert('Please provide Product code for concomitant medication.');\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function validate(){\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        aCM = new Conc_Medication((String) session.getAttribute("LogFileName"));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgConcMed.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }

        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; Invalid username/password.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            }
            if (!saveFlag.equals("I")) {
                if ((request.getParameter("RepNum") != null) && (request.getParameter("ProdCode") != null)) {
                    strRepNum = request.getParameter("RepNum");
                    strProdCode = request.getParameter("ProdCode");
                    aCM = new Conc_Medication((String) session.getAttribute("Company"), "y", Integer.parseInt(request.getParameter("RepNum")), request.getParameter("ProdCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                aCM = new Conc_Medication();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgConcMed.jsp; RecordNotFoundException:");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='700' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Concomitant Medication</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmConcMed\" name=\"FrmConcMed\" method=\"POST\" action=\"pgSavConcMed.jsp\"\r\n");
      out.write("                      onsubmit=\"return validate();\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Concomitant Medication</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtRepNum\">Rep Num</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtRepNum\" title=\"Report Number\"\r\n");
      out.write("                                                             id=\"txtRepNum\" size=\"15\" maxlength=\"9\"\r\n");
      out.write("                                                             ");
out.write(" value=\"" + strRepNum + "\"  readonly ");
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode\">Product Details</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <div id=\"divProdDynData\">\r\n");
      out.write("\r\n");
      out.write("                                        <input type=\"text\" id=\"txtProdCode\" name=\"txtProdCode\" size=\"57\" onKeyUp=\"ajax_showOptions('txtProdCode_ID',this,'Product',event)\"\r\n");
      out.write("                                               value=\"");
      out.print(((aCM.getBrand_Nm() == null && aCM.getGeneric_Nm() == null) ? "" : (aCM.getBrand_Nm() + " (" + aCM.getGeneric_Nm() + ")")));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"13\" id=\"txtProdCode_ID\" name=\"txtProdCode_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((aCM.getProduct_Code() == null ? "" : aCM.getProduct_Code()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtIndicationNum\">Indication Num</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("\r\n");
      out.write("                                    <input type=\"text\" id=\"txtIndicationNum\" name=\"txtIndicationNum\" size=\"30\" onKeyUp=\"ajax_showOptions('txtIndicationNum_ID',this,'IndicationTreatment',event)\"\r\n");
      out.write("                                           value=\"");
      out.print((aCM.getIndication() == null ? "" : aCM.getIndication()));
      out.write("\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"8\" id=\"txtIndicationNum_ID\" name=\"txtIndicationNum_ID\"\r\n");
      out.write("                                           value=\"");
      out.print((aCM.getIndication_Num() == null ? "" : aCM.getIndication_Num()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\">\r\n");
      out.write("                                    <label for=\"txtStartDate\">Start date</label>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtStartDate\" id=\"txtStartDate\" size=\"15\" maxlength=\"11\" title=\"Start date\"\r\n");
      out.write("                                                       clear=202 value=\"");
      out.print((aCM.getTreat_Start() == null ? "" : (sdf.format(aCM.getTreat_Start()))));
      out.write("\" readonly\r\n");
      out.write("                                                       ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 202);\" ");
        }
      out.write("\r\n");
      out.write("                                                   </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                ");

        if (!saveFlag.equals("V")) {
                                                
      out.write("\r\n");
      out.write("                                                <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" title=\"Select Start date\"></a>\r\n");
      out.write("                                                <script type=\"text/javascript\">\r\n");
      out.write("                                                    Calendar.setup({\r\n");
      out.write("                                                        inputField : \"txtStartDate\", //*\r\n");
      out.write("                                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                        showsTime : true,\r\n");
      out.write("                                                        button : \"cal_trigger1\", //*\r\n");
      out.write("                                                        step : 1\r\n");
      out.write("                                                    });\r\n");
      out.write("                                                </script>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\">\r\n");
      out.write("                                    <label for=\"txtEndDate\">End date</label>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtEndDate\" id=\"txtEndDate\" size=\"15\" maxlength=\"11\" title=\"End date\"\r\n");
      out.write("                                                       clear=203 value=\"");
      out.print((aCM.getTreat_Stop() == null ? "" : (sdf.format(aCM.getTreat_Stop()))));
      out.write("\" readonly\r\n");
      out.write("                                                       ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 203);\" ");
        }
      out.write("\r\n");
      out.write("                                                   </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                ");

        if (!saveFlag.equals("V")) {
                                                
      out.write("\r\n");
      out.write("                                                <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" title=\"Select End date\"></a>\r\n");
      out.write("                                                <script type=\"text/javascript\">\r\n");
      out.write("                                                    Calendar.setup({\r\n");
      out.write("                                                        inputField : \"txtEndDate\", //*\r\n");
      out.write("                                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                        showsTime : true,\r\n");
      out.write("                                                        button : \"cal_trigger2\", //*\r\n");
      out.write("                                                        step : 1\r\n");
      out.write("                                                    });\r\n");
      out.write("                                                </script>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSave\" onClick=\"return validateCMFields();\" class=\"");
      out.print((saveFlag.equals("V") ? "button disabled" : "button"));
      out.write("\" title=\"Save\" value=\"Save\" style=\"width: 60px;\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"SaveFlag\" name=\"SaveFlag\" value=\"");
      out.print(saveFlag);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"");
      out.print(strParent);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"RepNum\" name=\"RepNum\" value=\"");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"DiagNum\" name=\"DiagNum\" value=\"");
      out.print(strProdCode);
      out.write("\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdNew\" class=\"button\" value=\"Cancel\" title=\"Cancel the operation\" style=\"width: 60px;\"\r\n");
      out.write("                                           title=\"Cancel\" onclick=\"javascript:location.replace('");
      out.print("pgCaseSummary.jsp?RepNum=" + strRepNum);
      out.write("')\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
