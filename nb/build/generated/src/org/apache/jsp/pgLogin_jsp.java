package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.GenConf;
import bl.sapher.general.ClientConf;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class pgLogin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : USM3\r\n");
      out.write("Date    : 11-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Sapher</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\"> @import \"css/master.css\"; </style>\r\n");
      out.write("        <!--[if IE]> <style type=\"text/css\"> .login_form label{ float: none; }  </style> <![endif]-->\r\n");
      out.write("        <link rel=\"shortcut icon\" href=\"images/icons/sapher.ico\" type=\"image/vnd.microsoft.icon\" />\r\n");
      out.write("        <link rel=\"icon\" href=\"images/icons/sapher.ico\" type=\"image/vnd.microsoft.icon\" />\r\n");
      out.write("    </head>\r\n");
      out.write("    ");

        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        GenConf.setLogPath(application.getRealPath("/Reports/logs"));
        String strVer = "";
        String strMessage = "";
        String strCRight = "";
        GenConf gConf = new GenConf();
        session.setAttribute("LogFileName", "sapher_startup_" +
                new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US).format(Calendar.getInstance().getTime()));
        Logger.writeLog((String) session.getAttribute("LogFileName"), "pgLogin; RemoteIP: " + request.getRemoteAddr());

        strVer = gConf.getSysVersion();
        strCRight = gConf.getSysCRight();
       
        if (request.getParameter("LoginStatus") != null) {
            if (request.getParameter("LoginStatus").equals(Constants.LOGIN_STATUS_INVALID_UN_PW)) {
                strMessage = "Invalid username or password support/pg_login";
            } else if (request.getParameter("LoginStatus").equals("2")) {
                strMessage = "Account Locked testram, Please contact administrator";
            } else if (request.getParameter("LoginStatus").equals("3")) {
                strMessage = "User already logged in.";
            } else if (request.getParameter("LoginStatus").equals("4")) {
                strMessage = "Cannot Login! Unknown Reason";
            } else if (request.getParameter("LoginStatus").equals("5")) {
                strMessage = "Login Blocked, DB Maintenance in progress";
            } else if (request.getParameter("LoginStatus").equals("6")) {
                strMessage = "Session timed out. Login again.";
            } else if (request.getParameter("LoginStatus").equals("7")) {
                strMessage = "No more login allowed, as per the license(s).";
            }
        }

        if (request.getParameter("AppStabilityStatus") != null) {
            if (request.getParameter("AppStabilityStatus").equals(Constants.APP_STABILITY_STATUS_ILLEGAL_OP)) {
                strMessage = "Illegal operation. Log in again.";
            }
        }
    
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td align=\"center\" valign=\"middle\" height=\"100%\">\r\n");
      out.write("                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"login_table\">\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td rowspan=\"2\" align=\"left\" width=\"8\" class=\"bg_norepeat\" background=\"images/login/login-left.jpg\"></td>\r\n");
      out.write("                            <td valign=\"top\" align=\"left\"  height=\"158\" class=\"bg_norepeat\" background=\"images/login/loginpage-logo.jpg\">\r\n");
      out.write("                                <div id=\"version\"\r\n");
      out.write("                                     style=\"position: relative; font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#FFFFFF; left: 130px; top: 115px; width: 250px; \">\r\n");
      out.write("                                    ");
      out.print(strVer);
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td rowspan=\"2\" align=\"left\" width=\"8\" class=\"bg_norepeat\" background=\"images/login/login-right.jpg\"></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <form name=\"login\" method=\"post\" action=\"pgValidateLogin.jsp\">\r\n");
      out.write("                                <td align=\"right\" valign=\"top\" height=\"142\">\r\n");
      out.write("                                    <div class=\"error\" align=\"center\" style=\"padding: 5px 0px 2px 0px; \">");
      out.print(strMessage);
      out.write("</div>\r\n");
      out.write("                                    <div class=\"login_form\">\r\n");
      out.write("                                        <div><label for=\"user\">Company: </label>\r\n");
      out.write("                                            <select name=\"cmbCompany\" style=\"width:160px\">\r\n");
      out.write("                                                ");

        ClientConf.setXmlPath(application.getRealPath("/WEB-INF"));
        java.util.ArrayList al = ClientConf.getClients();
        for (int i = 0; i < al.size(); i++) {
            out.write("<option>" + ((ClientConf) al.get(i)).getClientName() + "</option>");
        }
      out.write("\r\n");
      out.write("                                            </select>\r\n");
      out.write("                                        </div>\r\n");
      out.write("\r\n");
      out.write("                                        <div><label for=\"user\">User Id: </label><input type=\"text\" name=\"txtUsername\" id=\"txtUsername\" class=\"txt\"></div>\r\n");
      out.write("                                        <div><label for=\"password\">Password: </label><input type=\"password\" name=\"txtPassword\" id=\"txtPassword\" class=\"txt\"></div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <div class=\"col2\" style=\"padding-left: 248px; \" align='left'><input type=\"submit\" style=\"width: 60px;\" value=\"Login\" class=\"button\" name=\"cmdLogin\"/></div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                    <div align=\"center\" class=\"copyright_1\" >");
      out.print(strCRight);
      out.write("</div>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
