package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.CaseDetails;
import bl.sapher.Adverse_Event;
import bl.sapher.RepType;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.Conc_Medication;
import bl.sapher.Product;
import bl.sapher.AEDiag;
import bl.sapher.AELLT;
import bl.sapher.LHA;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.ArrayList;

public final class pgCaseDetails_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD1\r\n");
      out.write("Date    : 17-01-2008, Thu.\r\n");
      out.write("Author  : Anoop Varma.\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
      out.write("\"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>Case details - [Sapher]</title>\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/tab-view.css\" type=\"text/css\" media=\"screen\">\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/tab/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/tab/tab-view.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/pgCaseDetails.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <script>var SAPHER_PARENT_NAME = \"pgCaseDetails\";</script>\r\n");
      out.write("\r\n");
      out.write("    <!-- Calendar Library Includes.. Start -->\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" >\r\n");
      out.write("    <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" >\r\n");
      out.write("    <!-- import the calendar script -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("    <!-- import the language module -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("    <!-- helper script that uses the calendar -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("    <!--Calendar Library Includes.. End -->\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\">\r\n");
      out.write("        input{height: 18px;}\r\n");
      out.write("    </style>\r\n");
      out.write("</head>\r\n");
      out.write("<body onload=\"setAllControls();\">\r\n");
      out.write("\r\n");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaseDetails.jsp");
        CaseDetails aCase = new CaseDetails((String) session.getAttribute("LogFileName"));
        final String FORM_ID = "CSD1";
        String strRepNum = "";
        String saveFlag = "";
        String strMessageInfo = "";
        String strParent = "";
        int nSuspended = 0;
        boolean bAdd;
        boolean bEdit;
        boolean bView;
        User currentUser = null;
        Inventory inv = null;
        SimpleDateFormat sdf = null;


        sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; Wrong username/password");
                pageContext.forward("pgLogin.jsp?LoginStatus=1");
                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; TimeoutException");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; User already logged in.");
            pageContext.forward("pgLogin.jsp?LoginStatus=1");
            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");

                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            }
            if (request.getParameter("SaveFlag") != null) {
                if (!request.getParameter("SaveFlag").equals("I")) {
                    aCase = new CaseDetails((String) session.getAttribute("Company"), "y", Integer.parseInt(request.getParameter("RepNum")));

                    strRepNum = request.getParameter("RepNum");
                    strMessageInfo = (aCase.getIs_Suspended() == 1 ? "SUSPENDED" : "VALID");
                    nSuspended = (aCase.getIs_Suspended() == 1 ? 1 : 0);
                } else { // Insertion mode
                    aCase = new CaseDetails();
                }
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaseDetails.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            return;
        }

      out.write("\r\n");
      out.write("\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("<table border='0' cellspacing='0' cellpadding='0' width='400' align=\"center\" id=\"formwindow\">\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td id=\"formtitle\">Case Detail</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <!-- Form Body Starts -->\r\n");
      out.write("    <tr>\r\n");
      out.write("        <form id=\"FrmCaseDetails\" name=\"FrmCaseDetails\" method=\"psot\" action=\"pgSavCase.jsp?Parent=1\"\r\n");
      out.write("              onsubmit=\"return validate();\">\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\"><label for=\"txtRepNum\">Rep Num</label></td>\r\n");
      out.write("                    <td class=\"field\"><input type=\"text\" name=\"txtRepNum\" title=\"Report Number\" style=\"border:0;font-weight:bold\"\r\n");
      out.write("                                                 id=\"txtRepNum\" size=\"15\" maxlength=\"10\"\r\n");
      out.write("                                                 ");

        if (saveFlag.equals("U")) {
            out.write(" value=\"" + strRepNum + "\"  readonly");
        } else if (saveFlag.equals("I")) {
            out.write(" value=\"\" readonly");
        }
                                                 
      out.write(" >\r\n");
      out.write("                    </td>\r\n");
      out.write("\r\n");
      out.write("                    <td class=\"fLabelR\"><label>Suspended</label></td>\r\n");
      out.write("                    <td class=\"field\"><input type=\"checkbox\" name=\"chkSuspended\" id=\"chkSuspended\" class=\"chkbox\" title=\"Active/Suspended\"\r\n");
      out.write("                                                 ");

        if (aCase.getIs_Suspended() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                 
      out.write(" >\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td  colspan=\"9\">\r\n");
      out.write("                        <div id=\"casedetailstab\">\r\n");
      out.write("                        <!-- Tab 1.1 (1): Data Reception -->\r\n");
      out.write("                        <div id=\"tabDataRec\" class=\"dhtmlgoodies_aTab\" align=\"left\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                                <tr height=\"2px\"></tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"form_group_title\" colspan=\"10\">Data Reception</td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr height=\"2px\"></tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"height: 10px;\"></td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\">\r\n");
      out.write("                                        <label for=\"txtEntryDate\">Entry Date</label>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td class=\"field\" title=\"Entry date\">\r\n");
      out.write("                                        <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                            <tr>\r\n");
      out.write("                                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                    <input type=\"text\" name=\"txtEntryDate\" id=\"txtEntryDate\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                                           value=\"");
      out.print((aCase.getDate_Entry() != null ? sdf.format(aCase.getDate_Entry()) : ""));
      out.write("\" readonly clear=50 title=\"Entry Date\"\r\n");
      out.write("                                                           ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 50);\" ");
        }
      out.write(" >\r\n");
      out.write("                                                </td>\r\n");
      out.write("                                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                    ");

        if (!saveFlag.equals("V")) {
                                                    
      out.write("\r\n");
      out.write("                                                    <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select Entry Date\"></a>\r\n");
      out.write("                                                    <script type=\"text/javascript\">\r\n");
      out.write("                                                        Calendar.setup({\r\n");
      out.write("                                                            inputField : \"txtEntryDate\", //*\r\n");
      out.write("                                                            ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                            showsTime : true,\r\n");
      out.write("                                                            button : \"cal_trigger1\", //*\r\n");
      out.write("                                                            step : 1\r\n");
      out.write("                                                        });\r\n");
      out.write("                                                    </script>\r\n");
      out.write("                                                    ");
}
      out.write("\r\n");
      out.write("                                                </td>\r\n");
      out.write("                                            </tr>\r\n");
      out.write("                                        </table>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\"><label for=\"txtRecdHq\">Received By</label></td>\r\n");
      out.write("                                    <td class=\"field\" title=\"Received by\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtRecdHq\"\r\n");
      out.write("                                               id=\"txtRecdHq\" size=\"36\" maxlength=\"20\" value=\"");
      out.print((aCase.getRecv_By_At_Hq() == null ? "" : aCase.getRecv_By_At_Hq()));
      out.write("\"\r\n");
      out.write("                                               title=\"Received By whom at company\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\">\r\n");
      out.write("                                        <label for=\"txtRecvDate\">Receipt Date</label>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td class=\"field\" title=\"Receipt date\">\r\n");
      out.write("                                        <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                            <tr>\r\n");
      out.write("                                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                    <input type=\"text\" name=\"txtRecvDate\" id=\"txtRecvDate\" size=\"15\" maxlength=\"12\" title=\"Receipt Date\"\r\n");
      out.write("                                                           onchange=\"if(!dateDifference_new('txtDoB', 'txtRecvDate', 'txtAgeOnset')) {\r\n");
      out.write("                                                               this.value=null;\r\n");
      out.write("                                                               document.FrmCaseDetails.txtAgeOnset.value = null;\r\n");
      out.write("                                                           }\"\r\n");
      out.write("                                                           value=\"");
      out.print((aCase.getDate_Recv() != null ? sdf.format(aCase.getDate_Recv()) : ""));
      out.write("\" readonly clear=51\r\n");
      out.write("                                                           ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 51);\" ");
        }
      out.write(" />\r\n");
      out.write("                                                </td>\r\n");
      out.write("                                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                    ");

        if (!saveFlag.equals("V")) {
                                                    
      out.write("\r\n");
      out.write("                                                    <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Receipt Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                                    <script type=\"text/javascript\">\r\n");
      out.write("                                                    Calendar.setup({\r\n");
      out.write("                                                        inputField : \"txtRecvDate\", //*\r\n");
      out.write("                                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                        showsTime : true,\r\n");
      out.write("                                                        button : \"cal_trigger2\", //*\r\n");
      out.write("                                                        step : 1\r\n");
      out.write("                                                    });\r\n");
      out.write("                                                    </script>\r\n");
      out.write("                                                    ");
}
      out.write("\r\n");
      out.write("                                                </td>\r\n");
      out.write("                                            </tr>\r\n");
      out.write("                                        </table>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\"><label for=\"txtLocalRep\">Local Report No.</label></td>\r\n");
      out.write("                                    <td class=\"field\" title=\"Local report number\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtLocalRep\" title=\"Local Report Number\"\r\n");
      out.write("                                               id=\"txtLocalRep\" size=\"36\" maxlength=\"10\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getRep_Num_Local() == null ? "" : aCase.getRep_Num_Local()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\"><label for=\"txtCrossRef\">Cross Ref Rep No.</label></td>\r\n");
      out.write("                                    <td class=\"field\" title=\"Cross reference report number\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtCrossRef\" title=\"Cross Ref Rep Number\"\r\n");
      out.write("                                               id=\"txtCrossRef\" size=\"36\" maxlength=\"10\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getRep_Xref_Num() == null ? "" : aCase.getRep_Xref_Num()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\"><label for=\"txtCaseDRRTypeCode\">Report Type</label></td>\r\n");
      out.write("                                    <td class=\"field\" title=\"Report type\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtCaseDRRTypeCode\" name=\"txtCaseDRRTypeCode\" size=\"45\" onkeyup=\"ajax_showOptions('txtCaseDRRTypeCode_ID',this,'ReportType',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getReportTypeDesc() == null ? "" : aCase.getReportTypeDesc()));
      out.write("\"\r\n");
      out.write("                                               title=\"Report Type description\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"3\" id=\"txtCaseDRRTypeCode_ID\" name=\"txtCaseDRRTypeCode_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getRtype() == null ? "" : aCase.getRtype()));
      out.write("\" />\r\n");
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"height: 20px;\"></td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <!-- Tab 1.2 (2): Source -->\r\n");
      out.write("                                <tr height=\"2px\"></tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"form_group_title\" colspan=\"10\">Source</td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr height=\"2px\"></tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"height: 10px;\"></td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtCountryCode\">Country Code</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <div id=\"divCntryDynData\">\r\n");
      out.write("                                            <input type=\"text\" id=\"txtCountryCode\" name=\"txtCountryCode\" size=\"45\" onkeyup=\"ajax_showOptions('txtCountryCode_ID',this,'Country',event)\"\r\n");
      out.write("                                                   value=\"");
      out.print((aCase.getCountry_Nm() == null ? "" : aCase.getCountry_Nm()));
      out.write("\" title=\"Country details\"\r\n");
      out.write("                                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                   
      out.write(">\r\n");
      out.write("                                            <input type=\"text\" readonly size=\"3\" id=\"txtCountryCode_ID\" name=\"txtCountryCode_ID\"\r\n");
      out.write("                                                   value=\"");
      out.print((aCase.getCntry() == null ? "" : aCase.getCntry()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"60\"><label for=\"txtReporterStat\">Reporter status</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtReporterStat\" name=\"txtReporterStat\" size=\"30\" onkeyup=\"ajax_showOptions('txtReporterStat_ID',this,'ReporterStatus',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getRepstatDesc() == null ? "" : aCase.getRepstatDesc()));
      out.write("\" title=\"Reporter status details\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"4\" id=\"txtReporterStat_ID\" name=\"txtReporterStat_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getRepstat() == null ? "" : aCase.getRepstat()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtClinRep\">Clinical Reporter Info</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtClinRep\" id=\"txtClinRep\" size=\"37\" maxlength=\"35\" title=\"Clinical reporter information\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getClin_Rep_Info() == null ? "" : aCase.getClin_Rep_Info()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtCpnyRepInfo\">Com. Reporter Info</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtCpnyRepInfo\" id=\"txtCpnyRepInfo\" size=\"37\" maxlength=\"50\" title=\"Company reporter information\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getComp_Rep_Info() == null ? "" : aCase.getComp_Rep_Info()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtPatNum\">Patient Num</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtPatNum\" id=\"txtPatNum\" size=\"37\" maxlength=\"20\" title=\"Patient Number\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getPatient_Num() == null ? "" : aCase.getPatient_Num()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtReporter\">Reporter</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtReporter\" name=\"txtReporter\" size=\"30\" onkeyup=\"ajax_showOptions('txtReporter_ID',this,'ReportSource',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getSource_Desc() == null ? "" : aCase.getSource_Desc()));
      out.write("\" title=\"Reporter code\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"4\" id=\"txtReporter_ID\" name=\"txtReporter_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getSrccode() == null ? "" : aCase.getSrccode()));
      out.write("\">\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtTrialNum\">Source/Trial Num</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtTrialNum\" name=\"txtTrialNum\" size=\"37\" onkeyup=\"ajax_showOptions('txtTrialNum_ID',this,'TrialNo',event)\"\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getTrialnum() == null ? "" : aCase.getTrialnum()));
      out.write("\" title=\"Source/Trial Num\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"hidden\" size=\"3\" id=\"txtTrialNum_ID\" name=\"txtTrialNum_ID\"\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getTrialnum() == null ? "" : aCase.getTrialnum()));
      out.write("\">\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td class=\"fLabel\" width=\"125\"><label for=\"txtHospitalName\">Hospital name</label></td>\r\n");
      out.write("                                    <td class=\"field\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtHospitalName\" id=\"txtHospitalName\" size=\"37\" maxlength=\"50\" title=\"Hospital name\"\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getHosp_Nm() == null ? "" : aCase.getHosp_Nm()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </div>\r\n");
      out.write("\r\n");
      out.write("                        <!-- Tab 2.1 (3): Product Info -->\r\n");
      out.write("                        <div id=\"tab3\" class=\"dhtmlgoodies_aTab\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <tr height=\"2px\"></tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Product Info</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr height=\"2px\"></tr>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode\">Product</label></td>\r\n");
      out.write("                            <td class=\"field\" colspan=\"9\">\r\n");
      out.write("\r\n");
      out.write("                                <input type=\"text\" id=\"txtProdCode\" name=\"txtProdCode\" size=\"56\" onkeyup=\"ajax_showOptions('txtProdCode_ID',this,'Product',event)\"\r\n");
      out.write("                                       value=\"");

        if ((aCase.getGeneric_Nm() != null && aCase.getBrand_Nm() != null) &&
                (aCase.getGeneric_Nm() != "" && aCase.getBrand_Nm() != "")) {
            out.write(aCase.getBrand_Nm() + " (" + aCase.getGeneric_Nm() + ")");
        } else {
            out.write("");
        }
                                       
      out.write("\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        } else {
            out.write(" onpress=\"return ClearInput(event, 'txtProdCode_ID');\" ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"13\" id=\"txtProdCode_ID\" name=\"txtProdCode_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getProdcode() == null ? "" : aCase.getProdcode()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtCpnyCode\">Manufacturer</label></td>\r\n");
      out.write("                            <td class=\"field\" width=\"33%\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtCpnyCode\" name=\"txtCpnyCode\" size=\"35\" onkeyup=\"ajax_showOptions('txtCpnyCode_ID', this,'Manufacturer',event);\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getM_Name() == null ? "" : aCase.getM_Name()));
      out.write("\" title=\"Manufacturer\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"5\" id=\"txtCpnyCode_ID\" name=\"txtCpnyCode_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getMcode() == null ? "" : aCase.getMcode()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"20%\"><label for=\"txtBatchNum\">Batch #</label></td>\r\n");
      out.write("                            <td class=\"field\" width=\"30%\"/>\r\n");
      out.write("                                <input type=\"text\" name=\"txtBatchNum\" id=\"txtBatchNum\" size=\"7\" maxlength=\"6\" title=\"Batch number\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getBatch_Num() == null ? "" : aCase.getBatch_Num()));
      out.write("\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(" />\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtFormulationCode\">Formulation</label></td>\r\n");
      out.write("                            <td class=\"field\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtFormulationCode\" name=\"txtFormulationCode\" size=\"15\" onkeyup=\"ajax_showOptions('txtFormulationCode_ID',this,'Formulation',event);\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getFormulation_Desc() == null ? "" : aCase.getFormulation_Desc()));
      out.write("\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"5\" id=\"txtFormulationCode_ID\" name=\"txtFormulationCode_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getFormcode() == null ? "" : aCase.getFormcode()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtDose\">Dose</label></td>\r\n");
      out.write("                            <td class=\"field\">\r\n");
      out.write("                                <input type=\"text\" name=\"txtDose\" id=\"txtDose\" size=\"6\" maxlength=\"6\" title=\"Dose\"\r\n");
      out.write("                                       onkeydown=\"if(!checkIsNumericDecVar(this.value))this.value='';\"\r\n");
      out.write("                                       onkeyup=\"if(!checkIsNumericDecVar(this.value))this.value='';\"\r\n");
      out.write("                                       value=\"");
if ((aCase.getDose() != -1f) && (aCase.getDose() != 0f)) {
            out.write("" + aCase.getDose());
        } else {
            out.write("");
        }
      out.write("\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(" />\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtDoseUnit\">Dose Unit</label></td>\r\n");
      out.write("                            <td class=\"field\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtDoseUnit\" name=\"txtDoseUnit\" size=\"15\" onkeyup=\"ajax_showOptions('txtDoseUnit_ID',this,'DoseUnits',event)\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getUnit_Desc() == null ? "" : aCase.getUnit_Desc()));
      out.write("\" title=\"Dose unit\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        } else {
            out.write(" onpress=\"return ClearInput(event, 'txtFormulationCode_ID');\" ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"3\" id=\"txtDoseUnit_ID\" name=\"txtDoseUnit_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getUnitcode() == null ? "" : aCase.getUnitcode()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtDoseRegimen\">Dose regimen</label></td>\r\n");
      out.write("                            <td class=\"field\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtDRegimenCode\" name=\"txtDRegimenCode\" size=\"28\" onkeyup=\"ajax_showOptions('txtDRegimenCode_ID',this,'DoseRgmn',event)\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getDregimen_Desc() == null ? "" : aCase.getDregimen_Desc()));
      out.write("\" title=\"Dose regimen\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"6\" id=\"txtDRegimenCode_ID\" name=\"txtDRegimenCode_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getDregimen() == null ? "" : aCase.getDregimen()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtDoseFreq\">Dosing Period</label></td>\r\n");
      out.write("                            <td class=\"field\" title=\"Dose frequency\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtDoseFreq\" name=\"txtDoseFreq\" size=\"15\" onkeyup=\"ajax_showOptions('txtDoseFreq_ID',this,'DoseFreq',event)\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getDfreq_Desc() == null ? "" : aCase.getDfreq_Desc()));
      out.write("\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"5\" id=\"txtDoseFreq_ID\" name=\"txtDoseFreq_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getDfreq() == null ? "" : aCase.getDfreq()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtRoute\">Route</label></td>\r\n");
      out.write("                            <td class=\"field\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtRouteCode\" name=\"txtRouteCode\" size=\"28\" onkeyup=\"ajax_showOptions('txtRouteCode_ID',this,'Route',event)\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getRoute_Desc() == null ? "" : aCase.getRoute_Desc()));
      out.write("\" title=\"Route\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"6\" id=\"txtRouteCode_ID\" name=\"txtRouteCode_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getRoutecode() == null ? "" : aCase.getRoutecode()));
      out.write("\">\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                        <td class=\"fLabel\">\r\n");
      out.write("                            <label for=\"txtTreatmentStartDate\">Treatment Start</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Treatment start date\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtTreatmentStartDate\" id=\"txtTreatmentStartDate\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                               clear=63 value=\"");
      out.print((aCase.getTreat_Start() == null ? "" : sdf.format(aCase.getTreat_Start())));
      out.write("\" readonly\r\n");
      out.write("                                               onchange=\"if(!dateDifference(this.value, document.FrmCaseDetails.txtTreatmentEndDate.value)){\r\n");
      out.write("                                               this.value=null;\r\n");
      out.write("                                               document.FrmCaseDetails.txtDuration.value = null;\r\n");
      out.write("                                           }\"\r\n");
      out.write("                                               ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"ClearInput(event, 63);if(!dateDifference(this.value, document.FrmCaseDetails.txtTreatmentEndDate.value)){ this.value=null; document.FrmCaseDetails.txtDuration.value = null; }\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Treatment start date\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerTreatStart\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                                    Calendar.setup({\r\n");
      out.write("                                        inputField : \"txtTreatmentStartDate\", //*\r\n");
      out.write("                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                        showsTime : true,\r\n");
      out.write("                                        button : \"cal_triggerTreatStart\", //*\r\n");
      out.write("                                        step : 1\r\n");
      out.write("                                    });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"fLabel\">\r\n");
      out.write("                            <label for=\"txtTreatmentEndDate\">Treatment End</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Treatment end date\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtTreatmentEndDate\" id=\"txtTreatmentEndDate\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                               clear=64 value=\"");
      out.print((aCase.getTreat_Stop() == null ? "" : sdf.format(aCase.getTreat_Stop())));
      out.write("\" readonly\r\n");
      out.write("                                               onchange=\"if(!dateDifference(document.FrmCaseDetails.txtTreatmentStartDate.value, this.value)) {\r\n");
      out.write("                                           this.value=null;\r\n");
      out.write("                                           document.FrmCaseDetails.txtDuration.value = null;\r\n");
      out.write("                                       }\"\r\n");
      out.write("                                               ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"ClearInput(event, 64);if(!dateDifference(document.FrmCaseDetails.txtTreatmentStartDate.value, this.value)) { this.value=null; document.FrmCaseDetails.txtDuration.value = null;}\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Treatment end date\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerTreatStop\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                                Calendar.setup({\r\n");
      out.write("                                    inputField : \"txtTreatmentEndDate\", //*\r\n");
      out.write("                                    ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                    showsTime : true,\r\n");
      out.write("                                    button : \"cal_triggerTreatStop\", //*\r\n");
      out.write("                                    step : 1\r\n");
      out.write("                                });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtDuration\">Duration (Days)</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtDuration\" id=\"txtDuration\" size=\"30\" maxlength=\"30\" title=\"Duration\"\r\n");
      out.write("                               onkeydown=\"return checkIsNumeric(event);\" value=\"");

        if ((aCase.getDuration() > 0)) {
            out.write("" + aCase.getDuration());
        }
                               
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtTratmentIndicationCode\">Treatment Indication</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" id=\"txtTratmentIndicationCode\" name=\"txtTratmentIndicationCode\" size=\"20\" onkeyup=\"ajax_showOptions('txtTratmentIndicationCode_ID',this,'IndicationTreatment',event)\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getIndication() == null ? "" : aCase.getIndication()));
      out.write("\" title=\"Treatment indication\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(">\r\n");
      out.write("                        <input type=\"text\" readonly size=\"8\" id=\"txtTratmentIndicationCode_ID\" name=\"txtTratmentIndicationCode_ID\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getIndnum() == null ? "" : aCase.getIndnum()));
      out.write("\">\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtAuthNum\">Authorization Number</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtAuthNum\" id=\"txtAuthNum\" size=\"30\" maxlength=\"20\" title=\"Authorization Number\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getAuthorization_No() == null ? "" : aCase.getAuthorization_No()));
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr height=\"2px\"></tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\"><label for=\"chkRechallenge\">Rechallenge</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"checkbox\" name=\"chkRechallenge\" id=\"chkRechallenge\" class=\"chkbox\" onclick=\"setRechallengeControls();\"\r\n");
      out.write("                               title=\"Rechallenge\"\r\n");
      out.write("                               ");

        if (aCase.getRechallenge() != null) {
            if (aCase.getRechallenge().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\">\r\n");
      out.write("                        <label for=\"txtRechallengeDate\">Rechallenge Date</label>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Rechallenge date\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtRechallengeDate\" id=\"txtRechallengeDate\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                           clear=66 value=\"");
      out.print((aCase.getRechallenge_Date() != null ? sdf.format(aCase.getRechallenge_Date()) : ""));
      out.write("\" readonly\r\n");
      out.write("                                           ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 66);\" ");
        }
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                    ");

        if (!saveFlag.equals("V")) {
                                    
      out.write("\r\n");
      out.write("                                    <a href=\"#\" id=\"calRechDate\" >\r\n");
      out.write("                                        <img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' id=\"imgCalRechDate\" name=\"imgCalRechDate\">\r\n");
      out.write("                                    </a>\r\n");
      out.write("                                    <!--script type=\"text/javascript\">\r\n");
      out.write("        /*    if (document.FrmCaseDetails.chkRechallenge.checked) {\r\n");
      out.write("                Calendar.setup({\r\n");
      out.write("                        inputField : \"txtRechallengeDate\", //*\r\n");
      out.write("                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                        button : \"calRechDate\", //*\r\n");
      out.write("                        step : 1\r\n");
      out.write("                        });\r\n");
      out.write("            } else {\r\n");
      out.write("                        Calendar.setup({\r\n");
      out.write("                        displayStat : \"D\",\r\n");
      out.write("                        inputField : \"txtRechallengeDate\", //*\r\n");
      out.write("                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                        button : \"calRechDate\", //*\r\n");
      out.write("                        step : 1\r\n");
      out.write("                        });\r\n");
      out.write("           }*/\r\n");
      out.write("        </script-->\r\n");
      out.write("                                    ");
}
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtRechallengeDose\">Rechallenge Dose</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtRechallengeDose\" id=\"txtRechallengeDose\" size=\"30\" maxlength=\"10\"\r\n");
      out.write("                               onkeydown=\"return checkIsNumericDec(event);\" title=\"Rechallenge Dose\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getRechallenge_Dose() == null ? "" : aCase.getRechallenge_Dose()));
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td class=\"fLabel\"><label for=\"txtRechallengeResult\">Rechallenge Result</label></td>\r\n");
      out.write("                    <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                        <input type=\"text\" id=\"txtRechallengeResult\" name=\"txtRechallengeResult\" size=\"24\" onkeyup=\"ajax_showOptions('txtRechallengeResult_ID',this,'RechallengeResult',event)\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getResult_Desc() == null ? "" : aCase.getResult_Desc()));
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(">\r\n");
      out.write("                        <input type=\"text\" readonly size=\"3\" id=\"txtRechallengeResult_ID\" name=\"txtRechallengeResult_ID\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getRescode() == null ? "" : aCase.getRescode()));
      out.write("\">\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <!-- Tab 2.2 (4): Patient -->\r\n");
      out.write("                <tr height=\"2px\"></tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"form_group_title\" colspan=\"10\">Patient</td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr height=\"2px\"></tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtPatientInitials\">Patient Initials</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtPatientInitials\" id=\"txtPatientInitials\" size=\"5\" maxlength=\"4\" title=\"Patient initials\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getP_Initials() == null ? "" : aCase.getP_Initials()));
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td class=\"fLabel\"><label for=\"txtSexCode\">Sex</label></td>\r\n");
      out.write("                    <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                        <div id=\"divSexDynData\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtSexCode\" name=\"txtSexCode\" size=\"20\" onkeyup=\"ajax_showOptions('txtSexCode_ID',this,'Sex',event)\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getSex_Desc() == null ? "" : aCase.getSex_Desc()));
      out.write("\" title=\"Sex code\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(">\r\n");
      out.write("                            <input type=\"text\" readonly size=\"3\" id=\"txtSexCode_ID\" name=\"txtSexCode_ID\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getSexcode() == null ? "" : aCase.getSexcode()));
      out.write("\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtDoB\">Date of Birth</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Birth date\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtDoB\" id=\"txtDoB\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                           onchange=\"if(!dateDifference_new('txtDoB', 'txtRecvDate', 'txtAgeOnset')){\r\n");
      out.write("                                   this.value=null;\r\n");
      out.write("                                   document.FrmCaseDetails.txtAgeOnset.value = null;\r\n");
      out.write("                               }\"\r\n");
      out.write("                                           value=\"");
      out.print((aCase.getDate_Of_Birth() == null ? "" : sdf.format(aCase.getDate_Of_Birth())));
      out.write("\" readonly clear=69\r\n");
      out.write("                                           ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 69);\" ");
        }
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td style=\"padding-right: 3px;padding-left: 0px;\">\r\n");
      out.write("                                    ");

        if (!saveFlag.equals("V")) {
                                    
      out.write("\r\n");
      out.write("                                    <a href=\"#\" id=\"cal_triggerDoB\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                    <script type=\"text/javascript\">\r\n");
      out.write("                        Calendar.setup({\r\n");
      out.write("                            inputField : \"txtDoB\", //*\r\n");
      out.write("                            ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                            showsTime : true,\r\n");
      out.write("                            button : \"cal_triggerDoB\", //*\r\n");
      out.write("                            step : 1\r\n");
      out.write("                        });\r\n");
      out.write("                                    </script>\r\n");
      out.write("                                    ");
}
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtAgeOnset\">Age on Set (yrs)</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtAgeOnset\" id=\"txtAgeOnset\" size=\"4\" maxlength=\"3\" title=\"Age on set\"\r\n");
      out.write("                               readonly value=\"\" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtAgeRep\">Age Reported</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtAgeRep\" id=\"txtAgeRep\" size=\"4\" maxlength=\"3\" title=\"Age reported\"\r\n");
      out.write("                               onkeydown=\"return checkIsNumeric(event);\" value=\"");

        if ((aCase.getAge_Reported() != -1) && (aCase.getAge_Reported() != 0)) {
            out.write("" + aCase.getAge_Reported());
        } else {
            out.write("");
        }
                               
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtWeight\">Weight</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtWeight\" id=\"txtWeight\" size=\"4\" maxlength=\"3\" title=\"Weight\"\r\n");
      out.write("                               onkeydown=\"return checkIsNumeric(event);\" value=\"");
      out.print(((aCase.getWeight() != -1) && (aCase.getWeight() != 0)) ? "" + aCase.getWeight() : "");
      out.write("\"\r\n");
      out.write("                               ");
 if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
      out.write(" >\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtHeight\">Height</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" name=\"txtHeight\" id=\"txtHeight\" size=\"4\" maxlength=\"3\" title=\"Height\"\r\n");
      out.write("                               onkeydown=\"return checkIsNumeric(event);\" value=\"");
      out.print(((aCase.getHeight() != -1) && (aCase.getHeight() != 0)) ? "" + aCase.getHeight() : "");
      out.write("\"\r\n");
      out.write("                               ");
 if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td class=\"fLabel\" width=\"125\"><label for=\"txtEthnicCode\">Ethnic Origin</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"text\" id=\"txtEthnicCode\" name=\"txtEthnicCode\" size=\"30\" onkeyup=\"ajax_showOptions('txtEthnicCode_ID',this,'EthnicOrigin',event)\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getEthnic_Desc() == null ? "" : aCase.getEthnic_Desc()));
      out.write("\"\r\n");
      out.write("                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                               
      out.write(">\r\n");
      out.write("                        <input type=\"text\" readonly size=\"3\" id=\"txtEthnicCode_ID\" name=\"txtEthnicCode_ID\"\r\n");
      out.write("                               value=\"");
      out.print((aCase.getEtnicorig() == null ? "" : aCase.getEtnicorig()));
      out.write("\">\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"fLabel\"><label for=\"chkPregnant\">Pregnant</label></td>\r\n");
      out.write("                    <td class=\"field\">\r\n");
      out.write("                        <input type=\"checkbox\" name=\"chkPregnant\" id=\"chkPregnant\" class=\"chkbox\" onclick=\"setPregControls();\"\r\n");
      out.write("                               title=\"Pregnancy\"\r\n");
      out.write("                               ");

        if (aCase.getPregnancy() != null) {
            if (aCase.getPregnancy().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                               
      out.write(" />\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtGestWeek\">Gestation (week)</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtGestWeek\" id=\"txtGestWeek\" size=\"4\" maxlength=\"3\" title=\"Gestation week at onset\"\r\n");
      out.write("                                   onkeydown=\"return checkIsNumeric(event);\"\r\n");
      out.write("                                   value=\"");
if ((aCase.getPregnancy_Week() != -1) && (aCase.getPregnancy_Week() != 0)) {
            out.write("" + aCase.getPregnancy_Week());
        } else {
            out.write("");
        }
      out.write("\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(" />\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            <!-- Tab 3.1 (6): Outcome & Assessment -->\r\n");
      out.write("            <div id=\"tab6\" class=\"dhtmlgoodies_aTab\">\r\n");
      out.write("                <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                    <tr height=\"2px\"></tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Outcome & Assessment</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr height=\"2px\"></tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" colspan=\"1\"><label for=\"txtOutcomeCode\">Outcome</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <div id=\"divOCDynData\">\r\n");
      out.write("                                <input type=\"text\" id=\"txtOutcomeCode\" name=\"txtOutcomeCode\" size=\"24\" onkeyup=\"ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getOutcome() == null ? "" : aCase.getOutcome()));
      out.write("\"\r\n");
      out.write("                                       ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                       
      out.write(">\r\n");
      out.write("                                <input type=\"text\" readonly size=\"3\" id=\"txtOutcomeCode_ID\" name=\"txtOutcomeCode_ID\"\r\n");
      out.write("                                       value=\"");
      out.print((aCase.getOutcomecode() == null ? "" : aCase.getOutcomecode()));
      out.write("\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td colspan=\"7\">\r\n");
      out.write("                            <input style=\"border:0\" type=\"text\" name=\"txtAssConclusion\" id=\"txtAssConclusion\" size=\"25\" readonly />\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" colspan=\"1\">\r\n");
      out.write("                            <label for=\"txtDateOfOutcome\">Date of Outcome</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Date of outcome\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtDateOfOutcome\" id=\"txtDateOfOutcome\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                               clear=72 value=\"");
      out.print((aCase.getOutcome_Rep_Date() == null ? "" : sdf.format(aCase.getOutcome_Rep_Date())));
      out.write("\" readonly\r\n");
      out.write("                                               ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 72);\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerDoOC\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtDateOfOutcome\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_triggerDoOC\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtPhyAssCode\">Physician Assessment</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtPhyAssCode\" name=\"txtPhyAssCode\" size=\"22\" onkeyup=\"ajax_showOptions('txtPhyAssCode_ID',this,'AssCausality',event)\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getPhy_Assessment_Code() == null ? "" : aCase.getPhy_Assessment()));
      out.write("\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(">\r\n");
      out.write("                            <input type=\"text\" readonly size=\"3\" id=\"txtPhyAssCode_ID\" name=\"txtPhyAssCode_ID\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getPhy_Assessment_Code() == null ? "" : aCase.getPhy_Assessment_Code()));
      out.write("\">\r\n");
      out.write("                        </td>\r\n");
      out.write("\r\n");
      out.write("                        <td class=\"fLabelR\">\r\n");
      out.write("                            <label for=\"txtDateOfPhyAssessment\">Date of Assessment</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Date of Assessment\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtDateOfPhyAssessment\" id=\"txtDateOfPhyAssessment\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                               clear=74 readonly\r\n");
      out.write("                                               value=\"");
      out.print((aCase.getPhys_Assessment_Date() == null ? "" : sdf.format(aCase.getPhys_Assessment_Date())));
      out.write("\"\r\n");
      out.write("                                               ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 74);\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Date of Assessment\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerDoPA\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtDateOfPhyAssessment\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_triggerDoPA\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtPhyAssReason\">Physician's Reason</label></td>\r\n");
      out.write("                        <td colspan=\"9\">\r\n");
      out.write("                            <textarea name=\"txtPhyAssReason\" id=\"txtPhyAssReason\" cols=\"110\" wrap=\"physical\" title=\"Physician's Reason for assessment\"\r\n");
      out.write("                                      ");
 if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
      out.write("\r\n");
      out.write("                                      >");
if (aCase.getPhys_Assessment_Reason() == null) {
            out.write("");
        } else {
            out.write(aCase.getPhys_Assessment_Reason());
        }
                            
      out.write("</textarea>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtContribFactor\">Contributing factors</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtContribFactor\" id=\"txtContribFactor\" size=\"50\" maxlength=\"50\" title=\"Contributing factors\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getContributing_Factors() == null ? "" : aCase.getContributing_Factors()));
      out.write("\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(" />\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtCoAssCode\">Company Assessment</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtCoAssCode\" name=\"txtCoAssCode\" size=\"22\" onkeyup=\"ajax_showOptions('txtCoAssCode_ID',this,'AssCausality',event)\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getCo_Assessment() == null ? "" : aCase.getCo_Assessment()));
      out.write("\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(">\r\n");
      out.write("                            <input type=\"text\" readonly size=\"3\" id=\"txtCoAssCode_ID\" name=\"txtCoAssCode_ID\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getCo_Assessment_Code() == null ? "" : aCase.getCo_Assessment_Code()));
      out.write("\">\r\n");
      out.write("                        </td>\r\n");
      out.write("\r\n");
      out.write("                        <td class=\"fLabelR\">\r\n");
      out.write("                            <label for=\"txtDateOfCoAssessment\">Date of Assessment</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" title=\"Date of Assessment\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtDateOfCoAssessment\" id=\"txtDateOfCoAssessment\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                               clear=76 value=\"");
      out.print((aCase.getCo_Assessment_Date() == null ? "" : sdf.format(aCase.getCo_Assessment_Date())));
      out.write("\" readonly\r\n");
      out.write("                                               ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 76);\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerDoCA\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtDateOfCoAssessment\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_triggerDoCA\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"fLabel\" width=\"125\"><label for=\"txtCoAssReason\">Company's Reason</label></td>\r\n");
      out.write("                            <td colspan=\"9\">\r\n");
      out.write("                                <textarea name=\"txtCoAssReason\" id=\"txtCoAssReason\" cols=\"110\" wrap=\"physical\" title=\"Company's Reason for assessment\"\r\n");
      out.write("                                          ");
 if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
      out.write("\r\n");
      out.write("                                          >");
if (aCase.getCo_Assessment_Reason() == null) {
            out.write("");
        } else {
            out.write(aCase.getCo_Assessment_Reason());
        }
                                
      out.write("</textarea>\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </tr>\r\n");
      out.write("\r\n");
      out.write("                    <!-- Tab 3.2 (9): Distribution -->\r\n");
      out.write("                    <tr height=\"2px\"></tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Distribution</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr height=\"2px\"></tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtDistribnCpny\">Distributed to</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtDistribnCpny\" name=\"txtDistribnCpny\" size=\"50\" onkeyup=\"ajax_showOptions('txtDistribnCpny_ID',this,'SubmCo',event)\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getDcompany() == null ? "" : aCase.getDcompany()));
      out.write("\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(">\r\n");
      out.write("                            <input type=\"hidden\" readonly size=\"3\" id=\"txtDistribnCpny_ID\" name=\"txtDistribnCpny_ID\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getDcompany() == null ? "" : aCase.getDcompany()));
      out.write("\">\r\n");
      out.write("                        </td>\r\n");
      out.write("\r\n");
      out.write("                        <td class=\"fLabelR\">\r\n");
      out.write("                            <label for=\"txtDistribnDate\">On</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        <input type=\"text\" name=\"txtDistribnDate\" id=\"txtDistribnDate\" size=\"15\" maxlength=\"11\" title=\"Distribution Date\"\r\n");
      out.write("                                               clear=78 value=\"");
      out.print((aCase.getDistbn_Date() == null ? "" : sdf.format(aCase.getDistbn_Date())));
      out.write("\" readonly\r\n");
      out.write("                                               ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 78);\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerDD\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtDistribnDate\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_triggerDD\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtLHACode\">LHA</label></td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <input type=\"text\" id=\"txtLHACode\" name=\"txtLHACode\" size=\"30\" onkeyup=\"ajax_showOptions('txtLHACode_ID',this,'LHA',event)\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getLha_Desc() == null ? "" : aCase.getLha_Desc()));
      out.write("\" title=\"LHA\"\r\n");
      out.write("                                   ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                   
      out.write(">\r\n");
      out.write("                            <input type=\"text\" readonly size=\"12\" id=\"txtLHACode_ID\" name=\"txtLHACode_ID\"\r\n");
      out.write("                                   value=\"");
      out.print((aCase.getSublha() == null ? "" : aCase.getSublha()));
      out.write("\">\r\n");
      out.write("                        </td>\r\n");
      out.write("\r\n");
      out.write("                        <td class=\"fLabelR\">\r\n");
      out.write("                            <label for=\"txtSubmissionDate\">Submission On</label>\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td class=\"field\">\r\n");
      out.write("                            <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\" >\r\n");
      out.write("                                        <input type=\"text\" name=\"txtSubmissionDate\" id=\"txtSubmissionDate\" size=\"15\" maxlength=\"11\"\r\n");
      out.write("                                               clear=79 value=\"");
      out.print((aCase.getSubmission_Date() == null ? "" : sdf.format(aCase.getSubmission_Date())));
      out.write("\" readonly\r\n");
      out.write("                                               title=\"Submission date\" ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 79);\" ");
        }
      out.write(" />\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                        ");

        if (!saveFlag.equals("V")) {
                                        
      out.write("\r\n");
      out.write("                                        <a href=\"#\" id=\"cal_triggerSubmD\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select Date\" border='0' name=\"imgCal\"></a>\r\n");
      out.write("                                        <script type=\"text/javascript\">\r\n");
      out.write("                            Calendar.setup({\r\n");
      out.write("                                inputField : \"txtSubmissionDate\", //*\r\n");
      out.write("                                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                showsTime : true,\r\n");
      out.write("                                button : \"cal_triggerSubmD\", //*\r\n");
      out.write("                                step : 1\r\n");
      out.write("                            });\r\n");
      out.write("                                        </script>\r\n");
      out.write("                                        ");
}
      out.write("\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                            </table>\r\n");
      out.write("                        </td>\r\n");
      out.write("\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtFurtherCommunication\">Further Communication</label></td>\r\n");
      out.write("                        <td title=\"Verbatim\" colspan=\"9\">\r\n");
      out.write("                            <textarea name=\"txtFurtherCommunication\" id=\"txtFurtherCommunication\" cols=\"110\" rows=\"3\" wrap=\"physical\" title=\"Further Communication\"\r\n");
      out.write("                                      ");
 if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
      out.write("\r\n");
      out.write("                                      >");
      out.print((aCase.getFurther_Commn() == null ? "" : aCase.getFurther_Commn()));
      out.write("</textarea>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"form_group_title\" colspan=\"10\">Autopsy</td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\"><label for=\"chkAutopsy\">Autopsy</label></td>\r\n");
      out.write("                        <td class=\"field\" colspan=\"1\">\r\n");
      out.write("                            <input type=\"checkbox\" name=\"chkAutopsy\" id=\"chkAutopsy\" class=\"chkbox\" onclick=\"setAutopsyControls();\"\r\n");
      out.write("                                   title=\"Autopsy\" ");

        if (aCase != null) {
            if (aCase.getAutopsy() != null) {
                if (aCase.getAutopsy().equalsIgnoreCase("Y")) {
                    out.write("CHECKED ");
                }
            }
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                   
      out.write("\r\n");
      out.write("                                   />\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td class=\"fLabel\" width=\"125\"><label for=\"txtAutopsyResult\">Autopsy Result</label></td>\r\n");
      out.write("                        <td colspan=\"9\">\r\n");
      out.write("                            <textarea name=\"txtAutopsyResult\" id=\"txtAutopsyResult\" cols=\"110\" wrap=\"physical\" title=\"Autopsy Result\"\r\n");
      out.write("                                      ");
 if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
      out.write("\r\n");
      out.write("                                      >");
if (aCase.getAutopsy_Result() == null) {
            out.write("");
        } else {
            out.write(aCase.getAutopsy_Result());
        }
                            
      out.write("</textarea>\r\n");
      out.write("                        </td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </table>\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td colspan='3'>&nbsp;  </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td colspan='3'>&nbsp;  </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("        <td class=\"field\" align=\"right\" colspan=\"4\">\r\n");
      out.write("            <form id=\"FrmCaseDetails\" name=\"FrmCaseDetails\" method=\"POST\" action=\"pgSavCase.jsp?Parent=1&SaveFlag=");
      out.print(saveFlag);
      out.write("\"\r\n");
      out.write("                  onsubmit=\"if(confirmSave()) { return validate(); } return false;\">\r\n");
      out.write("\r\n");
      out.write("                <input type=\"submit\" name=\"cmdSave\" class=\"");
      out.print((saveFlag.equals("V") ? "button disabled" : "button"));
      out.write("\"\r\n");
      out.write("                       value=\"Save\" style=\"width: 60px;\" ");
 if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
      out.write(" onclick=\"if(confirmSave()) { return validate(); } return false;\" title=\"Save\"/>\r\n");
      out.write("                <input type=\"reset\" align=\"right\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                       onclick=\"javascript:location.replace('");
      out.print(strParent.equals("0") ? "pgCaseSummary.jsp?RepNum=" + strRepNum : "pgCaselist.jsp");
      out.write("');\" title=\"Cancel\"/>\r\n");
      out.write("                <input type=\"hidden\" id=\"SaveFlag\" name=\"SaveFlag\" value=\"");
      out.print(saveFlag);
      out.write("\" />\r\n");
      out.write("                <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"");
      out.print(strParent);
      out.write("\" />\r\n");
      out.write("                <input type=\"hidden\" id=\"hfPhyAssFlag\" name=\"hfPhyAssFlag\" value=\"\"/>\r\n");
      out.write("                <input type=\"hidden\" id=\"hfCoAssFlag\" name=\"hfCoAssFlag\" value=\"\"/>\r\n");
      out.write("            </form>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\"><!-- Outcome & Assessment / Distribution -->\r\n");
      out.write("initTabs('casedetailstab', Array(\r\n");
      out.write("'Data Reception / Source',\r\n");
      out.write("'Product Info / Patient',\r\n");
      out.write("'O&amp;A / Distribution'\r\n");
      out.write("), 0, 750, 480, Array(false,false,false));\r\n");
      out.write("    </script>\r\n");
      out.write("    </td>\r\n");
      out.write("</table>\r\n");
      out.write("</td>\r\n");
      out.write("</form>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Ends -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 10px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
