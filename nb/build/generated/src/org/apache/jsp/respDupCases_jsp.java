package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.CaseDetails;
import java.util.ArrayList;
import java.text.SimpleDateFormat;

public final class respDupCases_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\r');
      out.write('\n');
    ArrayList<CaseDetails> al = null;
        String strReceiveDate = "";
        String strClinRepInfo = "";
        String strCntryCode = "";
        String strProdCode = "";
        String strInitials = "";
        int nAgeReported = -1;
        String strSexCode = "";
        String strTrialNum = "";
        String strPatientNum = "";
        String strPTCode = "";
        String strAggrPT = "N";
        String strLLTCode = "";
        String strAggrLLT = "N";
        String strBodySys = "";
        String Seriousness = "";
        String strOutcomeCode = "";
        String strPhyAssCode = "";
        String strCoAssCode = "";
        String strComplInfo = "";

        if (request.getParameter("RcvDt") != null) {
            strReceiveDate = request.getParameter("RcvDt");
        }
        if (request.getParameter("CRI") != null) {
            strClinRepInfo = request.getParameter("CRI");
        }
        if (request.getParameter("CtryCd") != null) {
            strCntryCode = request.getParameter("CtryCd");
        }
        if (request.getParameter("ProdCd") != null) {
            strProdCode = request.getParameter("ProdCd");
        }
        if (request.getParameter("PInit") != null) {
            strInitials = request.getParameter("PInit");
        }
        if (request.getParameter("AgeRep") != null) {
            if (request.getParameter("AgeRep") != "") {
                nAgeReported = Integer.parseInt(request.getParameter("AgeRep"));
            }
        }
        if (request.getParameter("SxCd") != null) {
            strSexCode = request.getParameter("SxCd");
        }
        if (request.getParameter("TrialNm") != null) {
            strTrialNum = request.getParameter("TrialNm");
        }
        if (request.getParameter("PatNum") != null) {
            strPatientNum = request.getParameter("PatNum");
        }
        if (request.getParameter("PTCod") != null) {
            strPTCode = request.getParameter("PTCod");
        }
        if (request.getParameter("AggrPT") != null) {
            strAggrPT = "Y";
        } else {
            strAggrPT = "N";
        }
        if (request.getParameter("LLTCd") != null) {
            strLLTCode = request.getParameter("LLTCd");
        }
        if (request.getParameter("AggrLLT") != null) {
            strAggrLLT = "Y";
        } else {
            strAggrLLT = "N";
        }
        if (request.getParameter("BSysNum") != null) {
            strBodySys = request.getParameter("BSysNum");
        }
        if (request.getParameter("ClasfCd") != null) {
            Seriousness = request.getParameter("ClasfCd");
        }
        if (request.getParameter("OutCCod") != null) {
            strOutcomeCode = request.getParameter("OutCCod");
        }
        if (request.getParameter("PhAssCd") != null) {
            strPhyAssCode = request.getParameter("PhAssCd");
        }
        if (request.getParameter("CoAssCd") != null) {
            strCoAssCode = request.getParameter("CoAssCd");
        }
        if (request.getParameter("Nrr") != null) {
            strComplInfo = request.getParameter("Nrr");
        }

        CaseDetails aCase = new CaseDetails();
        al = aCase.listDuplicateCase((String) session.getAttribute("Company"),
                strReceiveDate, strClinRepInfo, strCntryCode,
                strProdCode, strInitials, nAgeReported,
                strSexCode, strTrialNum, strPatientNum,
                strPTCode, strAggrPT, strLLTCode,
                strAggrLLT, strBodySys, Seriousness,
                strOutcomeCode, strPhyAssCode, strCoAssCode,
                strComplInfo);

      out.write("\r\n");
      out.write("<table width=\"100%\">\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                        ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("            <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                <tr class=\"grid_header\">\r\n");
      out.write("                    <td width=\"16\"></td>\r\n");
      out.write("                    <td width=\"200\">Rep Num</td>\r\n");
      out.write("                    <td width=\"100\">Source/Trail Num</td>\r\n");
      out.write("                    <td width=\"100\">Rec. Date</td>\r\n");
      out.write("                    <td width=\"50\">Status</td>\r\n");
      out.write("                    <td></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                ");

        for (int i = 0; i < al.size(); i++) {
            aCase = al.get(i);
                
      out.write("\r\n");
      out.write("                <tr class=\"\r\n");
      out.write("                    ");

                    if (i % 2 == 0) {
                        out.write("even_row");
                    } else {
                        out.write("odd_row");
                    }
                    
      out.write("\r\n");
      out.write("                    \">\r\n");
      out.write("                    <td width=\"16\" align=\"center\" >\r\n");
      out.write("                        <a href=\"#\" onclick=\"popupWnd('_blank', 'no','pgCaseSummary.jsp?ShowBkBtn=0&RepNum=");
      out.print(aCase.getRep_Num());
      out.write("',800,600)\">\r\n");
      out.write("                            <img src=\"images/icons/view.gif\" title=\"\" border='0' alt=\"View\"/>\r\n");
      out.write("                        </a>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td width=\"200\" align='left'>\r\n");
      out.write("                        ");
      out.print(aCase.getRep_Num());
      out.write("\r\n");
      out.write("                    </td>\r\n");
      out.write("                    <td width=\"100\" align='left'></td>\r\n");
      out.write("                    <td width=\"100\" align='left'></td>\r\n");
      out.write("                    <td width=\"50\"></td>\r\n");
      out.write("                    <td></td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                ");
 }
      out.write("\r\n");
      out.write("            </table>\r\n");
      out.write("        </td>\r\n");
      out.write("        <tr></tr>\r\n");
      out.write("    </tr>\r\n");
      out.write("</table>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
