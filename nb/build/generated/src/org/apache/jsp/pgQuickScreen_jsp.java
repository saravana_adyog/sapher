package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.Country;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public final class pgQuickScreen_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CSD1";
    private String saveFlag = "";
    private String strParent = "";
    private String strCurDate = "";
    private boolean bAdd;
    private User currentUser = null;
    private Inventory inv = null;
    private SimpleDateFormat dFormat = null;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD1\r\n");
      out.write("Date    : 27-12-2007.\r\n");
      out.write("Author  : Anoop Varma, Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Quick Screen - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("        <script src=\"libjs/combobox/combobox.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/pgQuickScreen.js\"></script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgQuickScreen.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; Invalid username/password.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp: bAdd=" + bAdd);
            if (!bAdd) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; No Add permission.");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            dFormat = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
            strCurDate = dFormat.format(new java.util.Date());
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgQuickScreen.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='750' align=\"center\" id=\"formwindow\"><!-- width='710' -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Quick Screen </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmQuickScreen\" name=\"FrmQuickScreen\" method=\"POST\" action=\"pgSavCaseQS.jsp\"\r\n");
      out.write("                      onsubmit=\"if(confirmSave()) { return validate(); } return false;\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Reporter</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtEntryDate\">Entry date</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"3\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtEntryDate\" id=\"txtEntryDate\" size=\"12\" maxlength=\"12\" clear=1 title=\"Entry date\"\r\n");
      out.write("                                                       value=\"");
      out.print(strCurDate);
      out.write("\" readonly  onkeydown=\"return ClearInput(event,1);\">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select\" border='0' name=\"imgCal\"></a></td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\" colspan=\"2\"><label for=\"txtRecvDate\">Received Date</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtRecvDate\" id=\"txtRecvDate\" size=\"12\" maxlength=\"12\" clear=2 title=\"Received Date\"\r\n");
      out.write("                                                       readonly  onkeydown=\"return ClearInput(event,2);\">\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                            <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" alt=\"Select\" title=\"Select\" border='0' name=\"imgCal\"></a></td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <script type=\"text/javascript\">\r\n");
      out.write("                                Calendar.setup({\r\n");
      out.write("                                    inputField : \"txtEntryDate\", //*\r\n");
      out.write("                                    ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                    showsTime : true,\r\n");
      out.write("                                    button : \"cal_trigger1\", //*\r\n");
      out.write("                                    step : 1\r\n");
      out.write("                                });\r\n");
      out.write("                                Calendar.setup({\r\n");
      out.write("                                    inputField : \"txtRecvDate\", //*\r\n");
      out.write("                                    ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                    showsTime : true,\r\n");
      out.write("                                    button : \"cal_trigger2\", //*\r\n");
      out.write("                                    step : 1\r\n");
      out.write("                                });\r\n");
      out.write("                            </script>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtClinRep\">Clinical Reporter</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtClinRep\" id=\"txtClinRep\" size=\"98\" maxlength=\"100\" title=\"Clinical Reporter Information\" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtCountryCode\">Country</label></td>\r\n");
      out.write("                                <td colspan=\"9\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtCountryCode\" name=\"txtCountryCode\" size=\"45\" onkeyup=\"ajax_showOptions('txtCountryCode_ID',this,'Country',event)\">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"3\" id=\"txtCountryCode_ID\" name=\"txtCountryCode_ID\" value=\"\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Product</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode\">Details</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtProdCode\" name=\"txtProdCode\" size=\"70\" onkeyup=\"ajax_showOptions('txtProdCode_ID',this,'Product',event)\">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"6\" id=\"txtProdCode_ID\" name=\"txtProdCode_ID\" value=\"\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Patient</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtInitials\">Initials</label></td>\r\n");
      out.write("                                <td >\r\n");
      out.write("                                    <input type=\"text\" name=\"txtInitials\" id=\"txtInitials\" size=\"5\" maxlength=\"4\" title=\"Patient Initials\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtAgeRep\">Age Reported</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtAgeRep\" id=\"txtAgeRep\" size=\"3\" maxlength=\"3\" title=\"Age Reported\"\r\n");
      out.write("                                           onkeydown=\"return checkIsNumeric(event);\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"txtSexCode\">Sex</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtSexCode\" name=\"txtSexCode\" value=\"\" onkeyup=\"ajax_showOptions('txtSexCode_ID',this,'Sex',event)\">\r\n");
      out.write("                                    <input size=\"3\" type=\"text\" readonly id=\"txtSexCode_ID\" name=\"txtSexCode_ID\" value=\"\" >\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtTrial\">Source/Trail</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtTrial\" name=\"txtTrial\" size=\"30\" onkeyup=\"ajax_showOptions('txtTrial_ID',this,'TrialNo',event)\">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"3\" id=\"txtTrial_ID\" name=\"txtTrial_ID\" value=\"\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"txtPatNum\">Patient Number/Centre</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtPatNum\" id=\"txtPatNum\" size=\"30\" maxlength=\"20\" title=\"Patient Number/Centre\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Adverse Event</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtLLTCode\">Low Level Term</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                                    <div id=\"divLLTDynData\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtLLTCode\" name=\"txtLLTCode\" size=\"45\" onkeyup=\"ajax_showOptions('txtLLTCode_ID',this,'AELLT',event)\">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"10\" id=\"txtLLTCode_ID\" name=\"txtLLTCode_ID\" value=\"\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"chkAggrLLT\">Aggravation of</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"chkAggrLLT\" id=\"chkAggrLLT\" class=\"chkbox\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtPTCode\">Preferred Term</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                                    <div id=\"divDiagDynData\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtPTCode\" name=\"txtPTCode\" size=\"45\" onkeyup=\"ajax_showOptions('txtPTCode_ID',this,'AEPT',event)\">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"10\" id=\"txtPTCode_ID\" name=\"txtPTCode_ID\" value=\"\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"chkAggrPT\">Aggravation of</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"chkAggrPT\" id=\"chkAggrPT\" title=\"Aggravation of\" class=\"chkbox\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtBodySysNum\">System Organ Class</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <div id=\"divBodySysDynData\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtBodySysNum\" name=\"txtBodySysNum\" size=\"45\" onkeyup=\"ajax_showOptions('txtBodySysNum_ID',this,'BodySys',event)\">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"10\" id=\"txtBodySysNum_ID\" name=\"txtBodySysNum_ID\" value=\"\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtClasfCode\">Seriousness</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <div id=\"divClasfDynData\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtClasfCode\" name=\"txtClasfCode\" size=\"50\" onkeyup=\"ajax_showOptions('txtClasfCode_ID',this,'Seriousness',event)\">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"5\" id=\"txtClasfCode_ID\" name=\"txtClasfCode_ID\" value=\"\">\r\n");
      out.write("\r\n");
      out.write("                                        <input style=\"border: 0; font-color: red\" type=\"text\" name=\"txtClasfType\" id=\"txtClasfType\" size=\"12\" maxlength=\"12\"\r\n");
      out.write("                                               readonly value=\"NON SERIOUS\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtOutcomeCode\">AE Outcome</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <div id=\"divOCDynData\">\r\n");
      out.write("                                        <input type=\"text\" id=\"txtOutcomeCode\" name=\"txtOutcomeCode\" size=\"50\" onkeyup=\"ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)\">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"5\" id=\"txtOutcomeCode_ID\" name=\"txtOutcomeCode_ID\" value=\"\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Assessment</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtPhyAssCode\">Physician Assessment</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtPhyAssCode\" name=\"txtPhyAssCode\" size=\"22\" onkeyup=\"ajax_showOptions('txtPhyAssCode_ID',this,'AssCausality',event)\">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"3\" id=\"txtPhyAssCode_ID\" name=\"txtPhyAssCode_ID\" value=\"\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"txtCoAssCode\">Company Assessment</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"4\">\r\n");
      out.write("                                    <input type=\"text\" id=\"txtCoAssCode\" name=\"txtCoAssCode\" size=\"22\" onkeyup=\"ajax_showOptions('txtCoAssCode_ID',this,'AssCausality',event)\">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"3\" id=\"txtCoAssCode_ID\" name=\"txtCoAssCode_ID\" value=\"\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtNarrative\">Complementary Information</label></td>\r\n");
      out.write("                                <td colspan=\"9\">\r\n");
      out.write("                                    <textarea name=\"txtNarrative\" id=\"txtNarrative\" cols=\"110\" wrap=\"physical\" title=\"Complementary Information\"\r\n");
      out.write("                                              onkeydown=\"return checkCharCount(document.FrmQuickScreen.txtNarrative,event, 1000);\"></textarea>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdList\" class=\"button\" value=\"List similar Cases\" style=\"width: 120px;\" title=\"List\"\r\n");
      out.write("                                           onclick=\"return populateList();\">\r\n");
      out.write("                                    <input type=\"button\" name=\"cmdClearList\" class=\"button\" value=\"Clear List\" style=\"width: 120px;\" title=\"Clear List\"\r\n");
      out.write("                                           onclick=\"return clearList();\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"3\" align=\"right\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSave\" class=\"button\" value=\"Save\" style=\"width: 60px;\" title=\"Save\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"0\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdCancel\" class=\"button\"\r\n");
      out.write("                                           value=\"Cancel\" title=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('content.jsp');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                <script language=\"JavaScript\" type=\"text/JavaScript\">\r\n");
      out.write("                    function shiftTo(x,y) {\r\n");
      out.write("                        var id = \"cb1_scroll_div\";\r\n");
      out.write("                        var el = (document.getElementById)? document.getElementById(id): (document.all)? document.all[id]: (document.layers)? getLyrRef(id,document): null;\r\n");
      out.write("\r\n");
      out.write("                        this.doc = (document.layers)? this.el.document: this.el;\r\n");
      out.write("\r\n");
      out.write("                        var css = (el.style)? el.style: el;\r\n");
      out.write("                        var px = (document.layers||window.opera)? \"\": \"px\";\r\n");
      out.write("\r\n");
      out.write("                        if (css.moveTo) {\r\n");
      out.write("                            alert(\"1\");\r\n");
      out.write("                            css.moveTo(Math.round(x),Math.round(y));\r\n");
      out.write("                        } else { alert(\"2\");\r\n");
      out.write("                            css.left=Math.round(x)+\"px\";\r\n");
      out.write("                            css.top=Math.round(y)+\"px\";\r\n");
      out.write("                        }\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("                    function siv(nm,top){\r\n");
      out.write("                        if(document.getElementById){\r\n");
      out.write("                            var element=document.getElementById(nm);\r\n");
      out.write("                            if(element.scrollIntoView){\r\n");
      out.write("                                element.scrollIntoView(top);\r\n");
      out.write("                            }\r\n");
      out.write("                            else { notSupported(); }\r\n");
      out.write("                        }\r\n");
      out.write("                        else { notSupported(); }\r\n");
      out.write("                    }\r\n");
      out.write("                </script>\r\n");
      out.write("            </tr>\r\n");
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <div id=\"divDupCases\" style=\"display:none\" >\r\n");
      out.write("                    </div>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
