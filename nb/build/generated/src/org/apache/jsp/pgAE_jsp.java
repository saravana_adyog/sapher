package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import bl.sapher.Adverse_Event;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;

public final class pgAE_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD2\r\n");
      out.write("Date    : 22-01-2008.\r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Adverse Event - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <!--Calendar Library Includes.. S => -->\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("        <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("        <!-- import the calendar script -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("        <!-- import the language module -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("        <!-- helper script that uses the calendar -->\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("        <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <script>\r\n");
      out.write("            var SAPHER_PARENT_NAME = \"pgAE\";\r\n");
      out.write("\r\n");
      out.write("            function validate(){\r\n");
      out.write("                if(!checkIsAlphaSpaceVar(document.getElementById('txtPrevReport').value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in previous report');\r\n");
      out.write("                    document.getElementById('txtPrevReport').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsAlphaSpaceVar(document.getElementById('txtEvtTreatment').value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in event treatment');\r\n");
      out.write("                    document.getElementById('txtEvtTreatment').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function setRepFlagControls() {\r\n");
      out.write("                if (document.FrmAE.chkPrevReportFlag.checked) {\r\n");
      out.write("                    document.FrmAE.txtPrevReport.disabled      = false;\r\n");
      out.write("                }\r\n");
      out.write("                else {\r\n");
      out.write("                    document.FrmAE.txtPrevReport.value = \"\";\r\n");
      out.write("                    document.FrmAE.txtPrevReport.disabled      = true;\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function setLhaControls() {\r\n");
      out.write("                if (document.FrmAE.chkReportedToLha.checked) {\r\n");
      out.write("                    document.FrmAE.txtLhaCode.disabled      = false;\r\n");
      out.write("                    document.FrmAE.txtLhaCode_ID.disabled   = false;\r\n");
      out.write("                    document.FrmAE.txtDateOfReport.disabled = false;\r\n");
      out.write("                }\r\n");
      out.write("                else {\r\n");
      out.write("                    document.FrmAE.txtLhaCode.value = \"\";\r\n");
      out.write("                    document.FrmAE.txtLhaCode_ID.value = \"\";\r\n");
      out.write("                    document.FrmAE.txtDateOfReport.value = \"\";\r\n");
      out.write("                    document.FrmAE.txtLhaCode.disabled = true;\r\n");
      out.write("                    document.FrmAE.txtLhaCode_ID.disabled = true;\r\n");
      out.write("                    document.FrmAE.txtDateOfReport.disabled = true;\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function setControls() {\r\n");
      out.write("                setRepFlagControls();\r\n");
      out.write("                setLhaControls();\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            function validateAEFields(){\r\n");
      out.write("                if(document.FrmAE.txtLLTCode.value == '') {\r\n");
      out.write("                    window.alert('Please provide Adverse Event LLT.');\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body onLoad=\"setControls()\">\r\n");
      out.write("        ");
    Adverse_Event anAE = new Adverse_Event((String) session.getAttribute("LogFileName"));
        final String FORM_ID = "CSD1";
        String saveFlag = "";
        String strParent = "";
        boolean bAdd;
        boolean bEdit;
        //private boolean bDelete;
        boolean bView;
        User currentUser = null;
        Inventory inv = null;
        String strRepNum = "";
        String strLltCode = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgAE.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }

        if (request.getParameter("AeSaveFlag") != null) {
            saveFlag = request.getParameter("AeSaveFlag");
        }

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }

        if (request.getParameter("LltCode") != null) {
            strLltCode = request.getParameter("LltCode");
        }

        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; UserBlocked exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp: Edit mode. Rep#:" + strRepNum + ", LLT=" + strLltCode);
                if ((strRepNum != null) && strLltCode != null) {
                    anAE = new Adverse_Event((String) session.getAttribute("Company"), "y", Integer.parseInt(strRepNum), strLltCode);
                } else {
                    //pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp: Insertion mode.");
                anAE = new Adverse_Event();
            }

        } catch (RecordNotFoundException e) {
            //pageContext.forward("content.jsp?Status=0");
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgAE.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='700' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Adverse Event</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmAE\" name=\"FrmAE\" method=\"POST\" action=\"pgSavAE.jsp\"\r\n");
      out.write("                      onsubmit=\"return validate();\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Adverse Event</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtRepNum\">Rep Num</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtRepNum\"\r\n");
      out.write("                                                             id=\"txtRepNum\" size=\"15\" maxlength=\"10\"\r\n");
      out.write("                                                             ");

        out.write(" value=\"" + strRepNum + "\" readonly ");
                                                             
      out.write(" title=\"Report number\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtLLTCode\">Adverse Event LLT</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                                    <div id=\"divLLTDynData\">\r\n");
      out.write("\r\n");
      out.write("                                        <input type=\"text\" id=\"txtLLTCode\" name=\"txtLLTCode\" size=\"45\" onKeyUp=\"ajax_showOptions('txtLLTCode_ID',this,'AELLT',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getLlt_Desc() == null ? "" : anAE.getLlt_Desc()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"10\" id=\"txtLLTCode_ID\" name=\"txtLLTCode_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getLlt_Code() == null ? "" : anAE.getLlt_Code()));
      out.write("\">\r\n");
      out.write("                                        <a href=\"#\" onclick=\"popupWnd('spopup','no','pgMedDraSoc.jsp', 800, 600)\">\r\n");
      out.write("                                            <img src=\"images/icons/star_on-16x16.gif\" alt=\"Select\" title=\"Popup MedDRA Browser\" border='0' name=\"imgSearch\">\r\n");
      out.write("                                        </a>\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"chkAggrLLT\">Aggravation of</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"chkAggrLLT\" id=\"chkAggrLLT\" class=\"chkbox\" title=\"Aggravation of Adverse Event Low Level Term\"\r\n");
      out.write("                                           ");

        if (anAE.getAggr_Of_Llt() != null) {
            if (anAE.getAggr_Of_Llt().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtPTCode\">Adverse Event PT</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"6\">\r\n");
      out.write("                                    <div id=\"divDiagDynData\">\r\n");
      out.write("\r\n");
      out.write("                                        <input type=\"text\" id=\"txtPTCode\" name=\"txtPTCode\" size=\"45\" onKeyUp=\"ajax_showOptions('txtPTCode_ID',this,'AEPT',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getDiag_Desc() == null ? "" : anAE.getDiag_Desc()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"10\" id=\"txtPTCode_ID\" name=\"txtPTCode_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getDiag_Code() == null ? "" : anAE.getDiag_Code()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"fLabelR\"><label for=\"chkAggrPT\">Aggravation of</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"chkAggrPT\" id=\"chkAggrPT\" class=\"chkbox\" title=\"Aggravation of Adverse Event Preferred Term\"\r\n");
      out.write("                                           ");

        if (anAE.getAggr_Of_Diag() != null) {
            if (anAE.getAggr_Of_Diag().equalsIgnoreCase("y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtBodySysNum\">System Organ Class</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <div id=\"divBodSysDynData\">\r\n");
      out.write("\r\n");
      out.write("                                        <input type=\"text\" id=\"txtBodySysNum\" name=\"txtBodySysNum\" size=\"45\" onKeyUp=\"ajax_showOptions('txtBodySysNum_ID',this,'BodySys',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getBody_System() == null ? "" : anAE.getBody_System()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"10\" id=\"txtBodySysNum_ID\" name=\"txtBodySysNum_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getBody_Sys_Num() == null ? "" : anAE.getBody_Sys_Num()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtClasfCode\">Seriousness</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("\r\n");
      out.write("                                    <input type=\"text\" id=\"txtClasfCode\" name=\"txtClasfCode\" size=\"50\" onKeyUp=\"ajax_showOptions('txtClasfCode_ID',this,'Seriousness',event)\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getClassification_Desc() == null ? "" : anAE.getClassification_Desc()));
      out.write("\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"5\" id=\"txtClasfCode_ID\" name=\"txtClasfCode_ID\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getClassification_Code() == null ? "" : anAE.getClassification_Code()));
      out.write("\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtOutcomeCode\">Total AE Outcome</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <div id=\"divOCDynData\">\r\n");
      out.write("\r\n");
      out.write("                                        <input type=\"text\" id=\"txtOutcomeCode\" name=\"txtOutcomeCode\" size=\"50\" onKeyUp=\"ajax_showOptions('txtOutcomeCode_ID',this,'AEOutcome',event)\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getOutcome() == null ? "" : anAE.getOutcome()));
      out.write("\"\r\n");
      out.write("                                               ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                               
      out.write(">\r\n");
      out.write("                                        <input type=\"text\" readonly size=\"5\" id=\"txtOutcomeCode_ID\" name=\"txtOutcomeCode_ID\"\r\n");
      out.write("                                               value=\"");
      out.print((anAE.getOutcome_Code() == null ? "" : anAE.getOutcome_Code()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtSeverityCode\">Severity</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("\r\n");
      out.write("                                    <input type=\"text\" id=\"txtSeverityCode\" name=\"txtSeverityCode\" size=\"50\" onKeyUp=\"ajax_showOptions('txtSeverityCode_ID',this,'EventSeverity',event)\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getESeverity_Desc() == null ? "" : anAE.getESeverity_Desc()));
      out.write("\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"5\" id=\"txtSeverityCode_ID\" name=\"txtSeverityCode_ID\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getESeverity_Code() == null ? "" : anAE.getESeverity_Code()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"chkReportedToLha\">Reported to LHA</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"chkReportedToLha\" id=\"chkReportedToLha\" size=\"1\" class=\"chkbox\" title=\"Whether the event is reported to LHA?\"\r\n");
      out.write("                                           onclick=\"setLhaControls();\" ");

        if (anAE.getReported_To_LHA() != null) {
            if (anAE.getReported_To_LHA().equalsIgnoreCase("Y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           
      out.write(" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtLhaCode\">LHA</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("\r\n");
      out.write("                                    <input type=\"text\" id=\"txtLhaCode\" name=\"txtLhaCode\" size=\"50\" onKeyUp=\"ajax_showOptions('txtLhaCode_ID',this,'LHA',event)\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getLha_Desc() == null ? "" : anAE.getLha_Desc()));
      out.write("\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"text\" readonly size=\"12\" id=\"txtLhaCode_ID\" name=\"txtLhaCode_ID\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getLha_Code() == null ? "" : anAE.getLha_Code()));
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\">\r\n");
      out.write("                                    <label for=\"txtDateOfReport\">Date of Report</label>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtDateOfReport\" id=\"txtDateOfReport\" size=\"15\" maxlength=\"11\" title=\"Date of report\"\r\n");
      out.write("                                                       clear=107 value=\"");
      out.print((anAE.getReport_Date() == null ? "" : sdf.format(anAE.getReport_Date())));
      out.write("\" readonly\r\n");
      out.write("                                                       ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 107);\" ");
        }
      out.write(" />\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                ");

        if (!saveFlag.equals("V")) {
                                                
      out.write("\r\n");
      out.write("                                                <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" title=\"Select Date of report\"></a>\r\n");
      out.write("                                                <script type=\"text/javascript\">\r\n");
      out.write("                                                    Calendar.setup({\r\n");
      out.write("                                                        inputField : \"txtDateOfReport\", //*\r\n");
      out.write("                                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                        showsTime : true,\r\n");
      out.write("                                                        button : \"cal_trigger1\", //*\r\n");
      out.write("                                                        step : 1\r\n");
      out.write("                                                    });\r\n");
      out.write("                                                </script>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\">\r\n");
      out.write("                                    <label for=\"txtStartDate\">Start date</label>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtStartDate\" id=\"txtStartDate\" size=\"15\" maxlength=\"11\" title=\"Event start date\"\r\n");
      out.write("                                                       clear=108 value=\"");
      out.print((anAE.getDate_Start() == null ? "" : sdf.format(anAE.getDate_Start())));
      out.write("\" readonly\r\n");
      out.write("                                                       ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 108);\" ");
        }
      out.write(" />\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                ");

        if (!saveFlag.equals("V")) {
                                                
      out.write("\r\n");
      out.write("                                                <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" title=\"Select Event start date\"></a>\r\n");
      out.write("                                                <script type=\"text/javascript\">\r\n");
      out.write("                                                    Calendar.setup({\r\n");
      out.write("                                                        inputField : \"txtStartDate\", //*\r\n");
      out.write("                                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                        showsTime : true,\r\n");
      out.write("                                                        button : \"cal_trigger2\", //*\r\n");
      out.write("                                                        step : 1\r\n");
      out.write("                                                    });\r\n");
      out.write("                                                </script>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\">\r\n");
      out.write("                                    <label for=\"txtEndDate\">End date</label>\r\n");
      out.write("                                </td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                <input type=\"text\" name=\"txtEndDate\" id=\"txtEndDate\" size=\"15\" maxlength=\"11\" title=\"Event end date\"\r\n");
      out.write("                                                       clear=109 value=\"");
      out.print((anAE.getDate_Stop() == null ? "" : sdf.format(anAE.getDate_Stop())));
      out.write("\" readonly\r\n");
      out.write("                                                       ");
 if (!saveFlag.equals("V")) {
            out.write(" onkeydown=\"return ClearInput(event, 109);\" ");
        }
      out.write("\r\n");
      out.write("                                                   </td>\r\n");
      out.write("                                            <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                                                ");

        if (!saveFlag.equals("V")) {
                                                
      out.write("\r\n");
      out.write("                                                <a href=\"#\" id=\"cal_trigger3\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" title=\"Select Event end date\"></a>\r\n");
      out.write("                                                <script type=\"text/javascript\">\r\n");
      out.write("                                                    Calendar.setup({\r\n");
      out.write("                                                        inputField : \"txtEndDate\", //*\r\n");
      out.write("                                                        ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                                                        showsTime : true,\r\n");
      out.write("                                                        button : \"cal_trigger3\", //*\r\n");
      out.write("                                                        step : 1\r\n");
      out.write("                                                    });\r\n");
      out.write("                                                </script>\r\n");
      out.write("                                                ");
}
      out.write("\r\n");
      out.write("                                            </td>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </table>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"chkLabelling\">Expected</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"ChkLabelling\" id=\"ChkLabelling\" size=\"1\" class=\"chkbox\" title=\"Whether it is labelled?\"\r\n");
      out.write("                                           ");

        if (anAE.getLabelling() != null) {
            if (anAE.getLabelling().equalsIgnoreCase("E")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           
      out.write(" >\r\n");
      out.write("                                    <label class=\"fLabel\">&nbsp;&nbsp;(Labelling)</label>\r\n");
      out.write("                                </td>\r\n");
      out.write("\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"chkPrevReportFlag\">Prev. reporting flag</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"checkbox\" class=\"chkbox\" name=\"chkPrevReportFlag\" id=\"chkPrevReportFlag\" size=\"2\" maxlength=\"1\"\r\n");
      out.write("                                           title=\"Previous reporting flag\"\r\n");
      out.write("                                           onclick=\"setRepFlagControls();\" ");

        if (anAE.getPrev_Report_Flg() != null) {
            if (anAE.getPrev_Report_Flg().equalsIgnoreCase("Y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtPrevReport\">Previous report</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtPrevReport\" id=\"txtPrevReport\" size=\"50\" maxlength=\"50\"  title=\"Previous report\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getPrev_Report() == null ? "" : anAE.getPrev_Report()));
      out.write("\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"txtEvtTreatment\">Event treatment</label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"9\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtEvtTreatment\" id=\"txtEvtTreatment\" size=\"100\" maxlength=\"100\" title=\"Event treatment\"\r\n");
      out.write("                                           value=\"");
      out.print((anAE.getEvent_Treatment() == null ? "" : anAE.getEvent_Treatment()));
      out.write("\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                           
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><label for=\"chkEvtAbateOnStop\">Event abate on stop</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"checkbox\" name=\"chkEvtAbateOnStop\" id=\"chkEvtAbateOnStop\" size=\"1\" class=\"chkbox\"\r\n");
      out.write("                                           title=\"Event abate on stop\"\r\n");
      out.write("                                           ");

        if (anAE.getEvent_Abate_On_Stop() != null) {
            if (anAE.getEvent_Abate_On_Stop().equalsIgnoreCase("Y")) {
                out.write(" checked ");
            }
        }
        if (saveFlag.equals("V")) {
            out.write(" disabled ");
        }
                                           
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSave\"  onclick=\"return validateAEFields();\" class=\"");
      out.print((saveFlag.equals("V") ? "button disabled" : "button"));
      out.write("\" value=\"Save\" style=\"width: 60px;\"\r\n");
      out.write("                                           title=\"Save the Adverse Event\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"SaveFlag\" name=\"SaveFlag\" value=\"");
      out.print(saveFlag);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"");
      out.print(strParent);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"RepNum\" name=\"RepNum\" value=\"");
      out.print(strRepNum);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"DiagNum\" name=\"DiagNum\" value=\"");
      out.print(strLltCode);
      out.write("\">\r\n");
      out.write("                                    <input type=\"reset\" id=\"cmdCancel\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\" title=\"Cancel the operation\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('");
      out.print("pgCaseSummary.jsp?RepNum=" + strRepNum);
      out.write("')\"/>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
