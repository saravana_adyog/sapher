package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import bl.sapher.Manufacturer;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.Logger;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;

public final class pgManufacturer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    Manufacturer mfr = null;
    private final String FORM_ID = "CDL6";
    private String saveFlag = "";
    private String strParent = "";
    private boolean bAdd;
    private boolean bEdit;
    private boolean bDelete;
    private boolean bView;
    private User currentUser = null;
    private Inventory inv = null;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CDL6\r\n");
      out.write("Date    : 13-12-2007.\r\n");
      out.write("Author  : Arun P. Jose, Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Manufacturer - [Sapher]</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <script>\r\n");
      out.write("            function validate(){\r\n");
      out.write("                if (document.FrmManufacturer.txtManuCode.value == \"\"){\r\n");
      out.write("                    alert( \"Manufacturer Code cannot be empty.\");\r\n");
      out.write("                    document.FrmManufacturer.txtManuCode.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if (trim(document.FrmManufacturer.txtManuDesc.value) == \"\"){\r\n");
      out.write("                    alert( \"Manufacturer name cannot be empty.\");\r\n");
      out.write("                    document.FrmManufacturer.txtManuDesc.focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsAlphaVar(document.FrmManufacturer.txtManuCode.value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in manufacturer code.');\r\n");
      out.write("                    document.getElementById('txtManuCode').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsAlphaSpaceVar(document.FrmManufacturer.txtManuDesc.value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in manufacturer name.');\r\n");
      out.write("                    document.getElementById('txtManuDesc').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsPhoneVar(document.FrmManufacturer.txtManuPhone.value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in phone number.');\r\n");
      out.write("                    document.getElementById('txtManuPhone').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(!checkIsFaxVar(document.FrmManufacturer.txtManuFax.value)) {\r\n");
      out.write("                    window.alert('Invalid character(s) in fax.');\r\n");
      out.write("                    document.getElementById('txtManuFax').focus();\r\n");
      out.write("                    return false;\r\n");
      out.write("                }\r\n");
      out.write("                if(document.FrmManufacturer.txtManuEmail.value != \"\") {\r\n");
      out.write("                    if(!validateEMail(document.FrmManufacturer.txtManuEmail.value)) {\r\n");
      out.write("                        window.alert('Invalid e-mail.');\r\n");
      out.write("                        document.getElementById('txtManuEmail').focus();\r\n");
      out.write("                        return false;\r\n");
      out.write("                    }\r\n");
      out.write("                }\r\n");
      out.write("                return true;\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgManufacturer.jsp");
        if (request.getParameter("Parent") != null) {
            strParent = request.getParameter("Parent");
        } else {
            strParent = "0";
        }
        if (request.getParameter("SaveFlag") != null) {
            saveFlag = request.getParameter("SaveFlag");
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            String strPW = (String) session.getAttribute("CurrentUserPW");
            if (!currentUser.getPassword().equals(strPW)) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; Wrong username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            }
        } catch (TimeoutException toe) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (UserBlockedException ube) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; UserBlocked exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        } catch (Exception e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; User already logged in.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }
        try {
            inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
            bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
            bEdit = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'E');
            bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp: A/E/V=" + bAdd + "/" + bEdit + "/" + bView);
            if (!bAdd && !bEdit && !bView) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; No permission." + "[Add: " + bAdd + "Edit: " + bEdit + "View:" + bView + "]");
                pageContext.forward("content.jsp?Status=1");
                return;
            }
            if (!bAdd && !bEdit) {
                saveFlag = "V";
            } else {
                session.setAttribute("DirtyFlag", "true");
            }
            if (!saveFlag.equals("I")) {
                if (request.getParameter("MfrCode") != null) {
                    mfr = new Manufacturer((String) session.getAttribute("Company"), request.getParameter("MfrCode"));
                } else {
                    pageContext.forward("content.jsp?Status=0");
                    return;
                }
            } else {
                mfr = new Manufacturer();
            }
        } catch (RecordNotFoundException e) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgManufacturer.jsp; RecordNotFoundException");
            Logger.writeLog((String) session.getAttribute("LogFileName"), e.getMessage());

            pageContext.forward("content.jsp?Status=0");
            return;
        }
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='400' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Code List</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <form id=\"FrmManufacturer\" name=\"FrmManufacturer\" method=\"POST\" action=\"pgSavManufacturer.jsp\"\r\n");
      out.write("                      onsubmit=\"return validate();\">\r\n");
      out.write("                    <td class=\"formbody\">\r\n");
      out.write("                        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">");
      out.print((saveFlag.equals("I") ? "New " : (saveFlag.equals("V") ? "View " : "Edit ")));
      out.write("Manufacturer</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 5px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuCode\">Mfr. Code</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuCode\" title=\"Manufacturer Code\"\r\n");
      out.write("                                                             id=\"txtManuCode\" size=\"6\" maxlength=\"5\"\r\n");
      out.write("                                                             value=\"");
      out.print(mfr.getM_Code());
      out.write("\"\r\n");
      out.write("                                                             ");

        if (!saveFlag.equals("I")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuDesc\">Name</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuDesc\" id=\"txtManuDesc\" size=\"30\" title=\"Manufacturer\"\r\n");
      out.write("                                                             maxlength=\"50\" value=\"");
      out.print(mfr.getM_Name());
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuAddr1\">Address</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuAddr1\" id=\"txtManuAddr1\" size=\"30\"\r\n");
      out.write("                                                             maxlength=\"50\" value=\"");
      out.print((mfr.getM_Addr1() == null ? "" : mfr.getM_Addr1()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuAddr2\"></label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuAddr2\" id=\"txtManuAddr2\" size=\"30\"\r\n");
      out.write("                                                             maxlength=\"50\" value=\"");
      out.print((mfr.getM_Addr2() == null ? "" : mfr.getM_Addr2()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuAddr3\"></label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuAddr3\" id=\"txtManuAddr3\" size=\"30\"\r\n");
      out.write("                                                             maxlength=\"50\" value=\"");
      out.print((mfr.getM_Addr3() == null ? "" : mfr.getM_Addr3()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuAddr4\"></label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuAddr4\" id=\"txtManuAddr43\" size=\"30\"\r\n");
      out.write("                                                             maxlength=\"50\" value=\"");
      out.print((mfr.getM_Addr4() == null ? "" : mfr.getM_Addr4()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuPhone\">Phone</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuPhone\" id=\"txtManuPhone\" size=\"30\"\r\n");
      out.write("                                                             maxlength=\"20\" value=\"");
      out.print((mfr.getM_Phone() == null ? "" : mfr.getM_Phone()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuFax\">Fax</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuFax\" id=\"txtManuFax\" size=\"30\"\r\n");
      out.write("                                                             maxlength=\"20\" value=\"");
      out.print((mfr.getM_Fax() == null ? "" : mfr.getM_Fax()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtManuEmail\">Email</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"text\" name=\"txtManuEmail\" id=\"txtManuEmail\" size=\"40\"\r\n");
      out.write("                                                             maxlength=\"80\" value=\"");
      out.print((mfr.getM_Email() == null ? "" : mfr.getM_Email()));
      out.write("\"\r\n");
      out.write("                                                             ");

        if (saveFlag.equals("V")) {
            out.write(" readonly ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"chkValid\">Active</label></td>\r\n");
      out.write("                                <td class=\"field\"><input type=\"checkbox\" name=\"chkValid\" id=\"chkValid\" title=\"Status\" class=\"chkbox\"\r\n");
      out.write("                                                             ");

        if (mfr.getIs_Valid() == 1) {
            out.write("CHECKED ");
        }
        if (saveFlag.equals("V")) {
            out.write("DISABLED ");
        }
                                                             
      out.write(">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"submit\" name=\"cmdSave\" class=\"");
      out.print((saveFlag.equals("V") ? "button disabled" : "button"));
      out.write("\" value=\"Save\" style=\"width: 60px;\"\r\n");
      out.write("                                           ");

        if (saveFlag.equals("V")) {
            out.write(" DISABLED ");
        }
                                           
      out.write(">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"SaveFlag\" name=\"SaveFlag\" value=\"");
      out.print(saveFlag);
      out.write("\">\r\n");
      out.write("                                    <input type=\"hidden\" id=\"Parent\" name=\"Parent\" value=\"");
      out.print(strParent);
      out.write("\">\r\n");
      out.write("                                    <input type=\"reset\" name=\"cmdCancel\" class=\"button\" value=\"Cancel\" style=\"width: 60px;\"\r\n");
      out.write("                                           onclick=\"javascript:location.replace('");
      out.print(!strParent.equals("0") ? "pgNavigate.jsp?Cancellation=1&Target=pgManufacturerlist.jsp" : "content.jsp");
      out.write("');\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </table>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </form>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
