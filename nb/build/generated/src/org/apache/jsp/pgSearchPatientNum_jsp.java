package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import bl.sapher.CaseDetails;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;

public final class pgSearchPatientNum_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private String FORM_ID = "";
    private String strCtryName = "";
    private boolean bAdd;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nCurPage;
    private Inventory inv = null;
    private String strPNum;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : \r\n");
      out.write("Date    : 12-04-2008.\r\n");
      out.write("Author  : Anoop Varma\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Patient number Filter- [Sapher]</title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        <script>\r\n");
      out.write("            function doClose(id, name) {\r\n");
      out.write("                var tmp = (opener.SAPHER_PARENT_NAME.localeCompare('pgQueryTool'));\r\n");
      out.write("                if(tmp == '0') {\r\n");
      out.write("                    opener.document.FrmQueryTool.txtPatNum.style.color = \"#000000\";\r\n");
      out.write("                    opener.document.FrmQueryTool.txtPatNum.style.fontWeight= \"normal\";\r\n");
      out.write("                    opener.document.FrmQueryTool.txtPatNum.value = id;\r\n");
      out.write("                }\r\n");
      out.write("\r\n");
      out.write("                window.close();\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgSearchPatientNum.jsp");
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchPatientNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Invalid username/password");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                    return;
                }
                if (request.getParameter("FormId") == null) {
                    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchPatientNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Permission denied.");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.document.location.replace(\"content.jsp?Status=1\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                    return;
                } else {
                    FORM_ID = request.getParameter("FormId");
                }
                inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
                bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
                bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchPatientNum.jsp: A/V=" + bAdd + "/" + bView);
            } catch (TimeoutException toe) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchPatientNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

                return;
            } catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchPatientNum.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgSearchPatientNum.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Permission denied. [Add: " + bAdd + ",View: " + bView + "]");
        
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            opener.document.location.replace(\"content.jsp?Status=1\");\r\n");
      out.write("            window.close();\r\n");
      out.write("        </script>\r\n");
      out.write("        ");

            return;
        }

//        if (request.getParameter("txtSrchPNum") == null) {
//            strCtryName = "";
//        } else {
//            strCtryName = request.getParameter("txtSrchPNum");
//        }

        String strValid = "";
        if (request.getParameter("ValidSrchPNum") == null) {
            strValid = "All";
        } else {
            strValid = request.getParameter("ValidSrchPNum");
        }

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + (String) session.getAttribute("Company"));
        al = CaseDetails.listPatientNum((String) session.getAttribute("Company"));

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            nCurPage = Integer.parseInt(request.getParameter("CurPage"));
        }

        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 15px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">Filter - Patient Number</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <FORM name=\"FrmCountryList\" METHOD=\"POST\" ACTION=\"pgSearchPatientNum.jsp\" >\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"form_group_title\" colspan=\"10\">Filter Criteria</td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td style=\"height: 10px;\"></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <input type=\"hidden\" id=\"FormId\" name=\"FormId\" value=\"");
      out.print(FORM_ID);
      out.write("\">\r\n");
      out.write("                            <input type=\"hidden\" id=\"ValidSrchPNum\" name=\"ValidSrchPNum\" value=\"");
      out.print(strValid);
      out.write("\">\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label for=\"txtSrchPNum\">Name</label></td>\r\n");
      out.write("                                <td class=\"field\">\r\n");
      out.write("                                    <input type=\"text\" name=\"txtSrchPNum\" id=\"txtSrchPNum\" size=\"30\" maxlength=\"50\"\r\n");
      out.write("                                           value=\"");
      out.print(strCtryName);
      out.write("\" title=\"Patient Number\">\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\" width=\"125\"><label></label></td>\r\n");
      out.write("                                <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                                    <span style=\"float: left\">\r\n");
      out.write("                                        <input type=\"submit\" name=\"cmdFilter\" title=\"Start filtering with specified criteria\" class=\"button\" value=\"Filter\" style=\"width: 87px;\">\r\n");
      out.write("                                    </span>\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 1px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"form_group_title\" colspan=\"10\">Filter Results\r\n");
      out.write("                                ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");
 if (al.size() != 0) {
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("                        <tr class=\"grid_header\">\r\n");
      out.write("                            <td width=\"100\">Patient Num</td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <tr class=\"even_row\">\r\n");
      out.write("                            <td width=\"16\" align=\"center\" >\r\n");
      out.write("                                <a  onclick=\"doClose('');\" href=\"#\">NONE</a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td width=\"100\" align='center'></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");

     for (int i = 0;
             i < al.size();
             i++) {
         strPNum = ((String) al.get(i));
                        
      out.write("\r\n");
      out.write("                        <tr class=\"\r\n");
      out.write("                            ");

                            if (i % 2 != 0) {
                                out.write("even_row");
                            } else {
                                out.write("odd_row");
                            }
                            
      out.write("\r\n");
      out.write("                            \">\r\n");
      out.write("                            <td width=\"100\" align='center'>\r\n");
      out.write("                                ");
      out.write("\r\n");
      out.write("                                <font>\r\n");
      out.write("                                    <a  onclick=\"doClose('");
      out.print(strPNum);
      out.write("');\" href=\"#\">");
      out.print(strPNum);
      out.write("</a>\r\n");
      out.write("                                </font>\r\n");
      out.write("                                ");
      out.write("\r\n");
      out.write("                            </td>\r\n");
      out.write("                            <td></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");
 }
      out.write("\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("                <tr>\r\n");
      out.write("                </tr>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Ends -->\r\n");
      out.write("            ");
}
      out.write("\r\n");
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
