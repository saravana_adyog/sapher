package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import bl.sapher.CaseDetails;
import bl.sapher.User;
import bl.sapher.Inventory;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.TimeoutException;
import bl.sapher.general.UserBlockedException;
import bl.sapher.general.Logger;
import bl.sapher.general.Constants;

public final class pgCaselist_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private final String FORM_ID = "CSD1";
    private int intRepNum = -1;
    private String strRepNum = "";
    private String strPTCode = "";
    private String strPTDesc = "";
    private String strTrialNum = "";
    private String strProdCode = "";
    private String strGenericNm = "";
    private String strBrandNm = "";
    private String strDtFrom = "";
    private String strDtTo = "";
    private int nCaseSuspended = 0;
    private boolean bAdd;
    private boolean bDelete;
    private boolean bView;
    private ArrayList al = null;
    private User currentUser = null;
    private int nRowCount = 10;
    private int nPageCount;
    private int nCurPage;
    private Inventory inv = null;
    private String strMessageInfo = "";

    public void jspDestroy() {
    }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : CSD1\r\n");
      out.write("Date    : 19-12-2007.\r\n");
      out.write("Author  : Arun P. Jose\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>Case Listing - [Sapher]</title>\r\n");
      out.write("    <script>var SAPHER_PARENT_NAME = \"pgCaselist\";</script>\r\n");
      out.write("    <script src=\"libjs/gui.js\"></script>\r\n");
      out.write("    <script src=\"libjs/ajax/ajax.js\"></script>\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("    <!--Calendar Library Includes.. S => -->\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/aqua/theme.css\" title=\"Aqua\" />\r\n");
      out.write("    <link rel=\"alternate stylesheet\" type=\"text/css\" media=\"all\" href=\"libjs/calendar/skins/calendar-green.css\" title=\"green\" />\r\n");
      out.write("    <!-- import the calendar script -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar.js\"></script>\r\n");
      out.write("    <!-- import the language module -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/lang/calendar-en.js\"></script>\r\n");
      out.write("    <!-- helper script that uses the calendar -->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-helper.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/calendar/calendar-setup.js\"></script>\r\n");
      out.write("    <!--Calendar Library Includes.. E <= -->\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"libjs/Ajax2/ajax-dynamic-list.js\"></script>\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgCaselist.jsp");
        boolean resetSession = false;
        strMessageInfo = "";
        if (request.getParameter("DelStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("DelStatus")).equals("0")) {
                strMessageInfo = "Deletion Success.";
            } else if (((String) request.getParameter("DelStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Case details reference exists.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("DelStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Deletion Failed! Unknown reason.</font>";
            }
        }
        if (request.getParameter("SavStatus") != null) {
            resetSession = true;
            if (((String) request.getParameter("SavStatus")).equals("0")) {
                strMessageInfo = "Update Success.";
            } else if (((String) request.getParameter("SavStatus")).equals("1")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Code already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("2")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Name already exists.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("3")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Incorrect login credentials.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("4")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! DB connection failure.</font>";
            } else if (((String) request.getParameter("SavStatus")).equals("5")) {
                strMessageInfo = "<font color=\"red\">Updation Failed! Unknown reason.</font>";
            }
        }
        try {
            if (session.getAttribute("CurrentUser") == null && session.getAttribute("SapherSessionId") == null) {
                throw new TimeoutException("Server timeout.Login again.");
            }
            currentUser = new User((String) session.getAttribute("LogFileName"),(String) session.getAttribute("Company"), (String) session.getAttribute("CurrentUser"), (String) session.getAttribute("SapherSessionId"));
            if (!currentUser.getPassword().equals(session.getAttribute("CurrentUserPW"))) {
                Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Wrong username/password");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=1\");\r\n");
      out.write("</script>\r\n");

        return;
    }
    inv = new Inventory((String) session.getAttribute("LogFileName"), (String) session.getAttribute("Company"), FORM_ID);
    bAdd = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'A');
    bDelete = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'D');
    bView = currentUser.getPermission((String) session.getAttribute("Company"), inv, 'V');

    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp: A/V/D=" + bAdd + "/" + bView + "/" + bDelete);
} catch (TimeoutException toe) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Timeout exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=6\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (UserBlockedException ube) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; UserBlocked exception");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=5\");\r\n");
      out.write("</script>\r\n");

    return;
} catch (Exception e) {
    Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; User already logged in.");

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?LoginStatus=3\");\r\n");
      out.write("</script>\r\n");

            return;
        }

        if (bAdd && !bView) {
            pageContext.forward("pgCaseDetails.jsp?Parent=0&SaveFlag=I");
        }

        if (!bAdd && !bView) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgCaselist.jsp; RemoteIP: " + request.getRemoteAddr() + "; Customer: " + (String) session.getAttribute("Company") + "; Lack of permissions. [Add=" + bAdd + ", View=" + bView + "]");
            pageContext.forward("content.jsp?Status=1");
            return;
        }

        if (request.getParameter("Cancellation") != null) {
            session.removeAttribute("DirtyFlag");
        }
        if (request.getParameter("txtRepNum") == null) {
            if (session.getAttribute("txtRepNum") == null) {
                strRepNum = "";
            } else {
                strRepNum = (String) session.getAttribute("txtRepNum");
            }
        } else {
            strRepNum = request.getParameter("txtRepNum");
        }
        session.setAttribute("txtRepNum", strRepNum);

        if (strRepNum.equals("")) {
            intRepNum = -1;
        } else {
            intRepNum = Integer.parseInt(strRepNum);
        }

        if (request.getParameter("txtPTCode_ID") == null) {
            if (session.getAttribute("txtPTCode_ID") == null) {
                strPTCode = "";
            } else {
                strPTCode = (String) session.getAttribute("txtPTCode_ID");
            }
        } else {
            strPTCode = request.getParameter("txtPTCode_ID");
        }
        session.setAttribute("txtPTCode_ID", strPTCode);

        if (request.getParameter("txtPTDesc") == null) {
            if (session.getAttribute("txtPTDesc") == null) {
                strPTDesc = "";
            } else {
                strPTDesc = (String) session.getAttribute("txtPTDesc");
            }
        } else {
            strPTDesc = request.getParameter("txtPTDesc");
        }
        session.setAttribute("txtPTDesc", strPTDesc);

        if (request.getParameter("txtCaseTrial") == null) {
            if (session.getAttribute("txtCaseTrial") == null) {
                strTrialNum = "";
            } else {
                strTrialNum = (String) session.getAttribute("txtCaseTrial");
            }
        } else {
            strTrialNum = request.getParameter("txtCaseTrial");
        }
        session.setAttribute("txtCaseTrial", strTrialNum);

        if (request.getParameter("txtProdCode_ID") == null) {
            if (session.getAttribute("txtProdCode_ID") == null) {
                strProdCode = "";
            } else {
                strProdCode = (String) session.getAttribute("txtProdCode_ID");
            }
        } else {
            strProdCode = request.getParameter("txtProdCode_ID");
        }
        session.setAttribute("txtProdCode_ID", strProdCode);

        if (request.getParameter("txtGenericNm") == null) {
            if (session.getAttribute("txtGenericNm") == null) {
                strGenericNm = "";
            } else {
                strGenericNm = (String) session.getAttribute("txtGenericNm");
            }
        } else {
            strGenericNm = request.getParameter("txtGenericNm");
        }
        session.setAttribute("txtGenericNm", strGenericNm);

        if (request.getParameter("txtBrandNm") == null) {
            if (session.getAttribute("txtBrandNm") == null) {
                strBrandNm = "";
            } else {
                strBrandNm = (String) session.getAttribute("txtBrandNm");
            }
        } else {
            strBrandNm = request.getParameter("txtBrandNm");
        }
        session.setAttribute("txtBrandNm", strBrandNm);

        if (request.getParameter("txtCaseDtFrom") == null) {
            if (session.getAttribute("txtCaseDtFrom") == null) {
                strDtFrom = "";
            } else {
                strDtFrom = (String) session.getAttribute("txtCaseDtFrom");
            }
        } else {
            strDtFrom = request.getParameter("txtCaseDtFrom");
        }
        session.setAttribute("txtCaseDtFrom", strDtFrom);

        if (request.getParameter("txtCaseDtTo") == null) {
            if (session.getAttribute("txtCaseDtTo") == null) {
                strDtTo = "";
            } else {
                strDtTo = (String) session.getAttribute("txtCaseDtTo");
            }
        } else {
            strDtTo = request.getParameter("txtCaseDtTo");
        }
        session.setAttribute("txtCaseDtTo", strDtTo);

        String strSuspended = "";
        if (request.getParameter("cmbSuspendedCase") == null) {
            if (session.getAttribute("cmbSuspendedCase") == null) {
                strSuspended = "All";
            } else {
                strSuspended = (String) session.getAttribute("cmbSuspendedCase");
            }
        } else {
            strSuspended = request.getParameter("cmbSuspendedCase");
        }
        session.setAttribute("cmbSuspendedCase", strSuspended);

        if (strSuspended.equals("Active")) {
            nCaseSuspended = 0;
        } else if (strSuspended.equals("Suspended")) {
            nCaseSuspended = 1;
        } else if (strSuspended.equals("All")) {
            nCaseSuspended = -1;
        }

        if (resetSession) {
            intRepNum = -1;
            strRepNum = "";
            strPTCode = "";
            strTrialNum = "";
            strProdCode = "";
            strGenericNm = "";
            strBrandNm = "";
            strDtFrom = "";
            strDtTo = "";
            nCaseSuspended = -1;
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading full list");
            al = CaseDetails.listCaseFromView((String) session.getAttribute("Company"), -1, "", "", "", "", "", -1);
        } else {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "Loading list with val:" + intRepNum + "," + strPTCode + "," + strTrialNum + "," + strProdCode +
                    "," + strDtFrom + "," + strDtTo + "," + nCaseSuspended);
            al = CaseDetails.listCaseFromView((String) session.getAttribute("Company"), intRepNum, strPTCode, strTrialNum, strProdCode,
                    strDtFrom, strDtTo, nCaseSuspended);
        }

        if (request.getParameter("CurPage") == null) {
            nCurPage = 1;
        } else {
            int n = 1;
            try {
                n = Integer.parseInt(request.getParameter("CurPage"));
            } catch (NumberFormatException nfe) {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
    }
    if (n > 0) {
        nCurPage = n;
    } else {

      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    parent.document.location.replace(\"pgLogin.jsp?AppStabilityStatus=\" + ");
      out.print(Constants.APP_STABILITY_STATUS_ILLEGAL_OP);
      out.write(");\r\n");
      out.write("</script>\r\n");
            return;
            }
        }


      out.write("\r\n");
      out.write("\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("<table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("<tr>\r\n");
      out.write("    <td id=\"formtitle\">Case Listing</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <form name=\"FrmCaseList\" METHOD=\"POST\" ACTION=\"pgCaselist.jsp\" >\r\n");
      out.write("        <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"form_group_title\" colspan=\"10\">Search Criteria</td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td style=\"height: 10px;\"></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"125\"><label for=\"txtRepNum\">Rep Num</label></td>\r\n");
      out.write("            <td class=\"field\">\r\n");
      out.write("                <input type=\"text\" name=\"txtRepNum\" id=\"txtRepNum\" size=\"10\" maxlength=\"9\" title=\"Report Number\"\r\n");
      out.write("                       value=\"");
      out.print(strRepNum);
      out.write("\" />\r\n");
      out.write("            </td>\r\n");
      out.write("            <td width=\"500\" align=\"right\"><b>");
      out.print(strMessageInfo);
      out.write("</b></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"150\"><label for=\"txtPTCode\">Preferred Term</label></td>\r\n");
      out.write("            <td class=\"field\" width=\"300\" colspan=\"6\">\r\n");
      out.write("\r\n");
      out.write("                <input type=\"text\" id=\"txtPTCode\" name=\"txtPTCode\" size=\"30\"\r\n");
      out.write("                       onKeyUp=\"ajax_showOptions('txtPTCode_ID',this,'AEPT',event)\" />\r\n");
      out.write("                <input type=\"text\" readonly size=\"10\" id=\"txtPTCode_ID\" name=\"txtPTCode_ID\">\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"125\"><label for=\"txtCaseTrial\">Source/Trail</label></td>\r\n");
      out.write("            <td class=\"field\" width=\"300\">\r\n");
      out.write("                <input type=\"text\" id=\"txtCaseTrial\" name=\"txtCaseTrial\" size=\"36\"\r\n");
      out.write("                       onKeyUp=\"ajax_showOptions('txtCaseTrial_ID',this,'TrialNo',event)\" />\r\n");
      out.write("                <input type=\"hidden\" id=\"txtCaseTrial_ID\" name=\"txtCaseTrial_ID\" />\r\n");
      out.write("\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"125\"><label for=\"txtProdCode\">Product</label></td>\r\n");
      out.write("            <td class=\"field\" width=\"500\" colspan=\"9\">\r\n");
      out.write("                <input type=\"text\" id=\"txtProdCode\" name=\"txtProdCode\" size=\"57\"\r\n");
      out.write("                       onKeyUp=\"ajax_showOptions('txtProdCode_ID',this,'Product',event)\"/>\r\n");
      out.write("                <input type=\"text\" readonly size=\"13\" id=\"txtProdCode_ID\" name=\"txtProdCode_ID\">\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"125\"><label for=\"txtCaseDtFrom\">Date Range</label></td>\r\n");
      out.write("            <td class=\"field\">\r\n");
      out.write("                <table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("                    <tr>\r\n");
      out.write("                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtCaseDtFrom\" id=\"txtCaseDtFrom\" size=\"12\" maxlength=\"12\" clear=3 title=\"From Date\"\r\n");
      out.write("                                   value=\"");
      out.print(strDtFrom);
      out.write("\" readonly onKeyDown=\"return ClearInput(event,3);\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                        <a href=\"#\" id=\"cal_trigger1\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select From Date\"></a></td>\r\n");
      out.write("                        <td class=\"fLabel\" style=\"padding-right: 3px; padding-left: 3px;\">TO</td>\r\n");
      out.write("                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                            <input type=\"text\" name=\"txtCaseDtTo\" id=\"txtCaseDtTo\" size=\"13\" maxlength=\"12\" clear=4 title=\"To Date\"\r\n");
      out.write("                                   value=\"");
      out.print(strDtTo);
      out.write("\" readonly  onkeydown=\"return ClearInput(event,4);\">\r\n");
      out.write("                        </td>\r\n");
      out.write("                        <td style=\"padding-right: 3px; padding-left: 0px;\">\r\n");
      out.write("                        <a href=\"#\" id=\"cal_trigger2\"><img src=\"images/icons/cal.gif\" border='0' name=\"imgCal\" alt=\"Select\" title=\"Select To Date\"></a></td>\r\n");
      out.write("                    </tr>\r\n");
      out.write("                </table>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            Calendar.setup({\r\n");
      out.write("                inputField : \"txtCaseDtFrom\", //*\r\n");
      out.write("                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                showsTime : true,\r\n");
      out.write("                button : \"cal_trigger1\", //*\r\n");
      out.write("                step : 1\r\n");
      out.write("            });\r\n");
      out.write("        </script>\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("            Calendar.setup({\r\n");
      out.write("                inputField : \"txtCaseDtTo\", //*\r\n");
      out.write("                ifFormat : \"%d-%b-%Y\",\r\n");
      out.write("                showsTime : true,\r\n");
      out.write("                button : \"cal_trigger2\", //*\r\n");
      out.write("                step : 1\r\n");
      out.write("            });\r\n");
      out.write("        </script>\r\n");
      out.write("        <tr>\r\n");
      out.write("            <td class=\"fLabel\" width=\"125\"><label for=\"cmbSuspendedCase\">Status</label></td>\r\n");
      out.write("            <td class=\"field\" colspan=\"2\">\r\n");
      out.write("                <span style=\"float: left\">\r\n");
      out.write("                    <select id=\"cmbSuspendedCase\" NAME=\"cmbSuspendedCase\" class=\"comboclass\" style=\"width: 100px;\"\r\n");
      out.write("                            title=\"Select Status\">\r\n");
      out.write("                        <option ");
      out.print((strSuspended.equals("All") ? "SELECTED" : ""));
      out.write(">All</option>\r\n");
      out.write("                        <option ");
      out.print((strSuspended.equals("Active") ? "SELECTED" : ""));
      out.write(">Active</option>\r\n");
      out.write("                        <option ");
      out.print((strSuspended.equals("Suspended") ? "SELECTED" : ""));
      out.write(">Suspended</option>\r\n");
      out.write("                    </select>\r\n");
      out.write("                </span>\r\n");
      out.write("            </td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("        <td>\r\n");
      out.write("            <td colspan=\"2\">\r\n");
      out.write("                <input type=\"submit\" name=\"cmdSearch\" title=\"Start searching with specified criteria\" class=\"button\" value=\"Search\" style=\"width: 87px;\">\r\n");
      out.write("                <input type=\"button\" name=\"cmdClear\" title=\"Clear searching criteria\" class=\"button\" value=\"Clear Search\" style=\"width: 87px;\"\r\n");
      out.write("                       onclick=\"location.replace('pgNavigate.jsp?Target=pgCaselist.jsp');\"/>\r\n");
      out.write("            </td>\r\n");
      out.write("        </td>\r\n");
      out.write("    </form>\r\n");
      out.write("    <td>\r\n");
      out.write("        <form METHOD=\"POST\" ACTION=\"pgCaseDetails.jsp?SaveFlag=I\">\r\n");
      out.write("            <span style=\"float: right\">\r\n");
      out.write("                <input type=\"hidden\" name=\"SaveFlag\" value=\"I\">\r\n");
      out.write("                <input type=\"hidden\" name=\"Parent\" value=\"1\">\r\n");
      out.write("                <input type=\"submit\" name=\"cmdNew\"  title=\"Adds a new entry\"  class=\"");
      out.print((bAdd ? "button" : "button disabled"));
      out.write("\"\r\n");
      out.write("                       value=\"New Case\" style=\"width: 85px;\" ");
      out.print((bAdd ? "" : " DISABLED"));
      out.write(">\r\n");
      out.write("            </span>\r\n");
      out.write("        </form>\r\n");
      out.write("    </td>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Form Body Ends -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 1px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Grid View Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"form_group_title\" colspan=\"10\">Search Results\r\n");
      out.write("                    ");
      out.print(" - " + al.size() + " record(s) found");
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
 if (al.size() != 0) {

      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border='0' cellspacing='1' cellpadding='0' width='100%' class=\"grid_table\" >\r\n");
      out.write("            <tr class=\"grid_header\">\r\n");
      out.write("                <td width=\"16\"></td>\r\n");
      out.write("                ");
 if (bDelete) {
      out.write("\r\n");
      out.write("                <td width=\"16\"></td>\r\n");
      out.write("                ");
 }
      out.write("\r\n");
      out.write("                <td width=\"200\">Rep Num</td>\r\n");
      out.write("                <td width=\"100\">Source/Trail Num</td>\r\n");
      out.write("                <td width=\"100\">Rec. Date</td>\r\n");
      out.write("                <td width=\"50\">Status</td>\r\n");
      out.write("                <td></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");

     CaseDetails cd = null;
     SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", java.util.Locale.US);
     String strStatusFlag = "";

     nPageCount = (int) Math.ceil((double) al.size() / nRowCount);
     if (nCurPage > nPageCount) {
         nCurPage = 1;
     }
     int nStartPos = ((nCurPage - 1) * nRowCount);
     for (int i = nStartPos;
             i < ((nStartPos + nRowCount) > al.size() ? al.size() : (nStartPos + nRowCount));
             i++) {
         cd = ((CaseDetails) al.get(i));
         strStatusFlag = "" + cd.getIs_Suspended();
            
      out.write("\r\n");
      out.write("            <tr class=\"\r\n");
      out.write("                ");

                if (i % 2 == 0) {
                    out.write("even_row");
                } else {
                    out.write("odd_row");
                }
                
      out.write("\r\n");
      out.write("                \">\r\n");
      out.write("                <td width=\"16\" align=\"center\" >\r\n");
      out.write("                    <a href=\"pgCaseSummary.jsp?RepNum=");
      out.print(cd.getRep_Num());
      out.write("\">\r\n");
      out.write("                        <img src=\"images/icons/view.gif\" title=\"View details of the entry with ID = ");
      out.print(cd.getRep_Num());
      out.write("\" border='0' alt=\"View\">\r\n");
      out.write("                    </a>\r\n");
      out.write("                </td>\r\n");
      out.write("                ");
 if (bDelete) {
      out.write("\r\n");
      out.write("                <td width=\"16\" align=\"center\" >\r\n");
      out.write("                    <a href=\"pgDelCase.jsp?RepNum=");
      out.print(cd.getRep_Num());
      out.write("\"\r\n");
      out.write("                       onclick=\"return confirm('Are you sure you want to delete?','Sapher')\">\r\n");
      out.write("                        <img src=\"images/icons/delete.gif\" title=\"Delete the entry with ID = ");
      out.print(cd.getRep_Num());
      out.write("\" border='0' alt=\"Delete\">\r\n");
      out.write("                    </a>\r\n");
      out.write("                </td>\r\n");
      out.write("                ");
 }
      out.write("\r\n");
      out.write("                <td width=\"200\" align='left'>\r\n");
      out.write("                    ");

                if (strStatusFlag.equals("1")) {
                    
      out.write("\r\n");
      out.write("                    <font color=\"red\">");
      out.print(cd.getRep_Num());
      out.write("</font>\r\n");
      out.write("                    ");

                } else {
                    out.write("" + cd.getRep_Num());
                }
                    
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"100\" align='left'>\r\n");
      out.write("                    ");

                if (cd.getTrialnum() != null) {
                    if (strStatusFlag.equals("1")) {
                    
      out.write("\r\n");
      out.write("                    <font color=\"red\">");
      out.print(cd.getTrialnum());
      out.write("</font>\r\n");
      out.write("                    ");

                    } else {
                        out.write("" + cd.getTrialnum());
                    }
                }
                    
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"100\" align='left'>\r\n");
      out.write("                    ");

                if (cd.getDate_Recv() != null) {
                    if (strStatusFlag.equals("1")) {
                    
      out.write("\r\n");
      out.write("                    <font color=\"red\">");
      out.print(sdf.format(cd.getDate_Recv()));
      out.write("</font>\r\n");
      out.write("                    ");

                    } else {
                        out.write(sdf.format(cd.getDate_Recv()));
                    }
                }
                    
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"50\">\r\n");
      out.write("                    ");
      out.print((strStatusFlag.equals("1") ? "<font color=\"RED\">Suspended</font>" : "Active"));
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            ");
 }
      out.write("\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("    <tr>\r\n");
      out.write("    </tr>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("<!-- Pagination Starts -->\r\n");
      out.write("<tr>\r\n");
      out.write("    <td class=\"formbody\">\r\n");
      out.write("        <table border=\"0\" cellspacing='0' cellpadding='0' width=\"100%\" class=\"paginate_panel\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <a href=\"pgCaselist.jsp?CurPage=1\">\r\n");
      out.write("                    <img src=\"images/icons/page-first.gif\" title=\"Go to first page\" border=\"0\" alt=\"First\"></a>\r\n");
      out.write("                </td>\r\n");
      out.write("                <td>");
 if (nCurPage > 1) {
      out.write("\r\n");
      out.write("                    <A href=\"pgCaselist.jsp?CurPage=");
      out.print((nCurPage - 1));
      out.write("\">\r\n");
      out.write("                    <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\" alt=\"Prev\"></A>\r\n");
      out.write("                    ");
 } else {
      out.write("\r\n");
      out.write("                    <img src=\"images/icons/page-prev.gif\" title=\"Go to previous page\" border=\"0\" alt=\"Prev\">\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td nowrap class=\"page_number\">Page ");
      out.print(nCurPage);
      out.write(" of ");
      out.print(nPageCount);
      out.write(" </td>\r\n");
      out.write("                <td>");
 if (nCurPage < nPageCount) {
      out.write("\r\n");
      out.write("                    <A href=\"pgCaselist.jsp?CurPage=");
      out.print(nCurPage + 1);
      out.write("\">\r\n");
      out.write("                    <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\" alt=\"Next\"></A>\r\n");
      out.write("                    ");
 } else {
      out.write("\r\n");
      out.write("                    <img src=\"images/icons/page-next.gif\" title=\"Go to next page\" border=\"0\" alt=\"Next\">\r\n");
      out.write("                    ");
 }
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("                <td>\r\n");
      out.write("                    <a href=\"pgCaselist.jsp?CurPage=");
      out.print(nPageCount);
      out.write("\">\r\n");
      out.write("                    <img src=\"images/icons/page-last.gif\" title=\"Go to last page\" border=\"0\" alt=\"Last\"></a>\r\n");
      out.write("                </td>\r\n");
      out.write("                <td width=\"100%\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("    </td>\r\n");
      out.write("</tr>\r\n");
      out.write("<!-- Pagination Ends -->\r\n");
}
      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("    <td style=\"height: 10px;\"></td>\r\n");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<div style=\"height: 25px;\"></div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
