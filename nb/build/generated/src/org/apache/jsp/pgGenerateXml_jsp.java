package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.f3.edx.ich_icsr_v2_1.e2bm.tag.IcsrMessage;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import bl.sapher.general.GenConf;
import bl.sapher.general.Logger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

public final class pgGenerateXml_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private String strXmlFile;
        
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=windows-1252");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\r\n");
      out.write("        <script type=\"text/javascript\" src=\"libjs/gui.js\"></script>\r\n");
      out.write("        <title></title>\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">@import \"css/master.css\";</style>\r\n");
      out.write("\r\n");
      out.write("        <script type=\"text/javascript\">\r\n");
      out.write("\r\n");
      out.write("            function sendXml(xml_file) {\r\n");
      out.write("                var sel= document.getElementById('cmbMedium').value;\r\n");
      out.write("\r\n");
      out.write("                if(sel == 'e-Mail') {\r\n");
      out.write("                    document.location.replace('pgSendXmlThruEMail.jsp?XmlFile='+xml_file);\r\n");
      out.write("                } else if(sel == 'HTTP') {\r\n");
      out.write("                    document.location.replace('pgSendXmlThruHttp.jsp?XmlFile='+xml_file);\r\n");
      out.write("                }else if(sel == 'FTP') {\r\n");
      out.write("                    document.location.replace('pgSendXmlThruFtp.jsp?XmlFile='+xml_file);\r\n");
      out.write("                }\r\n");
      out.write("                return;\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        ");

        Logger.writeLog((String) session.getAttribute("LogFileName"), "Inside pgGenerateXml.jsp");
        String strRepNum = "";

        if (request.getParameter("RepNum") != null) {
            strRepNum = request.getParameter("RepNum");
        }
        IcsrMessage icsrMsg = new IcsrMessage((String) session.getAttribute("Company"), strRepNum);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US);
        String str = application.getRealPath("/XML");

        String strXmlFile =
                sdf.format(Calendar.getInstance().getTime()) +
                "-" + strRepNum +
                ".sgm";

        Writer output = null;
        File file = null;
        try {
            String strXmlData = icsrMsg.getTag().toString();
            if (strXmlData.length() == 0) {
                throw new Exception("IcsrMessage::getTag() did'nt provoded data.");
            }
            file = new File(str + "/" + strXmlFile);
            output = new BufferedWriter(new FileWriter(file));
            output.write(strXmlData);
            output.close();

            out.write("<script type=\"text/javascript\">");
            //out.write("     document.location.replace('XML/" + strXmlFile + "');");
            out.write("</script>");

        } catch (InvalidDataException ide) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgGenerateXml.jsp; Failed to create XML. This is due to absence of mandatory tags or presence too lengthy values.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ide.getMessage());

            String s = "Failed to create XML. This is due to &lt;b&gt;" + ide.getMessage() + "&lt;/b&gt;";
            System.out.println(s);
            pageContext.forward("pgError.jsp?ErrDetails=" + s);
        } catch (InvalidTagException ite) {
            Logger.writeLog((String) session.getAttribute("LogFileName"), "pgGenerateXml.jsp; Failed to create XML. This is due to absence of mandatory tags or presence too lengthy values.");
            Logger.writeLog((String) session.getAttribute("LogFileName"), ite.getMessage());

            String s = "Failed to create XML. This is due to &lt;b&gt;" + ite.getMessage() + "&lt;/b&gt;";
            System.out.println(s);
            pageContext.forward("pgError.jsp?ErrDetails=" + s);
        }

        //pageContext.forward("XML/" + strXmlFile);

      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div style=\"height: 15px;\"></div>\r\n");
      out.write("        <table border='0' cellspacing='0' cellpadding='0' width='90%' align=\"center\" id=\"formwindow\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td id=\"formtitle\">XML Submission Options</td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("                    <table border=\"0\">\r\n");
      out.write("                        <tbody>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\">Send XML via :</td>\r\n");
      out.write("                                <td><select name=\"cmbMedium\" id=\"cmbMedium\">\r\n");
      out.write("                                        <option>e-Mail</option>\r\n");
      out.write("                                        <option>HTTP</option>\r\n");
      out.write("                                        <option>FTP</option>\r\n");
      out.write("                                </select></td>\r\n");
      out.write("                                <td>\r\n");
      out.write("                                    <input type=\"button\" value=\"Send\" class=\"button\" name=\"cmdSend\" onclick=\"sendXml('");
      out.print(strXmlFile);
      out.write("');\" />\r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            <tr>\r\n");
      out.write("                                <td class=\"fLabel\"><a href=\"#\" onclick=\"document.location.replace('pgDisplyXML.jsp?XmlFile='+'");
      out.print(strXmlFile);
      out.write("');\">Show generated XML file.</a></td>\r\n");
      out.write("                                <td></td>\r\n");
      out.write("                                <td></td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                        </tbody>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Form Body Ends -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 1px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Starts -->\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td class=\"formbody\">\r\n");
      out.write("\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("            <!-- Grid View Ends -->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td style=\"height: 10px;\"></td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        <div style=\"height: 25px;\"></div>\r\n");
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("\r\n");
      out.write("    </script>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
