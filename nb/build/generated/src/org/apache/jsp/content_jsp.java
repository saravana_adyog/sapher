package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.GenConf;

public final class content_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

    private String strMessage;
    private String strHdr;
    
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : NAV1.5\r\n");
      out.write("Author  : Ratheesh\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("        <title>Sapher</title>\r\n");
      out.write("        <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("        <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("        <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("        <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("        <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("        <style type=\"text/css\" media=\"all\">\r\n");
      out.write("            @import \"css/master.css\";\r\n");
      out.write("            body{\r\n");
      out.write("                text-align: center;\r\n");
      out.write("                background: #588A89 url(images/bg/BG-effect.jpg);\r\n");
      out.write("                background-repeat:  no-repeat;\r\n");
      out.write("                background-position: center;\r\n");
      out.write("                margin: 0; \tpadding: 0;\r\n");
      out.write("            }\r\n");
      out.write("        </style>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <body>\r\n");
      out.write("        ");

        //GenConf gConf = new GenConf();
        //String strVer = gConf.getSysVersion();

        strMessage = "";
        strHdr = "";

        if (request.getParameter("Status") != null) {
            if (request.getParameter("Status").equals("0")) {
                strHdr = "Error:";
                strMessage = "Sorry, Specified Record not found!";
            } else if (request.getParameter("Status").equals("1")) {
                strHdr = "Error:";
                strMessage = "Sorry, Permission Denied!";
            } else if (request.getParameter("Status").equals("2")) {
                strHdr = "Status:";
                strMessage = "Update Success!";
                if (request.getParameter("RepNum") != null) {
                    strMessage = "Update Success, Report Number - " + request.getParameter("RepNum");
                }
            } else if (request.getParameter("Status").equals("3")) {
                strHdr = "Status:";
                strMessage = "Update Failed!";
            }
        }

        if (!strMessage.equals("")) {
        
      out.write("\r\n");
      out.write("        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"50%\">\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td height=\"200\" colspan=\"2\">\r\n");
      out.write("                    <table border='0' cellspacing='0' cellpadding='0' width='400' align=\"center\" id=\"formwindow\">\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td id=\"formtitle\">");
      out.print(strHdr);
      out.write("</td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <!-- Form Body Starts -->\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td class=\"formbody\">\r\n");
      out.write("                                <div class=\"error\">");
      out.print(strMessage);
      out.write("</div>\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        <!-- Form Body Ends -->\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <td style=\"height: 10px;\"></td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </table>\r\n");
      out.write("                </td>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </table>\r\n");
      out.write("        ");
}
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
