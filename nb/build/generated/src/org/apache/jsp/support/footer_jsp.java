package org.apache.jsp.support;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.GenConf;

public final class footer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : NAV1.2\r\n");
      out.write("Author  : Ratheesh\r\n");
      out.write("-->\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\t<meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("\t<title>Sapher</title>\r\n");
      out.write("\t<meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\t\r\n");
      out.write("\t<meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("\t<meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\t\r\n");
      out.write("\t<meta name=\"description\" content=\"Description\" />\r\n");
      out.write("\t<meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\t\r\n");
      out.write("\t<meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\t\r\n");
      out.write("\t<style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("</head>\r\n");

    String strClient = "";
    String strCRight = "";
    GenConf gConf = new GenConf();
    
    strClient = gConf.getSysOrgName();
    strCRight = gConf.getSysCRight();

      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t\t<td id=\"footer\">\r\n");
      out.write("\t\t\t<div style=\"float:left; padding-left: 20px;\">The product is Licensed to ");
      out.print((String)session.getAttribute("Company"));
      out.write("</div>\r\n");
      out.write("\t\t\t<div style=\"float:right; padding-right: 20px; \">");
      out.print(strCRight);
      out.write("</div>\r\n");
      out.write("\t\t</td>\r\n");
      out.write("\t</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
