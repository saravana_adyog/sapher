package org.apache.jsp.support;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import bl.sapher.general.GenConf;
import bl.sapher.general.SessionCounter;

public final class header_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\r\n");
      out.write("Form Id : NAV1.1\r\n");
      out.write("Author  : Ratheesh\r\n");
      out.write("-->\r\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("    <title>Sapher</title>\r\n");
      out.write("    <meta http-equiv=\"Content-Language\" content=\"en-us\" />\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"imagetoolbar\" content=\"no\" />\r\n");
      out.write("    <meta name=\"MSSmartTagsPreventParsing\" content=\"true\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"description\" content=\"Description\" />\r\n");
      out.write("    <meta name=\"keywords\" content=\"Keywords\" />\r\n");
      out.write("\r\n");
      out.write("    <meta name=\"author\" content=\"Focal3\" />\r\n");
      out.write("\r\n");
      out.write("    <style type=\"text/css\" media=\"all\">@import \"../css/master.css\";</style>\r\n");
      out.write("</head>\r\n");

    String strVer = "";
    String strUser = "";
    GenConf gConf = new GenConf();

    strVer = gConf.getSysVersion();
    if (session.getAttribute("CurrentUser") != null)
        strUser = (String)session.getAttribute("CurrentUser");

      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >\r\n");
      out.write("\t<tr>\r\n");
      out.write("            <td id=\"header\" align=\"left\"><img src=\"../images/header/logo-inner.jpg\"><div id=\"version\">");
      out.print(strVer);
      out.write("</div></td>\r\n");
      out.write("\t</tr>\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t\t<td id=\"navbar\">\r\n");
      out.write("\t\t\t<table border='0' cellspacing='0' cellpadding='0' width='100%'>\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<td noWrap style=\"padding-left: 20px; color: #000; font-size: 11px;\">\r\n");
      out.write("\t\t\t\t\t\tLogged in As : <B>");
      out.print(strUser);
      out.write("</B>\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t\t<td noWrap align=\"right\" valign=\"top\">\r\n");
      out.write("\t\t\t\t\t<table border='0' cellspacing='0' cellpadding='0'>\r\n");
      out.write("\t\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t\t<td width='20' align='center'><img src=\"../images/header/seperator.gif\"></td>\r\n");
      out.write("\t\t\t\t\t\t\t<td width='64' align='left'><a href=\"../pgNavigate.jsp?Target=content.jsp\" target=\"contentFrame\"><img src=\"../images/header/home.gif\" border=\"0\"></a></td>\r\n");
      out.write("\t\t\t\t\t\t\t<td width='61' align='left'><a href=\"#\"><img src=\"../images/header/help.gif\" border=\"0\"></a></td>\r\n");
      out.write("\t\t\t\t\t\t\t<td width='99' align='left'><a href=\"../pgLogout.jsp\" target=\"_top\"><img src=\"../images/header/logout.gif\" border=\"0\"></a></td>\r\n");
      out.write("\t\t\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t\t</table>\r\n");
      out.write("\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("\t\t\t</table>\r\n");
      out.write("\t\t</td>\r\n");
      out.write("\t</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
