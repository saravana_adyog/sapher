/*
 * CDl24
 * Body_System.java
 * Created on November 27, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Body_System implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -6110932025268439575L;
    private String Body_Sys_Num;
    private String strSysOrganClass;
    private int Is_Valid;
    private static String Log_File_Name;

    public Body_System() {
        this.Body_Sys_Num = "";
        this.strSysOrganClass = "";
        this.Is_Valid = -1;
    }

    public Body_System(String logFilename) {
        this.Body_Sys_Num = "";
        this.strSysOrganClass = "";
        this.Is_Valid = -1;
        Body_System.Log_File_Name = logFilename;
    }

    public Body_System(String Body_Sys_Num, String Body_System, int Is_Valid) {
        this.Body_Sys_Num = Body_Sys_Num;
        this.strSysOrganClass = Body_System;
        this.Is_Valid = Is_Valid;
    }

    public Body_System(String cpnyName, String Body_Sys_Num)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_BodySys(?,?,?,?,?)}");
            cStmt.setString("p_Code", Body_Sys_Num);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_BSys", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsBodySys = (ResultSet) cStmt.getObject("cur_BSys");
            rsBodySys.next();
            this.Body_Sys_Num = Body_Sys_Num;
            this.strSysOrganClass = rsBodySys.getString("Body_System");
            this.Is_Valid = rsBodySys.getInt("Is_Valid");
            rsBodySys.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Body System not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Body_System> listBodySystem(int SearchType, String cpnyName, String Body_Sys_Num, String Body_System, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Body_System.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Body_System> lstBodySys = new ArrayList<Body_System>();
        Body_System bdySys = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_BodySys(?,?,?,?,?)}");
            cStmt.setString("p_Code", Body_Sys_Num);
            cStmt.setString("p_Desc", Body_System);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_BSys", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsBdySys = (ResultSet) cStmt.getObject("cur_BSys");
            while (rsBdySys.next()) {
                bdySys = new Body_System(rsBdySys.getString("Body_Sys_Num"),
                        rsBdySys.getString("Body_System"),
                        rsBdySys.getInt("Is_Valid"));
                lstBodySys.add(bdySys);
            }
            rsBdySys.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Body System not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstBodySys;
    }

    public void saveBodySystem(String cpnyName, String Save_Flag, String Username,
            String Body_Sys_Num, String Body_System, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Body_System.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Body_System(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Body_Sys_Num",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Body_Sys_Num : Body_Sys_Num));
            cStmt.setString("p_Body_System", Body_System);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Body_Sys_Num = (Save_Flag.equalsIgnoreCase("U") ? this.Body_Sys_Num : Body_Sys_Num);
            this.strSysOrganClass = (!Body_System.equals("") ? Body_System : this.strSysOrganClass);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_BODY_SYS_NUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_BODY_SYSTEM") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteBodySystem(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Body_System.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Body_System(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Body_Sys_Num", this.Body_Sys_Num);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Body_Sys_Num = "";
            this.strSysOrganClass = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AE_BODYSYSTEM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Body_System.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_BodySys(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_BSys", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_BSys");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repBodySystem.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("System Organ class not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Body_System.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Body_System.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getBody_Sys_Num() {
        return Body_Sys_Num;
    }

    public String getBody_System() {
        return strSysOrganClass;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Body_Sys_Num = null;
        this.strSysOrganClass = null;
    }
}
