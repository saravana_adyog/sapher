/*
 * CDL7
 * Country.java
 * Created on November 24, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Country implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 8127625608171666802L;
    private String Country_Code;
    private String Country_Nm;
    private int Is_Valid;
    private static String Log_File_Name;

    public Country() {
        this.Country_Code = "";
        this.Country_Nm = "";
        this.Is_Valid = -1;
    }

    public Country(String logFilename) {
        this.Country_Code = "";
        this.Country_Nm = "";
        this.Is_Valid = -1;
        Country.Log_File_Name = logFilename;
    }

    public Country(String Country_Code, String Country_Nm, int Is_Valid) {
        this.Country_Code = Country_Code;
        this.Country_Nm = Country_Nm;
        this.Is_Valid = Is_Valid;
    }

    public Country(String cpnyName, String Country_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Country(?,?,?,?,?)}");
            cStmt.setString("p_Code", Country_Code);
            cStmt.setString("p_Name", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Ctry", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCountry = (ResultSet) cStmt.getObject("cur_Ctry");
            rsCountry.next();
            this.Country_Code = Country_Code;
            this.Country_Nm = rsCountry.getString("Country_Nm");
            this.Is_Valid = rsCountry.getInt("Is_Valid");
            rsCountry.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Country not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Country> listCountry(int SearchType, String cpnyName, String Country_Code, String Country_Nm, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Country.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Country> lstCountry = new ArrayList<Country>();
        Country cntry = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Country(?,?,?,?,?)}");
            cStmt.setString("p_Code", Country_Code);
            cStmt.setString("p_Name", Country_Nm);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Ctry", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCountry = (ResultSet) cStmt.getObject("cur_Ctry");
            while (rsCountry.next()) {
                cntry = new Country(rsCountry.getString("Country_Code"),
                        rsCountry.getString("Country_Nm"),
                        rsCountry.getInt("Is_Valid"));
                lstCountry.add(cntry);
            }

            rsCountry.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Country not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstCountry;
    }

    public void saveCountry(String cpnyName, String Save_Flag, String Username,
            String Country_Code, String Country_Nm, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Country.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Country(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Country_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Country_Code : Country_Code));
            cStmt.setString("p_Country_Nm", Country_Nm);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Country_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Country_Code : Country_Code);
            this.Country_Nm = (!Country_Nm.equals("") ? Country_Nm : this.Country_Nm);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_COUNTRY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_COUNTRY") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteCountry(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Country.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Country(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Country_Code", this.Country_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Country_Code = "";
            this.Country_Nm = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_LHA_COUNTRY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_SUB_CONF_COUNTRY") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_CASEDETAILS_COUNTRY") != -1) {
                throw new ConstraintViolationException("3");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("4");
            } else {
                Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Country.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Country(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Name", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Ctry", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCountry = (ResultSet) cStmt.getObject("cur_Ctry");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repCountry.jrxml", rsCountry, param);

            rsCountry.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Country not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Country.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Country.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getCountry_Code() {
        return Country_Code;
    }

    public String getCountry_Nm() {
        return Country_Nm;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Country_Code = null;
        this.Country_Nm = null;
    }
}
