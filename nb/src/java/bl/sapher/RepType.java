/*
 * CDL18
 * RepType.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class RepType implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -5780344395977335259L;
    private String Type_Code;
    private String Type_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public RepType() {
        this.Type_Code = "";
        this.Type_Desc = "";
        this.Is_Valid = -1;
    }

    public RepType(String logFilename) {
        this.Type_Code = "";
        this.Type_Desc = "";
        this.Is_Valid = -1;
        RepType.Log_File_Name = logFilename;
    }

    public RepType(String Type_Code, String Type_Desc, int Is_Valid) {
        this.Type_Code = Type_Code;
        this.Type_Desc = Type_Desc;
        this.Is_Valid = Is_Valid;
    }

    public RepType(String cpnyName, String Type_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepType(?,?,?,?,?)}");
            cStmt.setString("p_Code", Type_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Rep", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsRepType = (ResultSet) cStmt.getObject("cur_Rep");
            rsRepType.next();
            this.Type_Code = Type_Code;
            this.Type_Desc = rsRepType.getString("Type_Desc");
            this.Is_Valid = rsRepType.getInt("Is_Valid");
            rsRepType.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Report Type Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<RepType> listRepType(int SearchType, String cpnyName, String Type_Code, String Type_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "RepType.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<RepType> lstRepType = new ArrayList<RepType>();
        RepType reptype = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepType(?,?,?,?,?)}");
            cStmt.setString("p_Code", Type_Code);
            cStmt.setString("p_Desc", Type_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Rep", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsRepType = (ResultSet) cStmt.getObject("cur_Rep");
            while (rsRepType.next()) {
                reptype = new RepType(rsRepType.getString("Type_Code"),
                        rsRepType.getString("Type_Desc"),
                        rsRepType.getInt("Is_Valid"));
                lstRepType.add(reptype);
            }
            rsRepType.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Report Type Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstRepType;
    }

    public void saveRepType(String cpnyName, String Save_Flag, String Username,
            String Type_Code, String Type_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "RepType.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Rep_Type(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Type_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Type_Code : Type_Code));
            cStmt.setString("p_Type_Desc", Type_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Type_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Type_Code : Type_Code);
            this.Type_Desc = (!Type_Desc.equals("") ? Type_Desc : this.Type_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_REP_TYPE") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_REP_TYPE") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteRepType(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "RepType.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Rep_Type(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Type_Code", this.Type_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Type_Code = "";
            this.Type_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_REPTYPE") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "RepType.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepType(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Rep", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Rep");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repRepType.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Report type not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RepType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RepType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getType_Code() {
        return Type_Code;
    }

    public String getType_Desc() {
        return Type_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Type_Code = null;
        this.Type_Desc = null;
    }
}
