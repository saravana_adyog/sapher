/*
 * CDL9
 * DoseRegimen.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class DoseRegimen implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -2368882921007359517L;
    private String Dregimen_Code;
    private String Dregimen_Desc;
    private int nNoOfDosage;
    private int nDosageInterval;
    private String strDosageIntervalDesc;
    private int Is_Valid;
    private static String Log_File_Name;

    public DoseRegimen() {
        this.Dregimen_Code = "";
        this.Dregimen_Desc = "";
        this.nNoOfDosage = -1;
        this.nDosageInterval = -1;
        this.strDosageIntervalDesc = "";
        this.Is_Valid = -1;
    }

    public DoseRegimen(String logFilename) {
        this.Dregimen_Code = "";
        this.Dregimen_Desc = "";
        this.nNoOfDosage = -1;
        this.nDosageInterval = -1;
        this.strDosageIntervalDesc = "";
        this.Is_Valid = -1;
        DoseRegimen.Log_File_Name = logFilename;
    }

    public DoseRegimen(String Dregimen_Code, String Dregimen_Desc, int NoOfDosage,
            int DosageInterval, String IntervalDesc, int Is_Valid) {
        this.Dregimen_Code = Dregimen_Code;
        this.Dregimen_Desc = Dregimen_Desc;
        this.nNoOfDosage = NoOfDosage;
        this.nDosageInterval = DosageInterval;
        this.strDosageIntervalDesc = IntervalDesc;
        this.Is_Valid = Is_Valid;
    }

    public DoseRegimen(String cpnyName, String Dregimen_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DRgmn(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Dregimen_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.setInt("p_No_Of_Dosage", -1);
            cStmt.setInt("p_Dosage_Interval", -1);
            cStmt.registerOutParameter("cur_DRgmn", oracle.jdbc.OracleTypes.CURSOR);

            cStmt.execute();

            ResultSet rsDoseRegimen = (ResultSet) cStmt.getObject("cur_DRgmn");
            rsDoseRegimen.next();

            this.Dregimen_Code = Dregimen_Code;
            this.Dregimen_Desc = rsDoseRegimen.getString("d_Dregimen_Desc");
            this.nNoOfDosage = rsDoseRegimen.getInt("d_No_Of_Dosage");
            this.nDosageInterval = rsDoseRegimen.getInt("d_Dosage_Interval");
            this.strDosageIntervalDesc = rsDoseRegimen.getString("i_Interval");
            this.Is_Valid = rsDoseRegimen.getInt("d_Is_Valid");

            rsDoseRegimen.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose Regimen not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<DoseRegimen> listDoseRegimen(int SearchType, String cpnyName, String Dregimen_Code, String Dregimen_Desc,
            int NoOfDosage, int DosageInterval, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "DoseRegimen.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        String qry = "";
        int ctr = 0;
        ArrayList<DoseRegimen> lstDoseRegimen = new ArrayList<DoseRegimen>();
        DoseRegimen doseregimen = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DRgmn(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Dregimen_Code);
            cStmt.setString("p_Desc", Dregimen_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.setInt("p_No_Of_Dosage", NoOfDosage);
            cStmt.setInt("p_Dosage_Interval", DosageInterval);
            cStmt.registerOutParameter("cur_DRgmn", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDoseRegimen = (ResultSet) cStmt.getObject("cur_DRgmn");
            while (rsDoseRegimen.next()) {
                doseregimen = new DoseRegimen(rsDoseRegimen.getString("d_Dregimen_Code"),
                        rsDoseRegimen.getString("d_Dregimen_Desc"),
                        rsDoseRegimen.getInt("d_No_Of_Dosage"),
                        rsDoseRegimen.getInt("d_Dosage_Interval"),
                        rsDoseRegimen.getString("i_Interval"),
                        rsDoseRegimen.getInt("d_Is_Valid"));
                lstDoseRegimen.add(doseregimen);
            }
            rsDoseRegimen.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose Regimen not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstDoseRegimen;
    }

    public void saveDoseRegimen(String cpnyName, String Save_Flag, String Username,
            String Dregimen_Code, String Dregimen_Desc,
            int NoOfDosage, int DosageInterval, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "DoseRegimen.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Dose_Regimen(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Dregimen_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Dregimen_Code : Dregimen_Code));
            cStmt.setString("p_Dregimen_Desc", Dregimen_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.setInt("p_No_Of_Dosage", NoOfDosage);
            cStmt.setInt("p_Dosage_Interval", DosageInterval);

            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Dregimen_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Dregimen_Code : Dregimen_Code);
            this.Dregimen_Desc = (!Dregimen_Desc.equals("") ? Dregimen_Desc : this.Dregimen_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_DOSE_REGIMEN") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_DOSE_REGIMEN") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteDoseRegimen(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "DoseRegimen.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Dose_Regimen(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Dregimen_Code", this.Dregimen_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Dregimen_Code = "";
            this.Dregimen_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_DREGIMEN") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc,
            int dosg, int dosgInt, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "DoseRegimen.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DRgmn(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.setInt("p_No_Of_Dosage", dosg);
            cStmt.setInt("p_Dosage_Interval", dosgInt);
            cStmt.registerOutParameter("cur_DRgmn", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_DRgmn");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repDRegimen.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose regimen not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseRegimen.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

//    public static ArrayList<String> getDosageIntervalList(String cpnyName)
//            throws DBConnectionException, GenericException, SQLException {
//        DBCon dbCon = null;
//        Statement stmnt = null;
//        ResultSet rs = null;
//        ArrayList<String> al = new ArrayList<String>();
//
//        dbCon = new DBCon(cpnyName);
//        stmnt = dbCon.getDbCon().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
//        rs = stmnt.executeQuery("SELECT * FROM INTERVAL WHERE Is_Valid = 1");
//
//        boolean b = rs.first();
//
//        while (b) {
//            al.add( rs.getString("INTERVAL") );
//            b = rs.next();
//        }
//        stmnt = null;
//        rs = null;
//
//        return al;
//    }
    public String getDregimen_Code() {
        return Dregimen_Code;
    }

    public String getDregimen_Desc() {
        return Dregimen_Desc;
    }

    public String getNoOfDosage() {
        return "" + nNoOfDosage;
    }

    public String getDosageInterval() {
        return "" + nDosageInterval;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Dregimen_Code = null;
        this.Dregimen_Desc = null;
    }

    /**
     * @return the strDosageIntervalDesc
     */
    public String getDosageIntervalDesc() {
        return strDosageIntervalDesc;
    }
}
