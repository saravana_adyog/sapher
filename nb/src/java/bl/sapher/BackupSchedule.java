/*
 * BackupSchedule.java
 * Created on November 26, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import java.sql.Date;

public class BackupSchedule {

    private String jobName;
    private String schemaName;
    private Date startDate;
    private Date endDate;
    private String repeat_interval;
    private String lastRun;
    private String nextRun;
    private String state;
    private static String Log_File_Name;

    public BackupSchedule() {
        this.jobName = "";
        this.schemaName = "";
        this.startDate = null;
        this.endDate = null;
        this.repeat_interval = "";
        this.lastRun = "";
        this.nextRun = "";
        this.state = "";
    }

    public BackupSchedule(String logFilename) {
        this.jobName = "";
        this.schemaName = "";
        this.startDate = null;
        this.endDate = null;
        this.repeat_interval = "";
        this.lastRun = "";
        this.nextRun = "";
        this.state = "";
        BackupSchedule.Log_File_Name = logFilename;
    }

    public BackupSchedule(String jobName, String schemaName, Date startDate, Date endDate, String repeat_interval,
            String lastRun, String nextRun, String state) {
        this.jobName = jobName;
        this.schemaName = schemaName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.repeat_interval = repeat_interval;
        this.lastRun = lastRun;
        this.nextRun = nextRun;
        this.state = state;
    }

    public BackupSchedule(String username, String password, String jobName)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        this.jobName = jobName;
        DBCon dbCon = new DBCon(username, password);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapadmin_pkg.Get_Scheduled_Backup(?,?)}");
            cStmt.setString("p_jobName", jobName);
            cStmt.registerOutParameter("Cur_Backup_List", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsBkpSch = (ResultSet) cStmt.getObject("Cur_Backup_List");
            while (rsBkpSch.next()) {
                this.schemaName = rsBkpSch.getString("value");
                this.startDate = rsBkpSch.getDate("start_date");
                this.endDate = rsBkpSch.getDate("end_date");
                this.repeat_interval = rsBkpSch.getString("repeat_interval");
                this.lastRun = rsBkpSch.getString("last_start_date");
                this.nextRun = rsBkpSch.getString("next_run_date");
                this.state = rsBkpSch.getString("state");
            }
            rsBkpSch.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Caught SQLException-" + ex);
            ex.printStackTrace();
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Backup Schedule not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Caught Exception-" + ex);
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<BackupSchedule> listBkpSchedule(String username, String password)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "BackupSchedule.java; Inside listing function");
        DBCon dbCon = new DBCon(username, password);
        ArrayList<BackupSchedule> lstBkpSch = new ArrayList<BackupSchedule>();
        BackupSchedule bkpSch = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapadmin_pkg.Get_Scheduled_Backup(?,?)}");
            cStmt.setString("p_jobName", "");
            cStmt.registerOutParameter("Cur_Backup_List", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsBkpSch = (ResultSet) cStmt.getObject("Cur_Backup_List");
            while (rsBkpSch.next()) {
                bkpSch = new BackupSchedule(rsBkpSch.getString("job_name"), rsBkpSch.getString("value"),
                        rsBkpSch.getDate("start_date"), rsBkpSch.getDate("end_date"),
                        rsBkpSch.getString("repeat_interval"), rsBkpSch.getString("last_start_date"),
                        rsBkpSch.getString("next_run_date"), rsBkpSch.getString("state"));
                lstBkpSch.add(bkpSch);
            }
            rsBkpSch.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Caught SQLException-" + ex);
            ex.printStackTrace();
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Backup Schedule not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Caught Exception-" + ex);
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstBkpSch;
    }

    public static void saveBkpSchedule(String username, String password,
            String Save_Flag, String jobName, String schemaName,
            Date startDate, Date endDate,
            String repeat_interval)
            throws DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "BackupSchedule.java; Inside save function");

        DBCon dbCon = new DBCon(username, password);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapadmin_pkg.Save_Scheduled_Backup(?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_job_name", jobName);
            cStmt.setString("p_user_name", schemaName);
            cStmt.setDate("p_start_date", startDate);
            cStmt.setDate("p_end_date", endDate);
            cStmt.setString("p_repeat_interval", repeat_interval);

            System.out.println("AV> SFlag:" + Save_Flag + ", " + "Job name:" + jobName + ", " + "Username:" + schemaName + ", " + "Start Dt:" + startDate + ", " + "End Dt:" + endDate + ", " + "Repeat Interval:" + repeat_interval);

            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Caught Exception-" + ex);
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static void deleteBkpSchedule(String username, String password, String jobName)
            throws GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "BackupSchedule.java; Inside delete function");
        DBCon dbCon = new DBCon(username, password);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapadmin_pkg.Delete_Scheduled_Backup(?)}");
            cStmt.setString("p_job_name", jobName);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Caught Exception-" + ex);
            Logger.writeLog(Log_File_Name, "BackupSchedule.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * @return the schemaName
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @return the repeat_interval
     */
    public String getRepeat_interval() {
        return repeat_interval;
    }

    /**
     * @return the lastRun
     */
    public String getLastRun() {
        return lastRun;
    }

    /**
     * @return the nextRun
     */
    public String getNextRun() {
        return nextRun;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }
}
