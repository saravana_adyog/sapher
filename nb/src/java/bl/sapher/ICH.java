/*
 * ICH.java
 * Created on February 28, 2008
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ICH {

    private String Body_Sys_Num;
    private String Body_System;
    private int Rep_Num;
    private String Country_Nm;
    private String Source_Desc;
    private String Sex_Code;
    private int Age;
    private String Unit_Code;
    private String Treat_Start;
    private String Treat_Stop;
    private int Treat_Dur;
    private String Outcome;
    private String Date_Recv;
    private String Classification_Code;
    private String Labelling;
    private String Short_Comment;
    private String Llt_Code;
    private String Llt_Desc;
    private String Diag_Code;
    private String Diag_Desc;
    /** Log file name */
    private static String Log_File_Name;

    public ICH(String logFilename) {
        this.Body_Sys_Num = "";
        this.Body_System = "";
        this.Rep_Num = -1;
        this.Country_Nm = "";
        this.Source_Desc = "";
        this.Sex_Code = "";
        this.Age = -1;
        this.Unit_Code = "";
        this.Treat_Dur = -1;
        this.Outcome = "";
        this.Llt_Code = "";
        this.Llt_Desc = "";
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Classification_Code = "";
        this.Labelling = "";
        this.Short_Comment = "";
        this.Treat_Start = "";
        this.Treat_Stop = "";
        this.Date_Recv = "";
        ICH.Log_File_Name = logFilename;
    }

    public ICH(String Body_Sys_Num, String Body_System, int Rep_Num,
            String Country_Nm, String Source_Desc, String Sex_Code, int Age,
            String Unit_Code, String Treat_Start, String Treat_Stop, int Treat_Dur,
            String Outcome, String Llt_Code, String Llt_Desc, String Diag_Code, String Diag_Desc,
            String Date_Recv, String Classification_Code, String Labelling, String Short_Comment) {
        this.Body_Sys_Num = Body_Sys_Num;
        this.Body_System = Body_System;
        this.Rep_Num = Rep_Num;
        this.Country_Nm = Country_Nm;
        this.Source_Desc = Source_Desc;
        this.Sex_Code = Sex_Code;
        this.Age = Age;
        this.Unit_Code = Unit_Code;
        this.Treat_Dur = Treat_Dur;
        this.Outcome = Outcome;
        this.Llt_Code = Llt_Code;
        this.Llt_Desc = Llt_Desc;
        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Classification_Code = Classification_Code;
        this.Labelling = Labelling;
        this.Short_Comment = Short_Comment;
        this.Treat_Start = Treat_Start;
        this.Treat_Stop = Treat_Stop;
        this.Date_Recv = Date_Recv;
    }

    public static ArrayList listICH(String cpnyName, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "ICH.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<ICH> lstICH = new ArrayList<ICH>();
        ICH ich = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.ICH_Proc(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curICH", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsICH = (ResultSet) cStmt.getObject("p_curICH");
            while (rsICH.next()) {
                ich = new ICH(rsICH.getString("Body_Sys_Num"), rsICH.getString("Body_System"),
                        rsICH.getInt("Rep_Num"), rsICH.getString("Country_Nm"),
                        rsICH.getString("Source_Desc"), rsICH.getString("Sex_Code"),
                        rsICH.getInt("AGE"), rsICH.getString("Unit_Code"),
                        rsICH.getString("Treat_Start"), rsICH.getString("Treat_Stop"),
                        rsICH.getInt("Duration"), rsICH.getString("Outcome"),
                        rsICH.getString("Llt_Code"), rsICH.getString("Llt_Desc"),
                        rsICH.getString("Diag_Code"), rsICH.getString("Diag_Desc"),
                        rsICH.getString("Date_Recv"), rsICH.getString("Classification_Code"),
                        rsICH.getString("Labelling"), rsICH.getString("Short_Comment"));
                lstICH.add(ich);
            }
            rsICH.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ICH.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ICH.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("ICH records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ICH.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ICH.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstICH;
    }

    public static ResultSet listICH(DBCon dbCon, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "ICH.java; Inside listing function");

        ResultSet rsICH = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.ICH_Proc(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curICH", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            rsICH = (ResultSet) cStmt.getObject("p_curICH");
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ICH.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ICH.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("ICH records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ICH.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ICH.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsICH;
    }

    public String getBody_Sys_Num() {
        return Body_Sys_Num;
    }

    public String getBody_System() {
        return Body_System;
    }

    public String getRep_Num() {
        return String.valueOf(Rep_Num);
    }

    public String getCountry_Nm() {
        return Country_Nm;
    }

    public String getSource_Desc() {
        return Source_Desc;
    }

    public String getSex_Code() {
        return Sex_Code;
    }

    public int getAge() {
        return Age;
    }

    public String getUnit_Code() {
        return Unit_Code;
    }

    public String getTreat_Start() {
        return Treat_Start;
    }

    public String getTreat_Stop() {
        return Treat_Stop;
    }

    public int getTreat_Dur() {
        return Treat_Dur;
    }

    public String getOutcome() {
        return Outcome;
    }

    public String getDate_Recv() {
        return Date_Recv;
    }

    public String getClassification_Code() {
        return Classification_Code;
    }

    public String getLabelling() {
        return Labelling;
    }

    public String getShort_Comment() {
        return Short_Comment;
    }

    public String getLlt_Code() {
        return Llt_Code;
    }

    public String getLlt_Desc() {
        return Llt_Desc;
    }

    public String getDiag_Code() {
        return Diag_Code;
    }

    public String getDiag_Desc() {
        return Diag_Desc;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Body_Sys_Num = null;
        this.Body_System = null;
        this.Rep_Num = 0;
        this.Country_Nm = null;
        this.Source_Desc = null;
        this.Sex_Code = null;
        this.Age = 0;
        this.Unit_Code = null;
        this.Treat_Start = null;
        this.Treat_Stop = null;
        this.Treat_Dur = 0;
        this.Outcome = null;
        this.Date_Recv = null;
        this.Classification_Code = null;
        this.Labelling = null;
        this.Short_Comment = null;
        this.Llt_Code = null;
        this.Llt_Desc = null;
        this.Diag_Code = null;
        this.Diag_Desc = null;
    }
}
