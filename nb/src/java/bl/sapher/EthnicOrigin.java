/*
 * CDL11
 * EthnicOrigin.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class EthnicOrigin implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 5619076276800826428L;
    private String Ethnic_Code;
    private String Ethnic_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public EthnicOrigin() {
        this.Ethnic_Code = "";
        this.Ethnic_Desc = "";
        this.Is_Valid = -1;
    }

    public EthnicOrigin(String logFilename) {
        this.Ethnic_Code = "";
        this.Ethnic_Desc = "";
        this.Is_Valid = -1;
        EthnicOrigin.Log_File_Name = logFilename;
    }

    public EthnicOrigin(String Ethnic_Code, String Ethnic_Desc, int Is_Valid) {
        this.Ethnic_Code = Ethnic_Code;
        this.Ethnic_Desc = Ethnic_Desc;
        this.Is_Valid = Is_Valid;
    }

    public EthnicOrigin(String cpnyName, String Ethnic_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_EOrigin(?,?,?,?,?)}");
            cStmt.setString("p_Code", Ethnic_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_EOrgn", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsEthnicOrigin = (ResultSet) cStmt.getObject("cur_EOrgn");
            rsEthnicOrigin.next();
            this.Ethnic_Code = Ethnic_Code;
            this.Ethnic_Desc = rsEthnicOrigin.getString("Ethnic_Desc");
            this.Is_Valid = rsEthnicOrigin.getInt("Is_Valid");
            rsEthnicOrigin.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Ethnic Origin not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<EthnicOrigin> listEthnicOrigin(int SearchType, String cpnyName, String Ethnic_Code, String Ethnic_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<EthnicOrigin> lstEthnicOrigin = new ArrayList<EthnicOrigin>();
        EthnicOrigin ethnicorig = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_EOrigin(?,?,?,?,?)}");
            cStmt.setString("p_Code", Ethnic_Code);
            cStmt.setString("p_Desc", Ethnic_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_EOrgn", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsEthnicOrigin = (ResultSet) cStmt.getObject("cur_EOrgn");
            while (rsEthnicOrigin.next()) {
                ethnicorig = new EthnicOrigin(rsEthnicOrigin.getString("Ethnic_Code"),
                        rsEthnicOrigin.getString("Ethnic_Desc"),
                        rsEthnicOrigin.getInt("Is_Valid"));
                lstEthnicOrigin.add(ethnicorig);
            }
            rsEthnicOrigin.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Ethnic Origin not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstEthnicOrigin;
    }

    public void saveEthnicOrigin(String cpnyName, String Save_Flag, String Username,
            String Ethnic_Code, String Ethnic_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Ethnic_Origin(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Ethnic_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Ethnic_Code : Ethnic_Code));
            cStmt.setString("p_Ethnic_Desc", Ethnic_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Ethnic_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Ethnic_Code : Ethnic_Code);
            this.Ethnic_Desc = (!Ethnic_Desc.equals("") ? Ethnic_Desc : this.Ethnic_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_ETHNIC_ORIGIN") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_ETHNIC_ORIGIN") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteEthnicOrigin(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Ethnic_Origin(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Ethnic_Code", this.Ethnic_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Ethnic_Code = "";
            this.Ethnic_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_ETHNIC") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_EOrigin(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_EOrgn", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_EOrgn");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repEthnicOrigin.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Ethnic Origin not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "EthnicOrigin.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getEthnic_Code() {
        return Ethnic_Code;
    }

    public String getEthnic_Desc() {
        return Ethnic_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Ethnic_Code = null;
        this.Ethnic_Desc = null;
    }
}
