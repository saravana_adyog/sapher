/*
 * CDL2
 * AEOutcome.java
 * Created on November 28, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class AEOutcome implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 7129916599399651041L;
    private String Outcome_Code;
    private String Outcome;
    private int Is_Valid;
    private static String Log_File_Name;

    public AEOutcome() {
        this.Outcome_Code = "";
        this.Outcome = "";
        this.Is_Valid = -1;
    }

    public AEOutcome(String logFilename) {
        this.Outcome_Code = "";
        this.Outcome = "";
        this.Is_Valid = -1;
        AEOutcome.Log_File_Name = logFilename;
    }

    public AEOutcome(String Outcome_Code, String Outcome, int Is_Valid) {
        this.Outcome_Code = Outcome_Code;
        this.Outcome = Outcome;
        this.Is_Valid = Is_Valid;
    }

    public AEOutcome(String cpnyName, String Outcome_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            System.out.println(Outcome_Code);
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Outcome(?,?,?,?,?)}");
            cStmt.setString("p_Code", Outcome_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Outcome", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsOutcome = (ResultSet) cStmt.getObject("cur_Outcome");
            rsOutcome.next();
            this.Outcome_Code = Outcome_Code;
            this.Outcome = rsOutcome.getString("Outcome");
            this.Is_Valid = rsOutcome.getInt("Is_Valid");
            rsOutcome.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("AE Outcome not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<AEOutcome> listOutcome(int SearchType, String cpnyName, String Outcome_Code, String Outcome, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "AEOutcome.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<AEOutcome> lstOutcome = new ArrayList<AEOutcome>();
        AEOutcome aeOutcome = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Outcome(?,?,?,?,?)}");
            cStmt.setString("p_Code", Outcome_Code);
            cStmt.setString("p_Desc", Outcome);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Outcome", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsOutcome = (ResultSet) cStmt.getObject("cur_Outcome");
            while (rsOutcome.next()) {
                aeOutcome = new AEOutcome(rsOutcome.getString("Outcome_Code"), rsOutcome.getString("Outcome"),
                        rsOutcome.getInt("Is_Valid"));
                lstOutcome.add(aeOutcome);
            }
            rsOutcome.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("AE Outcome not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstOutcome;
    }

    public void saveOutcome(String cpnyName, String Save_Flag, String Username, String Outcome_Code, String Outcome, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AEOutcome.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Ae_Outcome(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Outcome_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Outcome_Code : Outcome_Code));
            cStmt.setString("p_Outcome", Outcome);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Outcome_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Outcome_Code : Outcome_Code);
            this.Outcome = (Outcome != "" ? Outcome : this.Outcome);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_AE_OUTCOME") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_AE_OUTCOME") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteOutcome(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AEOutcome.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Ae_Outcome(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Outcome_Code", this.Outcome_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Outcome_Code = "";
            this.Outcome = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AE_OUTCOME") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_CASEDETAILS_OUTCOME") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "AEOutcome.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Outcome(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Outcome", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Outcome");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repOutcome.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("AEOutcome not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AEOutcome.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getOutcome_Code() {
        return Outcome_Code;
    }

    public String getOutcome() {
        return Outcome;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Outcome = null;
        this.Outcome_Code = null;
    }
}
