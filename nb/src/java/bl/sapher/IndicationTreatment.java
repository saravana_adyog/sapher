/*
 * CDL14
 * IndicationTreatment.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class IndicationTreatment implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 8261486868544886962L;
    private String Indication_Num;
    private String Indication;
    private int Is_Valid;
    private static String Log_File_Name;

    public IndicationTreatment() {
        this.Indication_Num = "";
        this.Indication = "";
        this.Is_Valid = -1;
    }

    public IndicationTreatment(String logFilename) {
        this.Indication_Num = "";
        this.Indication = "";
        this.Is_Valid = -1;
        IndicationTreatment.Log_File_Name = logFilename;
    }

    public IndicationTreatment(String Indication_Num, String Indication, int Is_Valid) {
        this.Indication_Num = Indication_Num;
        this.Indication = Indication;
        this.Is_Valid = Is_Valid;
    }

    public IndicationTreatment(String cpnyName, String Indication_Num)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Indication(?,?,?,?,?)}");
            cStmt.setString("p_Code", Indication_Num);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Ind", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsIndicationTreatment = (ResultSet) cStmt.getObject("cur_Ind");
            rsIndicationTreatment.next();
            this.Indication_Num = Indication_Num;
            this.Indication = rsIndicationTreatment.getString("Indication");
            this.Is_Valid = rsIndicationTreatment.getInt("Is_Valid");
            rsIndicationTreatment.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Indication Treatment not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<IndicationTreatment> listIndicationTreatment(int SearchType, String cpnyName, String Indication_Num, String Indication, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<IndicationTreatment> lstIndicationTreatment = new ArrayList<IndicationTreatment>();
        IndicationTreatment inttreat = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Indication(?,?,?,?,?)}");
            cStmt.setString("p_Code", Indication_Num);
            cStmt.setString("p_Desc", Indication);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Ind", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsIndicationTreatment = (ResultSet) cStmt.getObject("cur_Ind");
            while (rsIndicationTreatment.next()) {
                inttreat = new IndicationTreatment(rsIndicationTreatment.getString("Indication_Num"),
                        rsIndicationTreatment.getString("Indication"),
                        rsIndicationTreatment.getInt("Is_Valid"));
                lstIndicationTreatment.add(inttreat);
            }
            rsIndicationTreatment.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Indication Treatment not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstIndicationTreatment;
    }

    public void saveIndicationTreatment(String cpnyName, String Save_Flag, String Username,
            String Indication_Num, String Indication, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Indication_Treatment(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Indication_Num",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Indication_Num : Indication_Num));
            cStmt.setString("p_Indication", Indication);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Indication_Num = (Save_Flag.equalsIgnoreCase("U") ? this.Indication_Num : Indication_Num);
            this.Indication = (!Indication.equals("") ? Indication : this.Indication);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_INDICATION_TREATMENT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_INDICATION_TREATMENT") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteIndicationTreatment(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Indication_Treatment(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Indication_Num", this.Indication_Num);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Indication_Num = "";
            this.Indication = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_INDICATION") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_CONC_MEDIC_INDICATION") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Indication(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Ind", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Ind");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repIndTreat.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Indication treatment not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "IndicationTreatment.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getIndication_Num() {
        return Indication_Num;
    }

    public String getIndication() {
        return Indication;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Indication = null;
        this.Indication_Num = null;
    }
}
