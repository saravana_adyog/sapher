/*
 * CDL4
 * Causality.java
 * Created on November 28, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Causality implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 2387252307428405277L;
    private String Assessment_Code;
    private String Assessment;
    private String Assessment_Flag;
    private int Is_Valid;
    private static String Log_File_Name;

    public Causality() {
        this.Assessment_Code = "";
        this.Assessment = "";
        this.Assessment_Flag = "";
        this.Is_Valid = -1;
    }

    public Causality(String logFilename) {
        this.Assessment_Code = "";
        this.Assessment = "";
        this.Assessment_Flag = "";
        this.Is_Valid = -1;
        Causality.Log_File_Name = logFilename;
    }

    public Causality(String Assessment_Code, String Assessment, String Assessment_Flag, int Is_Valid) {
        this.Assessment_Code = Assessment_Code;
        this.Assessment = Assessment;
        this.Assessment_Flag = Assessment_Flag;
        this.Is_Valid = Is_Valid;
    }

    public Causality(String cpnyName, String Assessment_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Assessment(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Assessment_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setString("p_Flag", "");
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Asmt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCausality = (ResultSet) cStmt.getObject("cur_Asmt");
            rsCausality.next();
            this.Assessment_Code = Assessment_Code;
            this.Assessment = rsCausality.getString("Assessment");
            this.Assessment_Flag = rsCausality.getString("Assessment_Flag");
            this.Is_Valid = rsCausality.getInt("Is_Valid");
            rsCausality.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Assessment not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Causality> listCausality(int SearchType, String cpnyName, String Assessment_Code, String Assessment, String Assessment_Flag, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Causality.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Causality> lstCausality = new ArrayList<Causality>();
        Causality causality = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Assessment(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Assessment_Code);
            cStmt.setString("p_Desc", Assessment);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.setString("p_Flag", Assessment_Flag);
            cStmt.registerOutParameter("cur_Asmt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCausality = (ResultSet) cStmt.getObject("cur_Asmt");
            while (rsCausality.next()) {
                causality = new Causality(rsCausality.getString("Assessment_Code"), rsCausality.getString("Assessment"),
                        rsCausality.getString("Assessment_Flag"), rsCausality.getInt("Is_Valid"));
                lstCausality.add(causality);
            }
            rsCausality.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Assessment not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstCausality;
    }

    public void saveCausality(String cpnyName, String Save_Flag, String Username, String Assessment_Code, String Assessment, String Assessment_Flag, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Causality.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Assessment_Causality(?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Assessment_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Assessment_Code : Assessment_Code));
            cStmt.setString("p_Assessment", Assessment);
            cStmt.setString("p_Assessment_Flag", Assessment_Flag);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Assessment_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Assessment_Code : Assessment_Code);
            this.Assessment = (Assessment != "" ? Assessment : this.Assessment);
            this.Assessment_Flag = (Assessment_Flag != "" ? Assessment_Flag : this.Assessment_Flag);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_ASSESSMENT_CAUSALITY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_ASSESSMENT_CAUSALITY") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteCausality(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Causality.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Assessment_Causality(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Assessment_Code", this.Assessment_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Assessment_Code = "";
            this.Assessment = "";
            this.Assessment_Flag = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_CO_ASSESSMENT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_CASEDETAILS_PHY_ASSESSMENT") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, String flag, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Causality.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Assessment(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.setString("p_Flag", flag);
            cStmt.registerOutParameter("cur_Asmt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Asmt");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repAssessment.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Causality not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Causality.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Causality.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getAssessment_Code() {
        return Assessment_Code;
    }

    public String getAssessment() {
        return Assessment;
    }

    public String getAssessment_Flag() {
        return Assessment_Flag;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Assessment = null;
        this.Assessment_Code = null;
        this.Assessment_Flag = null;
    }
}
