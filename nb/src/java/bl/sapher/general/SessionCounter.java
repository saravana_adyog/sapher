/* SessionCounter.java */

package bl.sapher.general;

import java.util.HashMap;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;

/**
 * Class that handles the count of simultaneous sessions. This class is related with the licensng model.
 *
 * @author Anoop VArma
 */
public class SessionCounter implements HttpSessionListener {

    private static HashMap<String, String> hm = null;


    /**
     * Static region that initializes the <code><b>HashMap</b></code>
     */
    static {
        hm = new HashMap<String, String>();
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
    }

    public static void increaseCounter(String SessionId) {
        hm.put(SessionId, "1");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        hm.remove(se.getSession().getAttribute("SapherSessionId"));
    }

    /**
     * Function to get the count of active sessions.
     *
     * @return session count as <code><b>int</b></code>
     */
    public static int getActiveSessions() {
        return hm.size();
    }
}