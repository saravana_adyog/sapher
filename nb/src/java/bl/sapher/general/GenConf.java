/*
 * GenConf.java
 * Created on November 23, 2007
 * @author Arun P. Jose, Anoop Varma
 */
package bl.sapher.general;

public class GenConf {

    private String sysOrgName;
    private String sysVersion;
    private String sysCRight;
    private String adminUser;
    private String appUser;
    private int sysLoginMistakeCount;
    private int sysPwdChangeIntvl;
    private int sysMinPwdLength;
    // Added for debug enabling. Anoop Varma, 16/09/2008.
    private static String debugMode;
    private static String logPath;
    // Added for e-mail submission. Anoop Varma, 09/01/2009.
    private String strEmailHost;
    private String strEmailFrom;
    private String strEmailTo;
    private int nSmtpPort;
    private String strEmailDebugMode;

    /**
     * The function fetches the following data from the <code>genConf.properties</code> file :
     *   Version
     *   Copyright
     *   ExpCmdLine
     *   ExpPath
     *   AdminUser
     *   AppUser
     *   AppRootPath
     *   DebugMode
     *
     * These parameters are common to all clients/companies. So these are separated from the XML configuration file.
     * @throws bl.sapher.general.GenericException
     */
    public GenConf()
            throws GenericException {
        this.sysVersion = ClientConf.getGeneralDetails("Version");
        this.sysCRight = ClientConf.getGeneralDetails("Copyright");
        GenConf.debugMode = ClientConf.getGeneralDetails("DebugMode");
    }

    /**
     * The function fetches common data from the <code><b>genConf.properties</b></code> file and
     * customer specific data from the <code>clientConf.xml</code> file.
     * 
     * @throws bl.sapher.general.GenericException
     */
    public GenConf(String cpnyName)
            throws GenericException {
        this.sysVersion = ClientConf.getGeneralDetails("Version");
        this.sysCRight = ClientConf.getGeneralDetails("Copyright");
        GenConf.debugMode = ClientConf.getGeneralDetails("DebugMode");
        this.sysOrgName = cpnyName;

        ClientConf cc = ClientConf.getByClientName(cpnyName);

        this.appUser = cc.getUserName();
        this.sysLoginMistakeCount = cc.getLoginMistakeCount();
        this.sysPwdChangeIntvl = cc.getPwdChangeIntvl();
        this.sysMinPwdLength = cc.getMinPwdLength();

        // Added for e-mail submission. Anoop Varma, 09/01/2009.
        this.strEmailHost = cc.getEMailHost();
        this.strEmailFrom = cc.getEMailFrom();
        this.strEmailTo = cc.getEMailTo();
        this.nSmtpPort = cc.getSMTPPort();
    }

    /**
     * System login mistake count setter function
     * 
     * @return sysLoginMistakeCount as <code><b>String</b></code>
     */
    public int getSysLoginMistakeCount() {
        return sysLoginMistakeCount;
    }

    /**
     * 
     * @return sysPwdChangeIntvl as <code><b>String</b></code>
     */
    public int getSysPwdChangeIntvl() {
        return sysPwdChangeIntvl;
    }

    /**
     * 
     * @return sysOrgName as <code><b>String</b></code>
     */
    public String getSysOrgName() {
        return sysOrgName;
    }

    /**
     * 
     * @return sysMinPwdLength as <code><b>int</b></code>
     */
    public int getSysMinPwdLength() {
        return sysMinPwdLength;
    }

    /**
     * 
     * @return sysCRight as <code><b>String</b></code>
     */
    public String getSysCRight() {
        return sysCRight;
    }

    /**
     * 
     * @return sysVersion as <code><b>String</b></code>
     */
    public String getSysVersion() {
        return sysVersion;
    }

    /**
     * 
     * @return adminUser as <code><b>String</b></code>
     */
    public String getAdminUser() {
        return adminUser;
    }

    /**
     * 
     * @return appUser as <code><b>String</b></code>
     */
    public String getAppUser() {
        return appUser;
    }

    /**
     * 
     * @return  as <code><b>String</b></code>
     */
    public static boolean isDebugMode() {
        if (GenConf.debugMode != null) {
            if (GenConf.debugMode.equals("1")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return the debugMode
     */
    public static String getDebugMode() {
        return GenConf.debugMode;
    }

    /**
     * @return the strEmailHost
     */
    public String getEmailHost() {
        return strEmailHost;
    }

    /**
     * @return the strEmailFrom
     */
    public String getEmailFrom() {
        return strEmailFrom;
    }

    /**
     * @return the strEmailTo
     */
    public String getEmailTo() {
        return strEmailTo;
    }

    /**
     * @return the nSmtpPort
     */
    public int getSmtpPort() {
        return nSmtpPort;
    }

    /**
     * @return the strEmailDebugMode
     */
    public String getEmailDebugMode() {
        return strEmailDebugMode;
    }

    /**
     * @return the logPath
     */
    public static String getLogPath() {
        return logPath;
    }

    /**
     * @param aLogPath the logPath to set
     */
    public static void setLogPath(String aLogPath) {
        logPath = aLogPath;
    }
}
