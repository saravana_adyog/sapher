/*
 * RecordNotFoundException.java
 * Created on November 19, 2007
 * @author Arun P. Jose
 */
package bl.sapher.general;

public class RecordNotFoundException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = 993968608008194476L;

    /**
     * Constructor of <code><b>RecordNotFoundException</b></code>
     * @param msgText Exception message as <code><b>String</b></code>
     */
    public RecordNotFoundException(String msgText) {
        super(msgText);
    }
}
