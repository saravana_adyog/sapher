/*
 * DBCon.java
 * Created on November 22, 2007
 * @author Arun P. Jose
 */
package bl.sapher.general;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class DBCon {
    private Connection dbCon;


    /**
     * Constructor of <code><b>DBCon</b></code>
     * 
     * @param company Company/Client name as <code><b>String</b></code>
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     */
    public DBCon(String company) throws DBConnectionException, GenericException {
        ClientConf cc = ClientConf.getByClientName(company);
        
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            this.dbCon = DriverManager.getConnection("jdbc:oracle:thin:@" +
                    cc.getHostName() + ":" +
                    cc.getPort() + ":" +
                    cc.getSID(),
                    cc.getUserName(),
                    cc.getPassword());
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new DBConnectionException("Cannot connect to database!!");
        } catch (ClassNotFoundException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new DBConnectionException("JDBC Driver Error!!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * Used in CPanel.
     *
     * @param uname Username as <code><b>String</b></code>
     * @param pswd  Password as <code><b>String</b></code>
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     */
    public DBCon(String uname, String pswd)
            throws DBConnectionException, GenericException {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            this.dbCon = DriverManager.getConnection("jdbc:oracle:thin:@" +
                    ClientConf.getGeneralDetails("host") + ":" +
                    ClientConf.getGeneralDetails("port") + ":" +
                    ClientConf.getGeneralDetails("sid"),
                    uname, pswd);
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            ex.printStackTrace();
            throw new DBConnectionException("Cannot connect to database!!");
        } catch (ClassNotFoundException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new DBConnectionException("JDBC Driver Error!!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * Function that closes the database connection.
     *
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     */
    public void closeCon()
            throws DBConnectionException, GenericException {
        try {
            this.dbCon.close();
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new DBConnectionException("Cannot close database!!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * Function to get the <code><b>Connection</b></code> object.
     *
     * @return dbCon as <code><b>Connection</b></code>
     */
    public Connection getDbCon() {
        return dbCon;
    }
    
}
