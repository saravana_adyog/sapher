/*
 * SimultaneousOpException.java
 *
 * Created by anoop, on May 22, 2008, 11:32 AM
 *
 */
package bl.sapher.general;

/**
 *
 * @author Anoop Varma
 */
public class SimultaneousOpException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = 2332476422824553938L;

    /**
     * SimultaneousOpException class constructor.
     * @param StrErr Exception message as <code><b>String</b></code>
     */
    public SimultaneousOpException(String StrErr) {
        super(StrErr);
    }
}
