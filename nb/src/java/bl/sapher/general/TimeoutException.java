/*
 * GenericException.java
 * Created on April 10, 2008
 * @author Anoop Varma
 */
package bl.sapher.general;

/**
 * GenericException
 * 
 * @author Anoop Varma
 */
public class TimeoutException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = 1318497041950290213L;

    /**
     * TimeoutException class constructor.
     * @param msgText Exception message as <code><b>String</b></code>
     */
    public TimeoutException(String msgText) {
        super(msgText);
    }
}
