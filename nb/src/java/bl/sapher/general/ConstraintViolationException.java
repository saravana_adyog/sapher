/*
 * ConstraintViolationException.java
 * Created on November 19, 2007
 * @author Arun P. Jose
 */
package bl.sapher.general;

public class ConstraintViolationException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = 6320303408135439326L;

    /**
     * Constructor for ConstraintViolationException
     * @param msgText as <code><b>String</b></code>
     */
    public ConstraintViolationException(String msgText) {
        super(msgText);
    }
}
