
package bl.sapher.general;

/**
 * UserCountException Class.
 * This exception is thrown when the number of users logged in exceeds the maximum allowed license count.
 * 
 * @author Anoop Varma
 */
public class UserCountException extends Exception {
    
    /**
     * Constructor for UserCountException
     */
    public UserCountException() {
        super();
    }

    /**
     * Constructor for UserCountException
     *
     * @param msg Ecxeption message as <code><b>String</b></code>
     */
    public UserCountException(String msg) {
        super(msg);
    }
}
