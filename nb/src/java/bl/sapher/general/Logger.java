
package bl.sapher.general;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Class that creates log files. The logger will be used in all places,
 * if the Debug mode is enabled in the configuration XML file.
 * @author Anoop Varma
 */
public class Logger {

    /**
     * Function tp prepare log file
     *
     * @param fileName Log file name as <code><b>String</b></code>
     * @param text log message as <code><b>String</b></code>
     * @throws bl.sapher.general.GenericException
     */
    public static void writeLog(String fileName, String text) throws GenericException {
        if (GenConf.isDebugMode()) {
            FileOutputStream out;
            PrintStream p;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmssSSS", java.util.Locale.US);
            try {
                out = new FileOutputStream(GenConf.getLogPath() + "\\" + fileName + ".log", true);
                p = new PrintStream(out);
                p.println(sdf.format(Calendar.getInstance().getTime()) + "|" + text);
                p.close();
                out.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
