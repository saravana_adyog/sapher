/*
 * DBConnectionException.java
 * Created on November 19, 2007
 * @author Arun P. Jose
 */
package bl.sapher.general;

public class DBConnectionException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = -3024098925835664070L;

    /**
     * Constructor of <code><b>DBConnectionException</b></code>
     * @param msgText Exception message as <code><b>String</b></code>
     */
    public DBConnectionException(String msgText) {
        super(msgText);
    }
}

