/*
 * ClientConf.java
 * Created on January 22, 2007
 * @author Anoop Varma.
 */
package bl.sapher.general;

import java.util.ArrayList;

import java.io.File;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Anoop Varma.
 */
public class ClientConf {

    private static String xmlPath;
    /** Variable to hold Name of client. */
    private String strClientName;
    /** Variable to hold Oracle DB Host name. */
    private String strHostName;
    /** Variable to hold Oeacle DB SID. */
    private String strSID;
    /** Variable to hold Oracle DB Port. */
    private int nPort = 0;
    /** Variable to hold Oracle DB user name. */
    private String strUserName;
    /** Variable to hold Oracle DB password. */
    private String strPassword;
    /** Variable to hold Login mistake counter. */
    private int nLoginMistakeCount;
    /** Variable to hold Password change interval. */
    private int nPwdChangeIntvl;
    /** Variable to hold minimum length for the password. */
    private int nMinPwdLength;
    /** Variable to hold the e-mail server. */
    private String strEMailHost;
    /** Added for document upload(new method of mapping on App server, instead of DB server.) 15/09/2009 */
    private String strDocUploadPath;
    /** Path to store MedDRA update files 03/11/2009 */
    private String strMedDraUpdatePath;
    /**
     * Variable to hold the e-mail to address.
     * This address will be used to send generated XML files for electronic submission to government bodies.
     */
    private String strEMailFrom;
    /**
     * Variable to hold the e-mail from address.
     * This will be used as from address while ending generated XML files for electronic submission to government bodies.
     */
    private String strEMailTo;
    /** Variable to hold the e-mail server SMTP Port. */
    private int nSMTPPort;

    /**
     * Default constructor for <code>ClientConf</code> class
     */
    public ClientConf() {
        strClientName = "";
        strHostName = "";
        strSID = "";
        strUserName = "";
        strPassword = "";
        nPort = 0;
        nLoginMistakeCount = 0;
        nPwdChangeIntvl = 0;
        nMinPwdLength = 0;
        strEMailHost = "";
        strEMailFrom = "";
        strEMailTo = "";
        nSMTPPort = 0;
        strDocUploadPath = "";
        strMedDraUpdatePath = "";
    }

    /**
     * Parameterized constructor for <code>ClientConf</code> class.
     *
     * @param cn               The DB connection as <code><b>String</b></code> to get the client configuration settings.
     * @param host             The DB host name as <code><b>String</b></code> to get the client configuration settings.
     * @param sid              The DB System ID as <code><b>String</b></code> to get the client configuration settings.
     * @param un               The DB user name as <code><b>String</b></code> to get the client configuration settings.
     * @param pw               The DB password as <code><b>String</b></code> to get the client configuration settings.
     * @param port             The DB port as <code><b>int</b></code> to get the client configuration settings.
     * @param Lmc              The login mistake count as <code><b>int</b></code> to get the client configuration settings.
     * @param PwCI             The password change interval as <code><b>int</b></code> to get the client configuration settings.
     * @param mpl              The minimum password length as <code><b>int</b></code> to get the client configuration settings.
     * @param arp              The .. as <code><b>String</b></code> to get the client configuration settings.
     * @param eMH              The e-mail host name as <code><b>String</b></code> to get the client configuration settings.
     * @param eMF              The e-mail from address as <code><b>String</b></code> to get the client configuration settings.
     * @param eMT              The e-mail to address as <code><b>String</b></code> to get the client configuration settings.
     * @param eDM              The .. as <code><b>String</b></code> to get the client configuration settings.
     * @param smtpport         The SMTP Port as <code><b>int</b></code> to get the client configuration settings.
     * @param docUploadPath    The document upload path as <code><b>String</b></code> to get the client configuration settings.
     * @param MedDraUpdatePath The MedDRA update path as <code><b>String</b></code> to get the client configuration settings.
     */
    public ClientConf(String cn, String host, String sid, String un, String pw, int port,
            int Lmc, int PwCI, int mpl,
            String arp,
            String eMH, String eMF, String eMT, String eDM, int smtpport,
            String docUploadPath, String MedDraUpdatePath) {
        strClientName = cn;
        strHostName = host;
        strSID = sid;
        strUserName = un;
        strPassword = pw;
        nPort = port;
        nLoginMistakeCount = Lmc;
        nPwdChangeIntvl = PwCI;
        nMinPwdLength = mpl;
        strEMailHost = eMH;
        strEMailFrom = eMF;
        strEMailTo = eMT;
        nSMTPPort = smtpport;
        strDocUploadPath = docUploadPath;
        strMedDraUpdatePath = MedDraUpdatePath;
    }

    /**
     * Returns the client specific settings from the ClientConf.xml file
     * 
     * @param name The client name as <code>String</code> to get the client configuration settings.
     * @return The <code>ClientConf</code> object of the required client
     */
    public static ClientConf getByClientName(String name) {
        ClientConf cc = null;
        ArrayList al = getClients();
        for (int i = 0; i < al.size(); i++) {
            if (((ClientConf) al.get(i)).getClientName().equalsIgnoreCase(name)) {
                cc = (ClientConf) al.get(i);
                break;
            }
        }
        return cc;
    }

    /**
     * Returns the client specific settings from the ClientConf.xml file
     * 
     * @param name The schema name as <code>String</code> to get the client configuration settings.
     * @return The <code>ClientConf</code> object of the required client
     */
    public static ClientConf getBySchemaName(String name) {
        ClientConf cc = null;
        ArrayList al = getClients();
        for (int i = 0; i < al.size(); i++) {
            if (((ClientConf) al.get(i)).getUserName().equalsIgnoreCase(name)) {
                cc = (ClientConf) al.get(i);
                break;
            }
        }
        return cc;
    }

    /**
     *
     *
     * @param key tag name
     * @return
     */
    public static String getGeneralDetails(String key) {
        String host = null, sid = null, port = null, version = null,
                cRight = null, debugMode = null;
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

            Document doc = docBuilder.parse(new File(xmlPath + "\\clientConf.xml"));

            NodeList nlClient = doc.getElementsByTagName("general");

            for (int s = 0; s < nlClient.getLength(); s++) {

                Node firstClientNode = nlClient.item(s);
                if (firstClientNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element firstClientElement = (Element) firstClientNode;

                    NodeList firstNameList = firstClientElement.getElementsByTagName("HostName");
                    Element firstNameElement = (Element) firstNameList.item(0);
                    NodeList textFNList = firstNameElement.getChildNodes();
                    host = ((Node) textFNList.item(0)).getNodeValue().trim();

                    NodeList lastNameList = firstClientElement.getElementsByTagName("SID");
                    Element lastNameElement = (Element) lastNameList.item(0);
                    NodeList textLNList = lastNameElement.getChildNodes();
                    sid = ((Node) textLNList.item(0)).getNodeValue().trim();

                    NodeList nlPW = firstClientElement.getElementsByTagName("Port");
                    Element pwElement = (Element) nlPW.item(0);
                    NodeList textpwList = pwElement.getChildNodes();
                    port = ((Node) textpwList.item(0)).getNodeValue().trim();

                    NodeList nlVersion = firstClientElement.getElementsByTagName("Version");
                    Element versionElement = (Element) nlVersion.item(0);
                    NodeList textVersionList = versionElement.getChildNodes();
                    version = ((Node) textVersionList.item(0)).getNodeValue().trim();

                    NodeList nlCRight = firstClientElement.getElementsByTagName("Copyright");
                    Element cRightElement = (Element) nlCRight.item(0);
                    NodeList textcRightList = cRightElement.getChildNodes();
                    cRight = ((Node) textcRightList.item(0)).getNodeValue().trim();

                    NodeList nlDebugMode = firstClientElement.getElementsByTagName("DebugMode");
                    Element debugModeElement = (Element) nlDebugMode.item(0);
                    NodeList textdebugModeList = debugModeElement.getChildNodes();
                    debugMode = ((Node) textdebugModeList.item(0)).getNodeValue().trim();
                }
            }

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }

        if (key.equalsIgnoreCase("host")) {
            return host;
        } else if (key.equalsIgnoreCase("sid")) {
            return sid;
        } else if (key.equalsIgnoreCase("port")) {
            return port;
        } else if (key.equalsIgnoreCase("version")) {
            return version;
        } else if (key.equalsIgnoreCase("copyright")) {
            return cRight;
        } else if (key.equalsIgnoreCase("debugmode")) {
            return debugMode;
        } else {
            return "";
        }
    }

    /**
     * 
     * @return
     */
    public static ArrayList<ClientConf> getClients() {
        ArrayList<ClientConf> al = new ArrayList<ClientConf>();
        String cn = null, un = null, pw = null, hn = null, sid = null;
        String arp = null;
        String eMH = null, eMF = null, eMT = null, eDM = null;
        int nPort = 0, nLMC = 0, nPwCI = 0, nMPL = 0, nSmtpPort = 0;
        String FURPath = "", MedDraUpdates = "";
        ClientConf cc;

        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

            Document doc = docBuilder.parse(new File(xmlPath + "\\clientConf.xml"));

            /* normalize text representation
            System.out.println("Root element of the doc is " +
            doc.getDocumentElement().getNodeName());
             */

            NodeList nlClient = doc.getElementsByTagName("client");

            for (int s = 0; s < nlClient.getLength(); s++) {


                Node firstClientNode = nlClient.item(s);
                if (firstClientNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element firstClientElement = (Element) firstClientNode;

                    //Read <name>
                    NodeList firstNameList = firstClientElement.getElementsByTagName("name");
                    Element firstNameElement = (Element) firstNameList.item(0);
                    NodeList textFNList = firstNameElement.getChildNodes();
                    cn = ((Node) textFNList.item(0)).getNodeValue().trim();

                    //Read <uname>
                    NodeList lastNameList = firstClientElement.getElementsByTagName("uname");
                    Element lastNameElement = (Element) lastNameList.item(0);
                    NodeList textLNList = lastNameElement.getChildNodes();
                    un = ((Node) textLNList.item(0)).getNodeValue().trim();

                    //Read <passwd>
                    NodeList nlPW = firstClientElement.getElementsByTagName("passwd");
                    Element pwElement = (Element) nlPW.item(0);
                    NodeList textpwList = pwElement.getChildNodes();
                    pw = ((Node) textpwList.item(0)).getNodeValue().trim();

                    //Read <HostName>
                    NodeList nlHostname = firstClientElement.getElementsByTagName("HostName");
                    Element hnElement = (Element) nlHostname.item(0);
                    NodeList textHnList = hnElement.getChildNodes();
                    hn = ((Node) textHnList.item(0)).getNodeValue().trim();

                    //Read <SID>
                    NodeList nlSID = firstClientElement.getElementsByTagName("SID");
                    Element sidElement = (Element) nlSID.item(0);
                    NodeList textSidList = sidElement.getChildNodes();
                    sid = ((Node) textSidList.item(0)).getNodeValue().trim();

                    //Read <Port>
                    NodeList nlPort = firstClientElement.getElementsByTagName("Port");
                    Element portElement = (Element) nlPort.item(0);
                    NodeList textPortList = portElement.getChildNodes();
                    nPort = Integer.parseInt(((Node) textPortList.item(0)).getNodeValue().trim());

                    //Read <LoginMistakeCount>
                    NodeList nlLMC = firstClientElement.getElementsByTagName("LoginMistakeCount");
                    Element LmcElement = (Element) nlLMC.item(0);
                    NodeList textLmcList = LmcElement.getChildNodes();
                    nLMC = Integer.parseInt(((Node) textLmcList.item(0)).getNodeValue().trim());

                    //Read <PwdChangeIntvl>
                    NodeList nlPwCI = firstClientElement.getElementsByTagName("PwdChangeIntvl");
                    Element PwCIElement = (Element) nlPwCI.item(0);
                    NodeList textPwCIList = PwCIElement.getChildNodes();
                    nPwCI = Integer.parseInt(((Node) textPwCIList.item(0)).getNodeValue().trim());

                    //Read <MinPwdLength>
                    NodeList nlMPL = firstClientElement.getElementsByTagName("MinPwdLength");
                    Element MplElement = (Element) nlMPL.item(0);
                    NodeList textMplList = MplElement.getChildNodes();
                    nMPL = Integer.parseInt(((Node) textMplList.item(0)).getNodeValue().trim());

                    //Read <eMailHost>
                    NodeList nleMH = firstClientElement.getElementsByTagName("eMailHost");
                    Element eMHElement = (Element) nleMH.item(0);
                    NodeList texteMHList = eMHElement.getChildNodes();
                    eMH = ((Node) texteMHList.item(0)).getNodeValue().trim();

                    //Read <eMailFrom>
                    NodeList nleMF = firstClientElement.getElementsByTagName("eMailFrom");
                    Element eMFElement = (Element) nleMF.item(0);
                    NodeList texteMFList = eMFElement.getChildNodes();
                    eMF = ((Node) texteMFList.item(0)).getNodeValue().trim();

                    //Read <eMailTo>
                    NodeList nleMT = firstClientElement.getElementsByTagName("eMailTo");
                    Element eMTElement = (Element) nleMT.item(0);
                    NodeList texteMTList = eMTElement.getChildNodes();
                    eMT = ((Node) texteMTList.item(0)).getNodeValue().trim();

                    //Read <SMTPPort>
                    NodeList nlSmtpPort = firstClientElement.getElementsByTagName("SMTPPort");
                    Element smtpportElement = (Element) nlSmtpPort.item(0);
                    NodeList textSmtpPortList = smtpportElement.getChildNodes();
                    nSmtpPort = Integer.parseInt(((Node) textSmtpPortList.item(0)).getNodeValue().trim());

                    // Read <DocUploadPath>
                    NodeList nlfURPath = firstClientElement.getElementsByTagName("DocUploadPath");
                    Element eFURPlement = (Element) nlfURPath.item(0);
                    NodeList textFURPList = eFURPlement.getChildNodes();
                    FURPath = ((Node) textFURPList.item(0)).getNodeValue().trim();

                    // Read <MedDraUpdatePath>
                    NodeList nlMedDraUpdatePath = firstClientElement.getElementsByTagName("MedDraUpdatePath");
                    Element eMedDraUpdateElement = (Element) nlMedDraUpdatePath.item(0);
                    NodeList textMedDraUpdateList = eMedDraUpdateElement.getChildNodes();
                    MedDraUpdates = ((Node) textMedDraUpdateList.item(0)).getNodeValue().trim();
                    
                    cc = new ClientConf(cn, hn, sid, un, pw, nPort,
                            nLMC, nPwCI, nMPL,
                            arp,
                            eMH, eMF, eMT, eDM, nSmtpPort, FURPath, MedDraUpdates);
                    al.add(cc);
                }//end of if clause

            }//end of for loop with s var

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return al;
    }

    /**
     * Function to get Client Name
     * 
     * @return the strClientName
     */
    public String getClientName() {
        return strClientName;
    }

    /**
     * Function to get User Name
     * 
     * @return the strUserName
     */
    public String getUserName() {
        return strUserName;
    }

    /**
     * Function to get password
     * 
     * @return the strPassword
     */
    public String getPassword() {
        return strPassword;
    }

    /**
     * Function to get Host Name
     * 
     * @return the strHostName
     */
    public String getHostName() {
        return strHostName;
    }

    /**
     * Function to get DB Sys ID
     * 
     * @return the strSID
     */
    public String getSID() {
        return strSID;
    }

    /**
     * Function to get DB port number
     * 
     * @return the nPort
     */
    public int getPort() {
        return nPort;
    }

    /**
     * Function to get login mistake count
     * 
     * @return the nLoginMistakeCount
     */
    public int getLoginMistakeCount() {
        return nLoginMistakeCount;
    }

    /**
     * Function to get password changing interval
     * 
     * @return the nPwdChangeIntvl
     */
    public int getPwdChangeIntvl() {
        return nPwdChangeIntvl;
    }

    /**
     * Function to get minimum length of password
     * 
     * @return the nMinPwdLength
     */
    public int getMinPwdLength() {
        return nMinPwdLength;
    }

    /**
     * Function to get e-mail host name
     * 
     * @return the strEMailHost
     */
    public String getEMailHost() {
        return strEMailHost;
    }

    /**
     * Function to get e-mail from address
     * 
     * @return the strEMailFrom
     */
    public String getEMailFrom() {
        return strEMailFrom;
    }

    /**
     * Function to get e-mail to address
     * 
     * @return the strEMailTo
     */
    public String getEMailTo() {
        return strEMailTo;
    }

    /**
     * Function to get XML file path
     * 
     * @return the xmlPath
     */
    public static String getXmlPath() {
        return xmlPath;
    }

    /**
     * Function to set XML file path
     * 
     * @param aXmlPath the xmlPath to set as <code><b>String</b></code>
     */
    public static void setXmlPath(String aXmlPath) {
        xmlPath = aXmlPath;
    }

    /**
     * Function to get SMTP port number
     * 
     * @return the nSMTPPort
     */
    public int getSMTPPort() {
        return nSMTPPort;
    }

    /**
     * Function to get document upload path
     * 
     * @return the strDocUploadPath
     */
    public String getDocUploadPath() {
        return strDocUploadPath;
    }

    /**
     * Function to set document upload path
     * 
     * @param strDocUploadPath the strDocUploadPath to set
     */
    public void setDocUploadPath(String strDocUploadPath) {
        this.strDocUploadPath = strDocUploadPath;
    }

    /**
     * 
     * 
     * @return the strMedDraUpdatePath
     */
    public String getMedDraUpdatePath() {
        return strMedDraUpdatePath;
    }

    /**
     * Function to set MedDra update path
     *
     * @param strMedDraUpdatePath the strMedDraUpdatePath to set
     */
    public void setMedDraUpdatePath(String strMedDraUpdatePath) {
        this.strMedDraUpdatePath = strMedDraUpdatePath;
    }
}
