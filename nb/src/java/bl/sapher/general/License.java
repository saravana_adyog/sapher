/*
 * License.java
 * Created on August 13, 2008
 * @author Anoop Varma
 */
package bl.sapher.general;

import com.f3.sapher.keygen.SapherKey;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Class that handles the licenses of Sapher.
 * 
 * @author Anoop Varma
 */
public class License {

    /** Max num of users allowed, as per the license taken. */
    private static int nNumOfUsers = -1;
    private String strLicenseKey;

    public License() {
        this.strLicenseKey = "";
    }

    public License(String licensekey) {
        this.strLicenseKey = licensekey;
    }

    /**
     * Function to delete a specific licenmse.
     * 
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     * @throws bl.sapher.general.ConstraintViolationException
     */
    public void deleteLicense(String cpnyName)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_License(?)}");
            cStmt.setString(1, this.getLicenseKey());
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.strLicenseKey = "";
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("1");
            } else {
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * Function that lists licenses.
     * @param LicenseKey as <code><b>String</b></code>. If this parameter is empty string, the function returns all the licenses.
     * @return
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     * @throws bl.sapher.general.RecordNotFoundException
     */
    public static ArrayList<License> listLicense(String cpnyName, String LicenseKey) throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        String qry = "";
        int ctr = 0;
        ArrayList<License> lstLicense = new ArrayList<License>();
        License licenseKey = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_License(?,?)}");
            cStmt.setString("p_License_Key", LicenseKey);
            cStmt.registerOutParameter("cur_License", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsUser = (ResultSet) cStmt.getObject("cur_License");
            while (rsUser.next()) {
                licenseKey = new License(rsUser.getString("License_Key"));
                lstLicense.add(licenseKey);
            }
            rsUser.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new RecordNotFoundException("License not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
        return lstLicense;
    }

    /**
     * Function to save a license
     *
     * @param cpnyName Company/Client name as <code><b>String</b></code>
     * @param LicenseKey License key as <code><b>String</b></code>
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.ConstraintViolationException
     * @throws bl.sapher.general.GenericException
     * @throws bl.sapher.general.RecordNotFoundException
     */
    public void saveLicense(String cpnyName, String LicenseKey)
            throws DBConnectionException, ConstraintViolationException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        //TODO GenConf sysProp = new GenConf(cpnyName);

        try {
            //AV System.out.println("AV> Client Name in License: " + SapherKey.calcCompanyName(LicenseKey));

            if (!SapherKey.calcCompanyName(LicenseKey).equalsIgnoreCase(cpnyName)) {
                throw new Exception("Invalid Company Name");
            }

            CallableStatement cStmt = dbCon.getDbCon().prepareCall("{call sapher_pkg.Save_License(?)}");
            cStmt.setString("p_License_Key", LicenseKey);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.strLicenseKey = LicenseKey;
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            ex.printStackTrace();
            if (ex.getMessage().indexOf("PK_USER") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_USER_ROLE") != -1) {
                throw new ConstraintViolationException("2");
            }
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

//    public String generateKey(String Username, String ClientName) {
//        float uSum = 0;
//        float cSum = 0;
//        char[] cArr = Username.toCharArray();
//        for (int i = 0; i < cArr.length; i++) {
//            uSum += (int) cArr[i] + 12;
//        }
//        cArr = ClientName.toCharArray();
//        for (int i = 0; i < cArr.length; i++) {
//            cSum += (int) cArr[i] + 12;
//        }
//        String result = "SFR-U" + uSum + "-C" + cSum;
//        return result;
//    }
    /**
     * Function that returns the maximum number of users allowed as per the client's license.
     * @return Maximum number of users as <code><b>int</b></code>
     */
    public static int getNumOfUsers(String cpnyName)
            throws DBConnectionException, GenericException, SQLException {
        nNumOfUsers=1000;
        /*DBCon dbCon = null;
        Statement stmnt = null;
        ResultSet rs = null;

        dbCon = new DBCon(cpnyName);
        stmnt = dbCon.getDbCon().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        rs = stmnt.executeQuery("SELECT * FROM License");

        boolean b = rs.first();

        nNumOfUsers = 0;
        while (b) {
            //AV System.out.println("AV> license: " + rs.getString("LICENSE_KEY"));
            nNumOfUsers += Integer.parseInt(SapherKey.calcUserCount(rs.getString("LICENSE_KEY")));
           SapherKey.
            b = rs.next();
        }
        stmnt = null;
        rs = null;*/

        return nNumOfUsers;
    }

    /**
     * Getter function for licese.
     * @return License as <code><b>String</b></code>
     */
    public String getLicenseKey() {
        return strLicenseKey;
    }
}
