/*
 * UserBlockedException.java
 * Created on January 14, 2008
 * @author Arun P. Jose
 */
package bl.sapher.general;

public class UserBlockedException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = -1476726355614131712L;

    /**
     * Constructor for UserBlockedException
     */
    public UserBlockedException() {
    }
}
