/*
 * GenericException.java
 * Created on April 10, 2008
 * @author Anoop Varma
 */
package bl.sapher.general;

public class GenericException extends Exception {

    /** Serialization version UID */
    private static final long serialVersionUID = -5464916064789786656L;

    /**
     * Constructor for <code><b>GenericException</b></code>
     * @param msgText Exception message as <code><b>String</b></code>
     */
    public GenericException(String msgText) {
        super(msgText);
    }
}
