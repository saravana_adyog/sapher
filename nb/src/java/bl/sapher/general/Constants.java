
package bl.sapher.general;

/**
 * Class containing many constant values used in the entire project.
 * 
 * @author Anoop Varma
 */
public class Constants {

    /** Search Type Flag */
    public static final int SEARCH_LIBERAL = 0;
    /** Search Type Flag */
    public static final int SEARCH_STRICT = 1;

    public static final String SEARCH_ANY_VAL = "";

    /** Status Code */
    public static final int STATUS_SUSPENDED = 0;
    /** Status Code */
    public static final int STATUS_ACTIVE = 1;
    /** Status Code */
    public static final int STATUS_ALL = -1;
    /** Document download Flag */
    public static final int DOCS_PARTIAL_DOWNLOAD = 0;
    /** Document download Flag */
    public static final int DOCS_FULL_DOWNLOAD = 1;
    /** Login Status Flag */
    public static final String LOGIN_STATUS_INVALID_UN_PW = "1";
    /** Login Status Flag */
    public static final String LOGIN_STATUS_ACC_LOCKED = "2";
    /** Login Status Flag */
    public static final String LOGIN_STATUS_ALREADY_LOGGEDIN = "3";
    /** Login Status Flag */
    public static final String LOGIN_STATUS_UNKNOWN_ERR = "4";
    /** Login Status Flag */
    public static final String LOGIN_STATUS_DB_MAINTANANCE_IN_PROGRESS = "5";
    /** Login Status Flag */
    public static final String LOGIN_STATUS_SESSION_TIMEOUT = "6";
    /** Login Status Flag */
    public static final String LOGIN_STATUS_LICENSE_LIMIT = "7";
    /** Application stability Status Flag */
    public static final String APP_STABILITY_STATUS_ILLEGAL_OP = "1";
}
