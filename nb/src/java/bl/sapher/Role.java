/*
 * USM2
 * Role.java
 * Created on November 22, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

public class Role implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = 5293715975180694453L;
    private String Role_Name;
    private int Is_Locked;
    private static String Log_File_Name;

    public Role() {
        this.Role_Name = "";
        this.Is_Locked = -1;
    }

    public Role(String logFilename) {
        this.Role_Name = "";
        this.Is_Locked = -1;
        Role.Log_File_Name = logFilename;
    }

    public Role(String Role_Name, int Is_Locked) {
        this.Role_Name = Role_Name;
        this.Is_Locked = Is_Locked;
    }

    public Role(String cpnyName, String Role_Name)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Role(?,?,?)}");
            cStmt.setString("p_RoleName", Role_Name);
            cStmt.setInt("p_Is_Locked", -1);
            cStmt.registerOutParameter("cur_Role", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsRole = (ResultSet) cStmt.getObject("cur_Role");
            rsRole.next();
            this.Role_Name = Role_Name;
            this.Is_Locked = rsRole.getInt("Is_Locked");
            rsRole.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Role.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Role not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Role.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Role> listRole(int SearchType, String cpnyName, String Role_Name, int Is_Locked)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Role.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        String qry = "";
        int ctr = 0;
        ArrayList<Role> lstRole = new ArrayList<Role>();
        Role rl = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Role(?,?,?)}");
            cStmt.setString("p_RoleName", Role_Name);
            cStmt.setInt("p_Is_Locked", Is_Locked);
            cStmt.registerOutParameter("cur_Role", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsRole = (ResultSet) cStmt.getObject("cur_Role");
            while (rsRole.next()) {
                rl = new Role(rsRole.getString("Role_Name"), rsRole.getInt("Is_Locked"));
                lstRole.add(rl);
            }
            rsRole.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Role.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Role not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Role.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstRole;
    }

    public boolean getPermission(String cpnyName, Inventory inv, char pType)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        return Permission.getPermission(cpnyName, this.Role_Name, inv.getInv_Id(), pType);
    }

    public void saveRole(String cpnyName, String Save_Flag, String Role_Name, int Is_Locked)
            throws DBConnectionException, ConstraintViolationException, GenericException {
        Logger.writeLog(Log_File_Name, "Role.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Role_List(?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Role_Name", (Save_Flag.equalsIgnoreCase("U") ? this.Role_Name : Role_Name));
            cStmt.setInt("p_Is_Locked", Is_Locked);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Role_Name = (Save_Flag.equalsIgnoreCase("U") ? this.Role_Name : Role_Name);
            this.Is_Locked = (Is_Locked != -1 ? Is_Locked : this.Is_Locked);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_ROLE") != -1) {
                throw new ConstraintViolationException("1");
            } else {
                Logger.writeLog(Log_File_Name, "Role.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Role.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteRole(String cpnyName)
            throws DBConnectionException, ConstraintViolationException, GenericException {
        Logger.writeLog(Log_File_Name, "Role.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Role_List(?)}");
            cStmt.setString("p_Role_Name", this.Role_Name);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Role_Name = "";
            this.Is_Locked = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_USER_ROLE") != -1) {
                throw new ConstraintViolationException("1");
            } else {
                Logger.writeLog(Log_File_Name, "Role.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Role.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Role.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public String getRole_Name() {
        return Role_Name;
    }

    public int getIs_Locked() {
        return Is_Locked;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Role_Name = null;
    }
}
