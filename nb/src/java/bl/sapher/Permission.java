/*
 * USM4
 * Permission.java
 * Created on March 27, 2008
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Permission implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = -8936561637720820288L;
    private Inventory inv;
    private int pView;
    private int pAdd;
    private int pEdit;
    private int pDelete;
    private static String Log_File_Name;

    public Permission() {
        inv = null;
        pView = -1;
        pAdd = -1;
        pEdit = -1;
        pDelete = -1;
    }

    public Permission(String logFilename) {
        inv = null;
        pView = -1;
        pAdd = -1;
        pEdit = -1;
        pDelete = -1;
        Permission.Log_File_Name = logFilename;
    }

    public Permission(Inventory inv, int pView, int pAdd, int pEdit, int pDelete) {
        this.inv = inv;
        this.pView = pView;
        this.pAdd = pAdd;
        this.pEdit = pEdit;
        this.pDelete = pDelete;
    }

    public Permission(String cpnyName, String Inv_Id, String Role_Name)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        Permission prm = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Role_Permissions(?,?,?,?)}");
            cStmt.setString("p_RoleName", Role_Name);
            cStmt.setString("p_InventoryId", Inv_Id);
            cStmt.setString("p_InvType", "");
            cStmt.registerOutParameter("cur_Perm", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsPerm = (ResultSet) cStmt.getObject("cur_Perm");
            rsPerm.next();
            this.inv = new Inventory(rsPerm.getString("Inv_Id"),
                    rsPerm.getString("Inv_Desc"), rsPerm.getString("Inv_Type"), -1, -1);
            this.pView = rsPerm.getInt("pView");
            this.pAdd = rsPerm.getInt("pAdd");
            this.pEdit = rsPerm.getInt("pEdit");
            this.pDelete = rsPerm.getInt("pDelete");
            rsPerm.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Permissions not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList listPermissions(String cpnyName, String Inv_Id, String Role_Name, String Inv_Type)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Permission.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        String qry = "";
        int ctr = 0;
        ArrayList<Permission> lstPerm = new ArrayList<Permission>();
        Permission prm = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Role_Permissions(?,?,?,?)}");
            cStmt.setString("p_RoleName", Role_Name);
            cStmt.setString("p_InventoryId", Inv_Id);
            cStmt.setString("p_InvType", Inv_Type);
            cStmt.registerOutParameter("cur_Perm", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsPerm = (ResultSet) cStmt.getObject("cur_Perm");
            while (rsPerm.next()) {
                prm = new Permission(new Inventory(rsPerm.getString("Inv_Id"),
                        rsPerm.getString("Inv_Desc"), rsPerm.getString("Inv_Type"), -1, -1),
                        rsPerm.getInt("pView"), rsPerm.getInt("pAdd"),
                        rsPerm.getInt("pEdit"), rsPerm.getInt("pDelete"));
                lstPerm.add(prm);
            }
            rsPerm.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Permissions not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstPerm;
    }

    public static void setPermission(String cpnyName, String Inv_Id, String Role_Name, int pView, int pAdd, int pEdit, int pDelete)
            throws DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Update_Role_Permissions(?,?,?,?,?,?)}");
            cStmt.setString("p_Role_Name", Role_Name);
            cStmt.setString("p_Inv_Id", Inv_Id);
            cStmt.setInt("p_Pview", pView);
            cStmt.setInt("p_Padd", pAdd);
            cStmt.setInt("p_Pedit", pEdit);
            cStmt.setInt("p_Pdelete", pDelete);
            cStmt.execute();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException("Cannot update permission!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static boolean getPermission(String cpnyName, String Role_Name, String Inv_Id, char pType)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        boolean result = false;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Role_Permissions(?,?,?,?)}");
            cStmt.setString("p_RoleName", Role_Name);
            cStmt.setString("p_InventoryId", Inv_Id);
            cStmt.setString("p_InvType", "");
            cStmt.registerOutParameter("cur_Perm", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsPermissions = (ResultSet) cStmt.getObject("cur_Perm");
            rsPermissions.next();
            if (pType == 'V') {
                result = (rsPermissions.getInt("pView") == 1 ? true : false);
            } else if (pType == 'A') {
                result = (rsPermissions.getInt("pAdd") == 1 ? true : false);
            } else if (pType == 'E') {
                result = (rsPermissions.getInt("pEdit") == 1 ? true : false);
            } else if (pType == 'D') {
                result = (rsPermissions.getInt("pDelete") == 1 ? true : false);
            }
            rsPermissions.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Permission entry not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return result;
    }

    public Inventory getInv() {
        return inv;
    }

    public int getPView() {
        return pView;
    }

    public int getPAdd() {
        return pAdd;
    }

    public int getPEdit() {
        return pEdit;
    }

    public int getPDelete() {
        return pDelete;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.inv = null;
        this.pView = 0;
        this.pAdd = 0;
        this.pEdit = 0;
        this.pDelete = 0;
    }
}
