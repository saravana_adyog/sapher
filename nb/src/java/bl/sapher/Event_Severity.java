/*
 * CDL12
 * Event_Severity.java
 * Created on November 27, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Event_Severity implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -5372621827686976177L;
    private String Eseverity_Code;
    private String Eseverity_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public Event_Severity() {
        this.Eseverity_Code = "";
        this.Eseverity_Desc = "";
        this.Is_Valid = -1;
    }

    public Event_Severity(String logFilename) {
        this.Eseverity_Code = "";
        this.Eseverity_Desc = "";
        this.Is_Valid = -1;
        Event_Severity.Log_File_Name = logFilename;
    }

    public Event_Severity(String Eseverity_Code, String Eseverity_Desc, int Is_Valid) {
        this.Eseverity_Code = Eseverity_Code;
        this.Eseverity_Desc = Eseverity_Desc;
        this.Is_Valid = Is_Valid;
    }

    public Event_Severity(String cpnyName, String Eseverity_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_ESvty(?,?,?,?,?)}");
            cStmt.setString("p_Code", Eseverity_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_ESvty", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsESeverity = (ResultSet) cStmt.getObject("cur_ESvty");
            rsESeverity.next();
            this.Eseverity_Code = Eseverity_Code;
            this.Eseverity_Desc = rsESeverity.getString("ESeverity_Desc");
            this.Is_Valid = rsESeverity.getInt("Is_Valid");
            rsESeverity.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Event Severity not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Event_Severity> listESeverity(int SearchType, String cpnyName, String Eseverity_Code, String Eseverity_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Event_Severity.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Event_Severity> lstEsvty = new ArrayList<Event_Severity>();
        Event_Severity esvty = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_ESvty(?,?,?,?,?)}");
            cStmt.setString("p_Code", Eseverity_Code);
            cStmt.setString("p_Desc", Eseverity_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_ESvty", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsESvty = (ResultSet) cStmt.getObject("cur_ESvty");
            while (rsESvty.next()) {
                esvty = new Event_Severity(rsESvty.getString("Eseverity_Code"),
                        rsESvty.getString("Eseverity_Desc"),
                        rsESvty.getInt("Is_Valid"));
                lstEsvty.add(esvty);
            }
            rsESvty.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Event Severity not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstEsvty;
    }

    public void saveESeverity(String cpnyName, String Save_Flag, String Username,
            String Eseverity_Code, String Eseverity_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Event_Severity.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Event_Severity(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_ESeverity_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Eseverity_Code : Eseverity_Code));
            cStmt.setString("p_ESeverity_Desc", Eseverity_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Eseverity_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Eseverity_Code : Eseverity_Code);
            this.Eseverity_Desc = (!Eseverity_Desc.equals("") ? Eseverity_Desc : this.Eseverity_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_EVENT_SEVERITY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_ESEVERITY") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteESeverity(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Event_Severity.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Event_Severity(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_ESeverity_Code", this.Eseverity_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Eseverity_Code = "";
            this.Eseverity_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AE_SEVERITY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Event_Severity.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_ESvty(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_ESvty", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_ESvty");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repESeverity.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("AELLT not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Event_Severity.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getEseverity_Code() {
        return Eseverity_Code;
    }

    public String getEseverity_Desc() {
        return Eseverity_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Eseverity_Code = null;
        this.Eseverity_Desc = null;
    }
}
