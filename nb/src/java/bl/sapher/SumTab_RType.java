/*
 * SumTab_RType.java
 * Created on March 3, 2008
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SumTab_RType implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = 1935407183105671881L;
    private String Body_Sys_Num;
    private String Body_System;
    private String Diag_Code;
    private String Diag_Desc;
    private int Clinical_Trials;
    private int Spontaneous;
    private int Regulatory;
    private int Literature;
    private int Others;
    private int Total;
    private static String Log_File_Name;

    public SumTab_RType(String logFilename) {
        this.Body_Sys_Num = "";
        this.Body_System = "";
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Clinical_Trials = -1;
        this.Spontaneous = -1;
        this.Regulatory = -1;
        this.Literature = -1;
        this.Others = -1;
        this.Total = -1;
        SumTab_RType.Log_File_Name = logFilename;
    }

    public SumTab_RType(String Body_Sys_Num, String Body_System, String Diag_Code, String Diag_Desc,
            int Clinical_Trials, int Spontaneous, int Regulatory, int Literature, int Others, int Total) {
        this.Body_Sys_Num = Body_Sys_Num;
        this.Body_System = Body_System;
        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Clinical_Trials = Clinical_Trials;
        this.Spontaneous = Spontaneous;
        this.Regulatory = Regulatory;
        this.Literature = Literature;
        this.Others = Others;
        this.Total = Total;
    }

    public static ArrayList listSumTab_RType(String cpnyName, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SumTab_RType.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<SumTab_RType> lstSumTabRType = new ArrayList<SumTab_RType>();
        SumTab_RType str = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Sumtab_RType(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curSumtabR", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSTRType = (ResultSet) cStmt.getObject("p_curSumtabR");
            while (rsSTRType.next()) {
                str = new SumTab_RType(rsSTRType.getString("Body_Sys_Num"), rsSTRType.getString("Body_System"),
                        rsSTRType.getString("Diag_Code"), rsSTRType.getString("Diag_Desc"),
                        rsSTRType.getInt("Clinical_Trial"), rsSTRType.getInt("Spontaneous"),
                        rsSTRType.getInt("Regulatory"), rsSTRType.getInt("Literature"),
                        rsSTRType.getInt("Others"), rsSTRType.getInt("Total"));
                lstSumTabRType.add(str);
            }
            rsSTRType.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("SumTab_RType records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstSumTabRType;
    }

    public static ResultSet listSumTab_RType(DBCon dbCon, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SumTab_RType.java; Inside listing function");
        
        ResultSet rsSTRType = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Sumtab_RType(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curSumtabR", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            rsSTRType = (ResultSet) cStmt.getObject("p_curSumtabR");
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("SumTab_RType records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_RType.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsSTRType;
    }

    public String getBody_Sys_Num() {
        return Body_Sys_Num;
    }

    public String getBody_System() {
        return Body_System;
    }

    public String getDiag_Code() {
        return Diag_Code;
    }

    public String getDiag_Desc() {
        return Diag_Desc;
    }

    public int getClinical_Trials() {
        return Clinical_Trials;
    }

    public int getSpontaneous() {
        return Spontaneous;
    }

    public int getRegulatory() {
        return Regulatory;
    }

    public int getLiterature() {
        return Literature;
    }

    public int getOthers() {
        return Others;
    }

    public int getTotal() {
        return Total;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Body_Sys_Num = null;
        this.Body_System = null;
        this.Diag_Code = null;
        this.Diag_Desc = null;
        this.Clinical_Trials = 0;
        this.Spontaneous = 0;
        this.Regulatory = 0;
        this.Literature = 0;
        this.Others = 0;
        this.Total = 0;
    }
}
