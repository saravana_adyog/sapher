/*
 * CDL6
 * Manufacturer.java
 * Created on November 28, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Manufacturer implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 3911328282165384448L;
    private String M_Code;
    private String M_Name;
    private String M_Addr1;
    private String M_Addr2;
    private String M_Addr3;
    private String M_Addr4;
    private String M_Phone;
    private String M_Fax;
    private String M_Email;
    private int Is_Valid;
    private static String Log_File_Name;

    public Manufacturer() {
        this.M_Code = "";
        this.M_Name = "";
        this.M_Addr1 = "";
        this.M_Addr2 = "";
        this.M_Addr3 = "";
        this.M_Addr4 = "";
        this.M_Phone = "";
        this.M_Fax = "";
        this.M_Email = "";
        this.Is_Valid = -1;
    }

    public Manufacturer(String logFilename) {
        this.M_Code = "";
        this.M_Name = "";
        this.M_Addr1 = "";
        this.M_Addr2 = "";
        this.M_Addr3 = "";
        this.M_Addr4 = "";
        this.M_Phone = "";
        this.M_Fax = "";
        this.M_Email = "";
        this.Is_Valid = -1;
        Manufacturer.Log_File_Name = logFilename;
    }

    public Manufacturer(String M_Code, String M_Name, String M_Addr1,
            String M_Addr2, String M_Addr3, String M_Addr4,
            String M_Phone, String M_Fax, String M_Email,
            int Is_Valid) {
        this.M_Code = M_Code;
        this.M_Name = M_Name;
        this.M_Addr1 = M_Addr1;
        this.M_Addr2 = M_Addr2;
        this.M_Addr3 = M_Addr3;
        this.M_Addr4 = M_Addr4;
        this.M_Phone = M_Phone;
        this.M_Fax = M_Fax;
        this.M_Email = M_Email;
        this.Is_Valid = Is_Valid;
    }

    public Manufacturer(String cpnyName, String M_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Manufacturer(?,?,?,?,?)}");
            cStmt.setString("p_Code", M_Code);
            cStmt.setString("p_Name", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Mfr", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsManufacturer = (ResultSet) cStmt.getObject("cur_Mfr");
            rsManufacturer.next();
            this.M_Code = M_Code;
            this.M_Name = rsManufacturer.getString("M_Name");
            this.M_Addr1 = rsManufacturer.getString("M_Addr1");
            this.M_Addr2 = rsManufacturer.getString("M_Addr2");
            this.M_Addr3 = rsManufacturer.getString("M_Addr3");
            this.M_Addr4 = rsManufacturer.getString("M_Addr4");
            this.M_Phone = rsManufacturer.getString("M_Phone");
            this.M_Fax = rsManufacturer.getString("M_Fax");
            this.M_Email = rsManufacturer.getString("M_Email");
            this.Is_Valid = rsManufacturer.getInt("Is_Valid");
            rsManufacturer.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Manufacturer not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Manufacturer> listManufacturer(int SearchType, String cpnyName, String M_Code, String M_Name, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Manufacturer.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Manufacturer> lstManufacturer = new ArrayList<Manufacturer>();
        Manufacturer manufacturer = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Manufacturer(?,?,?,?,?)}");
            cStmt.setString("p_Code", M_Code);
            cStmt.setString("p_Name", M_Name);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Mfr", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsManufacturer = (ResultSet) cStmt.getObject("cur_Mfr");
            while (rsManufacturer.next()) {
                manufacturer = new Manufacturer(rsManufacturer.getString("M_Code"), rsManufacturer.getString("M_Name"),
                        rsManufacturer.getString("M_Addr1"), rsManufacturer.getString("M_Addr2"), rsManufacturer.getString("M_Addr3"),
                        rsManufacturer.getString("M_Addr4"), rsManufacturer.getString("M_Phone"), rsManufacturer.getString("M_Fax"),
                        rsManufacturer.getString("M_Email"), rsManufacturer.getInt("Is_Valid"));
                lstManufacturer.add(manufacturer);
            }
            rsManufacturer.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Manufacturer not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstManufacturer;
    }

    public void saveManufacturer(String cpnyName, String Save_Flag, String Username, String M_Code, String M_Name, String M_Addr1,
            String M_Addr2, String M_Addr3, String M_Addr4, String M_Phone, String M_Fax,
            String M_Email, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Manufacturer.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Manufacturer(?,?,?,?,?,?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_M_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.M_Code : M_Code));
            cStmt.setString("p_M_Name", M_Name);
            cStmt.setString("p_M_Addr1", M_Addr1);
            cStmt.setString("p_M_Addr2", M_Addr2);
            cStmt.setString("p_M_Addr3", M_Addr3);
            cStmt.setString("p_M_Addr4", M_Addr4);
            cStmt.setString("p_M_Phone", M_Phone);
            cStmt.setString("p_M_Fax", M_Fax);
            cStmt.setString("p_M_Email", M_Email);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.M_Code = (Save_Flag.equalsIgnoreCase("U") ? this.M_Code : M_Code);
            this.M_Name = (!M_Name.equals("") ? M_Name : this.M_Name);
            this.M_Addr1 = (!M_Addr1.equals("") ? M_Addr1 : this.M_Addr1);
            this.M_Addr2 = (!M_Addr2.equals("") ? M_Addr2 : this.M_Addr2);
            this.M_Addr3 = (!M_Addr3.equals("") ? M_Addr3 : this.M_Addr3);
            this.M_Addr4 = (!M_Addr4.equals("") ? M_Addr4 : this.M_Addr4);
            this.M_Phone = (!M_Phone.equals("") ? M_Phone : this.M_Phone);
            this.M_Fax = (!M_Fax.equals("") ? M_Fax : this.M_Fax);
            this.M_Email = (!M_Email.equals("") ? M_Email : this.M_Email);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_MANUFACTURER") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteManufacturer(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Manufacturer.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Manufacturer(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_M_Code", this.M_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.M_Code = "";
            this.M_Name = "";
            this.M_Addr1 = "";
            this.M_Addr2 = "";
            this.M_Addr3 = "";
            this.M_Addr4 = "";
            this.M_Phone = "";
            this.M_Fax = "";
            this.M_Email = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_MANUFACTURER") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Manufacturer.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Manufacturer(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Name", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Mfr", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Mfr");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repManf.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Manufacturer not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Manufacturer.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getM_Code() {
        return M_Code;
    }

    public String getM_Name() {
        return M_Name;
    }

    public String getM_Addr1() {
        return M_Addr1;
    }

    public String getM_Addr2() {
        return M_Addr2;
    }

    public String getM_Addr3() {
        return M_Addr3;
    }

    public String getM_Addr4() {
        return M_Addr4;
    }

    public String getM_Phone() {
        return M_Phone;
    }

    public String getM_Fax() {
        return M_Fax;
    }

    public String getM_Email() {
        return M_Email;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.M_Addr1 = null;
        this.M_Addr2 = null;
        this.M_Addr3 = null;
        this.M_Addr4 = null;
        this.M_Code = null;
        this.M_Email = null;
        this.M_Fax = null;
        this.M_Name = null;
        this.M_Phone = null;
    }
}
