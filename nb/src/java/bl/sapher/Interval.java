package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Anoop Varma, Focal3 LLC.
 */
public class Interval {

    private int nIntervalCode;
    private String strInterval;
    private int nValidity;
    private static String Log_File_Name;

    public Interval(String logFilename) {
        Interval.Log_File_Name = logFilename;
    }

    public Interval(int code, String intervalDesc, int validity) {
        this.nIntervalCode = code;
        this.strInterval = intervalDesc;
        this.nValidity = validity;
    }

    public static ArrayList<Interval> listDosageInterval(String cpnyName,
            String intervalCode, String intervalDesc, int valid)
            throws DBConnectionException, GenericException, SQLException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Interval.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Interval> lstInterval = new ArrayList<Interval>();
        Interval interval = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Interval(?,?,?,?)}");
            cStmt.setString("p_Interval_Code", intervalCode);
            cStmt.setString("p_Interval", intervalDesc);
            cStmt.setInt("p_Active", valid);
            cStmt.registerOutParameter("cur_Interval", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsInterval = (ResultSet) cStmt.getObject("cur_Interval");
            while (rsInterval.next()) {
                interval = new Interval(rsInterval.getInt("Interval_Code"), rsInterval.getString("Interval"),
                        rsInterval.getInt("is_valid"));
                lstInterval.add(interval);
            }
            rsInterval.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Interval.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Interval.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Interval not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Interval.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Interval.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstInterval;
    }

    /**
     * @return the nIntervalCode
     */
    public int getIntervalCode() {
        return nIntervalCode;
    }

    /**
     * @return the strInterval
     */
    public String getInterval() {
        return strInterval;
    }
}
