/*
 * CDL21
 * Sex.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Sex implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -4388499574837096161L;
    private String Sex_Code;
    private String Sex_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public Sex() {
        this.Sex_Code = "";
        this.Sex_Desc = "";
        this.Is_Valid = -1;
    }

    public Sex(String logFilename) {
        this.Sex_Code = "";
        this.Sex_Desc = "";
        this.Is_Valid = -1;
        Sex.Log_File_Name = logFilename;
    }

    public Sex(String Sex_Code, String Sex_Desc, int Is_Valid) {
        this.Sex_Code = Sex_Code;
        this.Sex_Desc = Sex_Desc;
        this.Is_Valid = Is_Valid;
    }

    public Sex(String cpnyName, String Sex_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Sex(?,?,?,?,?)}");
            cStmt.setString("p_Code", Sex_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Sex", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSex = (ResultSet) cStmt.getObject("cur_Sex");
            rsSex.next();
            this.Sex_Code = Sex_Code;
            this.Sex_Desc = rsSex.getString("Sex_Desc");
            this.Is_Valid = rsSex.getInt("Is_Valid");
            rsSex.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Sex Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Sex> listSex(int SearchType, String cpnyName, String Sex_Code, String Sex_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Sex.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Sex> lstSex = new ArrayList<Sex>();
        Sex sex = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Sex(?,?,?,?,?)}");
            cStmt.setString("p_Code", Sex_Code);
            cStmt.setString("p_Desc", Sex_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Sex", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSex = (ResultSet) cStmt.getObject("cur_Sex");
            while (rsSex.next()) {
                sex = new Sex(rsSex.getString("Sex_Code"), rsSex.getString("Sex_Desc"),
                        rsSex.getInt("Is_Valid"));
                lstSex.add(sex);
            }
            rsSex.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Sex Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstSex;
    }

    public void saveSex(String cpnyName, String Save_Flag, String Username, String Sex_Code, String Sex_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Sex.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Sex(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Sex_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Sex_Code : Sex_Code));
            cStmt.setString("p_Sex_Desc", Sex_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Sex_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Sex_Code : Sex_Code);
            this.Sex_Desc = (Sex_Desc != "" ? Sex_Desc : this.Sex_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_SEX") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_SEX") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteSex(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Sex.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Sex(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Sex_Code", this.Sex_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Sex_Code = "";
            this.Sex_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_SEX") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Sex.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Sex(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Sex", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Sex");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repSex.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Sex not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Sex.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Sex.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getSex_Code() {
        return Sex_Code;
    }

    public String getSex_Desc() {
        return Sex_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Sex_Code = null;
        this.Sex_Desc = null;
    }
}
