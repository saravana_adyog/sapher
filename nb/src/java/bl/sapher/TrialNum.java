/*
 * CDL22
 * TrialNum.java
 * Created on November 30, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class TrialNum implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -874261425410153223L;
    private String Trial_Num;
    private int Is_Valid;
    private static String Log_File_Name;

    public TrialNum() {
        this.Trial_Num = "";
        this.Is_Valid = -1;
    }

    public TrialNum(String logFilename) {
        this.Trial_Num = "";
        this.Is_Valid = -1;
        TrialNum.Log_File_Name = logFilename;
    }

    public TrialNum(String Trial_Num, int Is_Valid) {
        this.Trial_Num = Trial_Num;
        this.Is_Valid = Is_Valid;
    }

    public TrialNum(String cpnyName, String Trial_Num)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Trial(?,?,?,?)}");
            cStmt.setString("p_Code", Trial_Num);
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Trial", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsTrialNum = (ResultSet) cStmt.getObject("cur_Trial");
            rsTrialNum.next();
            this.Trial_Num = Trial_Num;
            this.Is_Valid = rsTrialNum.getInt("Is_Valid");
            rsTrialNum.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Trial Number not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<TrialNum> listTrialNum(int SearchType, String cpnyName, String Trial_Num, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "TrialNum.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<TrialNum> lstTrialNum = new ArrayList<TrialNum>();
        TrialNum trialnum = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Trial(?,?,?,?)}");
            cStmt.setString("p_Code", Trial_Num);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Trial", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsTrialNum = (ResultSet) cStmt.getObject("cur_Trial");
            while (rsTrialNum.next()) {
                trialnum = new TrialNum(rsTrialNum.getString("Trial_Num"),
                        rsTrialNum.getInt("Is_Valid"));
                lstTrialNum.add(trialnum);
            }
            rsTrialNum.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Trial Number not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstTrialNum;
    }

    public void saveTrialNum(String cpnyName, String Save_Flag, String Username,
            String Trial_Num, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "TrialNum.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Trial_Num(?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Trial_Num",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Trial_Num : Trial_Num));
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Trial_Num = (Save_Flag.equalsIgnoreCase("U") ? this.Trial_Num : Trial_Num);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_TRIAL_NUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteTrialNum(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "TrialNum.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Trial_Num(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Trial_Num", this.Trial_Num);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Trial_Num = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_TRIALNUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "TrialNum.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Trial(?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Trial", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Trial");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repTrailNum.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Trial num not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "TrialNum.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "TrialNum.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getTrial_Num() {
        return Trial_Num;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Trial_Num = null;
    }
}
