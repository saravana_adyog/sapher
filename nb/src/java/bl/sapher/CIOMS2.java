/*
 * CIOMS2.java
 * Created on November 23, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CIOMS2 implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = -6735608075394078325L;
    private String Body_Sys_Num;
    private String Body_System;
    private int Rep_Num;
    private String Country_Nm;
    private String Source_Desc;
    private String Sex_Code;
    private int Age;
    private String Unit_Code;
    private int Treat_Dur;
    private String Outcome;
    private String Llt_Code;
    private String Llt_Desc;
    private String Diag_Code;
    private String Diag_Desc;
    private String Classification_Code;
    private String Labelling;
    private String Short_Comment;
    private static String Log_File_Name;

    public CIOMS2(String logFilename) {
        this.Body_Sys_Num = "";
        this.Body_System = "";
        this.Rep_Num = -1;
        this.Country_Nm = "";
        this.Source_Desc = "";
        this.Sex_Code = "";
        this.Age = -1;
        this.Unit_Code = "";
        this.Treat_Dur = -1;
        this.Outcome = "";
        this.Llt_Code = "";
        this.Llt_Desc = "";
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Classification_Code = "";
        this.Labelling = "";
        this.Short_Comment = "";
        CIOMS2.Log_File_Name = logFilename;
    }

    public CIOMS2(String Body_Sys_Num, String Body_System, int Rep_Num,
            String Country_Nm, String Source_Desc, String Sex_Code, int Age,
            String Unit_Code, int Treat_Dur, String Outcome, String Llt_Code,
            String Llt_Desc, String Diag_Code, String Diag_Desc,
            String Classification_Code, String Labelling, String Short_Comment) {
        this.Body_Sys_Num = Body_Sys_Num;
        this.Body_System = Body_System;
        this.Rep_Num = Rep_Num;
        this.Country_Nm = Country_Nm;
        this.Source_Desc = Source_Desc;
        this.Sex_Code = Sex_Code;
        this.Age = Age;
        this.Unit_Code = Unit_Code;
        this.Treat_Dur = Treat_Dur;
        this.Outcome = Outcome;
        this.Llt_Code = Llt_Code;
        this.Llt_Desc = Llt_Desc;
        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Classification_Code = Classification_Code;
        this.Labelling = Labelling;
        this.Short_Comment = Short_Comment;
    }

    public static ArrayList listCIOMS2(String cpnyName, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "CIOMS2.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        
        ArrayList<CIOMS2> lstCIOMS2 = new ArrayList<CIOMS2>();
        CIOMS2 c2 = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Cioms2_Proc(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curCioms2", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCIOMS2 = (ResultSet) cStmt.getObject("p_curCioms2");
            while (rsCIOMS2.next()) {
                c2 = new CIOMS2(rsCIOMS2.getString("Body_Sys_Num"),
                        rsCIOMS2.getString("Body_System"), rsCIOMS2.getInt("Rep_Num"),
                        rsCIOMS2.getString("Country_Nm"), rsCIOMS2.getString("Source_Desc"),
                        rsCIOMS2.getString("Sex_Code"), rsCIOMS2.getInt("Age"),
                        rsCIOMS2.getString("Unit_Code"), rsCIOMS2.getInt("Duration"),
                        rsCIOMS2.getString("Outcome"), rsCIOMS2.getString("Llt_Code"),
                        rsCIOMS2.getString("Llt_Desc"), rsCIOMS2.getString("Diag_Code"),
                        rsCIOMS2.getString("Diag_Desc"), rsCIOMS2.getString("Classification_Code"),
                        rsCIOMS2.getString("Labelling"), rsCIOMS2.getString("Short_Comment"));
                lstCIOMS2.add(c2);
            }
            rsCIOMS2.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("CIOMS2 records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstCIOMS2;
    }

    public static ResultSet listCIOMS2(DBCon dbCon, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "CIOMS2.java; Inside listing function returning ResultSet");

        ResultSet rsCIOMS2 = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Cioms2_Proc(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curCioms2", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            rsCIOMS2 = (ResultSet) cStmt.getObject("p_curCioms2");
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("CIOMS2 records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CIOMS2.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsCIOMS2;
    }

    public String getBody_Sys_Num() {
        return Body_Sys_Num;
    }

    public String getBody_System() {
        return Body_System;
    }

    public String getRep_Num() {
        return String.valueOf(Rep_Num);
    }

    public String getCountry_Nm() {
        return Country_Nm;
    }

    public String getSource_Desc() {
        return Source_Desc;
    }

    public String getSex_Code() {
        return Sex_Code;
    }

    public int getAge() {
        return Age;
    }

    public String getUnit_Code() {
        return Unit_Code;
    }

    public int getTreat_Dur() {
        return Treat_Dur;
    }

    public String getOutcome() {
        return Outcome;
    }

    public String getLlt_Code() {
        return Llt_Code;
    }

    public String getLlt_Desc() {
        return Llt_Desc;
    }

    public String getDiag_Code() {
        return Diag_Code;
    }

    public String getDiag_Desc() {
        return Diag_Desc;
    }

    public String getClassification_Code() {
        return Classification_Code;
    }

    public String getLabelling() {
        return Labelling;
    }

    public String getShort_Comment() {
        return Short_Comment;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Body_Sys_Num = null;
        this.Body_System = null;
        this.Rep_Num = 0;
        this.Country_Nm = null;
        this.Source_Desc = null;
        this.Sex_Code = null;
        this.Age = 0;
        this.Unit_Code = null;
        this.Treat_Dur = 0;
        this.Outcome = null;
        this.Llt_Code = null;
        this.Llt_Desc = null;
        this.Diag_Code = null;
        this.Diag_Desc = null;
        this.Classification_Code = null;
        this.Labelling = null;
        this.Short_Comment = null;
    }
}
