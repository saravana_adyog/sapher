/*
 * CDL13
 * Formulation.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Formulation implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 5145384194094584980L;
    private String Formulation_Code;
    private String Formulation_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public Formulation() {
        this.Formulation_Code = "";
        this.Formulation_Desc = "";
        this.Is_Valid = -1;
    }

    public Formulation(String logFilename) {
        this.Formulation_Code = "";
        this.Formulation_Desc = "";
        this.Is_Valid = -1;
        Formulation.Log_File_Name = logFilename;
    }

    public Formulation(String Formulation_Code, String Formulation_Desc, int Is_Valid) {
        this.Formulation_Code = Formulation_Code;
        this.Formulation_Desc = Formulation_Desc;
        this.Is_Valid = Is_Valid;
    }

    public Formulation(String cpnyName, String Formulation_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Formulation(?,?,?,?,?)}");
            cStmt.setString("p_Code", Formulation_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Frmln", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsFormulation = (ResultSet) cStmt.getObject("cur_Frmln");
            rsFormulation.next();
            this.Formulation_Code = Formulation_Code;
            this.Formulation_Desc = rsFormulation.getString("Formulation_Desc");
            this.Is_Valid = rsFormulation.getInt("Is_Valid");
            rsFormulation.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Formulation Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Formulation> listFormulation(int SearchType, String cpnyName, String Formulation_Code, String Formulation_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Formulation.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Formulation> lstFormulation = new ArrayList<Formulation>();
        Formulation forml = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Formulation(?,?,?,?,?)}");
            cStmt.setString("p_Code", Formulation_Code);
            cStmt.setString("p_Desc", Formulation_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Frmln", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsFormulation = (ResultSet) cStmt.getObject("cur_Frmln");
            while (rsFormulation.next()) {
                forml = new Formulation(rsFormulation.getString("Formulation_Code"),
                        rsFormulation.getString("Formulation_Desc"),
                        rsFormulation.getInt("Is_Valid"));
                lstFormulation.add(forml);
            }
            rsFormulation.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Formulation not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstFormulation;
    }

    public void saveFormulation(String cpnyName, String Save_Flag, String Username,
            String Formulation_Code, String Formulation_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Formulation.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Formulation(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Formulation_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Formulation_Code : Formulation_Code));
            cStmt.setString("p_Formulation_Desc", Formulation_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Formulation_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Formulation_Code : Formulation_Code);
            this.Formulation_Desc = (!Formulation_Desc.equals("") ? Formulation_Desc : this.Formulation_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_Formulation") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_Formulation") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteFormulation(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Formulation.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Formulation(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Formulation_Code", this.Formulation_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Formulation_Code = "";
            this.Formulation_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_FORMULATION") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Formulation.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Formulation(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Frmln", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Frmln");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repFormulation.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Formulation not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Formulation.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Formulation.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getFormulation_Code() {
        return Formulation_Code;
    }

    public String getFormulation_Desc() {
        return Formulation_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Formulation_Code = null;
        this.Formulation_Desc = null;
    }
}
