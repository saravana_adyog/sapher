
/* CaseDetails.java
 * Created on December 3, 2007
 * @author Jaikishan S, Arun P. Jose, Anoop Varma.
 */
package bl.sapher;

import bl.sapher.general.Constants;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class to handle class details.
 * 
 * @author Jaikishan S, Arun P. Jose, Anoop Varma.
 */
public class CaseDetails implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -3481988137668259020L;
    private int Rep_Num;
    private String rType;
    private String trialNum;
    private String cntry;
    private String repStat;
    private String repStatDesc;
    private String infSource;
    private String mfr;
    private String fmln;
    private String unit;
    private String dRgmn;
    private String dFreq;
    private String route;
    private String indication;
    private String sex;
    private String eOrig;
    private String aeOutcome;
    private String pdt;
    private String phyAssess;
    private String coAssess;
    private String rRes;
    private String sCompany;
    private String subLha;
    private Date Date_Entry;
    private String Recv_By_At_Hq;
    private Date Date_Recv;
    private String Rep_Num_Local;
    private String Rep_Xref_Num;
    private String Clin_Rep_Info;
    private String Comp_Rep_Info;
    private String Patient_Num;
    private String Hosp_Nm;
    private String Batch_Num;
    private float Dose;
    private Date Treat_Start;
    private Date Treat_Stop;
    private String Rech_Flag;
    private String Authorization_No;
    private String P_Initials;
    private Date Date_Of_Birth;
    private int Age_Reported;
    private int Weight;
    private int Height;
    private String Pregnancy;
    private int Pregnancy_Week;
    private String Short_History;
    private Date Outcome_Rep_Date;
    private String Autopsy;
    private String Autopsy_Result;
    private Date Phys_Assessment_Date;
    private Date Co_Assessment_Date;
    private String Phys_Assessment_Reason;
    private String Co_Assessment_Reason;
    private String Contributing_Factors;
    private Date Rechallenge_Date;
    private String Rechallenge_Dose;
    private String Verbatim;
    private String Short_Comment;
    private Date Distbn_Date;
    private Date Submission_Date;
    private String Further_Commn;
    private int Is_Suspended;
    private String AE_Desc;
    // Newly added fields. [15/01/2008]
    private String Type_Desc;
    private String Country_Nm;
    private String Status_Desc;
    private String Source_Desc;
    private String Source_Code;
    private String M_Name;
    private String Formulation_Desc;
    private String Unit_Desc;
    private String Dregimen_Desc;
    private String Dfreq_Desc;
    private String Route_Desc;
    private String Indication;
    private String Sex_Desc;
    private String Ethnic_Desc;
    private String Outcome;
    private String Generic_Nm;
    private String Brand_Nm;
    private String Phy_Assessment;
    private String Phy_AssessmentCode;
    private String Phy_Assess_Flg;
    private String Co_Assessment;
    private String Co_AssessmentCode;
    private String Co_Assess_Flg;
    private String Result_Desc;
    private String Lha_Desc;
    private int Duration;

    // Newly added fileds related to Dose Regimen. [26/03/2009]
    private int nNoOfDosage;
    private int nDosageInterval;
    private String strIntervalDesc;

    // New filed related to Report type
    private String strReportTypeDesc;
    /** Log file name */
    private static String Log_File_Name;

    public CaseDetails() {
        Rep_Num = -1;
        rType = "";
        strReportTypeDesc = "";
        trialNum = "";
        cntry = "";
        repStat = "";
        infSource = "";
        mfr = "";
        fmln = "";
        unit = "";
        dRgmn = "";
        dFreq = "";
        route = "";
        indication = "";
        sex = "";
        eOrig = "";
        aeOutcome = "";
        pdt = "";
        phyAssess = "";
        coAssess = "";
        rRes = "";
        sCompany = "";
        subLha = "";
        Date_Entry = null;
        Recv_By_At_Hq = "";
        Date_Recv = null;
        Rep_Num_Local = "";
        Rep_Xref_Num = "";
        Clin_Rep_Info = "";
        Comp_Rep_Info = "";
        Patient_Num = "";
        Hosp_Nm = "";
        Batch_Num = "";
        Dose = -1;
        Treat_Start = null;
        Treat_Stop = null;
        Rech_Flag = "";
        Authorization_No = "";
        P_Initials = "";
        Date_Of_Birth = null;
        Age_Reported = -1;
        Weight = -1;
        Height = -1;
        Pregnancy = "";
        Pregnancy_Week = -1;
        Short_History = "";
        Outcome_Rep_Date = null;
        Autopsy = "";
        Autopsy_Result = "";
        Phys_Assessment_Date = null;
        Co_Assessment_Date = null;
        Phys_Assessment_Reason = "";
        Co_Assessment_Reason = "";
        Contributing_Factors = "";
        Rechallenge_Date = null;
        Rechallenge_Dose = "";
        Verbatim = "";
        Short_Comment = "";
        Distbn_Date = null;
        Submission_Date = null;
        Further_Commn = "";
        AE_Desc = "";
        Is_Suspended = -1;

        Type_Desc = "";
        Country_Nm = "";
        Status_Desc = "";
        Source_Desc = "";
        M_Name = "";
        Formulation_Desc = "";
        Unit_Desc = "";
        Dregimen_Desc = "";
        Dfreq_Desc = "";
        Route_Desc = "";
        Indication = "";
        Sex_Desc = "";
        Ethnic_Desc = "";
        Outcome = "";
        Generic_Nm = "";
        Brand_Nm = "";
        Phy_Assessment = "";
        Phy_Assess_Flg = "";
        Co_Assessment = "";
        Co_Assess_Flg = "";
        Result_Desc = "";
        Lha_Desc = "";
        Duration = -1;
    }

    public CaseDetails(String logFilename) {
        Rep_Num = -1;
        rType = "";
        strReportTypeDesc = "";
        trialNum = "";
        cntry = "";
        repStat = "";
        infSource = "";
        mfr = "";
        fmln = "";
        unit = "";
        dRgmn = "";
        dFreq = "";
        route = "";
        indication = "";
        sex = "";
        eOrig = "";
        aeOutcome = "";
        pdt = "";
        phyAssess = "";
        coAssess = "";
        rRes = "";
        sCompany = "";
        subLha = "";
        Date_Entry = null;
        Recv_By_At_Hq = "";
        Date_Recv = null;
        Rep_Num_Local = "";
        Rep_Xref_Num = "";
        Clin_Rep_Info = "";
        Comp_Rep_Info = "";
        Patient_Num = "";
        Hosp_Nm = "";
        Batch_Num = "";
        Dose = -1;
        Treat_Start = null;
        Treat_Stop = null;
        Rech_Flag = "";
        Authorization_No = "";
        P_Initials = "";
        Date_Of_Birth = null;
        Age_Reported = -1;
        Weight = -1;
        Height = -1;
        Pregnancy = "";
        Pregnancy_Week = -1;
        Short_History = "";
        Outcome_Rep_Date = null;
        Autopsy = "";
        Autopsy_Result = "";
        Phys_Assessment_Date = null;
        Co_Assessment_Date = null;
        Phys_Assessment_Reason = "";
        Co_Assessment_Reason = "";
        Contributing_Factors = "";
        Rechallenge_Date = null;
        Rechallenge_Dose = "";
        Verbatim = "";
        Short_Comment = "";
        Distbn_Date = null;
        Submission_Date = null;
        Further_Commn = "";
        AE_Desc = "";
        Is_Suspended = -1;

        Type_Desc = "";
        Country_Nm = "";
        Status_Desc = "";
        Source_Desc = "";
        M_Name = "";
        Formulation_Desc = "";
        Unit_Desc = "";
        Dregimen_Desc = "";
        Dfreq_Desc = "";
        Route_Desc = "";
        Indication = "";
        Sex_Desc = "";
        Ethnic_Desc = "";
        Outcome = "";
        Generic_Nm = "";
        Brand_Nm = "";
        Phy_Assessment = "";
        Phy_Assess_Flg = "";
        Co_Assessment = "";
        Co_Assess_Flg = "";
        Result_Desc = "";
        Lha_Desc = "";
        Duration = -1;
        CaseDetails.Log_File_Name = logFilename;
    }

    public CaseDetails(int Rep_Num, String Trial_No, Date Date_Recv, int Is_Suspended) {
        this.Rep_Num = Rep_Num;
        this.trialNum = Trial_No;
        this.Date_Recv = Date_Recv;
        this.Is_Suspended = Is_Suspended;
    }

    public CaseDetails(int Rep_Num) {
        this.Rep_Num = Rep_Num;
    }

    /**
     * 
     * @param Rep_Num
     * @param Rep_Type
     * @param Trial_No
     * @param ctry
     * @param repStat
     * @param srcInfo
     * @param mfr
     * @param form
     * @param dUnit
     * @param dRgmn
     * @param dFreq
     * @param route
     * @param ind
     * @param sx
     * @param eOrig
     * @param aeOutcome
     * @param pdt
     * @param phy_Assess
     * @param co_Assess
     * @param rechRes
     * @param sCo
     * @param lha
     * @param Date_Entry
     * @param Recv_By_At_Hq
     * @param Date_Recv
     * @param Rep_Num_Local
     * @param Rep_Xref_Num
     * @param Clin_Rep_Info
     * @param Comp_Rep_Info
     * @param Patient_Num
     * @param Hosp_Nm
     * @param Batch_Num
     * @param Dose
     * @param Treat_Start
     * @param Treat_Stop
     * @param Rech_Flag
     * @param Authorization_No
     * @param P_Initials
     * @param Date_Of_Birth
     * @param Age_Reported
     * @param Weight
     * @param Height
     * @param Pregnancy
     * @param Pregnancy_Week
     * @param Short_History
     * @param Outcome_Rep_Date
     * @param Autopsy
     * @param Autopsy_Result
     * @param Phys_Assessment_Date
     * @param Co_Assessment_Date
     * @param Phys_Assessment_Reason
     * @param Co_Assessment_Reason
     * @param Contributing_Factors
     * @param Rechallenge_Date
     * @param Rechallenge_Dose
     * @param Verbatim
     * @param Short_Comment
     * @param Distbn_Date
     * @param Submission_Date
     * @param Further_Commn
     * @param AE_Desc
     * @param Is_Suspended
     * @param Duration
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     * @throws bl.sapher.general.RecordNotFoundException
     */
    public CaseDetails(int Rep_Num, String Rep_Type, String Trial_No, String ctry,
            String repStat, String srcInfo, String mfr, String form,
            String dUnit, String dRgmn, String dFreq, String route,
            String ind, String sx, String eOrig, String aeOutcome,
            String pdt, String phy_Assess, String co_Assess, String rechRes,
            String sCo, String lha, Date Date_Entry, String Recv_By_At_Hq,
            Date Date_Recv, String Rep_Num_Local, String Rep_Xref_Num,
            String Clin_Rep_Info, String Comp_Rep_Info, String Patient_Num,
            String Hosp_Nm, String Batch_Num, float Dose, Date Treat_Start,
            Date Treat_Stop, String Rech_Flag, String Authorization_No,
            String P_Initials, Date Date_Of_Birth, int Age_Reported, int Weight,
            int Height, String Pregnancy, int Pregnancy_Week, String Short_History,
            Date Outcome_Rep_Date, String Autopsy, String Autopsy_Result,
            Date Phys_Assessment_Date, Date Co_Assessment_Date,
            String Phys_Assessment_Reason, String Co_Assessment_Reason,
            String Contributing_Factors, Date Rechallenge_Date, String Rechallenge_Dose,
            String Verbatim, String Short_Comment, Date Distbn_Date,
            Date Submission_Date, String Further_Commn, String AE_Desc,
            int Is_Suspended, int Duration)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        this.Rep_Num = Rep_Num;
        this.rType = Rep_Type;
        this.trialNum = Trial_No;
        this.cntry = ctry;
        this.repStat = repStat;
        this.infSource = srcInfo;
        this.mfr = mfr;
        this.fmln = form;
        this.unit = dUnit;
        this.dRgmn = dRgmn;
        this.dFreq = dFreq;
        this.route = route;
        this.indication = ind;
        this.sex = sx;
        this.eOrig = eOrig;
        this.aeOutcome = aeOutcome;
        this.pdt = pdt;
        this.phyAssess = phy_Assess;
        this.coAssess = co_Assess;
        this.rRes = rechRes;
        this.sCompany = sCo;
        this.subLha = lha;
        this.Date_Entry = Date_Entry;
        this.Recv_By_At_Hq = Recv_By_At_Hq;
        this.Date_Recv = Date_Recv;
        this.Rep_Num_Local = Rep_Num_Local;
        this.Rep_Xref_Num = Rep_Xref_Num;
        this.Clin_Rep_Info = Clin_Rep_Info;
        this.Comp_Rep_Info = Comp_Rep_Info;
        this.Patient_Num = Patient_Num;
        this.Hosp_Nm = Hosp_Nm;
        this.Batch_Num = Batch_Num;
        this.Dose = Dose;
        this.Treat_Start = Treat_Start;
        this.Treat_Stop = Treat_Stop;
        this.Rech_Flag = Rech_Flag;
        this.Authorization_No = Authorization_No;
        this.P_Initials = P_Initials;
        this.Date_Of_Birth = Date_Of_Birth;
        this.Age_Reported = Age_Reported;
        this.Weight = Weight;
        this.Height = Height;
        this.Pregnancy = Pregnancy;
        this.Pregnancy_Week = Pregnancy_Week;
        this.Short_History = Short_History;
        this.Outcome_Rep_Date = Outcome_Rep_Date;
        this.Autopsy = Autopsy;
        this.Autopsy_Result = Autopsy_Result;
        this.Phys_Assessment_Date = Phys_Assessment_Date;
        this.Co_Assessment_Date = Co_Assessment_Date;
        this.Phys_Assessment_Reason = Phys_Assessment_Reason;
        this.Co_Assessment_Reason = Co_Assessment_Reason;
        this.Contributing_Factors = Contributing_Factors;
        this.Rechallenge_Date = Rechallenge_Date;
        this.Rechallenge_Dose = Rechallenge_Dose;
        this.Verbatim = Verbatim;
        this.Short_Comment = Short_Comment;
        this.Distbn_Date = Distbn_Date;
        this.Submission_Date = Submission_Date;
        this.Further_Commn = Further_Commn;
        this.AE_Desc = AE_Desc;
        this.Is_Suspended = Is_Suspended;
        this.Duration = Duration;
    }

    /**
     * The new constructor that fetches data from a database view.
     *
     * @param From_View A dummy parameter that is supposed to be 'y' always.
     *                  This is just because, single parameter constructor is already in use.
     *                  This param MUST be removed.
     * @param Rep_Num Report number as <code><b>int</b></code>.
     *
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public CaseDetails(String cpnyName, final String From_View, int Rep_Num)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Case(?,?,?,?,?,?,?,?)}");

            cStmt.setInt("p_RepNum", Rep_Num);
            cStmt.setString("p_Diag_Code", Constants.SEARCH_ANY_VAL);
            cStmt.setString("p_Trial_Num", Constants.SEARCH_ANY_VAL);
            cStmt.setString("p_Prod_Code", Constants.SEARCH_ANY_VAL);
            cStmt.setString("p_Frm", Constants.SEARCH_ANY_VAL);
            cStmt.setString("p_To", Constants.SEARCH_ANY_VAL);
            cStmt.setInt("p_Is_Suspended", Constants.STATUS_ALL);
            cStmt.registerOutParameter("cur_Case", oracle.jdbc.OracleTypes.CURSOR);

            cStmt.execute();
            ResultSet rsCase = (ResultSet) cStmt.getObject("cur_Case");
            rsCase.next();

            this.Rep_Num = Rep_Num;
            this.rType = rsCase.getString("cd_Type_Code");
            this.strReportTypeDesc = rsCase.getString("cd_Type_Desc");
            this.trialNum = rsCase.getString("cd_Trial_Num");
            this.cntry = rsCase.getString("cd_Country_Code");
            this.Country_Nm = rsCase.getString("cd_Country_Nm");
            this.repStat = rsCase.getString("cd_Status_Code");
            this.infSource = rsCase.getString("cd_Source_code");
            this.mfr = rsCase.getString("cd_M_Code");
            this.fmln = rsCase.getString("cd_Formulation_Code");
            this.unit = rsCase.getString("cd_Unit_Code");
            this.dRgmn = rsCase.getString("cd_Dregimen_Code");
            this.dFreq = rsCase.getString("cd_Dfreq_Code");
            this.route = rsCase.getString("cd_Route_Code");
            this.indication = rsCase.getString("cd_Indication_Num");
            this.sex = rsCase.getString("cd_Sex_Code");
            this.eOrig = rsCase.getString("cd_Ethnic_Code");
            this.aeOutcome = rsCase.getString("cd_Outcome_Code");
            this.pdt = rsCase.getString("cd_Product_Code");
            this.phyAssess = rsCase.getString("pac_Assessment_Code");
            this.coAssess = rsCase.getString("cac_Assessment_Code");
            this.rRes = rsCase.getString("cd_Result_Code");
            this.sCompany = rsCase.getString("cd_Subm_Company_Nm");
            this.subLha = rsCase.getString("cd_Lha_Code");
            this.Date_Entry = rsCase.getDate("cd_Date_Entry");
            this.Recv_By_At_Hq = rsCase.getString("cd_Recv_By_At_Hq");
            this.Date_Recv = rsCase.getDate("cd_Date_Recv");
            this.Rep_Num_Local = rsCase.getString("cd_Rep_Num_Local");
            this.Rep_Xref_Num = rsCase.getString("cd_Rep_Xref_Num");
            this.Clin_Rep_Info = rsCase.getString("cd_Clin_Rep_Info");
            this.Comp_Rep_Info = rsCase.getString("cd_Comp_Rep_Info");
            this.Patient_Num = rsCase.getString("cd_Patient_Num");
            this.Hosp_Nm = rsCase.getString("cd_Hosp_Nm");
            this.Batch_Num = rsCase.getString("cd_Batch_Num");
            this.Dose = rsCase.getFloat("cd_Dose");
            this.Treat_Start = rsCase.getDate("cd_Treat_Start");
            this.Treat_Stop = rsCase.getDate("cd_Treat_Stop");
            this.Rech_Flag = rsCase.getString("cd_Rechallenge");
            this.Authorization_No = rsCase.getString("cd_Authorization_No");
            this.P_Initials = rsCase.getString("cd_P_Initials");
            this.Date_Of_Birth = rsCase.getDate("cd_Date_Of_Birth");
            this.Age_Reported = rsCase.getInt("cd_Age_Reported");
            this.Weight = rsCase.getInt("cd_Weight");
            this.Height = rsCase.getInt("cd_Height");
            this.Pregnancy = rsCase.getString("cd_Pregnancy");
            this.Pregnancy_Week = rsCase.getInt("cd_Pregnancy_Week");
            this.Short_History = rsCase.getString("cd_Short_History");
            this.Outcome_Rep_Date = rsCase.getDate("cd_Outcome_Rep_Date");
            this.Autopsy = rsCase.getString("cd_Autopsy");
            this.Autopsy_Result = rsCase.getString("cd_Autopsy_Result");
            this.Phys_Assessment_Date = rsCase.getDate("cd_Phy_Assessment_Date");
            this.Co_Assessment_Date = rsCase.getDate("cd_Co_Assessment_Date");
            this.Phys_Assessment_Reason = rsCase.getString("cd_Phy_Assessment_Reason");
            this.Co_Assessment_Reason = rsCase.getString("cd_Co_Assessment_Reason");
            this.Contributing_Factors = rsCase.getString("cd_Contributing_Factors");
            this.Rechallenge_Date = rsCase.getDate("cd_Rechallenge_Date");
            this.Rechallenge_Dose = rsCase.getString("cd_Rechallenge_Dose");
            this.Verbatim = rsCase.getString("cd_Verbatim");
            this.Short_Comment = rsCase.getString("cd_Short_Comment");
            this.Distbn_Date = rsCase.getDate("cd_Distbn_Date");
            this.Submission_Date = rsCase.getDate("cd_Submission_Date");
            this.Further_Commn = rsCase.getString("cd_Further_Commn");
            this.AE_Desc = rsCase.getString("cd_AE_Desc");
            this.Is_Suspended = rsCase.getInt("cd_Is_Suspended");

            this.Source_Desc = rsCase.getString("cd_Source_Desc");
            this.Source_Code = rsCase.getString("cd_Status_Desc");
            this.M_Name = rsCase.getString("cd_M_Name");
            this.Formulation_Desc = rsCase.getString("cd_Formulation_Desc");
            this.Unit_Desc = rsCase.getString("cd_Unit_Desc");
            this.Dregimen_Desc = rsCase.getString("cd_Dregimen_Desc");
            this.Dfreq_Desc = rsCase.getString("cd_Dfreq_Desc");
            this.Route_Desc = rsCase.getString("cd_Route_Desc");
            this.Indication = rsCase.getString("cd_Indication");
            this.Sex_Desc = rsCase.getString("cd_Sex_Desc");
            this.Ethnic_Desc = rsCase.getString("cd_Ethnic_Desc");
            this.Outcome = rsCase.getString("cd_Outcome");
            this.Generic_Nm = rsCase.getString("cd_Generic_Nm");
            this.Brand_Nm = rsCase.getString("cd_Brand_Nm");
            this.Phy_AssessmentCode = rsCase.getString("pac_Assessment_Code");
            this.Phy_Assessment = rsCase.getString("cd_Phy_Assessment");
            this.Phy_Assess_Flg = rsCase.getString("cd_Phy_Assess_Flg");
            this.Co_AssessmentCode = rsCase.getString("cac_Assessment_Code");
            this.Co_Assessment = rsCase.getString("cd_Co_Assessment");
            this.Co_Assess_Flg = rsCase.getString("cd_Co_Assess_Flg");
            this.Result_Desc = rsCase.getString("cd_Result_Desc");
            this.Lha_Desc = rsCase.getString("cd_Lha_Desc");
            this.Duration = rsCase.getInt("cd_Duration");
            this.repStatDesc = rsCase.getString("cd_Status_Desc");

            this.nNoOfDosage = rsCase.getInt("cd_No_Of_Dosage");
            this.nDosageInterval = rsCase.getInt("cd_Dosage_Interval");
            this.strIntervalDesc = rsCase.getString("cd_Interval");

            rsCase.close();
            cStmt.close();
            dbCon.closeCon();

        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Case not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * This static function is used to list all case details from a database view.
     *
     * @param Rep_Num Report number as <code><b>int</b></code>.
     * @param strDiagCode 
     * @param Trial_Num 
     * @param Prod_Code Product code as <code><b>String</b></code>.
     * @param From_Date_Recv From date as <code><b>String</b></code>.
     * @param To_Date_Recv To date as <code><b>String</b></code>.
     * @param Is_Suspended Suspend status as <code><b>int</b></code>.
     *
     * @return <code><b>CaseDetails</b></code> objects as <code><b>ArrayList</b></code>.
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public static ArrayList listCaseFromView(String cpnyName, int Rep_Num, /*String Rep_Type,*/
            String strDiagCode, String Trial_Num,
            String Prod_Code, String From_Date_Recv, String To_Date_Recv, int Is_Suspended)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside listing function");

        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<CaseDetails> lstCase = new ArrayList<CaseDetails>();
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Case(?,?,?,?,?,?,?,?)}");
            cStmt.setInt("p_RepNum", Rep_Num);
            cStmt.setString("p_Diag_Code", strDiagCode);
            cStmt.setString("p_Trial_Num", Trial_Num);
            cStmt.setString("p_Prod_Code", Prod_Code);
            cStmt.setString("p_Frm", From_Date_Recv);
            cStmt.setString("p_To", To_Date_Recv);
            cStmt.setInt("p_Is_Suspended", Is_Suspended);
            cStmt.registerOutParameter("cur_Case", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCase = (ResultSet) cStmt.getObject("cur_Case");

            CaseDetails aCase = null;
            while (rsCase.next()) {
                aCase = new CaseDetails(rsCase.getInt("cd_Rep_Num"),
                        rsCase.getString("cd_Trial_Num"),
                        rsCase.getDate("cd_Date_Recv"),
                        rsCase.getInt("cd_Is_Suspended"));
                lstCase.add(aCase);
            }
            rsCase.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Case not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstCase;
    }

    public static ResultSet listCase(DBCon dbCon, String cpnyName, int Rep_Num, /*String Rep_Type,*/
            String strDiagCode, String Trial_Num,
            String Prod_Code, String From_Date_Recv, String To_Date_Recv, int Is_Suspended)
            throws RecordNotFoundException, DBConnectionException, GenericException {

        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside listing function");

        //DBCon dbCon = new DBCon(cpnyName);
        ResultSet rsCase = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Case(?,?,?,?,?,?,?,?)}");
            cStmt.setInt("p_RepNum", Rep_Num);
            cStmt.setString("p_Diag_Code", strDiagCode);
            cStmt.setString("p_Trial_Num", Trial_Num);
            cStmt.setString("p_Prod_Code", Prod_Code);
            cStmt.setString("p_Frm", From_Date_Recv);
            cStmt.setString("p_To", To_Date_Recv);
            cStmt.setInt("p_Is_Suspended", Is_Suspended);
            cStmt.registerOutParameter("cur_Case", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            rsCase = (ResultSet) cStmt.getObject("cur_Case");

            cStmt.close();
        //dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Case not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsCase;
    }

    public static ArrayList<CaseDetails> listDuplicateCase(String cpnyName,
            String strReceiveDate, String strClinRepInfo,
            String strCntryCode, String strProdCode,
            String strInitials, int nAgeReported,
            String strSexCode, String strTrialNum,
            String strPatientNum,
            String strPTCode, String strAggrPT,
            String strLLTCode, String strAggrLLT,
            String strBodySys, String strSeriousness,
            String strOutcomeCode, String strPhyAssCode,
            String strCoAssCode, String strComplInfo)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside duplicate listing function");

        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<CaseDetails> lstCase = new ArrayList<CaseDetails>();
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Case_Duplicate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cStmt.setString("p_Date_Recv", strReceiveDate);
            cStmt.setString("p_Clin_Rep_Info", strClinRepInfo);
            cStmt.setString("p_Country_Code", strCntryCode);
            cStmt.setString("p_Product_Code", strProdCode);
            cStmt.setString("p_P_Initials", strInitials);
            cStmt.setInt("p_Age_Reported", nAgeReported);
            cStmt.setString("p_Sex_Code", strSexCode);
            cStmt.setString("p_Trial_Num", strTrialNum);
            cStmt.setString("p_Patient_Num", strPatientNum);
            cStmt.setString("p_Diag_Code", strPTCode);
            cStmt.setString("p_Aggr_Of_Diag", strAggrPT);
            cStmt.setString("p_Llt_Code", strLLTCode);
            cStmt.setString("p_Aggr_Of_Llt", strAggrLLT);
            cStmt.setString("p_Body_Sys_Num", strBodySys);
            cStmt.setString("p_Classification_Code", strSeriousness);
            cStmt.setString("p_Outcome_Code", strOutcomeCode);
            cStmt.setString("p_Phy_Assessment_Code", strPhyAssCode);
            cStmt.setString("p_Co_Assessment_Code", strCoAssCode);
            cStmt.setString("p_Verbatim", strComplInfo);

            System.out.println("p_Date_Recv=" + strReceiveDate + ", " +
                    "p_Clin_Rep_Info=" + strClinRepInfo + ", " +
                    "p_Country_Code=" + strCntryCode + ", " +
                    "p_Product_Code=" + strProdCode + ", " +
                    "p_P_Initials=" + strInitials + ", " +
                    "p_Age_Reported=" + nAgeReported + ", " +
                    "p_Sex_Code=" + strSexCode + ", " +
                    "p_Trial_Num=" + strTrialNum + ", " +
                    "p_Patient_Num=" + strPatientNum + ", " +
                    "p_Diag_Code=" + strPTCode + ", " +
                    "p_Aggr_Of_Diag=" + strAggrPT + ", " +
                    "p_Llt_Code=" + strLLTCode + ", " +
                    "p_Aggr_Of_Llt=" + strAggrLLT + ", " +
                    "p_Body_Sys_Num=" + strBodySys + ", " +
                    "p_Classification_Code=" + strSeriousness + ", " +
                    "p_Outcome_Code=" + strOutcomeCode + ", " +
                    "p_Phy_Assessment_Code=" + strPhyAssCode + ", " +
                    "p_Co_Assessment_Code=" + strCoAssCode + ", " +
                    "p_Verbatim=" + strComplInfo);

            cStmt.registerOutParameter("cur_Case", oracle.jdbc.OracleTypes.CURSOR);

            cStmt.execute();
            ResultSet rsCase = (ResultSet) cStmt.getObject("cur_Case");

            CaseDetails aCase = null;
            while (rsCase.next()) {
                aCase = new CaseDetails(rsCase.getInt("Rep_Num"));
                lstCase.add(aCase);
            }
            rsCase.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Case not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstCase;
    }

    /**
     * This static function is used to list all distinct patient numbers of Case detail from a database view.
     *
     * @return ArrayList list of patient num
     *
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public static ArrayList listPatientNum(String cpnyName)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside listPatientNum()");

        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<String> lstPNum = new ArrayList<String>();
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.List_Pat_Num(?)}");

            cStmt.registerOutParameter("cur_PatNum", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCase = (ResultSet) cStmt.getObject("cur_PatNum");
            while (rsCase.next()) {
                lstPNum.add(rsCase.getString("Patient_Num"));
            }
            rsCase.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Patient number not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstPNum;
    }

    /**
     * This static function is used to list all distinct report numbers of Case detail from a database view.
     *
     * @return ArrayList list of report num
     *
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public static ArrayList listReportNum(String cpnyName)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside listReportNum()");

        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<String> lstRNum = new ArrayList<String>();
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.List_Rep_Num(?)}");

            cStmt.registerOutParameter("cur_RepNum", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCase = (ResultSet) cStmt.getObject("cur_RepNum");
            while (rsCase.next()) {
                lstRNum.add(rsCase.getString("Rep_Num"));
            }
            rsCase.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Report number not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstRNum;
    }

    /**
     * Function to save a case. This function saves all values (except Short_History,Verbatim,Short_Comment,AE_Desc)
     * of the Case_Details table.
     *
     * @see saveCaseAeSummary
     * @see saveCaseCmSummary
     * 
     * @param Save_Flag as <code><b>String</b></code>.
     * @param Username as <code><b>String</b></code>.
     * @param Rep_Num as <code><b>String</b></code>.
     * @param Rep_Type as <code><b>String</b></code>.
     * @param Trial_No as <code><b>String</b></code>.
     * @param Country_Code as <code><b>String</b></code>.
     * @param Status_Code as <code><b>String</b></code>.
     * @param Source_Code as <code><b>String</b></code>.
     * @param M_Code as <code><b>String</b></code>.
     * @param Form_Code as <code><b>String</b></code>.
     * @param Unit_Code as <code><b>String</b></code>.
     * @param DRgmn_Code as <code><b>String</b></code>.
     * @param Dfreq_Code as <code><b>String</b></code>.
     * @param Route_Code as <code><b>String</b></code>.
     * @param Ind_Num as <code><b>String</b></code>.
     * @param Sex_Code as <code><b>String</b></code>.
     * @param Ethnic_Orig as <code><b>String</b></code>.
     * @param Outcome_Code as <code><b>String</b></code>.
     * @param Prod_Code as <code><b>String</b></code>.
     * @param Phy_Assess_Code as <code><b>String</b></code>.
     * @param Co_Assess_Code as <code><b>String</b></code>.
     * @param Res_Code as <code><b>String</b></code>.
     * @param Distbn_Co as <code><b>String</b></code>.
     * @param Sub_Lha as <code><b>String</b></code>.
     * @param Date_Entry as <code><b>Date</b></code>.
     * @param Recv_By_At_Hq as <code><b>String</b></code>.
     * @param Date_Recv as <code><b>Date</b></code>.
     * @param Rep_Num_Local as <code><b>String</b></code>.
     * @param Rep_Xref_Num as <code><b>String</b></code>.
     * @param Clin_Rep_Info as <code><b>String</b></code>.
     * @param Comp_Rep_Info as <code><b>String</b></code>.
     * @param Patient_Num as <code><b>String</b></code>.
     * @param Hosp_Nm as <code><b>String</b></code>.
     * @param Batch_Num as <code><b>String</b></code>.
     * @param Dose as <code><b>float</b></code>.
     * @param Treat_Start as <code><b>Date</b></code>.
     * @param Treat_Stop as <code><b>Date</b></code>.
     * @param Rech_Flag as <code><b>String</b></code>.
     * @param Authorization_No as <code><b>String</b></code>.
     * @param P_Initials as <code><b>String</b></code>.
     * @param Date_Of_Birth as <code><b>Date</b></code>.
     * @param Age_Reported as <code><b>int</b></code>.
     * @param Weight as <code><b>int</b></code>.
     * @param Height as <code><b>int</b></code>.
     * @param Pregnancy as <code><b>String</b></code>.
     * @param Pregnancy_Week as <code><b>int</b></code>.
     * @param Outcome_Rep_Date as <code><b>Date</b></code>.
     * @param Autopsy as <code><b>String</b></code>.
     * @param Autopsy_Result as <code><b>String</b></code>.
     * @param Phys_Assessment_Date as <code><b>Date</b></code>.
     * @param Co_Assessment_Date as <code><b>Date</b></code>.
     * @param Phys_Assessment_Reason as <code><b>String</b></code>.
     * @param Co_Assessment_Reason as <code><b>String</b></code>.
     * @param Contributing_Factors as <code><b>String</b></code>.
     * @param Rechallenge_Date as <code><b>Date</b></code>.
     * @param Rechallenge_Dose as <code><b>String</b></code>.
     * @param Distbn_Date as <code><b>Date</b></code>.
     * @param Submission_Date as <code><b>Date</b></code>.
     * @param Further_Commn as <code><b>String</b></code>.
     * @param Is_Suspended as <code><b>String</b></code>.
     * 
     * @return Newly generated report number as <code><b>int</b></code>.
     * 
     * @throws bl.sapher.general.ConstraintViolationException Raised when contraint violation occurs.
     * @throws bl.sapher.general.DBConnectionException Raised when the database connection fails.
     * @throws bl.sapher.general.GenericException Raised when any general error occurs.
     * @throws bl.sapher.general.RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     */
    public int saveCaseDetails(String cpnyName,
            String Save_Flag, String Username, int Rep_Num, String Rep_Type,
            String Trial_No, String Country_Code, String Status_Code, String Source_Code,
            String M_Code, String Form_Code, String Unit_Code, String DRgmn_Code,
            String Dfreq_Code, String Route_Code, String Ind_Num, String Sex_Code,
            String Ethnic_Orig, String Outcome_Code, String Prod_Code, String Phy_Assess_Code,
            String Co_Assess_Code, String Res_Code, String Distbn_Co, String Sub_Lha,
            Date Date_Entry, String Recv_By_At_Hq, Date Date_Recv, String Rep_Num_Local,
            String Rep_Xref_Num, String Clin_Rep_Info, String Comp_Rep_Info, String Patient_Num,
            String Hosp_Nm, String Batch_Num, float Dose, Date Treat_Start,
            Date Treat_Stop, String Rech_Flag, String Authorization_No, String P_Initials,
            Date Date_Of_Birth, int Age_Reported, int Weight, int Height,
            String Pregnancy, int Pregnancy_Week, String Short_History, Date Outcome_Rep_Date,
            String Autopsy, String Autopsy_Result, Date Phys_Assessment_Date, Date Co_Assessment_Date,
            String Phys_Assessment_Reason, String Co_Assessment_Reason, String Contributing_Factors, Date Rechallenge_Date,
            String Rechallenge_Dose, String Verbatim, String Short_Comment, Date Distbn_Date,
            Date Submission_Date, String Further_Commn, String AE_Desc, int Is_Suspended)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Case_Details(?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Rep_Num : Rep_Num));
            cStmt.setString("p_Type_Code", Rep_Type);
            cStmt.setString("p_Trial_Num", Trial_No);
            cStmt.setString("p_Country_Code", Country_Code);
            cStmt.setString("p_Status_Code", Status_Code);
            cStmt.setString("p_Source_Code", Source_Code);
            cStmt.setString("p_M_Code", M_Code);
            cStmt.setString("p_Formulation_Code", Form_Code);
            cStmt.setString("p_Unit_Code", Unit_Code);
            cStmt.setString("p_Dregimen_Code", DRgmn_Code);
            cStmt.setString("p_Dfreq_Code", Dfreq_Code);
            cStmt.setString("p_Route_Code", Route_Code);
            cStmt.setString("p_Indication_Num", Ind_Num);
            cStmt.setString("p_Sex_Code", Sex_Code);
            cStmt.setString("p_Ethnic_Code", Ethnic_Orig);
            cStmt.setString("p_Outcome_Code", Outcome_Code);
            cStmt.setString("p_Product_Code", Prod_Code);
            cStmt.setString("p_Phy_Assessment_Code", Phy_Assess_Code);
            cStmt.setString("p_Co_Assessment_Code", Co_Assess_Code);
            cStmt.setString("p_Result_Code", Res_Code);
            cStmt.setString("p_Subm_Company_Nm", Distbn_Co);
            cStmt.setString("p_Subm_Lha_Code", Sub_Lha);
            cStmt.setDate("p_Date_Entry", Date_Entry);
            cStmt.setString("p_Recv_By_At_Hq", Recv_By_At_Hq);
            cStmt.setDate("p_Date_Recv", Date_Recv);
            cStmt.setString("p_Rep_Num_Local", Rep_Num_Local);
            cStmt.setString("p_Rep_Xref_Num", Rep_Xref_Num);
            cStmt.setString("p_Clin_Rep_Info", Clin_Rep_Info);
            cStmt.setString("p_Comp_Rep_Info", Comp_Rep_Info);
            cStmt.setString("p_Patient_Num", Patient_Num);
            cStmt.setString("p_Hosp_Nm", Hosp_Nm);
            cStmt.setString("p_Batch_Num", Batch_Num);
            cStmt.setFloat("p_Dose", Dose);
            cStmt.setDate("p_Treat_Start", Treat_Start);
            cStmt.setDate("p_Treat_Stop", Treat_Stop);
            cStmt.setString("p_Rechallenge", Rech_Flag);
            cStmt.setString("p_Authorization_No", Authorization_No);
            cStmt.setString("p_p_Initials", P_Initials);
            cStmt.setDate("p_Date_Of_Birth", Date_Of_Birth);
            cStmt.setInt("p_Age_Reported", Age_Reported);
            cStmt.setInt("p_Weight", Weight);
            cStmt.setInt("p_Height", Height);
            cStmt.setString("p_Pregnancy", Pregnancy);
            cStmt.setInt("p_Pregnancy_Week", Pregnancy_Week);
            cStmt.setDate("p_Outcome_Rep_Date", Outcome_Rep_Date);
            cStmt.setString("p_Autopsy", Autopsy);
            cStmt.setString("p_Autopsy_Result", Autopsy_Result);
            cStmt.setDate("p_Phy_Assessment_Date", Phys_Assessment_Date);
            cStmt.setDate("p_Co_Assessment_Date", Co_Assessment_Date);
            cStmt.setString("p_Phy_Assessment_Reason", Phys_Assessment_Reason);
            cStmt.setString("p_Co_Assessment_Reason", Co_Assessment_Reason);
            cStmt.setString("p_Contributing_Factors", Contributing_Factors);
            cStmt.setDate("p_Rechallenge_Date", Rechallenge_Date);
            cStmt.setString("p_Rechallenge_Dose", Rechallenge_Dose);
            cStmt.setDate("p_Distbn_Date", Distbn_Date);
            cStmt.setDate("p_Submission_Date", Submission_Date);
            cStmt.setString("p_Further_Commn", Further_Commn);
            cStmt.setInt("p_Is_Suspended", Is_Suspended);
            cStmt.registerOutParameter("p_Rep_Num", java.sql.Types.INTEGER);

            cStmt.setString("p_Short_History", Short_History);
            cStmt.setString("p_Verbatim", Verbatim);
            cStmt.setString("p_Short_Comment", Short_Comment);
            cStmt.setString("p_AE_Desc", AE_Desc);

            cStmt.execute();
            this.Rep_Num = cStmt.getInt("p_Rep_Num");
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            return this.Rep_Num;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_BODY_SYS_NUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_BODY_SYSTEM") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public int saveCloneCaseDetails(String cpnyName, String user, int Rep_Num)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Copy_Case(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Username", user);
            cStmt.registerOutParameter("p_Rep_Num_New", java.sql.Types.INTEGER);

            cStmt.execute();
            this.Rep_Num = cStmt.getInt("p_Rep_Num_New");
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            return this.Rep_Num;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_BODY_SYS_NUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_BODY_SYSTEM") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * The function saves the AE related description fields Verbatim and AE_Desc of the Case_Details table.
     *
     * @param cpnyName as <code><b>String</b></code>.
     * @param Username as <code><b>String</b></code>.
     * @param Rep_Num as <code><b>int</b></code>.
     * @param Verbatim as <code><b>String</b></code>.
     * @param AE_Desc as <code><b>String</b></code>.
     * 
     * @throws bl.sapher.general.ConstraintViolationException Raised when contraint violation occurs.
     * @throws bl.sapher.general.DBConnectionException Raised when the database connection fails.
     * @throws bl.sapher.general.GenericException Raised when any general error occurs.
     * @throws bl.sapher.general.RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     */
    public void saveCaseAeSummary(String cpnyName, String Username,
            int Rep_Num, String Verbatim, String AE_Desc)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside saveCaseAeSummary()");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Case_AE_Details(?,?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Verbatim", Verbatim);
            cStmt.setString("p_AE_Desc", AE_Desc);

            cStmt.execute();

            dbCon.getDbCon().commit();

            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_BODY_SYS_NUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_BODY_SYSTEM") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * The function saves the CM related description fields Short_History and Short_Comment of the Case_Details table.
     * 
     * @param cpnyName as <code><b>String</b></code>.
     * @param Username as <code><b>String</b></code>.
     * @param Rep_Num as <code><b>int</b></code>.
     * @param Short_History as <code><b>String</b></code>.
     * @param Short_Comment as <code><b>String</b></code>.
     * 
     * @throws bl.sapher.general.ConstraintViolationException Raised when contraint violation occurs.
     * @throws bl.sapher.general.DBConnectionException Raised when the database connection fails.
     * @throws bl.sapher.general.GenericException Raised when any general error occurs.
     * @throws bl.sapher.general.RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     */
    public void saveCaseCmSummary(String cpnyName, String Username,
            int Rep_Num, String Short_History, String Short_Comment)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside saveCaseCmSummary()");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Case_CM_Details(?,?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Short_History", Short_History);
            cStmt.setString("p_Short_Comment", Short_Comment);

            cStmt.execute();

            dbCon.getDbCon().commit();

            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_BODY_SYS_NUM") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_BODY_SYSTEM") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * Function to delete a particular case.
     * 
     * @param Username as <code><b>String</b></code>.
     * 
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     * @throws bl.sapher.general.ConstraintViolationException
     */
    public void deleteCaseDetails(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "CaseDetails.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Case_Details(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", this.Rep_Num);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            Rep_Num = -1;
            rType = null;
            trialNum = null;
            cntry = null;
            repStat = null;
            infSource = null;
            mfr = null;
            fmln = null;
            unit = null;
            dRgmn = null;
            dFreq = null;
            route = null;
            indication = null;
            sex = null;
            eOrig = null;
            aeOutcome = null;
            pdt = null;
            phyAssess = null;
            coAssess = null;
            rRes = null;
            sCompany = null;
            subLha = null;
            Date_Entry = null;
            Recv_By_At_Hq = "";
            Date_Recv = null;
            Rep_Num_Local = "";
            Rep_Xref_Num = "";
            Clin_Rep_Info = "";
            Comp_Rep_Info = "";
            Patient_Num = "";
            Hosp_Nm = "";
            Batch_Num = "";
            Dose = -1;
            Treat_Start = null;
            Treat_Stop = null;
            Rech_Flag = "";
            Authorization_No = "";
            P_Initials = "";
            Date_Of_Birth = null;
            Age_Reported = -1;
            Weight = -1;
            Height = -1;
            Pregnancy = "";
            Pregnancy_Week = -1;
            Short_History = "";
            Outcome_Rep_Date = null;
            Autopsy = "";
            Autopsy_Result = "";
            Phys_Assessment_Date = null;
            Co_Assessment_Date = null;
            Phys_Assessment_Reason = "";
            Co_Assessment_Reason = "";
            Contributing_Factors = "";
            Rechallenge_Date = null;
            Rechallenge_Dose = "";
            Verbatim = "";
            Short_Comment = "";
            Distbn_Date = null;
            Submission_Date = null;
            Further_Commn = "";
            AE_Desc = "";
            Is_Suspended = -1;
            Duration = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_CASEDETAILS") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CaseDetails.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public int getRep_Num() {
        return Rep_Num;
    }

    public void setRep_Num(int Rep_Num) {
        this.Rep_Num = Rep_Num;
    }

    public String getRtype() {
        return rType;
    }

    public void setRtype(String rType) {
        this.rType = rType;
    }

    public String getReportTypeDesc() {
        return strReportTypeDesc;
    }

    public void getReportTypeDesc(String rTypeDesc) {
        this.strReportTypeDesc = rTypeDesc;
    }

    public String getTrialnum() {
        return trialNum;
    }

    public void setTrialnum(String trialNum) {
        this.trialNum = trialNum;
    }

    public String getCntry() {
        return cntry;
    }

    public void setCntry(String cntry) {
        this.cntry = cntry;
    }

    public String getRepstat() {
        return repStat;
    }

    public String getRepstatDesc() {
        return repStatDesc;
    }

    public void setRepstat(String repStat) {
        this.repStat = repStat;
    }

    public String getSrccode() {
        return infSource;
    }

    public void setSrccode(String infSource) {
        this.infSource = infSource;
    }

    public String getMcode() {
        return mfr;
    }

    public void setMcode(String mfr) {
        this.mfr = mfr;
    }

    public String getFormcode() {
        return fmln;
    }

    public void setFormcode(String fmln) {
        this.fmln = fmln;
    }

    public String getUnitcode() {
        return unit;
    }

    public void setUnitcode(String unit) {
        this.unit = unit;
    }

    public String getUnitcodeDesc() {
        return Unit_Desc;
    }

    public void setUnitcodeDesc(String unitDesc) {
        this.Unit_Desc = unitDesc;
    }

    public String getDregimen() {
        return dRgmn;
    }

    public void setDregimen(String dRgmn) {
        this.dRgmn = dRgmn;
    }

    public String getDfreq() {
        return dFreq;
    }

    public void setDfreq(String dFreq) {
        this.dFreq = dFreq;
    }

    public String getRoutecode() {
        return route;
    }

    public void setRoutecode(String route) {
        this.route = route;
    }

    public String getIndnum() {
        return indication;
    }

    public void setIndnum(String indication) {
        this.indication = indication;
    }

    public String getSexcode() {
        return sex;
    }

    public void setSexcode(String sex) {
        this.sex = sex;
    }

    public String getEtnicorig() {
        return eOrig;
    }

    public void setEtnicorig(String eOrig) {
        this.eOrig = eOrig;
    }

    public String getOutcomecode() {
        return aeOutcome;
    }

    public void setOutcomecode(String aeOutcome) {
        this.aeOutcome = aeOutcome;
    }

    public String getProdcode() {
        return pdt;
    }

    public void setProdcode(String pdt) {
        this.pdt = pdt;
    }

    public String getPassesscode() {
        return phyAssess;
    }

    public void setPassesscode(String phyAssess) {
        this.phyAssess = phyAssess;
    }

    public String getCoassesscode() {
        return coAssess;
    }

    public void setCoassesscode(String coAssess) {
        this.coAssess = coAssess;
    }

    public String getRescode() {
        return rRes;
    }

    public void setRescode(String rRes) {
        this.rRes = rRes;
    }

    public String getDcompany() {
        return sCompany;
    }

    public void setDcompany(String sCompany) {
        this.sCompany = sCompany;
    }

    public String getSublha() {
        return subLha;
    }

    public void setSublha(String subLha) {
        this.subLha = subLha;
    }

    public Date getDate_Entry() {
        return Date_Entry;
    }

    public void setDate_Entry(Date Date_Entry) {
        this.Date_Entry = Date_Entry;
    }

    public String getRecv_By_At_Hq() {
        return Recv_By_At_Hq;
    }

    public void setRecv_By_At_Hq(String Recv_By_At_Hq) {
        this.Recv_By_At_Hq = Recv_By_At_Hq;
    }

    public Date getDate_Recv() {
        return Date_Recv;
    }

    public void setDate_Recv(Date Date_Recv) {
        this.Date_Recv = Date_Recv;
    }

    public String getRep_Num_Local() {
        return Rep_Num_Local;
    }

    public void setRep_Num_Local(String Rep_Num_Local) {
        this.Rep_Num_Local = Rep_Num_Local;
    }

    public String getRep_Xref_Num() {
        return Rep_Xref_Num;
    }

    public void setRep_Xref_Num(String Rep_Xref_Num) {
        this.Rep_Xref_Num = Rep_Xref_Num;
    }

    public String getClin_Rep_Info() {
        return Clin_Rep_Info;
    }

    public void setClin_Rep_Info(String Clin_Rep_Info) {
        this.Clin_Rep_Info = Clin_Rep_Info;
    }

    public String getComp_Rep_Info() {
        return Comp_Rep_Info;
    }

    public void setComp_Rep_Info(String Comp_Rep_Info) {
        this.Comp_Rep_Info = Comp_Rep_Info;
    }

    public String getPatient_Num() {
        return Patient_Num;
    }

    public void setPatient_Num(String Patient_Num) {
        this.Patient_Num = Patient_Num;
    }

    public String getHosp_Nm() {
        return Hosp_Nm;
    }

    public void setHosp_Nm(String Hosp_Nm) {
        this.Hosp_Nm = Hosp_Nm;
    }

    public String getBatch_Num() {
        return Batch_Num;
    }

    public void setBatch_Num(String Batch_Num) {
        this.Batch_Num = Batch_Num;
    }

    public float getDose() {
        return Dose;
    }

    public void setDose(float Dose) {
        this.Dose = Dose;
    }

    public Date getTreat_Start() {
        return Treat_Start;
    }

    public void setTreat_Start(Date Treat_Start) {
        this.Treat_Start = Treat_Start;
    }

    public Date getTreat_Stop() {
        return Treat_Stop;
    }

    public void setTreat_Stop(Date Treat_Stop) {
        this.Treat_Stop = Treat_Stop;
    }

    public String getRechallenge() {
        return Rech_Flag;
    }

    public void setRechallenge(String Rech_Flag) {
        this.Rech_Flag = Rech_Flag;
    }

    public String getAuthorization_No() {
        return Authorization_No;
    }

    public void setAuthorization_No(String Authorization_No) {
        this.Authorization_No = Authorization_No;
    }

    public String getP_Initials() {
        return P_Initials;
    }

    public void setP_Initials(String P_Initials) {
        this.P_Initials = P_Initials;
    }

    public Date getDate_Of_Birth() {
        return Date_Of_Birth;
    }

    public void setDate_Of_Birth(Date Date_Of_Birth) {
        this.Date_Of_Birth = Date_Of_Birth;
    }

    public int getAge_Reported() {
        return Age_Reported;
    }

    public void setAge_Reported(int Age_Reported) {
        this.Age_Reported = Age_Reported;
    }

    public int getWeight() {
        return Weight;
    }

    public void setWeight(int Weight) {
        this.Weight = Weight;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int Height) {
        this.Height = Height;
    }

    public String getPregnancy() {
        return Pregnancy;
    }

    public void setPregnancy(String Pregnancy) {
        this.Pregnancy = Pregnancy;
    }

    public int getPregnancy_Week() {
        return Pregnancy_Week;
    }

    public void setPregnancy_Week(int Pregnancy_Week) {
        this.Pregnancy_Week = Pregnancy_Week;
    }

    public String getShort_History() {
        return Short_History;
    }

    public void setShort_History(String Short_History) {
        this.Short_History = Short_History;
    }

    public Date getOutcome_Rep_Date() {
        return Outcome_Rep_Date;
    }

    public void setOutcome_Rep_Date(Date Outcome_Rep_Date) {
        this.Outcome_Rep_Date = Outcome_Rep_Date;
    }

    public String getAutopsy() {
        return Autopsy;
    }

    public void setAutopsy(String Autopsy) {
        this.Autopsy = Autopsy;
    }

    public String getAutopsy_Result() {
        return Autopsy_Result;
    }

    public void setAutopsy_Result(String Autopsy_Result) {
        this.Autopsy_Result = Autopsy_Result;
    }

    public Date getPhys_Assessment_Date() {
        return Phys_Assessment_Date;
    }

    public void setPhys_Assessment_Date(Date Phys_Assessment_Date) {
        this.Phys_Assessment_Date = Phys_Assessment_Date;
    }

    public Date getCo_Assessment_Date() {
        return Co_Assessment_Date;
    }

    public void setCo_Assessment_Date(Date Co_Assessment_Date) {
        this.Co_Assessment_Date = Co_Assessment_Date;
    }

    public String getPhys_Assessment_Reason() {
        return Phys_Assessment_Reason;
    }

    public void setPhys_Assessment_Reason(String Phys_Assessment_Reason) {
        this.Phys_Assessment_Reason = Phys_Assessment_Reason;
    }

    public String getCo_Assessment_Reason() {
        return Co_Assessment_Reason;
    }

    public void setCo_Assessment_Reason(String Co_Assessment_Reason) {
        this.Co_Assessment_Reason = Co_Assessment_Reason;
    }

    public String getContributing_Factors() {
        return Contributing_Factors;
    }

    public void setContributing_Factors(String Contributing_Factors) {
        this.Contributing_Factors = Contributing_Factors;
    }

    public Date getRechallenge_Date() {
        return Rechallenge_Date;
    }

    public void setRechallenge_Date(Date Rechallenge_Date) {
        this.Rechallenge_Date = Rechallenge_Date;
    }

    public String getRechallenge_Dose() {
        return Rechallenge_Dose;
    }

    public void setRechallenge_Dose(String Rechallenge_Dose) {
        this.Rechallenge_Dose = Rechallenge_Dose;
    }

    public String getVerbatim() {
        return Verbatim;
    }

    public void setVerbatim(String Verbatim) {
        this.Verbatim = Verbatim;
    }

    public String getShort_Comment() {
        return Short_Comment;
    }

    public void setShort_Comment(String Short_Comment) {
        this.Short_Comment = Short_Comment;
    }

    public Date getDistbn_Date() {
        return Distbn_Date;
    }

    public void setDistbn_Date(Date Distbn_Date) {
        this.Distbn_Date = Distbn_Date;
    }

    public Date getSubmission_Date() {
        return Submission_Date;
    }

    public void setSubmission_Date(Date Submission_Date) {
        this.Submission_Date = Submission_Date;
    }

    public String getFurther_Commn() {
        return Further_Commn;
    }

    public void setFurther_Commn(String Further_Commn) {
        this.Further_Commn = Further_Commn;
    }

    public int getIs_Suspended() {
        return Is_Suspended;
    }

    public void setIs_Suspended(int Is_Suspended) {
        this.Is_Suspended = Is_Suspended;
    }

    public String getAE_Desc() {
        return AE_Desc;
    }

    public void setAE_Desc(String AE_Desc) {
        this.AE_Desc = AE_Desc;
    }

    /**
     * Function returns type description of the Case.
     *
     * @return Type Descrition as <code><b>String</b></code>.
     */
    public String getType_Desc() {
        return this.Type_Desc;
    }

    /**
     * Function sets type description of the Case.
     *
     * @param Type_Desc as <code><b>String</b></code>.
     */
    public void setType_Desc(String Type_Desc) {
        this.Type_Desc = Type_Desc;
    }

    /**
     * Function returns country name of the Case.
     *
     * @return country name as <code><b>String</b></code>.
     */
    public String getCountry_Nm() {
        return this.Country_Nm;
    }

    /**
     * Function sets country name of the Case.
     *
     * @param Country_Nm as <code><b>String</b></code>.
     */
    public void setCountry_Nm(String Country_Nm) {
        this.Country_Nm = Country_Nm;
    }

    /**
     * Function returns status description of the Case.
     *
     * @return status description as <code><b>String</b></code>.
     */
    public String getStatus_Desc() {
        return this.Status_Desc;
    }

    /**
     * Function sets status description of the Case.
     *
     * @param Status_Desc as <code><b>String</b></code>.
     */
    public void setStatus_Desc(String Status_Desc) {
        this.Status_Desc = Status_Desc;
    }

    /**
     * Function returns source description of the Case.
     *
     * @return source Descrition as <code><b>String</b></code>.
     */
    public String getSource_Desc() {
        return this.Source_Desc;
    }

    /**
     * Function sets source description of the Case.
     *
     * @param Source_Desc as <code><b>String</b></code>.
     */
    public void setSource_Desc(String Source_Desc) {
        this.Source_Code = Source_Desc;
    }

    /**
     * Function returns manufacturer name of the Case.
     *
     * @return manufacturer name as <code><b>String</b></code>.
     */
    public String getM_Name() {
        return this.M_Name;
    }

    /**
     * Function sets manufacturer name of the Case.
     *
     * @param M_Name as <code><b>String</b></code>.
     */
    public void setM_Name(String M_Name) {
        this.M_Name = M_Name;
    }

    /**
     * Function returns formulation description of the Case.
     *
     * @return formulation Descrition as <code><b>String</b></code>.
     */
    public String getFormulation_Desc() {
        return this.Formulation_Desc;
    }

    /**
     * Function sets formulation description of the Case.
     *
     * @param Formulation_Desc as <code><b>String</b></code>.
     */
    public void setFormulation_Desc(String Formulation_Desc) {
        this.Formulation_Desc = Formulation_Desc;
    }

    /**
     * Function returns unit description of the Case.
     *
     * @return String unit Descrition
     */
    public String getUnit_Desc() {
        return this.Unit_Desc;
    }

    /**
     * Function sets unit description of the Case.
     *
     * @param Unit_Desc as <code><b>String</b></code>.
     */
    public void setUnit_Desc(String Unit_Desc) {
        this.Unit_Desc = Unit_Desc;
    }

    /**
     * Function returns dose regimen description of the Case.
     *
     * @return dose regimen Descrition as <code><b>String</b></code>.
     */
    public String getDregimen_Desc() {
        return this.Dregimen_Desc;
    }

    /**
     * Function sets dose regimen description of the Case.
     *
     * @param Dregimen_Desc as <code><b>String</b></code>.
     */
    public void setDregimen_Desc(String Dregimen_Desc) {
        this.Dregimen_Desc = Dregimen_Desc;
    }

    /**
     * Function returns dose frequency of the Case.
     *
     * @return dose frequency Descrition as <code><b>String</b></code>.
     */
    public String getDfreq_Desc() {
        return this.Dfreq_Desc;
    }

    /**
     * Function sets dose frequency of the Case.
     *
     * @param Dfreq_Desc as <code><b>String</b></code>.
     */
    public void setDfreq_Desc(String Dfreq_Desc) {
        this.Dfreq_Desc = Dfreq_Desc;
    }

    /**
     * Function returns route description of the Case.
     *
     * @return route Descrition as <code><b>String</b></code>.
     */
    public String getRoute_Desc() {
        return this.Route_Desc;
    }

    /**
     * Function sets route description of the Case.
     *
     * @param Route_Desc as <code><b>String</b></code>.
     */
    public void setRoute_Desc(String Route_Desc) {
        this.Route_Desc = Route_Desc;
    }

    /**
     * Function returns indication of the Case.
     *
     * @return indication as <code><b>String</b></code>.
     */
    public String getIndication() {
        return this.Indication;
    }

    /**
     * Function sets indication of the Case.
     *
     * @param Indication as <code><b>String</b></code>.
     */
    public void setIndication(String Indication) {
        this.Indication = Indication;
    }

    /**
     * Function returns sex description of the Case.
     *
     * @return sex Descrition as <code><b>String</b></code>.
     */
    public String getSex_Desc() {
        return this.Sex_Desc;
    }

    /**
     * Function sets sex description of the Case.
     *
     * @param Sex_Desc as <code><b>String</b></code>.
     */
    public void setSex_DescString(String Sex_Desc) {
        this.Sex_Desc = Sex_Desc;
    }

    /**
     * Function returns ethnic description of the Case.
     *
     * @return ethnic Descrition as <code><b>String</b></code>.
     */
    public String getEthnic_Desc() {
        return this.Ethnic_Desc;
    }

    /**
     * Function sets ethnic description of the Case.
     *
     * @param Ethnic_Desc as <code><b>String</b></code>.
     */
    public void setEthnic_Desc(String Ethnic_Desc) {
        this.Ethnic_Desc = Ethnic_Desc;
    }

    /**
     * Function returns outcome of the Case.
     *
     * @return outcome as <code><b>String</b></code>.
     */
    public String getOutcome() {
        return this.Outcome;
    }

    /**
     * Function sets outcome of the Case.
     *
     * @param Outcome as <code><b>String</b></code>.
     */
    public void setOutcome(String Outcome) {
        this.Outcome = Outcome;
    }

    /**
     * Function returns generic name of the Case.
     *
     * @return generic name as <code><b>String</b></code>.
     */
    public String getGeneric_Nm() {
        return this.Generic_Nm;
    }

    /**
     * Function sets generic name of the Case.
     *
     * @param Generic_Nm as <code><b>String</b></code>.
     */
    public void setGeneric_Nm(String Generic_Nm) {
        this.Generic_Nm = Generic_Nm;
    }

    /**
     * Function returns brand name of the Case.
     *
     * @return brand name as <code><b>String</b></code>.
     */
    public String getBrand_Nm() {
        return this.Brand_Nm;
    }

    /**
     * Function sets brand name of the Case.
     *
     * @param Brand_Nm as <code><b>String</b></code>.
     */
    public void setBrand_Nm(String Brand_Nm) {
        this.Brand_Nm = Brand_Nm;
    }

    /**
     * Function returns physical assessment of the Case.
     *
     * @return physical assessment as <code><b>String</b></code>.
     */
    public String getPhy_Assessment() {
        return this.Phy_Assessment;
    }

    /**
     * Function returns physical assessment code of the Case.
     *
     * @return physical assessment as <code><b>String</b></code>.
     */
    public String getPhy_Assessment_Code() {
        return this.Phy_AssessmentCode;
    }

    /**
     * Function sets physical assessment of the Case.
     *
     * @param Phy_Assessment as <code><b>String</b></code>.
     */
    public void setPhy_Assessment(String Phy_Assessment) {
        this.Phy_Assessment = Phy_Assessment;
    }

    /**
     * Function sets physical assessment code of the Case.
     *
     * @param Phy_AssessmentCod as <code><b>String</b></code>.
     */
    public void setPhy_Assessment_Code(String Phy_AssessmentCod) {
        this.Phy_AssessmentCode = Phy_AssessmentCod;
    }

    /**
     * Function returns physical assessment flag of the Case.
     *
     * @return physical assessment flag as <code><b>String</b></code>.
     */
    public String getPhy_Assess_Flg() {
        return this.Phy_Assess_Flg;
    }

    /**
     * Function sets physical assessment flag of the Case.
     *
     * @param Phy_Assess_Flg as <code><b>String</b></code>.
     */
    public void setPhy_Assess_Flg(String Phy_Assess_Flg) {
        this.Phy_Assess_Flg = Phy_Assess_Flg;
    }

    /**
     * Function returns company assessment of the Case.
     *
     * @return company assessment as <code><b>String</b></code>.
     */
    public String getCo_Assessment() {
        return this.Co_Assessment;
    }

    /**
     * Function returns company assessment code of the Case.
     *
     * @return company assessment as <code><b>String</b></code>.
     */
    public String getCo_Assessment_Code() {
        return this.Co_AssessmentCode;
    }

    /**
     * Function sets company assessment of the Case.
     *
     * @param Co_Assessment as <code><b>String</b></code>.
     */
    public void setCo_Assessment(String Co_Assessment) {
        this.Co_Assessment = Co_Assessment;
    }

    /**
     * Function sets company assessment of the Case.
     *
     * @param Co_AssessmentCod as <code><b>String</b></code>.
     */
    public void setCo_Assessment_Code(String Co_AssessmentCod) {
        this.Co_AssessmentCode = Co_AssessmentCod;
    }

    /**
     * Function returns company assessment flag of the Case.
     *
     * @return company assessment flag as <code><b>String</b></code>.
     */
    public String getCo_Assess_Flg() {
        return this.Co_Assess_Flg;
    }

    /**
     * Function sets company assessment flag of the Case.
     *
     * @param Co_Assess_Flg as <code><b>String</b></code>.
     */
    public void setCo_Assess_Flg(String Co_Assess_Flg) {
        this.Co_Assess_Flg = Co_Assess_Flg;
    }

    /**
     * Function returns result description of the Case.
     *
     * @return result Descrition as <code><b>String</b></code>.
     */
    public String getResult_Desc() {
        return this.Result_Desc;
    }

    /**
     * Function sets result description of the Case.
     *
     * @param Result_Desc as <code><b>String</b></code>.
     */
    public void setResult_Desc(String Result_Desc) {
        this.Result_Desc = Result_Desc;
    }

    /**
     * Function returns LHA description of the Case.
     *
     * @return String LHA Descrition
     */
    public String getLha_Desc() {
        return this.Lha_Desc;
    }

    /**
     * Function sets LHA description of the Case.
     *
     * @param Lha_Desc as <code><b>String</b></code>.
     */
    public void setLha_Desc(String Lha_Desc) {
        this.Lha_Desc = Lha_Desc;
    }

    /**
     * Function returns duration of the Case.
     *
     * @return Duration as <code><b>int</b></code>.
     */
    public int getDuration() {
        return this.Duration;
    }

    /**
     * Function sets duration of the Case.
     *
     * @param Duration as <code><b>int</b></code>.
     */
    public void setDuration(int Duration) {
        this.Duration = Duration;
    }

    public int getDosageInterval() {
        return this.nDosageInterval;
    }

    public int getNoOfDosage() {
        return this.nNoOfDosage;
    }

    public String getIntervalDesc() {
        return this.strIntervalDesc;
    }

    /**
     * finalize method for CaseDetails class.
     * @throws java.lang.Throwable 
     */
    @Override
    protected void finalize() throws Throwable {
        this.AE_Desc = null;
        this.Authorization_No = null;
        this.Autopsy = null;
        this.Autopsy_Result = null;
        this.Batch_Num = null;
        this.Clin_Rep_Info = null;
        this.Co_Assessment_Date = null;
        this.Co_Assessment_Reason = null;
        this.Comp_Rep_Info = null;
        this.Contributing_Factors = null;
        this.Date_Entry = null;
        this.Date_Of_Birth = null;
        this.Date_Recv = null;
        this.Distbn_Date = null;
        this.Further_Commn = null;
        this.Hosp_Nm = null;
        this.Outcome_Rep_Date = null;
        this.P_Initials = null;
        this.Patient_Num = null;
        this.Phys_Assessment_Date = null;
        this.Phys_Assessment_Reason = null;
        this.Pregnancy = null;
        this.Rechallenge_Date = null;
        this.Rechallenge_Dose = null;
        this.Recv_By_At_Hq = null;
        this.Rep_Num_Local = null;
        this.Rep_Xref_Num = null;
        this.Short_Comment = null;
        this.Short_History = null;
        this.Submission_Date = null;
        this.Treat_Start = null;
        this.Treat_Stop = null;
        this.Verbatim = null;
        this.cntry = null;
        this.coAssess = null;
        this.sCompany = null;
        this.dFreq = null;
        this.dRgmn = null;
        this.eOrig = null;
        this.fmln = null;
        this.indication = null;
        this.mfr = null;
        this.aeOutcome = null;
        this.phyAssess = null;
        this.pdt = null;
        this.Rech_Flag = null;
        this.repStat = null;
        this.rRes = null;
        this.route = null;
        this.rType = null;
        this.sex = null;
        this.infSource = null;
        this.subLha = null;
        this.trialNum = null;
        this.unit = null;

        this.Type_Desc = null;
        this.Country_Nm = null;
        this.Status_Desc = null;
        this.Source_Desc = null;
        this.M_Name = null;
        this.Formulation_Desc = null;
        this.Unit_Desc = null;
        this.Dregimen_Desc = null;
        this.Dfreq_Desc = null;
        this.Route_Desc = null;
        this.Indication = null;
        this.Sex_Desc = null;
        this.Ethnic_Desc = null;
        this.Outcome = null;
        this.Generic_Nm = null;
        this.Brand_Nm = null;
        this.Phy_Assessment = null;
        this.Phy_Assess_Flg = null;
        this.Co_Assessment = null;
        this.Co_Assess_Flg = null;
        this.Result_Desc = null;
        this.Lha_Desc = null;
    }
}
