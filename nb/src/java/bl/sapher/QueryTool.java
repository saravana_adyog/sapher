/*
 * QueryTool.java
 * Created on February 26, 2008
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

import bl.sapher.general.ReportEngine;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class QueryTool implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = 9017465151270477575L;
    private int Rep_Num;
    private String BrandNm;
    private String Llt_Code;
    private String Llt_Desc;
    private String Diag_Code;
    private String Diag_Desc;
    private String Country_Nm;
    private String Sex_Code;
    private int Age;
    private int Treat_Dur;
    private String Phy_Ass_Code;
    private String Co_Ass_Code;
    private String Outcome;
    private static String Log_File_Name;

    public QueryTool(String logFilename) {
        this.Rep_Num = -1;
        this.BrandNm = "";
        this.Llt_Code = "";
        this.Llt_Desc = "";
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Country_Nm = "";
        this.Sex_Code = "";
        this.Age = -1;
        this.Treat_Dur = -1;
        this.Phy_Ass_Code = "";
        this.Co_Ass_Code = "";
        this.Outcome = "";
        QueryTool.Log_File_Name = logFilename;
    }

    public QueryTool(int Rep_Num, String BrandNm, String Llt_Code,
            String Llt_Desc, String Diag_Code, String Diag_Desc,
            String Country_Nm, String Sex_Code, int Age, int Treat_Dur,
            String Phy_Ass_Code, String Co_Ass_Code, String Outcome) {
        this.Rep_Num = Rep_Num;
        this.BrandNm = BrandNm;
        this.Llt_Code = Llt_Code;
        this.Llt_Desc = Llt_Desc;
        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Country_Nm = Country_Nm;
        this.Sex_Code = Sex_Code;
        this.Age = Age;
        this.Treat_Dur = Treat_Dur;
        this.Phy_Ass_Code = Phy_Ass_Code;
        this.Co_Ass_Code = Co_Ass_Code;
        this.Outcome = Outcome;
    }

    public static ArrayList listQueryTool(String cpnyName, String Prod_Code, String Status_Code, String Country_Code,
            String Source_Code, String Clasf_Code, String Unit_Code, float Dose, String Ind_Num, String LLT_Code,
            String Outcome_Code, String Diag_Code, String Co_Ass_Code, String Body_Sys_Num, String Sex_Code,
            String Phy_Ass_Code, String M_Code, String Patient_Num, int Frm_Rep_Num, int To_Rep_Num,
            int From_Age, int To_Age, int From_Treat, int To_Treat, int From_Event, int To_Event,
            String From_Date_Recv, String To_Date_Recv, int Is_Suspended)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "QueryTool.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<QueryTool> lstQueryTool = new ArrayList<QueryTool>();
        QueryTool qt = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{ call sapher_pkg.Query_Tool_Proc(?,?,?," +
                    "?,?,?,?,?,?," +
                    "?,?,?,?,?," +
                    "?,?,?,?,?," +
                    "?,?,?,?,?,?," +
                    "?,?,?,?)}");
            cStmt.setString("p_Product_code", Prod_Code);
            cStmt.setString("p_Status_Code", Status_Code);
            cStmt.setString("p_Country_Code", Country_Code);
            cStmt.setString("p_Source_Code", Source_Code);
            cStmt.setString("p_Classification_Code", Clasf_Code);
            cStmt.setString("p_Unit_Code", Unit_Code);
            cStmt.setFloat("p_Dose", Dose);
            cStmt.setString("p_Indication_Num", Ind_Num);
            cStmt.setString("p_Llt_Code", LLT_Code);
            cStmt.setString("p_Outcome_Code", Outcome_Code);
            cStmt.setString("p_Diag_Code", Diag_Code);
            cStmt.setString("p_Co_Assessment_Code", Co_Ass_Code);
            cStmt.setString("p_Body_Sys_Num", Body_Sys_Num);
            cStmt.setString("p_Sex_Code", Sex_Code);
            cStmt.setString("p_Phy_Assessment_Code", Phy_Ass_Code);
            cStmt.setString("p_Patient_Num", Patient_Num);
            cStmt.setString("p_M_Code", M_Code);
            cStmt.setInt("p_Frm_Rep_num", Frm_Rep_Num);
            cStmt.setInt("p_To_Rep_Num", To_Rep_Num);
            cStmt.setInt("p_Frm_Age", From_Age);
            cStmt.setInt("p_To_Age", To_Age);
            cStmt.setInt("p_Frm_Treat", From_Treat);
            cStmt.setInt("p_To_Treat", To_Treat);
            cStmt.setInt("p_Frm_Event", From_Event);
            cStmt.setInt("p_To_Event", To_Event);
            cStmt.setString("p_Frm_Date_Recv", From_Date_Recv);
            cStmt.setString("p_to_Date_Recv", To_Date_Recv);
            cStmt.setInt("p_Status", Is_Suspended);
            cStmt.registerOutParameter("p_CurQTool", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsQueryTool = (ResultSet) cStmt.getObject("p_CurQTool");
            while (rsQueryTool.next()) {
                qt = new QueryTool(rsQueryTool.getInt("Rep_Num"), rsQueryTool.getString("Brand_Nm"),
                        rsQueryTool.getString("Llt_Code"), rsQueryTool.getString("Llt_Desc"),
                        rsQueryTool.getString("Diag_Code"), rsQueryTool.getString("Diag_Desc"),
                        rsQueryTool.getString("Country_Nm"), rsQueryTool.getString("Sex_Code"),
                        rsQueryTool.getInt("Age"), rsQueryTool.getInt("Duration"),
                        rsQueryTool.getString("Phy_Assessment_Code"), rsQueryTool.getString("Co_Assessment_Code"),
                        rsQueryTool.getString("Outcome"));
                lstQueryTool.add(qt);
            }
            rsQueryTool.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "QueryTool.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "QueryTool.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("QueryTool records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "QueryTool.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "QueryTool.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstQueryTool;
    }

    public static ResultSet listQueryTool4Rep(String cpnyName, String Prod_Code, String Status_Code, String Country_Code,
            String Source_Code, String Clasf_Code, String Unit_Code, float Dose, String Ind_Num, String LLT_Code,
            String Outcome_Code, String Diag_Code, String Co_Ass_Code, String Body_Sys_Num, String Sex_Code,
            String Phy_Ass_Code, String M_Code, String Patient_Num, int Frm_Rep_Num, int To_Rep_Num,
            int From_Age, int To_Age, int From_Treat, int To_Treat, int From_Event, int To_Event,
            String From_Date_Recv, String To_Date_Recv, int Is_Suspended)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "QueryTool.java; Inside listing function (PDF)");
        DBCon dbCon = new DBCon(cpnyName);
        ResultSet rsQueryTool = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{ call sapher_pkg.Query_Tool_Proc(?,?,?," +
                    "?,?,?,?,?,?," +
                    "?,?,?,?,?," +
                    "?,?,?,?,?," +
                    "?,?,?,?,?,?," +
                    "?,?,?,?)}");
            cStmt.setString("p_Product_code", Prod_Code);
            cStmt.setString("p_Status_Code", Status_Code);
            cStmt.setString("p_Country_Code", Country_Code);
            cStmt.setString("p_Source_Code", Source_Code);
            cStmt.setString("p_Classification_Code", Clasf_Code);
            cStmt.setString("p_Unit_Code", Unit_Code);
            cStmt.setFloat("p_Dose", Dose);
            cStmt.setString("p_Indication_Num", Ind_Num);
            cStmt.setString("p_Llt_Code", LLT_Code);
            cStmt.setString("p_Outcome_Code", Outcome_Code);
            cStmt.setString("p_Diag_Code", Diag_Code);
            cStmt.setString("p_Co_Assessment_Code", Co_Ass_Code);
            cStmt.setString("p_Body_Sys_Num", Body_Sys_Num);
            cStmt.setString("p_Sex_Code", Sex_Code);
            cStmt.setString("p_Phy_Assessment_Code", Phy_Ass_Code);
            cStmt.setString("p_Patient_Num", Patient_Num);
            cStmt.setString("p_M_Code", M_Code);
            cStmt.setInt("p_Frm_Rep_num", Frm_Rep_Num);
            cStmt.setInt("p_To_Rep_Num", To_Rep_Num);
            cStmt.setInt("p_Frm_Age", From_Age);
            cStmt.setInt("p_To_Age", To_Age);
            cStmt.setInt("p_Frm_Treat", From_Treat);
            cStmt.setInt("p_To_Treat", To_Treat);
            cStmt.setInt("p_Frm_Event", From_Event);
            cStmt.setInt("p_To_Event", To_Event);
            cStmt.setString("p_Frm_Date_Recv", From_Date_Recv);
            cStmt.setString("p_to_Date_Recv", To_Date_Recv);
            cStmt.setInt("p_Status", Is_Suspended);
            cStmt.registerOutParameter("p_CurQTool", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            rsQueryTool = (ResultSet) cStmt.getObject("p_CurQTool");
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "QueryTool.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "QueryTool.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("QueryTool records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "QueryTool.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "QueryTool.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsQueryTool;
    }

    public static String printPdf(String cpnyName, String path, ResultSet rs)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Reports_Listing.java; Inside printPdf()");
        String strPdfFileName = null;

        DBCon dbCon = null;
        try {
            dbCon = new DBCon(cpnyName);

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("SAPHER_PARAM_APP_VER", (new GenConf().getSysVersion()));
            strPdfFileName = ReportEngine.exportAsPdf(path, "repQT.jrxml", rs, param);

        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Reports_Listing.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Reports_Listing.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        } finally {
            try {
                rs.close();
                dbCon.closeCon();
            } catch (SQLException ex) {
                Logger.writeLog(Log_File_Name, "Reports_Listing.java; Caught SQLException-" + ex);
            }
        }
        return strPdfFileName;
    }

    public String getRep_Num() {
        return String.valueOf(Rep_Num).substring(2);
    }

    public String getBrandNm() {
        return BrandNm;
    }

    public String getLlt_Code() {
        return Llt_Code;
    }

    public String getLlt_Desc() {
        return Llt_Desc;
    }

    public String getDiag_Code() {
        return Diag_Code;
    }

    public String getDiag_Desc() {
        return Diag_Desc;
    }

    public String getCountry_Nm() {
        return Country_Nm;
    }

    public String getSex_Code() {
        return Sex_Code;
    }

    public int getAge() {
        return Age;
    }

    public int getTreat_Dur() {
        return Treat_Dur;
    }

    public String getPhy_Ass_Code() {
        return Phy_Ass_Code;
    }

    public String getCo_Ass_Code() {
        return Co_Ass_Code;
    }

    public String getOutcome() {
        return Outcome;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Rep_Num = 0;
        this.BrandNm = null;
        this.Llt_Code = null;
        this.Llt_Desc = null;
        this.Diag_Code = null;
        this.Diag_Desc = null;
        this.Country_Nm = null;
        this.Sex_Code = null;
        this.Age = 0;
        this.Treat_Dur = 0;
        this.Phy_Ass_Code = null;
        this.Co_Ass_Code = null;
        this.Outcome = null;
    }
}
