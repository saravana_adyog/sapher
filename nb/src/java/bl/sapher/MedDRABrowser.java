/**
 * MedDRABrowser.java
 * Created on 09-10-2009
 * @author Anoop Varma, Focal3 LLC.
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.SimultaneousOpException;
import java.io.File;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

/**
 *
 * @author Anoop Varma, Focal3 LLC.
 */
public class MedDRABrowser implements Serializable {

    private static MedDRABrowser me = null;
    private static boolean isMedDraUpdateRunning = false;
    /** SOC (System Organ Class) Type Identifier */
    public static final int MEDDRA_ITEMTYPE_SOC = 1;
    /** HLGT (High Level Group Term) Type Identifier */
    public static final int MEDDRA_ITEMTYPE_HLGT = 2;
    /** HLT (High Level Term) Type Identifier */
    public static final int MEDDRA_ITEMTYPE_HLT = 3;
    /** PT (Preferred Term) Type Identifier */
    public static final int MEDDRA_ITEMTYPE_PT = 4;
    /** LLT (Low Level Term) Type Identifier */
    public static final int MEDDRA_ITEMTYPE_LLT = 5;
    /** Serialization version UID */
    private static final long serialVersionUID = 8127625608171666879L;
    /** Log file name */
    private static String Log_File_Name;

    public static MedDRABrowser getInstance(String logFilename) {
        if (me == null) {
            me = new MedDRABrowser(logFilename);
        }
        return me;
    }

    public MedDRABrowser(String logFilename) {
        MedDRABrowser.Log_File_Name = logFilename;
    }

    public static ArrayList<String> list(int SearchType, String cpnyName,
            int level, String code, String soc)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<String> lstMedDra = new ArrayList<String>();
        String strMedDraResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Get_Meddra_Listing(?,?,?,?)}");
            cStmt.setInt("p_level_no", level);
            cStmt.setString("p_code", code);
            cStmt.setString("p_soc", soc);
            cStmt.registerOutParameter("cur_MListing", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsMedDra = (ResultSet) cStmt.getObject("cur_MListing");
            while (rsMedDra.next()) {
                switch (level) {
                    case 1:
                        strMedDraResult = rsMedDra.getString("Body_Sys_Num") + "|" + rsMedDra.getString("Body_System");
                        lstMedDra.add(strMedDraResult);
                        break;
                    case 2:
                        strMedDraResult = rsMedDra.getString("Hlgt_Code") + "|" + rsMedDra.getString("Hlgt_Desc");
                        lstMedDra.add(strMedDraResult);
                        break;
                    case 3:
                        strMedDraResult = rsMedDra.getString("Hlt_Code") + "|" + rsMedDra.getString("Hlt_Desc");
                        lstMedDra.add(strMedDraResult);
                        break;
                    case 4:
                        strMedDraResult = rsMedDra.getString("Diag_Code") + "|" + rsMedDra.getString("Diag_Desc") + "|" + rsMedDra.getString("PT");
                        lstMedDra.add(strMedDraResult);
                        break;
                    case 5:
                        strMedDraResult = rsMedDra.getString("Llt_Code") + "|" + rsMedDra.getString("Llt_Desc") + "|" + rsMedDra.getString("Is_Valid");
                        lstMedDra.add(strMedDraResult);
                        break;
                }
            }

            rsMedDra.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException"); throw new GenericException(ex.getMessage());
        }
        return lstMedDra;
    }

    public static ArrayList<String> search(int SearchType, String cpnyName,
            String strLevel, String desc)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Inside search()");
        DBCon dbCon = new DBCon(cpnyName);

        int level = 0;
        ArrayList<String> lstMedDra = new ArrayList<String>();
        String strMedDraResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Meddra_Search_List(?,?,?)}");
            cStmt.setString("p_lvl_no", strLevel);
            cStmt.setString("p_desc", desc);
            cStmt.registerOutParameter("cur_listing", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsMedDra = (ResultSet) cStmt.getObject("cur_listing");
            while (rsMedDra.next()) {
                level = rsMedDra.getInt("LVL_NO");
                strMedDraResult = rsMedDra.getString("LVL_NO") + "|" +
                        rsMedDra.getString("SOC_CODE") + "|" +
                        rsMedDra.getString("SOC_DESC") + "|" +
                        rsMedDra.getString("HLGT_CODE") + "|" +
                        rsMedDra.getString("HLGT_DESC") + "|" +
                        rsMedDra.getString("HLT_CODE") + "|" +
                        rsMedDra.getString("HLT_DESC") + "|" +
                        rsMedDra.getString("PT_CODE") + "|" +
                        rsMedDra.getString("PT_DESC") + "|" +
                        rsMedDra.getString("LLT_CODE") + "|" +
                        rsMedDra.getString("LLT_DESC") + "|" +
                        rsMedDra.getString("CODE") + "|" +
                        rsMedDra.getString("DSC");
                lstMedDra.add(strMedDraResult);
            }

            rsMedDra.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException"); throw new GenericException(ex.getMessage());
        }
        return lstMedDra;
    }

    private void cleanupFolder(String folderName) {
        File folder = new File(folderName);
        File[] files = folder.listFiles();

        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }
    }

    public void uploadMedDraFiles(HttpServletRequest hsr, String path) {
        try {
            String FILE_REPOSITORY_PATH = path;
            final int FILE_SIZE_THRESHOLD = 40960;
            final long FILE_SIZE_MAX = 10485760; // 10 MB
            String strFileName = "";

            cleanupFolder(FILE_REPOSITORY_PATH);

            DiskFileItemFactory dfiFactory = new DiskFileItemFactory();
            dfiFactory.setSizeThreshold(FILE_SIZE_THRESHOLD); /* the unit is bytes */
            File repositoryPath = new File(FILE_REPOSITORY_PATH);
            dfiFactory.setRepository(repositoryPath);
            ServletFileUpload servletFileUpload = new ServletFileUpload(dfiFactory);
            servletFileUpload.setSizeMax(FILE_SIZE_MAX); /* the unit is bytes */
            List fileItemsList = servletFileUpload.parseRequest(hsr);

            Iterator it = fileItemsList.iterator();
            while (it.hasNext()) {
                FileItem fileItem = (FileItem) it.next();
                if (!fileItem.isFormField()) {
                    /* The file item contains an uploaded file */
                    strFileName = fileItem.getName();
                    if (!(strFileName.contains("hlgt.asc") ||
                            strFileName.contains("hlgt_hlt.asc") ||
                            strFileName.contains("hlt.asc") ||
                            strFileName.contains("hlt_pt.asc") ||
                            strFileName.contains("llt.asc") ||
                            strFileName.contains("pt.asc") ||
                            strFileName.contains("soc.asc") ||
                            strFileName.contains("soc_hlgt.asc"))) {
                        throw new Exception("Invalid File. Please select proper set of files");
                    }
                    System.out.println("AV> uploaded file=" + strFileName);
                    File uploadedFile = new File(FILE_REPOSITORY_PATH, strFileName);
                    fileItem.write(uploadedFile);
                }
            }
        } catch (FileUploadException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                e.printStackTrace();
            }
        }
    }

    public void updateDB(String uname, String pwd, String client)
            throws DBConnectionException, GenericException, SimultaneousOpException {
        if (isMedDraUpdateRunning) {
            throw new SimultaneousOpException("Other instance of MedDRA update in progress.");
        } else {
            isMedDraUpdateRunning = true;

            DBCon dbCon = new DBCon(uname, pwd);
            GenConf genConf = new GenConf(client);

            try {
                CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                        "{call " + genConf.getAppUser().toUpperCase() + ".MEDDRA_PKG.MedDRA_Update()}");

                cStmt.execute();
                cStmt.close();
                dbCon.closeCon();
            } catch (SQLException ex) {
                Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
                if (bl.sapher.general.GenConf.isDebugMode()) {
                    ex.printStackTrace();
                }
                throw new GenericException("SQL Error with MedDRA update.");
            } catch (Exception ex) {
                Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
                if (bl.sapher.general.GenConf.isDebugMode()) {
                    ex.printStackTrace();
                }
                Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            } finally {
                isMedDraUpdateRunning = false;
            }
        }
    }

    public static String getSocDetails(String cpnyName, String code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);

        String strSocResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Get_Meddra_Soc(?,?)}");
            cStmt.setString("p_soc_code", code);
            cStmt.registerOutParameter("cur_soc", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSoc = (ResultSet) cStmt.getObject("cur_soc");
            while (rsSoc.next()) {
                strSocResult = rsSoc.getString("BODY_SYS_NUM") + "|" +
                        rsSoc.getString("BODY_SYSTEM") + "|" +
                        rsSoc.getString("IS_VALID") + "|" +
                        rsSoc.getString("WHO_ART_CODE") + "|" +
                        rsSoc.getString("HARTS_CODE") + "|" +
                        rsSoc.getString("COSTART_SYM") + "|" +
                        rsSoc.getString("ICD9_CODE") + "|" +
                        rsSoc.getString("ICD9_CM_CODE") + "|" +
                        rsSoc.getString("ICD10_CODE") + "|" +
                        rsSoc.getString("JART_CODE");
            }

            rsSoc.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
            //}
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strSocResult;
    }

    public static String getHlgtDetails(String cpnyName, String code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);

        String strHlgResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Get_Meddra_Hlgt(?,?)}");
            cStmt.setString("p_hlgt_code", code);
            cStmt.registerOutParameter("cur_hlgt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSoc = (ResultSet) cStmt.getObject("cur_hlgt");
            while (rsSoc.next()) {
                strHlgResult = rsSoc.getString("HLGT_CODE") + "|" +
                        rsSoc.getString("HLGT_DESC") + "|" +
                        rsSoc.getString("WHO_ART_CODE") + "|" +
                        rsSoc.getString("HARTS_CODE") + "|" +
                        rsSoc.getString("COSTART_SYM") + "|" +
                        rsSoc.getString("ICD9_CODE") + "|" +
                        rsSoc.getString("ICD9_CM_CODE") + "|" +
                        rsSoc.getString("ICD10_CODE") + "|" +
                        rsSoc.getString("JART_CODE");
            }

            rsSoc.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
            //}
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strHlgResult;
    }

    public static String getHltDetails(String cpnyName, String code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);

        String strHltResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Get_Meddra_Hlt(?,?)}");
            cStmt.setString("p_hlt_code", code);
            cStmt.registerOutParameter("cur_hlt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSoc = (ResultSet) cStmt.getObject("cur_hlt");
            while (rsSoc.next()) {
                strHltResult = rsSoc.getString("HLT_CODE") + "|" +
                        rsSoc.getString("HLT_DESC") + "|" +
                        rsSoc.getString("WHO_ART_CODE") + "|" +
                        rsSoc.getString("HARTS_CODE") + "|" +
                        rsSoc.getString("COSTART_SYM") + "|" +
                        rsSoc.getString("ICD9_CODE") + "|" +
                        rsSoc.getString("ICD9_CM_CODE") + "|" +
                        rsSoc.getString("ICD10_CODE") + "|" +
                        rsSoc.getString("JART_CODE");
            }

            rsSoc.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
            //}
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strHltResult;
    }

    public static String getPtDetails(String cpnyName, String code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);

        String strPtResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Get_Meddra_Pt(?,?)}");
            cStmt.setString("p_pt_code", code);
            cStmt.registerOutParameter("cur_pt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSoc = (ResultSet) cStmt.getObject("cur_pt");
            while (rsSoc.next()) {
                strPtResult = rsSoc.getString("DIAG_CODE") + "|" +
                        rsSoc.getString("DIAG_DESC") + "|" +
                        rsSoc.getString("IS_VALID") + "|" +
                        rsSoc.getString("PRIMARY_SOC") + "|" +
                        rsSoc.getString("WHO_ART_CODE") + "|" +
                        rsSoc.getString("HARTS_CODE") + "|" +
                        rsSoc.getString("COSTART_SYM") + "|" +
                        rsSoc.getString("ICD9_CODE") + "|" +
                        rsSoc.getString("ICD9_CM_CODE") + "|" +
                        rsSoc.getString("ICD10_CODE") + "|" +
                        rsSoc.getString("JART_CODE");
            }

            rsSoc.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
            //}
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPtResult;
    }

    public static String getLltDetails(String cpnyName, String code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);

        String strLltResult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Meddra_pkg.Get_Meddra_Llt(?,?)}");
            cStmt.setString("p_llt_code", code);
            cStmt.registerOutParameter("cur_llt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSoc = (ResultSet) cStmt.getObject("cur_llt");
            while (rsSoc.next()) {
                strLltResult = rsSoc.getString("LLT_CODE") + "|" +
                        rsSoc.getString("LLT_DESC") + "|" +
                        rsSoc.getString("IS_VALID") + "|" +
                        rsSoc.getString("DIAG_CODE") + "|" +
                        rsSoc.getString("WHO_ART_CODE") + "|" +
                        rsSoc.getString("HARTS_CODE") + "|" +
                        rsSoc.getString("COSTART_SYM") + "|" +
                        rsSoc.getString("ICD9_CODE") + "|" +
                        rsSoc.getString("ICD9_CM_CODE") + "|" +
                        rsSoc.getString("ICD10_CODE") + "|" +
                        rsSoc.getString("JART_CODE");
            }

            rsSoc.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught SQLException-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
        //}
        //Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing RecordNotFoundException"); throw new RecordNotFoundException("MedDRA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
            //}
            Logger.writeLog(Log_File_Name, "MedDRABrowser.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strLltResult;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
    }
}

