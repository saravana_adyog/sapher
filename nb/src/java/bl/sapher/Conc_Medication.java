/*
 * Conc_Medication.java
 * Created on December 01, 2007
 * @author Jaikishan. S, Anoop Varma.
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

/**
 * Class that handles the manipulation of the Concomitant Medication of a case.
 *
 * @author Jaikishan S, Anoop Varma.
 */
public class Conc_Medication implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -4634310843128067047L;
    private int Rep_Num;
    //private Product pdt;
    //private IndicationTreatment iTreatment;
    private Date Treat_Start;
    private Date Treat_Stop;
    // Newly added fields, 16/01/2008, Wed.
    private String Product_Code;
    private String Generic_Nm;
    private String Brand_Nm;
    private String Indication_Num;
    private String Indication;
    private int Duration;
    private String strSaveFlag;
    /** Log file name */
    private static String Log_File_Name;

    public Conc_Medication() {
        this.Rep_Num = -1;
//        this.pdt = null;
//        this.iTreatment = null;
        this.Treat_Start = null;
        this.Treat_Stop = null;

        this.Product_Code = null;
        this.Generic_Nm = null;
        this.Brand_Nm = null;
        this.Indication_Num = null;
        this.Indication = null;
        this.Duration = -1;
    }

    public Conc_Medication(String logFilename) {
        this.Rep_Num = -1;
        this.Treat_Start = null;
        this.Treat_Stop = null;

        this.Product_Code = null;
        this.Generic_Nm = null;
        this.Brand_Nm = null;
        this.Indication_Num = null;
        this.Indication = null;
        this.Duration = -1;
        Conc_Medication.Log_File_Name = logFilename;
    }

    /** @deprecated */
    public Conc_Medication(int Rep_Num, Product pdt, IndicationTreatment ind,
            Date Treat_Start, Date Treat_Stop)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        this.Rep_Num = Rep_Num;
//        this.pdt = pdt;
//        this.iTreatment = ind;
        this.Treat_Start = Treat_Start;
        this.Treat_Stop = Treat_Stop;
    }

    /**
     * Adverse_Event Constructor.
     *
     * @param Rep_Num Report number as <code><b>int</b></code>.
     * @param Product_Code Product code as <code><b>String</b></code>.
     * @param Generic_Nm Generic name as <code><b>String</b></code>.
     * @param Brand_Nm Brand name as <code><b>String</b></code>.
     * @param Indication_Num Indication number as <code><b>String</b></code>.
     * @param Indication Indication description as <code><b>String</b></code>.
     * @param Treat_Start Treatment start date as <code><b>Date</b></code>.
     * @param Treat_Stop Treatment stop date as <code><b>Date</b></code>.
     * @param Duration duration as <code><b>int</b></code>.
     *
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public Conc_Medication(int Rep_Num, String Product_Code, String Generic_Nm, String Brand_Nm,
            String Indication_Num, String Indication, Date Treat_Start, Date Treat_Stop,
            int Duration)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        this.Rep_Num = Rep_Num;
        this.Treat_Start = Treat_Start;
        this.Treat_Stop = Treat_Stop;
        // Newly added params.
        this.Product_Code = Product_Code;
        this.Generic_Nm = Generic_Nm;
        this.Brand_Nm = Brand_Nm;
        this.Indication_Num = Indication_Num;
        this.Indication = Indication;
        this.Duration = Duration;
    }

    /**
     * The new constructor that fetches data from a database view.
     *
     * @param From_View A dummy parameter that is supposed to be 'y' always.
     *                  This is just because, single parameter constructor is already in use.
     *                  This param MUST be removed.
     * @param Rep_Num Report number as <code><b>int</b></code>.
     * @param Product_Code Product code as <code><b>String</b></code>.
     */
    public Conc_Medication(String cpnyName, final String From_View, int Rep_Num, String Product_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_CM(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Prod_Code", Product_Code);
            cStmt.registerOutParameter("cur_Case_CM", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.executeQuery();
            ResultSet rsCM = (ResultSet) cStmt.getObject("cur_Case_CM");
            rsCM.next();

            this.Rep_Num = Rep_Num;
            this.Product_Code = rsCM.getString("PRODUCT_CODE");
            this.Generic_Nm = rsCM.getString("GENERIC_NM");
            this.Brand_Nm = rsCM.getString("BRAND_NM");
            this.Indication_Num = rsCM.getString("INDICATION_NUM");
            this.Indication = rsCM.getString("INDICATION");
            this.Treat_Start = rsCM.getDate("TREAT_START");
            this.Treat_Stop = rsCM.getDate("TREAT_STOP");
            this.Duration = rsCM.getInt("CONC_DURATION");

            rsCM.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Concomitant Medication not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * This static function is used to list all Concom Medications of a Case, from a database view.
     *
     * @param Rep_Num Report number as <code><b>int</b></code>.
     *
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public static ArrayList<Conc_Medication> listConc_MedicationFromView(String cpnyName, int Rep_Num)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Conc_Medication.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<Conc_Medication> lstConc_Medication = new ArrayList<Conc_Medication>();
        Conc_Medication concmedication = null;

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_CM(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Prod_Code", "");
            cStmt.registerOutParameter("cur_Case_CM", oracle.jdbc.OracleTypes.CURSOR);

            if (Rep_Num != -1) {
                cStmt.setInt(1, Rep_Num);
            }
            cStmt.executeQuery();
            ResultSet rsCM = (ResultSet) cStmt.getObject("cur_Case_CM");

            while (rsCM.next()) {
                concmedication = new Conc_Medication(
                        rsCM.getInt(1/*"REP_NUM"*/),
                        rsCM.getString(2/*"PRODUCT_CODE"*/),
                        rsCM.getString(3/*"GENERIC_NM"*/),
                        rsCM.getString(4/*"BRAND_NM"*/),
                        rsCM.getString(5/*"INDICATION_NUM"*/),
                        rsCM.getString(6/*"INDICATION"*/),
                        rsCM.getDate(7/*"TREAT_START"*/),
                        rsCM.getDate(8/*"TREAT_STOP"*/),
                        rsCM.getInt(9/*"CON_DURATION"*/));
                lstConc_Medication.add(concmedication);
            }
            rsCM.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Concomitant Medication not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstConc_Medication;
    }

    /**
     * Same as <code><b>listConc_MedicationFromView</b></code> and the only difference is the return value.
     *
     * @param cpnyName
     * @param Rep_Num
     * @return
     * @throws bl.sapher.general.RecordNotFoundException
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     */
    public static ResultSet listConc_Medication(DBCon dbCon, String cpnyName, int Rep_Num)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Conc_Medication.java; Inside listing function returning ResultSet");
        //DBCon dbCon = new DBCon(cpnyName);
        ResultSet rsCM = null;

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_CM(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Prod_Code", "");
            cStmt.registerOutParameter("cur_Case_CM", oracle.jdbc.OracleTypes.CURSOR);

            if (Rep_Num != -1) {
                cStmt.setInt(1, Rep_Num);
            }
            cStmt.executeQuery();
            rsCM = (ResultSet) cStmt.getObject("cur_Case_CM");

        //dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Concomitant Medication not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsCM;
    }

    /**
     *
     */
    public void saveConc_Medication(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Conc_Medication.java; Inside save function");

        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Conc_Medications(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", this.strSaveFlag);
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", this.Rep_Num);
            cStmt.setString("p_Product_Code", this.Product_Code);
            cStmt.setString("p_Indication_Num", this.Indication_Num);
            cStmt.setDate("p_Treat_Start", this.Treat_Start);
            cStmt.setDate("p_Treat_Stop", this.Treat_Stop);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Rep_Num = (strSaveFlag.equalsIgnoreCase("U") ? this.Rep_Num : Rep_Num);
//            this.pdt = (Save_Flag.equalsIgnoreCase("U")?this.pdt:new Product(Prod_Code));
//            this.iTreatment = (Ind_Num != null?new IndicationTreatment(Ind_Num):this.iTreatment);

            this.Treat_Start = (Treat_Start != null ? Treat_Start : this.Treat_Start);
            this.Treat_Stop = (Treat_Stop != null ? Treat_Stop : this.Treat_Stop);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_CONC_MEDICATIONS") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void saveConc_Medication(String cpnyName,
            String Save_Flag, String Username, int Rep_Num, String Prod_Code,
            String Ind_Num, Date Treat_Start, Date Treat_Stop)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Conc_Medication.java; Inside save function with all params");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Conc_Medications(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Rep_Num : Rep_Num));
            cStmt.setString("p_Product_Code", Prod_Code);
            cStmt.setString("p_Indication_Num", Ind_Num);
            cStmt.setDate("p_Treat_Start", Treat_Start);
            cStmt.setDate("p_Treat_Stop", Treat_Stop);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Rep_Num = (Save_Flag.equalsIgnoreCase("U") ? this.Rep_Num : Rep_Num);
//            this.pdt = (Save_Flag.equalsIgnoreCase("U")?this.pdt:new Product(Prod_Code));
//            this.iTreatment = (Ind_Num != null?new IndicationTreatment(Ind_Num):this.iTreatment);

            this.Treat_Start = (Treat_Start != null ? Treat_Start : this.Treat_Start);
            this.Treat_Stop = (Treat_Stop != null ? Treat_Stop : this.Treat_Stop);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_CONC_MEDICATIONS") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteConc_Medication(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Conc_Medication.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Conc_Medications(?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", this.Rep_Num);
            cStmt.setString("p_Product_Code", this.Product_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();

            this.Rep_Num = -1;
            this.Product_Code = null;
            this.Generic_Nm = null;
            this.Brand_Nm = null;
            this.Indication_Num = null;
            this.Indication = null;
            this.Treat_Start = null;
            this.Treat_Stop = null;
            this.Duration = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("1");
            } else {
                Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Conc_Medication.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public int getRep_Num() {
        return Rep_Num;
    }

    /** */
    public void setRep_Num(int repNum) {
        this.Rep_Num = repNum;
    }

//    public Product getProduct() {
//        return pdt;
//    }
//    public IndicationTreatment getIndicationTreatment() {
//        return iTreatment;
//    }
    /**
     * Function returns product code of the Concomitant Medication.
     *
     * @return String Product code.
     */
    public String getProduct_Code() {
        return this.Product_Code;
    }

    /**
     * Function sets product code of the Concomitant Medication.
     *
     * @param String Product code.
     */
    public void setProduct_Code(String Product_Code) {
        this.Product_Code = Product_Code;
    }

    /**
     * Function returns generic name of the Concomitant Medication.
     *
     * @return String Generic name.
     */
    public String getGeneric_Nm() {
        return this.Generic_Nm;
    }

    /**
     * Function sets generic name of the Concomitant Medication.
     *
     * @param String Generic name.
     */
    public void setGeneric_Nm(String Generic_Nm) {
        this.Generic_Nm = Generic_Nm;
    }

    /**
     * Function returns brand name of the Concomitant Medication.
     *
     * @return String Brand name.
     */
    public String getBrand_Nm() {
        return this.Brand_Nm;
    }

    /**
     * Function sets brand name of the Concomitant Medication.
     *
     * @param String Brand name.
     */
    public void setBrand_Nm(String Brand_Nm) {
        this.Brand_Nm = Brand_Nm;
    }

    /**
     * Function returns indication number of the Concomitant Medication.
     *
     * @return String Indication number.
     */
    public String getIndication_Num() {
        return this.Indication_Num;
    }

    /**
     * Function sets indication number of the Concomitant Medication.
     *
     * @param String Indication number.
     */
    public void setIndication_Num(String Indication_Num) {
        this.Indication_Num = Indication_Num;
    }

    /**
     * Function returns indication of the Concomitant Medication.
     *
     * @return String Indication.
     */
    public String getIndication() {
        return this.Indication;
    }

    /**
     * Function sets indication of the Concomitant Medication.
     *
     * @param String Indication.
     */
    public void setIndication(String Indication) {
        this.Indication = Indication;
    }

    /**
     * Function returns duration of the Concomitant Medication.
     *
     * @return int Duration.
     */
    public int getDuration() {
        return this.Duration;
    }

    /**
     * Function sets duration of the Concomitant Medication.
     *
     * @param Duration 
     */
    public void setDuration(int Duration) {
        this.Duration = Duration;
    }

    public Date getTreat_Start() {
        return Treat_Start;
    }

    /** */
    public void setTreat_Start(Date Treat_Start) {
        this.Treat_Start = Treat_Start;
    }

    public Date getTreat_Stop() {
        return Treat_Stop;
    }

    /**
     * @param Treat_Stop 
     */
    public void setTreat_Stop(Date Treat_Stop) {
        this.Treat_Stop = Treat_Stop;
    }

    /**
     * @return String save flag.
     */
    public String setSaveFlag() {
        return this.strSaveFlag;
    }

    /**
     * @param saveFlag 
     */
    public void setSaveFlag(String saveFlag) {
        this.strSaveFlag = saveFlag;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Product_Code = null;
        this.Generic_Nm = null;
        this.Brand_Nm = null;
        this.Indication_Num = null;
        this.Indication = null;
        this.Treat_Start = null;
        this.Treat_Stop = null;

        this.strSaveFlag = null;
    }
}
