/*
 * CPanel.java
 * Created on March 18, 2008
 * @author Arun P. Jose, Anoop Varma
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.SimultaneousOpException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * CPanel [Singleton Class]
 */
public class CPanel {

    private static CPanel me = null;
    private boolean bRunning = false;
    /** Log file name */
    private static String Log_File_Name;

    protected CPanel(String logFilename) {
        CPanel.Log_File_Name = logFilename;
    }

    /**
     * To defeat instantiation.
     * @return
     */
    public static CPanel getInstance(String logFilename) {
        if (me == null) {
            me = new CPanel(logFilename);
        }
        return me;
    }

    public /*static*/ void doRestore(String uname, String pwd, String fileName, String fromUser, String toUser)
            throws DBConnectionException, GenericException, SimultaneousOpException {
        DBCon dbCon = new DBCon(uname, pwd);
        if (!bRunning) {
            bRunning = true;
            try {
                CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                        "{call sapadmin_pkg.Imp_User(?,?,?)}");
                cStmt.setString("P_Dmp_File", fileName);
                cStmt.setString("P_FromUser", fromUser);
                cStmt.setString("P_ToUser", toUser.toUpperCase());

                cStmt.execute();
                cStmt.close();
                dbCon.closeCon();
            } catch (Exception ex) {
                Logger.writeLog(Log_File_Name, "CPanel.java; Caught Exception-" + ex);
                //if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
                //}
                Logger.writeLog(Log_File_Name, "CPanel.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            } finally {
                bRunning = false;
            }
        } else {
            throw new SimultaneousOpException("Backup/Restore already running");
        }
    }

    public /*static*/ void doBackup(String uname, String pwd, String company)
            throws GenericException, DBConnectionException, SimultaneousOpException {
        DBCon dbCon = new DBCon(uname, pwd);
        GenConf gC = new GenConf(company);

        if (!bRunning) {
            bRunning = true;
            try {
                CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                        "{call sapadmin_pkg.Exp_User(?)}");
                cStmt.setString("p_Username", gC.getAppUser().toUpperCase());
                cStmt.execute();
                cStmt.close();
                dbCon.closeCon();
            } catch (Exception ex) {
                Logger.writeLog(Log_File_Name, "CPanel.java; Caught Exception-" + ex);
                if (bl.sapher.general.GenConf.isDebugMode()) {
                    ex.printStackTrace();
                }
                Logger.writeLog(Log_File_Name, "CPanel.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            } finally {
                bRunning = false;
            }
        } else {
            throw new SimultaneousOpException("Backup/Restore already running");
        }
    }

    public static void updateMedDraFiles(String Client, String Path) {
    }

    public static ArrayList lstFiles(String uname, String pwd, String type, String from, String to)
            throws GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "CPanel.java; Inside listing function");
        DBCon dbCon = new DBCon(uname, pwd);
        ArrayList<String> lstBkp = new ArrayList<String>();
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapadmin_pkg.Get_Dir_List(?,?,?,?)}");
            cStmt.setString("p_Type", type);
            cStmt.setString("p_From", from);
            cStmt.setString("p_To", to);
            cStmt.registerOutParameter("p_DirList", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsBkp = (ResultSet) cStmt.getObject("p_DirList");
            while (rsBkp.next()) {
                lstBkp.add(rsBkp.getString("Content"));
            }
            rsBkp.close();
            dbCon.closeCon();
            return lstBkp;
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CPanel.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CPanel.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList readLogFile(String uname, String pwd, String fileName)
            throws GenericException, DBConnectionException {
        DBCon dbCon = new DBCon(uname, pwd);
        int ctr = 0;
        ArrayList<String> lstContent = new ArrayList<String>();
        String bkp = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapadmin_pkg.Read_Log_File(?,?)}");
            cStmt.setString("p_FileName", fileName);
            cStmt.registerOutParameter("p_FileContent", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsContent = (ResultSet) cStmt.getObject("p_FileContent");
            while (rsContent.next()) {
                lstContent.add(rsContent.getString("Content"));
            }
            rsContent.close();
            dbCon.closeCon();
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CPanel.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CPanel.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstContent;
    }

    public static void ClearDB(String uname, String pwd, String client)
            throws GenericException, DBConnectionException {
        DBCon dbCon = new DBCon(uname, pwd);
        GenConf gC = new GenConf(client);

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call " + gC.getAppUser().toUpperCase() + ".SAPHER_PKG.Clear_Db()}");
            cStmt.execute();
            cStmt.close();
            dbCon.closeCon();
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CPanel.java; Caught Exception-" + ex);
            //if (bl.sapher.general.GenConf.isDebugMode()) {
            ex.printStackTrace();
            //}
            Logger.writeLog(Log_File_Name, "CPanel.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static void DelCases(String uname, String pwd, String client)
            throws GenericException, DBConnectionException {
        DBCon dbCon = new DBCon(uname, pwd);
        GenConf gC = new GenConf(client);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call " + gC.getAppUser().toUpperCase() + ".SAPHER_PKG.Del_Cases()}");
            cStmt.execute();
            cStmt.close();
            dbCon.closeCon();
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "CPanel.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "CPanel.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }
}
