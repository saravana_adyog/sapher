/*
 * LHA.java
 * Created on November 26, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class LHA implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 8444498825402386837L;
    private String Lha_Code;
    private Country ctry;
    private String Lha_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public LHA() {
        this.Lha_Code = "";
        this.ctry = null;
        this.Lha_Desc = "";
        this.Is_Valid = -1;
    }

    public LHA(String logFilename) {
        this.Lha_Code = "";
        this.ctry = null;
        this.Lha_Desc = "";
        this.Is_Valid = -1;
        LHA.Log_File_Name = logFilename;
    }

    public LHA(String Lha_Code, Country ctry, String Lha_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        this.Lha_Code = Lha_Code;
        this.ctry = ctry;
        this.Lha_Desc = Lha_Desc;
        this.Is_Valid = Is_Valid;
    }

    public LHA(String cpnyName, String Lha_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_LHA(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Lha_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setString("p_Ctry", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_LHA", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsLHA = (ResultSet) cStmt.getObject("cur_LHA");
            rsLHA.next();
            this.Lha_Code = Lha_Code;
            this.ctry = new Country(rsLHA.getString("c_Country_Code"), rsLHA.getString("c_Country_Nm"),
                    rsLHA.getInt("c_Is_Valid"));
            this.Lha_Desc = rsLHA.getString("l_LHA_Desc");
            this.Is_Valid = rsLHA.getInt("l_Is_Valid");
            rsLHA.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("LHA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<LHA> listLHA(int SearchType, String cpnyName, String Lha_Code, String Country_Nm, String Lha_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "LHA.java; Inside listing function");

        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<LHA> lstLHA = new ArrayList<LHA>();
        LHA lha = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_LHA(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Lha_Code);
            cStmt.setString("p_Desc", Lha_Desc);
            cStmt.setString("p_Ctry", Country_Nm);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_LHA", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsLHA = (ResultSet) cStmt.getObject("cur_LHA");
            while (rsLHA.next()) {

                lha = new LHA(rsLHA.getString("l_LHA_Code"), new Country(rsLHA.getString("c_Country_Code"),
                        rsLHA.getString("c_Country_Nm"), rsLHA.getInt("c_Is_Valid")),
                        rsLHA.getString("l_LHA_Desc"),
                        rsLHA.getInt("l_Is_Valid"));
                lstLHA.add(lha);
            }
            rsLHA.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("LHA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstLHA;
    }

    public void saveLHA(String cpnyName, String Save_Flag, String Username, String Lha_Code,
            String Ctry_Code, String Lha_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "LHA.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_LHA(?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_LHA_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Lha_Code : Lha_Code));
            cStmt.setString("p_LHA_Desc", Lha_Desc);
            cStmt.setString("p_Country_Code", Ctry_Code);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Lha_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Lha_Code : Lha_Code);
            this.ctry = (!Ctry_Code.equals("") ? new Country(cpnyName, Ctry_Code) : this.ctry);
            this.Lha_Desc = (Lha_Desc != "" ? Lha_Desc : this.Lha_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_LHA") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_LHA") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_LHA_COUNTRY") != -1) {
                throw new ConstraintViolationException("3");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("4");
            } else {
                Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteLHA(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "LHA.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_LHA(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_LHA_Code", this.Lha_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Lha_Code = "";
            this.Lha_Desc = "";
            this.ctry = null;
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            ex.printStackTrace();
            if (ex.getMessage().indexOf("FK_CASEDETAILS_LHA") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AE_LHA") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, String countryName, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "LHA.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_LHA(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setString("p_Ctry", countryName);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_LHA", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_LHA");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repLha.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("LHA not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "LHA.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "LHA.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getLha_Code() {
        return Lha_Code;
    }

    public Country getCtry() {
        return ctry;
    }

    public String getLha_Desc() {
        return Lha_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Lha_Code = null;
        this.Lha_Desc = null;
        this.ctry = null;
    }
}
