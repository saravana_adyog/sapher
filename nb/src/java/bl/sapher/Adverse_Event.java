/*
 * Adverse_Event.java
 * Created on December 1, 2007
 * @author Arun P. Jose, Anoop Varma
 */
package bl.sapher;

import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class that handles the manipulation of an Adverse Event of a case.
 *
 * @author Arun P. Jose, Anoop Varma.
 */
public class Adverse_Event implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 8828639066137322837L;
    private int Rep_Num;
    private Date Date_Start;
    private Date Date_Stop;
    private Date Report_Date;
    private String Labelling;
    private String Prev_Report_Flg;
    private String Prev_Report;
    private String Event_Treatment;
    private String Event_Abate_On_Stop;
    private String Aggr_Of_Diag;
    private String Aggr_Of_Llt;
    // Newly added fields, 15/01/2008, Tue.
    private String Diag_Code;
    private String Diag_Desc;
    private String Llt_Code;
    private String Llt_Desc;
    private String Outcome_Code;
    private String Outcome;
    private String Body_Sys_Num;
    private String Body_System;
    private String Classification_Code;
    private String Classification_Desc;
    private String ESeverity_Code;
    private String ESeverity_Desc;
    private String Lha_Code;
    private String Country_Code;
    private String Lha_Desc;
    private int Duration;
    private String strSaveFlag;
    private String strReportedToLha;
    private static String Log_File_Name;

    /**
     * Default Constructor for <code><b>Adverse_Event</b></code> class.
     * 
     * This constructor initializes
     * <code><b>String</b></code> member variables with empty string,
     * <code><b>int</b></code> member variables -1 and
     * <code><b>Date</b></code> member variables with null.
     */
    public Adverse_Event() {
        Rep_Num = -1;
        Date_Start = null;
        Date_Stop = null;
        Report_Date = null;
        Labelling = "";
        Prev_Report_Flg = "";
        Prev_Report = "";
        Event_Treatment = "";
        Event_Abate_On_Stop = "";
        Aggr_Of_Diag = "";
        Aggr_Of_Llt = "";

        // Newly added fields, 15/01/2008, Tue
        Diag_Code = "";
        Diag_Desc = "";
        Llt_Code = "";
        Llt_Desc = "";
        Outcome_Code = "";
        Outcome = "";
        Body_Sys_Num = "";
        Body_System = "";
        Classification_Code = "";
        Classification_Desc = "";
        ESeverity_Code = "";
        ESeverity_Desc = "";
        Lha_Code = "";
        Country_Code = "";
        Lha_Desc = "";
        Duration = -1;

        strReportedToLha = "";
    }

    /**
     * Constructor for <code><b>Adverse_Event</b></code> class.
     * 
     * @param logFileName log File nath as <code><b>int</b></code>.
     *
     * This constructor initializes
     * The log file name with supplied value and
     * <code><b>String</b></code> member variables with empty string,
     * <code><b>int</b></code> member variables -1 and
     * <code><b>Date</b></code> member variables with null.
     */
    public Adverse_Event(String logFileName) {
        Rep_Num = -1;
        Date_Start = null;
        Date_Stop = null;
        Report_Date = null;
        Labelling = "";
        Prev_Report_Flg = "";
        Prev_Report = "";
        Event_Treatment = "";
        Event_Abate_On_Stop = "";
        Aggr_Of_Diag = "";
        Aggr_Of_Llt = "";

        // Newly added fields, 15/01/2008, Tue
        Diag_Code = "";
        Diag_Desc = "";
        Llt_Code = "";
        Llt_Desc = "";
        Outcome_Code = "";
        Outcome = "";
        Body_Sys_Num = "";
        Body_System = "";
        Classification_Code = "";
        Classification_Desc = "";
        ESeverity_Code = "";
        ESeverity_Desc = "";
        Lha_Code = "";
        Country_Code = "";
        Lha_Desc = "";
        Duration = -1;

        strReportedToLha = "";

        Adverse_Event.Log_File_Name = logFileName;
    }

    @Deprecated
    public Adverse_Event(int Rep_Num, AEDiag aeDiag, AELLT aeLLT, AEOutcome aeOutcome,
            Body_System bSys, Classification_Seriousness clsf, Event_Severity svty, LHA lha, Date Rep_date, Date Date_Start,
            Date Date_Stop, String Labelling, String Prev_Report_Flg, String Prev_Report,
            String Event_Treatment, String Event_Abate_On_Stop, String Aggr_Of_Diag,
            String Aggr_Of_Llt)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        this.Rep_Num = Rep_Num;
        this.Report_Date = Rep_date;
        this.Date_Start = Date_Start;
        this.Date_Stop = Date_Stop;
        this.Labelling = Labelling;
        this.Prev_Report_Flg = Prev_Report_Flg;
        this.Prev_Report = Prev_Report;
        this.Event_Treatment = Event_Treatment;
        this.Event_Abate_On_Stop = Event_Abate_On_Stop;
        this.Aggr_Of_Diag = Aggr_Of_Diag;
        this.Aggr_Of_Llt = Aggr_Of_Llt;
    }

    /**
     * Adverse_Event Constructor.
     *
     * @param Rep_Num Report number as <code><b>int</b></code>.
     * @param Diag_Code Diag code as <code><b>String</b></code>.
     * @param Diag_Desc Diag description as <code><b>String</b></code>.
     * @param Llt_Code LLT code as <code><b>String</b></code>.
     * @param Llt_Desc LLT description as <code><b>String</b></code>.
     * @param Outcome_Code Outcome code as <code><b>String</b></code>.
     * @param Outcome Outcome description as <code><b>String</b></code>.
     * @param Body_Sys_Num Body system number as <code><b>String</b></code>.
     * @param Body_System Body system description as <code><b>String</b></code>.
     * @param Classification_Code Classification code as <code><b>String</b></code>.
     * @param Classification_Desc Classification description as <code><b>String</b></code>.
     * @param Eseverity_Code Event severity code as <code><b>String</b></code>.
     * @param Eseverity_Desc Event severity description as <code><b>String</b></code>.
     * @param Reported_To_Lha Information whether it is reporter to LHA as <code><b>String</b></code>.
     * @param Lha_Code LHA code as <code><b>String</b></code>.
     * @param Country_Code Country code as <code><b>String</b></code>.
     * @param Lha_Desc LHA description as <code><b>String</b></code>.
     * @param Report_Date Report date as <code><b>Date</b></code>.
     * @param Date_Start Start date as <code><b>Date</b></code>.
     * @param Date_Stop Stop date as <code><b>Date</b></code>.
     * @param Labelling Labelling as <code><b>String</b></code>.
     * @param Prev_Report_Flg as <code><b>String</b></code>.
     * @param Prev_Report as <code><b>String</b></code>.
     * @param Event_Treatment as <code><b>String</b></code>.
     * @param Event_Abate_On_Stop as <code><b>String</b></code>.
     * @param Aggr_Of_Diag as <code><b>String</b></code>.
     * @param Aggr_Of_Llt as <code><b>String</b></code>.
     * @param Duration date duration as <code><b>int</b></code>.
     */
    public Adverse_Event(int Rep_Num, String Diag_Code, String Diag_Desc, String Llt_Code,
            String Llt_Desc, String Outcome_Code, String Outcome, String Body_Sys_Num,
            String Body_System, String Classification_Code, String Classification_Desc, String Eseverity_Code,
            String Eseverity_Desc, String Reported_To_Lha, String Lha_Code, String Country_Code, String Lha_Desc,
            Date Report_Date, Date Date_Start, Date Date_Stop, String Labelling,
            String Prev_Report_Flg, String Prev_Report, String Event_Treatment, String Event_Abate_On_Stop,
            String Aggr_Of_Diag, String Aggr_Of_Llt, int Duration) {
        this.Rep_Num = Rep_Num;
        this.Date_Start = Date_Start;
        this.Date_Stop = Date_Stop;
        this.Report_Date = Report_Date;
        this.Labelling = Labelling;
        this.Prev_Report_Flg = Prev_Report_Flg;
        this.Prev_Report = Prev_Report;
        this.Event_Treatment = Event_Treatment;
        this.Event_Abate_On_Stop = Event_Abate_On_Stop;
        this.Aggr_Of_Diag = Aggr_Of_Diag;
        this.Aggr_Of_Llt = Aggr_Of_Llt;

        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Llt_Code = Llt_Code;
        this.Llt_Desc = Llt_Desc;
        this.Outcome_Code = Outcome_Code;
        this.Outcome = Outcome;
        this.Body_Sys_Num = Body_Sys_Num;
        this.Body_System = Body_System;
        this.Classification_Code = Classification_Code;
        this.Classification_Desc = Classification_Desc;
        this.ESeverity_Code = Eseverity_Code;
        this.ESeverity_Desc = Eseverity_Desc;
        this.strReportedToLha = Reported_To_Lha;
        this.Lha_Code = Lha_Code;
        this.Country_Code = Country_Code;
        this.Lha_Desc = Lha_Desc;
        this.Duration = Duration;
    }

    /**
     * The new constructor that fetches data from a database view.
     *
     * @param cpnyName Tha company name so that the connection parameters to be loaded from the configuration XML file.
     * @param From_View A dummy parameter that is supposed to be 'y' always.
     *                  This is just because, single parameter constructor is already in use.
     *                  This param MUST be removed.
     * @param Rep_Num Report number as <code><b>int</b></code>.
     * @param Diag_Code Diag code as <code><b>String</b></code>.
     *
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public Adverse_Event(String cpnyName, final String From_View, int Rep_Num, String Llt_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_AE(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Llt_Code", Llt_Code);
            cStmt.registerOutParameter("cur_Case_AE", oracle.jdbc.OracleTypes.CURSOR);

            cStmt.executeQuery();
            ResultSet rsAE = (ResultSet) cStmt.getObject("cur_Case_AE");
            rsAE.next();

            this.Rep_Num = Rep_Num;
            this.Labelling = rsAE.getString("LABELLING");
            this.Prev_Report_Flg = rsAE.getString("PREV_REPORT_FLG");
            this.Prev_Report = rsAE.getString("PREV_REPORT");
            this.Event_Treatment = rsAE.getString("EVENT_TREATMENT");
            this.Event_Abate_On_Stop = rsAE.getString("EVENT_ABATE_ON_STOP");
            this.Aggr_Of_Diag = rsAE.getString("AGGR_OF_DIAG");
            this.Aggr_Of_Llt = rsAE.getString("AGGR_OF_LLT");
            // Newly added fields
            this.Diag_Code = rsAE.getString("DIAG_CODE");
            this.Diag_Desc = rsAE.getString("DIAG_DESC");
            this.Llt_Code = rsAE.getString("LLT_CODE");
            this.Llt_Desc = rsAE.getString("LLT_DESC");
            this.Outcome_Code = rsAE.getString("OUTCOME_CODE");
            this.Outcome = rsAE.getString("OUTCOME");
            this.Body_Sys_Num = rsAE.getString("BODY_SYS_NUM");
            this.Body_System = rsAE.getString("BODY_SYSTEM");
            this.Classification_Code = rsAE.getString("CLASSIFICATION_CODE");
            this.Classification_Desc = rsAE.getString("CLASSIFICATION_DESC");
            this.ESeverity_Code = rsAE.getString("ESEVERITY_CODE");
            this.ESeverity_Desc = rsAE.getString("ESEVERITY_DESC");
            this.strReportedToLha = rsAE.getString("REPORTED_TO_LHA");
            this.Lha_Code = rsAE.getString("LHA_CODE");
            this.Country_Code = rsAE.getString("COUNTRY_CODE");
            this.Lha_Desc = rsAE.getString("LHA_DESC");
            this.Duration = rsAE.getInt("EVENT_DURATION");
            this.Report_Date = rsAE.getDate("REPORT_DATE");
            this.Date_Start = rsAE.getDate("DATE_START");
            this.Date_Stop = rsAE.getDate("DATE_STOP");

            rsAE.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Adverse Event not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    /**
     * This static function is used to list all adverse events of a Case, from a database view.
     *
     * @param Rep_Num Report number as <code><b>int</b></code>.
     *
     * @return ArrayList The <code><b>ArrayList</b></code> will be filled with <code><b>AdverseEvent</b></code> objects.
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @exception DBConnectionException Raised when there is any problem to connect to the database.
     * @exception GenericException Raised on all other errors or problems.
     */
    public static ArrayList<Adverse_Event> listAEventFromView(String cpnyName, int Rep_Num)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Adverse_Event.java; Inside listing function");

        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<Adverse_Event> lstAEvent = new ArrayList<Adverse_Event>();
        Adverse_Event aEvent = null;

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_AE(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Llt_Code", "");
            cStmt.registerOutParameter("cur_Case_AE", oracle.jdbc.OracleTypes.CURSOR);

            if (Rep_Num != -1) {
                cStmt.setInt(1, Rep_Num);
            }

            cStmt.executeQuery();
            ResultSet rsAE = (ResultSet) cStmt.getObject("cur_Case_AE");

            while (rsAE.next()) {
                aEvent = new Adverse_Event(
                        rsAE.getInt("REP_NUM"),
                        rsAE.getString("DIAG_CODE"),
                        rsAE.getString("DIAG_DESC"),
                        rsAE.getString("LLT_CODE"),
                        rsAE.getString("LLT_DESC"),
                        rsAE.getString("OUTCOME_CODE"),
                        rsAE.getString("OUTCOME"),
                        rsAE.getString("BODY_SYS_NUM"),
                        rsAE.getString("BODY_SYSTEM"),
                        rsAE.getString("CLASSIFICATION_CODE"),
                        rsAE.getString("CLASSIFICATION_DESC"),
                        rsAE.getString("ESEVERITY_CODE"),
                        rsAE.getString("ESEVERITY_DESC"),
                        rsAE.getString("REPORTED_TO_LHA"),
                        rsAE.getString("LHA_CODE"),
                        rsAE.getString("COUNTRY_CODE"),
                        rsAE.getString("LHA_DESC"),
                        rsAE.getDate("REPORT_DATE"),
                        rsAE.getDate("DATE_START"),
                        rsAE.getDate("DATE_STOP"),
                        rsAE.getString("LABELLING"),
                        rsAE.getString("PREV_REPORT_FLG"),
                        rsAE.getString("PREV_REPORT"),
                        rsAE.getString("EVENT_TREATMENT"),
                        rsAE.getString("EVENT_ABATE_ON_STOP"),
                        rsAE.getString("AGGR_OF_DIAG"),
                        rsAE.getString("AGGR_OF_LLT"),
                        rsAE.getInt("EVENT_DURATION"));
                lstAEvent.add(aEvent);
            }
            rsAE.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Adverse Event not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstAEvent;
    }

    /**
     * Same as <code><b>listAEventFromView</b></code> and the only difference is the return value.
     *
     * @param cpnyName
     * @param Rep_Num
     * @return
     * @throws bl.sapher.general.DBConnectionException
     * @throws bl.sapher.general.GenericException
     * @throws bl.sapher.general.RecordNotFoundException
     */
    public static ResultSet listAEvent(DBCon dbCon, String cpnyName, int Rep_Num)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Adverse_Event.java; Inside listing function returning ResultSet");

        //DBCon dbCon = new DBCon(cpnyName);
        ResultSet rsAE = null;

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_AE(?,?,?)}");
            cStmt.setInt("p_Rep_Num", Rep_Num);
            cStmt.setString("p_Llt_Code", "");
            cStmt.registerOutParameter("cur_Case_AE", oracle.jdbc.OracleTypes.CURSOR);

            if (Rep_Num != -1) {
                cStmt.setInt(1, Rep_Num);
            }

            cStmt.executeQuery();
            rsAE = (ResultSet) cStmt.getObject("cur_Case_AE");

        //dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Adverse Event not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsAE;
    }

    /**
     * Function that saves an Adverse Event to database.
     * Call this function if the parameters are already been set.
     *
     * @param Username Currently loggedin user's id as <code><b>String</b></code>
     * @exception RecordNotFoundException Raised when the table contains no records matching with the given citeria.
     * @throws bl.sapher.general.DBConnectionException Raised on all other errors or problems.
     * @exception GenericException Raised on all other errors or problems.
     * @exception ConstraintViolationException Raised when constraint get violated.
     */
    public void saveAdverse_Event(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Adverse_Event.java; Inside save function (2 params)");

        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Adverse_Event(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", this.strSaveFlag);
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", this.Rep_Num);
            cStmt.setString("p_Diag_Code", this.Diag_Code);
            cStmt.setString("p_LLT_Code", this.Llt_Code);
            cStmt.setString("p_Outcome_Code", this.Outcome_Code);
            cStmt.setString("p_Body_Sys_Num", this.Body_Sys_Num);
            cStmt.setString("p_Classification_Code", this.Classification_Code);
            cStmt.setString("p_ESeverity_Code", this.ESeverity_Code);
            cStmt.setString("p_Reported_To_LHA", this.strReportedToLha);
            cStmt.setString("p_LHA_Code", this.Lha_Code);
            cStmt.setDate("p_Report_Date", this.Report_Date);
            cStmt.setDate("p_Date_Start", this.Date_Start);
            cStmt.setDate("p_Date_Stop", this.Date_Stop);
            cStmt.setString("p_Labelling", this.Labelling);
            cStmt.setString("p_Prev_Report_Flg", this.Prev_Report_Flg);
            cStmt.setString("p_Prev_Report", this.Prev_Report);
            cStmt.setString("p_Event_Treatment", this.Event_Treatment);
            cStmt.setString("p_Event_Abate_On_Stop", this.Event_Abate_On_Stop);
            cStmt.setString("p_Aggr_Of_Diag", this.Aggr_Of_Diag);
            cStmt.setString("p_Aggr_Of_LLT", this.Aggr_Of_Llt);

            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_ADVERSE_EVENT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void saveAdverse_Event(String cpnyName,
            String Save_Flag, String Username, int Rep_Num, String Diag_Code,
            String LLT_Code, String Outcome_Code, String Body_Sys_Num, String Clasf_Code,
            String Svrty_Code, String Reported_To_Lha, String Lha_Code, Date Report_Date,
            Date Date_Start, Date Date_Stop, String Labelling, String Prev_Report_Flg,
            String Prev_Report, String Event_Treatment, String Event_Abate_On_Stop, String Aggr_Of_Diag,
            String Aggr_Of_LLT)
            throws ConstraintViolationException, DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "Adverse_Event.java; Inside save function (all params)");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Adverse_Event(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Rep_Num : Rep_Num));
            cStmt.setString("p_Diag_Code", Diag_Code);
            cStmt.setString("p_LLT_Code", LLT_Code);
            cStmt.setString("p_Outcome_Code", Outcome_Code);
            cStmt.setString("p_Body_Sys_Num", Body_Sys_Num);
            cStmt.setString("p_Classification_Code", Clasf_Code);
            cStmt.setString("p_ESeverity_Code", Svrty_Code);
            cStmt.setString("p_Reported_To_LHA", Reported_To_Lha);
            cStmt.setString("p_LHA_Code", Lha_Code);
            cStmt.setDate("p_Report_Date", Report_Date);
            cStmt.setDate("p_Date_Start", Date_Start);
            cStmt.setDate("p_Date_Stop", Date_Stop);
            cStmt.setString("p_Labelling", Labelling);
            cStmt.setString("p_Prev_Report_Flg", Prev_Report_Flg);
            cStmt.setString("p_Prev_Report", Prev_Report);
            cStmt.setString("p_Event_Treatment", Event_Treatment);
            cStmt.setString("p_Event_Abate_On_Stop", Event_Abate_On_Stop);
            cStmt.setString("p_Aggr_Of_Diag", Aggr_Of_Diag);
            cStmt.setString("p_Aggr_Of_LLT", Aggr_Of_LLT);

            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_ADVERSE_EVENT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteAdverse_Event(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Adverse_Event.java; Inside delete function");

        DBCon dbCon = new DBCon(cpnyName);
        CallableStatement cStmt;
        try {
            cStmt = dbCon.getDbCon().prepareCall("{call sapher_pkg.Delete_Adverse_Event(?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_Rep_Num", this.Rep_Num);
            cStmt.setString("p_Diag_Code", this.Diag_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Rep_Num = -1;
            this.Report_Date = null;
            this.Date_Start = null;
            this.Date_Stop = null;
            this.Labelling = "";
            this.Prev_Report_Flg = "";
            this.Prev_Report = "";
            this.Event_Treatment = "";
            this.Event_Abate_On_Stop = "";
            this.Aggr_Of_Diag = "";
            this.Aggr_Of_Llt = "";

            // Newly added fields, 15/01/2008, Tue
            this.Diag_Code = "";
            this.Diag_Desc = "";
            this.Llt_Code = "";
            this.Llt_Desc = "";
            this.Outcome_Code = "";
            this.Outcome = "";
            this.Body_Sys_Num = "";
            this.Body_System = "";
            this.Classification_Code = "";
            this.Classification_Desc = "";
            this.ESeverity_Code = "";
            this.ESeverity_Desc = "";
            this.Lha_Code = "";
            this.Country_Code = "";
            this.Lha_Desc = "";
            this.Duration = -1;

            this.strReportedToLha = "";
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                System.out.println("AAdverse_Event.java: Exception: " + dbex);
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("1");
            } else {
                Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Adverse_Event.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public int getRep_Num() {
        return Rep_Num;
    }

    public void setRep_Num(int Rep_Num) {
        this.Rep_Num = Rep_Num;
    }

    public Date getDate_Start() {
        return this.Date_Start;
    }

    /**
     * @param Date_Start 
     */
    public void setDate_Start(Date Date_Start) {
        this.Date_Start = Date_Start;
    }

    public Date getDate_Stop() {
        return this.Date_Stop;
    }

    /**
     * @param Date_Stop 
     */
    public void setDate_Stop(Date Date_Stop) {
        this.Date_Stop = Date_Stop;
    }

    public String getLabelling() {
        return Labelling;
    }

    /**
     * @param Labelling 
     */
    public void setLabelling(String Labelling) {
        this.Labelling = Labelling;
    }

    public String getPrev_Report_Flg() {
        return Prev_Report_Flg;
    }

    /**
     * @param Prev_Report_Flg 
     */
    public void setPrev_Report_Flg(String Prev_Report_Flg) {
        this.Prev_Report_Flg = Prev_Report_Flg;
    }

    public String getPrev_Report() {
        return Prev_Report;
    }

    /**
     * @param Prev_Report 
     */
    public void setPrev_Report(String Prev_Report) {
        this.Prev_Report = Prev_Report;
    }

    public String getEvent_Treatment() {
        return Event_Treatment;
    }

    /**
     * @param Event_Treatment 
     */
    public void setEvent_Treatment(String Event_Treatment) {
        this.Event_Treatment = Event_Treatment;
    }

    public String getEvent_Abate_On_Stop() {
        return Event_Abate_On_Stop;
    }

    /**
     * @param Event_Abate_On_Stop 
     */
    public void setEvent_Abate_On_Stop(String Event_Abate_On_Stop) {
        this.Event_Abate_On_Stop = Event_Abate_On_Stop;
    }

    public String getAggr_Of_Diag() {
        return Aggr_Of_Diag;
    }

    /**
     * @param Aggr_Of_Diag 
     */
    public void setAggr_Of_Diag(String Aggr_Of_Diag) {
        this.Aggr_Of_Diag = Aggr_Of_Diag;
    }

    public String getAggr_Of_Llt() {
        return Aggr_Of_Llt;
    }

    /**
     * @param Aggr_Of_Llt 
     */
    public void setAggr_Of_Llt(String Aggr_Of_Llt) {
        this.Aggr_Of_Llt = Aggr_Of_Llt;
    }

    /**
     * Function returns diag code of the Adverse Event.
     *
     * @return String Diag_Code.
     */
    public String getDiag_Code() {
        return this.Diag_Code;
    }

    /**
     * Function sets diag code of the Adverse Event.
     *
     * @param Diag_Code 
     */
    public void setDiag_Code(String Diag_Code) {
        this.Diag_Code = Diag_Code;
    }

    /**
     * Function returns diag description of the Adverse Event.
     *
     * @return String Diag_Desc.
     */
    public String getDiag_Desc() {
        return this.Diag_Desc;
    }

    /**
     * Function sets diag description of the Adverse Event.
     *
     * @param Diag_Desc 
     */
    public void setDiag_Desc(String Diag_Desc) {
        this.Diag_Desc = Diag_Desc;
    }

    /**
     * Function returns LLT code of the Adverse Event.
     *
     * @return String Llt_Code.
     */
    public String getLlt_Code() {
        return this.Llt_Code;
    }

    /**
     * Function sets LLT code of the Adverse Event.
     *
     * @param Llt_Code 
     */
    public void setLlt_Code(String Llt_Code) {
        this.Llt_Code = Llt_Code;
    }

    /**
     * Function returns LLT description of the Adverse Event.
     *
     * @return String Llt_Desc.
     */
    public String getLlt_Desc() {
        return this.Llt_Desc;
    }

    /**
     * Function sets LLT description of the Adverse Event.
     *
     * @param Llt_Desc 
     */
    public void setLlt_Desc(String Llt_Desc) {
        this.Llt_Desc = Llt_Desc;
    }

    /**
     * Function returns outcome code of the Adverse Event.
     *
     * @return String Outcome_Code.
     */
    public String getOutcome_Code() {
        return this.Outcome_Code;
    }

    /**
     * Function sets outcome code of the Adverse Event.
     *
     * @param Outcome_Code 
     */
    public void setOutcome_Code(String Outcome_Code) {
        this.Outcome_Code = Outcome_Code;
    }

    /**
     * Function returns outcome of the Adverse Event.
     *
     * @return String Outcome.
     */
    public String getOutcome() {
        return this.Outcome;
    }

    /**
     * Function sets outcome of the Adverse Event.
     *
     * @param Outcome 
     */
    public void setOutcome(String Outcome) {
        this.Outcome = Outcome;
    }

    /**
     * Function returns body system number of the Adverse Event.
     *
     * @return String Body_Sys_Num.
     */
    public String getBody_Sys_Num() {
        return this.Body_Sys_Num;
    }

    /**
     * Function sets body system number of the Adverse Event.
     *
     * @param Body_Sys_Num 
     */
    public void setBody_Sys_Num(String Body_Sys_Num) {
        this.Body_Sys_Num = Body_Sys_Num;
    }

    /**
     * Function returns body system description of the Adverse Event.
     *
     * @return String Body_System.
     */
    public String getBody_System() {
        return this.Body_System;
    }

    /**
     * Function sets body system description of the Adverse Event.
     *
     * @param Body_System 
     */
    public void setBody_System(String Body_System) {
        this.Body_System = Body_System;
    }

    /**
     * Function returns classification code of the Adverse Event.
     *
     * @return String Classification_Code.
     */
    public String getClassification_Code() {
        return this.Classification_Code;
    }

    /**
     * Function sets classification code of the Adverse Event.
     *
     * @param Classification_Code 
     */
    public void setClassification_Code(String Classification_Code) {
        this.Classification_Code = Classification_Code;
    }

    /**
     * Function returns classification description of the Adverse Event.
     *
     * @return String Classification_Desc.
     */
    public String getClassification_Desc() {
        return this.Classification_Desc;
    }

    /**
     * Function sets classification description of the Adverse Event.
     *
     * @param Classification_Desc 
     */
    public void setClassification_Desc(String Classification_Desc) {
        this.Classification_Desc = Classification_Desc;
    }

    /**
     * Function returns event severity code of the Adverse Event.
     *
     * @return String ESeverity_Code.
     */
    public String getESeverity_Code() {
        return this.ESeverity_Code;
    }

    /**
     * Function sets event severity code of the Adverse Event.
     *
     * @param ESeverity_Code 
     */
    public void setESeverity_Code(String ESeverity_Code) {
        this.ESeverity_Code = ESeverity_Code;
    }

    /**
     * Function returns event severity description of the Adverse Event.
     *
     * @return String ESeverity_Desc.
     */
    public String getESeverity_Desc() {
        return this.ESeverity_Desc;
    }

    /**
     * Function sets event severity description of the Adverse Event.
     *
     * @param ESeverity_Desc 
     */
    public void setESeverity_Desc(String ESeverity_Desc) {
        this.ESeverity_Desc = ESeverity_Desc;
    }

    /**
     * Function returns LHA code of the Adverse Event.
     *
     * @return String Lha_Code.
     */
    public String getLha_Code() {
        return this.Lha_Code;
    }

    /**
     * Function sets LHA code of the Adverse Event.
     *
     * @param Lha_Code 
     */
    public void setLha_Code(String Lha_Code) {
        this.Lha_Code = Lha_Code;
    }

    /**
     * Function returns country code of the Adverse Event.
     *
     * @return String Country_Code.
     */
    public String getCountry_Code() {
        return this.Country_Code;
    }

    /**
     * Function sets country code of the Adverse Event.
     *
     * @param Country_Code 
     */
    public void setCountry_Code(String Country_Code) {
        this.Country_Code = Country_Code;
    }

    /**
     * Function returns LHA description of the Adverse Event.
     *
     * @return String Lha_Desc.
     */
    public String getLha_Desc() {
        return this.Lha_Desc;
    }

    /**
     * Function sets LHA description of the Adverse Event.
     *
     * @param Lha_Desc 
     */
    public void setLha_Desc(String Lha_Desc) {
        this.Lha_Desc = Lha_Desc;
    }

    /**
     * Function returns duration of the Adverse Event.
     *
     * @return int Duration.
     */
    public int getDuration() {
        return this.Duration;
    }

    /**
     * Function sets duration of the Adverse Event.
     *
     * @param Duration 
     */
    public void setDuration(int Duration) {
        this.Duration = Duration;
    }

    /**
     * @return 
     */
    public Date getReport_Date() {
        return Report_Date;
    }

    /**
     * @param Report_Date 
     */
    public void setReport_Date(Date Report_Date) {
        this.Report_Date = Report_Date;
    }

    /**
     * Function returns save flag of the Adverse Event.
     *
     * @return String save flag.
     */
    public String getSaveFlag() {
        return this.strSaveFlag;
    }

    /**
     * Function sets save flag of the Adverse Event.
     *
     * @param SaveFlag 
     */
    public void setSaveFlag(String SaveFlag) {
        this.strSaveFlag = SaveFlag;
    }

    /**
     * Function returns strReportedToLha of the Adverse Event.
     *
     * @return String strReportedToLha.
     */
    public String getReported_To_LHA() {
        return this.strReportedToLha;
    }

    /**
     * Function sets strReportedToLha of the Adverse Event.
     *
     * @param reportedToLHA 
     */
    public void setReported_To_LHA(String reportedToLHA) {
        this.strReportedToLha = reportedToLHA;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Aggr_Of_Diag = null;
        this.Aggr_Of_Llt = null;
        this.Date_Start = null;
        this.Date_Stop = null;
        this.Report_Date = null;
        this.Event_Abate_On_Stop = null;
        this.Event_Treatment = null;
        this.Labelling = null;
        this.Prev_Report = null;
        this.Prev_Report_Flg = null;

        // Newly added fields, 15/01/2008, Tue
        this.Diag_Code = null;
        this.Diag_Desc = null;
        this.Llt_Code = null;
        this.Llt_Desc = null;
        this.Outcome_Code = null;
        this.Outcome = null;
        this.Body_Sys_Num = null;
        this.Body_System = null;
        this.Classification_Code = null;
        this.Classification_Desc = null;
        this.ESeverity_Code = null;
        this.ESeverity_Desc = null;
        this.Lha_Code = null;
        this.Country_Code = null;
        this.Lha_Desc = null;

        this.strSaveFlag = null;
        this.strReportedToLha = null;
    }
}
