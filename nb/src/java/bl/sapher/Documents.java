/*
 *
 * Documents.java
 * Created on June 18, 2009
 * @author Anoop Varma, Focal3 LLC.
 */
package bl.sapher;

import bl.sapher.general.Constants;
import bl.sapher.general.ConstraintViolationException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

/**
 *
 * @author Anoop Varma, Focal3 LLC.
 */
public class Documents {

    private int nRepNum;
    private int nDocId;
    private String strDocName;
    private String strDocSize;
    private String strType;
    private int nIsValid;
    private static int BUFFER_SIZE = 1024;
    public static final String DOCS_RELATIVE_PATH = "Reports/docs";
    /** Log file name */
    private static String Log_File_Name;

    public Documents() {
    }

    public Documents(String logFilename) {
        Documents.Log_File_Name = logFilename;
    }

    public Documents(int RepNum, int DocId, String DocName, String DocType, String DocSiz) {
        this.nRepNum = RepNum;
        this.nDocId = DocId;
        this.strDocName = DocName;
        this.strType = DocType;
        this.strDocSize = DocSiz;
    }

    public Documents(int RepNum, int DocId) {
        this.nRepNum = RepNum;
        this.nDocId = DocId;
    }

    public static ArrayList<Documents> listDocuments(int DocSelectionMode,
            int SearchType, String cpnyName, String path, String Report_Num,
            String DocId, String DocName, String DocType, String DocDesc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Documents.java; Inside listing function");

        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<Documents> lstDocuments = new ArrayList<Documents>();
        Documents doc = null;

        try {
            CallableStatement cstmnt = dbCon.getDbCon().prepareCall(
                    "{call Sapher_Docs_Pkg.Get_Doc(?,?,?,?,?,?,?)}");

            cstmnt.setInt("p_doc_id", DocId.equals("") ? -1 : Integer.parseInt(DocId));
            cstmnt.setInt("p_rep_num", Integer.parseInt(Report_Num));
            cstmnt.setString("p_doc_title", DocName);
            cstmnt.setString("p_doc_type", DocType);
            cstmnt.setString("p_doc_desc", DocDesc);
            cstmnt.setInt("p_is_valid", Is_Valid);
            cstmnt.registerOutParameter("cur_doc", oracle.jdbc.OracleTypes.CURSOR);

            cstmnt.execute();
            ResultSet rsDocuments = (ResultSet) cstmnt.getObject("cur_doc");

            while (rsDocuments.next()) {
                if (DocSelectionMode == Constants.DOCS_PARTIAL_DOWNLOAD) {
                    doc = new Documents(Integer.parseInt(Report_Num),
                            rsDocuments.getInt("doc_id"),
                            rsDocuments.getString("doc_title"),
                            rsDocuments.getString("doc_type"),
                            rsDocuments.getString("Doc_Size"));
                    lstDocuments.add(doc);
                }
                if (DocSelectionMode == Constants.DOCS_FULL_DOWNLOAD) {
                    BufferedOutputStream bos = null;
                    BufferedInputStream bis = null;
                    File f = null;
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int nRead = 0;

                    // get attached docs from database and save on hard disk to a relative path.
                    f = new File(path + "\\", rsDocuments.getString("doc_title"));

                    bis = new BufferedInputStream(rsDocuments.getBinaryStream("doc_blob"));
                    bos = new BufferedOutputStream(new FileOutputStream(f), BUFFER_SIZE);

                    while ((nRead = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {
                        bos.write(buffer, 0, nRead);
                    }
                    bos.flush();
                    bos.close();
                }
            }

            rsDocuments.close();
            cstmnt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Documents.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Documents not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
//            Logger.writeLog(Log_File_Name, "Documents.java; Throwing GenericException"); throw new GenericException(ex.getMessage());
        }
        return lstDocuments;
    }

    public static ArrayList<Documents> downloadDocument(String cpnyName, String path,
            String Report_Num, String DocId)
            throws RecordNotFoundException, DBConnectionException, GenericException {

        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<Documents> lstDocuments = new ArrayList<Documents>();
        Documents doc = null;

        try {
            CallableStatement cstmnt = dbCon.getDbCon().prepareCall(
                    "{call Sapher_Docs_Pkg.Get_Doc(?,?,?,?,?,?,?)}");

            cstmnt.setInt("p_doc_id", Integer.parseInt(DocId));
            cstmnt.setInt("p_rep_num", Integer.parseInt(Report_Num));
            cstmnt.setString("p_doc_title", Constants.SEARCH_ANY_VAL);
            cstmnt.setString("p_doc_type", Constants.SEARCH_ANY_VAL);
            cstmnt.setString("p_doc_desc", Constants.SEARCH_ANY_VAL);
            cstmnt.setInt("p_is_valid", Constants.STATUS_ALL);
            cstmnt.registerOutParameter("cur_doc", oracle.jdbc.OracleTypes.CURSOR);

            cstmnt.execute();
            ResultSet rsDocuments = (ResultSet) cstmnt.getObject("cur_doc");

            while (rsDocuments.next()) {

                BufferedOutputStream bos = null;
                BufferedInputStream bis = null;
                File f = null;
                byte[] buffer = new byte[BUFFER_SIZE];
                int nRead = 0;

                // get attached docs from database and save on hard disk to a relative path.
                f = new File(path + "\\", rsDocuments.getString("doc_title"));

                bis = new BufferedInputStream(rsDocuments.getBinaryStream("doc_blob"));
                bos = new BufferedOutputStream(new FileOutputStream(f), BUFFER_SIZE);

                while ((nRead = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {
                    bos.write(buffer, 0, nRead);
                }
                bos.flush();
                bos.close();
            }

            rsDocuments.close();
            cstmnt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Documents.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Documents not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
//            Logger.writeLog(Log_File_Name, "Documents.java; Throwing GenericException"); throw new GenericException(ex.getMessage());
        }
        return lstDocuments;
    }

    public static void uploadDoc2Db(String cpnyName, String Username,
            String Report_Num, String docName,
            String DocDesc, int IsValid)
            throws SQLException, GenericException {
        Logger.writeLog(Log_File_Name, "Documents.java; Inside save function [uploadDoc2Db() ]");
        try {
            DBCon dbCon = new DBCon(cpnyName);
            CallableStatement cstmnt = dbCon.getDbCon().prepareCall(
                    "{call Sapher_Docs_Pkg.save_doc(?,?,?,?,?)}");

            cstmnt.setString("p_Username", Username);
            cstmnt.setInt("p_rep_num", Integer.parseInt(Report_Num));
            cstmnt.setString("p_doc_title", docName);
            cstmnt.setString("p_doc_desc", DocDesc);
            cstmnt.setInt("p_is_valid", IsValid);

            cstmnt.execute();

            cstmnt.close();
            dbCon.closeCon();
        } catch (DBConnectionException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw ex;
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
        //Logger.writeLog(Log_File_Name, "Documents.java; Throwing GenericException"); throw new GenericException(ex.getMessage());
        }
    }

    public void deleteDocuments(String cpnyName, String Username, int RepNum, int DocId)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Documents.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call Sapher_Docs_Pkg.del_doc(?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setInt("p_rep_num", RepNum);
            cStmt.setInt("p_doc_id", DocId);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();

            // Re-init member variables.
            this.nRepNum = -1;
            this.nDocId = -1;
            this.strDocName = "";
            this.strType = "";
            this.nIsValid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }

        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Documents.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Documents.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public int getRepNum() {
        return nRepNum;
    }

    public void setRepNum(int nRepNum) {
        this.nRepNum = nRepNum;
    }

    public String getDocName() {
        return strDocName;
    }

    public String getDocSize() {
        return strDocSize;
    }

    public void setStrDocName(String strDocName) {
        this.strDocName = strDocName;
    }

    public String getType() {

        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    public int getDocId() {
        return nDocId;
    }

    public void setDocId(int nDocId) {
        this.nDocId = nDocId;
    }
}
