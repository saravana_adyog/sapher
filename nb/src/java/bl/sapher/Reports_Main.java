package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRResultSetDataSource;

/**
 *
 * @author Anoop Varma, Focal3 LLC.
 */
public class Reports_Main {

    public static final int REP_CIOMS_1 = 1;
    public static final int REP_FULLCASE = 2;
    public static final int REP_WHO = 3;
    public static final int REP_MEDWATCH = 4;
    /** Log file name */
    private static String Log_File_Name;

    Reports_Main(String logFilename) {
        Reports_Main.Log_File_Name = logFilename;
    }

    public static String printPdf(int repType, String cpnyName, String path, int nRepNum)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Reports_Main.java; Inside printPdf()");
        String strPdfFileName = null;
        ResultSet rsCaseCD = null;
        ResultSet rsCaseAE = null;
        ResultSet rsCaseCM = null;
        DBCon dbCon = null;
        try {
            dbCon = new DBCon(cpnyName);

            rsCaseCD = CaseDetails.listCase(dbCon, cpnyName, nRepNum,
                    bl.sapher.general.Constants.SEARCH_ANY_VAL,
                    bl.sapher.general.Constants.SEARCH_ANY_VAL,
                    bl.sapher.general.Constants.SEARCH_ANY_VAL,
                    bl.sapher.general.Constants.SEARCH_ANY_VAL,
                    bl.sapher.general.Constants.SEARCH_ANY_VAL,
                    bl.sapher.general.Constants.STATUS_ALL);
            rsCaseAE = Adverse_Event.listAEvent(dbCon, cpnyName, nRepNum);
            rsCaseCM = Conc_Medication.listConc_Medication(dbCon, cpnyName, nRepNum);

            HashMap<String, Object> param = new HashMap<String, Object>();
            param.put("SUBREPORT_DIR", path + "\\");
            param.put("srConn", new JRResultSetDataSource(rsCaseAE));
            param.put("srConnCM", new JRResultSetDataSource(rsCaseCM));

            switch (repType) {
                case REP_CIOMS_1:
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repCIOMS1.jrxml", rsCaseCD, param);
                    break;
                case REP_FULLCASE:
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repFullCase.jrxml", rsCaseCD, param);
                    break;
                case REP_WHO:
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repWHO.jrxml", rsCaseCD, param);
                    break;
                case REP_MEDWATCH:
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repMedWatch.jrxml", rsCaseCD, param);
                    break;
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Reports_Main.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Reports_Main.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        } finally {
            try {
                rsCaseCD.close();
                rsCaseAE.close();
                rsCaseCM.close();
                dbCon.closeCon();
            } catch (SQLException ex) {
                Logger.writeLog(Log_File_Name, "Reports_Main.java; Caught SQLException-" + ex);
            }
        }
        return strPdfFileName;
    }
}
