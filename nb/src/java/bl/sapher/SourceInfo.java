/*
 * CDL19
 * SourceInfo.java
 * Created on November 30, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class SourceInfo implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 7077186402458655706L;
    private String Source_Code;
    private String Source_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public SourceInfo() {
        this.Source_Code = "";
        this.Source_Desc = "";
        this.Is_Valid = -1;
    }

    public SourceInfo(String logFilename) {
        this.Source_Code = "";
        this.Source_Desc = "";
        this.Is_Valid = -1;
        SourceInfo.Log_File_Name = logFilename;
    }

    public SourceInfo(String Source_Code, String Source_Desc, int Is_Valid) {
        this.Source_Code = Source_Code;
        this.Source_Desc = Source_Desc;
        this.Is_Valid = Is_Valid;
    }

    public SourceInfo(String cpnyName, String Source_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepSrc(?,?,?,?,?)}");
            cStmt.setString("p_Code", Source_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_RSrc", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSourceInfo = (ResultSet) cStmt.getObject("cur_RSrc");
            rsSourceInfo.next();
            this.Source_Code = Source_Code;
            this.Source_Desc = rsSourceInfo.getString("Source_Desc");
            this.Is_Valid = rsSourceInfo.getInt("Is_Valid");
            rsSourceInfo.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Source Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<SourceInfo> listSourceInfo(int SearchType, String cpnyName, String Source_Code, String Source_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SourceInfo.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<SourceInfo> lstSourceInfo = new ArrayList<SourceInfo>();
        SourceInfo sourceinfo = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepSrc(?,?,?,?,?)}");
            cStmt.setString("p_Code", Source_Code);
            cStmt.setString("p_Desc", Source_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_RSrc", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSourceInfo = (ResultSet) cStmt.getObject("cur_RSrc");
            while (rsSourceInfo.next()) {
                sourceinfo = new SourceInfo(rsSourceInfo.getString("Source_Code"),
                        rsSourceInfo.getString("Source_Desc"),
                        rsSourceInfo.getInt("Is_Valid"));
                lstSourceInfo.add(sourceinfo);
            }
            rsSourceInfo.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Source Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstSourceInfo;
    }

    public void saveSourceInfo(String cpnyName, String Save_Flag, String Username,
            String Source_Code, String Source_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SourceInfo.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Source_Info(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Source_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Source_Code : Source_Code));
            cStmt.setString("p_Source_Desc", Source_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Source_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Source_Code : Source_Code);
            this.Source_Desc = (!Source_Desc.equals("") ? Source_Desc : this.Source_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_SOURCE_INFO") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_SOURCE_INFO") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteSourceInfo(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "SourceInfo.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Source_Info(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Source_Code", this.Source_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Source_Code = "";
            this.Source_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_SOURCE") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "SourceInfo.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepSrc(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_RSrc", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_RSrc");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repRepSrc.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Source information not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SourceInfo.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getSource_Code() {
        return Source_Code;
    }

    public String getSource_Desc() {
        return Source_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Source_Code = null;
        this.Source_Desc = null;
    }
}
