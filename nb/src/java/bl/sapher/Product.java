/*
 * CDL16
 * Product.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Product implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 1969467729937516537L;
    private String Product_Code;
    private String Generic_Nm;
    private String Brand_Nm;
    private int Is_Valid;
    private static String Log_File_Name;

    public Product() {
        this.Product_Code = "";
        this.Generic_Nm = "";
        this.Brand_Nm = "";
        this.Is_Valid = -1;
    }

    public Product(String logFilename) {
        this.Product_Code = "";
        this.Generic_Nm = "";
        this.Brand_Nm = "";
        this.Is_Valid = -1;
        Product.Log_File_Name = logFilename;
    }

    public Product(String Product_Code, String Generic_Nm, String Brand_Nm, int Is_Valid) {
        this.Product_Code = Product_Code;
        this.Generic_Nm = Generic_Nm;
        this.Brand_Nm = Brand_Nm;
        this.Is_Valid = Is_Valid;
    }

    public Product(String cpnyName, String Product_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Product(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Product_Code);
            cStmt.setString("p_GNm", "");
            cStmt.setString("p_BNm", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Pdt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsProduct = (ResultSet) cStmt.getObject("cur_Pdt");
            rsProduct.next();
            this.Product_Code = Product_Code;
            this.Generic_Nm = rsProduct.getString("Generic_Nm");
            this.Brand_Nm = rsProduct.getString("Brand_Nm");
            this.Is_Valid = rsProduct.getInt("Is_Valid");
            rsProduct.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Product not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Product> listProduct(int SearchType, String cpnyName, String Product_Code, String Generic_Nm, String Brand_Nm, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Permission.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Product> lstProduct = new ArrayList<Product>();
        Product prdct = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Product(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", Product_Code);
            cStmt.setString("p_GNm", Generic_Nm);
            cStmt.setString("p_BNm", Brand_Nm);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Pdt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsProduct = (ResultSet) cStmt.getObject("cur_Pdt");
            while (rsProduct.next()) {
                prdct = new Product(rsProduct.getString("Product_Code"),
                        rsProduct.getString("Generic_Nm"),
                        rsProduct.getString("Brand_Nm"),
                        rsProduct.getInt("Is_Valid"));
                lstProduct.add(prdct);
            }
            rsProduct.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Product not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstProduct;
    }

    public void saveProduct(String cpnyName, String Save_Flag, String Username,
            String Product_Code, String Generic_Nm, String Brand_Nm, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Permission.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Product(?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Product_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Product_Code : Product_Code));
            cStmt.setString("p_Generic_Nm", Generic_Nm);
            cStmt.setString("p_Brand_Nm", Brand_Nm);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Product_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Product_Code : Product_Code);
            this.Generic_Nm = (!Generic_Nm.equals("") ? Generic_Nm : this.Generic_Nm);
            this.Brand_Nm = (!Brand_Nm.equals("") ? Brand_Nm : this.Brand_Nm);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_PRODUCT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_PRODUCT") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteProduct(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Permission.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Product(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Product_Code", this.Product_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Product_Code = "";
            this.Generic_Nm = "";
            this.Brand_Nm = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_PRODUCT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_CONC_MEDIC_PRODUCT") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String genName, String brandName, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Permission.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Product(?,?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_GNm", genName);
            cStmt.setString("p_BNm", brandName);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Pdt", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Pdt");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repProduct.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Product not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Permission.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Permission.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getProduct_Code() {
        return Product_Code;
    }

    public String getGeneric_Nm() {
        return Generic_Nm;
    }

    public String getBrand_Nm() {
        return Brand_Nm;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Brand_Nm = null;
        this.Generic_Nm = null;
        this.Product_Code = null;
    }
}
