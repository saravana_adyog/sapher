/*
 * CDL17
 * RechallengeResult.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class RechallengeResult implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -5246801736343732379L;
    private String Result_Code;
    private String Result_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public RechallengeResult() {
        this.Result_Code = "";
        this.Result_Desc = "";
        this.Is_Valid = -1;
    }

    public RechallengeResult(String logFilename) {
        this.Result_Code = "";
        this.Result_Desc = "";
        this.Is_Valid = -1;
        RechallengeResult.Log_File_Name = logFilename;
    }

    public RechallengeResult(String Result_Code, String Result_Desc, int Is_Valid) {
        this.Result_Code = Result_Code;
        this.Result_Desc = Result_Desc;
        this.Is_Valid = Is_Valid;
    }

    public RechallengeResult(String cpnyName, String Result_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RechResult(?,?,?,?,?)}");
            cStmt.setString("p_Code", Result_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Res", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsRechallengeResult = (ResultSet) cStmt.getObject("cur_Res");
            rsRechallengeResult.next();
            this.Result_Code = Result_Code;
            this.Result_Desc = rsRechallengeResult.getString("Result_Desc");
            this.Is_Valid = rsRechallengeResult.getInt("Is_Valid");
            rsRechallengeResult.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Rechallenge Result Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<RechallengeResult> listRechallengeResult(int SearchType, String cpnyName, String Result_Code, String Result_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "RechallengeResult.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<RechallengeResult> lstRechallengeResult = new ArrayList<RechallengeResult>();
        RechallengeResult rechallengeresult = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RechResult(?,?,?,?,?)}");
            cStmt.setString("p_Code", Result_Code);
            cStmt.setString("p_Desc", Result_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Res", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsRechallengeResult = (ResultSet) cStmt.getObject("cur_Res");
            while (rsRechallengeResult.next()) {
                rechallengeresult = new RechallengeResult(rsRechallengeResult.getString("Result_Code"), rsRechallengeResult.getString("Result_Desc"),
                        rsRechallengeResult.getInt("Is_Valid"));
                lstRechallengeResult.add(rechallengeresult);
            }
            rsRechallengeResult.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Rechallenge Result Code not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstRechallengeResult;
    }

    public void saveRechallengeResult(String cpnyName, String Save_Flag, String Username, String Result_Code, String Result_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "RechallengeResult.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Rechallenge_Result(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Result_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Result_Code : Result_Code));
            cStmt.setString("p_Result_Desc", Result_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Result_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Result_Code : Result_Code);
            this.Result_Desc = (Result_Desc != "" ? Result_Desc : this.Result_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_RECHALLENGE_RESULT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_RECHALLENGE_RESULT") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteRechallengeResult(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "RechallengeResult.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Rechallenge_Result(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Result_Code", this.Result_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Result_Code = "";
            this.Result_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_RCH_RESULT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "RechallengeResult.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RechResult(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Res", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Res");

            java.util.Hashtable t = null;
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repRechResult.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Rechallenge result not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "RechallengeResult.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getResult_Code() {
        return Result_Code;
    }

    public String getResult_Desc() {
        return Result_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Result_Code = null;
        this.Result_Desc = null;
    }
}
