/*
 * AUD1
 * AuditTrial.java
 * Created on November 23, 2007, 2:18 PM
 * @author Arun P. Jose, Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Time;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class AuditTrial implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = -6081855916518351290L;
    private int Chg_No;
    private String Username;
    private Inventory inv;
    private String Tgt_Type;
    private String Txn_Type;
    private Date Chg_Date;
    private Time Chg_Time;
    private String Pk_Col_Value;
    private InventoryCols invCol;
    private String Old_Val;
    private String New_Val;
    private static String Log_File_Name;

    public AuditTrial() {
        this.Chg_No = -1;
        this.Username = "";
        this.inv = null;
        this.Tgt_Type = "";
        this.Txn_Type = "";
        this.Chg_Date = null;
        this.Chg_Time = null;
        this.Pk_Col_Value = "";
        this.invCol = null;
        this.Old_Val = "";
        this.New_Val = "";
    }

    public AuditTrial(String logFilename) {
        this.Chg_No = -1;
        this.Username = "";
        this.inv = null;
        this.Tgt_Type = "";
        this.Txn_Type = "";
        this.Chg_Date = null;
        this.Chg_Time = null;
        this.Pk_Col_Value = "";
        this.invCol = null;
        this.Old_Val = "";
        this.New_Val = "";
        AuditTrial.Log_File_Name = logFilename;
    }

    public AuditTrial(int Chg_No, String Username, Inventory inv, String Txn_Type,
            String Tgt_Type, Date Chg_Date, Time Chg_Time, String Pk_Col_Value, InventoryCols invCol,
            String Old_Val, String New_Val)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        this.Chg_No = Chg_No;
        this.Username = Username;
        this.inv = inv;
        this.Tgt_Type = Tgt_Type;
        this.Txn_Type = Txn_Type;
        this.Chg_Date = Chg_Date;
        this.Chg_Time = Chg_Time;
        this.Pk_Col_Value = Pk_Col_Value;
        this.invCol = invCol;
        this.Old_Val = Old_Val;
        this.New_Val = New_Val;
    }

    public AuditTrial(String cpnyName, int Chg_No)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Audit_Trial(?,?,?,?,?,?,?,?,?)}");
            cStmt.setInt("p_ChgNo", Chg_No);
            cStmt.setString("p_Username", "");
            cStmt.setString("p_InventoryId", "");
            cStmt.setString("p_Txn_Type", "");
            cStmt.setString("p_Tgt_Type", "");
            cStmt.setString("p_From", "");
            cStmt.setString("p_To", "");
            cStmt.setString("p_RefVal", "");
            cStmt.registerOutParameter("cur_Audit", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsAudit = (ResultSet) cStmt.getObject("cur_Audit");
            rsAudit.next();
            this.Chg_No = Chg_No;
            this.Username = rsAudit.getString("a_Username");
            this.inv = new Inventory(rsAudit.getString("i_Inv_Id"), rsAudit.getString("i_Inv_Desc"),
                    rsAudit.getString("i_Inv_Type"), rsAudit.getInt("i_Ins_Audit_Flag"), rsAudit.getInt("i_Del_Audit_Flag"));
            this.Tgt_Type = rsAudit.getString("a_Tgt_Type");
            this.Txn_Type = rsAudit.getString("a_Txn_Type");
            this.Chg_Date = rsAudit.getDate("a_Chg_Time");
            this.Chg_Time = rsAudit.getTime("a_Chg_Time");
            this.Pk_Col_Value = rsAudit.getString("a_Pk_Col_Value");
            this.invCol = new InventoryCols(rsAudit.getString("ic_Inv_Col_Id"), this.inv,
                    rsAudit.getString("ic_Field_Name"), rsAudit.getInt("ic_Audit_Flag"));
            this.Old_Val = rsAudit.getString("a_Old_Val");
            this.New_Val = rsAudit.getString("a_New_Val");
            rsAudit.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Audit Trial not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList listAudit(String cpnyName, String Username, String Inv_Id, String Txn_Type,
            String Tgt_Type, String From_Date, String To_Date, String Ref_Val)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AuditTrial.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        String qry = "";
        int ctr = 0;
        ArrayList<AuditTrial> lstAudit = new ArrayList<AuditTrial>();
        AuditTrial adt = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Audit_Trial(?,?,?,?,?,?,?,?,?)}");
            cStmt.setInt("p_ChgNo", -1);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_InventoryId", Inv_Id);
            cStmt.setString("p_Txn_Type", Txn_Type);
            cStmt.setString("p_Tgt_Type", Tgt_Type);
            cStmt.setString("p_From", From_Date);
            cStmt.setString("p_To", To_Date);
            cStmt.setString("p_RefVal", Ref_Val);
            cStmt.registerOutParameter("cur_Audit", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsAudit = (ResultSet) cStmt.getObject("cur_Audit");
            while (rsAudit.next()) {
                adt = new AuditTrial(rsAudit.getInt("a_Chg_No"), rsAudit.getString("a_Username"),
                        new Inventory(rsAudit.getString("i_Inv_Id"), rsAudit.getString("i_Inv_Desc"),
                        rsAudit.getString("i_Inv_Type"), rsAudit.getInt("i_Ins_Audit_Flag"), rsAudit.getInt("i_Del_Audit_Flag")),
                        rsAudit.getString("a_Txn_Type"), rsAudit.getString("a_Tgt_Type"),
                        rsAudit.getDate("a_Chg_Time"), rsAudit.getTime("a_Chg_Time"),
                        rsAudit.getString("a_Pk_Col_Value"),
                        (rsAudit.getString("ic_Inv_Col_Id") == null ? null : new InventoryCols(rsAudit.getString("ic_Inv_Col_Id"),
                        new Inventory(rsAudit.getString("i_Inv_Id"), rsAudit.getString("i_Inv_Desc"),
                        rsAudit.getString("i_Inv_Type"), rsAudit.getInt("i_Ins_Audit_Flag"), rsAudit.getInt("i_Del_Audit_Flag")),
                        rsAudit.getString("ic_Field_Name"),
                        rsAudit.getInt("ic_Audit_Flag"))),
                        rsAudit.getString("a_Old_Val"), rsAudit.getString("a_New_Val"));
                lstAudit.add(adt);
            }
            rsAudit.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Audit Trial not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstAudit;
    }

    public void deleteAuditTrial(String cpnyName)
            throws DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AuditTrial.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_AuditTrial(?)}");
            cStmt.setInt("p_Chg_No", this.Chg_No);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Chg_No = -1;
            this.Username = "";
            this.inv = null;
            this.Tgt_Type = "";
            this.Txn_Type = "";
            this.Chg_Date = null;
            this.Pk_Col_Value = "";
            this.invCol = null;
            this.Old_Val = "";
            this.New_Val = "";
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String username, String invId, String transType,
            String tgtType, String fromDate, String toDate, String refVal)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "AuditTrial.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Audit_Trial(?,?,?,?,?,?,?,?,?)}");
            cStmt.setInt("p_ChgNo", -1);
            cStmt.setString("p_Username", username);
            cStmt.setString("p_InventoryId", invId);
            cStmt.setString("p_Txn_Type", transType);
            cStmt.setString("p_Tgt_Type", tgtType);
            cStmt.setString("p_From", fromDate);
            cStmt.setString("p_To", toDate);
            cStmt.setString("p_RefVal", refVal);
            cStmt.registerOutParameter("cur_Audit", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Audit");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repAuditTrail.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Audit Trail not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "AuditTrial.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public int getChg_No() {
        return Chg_No;
    }

    public String getUsername() {
        return Username;
    }

    public Inventory getInventory() {
        return inv;
    }

    public String getTgt_Type() {
        return Tgt_Type;
    }

    public String getTxn_Type() {
        return Txn_Type;
    }

    public Date getChg_Date() {
        return Chg_Date;
    }

    public Time getChg_Time() {
        return Chg_Time;
    }

    public String getPk_Col_Value() {
        return Pk_Col_Value;
    }

    public InventoryCols getInventoryCols() {
        return invCol;
    }

    public String getOld_Val() {
        return Old_Val;
    }

    public String getNew_Val() {
        return New_Val;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Chg_Date = null;
        this.invCol = null;
        this.New_Val = null;
        this.Old_Val = null;
        this.Pk_Col_Value = null;
        this.Tgt_Type = null;
        this.Txn_Type = null;
        this.inv = null;
        this.Username = null;
    }
}
