/*
 * USM1
 * User.java
 * Created on November 20, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.UserBlockedException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import bl.sapher.general.Constants;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.License;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.SessionCounter;
import bl.sapher.general.UserCountException;

public class User {

    private String Username;
    private Role rl;
    private String Password;
    private int Pwd_Change_Flag;
    private int Pwd_Mistake_Count;
    private Date Pwd_Change_Date;
    private int Is_Locked;
    private int Is_User_Locked;
    private static int Is_Blocked;
    private String strRemoteIP;
    private int nMaxAllowedUsers = 0;
    private int nLoggedinUsers = 0;
    /** Log file name */
    private static String Log_File_Name;


    static {
        Is_Blocked = 0;
    }

    public static void blockUsers() {
        Is_Blocked = 1;
    }

    public static void unBlockUsers() {
        Is_Blocked = 0;
    }

    private String encrypt(String inStr) {
        String outStr = null;
        char[] cArr = inStr.toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = ((char) (cArr[i] + 8));
        }
        outStr = new String(cArr);
        return outStr;
    }

    private String decrypt(String inStr) {
        String outStr = null;
        char[] cArr = inStr.toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = ((char) (cArr[i] - 8));
        }
        outStr = new String(cArr);
        return outStr;
    }

    public User() {
        this.Username = "";
        this.rl = null;
        this.Password = "";
        this.Pwd_Change_Flag = 1;
        this.Pwd_Mistake_Count = -1;
        this.Pwd_Change_Date = null;
        this.Is_Locked = -1;
        this.Is_User_Locked = -1;
    }

    public User(String logFilename) {
        this.Username = "";
        this.rl = null;
        this.Password = "";
        this.Pwd_Change_Flag = 1;
        this.Pwd_Mistake_Count = -1;
        this.Pwd_Change_Date = null;
        this.Is_Locked = -1;
        this.Is_User_Locked = -1;
        User.Log_File_Name = logFilename;
    }

    public User(String Username, Role role,
            String Password, int Pwd_Change_Flag,
            int Pwd_Mistake_Count, Date Pwd_Change_Date,
            int Is_Locked, int Is_User_Locked) {
        this.Username = Username;
        this.rl = role;
        this.Password = decrypt(Password);
        this.Pwd_Change_Flag = Pwd_Change_Flag;
        this.Pwd_Mistake_Count = Pwd_Mistake_Count;
        this.Pwd_Change_Date = Pwd_Change_Date;
        this.Is_Locked = Is_Locked;
        this.Is_User_Locked = Is_User_Locked;
    }

    public User(String logFilename, String company, String Username)
            throws DBConnectionException, RecordNotFoundException, GenericException,
            UserBlockedException, UserCountException {
        User.Log_File_Name = logFilename;
        if (Is_Blocked == 1) {
            throw new UserBlockedException();
        }

        DBCon dbCon = new DBCon(company);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_User(?,?,?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_RoleName", "");
            cStmt.setInt("p_Is_Locked", -1);
            cStmt.setInt("p_Search_Type", Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_User", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();

            ResultSet rsUser = (ResultSet) cStmt.getObject("cur_User");
            rsUser.next();
            this.Username = Username;
            this.rl = new Role(rsUser.getString("r_Role_Name"), rsUser.getInt("r_Is_Locked"));
            this.Password = (rsUser.getString("u_Password") != null ? decrypt(rsUser.getString("u_Password")) : "");
            this.Pwd_Change_Flag = rsUser.getInt("u_Pwd_Change_Flag");
            this.Pwd_Mistake_Count = rsUser.getInt("u_Pwd_Mistake_Count");
            this.Pwd_Change_Date = rsUser.getDate("u_Pwd_Change_Date");
            this.Is_Locked = rsUser.getInt("u_Is_Locked");
            this.Is_User_Locked = rsUser.getInt("u_Lock");

            rsUser.close();
            cStmt.close();
            dbCon.closeCon();

            checkUserCount(company);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("User not found!");
        } catch (UserCountException uce) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                uce.printStackTrace();
            }
            throw uce;
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void checkUserCount(String company) throws UserCountException {

        try {
            nMaxAllowedUsers = License.getNumOfUsers(company);

            if (SessionCounter.getActiveSessions() > nMaxAllowedUsers) {
                throw new UserCountException();
            }
        } catch (UserCountException uce) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                uce.printStackTrace();
            }
            throw uce;
        } catch (Exception e) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                e.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public User(String logFilename, String cpnyName, String Username, String SessionId)
            throws DBConnectionException, RecordNotFoundException, GenericException, UserBlockedException {
        User.Log_File_Name = logFilename;
        if (Is_Blocked == 1) {
            throw new UserBlockedException();
        }
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_User_Session(?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Session", SessionId);
            cStmt.registerOutParameter("cur_User", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsUser = (ResultSet) cStmt.getObject("cur_User");
            rsUser.next();

            this.Username = Username;
            this.rl = new Role(rsUser.getString("r_Role_Name"), rsUser.getInt("r_Is_Locked"));
            this.Password = (rsUser.getString("u_Password") != null ? decrypt(rsUser.getString("u_Password")) : "");
            this.Pwd_Change_Flag = rsUser.getInt("u_Pwd_Change_Flag");
            this.Pwd_Mistake_Count = rsUser.getInt("u_Pwd_Mistake_Count");
            this.Pwd_Change_Date = rsUser.getDate("u_Pwd_Change_Date");
            this.Is_Locked = rsUser.getInt("u_Is_Locked");
            this.Is_User_Locked = rsUser.getInt("u_Lock");
            rsUser.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            System.out.println(Username + "-" + SessionId);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("User not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<User> listUser(int SearchType, String cpnyName, String Username, String Role_Name, int Is_Locked)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        Logger.writeLog(Log_File_Name, "User.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        String qry = "";
        int ctr = 0;
        ArrayList<User> lstUser = new ArrayList<User>();
        User usr = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_User(?,?,?,?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_RoleName", Role_Name);
            cStmt.setInt("p_Is_Locked", Is_Locked);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_User", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsUser = (ResultSet) cStmt.getObject("cur_User");
            while (rsUser.next()) {
                usr = new User(rsUser.getString("u_Username"),
                        new Role(rsUser.getString("r_Role_Name"), rsUser.getInt("r_Is_Locked")),
                        rsUser.getString("u_Password"), rsUser.getInt("u_Pwd_Change_Flag"),
                        rsUser.getInt("u_Pwd_Mistake_Count"), rsUser.getDate("u_Pwd_Change_Date"),
                        rsUser.getInt("u_Is_Locked"), rsUser.getInt("u_Lock"));
                lstUser.add(usr);
            }
            rsUser.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("User not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstUser;
    }

    public void saveUser(String cpnyName, String Save_Flag, String Username, String Role_Name,
            String Password, int Pwd_Change_Flag,
            int Pwd_Mistake_Count, int Is_Locked)
            throws DBConnectionException, ConstraintViolationException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "User.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        //GenConf sysProp = new GenConf();
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall("{call sapher_pkg.Save_User_List(?,?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", (Save_Flag.equalsIgnoreCase("U") ? this.Username : Username));
            cStmt.setString("p_Role_Name", Role_Name);
            cStmt.setString("p_Password", encrypt(Password));
            cStmt.setInt("p_Pwd_Change_Flag", Pwd_Change_Flag);
            cStmt.setInt("p_Pwd_Mistake_Count", Pwd_Mistake_Count);
            cStmt.setInt("p_Is_Locked", Is_Locked);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Username = (Save_Flag.equalsIgnoreCase("U") ? this.Username : Username);
            this.rl = (!Role_Name.equals("") ? new Role(cpnyName, Role_Name) : this.rl);
            if (!Password.equals("")) {
                this.Password = Password;
                this.Pwd_Change_Date = new Date(new java.util.Date().getTime());
            }
            this.Pwd_Change_Flag = (Pwd_Change_Flag != -1 ? Pwd_Change_Flag : this.Pwd_Change_Flag);
            this.Pwd_Mistake_Count = (Pwd_Mistake_Count != -1 ? Pwd_Mistake_Count : this.Pwd_Mistake_Count);
            this.Is_User_Locked = (Is_Locked != -1 ? Is_Locked : this.Is_Locked);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            ex.printStackTrace();
            if (ex.getMessage().indexOf("PK_USER") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_USER_ROLE") != -1) {
                throw new ConstraintViolationException("2");
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteUser(String cpnyName)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "User.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_User_List(?)}");
            cStmt.setString(1, this.Username);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Username = "";
            this.rl = null;
            this.Password = "";
            this.Pwd_Change_Flag = -1;
            this.Pwd_Mistake_Count = -1;
            this.Pwd_Change_Date = null;
            this.Is_Locked = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("1");
            } else {
                Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public String registerUser(String cpnyName, String strIP)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        DBCon dbCon = new DBCon(cpnyName);
        String sessId = "";
         Logger.writeLog(Log_File_Name, "User.java; print -" + cpnyName+
                " - " +
                this.Username + " - " +
             strIP + " - "  );
            
        try {
            Logger.writeLog(Log_File_Name, "User.java; license -" + License.getNumOfUsers(cpnyName));
           
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Register_User(?,?,?,?)}");
            cStmt.setInt("p_UserCount", License.getNumOfUsers(cpnyName));
            cStmt.setString("p_Username", this.Username);
            cStmt.setString("p_RemoteIp", strIP);
            cStmt.registerOutParameter("p_SessionId", java.sql.Types.VARCHAR);
            
            

            cStmt.execute();
            sessId = cStmt.getString("p_SessionId");
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();

            SessionCounter.increaseCounter(sessId);
            System.out.println("C-" + sessId + ", " + SessionCounter.getActiveSessions());
            return sessId;
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex.getMessage());
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void unRegisterUser(String cpnyName, String SessionId)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        DBCon dbCon = new DBCon(cpnyName);
        String sessId = "";
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.UnRegister_User(?)}");
            cStmt.setString(1, SessionId);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();

        //decreaseLoggedUserCount();
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "User.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            Logger.writeLog(Log_File_Name, "User.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public boolean getPermission(String cpnyName, Inventory inv, char pType)
            throws DBConnectionException, RecordNotFoundException, GenericException {
        return rl.getPermission(cpnyName, inv, pType);
    }

    public void decreaseLoggedUserCount() {
        nLoggedinUsers--;
    }

    public void increaseLoggedUserCount() {
        nLoggedinUsers++;
    }

    public String getUsername() {
        return Username;
    }

    public Role getRole() {
        return rl;
    }

    public String getPassword() {
        return Password;
    }

    public int getPwd_Change_Flag() {
        return Pwd_Change_Flag;
    }

    public int getPwd_Mistake_Count() {
        return Pwd_Mistake_Count;
    }

    public Date getPwd_Change_Date() {
        return Pwd_Change_Date;
    }

    public int getIs_Locked() {
        return Is_Locked;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Password = null;
        this.Pwd_Change_Date = null;
        this.Username = null;
        this.rl = null;
    }

    public int getIs_User_Locked() {
        return Is_User_Locked;
    }
}
