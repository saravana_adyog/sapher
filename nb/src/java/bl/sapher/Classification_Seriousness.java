/*
 * CDL5
 * Classification_Seriousness.java
 * Created on November 27, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class Classification_Seriousness implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -3619812742837340972L;
    private String Clasf_Code;
    private String Clasf_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public Classification_Seriousness() {
        this.Clasf_Code = "";
        this.Clasf_Desc = "";
        this.Is_Valid = -1;
    }

    public Classification_Seriousness(String logFilename) {
        this.Clasf_Code = "";
        this.Clasf_Desc = "";
        this.Is_Valid = -1;
        Classification_Seriousness.Log_File_Name = logFilename;
    }

    public Classification_Seriousness(String Clasf_Code, String Clasf_Desc, int Is_Valid) {
        this.Clasf_Code = Clasf_Code;
        this.Clasf_Desc = Clasf_Desc;
        this.Is_Valid = Is_Valid;
    }

    public Classification_Seriousness(String cpnyName, String Clasf_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Seriousness(?,?,?,?,?)}");
            cStmt.setString("p_Code", Clasf_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Clasf", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCSrs = (ResultSet) cStmt.getObject("cur_Clasf");
            rsCSrs.next();
            this.Clasf_Code = Clasf_Code;
            this.Clasf_Desc = rsCSrs.getString("Classification_Desc");
            this.Is_Valid = rsCSrs.getInt("Is_Valid");
            rsCSrs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Classification Seriousness not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Classification_Seriousness> listClasfSeriousness(int SearchType, String cpnyName, String Clasf_Code, String Clasf_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Classification_Seriousness> lstCSrs = new ArrayList<Classification_Seriousness>();
        Classification_Seriousness clfsrs = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Seriousness(?,?,?,?,?)}");
            cStmt.setString("p_Code", Clasf_Code);
            cStmt.setString("p_Desc", Clasf_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Clasf", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsCSrs = (ResultSet) cStmt.getObject("cur_Clasf");
            while (rsCSrs.next()) {
                clfsrs = new Classification_Seriousness(rsCSrs.getString("Classification_Code"),
                        rsCSrs.getString("Classification_Desc"),
                        rsCSrs.getInt("Is_Valid"));
                lstCSrs.add(clfsrs);
            }
            rsCSrs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Classification Seriousness not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstCSrs;
    }

    public void saveClasfSeriousness(String cpnyName, String Save_Flag, String Username,
            String Clasf_Code, String Clasf_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Seriousness(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Classification_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Clasf_Code : Clasf_Code));
            cStmt.setString("p_Classification_Desc", Clasf_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Clasf_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Clasf_Code : Clasf_Code);
            this.Clasf_Desc = (!Clasf_Desc.equals("") ? Clasf_Desc : this.Clasf_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_CLASSIFICATION_SERIOUSNESS") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_CLASSIFICATION_SERIOUSNESS") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteClasfSeriousness(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Seriousness(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Classification_Code", this.Clasf_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Clasf_Code = "";
            this.Clasf_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AE_CLASS_SERIOUSNESS") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Seriousness(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Clasf", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Clasf");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repClassSerious.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Classification Seriousness not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Classification_Seriousness.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getClasf_Code() {
        return Clasf_Code;
    }

    public String getClasf_Desc() {
        return Clasf_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Clasf_Code = null;
        this.Clasf_Desc = null;
    }
}
