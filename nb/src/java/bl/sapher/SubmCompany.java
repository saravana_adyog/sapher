/*
 * CDL23
 * SubmCompany.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class SubmCompany implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 3131601407723618410L;
    private String Company_Nm;
    private Country ctry;
    private int Is_Valid;
    private static String Log_File_Name;

    public SubmCompany() {
        this.Company_Nm = "";
        this.ctry = null;
        this.Is_Valid = -1;
    }

    public SubmCompany(String logFilename) {
        this.Company_Nm = "";
        this.ctry = null;
        this.Is_Valid = -1;
        SubmCompany.Log_File_Name = logFilename;
    }

    public SubmCompany(String Company_Nm, Country ctry, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        this.Company_Nm = Company_Nm;
        this.ctry = ctry;
        this.Is_Valid = Is_Valid;
    }

    public SubmCompany(String cpnyName, String Company_Nm)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_SCo(?,?,?,?,?)}");
            cStmt.setString("p_Name", Company_Nm);
            cStmt.setString("p_Ctry", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_SCo", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSubmCompany = (ResultSet) cStmt.getObject("cur_SCo");
            rsSubmCompany.next();
            this.Company_Nm = Company_Nm;
            this.ctry = new Country(rsSubmCompany.getString("c_Country_Code"), rsSubmCompany.getString("c_Country_Nm"),
                    rsSubmCompany.getInt("c_Is_Valid"));
            this.Is_Valid = rsSubmCompany.getInt("s_Is_Valid");
            rsSubmCompany.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Distribution Company not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<SubmCompany> listSubmCompany(int SearchType, String cpnyName, String Company_Nm, String Country_Nm, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SubmCompany.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<SubmCompany> lstSubmCompany = new ArrayList<SubmCompany>();
        SubmCompany distcomp = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_SCo(?,?,?,?,?)}");
            cStmt.setString("p_Name", Company_Nm);
            cStmt.setString("p_Ctry", Country_Nm);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_SCo", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSubmCompany = (ResultSet) cStmt.getObject("cur_SCo");
            while (rsSubmCompany.next()) {
                distcomp = new SubmCompany(rsSubmCompany.getString("s_Company_Nm"),
                        new Country(rsSubmCompany.getString("c_Country_Code"),
                        rsSubmCompany.getString("c_Country_Nm"),
                        rsSubmCompany.getInt("c_Is_Valid")),
                        rsSubmCompany.getInt("s_Is_Valid"));
                lstSubmCompany.add(distcomp);
            }
            rsSubmCompany.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Submission Company not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstSubmCompany;
    }

    public void saveSubmCompany(String cpnyName, String Save_Flag, String Username,
            String Company_Nm, String Ctry_Code, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SubmCompany.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Subm_Company(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Company_Nm",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Company_Nm : Company_Nm));
            cStmt.setString("p_Country_Code", Ctry_Code);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Company_Nm = (Save_Flag.equalsIgnoreCase("U") ? this.Company_Nm : Company_Nm);
            this.ctry = (!Ctry_Code.equals("") ? new Country(cpnyName, Ctry_Code) : this.ctry);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_DISTBN_COMPANY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteSubmCompany(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "SubmCompany.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Subm_Company(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Company_Nm", this.Company_Nm);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Company_Nm = "";
            this.ctry = null;
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_DCOMP") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String submCoName, String cntryName, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "SubmCompany.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_SCo(?,?,?,?,?)}");
            cStmt.setString("p_Name", submCoName);
            cStmt.setString("p_Ctry", cntryName);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_SCo", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_SCo");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repSubCompany.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Submission Company not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SubmCompany.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getCompany_Nm() {
        return Company_Nm;
    }

    public Country getCtry() {
        return ctry;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Company_Nm = null;
        this.ctry = null;
    }
}
