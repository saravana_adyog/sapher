/*
 * SumTab_RType1.java
 * Created on March 3, 2008
 * @author Arun P. Jose
 */
package bl.sapher;

import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SumTab_Label implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = -2184464216148509323L;
    private String Body_Sys_Num;
    private String Body_System;
    private String Diag_Code;
    private String Diag_Desc;
    private int Ul_Sev;
    private int L_Sev;
    private int Ul_Non_Sev;
    private int L_Non_Sev;
    private int Total;
    private static String Log_File_Name;

    public SumTab_Label(String logFilename) {
        this.Body_Sys_Num = "";
        this.Body_System = "";
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Ul_Sev = -1;
        this.L_Sev = -1;
        this.Ul_Non_Sev = -1;
        this.L_Non_Sev = -1;
        this.Total = -1;
        SumTab_Label.Log_File_Name = logFilename;
    }

    public SumTab_Label(String Body_Sys_Num, String Body_System, String Diag_Code, String Diag_Desc,
            int Ul_Sev, int L_Sev, int Ul_Non_Sev, int L_Non_Sev, int Total) {
        this.Body_Sys_Num = Body_Sys_Num;
        this.Body_System = Body_System;
        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Ul_Sev = Ul_Sev;
        this.L_Sev = L_Sev;
        this.Ul_Non_Sev = Ul_Non_Sev;
        this.L_Non_Sev = L_Non_Sev;
        this.Total = Total;
    }

    public static ArrayList listSumTab_Label(String cpnyName, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SumTab_Label.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);

        ArrayList<SumTab_Label> lstSumTabLbl = new ArrayList<SumTab_Label>();
        SumTab_Label str = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Sumtab_Label(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curSumtabL", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsSTRLbl = (ResultSet) cStmt.getObject("p_curSumtabL");
            while (rsSTRLbl.next()) {
                str = new SumTab_Label(rsSTRLbl.getString("Body_Sys_Num"), rsSTRLbl.getString("Body_System"),
                        rsSTRLbl.getString("Diag_Code"), rsSTRLbl.getString("Diag_Desc"),
                        rsSTRLbl.getInt("Ul_Sev"), rsSTRLbl.getInt("L_Sev"),
                        rsSTRLbl.getInt("Ul_Non_Sev"), rsSTRLbl.getInt("L_Non_Sev"),
                        rsSTRLbl.getInt("Total"));
                lstSumTabLbl.add(str);
            }
            rsSTRLbl.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("SumTab_Label records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstSumTabLbl;
    }

    public static ResultSet listSumTab_Label(DBCon dbCon, String Prod_Code, String From_Date, String To_Date, int Is_Active)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "SumTab_Label.java; Inside listing function");
        
        ResultSet rsSTRLbl = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Sumtab_Label(?,?,?,?,?)}");
            cStmt.setString("p_Product", Prod_Code);
            cStmt.setString("p_Frm_Date", From_Date);
            cStmt.setString("p_To_Date", To_Date);
            cStmt.setInt("p_Status", Is_Active);
            cStmt.registerOutParameter("p_curSumtabL", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            rsSTRLbl = (ResultSet) cStmt.getObject("p_curSumtabL");
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("SumTab_Label records not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "SumTab_Label.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return rsSTRLbl;
    }

    public String getBody_Sys_Num() {
        return Body_Sys_Num;
    }

    public String getBody_System() {
        return Body_System;
    }

    public String getDiag_Code() {
        return Diag_Code;
    }

    public String getDiag_Desc() {
        return Diag_Desc;
    }

    public int getUl_Sev() {
        return Ul_Sev;
    }

    public int getL_Sev() {
        return L_Sev;
    }

    public int getUl_Non_Sev() {
        return Ul_Non_Sev;
    }

    public int getL_Non_Sev() {
        return L_Non_Sev;
    }

    public int getTotal() {
        return Total;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Body_Sys_Num = null;
        this.Body_System = null;
        this.Diag_Code = null;
        this.Diag_Desc = null;
        this.Ul_Sev = 0;
        this.L_Sev = 0;
        this.Ul_Non_Sev = 0;
        this.L_Non_Sev = 0;
        this.Total = 0;
    }
}
