/*
 * CDL20
 * Manufacturer.java
 * Created on November 28, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class ReporterStatus implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = -8896120681819928809L;
    private String Status_Code;
    private String Status_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public ReporterStatus() {
        this.Status_Code = "";
        this.Status_Desc = "";
        this.Is_Valid = -1;
    }

    public ReporterStatus(String logFilename) {
        this.Status_Code = "";
        this.Status_Desc = "";
        this.Is_Valid = -1;
        ReporterStatus.Log_File_Name = logFilename;
    }

    public ReporterStatus(String Status_Code, String Status_Desc, int Is_Valid) {
        this.Status_Code = Status_Code;
        this.Status_Desc = Status_Desc;
        this.Is_Valid = Is_Valid;
    }

    public ReporterStatus(String cpnyName, String Status_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);

        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepStat(?,?,?,?,?)}");
            cStmt.setString("p_Code", Status_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_RStat", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsReporterStatus = (ResultSet) cStmt.getObject("cur_RStat");
            rsReporterStatus.next();
            this.Status_Code = Status_Code;
            this.Status_Desc = rsReporterStatus.getString("Status_Desc");
            this.Is_Valid = rsReporterStatus.getInt("Is_Valid");
            rsReporterStatus.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Reporter Status not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<ReporterStatus> listReporterStatus(int SearchType, String cpnyName, String Status_Code, String Status_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "ReporterStatus.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<ReporterStatus> lstReporterStatus = new ArrayList<ReporterStatus>();
        ReporterStatus repstat = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepStat(?,?,?,?,?)}");
            cStmt.setString("p_Code", Status_Code);
            cStmt.setString("p_Desc", Status_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_RStat", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsReporterStatus = (ResultSet) cStmt.getObject("cur_RStat");
            while (rsReporterStatus.next()) {
                repstat = new ReporterStatus(rsReporterStatus.getString("Status_Code"), rsReporterStatus.getString("Status_Desc"), rsReporterStatus.getInt("Is_Valid"));
                lstReporterStatus.add(repstat);
            }
            rsReporterStatus.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Reporter Status not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstReporterStatus;
    }

    public void saveReporterStatus(String cpnyName, String Save_Flag, String Username, String Status_Code, String Status_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "ReporterStatus.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Reporter_Stat(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Status_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Status_Code : Status_Code));
            cStmt.setString("p_Status_Desc", Status_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Status_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Status_Code : Status_Code);
            this.Status_Desc = (Status_Desc != "" ? Status_Desc : this.Status_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_REPORTER_STAT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_REP_STATUS") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteReporterStatus(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "ReporterStatus.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Reporter_Stat(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Status_Code", this.Status_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Status_Code = "";
            this.Status_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_REPSTAT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "ReporterStatus.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_RepStat(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_RStat", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_RStat");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repRepStat.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Reporter status not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "ReporterStatus.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getStatus_Code() {
        return Status_Code;
    }

    public String getStatus_Desc() {
        return Status_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Status_Code = null;
        this.Status_Desc = null;
    }
}
