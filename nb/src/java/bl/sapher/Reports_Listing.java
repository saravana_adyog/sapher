package bl.sapher;

import bl.sapher.QueryTool;
import bl.sapher.SumTab_RType;
import bl.sapher.general.ClientConf;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author Anoop Varma, Focal3 LLC.
 */
public class Reports_Listing {

    public static final int REP_CIOMS_2 = 1;
    public static final int REP_ICH = 2;
    public static final int REP_SUMTAB_SERIOUSNESS = 3;
    public static final int REP_SUMTAB_REPTYPE = 4;

    /** Log file name */
    private static String Log_File_Name;

    Reports_Listing(String logFilename) {
        Reports_Listing.Log_File_Name = logFilename;
    }

    public static String printPdf(int repType, String cpnyName, String path,
            String prodCod, String pdodDesc, String fromDate, String toDate, int nStatus)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "Reports_Listing.java; Inside printPdf()");
        String strPdfFileName = null;
        ResultSet rs = null;
        DBCon dbCon = null;
        try {
            dbCon = new DBCon(cpnyName);

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("SAPHER_PARAM_PRODUCT_DESC", pdodDesc); // GEN_NAME
            param.put("SAPHER_PARAM_FROM_DATE", fromDate); // FROM_DATE
            param.put("SAPHER_PARAM_TO_DATE", toDate); // TO_DATE
            param.put("SAPHER_PARAM_APP_VER", (new GenConf().getSysVersion()));

            switch (repType) {
                case REP_CIOMS_2:
                    rs = CIOMS2.listCIOMS2(dbCon, prodCod, fromDate, toDate, nStatus);
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repCIOMS2.jrxml", rs, param);
                    break;
                case REP_ICH:
                    rs = ICH.listICH(dbCon, prodCod, fromDate, toDate, nStatus);
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repICH.jrxml", rs, param);
                    break;
                case REP_SUMTAB_SERIOUSNESS:
                    rs = SumTab_Label.listSumTab_Label(dbCon, prodCod, fromDate, toDate, nStatus);
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repSumTab-S.jrxml", rs, param);
                    break;
                case REP_SUMTAB_REPTYPE:
                    rs = SumTab_RType.listSumTab_RType(dbCon, prodCod, fromDate, toDate, nStatus);
                    strPdfFileName = ReportEngine.exportAsPdf(path, "repSumTab-RT.jrxml", rs, param);
                    break;
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Reports_Listing.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Reports_Listing.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        } finally {
            try {
                rs.close();
                dbCon.closeCon();
            } catch (SQLException ex) {
                Logger.writeLog(Log_File_Name, "Reports_Listing.java; Caught SQLException-" + ex);
            }
        }
        return strPdfFileName;
    }
}
