/*
 * AELLT.java
 * Created on November 26, 2007
 * @author Arun P. Jose
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class AELLT implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 1806989337432971727L;
    private String Llt_Code;
    private String Llt_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public AELLT() {
        this.Llt_Code = "";
        this.Llt_Desc = "";
        this.Is_Valid = -1;
    }

    public AELLT(String logFilename) {
        this.Llt_Code = "";
        this.Llt_Desc = "";
        this.Is_Valid = -1;
        AELLT.Log_File_Name = logFilename;
    }

    public AELLT(String Llt_Code, String Llt_Desc, int Is_Valid) {
        this.Llt_Code = Llt_Code;
        this.Llt_Desc = Llt_Desc;
        this.Is_Valid = Is_Valid;
    }

    public AELLT(String cpnyName, String Llt_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Inside Constructor with code:" + Llt_Code);
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_LLT(?,?,?,?,?)}");
            cStmt.setString("p_Code", Llt_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_LLT", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsLLT = (ResultSet) cStmt.getObject("cur_LLT");
            rsLLT.next();
            this.Llt_Code = Llt_Code;
            this.Llt_Desc = rsLLT.getString("LLT_Desc");
            this.Is_Valid = rsLLT.getInt("Is_Valid");
            rsLLT.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException-" + ex);
                ex.printStackTrace();
            }
            throw new RecordNotFoundException("AE Low level term not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<AELLT> listLLT(int SearchType, String cpnyName, String Llt_Code, String Llt_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "AELLT.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<AELLT> lstLLT = new ArrayList<AELLT>();
        AELLT aeLLT = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_LLT(?,?,?,?,?)}");
            cStmt.setString("p_Code", Llt_Code);
            cStmt.setString("p_Desc", Llt_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_LLT", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsLLT = (ResultSet) cStmt.getObject("cur_LLT");
            while (rsLLT.next()) {
                aeLLT = new AELLT(rsLLT.getString("LLT_Code"), rsLLT.getString("LLT_Desc"),
                        rsLLT.getInt("Is_Valid"));
                lstLLT.add(aeLLT);
            }
            rsLLT.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new RecordNotFoundException("AE LLT not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
        return lstLLT;
    }

    public void saveLLT(String cpnyName, String Save_Flag, String Username, String Llt_Code, String Llt_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AELLT.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_AE_LLT(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_LLT_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Llt_Code : Llt_Code));
            cStmt.setString("p_LLT_Desc", Llt_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Llt_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Llt_Code : Llt_Code);
            this.Llt_Desc = (Llt_Desc != "" ? Llt_Desc : this.Llt_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException (RollBack)-" + ex);
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_AE_LLT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_AE_LLT") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteLLT(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AELLT.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_AE_LLT(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_LLT_Code", this.Llt_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Llt_Code = "";
            this.Llt_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException (RollBack)-" + ex);
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AE_LLT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "AELLT.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_LLT(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_LLT", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_LLT");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repLlt.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "AELLTAELLT.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new RecordNotFoundException("AELLT not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getLlt_Code() {
        return Llt_Code;
    }

    public String getLlt_Desc() {
        return Llt_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Llt_Code = null;
        this.Llt_Desc = null;
    }
}
