/*
 * AUD2
 * Invcols.java
 * Created on January 28, 2008
 * @author Jaikishan. S
 */
package bl.sapher;

import java.sql.CallableStatement;
import bl.sapher.general.ConstraintViolationException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

public class InventoryCols {

    private String Inv_Col_Id;
    private Inventory inv;
    private String Field_Name;
    private int Audit_Flag;
    private static String Log_File_Name;

    public InventoryCols() {
        this.Inv_Col_Id = "";
        this.inv = null;
        this.Field_Name = "";
        this.Audit_Flag = -1;
    }

    public InventoryCols(String logFilename) {
        this.Inv_Col_Id = "";
        this.inv = null;
        this.Field_Name = "";
        this.Audit_Flag = -1;
        InventoryCols.Log_File_Name = logFilename;
    }

    public InventoryCols(String Inv_Col_Id, Inventory inv, String Field_Name, int Audit_Flag)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        this.Inv_Col_Id = Inv_Col_Id;
        this.inv = inv;
        this.Field_Name = Field_Name;
        this.Audit_Flag = Audit_Flag;
    }

    public InventoryCols(String cpnyName, String Inv_Col_Id)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Inventory_Cols(?,?,?,?)}");
            cStmt.setString("p_InvId", "");
            cStmt.setString("p_InvCol", Inv_Col_Id);
            cStmt.setString("p_Field", "");
            cStmt.registerOutParameter("cur_Inv", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsinvcol = (ResultSet) cStmt.getObject("cur_Inv");
            rsinvcol.next();
            this.Inv_Col_Id = Inv_Col_Id;
            this.inv = new Inventory(rsinvcol.getString("i_Inv_Id"), rsinvcol.getString("i_Inv_Desc"),
                    rsinvcol.getString("i_Inv_Type"), rsinvcol.getInt("i_Ins_Audit_Flag"), rsinvcol.getInt("i_Del_Audit_Flag"));
            this.Field_Name = rsinvcol.getString("ic_Field_Name");
            this.Audit_Flag = rsinvcol.getInt("ic_Audit_Flag");
            rsinvcol.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Inventory Col not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList listInvcol(String cpnyName, String Inv_Col_Id, String Inv_Id, String Field_Name)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "InventoryCols.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<InventoryCols> lstInvcol = new ArrayList<InventoryCols>();
        InventoryCols ic = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Inventory_Cols(?,?,?,?)}");
            cStmt.setString("p_InvId", Inv_Id);
            cStmt.setString("p_InvCol", Inv_Col_Id);
            cStmt.setString("p_Field", Field_Name);
            cStmt.registerOutParameter("cur_Inv", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsinvcol = (ResultSet) cStmt.getObject("cur_Inv");
            while (rsinvcol.next()) {

                ic = new InventoryCols(rsinvcol.getString("ic_Inv_Col_Id"), new Inventory(rsinvcol.getString("i_Inv_Id"),
                        rsinvcol.getString("i_Inv_Desc"), rsinvcol.getString("i_Inv_Type"), rsinvcol.getInt("i_Ins_Audit_Flag"), rsinvcol.getInt("i_Del_Audit_Flag")),
                        rsinvcol.getString("ic_Field_Name"),
                        rsinvcol.getInt("ic_Audit_Flag"));
                lstInvcol.add(ic);
            }
            rsinvcol.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Inv Col not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstInvcol;
    }

    public void UpdateInvcol(String cpnyName, String Inv_Col_Id, int Audit_Flag)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Update_Inv_Col(?,?)}");
            cStmt.setString("p_Inv_Col_Id", Inv_Col_Id);
            cStmt.setInt("p_Audit_Flag", Audit_Flag);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Audit_Flag = (Audit_Flag != -1 ? Audit_Flag : this.Audit_Flag);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "InventoryCols.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public String getInv_Col_Id() {
        return Inv_Col_Id;
    }

    public Inventory getInv_Id() {
        return inv;
    }

    public String getField_Name() {
        return Field_Name;
    }

    public int getAudit_Flag() {
        return Audit_Flag;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Inv_Col_Id = null;
        this.Field_Name = null;
        this.inv = null;
    }
}