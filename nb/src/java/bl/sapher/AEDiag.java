/*
 * CDL3
 * AEDiag.java
 * Created on November 28, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class AEDiag implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 6373518310027050561L;
    private String Diag_Code;
    private String Diag_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public AEDiag() {
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Is_Valid = -1;
    }

    public AEDiag(String logFilename) {
        this.Diag_Code = "";
        this.Diag_Desc = "";
        this.Is_Valid = -1;
        AEDiag.Log_File_Name = logFilename;
    }

    public AEDiag(String Diag_Code, String Diag_Desc, int Is_Valid) {
        this.Diag_Code = Diag_Code;
        this.Diag_Desc = Diag_Desc;
        this.Is_Valid = Is_Valid;
    }

    public AEDiag(String cpnyName, String Diag_Code)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        DBCon dbCon = new DBCon(cpnyName);
        Logger.writeLog(Log_File_Name, "AEDiag.java; Inside Constructor with code:" + Diag_Code);
        try {
            Logger.writeLog(Log_File_Name, "AEDiag.java; invoking sapher_pkg.Get_Diag(?,?,?,?,?)");
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Diag(?,?,?,?,?)}");
            cStmt.setString("p_Code", Diag_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Diag", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDiag = (ResultSet) cStmt.getObject("cur_Diag");
            rsDiag.next();
            this.Diag_Code = Diag_Code;
            this.Diag_Desc = rsDiag.getString("Diag_Desc");
            this.Is_Valid = rsDiag.getInt("Is_Valid");
            rsDiag.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing RecordNotFoundException");
            }
            throw new RecordNotFoundException("AE Preferred term not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught RecordNotFoundException-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing GenericException");
            }
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<AEDiag> listDiag(int SearchType, String cpnyName, String Diag_Code, String Diag_Desc, int Is_Valid)
            throws DBConnectionException, GenericException, RecordNotFoundException {
        Logger.writeLog(Log_File_Name, "AEDiag.java; Inside listDiag()");
        DBCon dbCon = new DBCon(cpnyName);
        //int ctr = 0;
        ArrayList<AEDiag> lstDiag = new ArrayList<AEDiag>();
        AEDiag aeDiag = null;
        try {
            Logger.writeLog(Log_File_Name, "AEDiag.java; invoking sapher_pkg.Get_Diag(?,?,?,?,?)");
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Diag(?,?,?,?,?)}");
            cStmt.setString("p_Code", Diag_Code);
            cStmt.setString("p_Desc", Diag_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_Diag", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDiag = (ResultSet) cStmt.getObject("cur_Diag");
            while (rsDiag.next()) {
                aeDiag = new AEDiag(rsDiag.getString("Diag_Code"), rsDiag.getString("Diag_Desc"),
                        rsDiag.getInt("Is_Valid"));
                lstDiag.add(aeDiag);
            }
            rsDiag.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing RecordNotFoundException");
            }
            throw new RecordNotFoundException("AE Prefered Term not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught Exception-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing GenericException");
            }
            throw new GenericException(ex.getMessage());
        }
        return lstDiag;
    }

    public void saveDiag(String cpnyName, String Save_Flag, String Username, String Diag_Code, String Diag_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {

        Logger.writeLog(Log_File_Name, "AEDiag.java; Inside saveDiag()");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            Logger.writeLog(Log_File_Name, "AEDiag.java; invoking sapher_pkg.Save_AE_Diag(?,?,?,?,?)");
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_AE_Diag(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Diag_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Diag_Code : Diag_Code));
            cStmt.setString("p_AE_Diag_Desc", Diag_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Diag_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Diag_Code : Diag_Code);
            this.Diag_Desc = (Diag_Desc != "" ? Diag_Desc : this.Diag_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException-" + ex);
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException (RollBack)-" + ex);
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_AE_DIAG") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_AE_DIAG") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught Exception-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing GenericException");
            }
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteDiag(String cpnyName, String Username)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "AEDiag.java; Inside deleteDiag()");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            Logger.writeLog(Log_File_Name, "AEDiag.java; invoking sapher_pkg.Delete_AE_Diag(?,?), code:" + this.Diag_Code);
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_AE_Diag(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Diag_Code", this.Diag_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Diag_Code = "";
            this.Diag_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException-" + ex);
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException (RollBack)-" + ex);
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing DBConnectionException");
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_AE_DIAG") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught Exception-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing GenericException");
            }
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "AEDiag.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Diag(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Diag", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_Diag");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repDiag.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught SQLException-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing RecordNotFoundException");
            }
            throw new RecordNotFoundException("Preferred term not found!");
        } catch (Exception ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                Logger.writeLog(Log_File_Name, "AEDiag.java; Caught Exception-" + ex);
                ex.printStackTrace();
                Logger.writeLog(Log_File_Name, "AEDiag.java; Throwing GenericException");
            }
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getDiag_Code() {
        return Diag_Code;
    }

    public String getDiag_Desc() {
        return Diag_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Diag_Code = null;
        this.Diag_Desc = null;
    }
}
