/*
 * CDL8
 * DoseFrequency.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class DoseFrequency implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 1763575219317969277L;
    private String Dfreq_Code;
    private String Dfreq_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public DoseFrequency() {
        this.Dfreq_Code = "";
        this.Dfreq_Desc = "";
        this.Is_Valid = -1;
    }

    public DoseFrequency(String logFilename) {
        this.Dfreq_Code = "";
        this.Dfreq_Desc = "";
        this.Is_Valid = -1;
        DoseFrequency.Log_File_Name = logFilename;
    }

    public DoseFrequency(String Dfreq_Code, String Dfreq_Desc, int Is_Valid) {
        this.Dfreq_Code = Dfreq_Code;
        this.Dfreq_Desc = Dfreq_Desc;
        this.Is_Valid = Is_Valid;
    }

    public DoseFrequency(String cpnyName, String Dfreq_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DFreq(?,?,?,?,?)}");
            cStmt.setString("p_Code", Dfreq_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_DFreq", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDoseFrequency = (ResultSet) cStmt.getObject("cur_DFreq");
            rsDoseFrequency.next();
            this.Dfreq_Code = Dfreq_Code;
            this.Dfreq_Desc = rsDoseFrequency.getString("Dfreq_Desc");
            this.Is_Valid = rsDoseFrequency.getInt("Is_Valid");
            rsDoseFrequency.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose Frequency not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<DoseFrequency> listDoseFrequency(int SearchType, String cpnyName, String Dfreq_Code, String Dfreq_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "DoseFrequency.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<DoseFrequency> lstDoseFrequency = new ArrayList<DoseFrequency>();
        DoseFrequency dosefreq = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DFreq(?,?,?,?,?)}");
            cStmt.setString("p_Code", Dfreq_Code);
            cStmt.setString("p_Desc", Dfreq_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_DFreq", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDoseFrequency = (ResultSet) cStmt.getObject("cur_DFreq");
            while (rsDoseFrequency.next()) {
                dosefreq = new DoseFrequency(rsDoseFrequency.getString("Dfreq_Code"),
                        rsDoseFrequency.getString("Dfreq_Desc"),
                        rsDoseFrequency.getInt("Is_Valid"));
                lstDoseFrequency.add(dosefreq);
            }
            rsDoseFrequency.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose Frequency not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstDoseFrequency;
    }

    public void saveDoseFrequency(String cpnyName, String Save_Flag, String Username,
            String Dfreq_Code, String Dfreq_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "DoseFrequency.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Dose_Frequency(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Dfreq_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Dfreq_Code : Dfreq_Code));
            cStmt.setString("p_Dfreq_Desc", Dfreq_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Dfreq_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Dfreq_Code : Dfreq_Code);
            this.Dfreq_Desc = (!Dfreq_Desc.equals("") ? Dfreq_Desc : this.Dfreq_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            if (ex.getMessage().indexOf("PK_DOSE_FREQUENCY") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_DOSE_FREQUENCY") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteDoseFrequency(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "DoseFrequency.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Dose_Frequency(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Dfreq_Code", this.Dfreq_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Dfreq_Code = "";
            this.Dfreq_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_DFREQ") != -1) {
                throw new ConstraintViolationException("1");
            }
            if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "DoseFrequency.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DFreq(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_DFreq", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_DFreq");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repDoseFreq.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose frequency not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseFrequency.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getDfreq_Code() {
        return this.Dfreq_Code;
    }

    public String getDfreq_Desc() {
        return this.Dfreq_Desc;
    }

    public int getIs_Valid() {
        return this.Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Dfreq_Code = null;
        this.Dfreq_Desc = null;
    }
}
