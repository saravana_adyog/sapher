/*
 * AUD2
 * Inventory.java
 * Created on November 22, 2007
 * @author Arun P. Jose, Jaikishan. S, Anoop Varma
 */
package bl.sapher;

import bl.sapher.general.ConstraintViolationException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;

public class Inventory {

    private String Inv_Id;
    private String Inv_Desc;
    private String Inv_Type;
    private int nInsAuditFlag;
    private int nDelAuditFlag;
    private static String Log_File_Name;

    public Inventory() {
        this.Inv_Id = "";
        this.Inv_Desc = "";
        this.Inv_Type = "";
        this.nInsAuditFlag = -1;
        this.nDelAuditFlag = -1;
    }

    public Inventory(String Inv_Id, String Inv_Desc, String Inv_Type, int nInsAuditFlag, int nDelAuditFlag) {
        this.Inv_Id = Inv_Id;
        this.Inv_Desc = Inv_Desc;
        this.Inv_Type = Inv_Type;
        this.nInsAuditFlag = nInsAuditFlag;
        this.nDelAuditFlag = nDelAuditFlag;
    }

    public Inventory(String LogFilename, String cpnyName, String Inv_Id)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Inventory.Log_File_Name = LogFilename;

        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Inventory(?,?,?,?,?)}");
            cStmt.setString("p_InvId", Inv_Id);
            cStmt.setString("p_InvDesc", "");
            cStmt.setString("p_InvType", "");
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_Inv", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsInv = (ResultSet) cStmt.getObject("cur_Inv");
            rsInv.next();
            this.Inv_Id = Inv_Id;
            this.Inv_Desc = rsInv.getString("Inv_Desc");
            this.Inv_Type = rsInv.getString("Inv_Type");
            this.nInsAuditFlag = rsInv.getInt("Ins_Audit_Flag");
            this.nDelAuditFlag = rsInv.getInt("Del_Audit_Flag");
            rsInv.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Inventory.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Inventory.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Inventory not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Inventory.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Inventory.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<Inventory> listInventory(int SearchType, String cpnyName, String Inv_Id, String Inv_Desc, String Inv_Type)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "Inventory.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<Inventory> lstInv = new ArrayList<Inventory>();
        Inventory inv = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_Inventory(?,?,?,?,?)}");
            cStmt.setString("p_InvId", Inv_Id);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.setString("p_InvDesc", Inv_Desc);
            cStmt.setString("p_InvType", Inv_Type);
            cStmt.registerOutParameter("cur_Inv", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsInv = (ResultSet) cStmt.getObject("cur_Inv");
            while (rsInv.next()) {
                inv = new Inventory(rsInv.getString("Inv_Id"), rsInv.getString("Inv_Desc"), rsInv.getString("Inv_Type"), rsInv.getInt("Ins_Audit_Flag"), rsInv.getInt("Del_Audit_Flag"));
                lstInv.add(inv);
            }
            rsInv.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Inventory.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Inventory.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Inventory not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Inventory.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Inventory.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstInv;
    }

    public void UpdateInventory(String cpnyName, String Inv_Id, int Ins_Audit_Flag, int Del_Audit_Flag)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "Inventory.java; Inside save function [UpdateInventory()]");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Inventory(?,?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", "U");
            cStmt.setString("p_inv_id", Inv_Id);
            cStmt.setString("p_inv_desc", "");
            cStmt.setString("p_inv_type", "");
            cStmt.setInt("p_ins_audit_flag", Ins_Audit_Flag);
            cStmt.setInt("p_del_audit_flag", Del_Audit_Flag);

            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.nInsAuditFlag = (Ins_Audit_Flag != -1 ? Ins_Audit_Flag : this.nInsAuditFlag);
            this.nDelAuditFlag = (Del_Audit_Flag != -1 ? Del_Audit_Flag : this.nDelAuditFlag);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "Inventory.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            Logger.writeLog(Log_File_Name, "Inventory.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "Inventory.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "Inventory.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public String getInv_Id() {
        return Inv_Id;
    }

    public String getInv_Desc() {
        return Inv_Desc;
    }

    public String getInv_Type() {
        return Inv_Type;
    }

    public int getInsAuditFlag() {
        return nInsAuditFlag;
    }

    public int getDelAuditFlag() {
        return nDelAuditFlag;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Inv_Desc = null;
        this.Inv_Id = null;
        this.Inv_Type = null;
    }
}