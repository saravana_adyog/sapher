/*
 * CDL10
 * DoseUnits.java
 * Created on November 29, 2007
 * @author Jaikishan. S
 */
package bl.sapher;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bl.sapher.general.ConstraintViolationException;
import bl.sapher.general.DBCon;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenConf;
import bl.sapher.general.GenericException;
import bl.sapher.general.Logger;
import bl.sapher.general.RecordNotFoundException;
import bl.sapher.general.ReportEngine;
import java.util.HashMap;

public class DoseUnits implements Serializable {

    /** Serialization version UID */
    private static final long serialVersionUID = 724295616997894372L;
    private String Unit_Code;
    private String Unit_Desc;
    private int Is_Valid;
    private static String Log_File_Name;

    public DoseUnits() {
        this.Unit_Code = "";
        this.Unit_Desc = "";
        this.Is_Valid = -1;
    }

    public DoseUnits(String logFilename) {
        this.Unit_Code = "";
        this.Unit_Desc = "";
        this.Is_Valid = -1;
        DoseUnits.Log_File_Name = logFilename;
    }

    public DoseUnits(String Unit_Code, String Unit_Desc, int Is_Valid) {
        this.Unit_Code = Unit_Code;
        this.Unit_Desc = Unit_Desc;
        this.Is_Valid = Is_Valid;
    }

    public DoseUnits(String cpnyName, String Unit_Code)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DUnit(?,?,?,?,?)}");
            cStmt.setString("p_Code", Unit_Code);
            cStmt.setString("p_Desc", "");
            cStmt.setInt("p_Active", -1);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_DUnit", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDoseUnits = (ResultSet) cStmt.getObject("cur_DUnit");
            rsDoseUnits.next();
            this.Unit_Code = Unit_Code;
            this.Unit_Desc = rsDoseUnits.getString("Unit_Desc");
            this.Is_Valid = rsDoseUnits.getInt("Is_Valid");
            rsDoseUnits.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose Unit not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static ArrayList<DoseUnits> listDoseUnits(int SearchType, String cpnyName, String Unit_Code, String Unit_Desc, int Is_Valid)
            throws RecordNotFoundException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "DoseUnits.java; Inside listing function");
        DBCon dbCon = new DBCon(cpnyName);
        int ctr = 0;
        ArrayList<DoseUnits> lstDoseUnits = new ArrayList<DoseUnits>();
        DoseUnits doseunits = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DUnit(?,?,?,?,?)}");
            cStmt.setString("p_Code", Unit_Code);
            cStmt.setString("p_Desc", Unit_Desc);
            cStmt.setInt("p_Active", Is_Valid);
            cStmt.setInt("p_Search_Type", SearchType);
            cStmt.registerOutParameter("cur_DUnit", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rsDoseUnits = (ResultSet) cStmt.getObject("cur_DUnit");
            while (rsDoseUnits.next()) {
                doseunits = new DoseUnits(rsDoseUnits.getString("Unit_Code"),
                        rsDoseUnits.getString("Unit_Desc"),
                        rsDoseUnits.getInt("Is_Valid"));
                lstDoseUnits.add(doseunits);
            }
            rsDoseUnits.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose Unit not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return lstDoseUnits;
    }

    public void saveDoseUnits(String cpnyName, String Save_Flag, String Username,
            String Unit_Code, String Unit_Desc, int Is_Valid)
            throws ConstraintViolationException, DBConnectionException, GenericException {
        Logger.writeLog(Log_File_Name, "DoseUnits.java; Inside save function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Save_Dose_Units(?,?,?,?,?)}");
            cStmt.setString("p_Save_Flag", Save_Flag);
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Unit_Code",
                    (Save_Flag.equalsIgnoreCase("U") ? this.Unit_Code : Unit_Code));
            cStmt.setString("p_Unit_Desc", Unit_Desc);
            cStmt.setInt("p_Is_Valid", Is_Valid);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Unit_Code = (Save_Flag.equalsIgnoreCase("U") ? this.Unit_Code : Unit_Code);
            this.Unit_Desc = (!Unit_Desc.equals("") ? Unit_Desc : this.Unit_Desc);
            this.Is_Valid = (Is_Valid != -1 ? Is_Valid : this.Is_Valid);
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("PK_DOSE_UNITS") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("UK_DOSE_UNITS") != -1) {
                throw new ConstraintViolationException("2");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("3");
            } else {
                Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public void deleteDoseUnits(String cpnyName, String Username)
            throws DBConnectionException, GenericException, ConstraintViolationException {
        Logger.writeLog(Log_File_Name, "DoseUnits.java; Inside delete function");
        DBCon dbCon = new DBCon(cpnyName);
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Delete_Dose_Units(?,?)}");
            cStmt.setString("p_Username", Username);
            cStmt.setString("p_Unit_Code", this.Unit_Code);
            cStmt.execute();
            dbCon.getDbCon().commit();
            cStmt.close();
            dbCon.closeCon();
            this.Unit_Code = "";
            this.Unit_Desc = "";
            this.Is_Valid = -1;
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            try {
                dbCon.getDbCon().rollback();
            } catch (SQLException dbex) {
                throw new DBConnectionException(dbex.getMessage());
            }
            if (ex.getMessage().indexOf("FK_CASEDETAILS_DOSEUNIT") != -1) {
                throw new ConstraintViolationException("1");
            } else if (ex.getMessage().indexOf("FK_AUDITTRIAL_USERLIST") != -1) {
                throw new ConstraintViolationException("2");
            } else {
                Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
                throw new GenericException(ex.getMessage());
            }
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
    }

    public static String printPdf(String cpnyName, String path, String code, String desc, int valid)
            throws RecordNotFoundException, GenericException, DBConnectionException {
        Logger.writeLog(Log_File_Name, "DoseUnits.java; Inside printPdf()");
        DBCon dbCon = new DBCon(cpnyName);
        String strPdfFileName = null;
        try {
            CallableStatement cStmt = dbCon.getDbCon().prepareCall(
                    "{call sapher_pkg.Get_DUnit(?,?,?,?,?)}");
            cStmt.setString("p_Code", code);
            cStmt.setString("p_Desc", desc);
            cStmt.setInt("p_Active", valid);
            cStmt.setInt("p_Search_Type", bl.sapher.general.Constants.SEARCH_STRICT);
            cStmt.registerOutParameter("cur_DUnit", oracle.jdbc.OracleTypes.CURSOR);
            cStmt.execute();
            ResultSet rs = (ResultSet) cStmt.getObject("cur_DUnit");

            HashMap<String, String> param = new HashMap<String, String>();
            param.put("STRBUILD", (new GenConf()).getSysVersion());

            strPdfFileName = ReportEngine.exportAsPdf(path, "repDoseUnits.jrxml", rs, param);

            rs.close();
            cStmt.close();
            dbCon.closeCon();
        } catch (SQLException ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught SQLException-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing RecordNotFoundException");
            throw new RecordNotFoundException("Dose unit not found!");
        } catch (Exception ex) {
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Caught Exception-" + ex);
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
            }
            Logger.writeLog(Log_File_Name, "DoseUnits.java; Throwing GenericException");
            throw new GenericException(ex.getMessage());
        }
        return strPdfFileName;
    }

    public String getUnit_Code() {
        return Unit_Code;
    }

    public String getUnit_Desc() {
        return Unit_Desc;
    }

    public int getIs_Valid() {
        return Is_Valid;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.Unit_Code = null;
        this.Unit_Desc = null;
    }
}
