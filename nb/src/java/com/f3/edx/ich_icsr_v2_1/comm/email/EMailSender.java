package com.f3.edx.ich_icsr_v2_1.comm.email;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * This class provides feature to send the created XML file to send to a
 * specific e-mail address.
 *
 * @author Anoop Varma
 */
public class EMailSender {

    String strHost = ""; //smtp.gmail.com
    String strFrom = "";
    String strTo = "";
    int nSmtpPort = 25;
    String strDebugMode = "true";
    String strAttachmentFileName = "";
    private String strSubject;
    String strMessageBody = "";

    /**
     *
     * @param strHost               as <code>String</code>
     * @param strFrom               as <code>String</code>
     * @param strTo                 as <code>String</code>
     * @param nSmtpPort             as <code>String</code>
     * @param strDebugMode          as <code>String</code>
     * @param strAttachmentFileName as <code>String</code>
     * @param strSubject            as <code>String</code>
     * @param strMessageBody        as <code>String</code>
     */
    public EMailSender(String strHost,
            String strFrom,
            String strTo,
            int nSmtpPort,
            String strDebugMode,
            String strAttachmentFileName,
            String strSubject,
            String strMessageBody) {
        this.strHost = strHost;
        this.strFrom = strFrom;
        this.strTo = strTo;
        this.nSmtpPort = nSmtpPort;
        this.strDebugMode = strDebugMode;
        this.strAttachmentFileName = strAttachmentFileName;
        this.strSubject = strSubject;
        this.strMessageBody = strMessageBody;
    }

    /**
     *
     *
     * @return <code>true</code> if the e-mail sending was successful.
     *
     * @throws javax.mail.MessagingException
     * @throws java.io.IOException
     */
    public boolean send() throws MessagingException, IOException {

        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", strHost);
        prop.put("mail.smtp.port", "" + nSmtpPort);
        prop.put("mail.debug", strDebugMode);

        Session ssn = Session.getDefaultInstance(prop, null);

        MimeMessage msg = new MimeMessage(ssn);

        msg.setFrom(new InternetAddress(strFrom));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(strTo));

        msg.setSubject("XML report for submitting to EMEA.");

        MimeBodyPart mbpMsgBody = new MimeBodyPart();
        mbpMsgBody.setText(strMessageBody);

        MimeBodyPart mbpAttachment = new MimeBodyPart();
        //FileDataSource fds = new FileDataSource(AttachmentFileName);
        mbpAttachment.attachFile(new File(strAttachmentFileName));
        //mbpAttachment.setDataHandler(new DataHandler(fds));
        //mbpAttachment.setFileName(fds.getName());

        Multipart mp = new MimeMultipart();
        mp.addBodyPart(mbpMsgBody);
        mp.addBodyPart(mbpAttachment);

        msg.setContent(mp);

        Transport.send(msg);

        return true;
    }
}
