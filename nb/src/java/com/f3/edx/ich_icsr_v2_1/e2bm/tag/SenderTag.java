package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class SenderTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 1368194601026574752L;
    private int MAXLEN_SENDERTYPE = 1;
    private int MAXLEN_SENDERORGANIZATION = 60;
    private int MAXLEN_SENDERDEPARTMENT = 60;
    private int MAXLEN_SENDERTITLE = 10;
    private int MAXLEN_SENDERGIVENAME = 35;
    private int MAXLEN_SENDERMIDDLENAME = 15;
    private int MAXLEN_SENDERFAMILYNAME = 35;
    private int MAXLEN_SENDERSTREETADDRESS = 100;
    private int MAXLEN_SENDERCITY = 35;
    private int MAXLEN_SENDERSTATE = 40;
    private int MAXLEN_SENDERPOSTCODE = 15;
    private int MAXLEN_SENDERCOUNTRYCODE = 2;
    private int MAXLEN_SENDERTEL = 10;
    private int MAXLEN_SENDERTELEXTENSION = 5;
    private int MAXLEN_SENDERTELCOUNTRYCODE = 3;
    private int MAXLEN_SENDERFAX = 10;
    private int MAXLEN_SENDERFAXEXTENSION = 5;
    private int MAXLEN_SENDERFAXCOUNTRYCODE = 3;
    private int MAXLEN_SENDEREMAILADDRESS = 100;
    //////////////////////////////////////////
    private String strSenderType;
    private String strSenderOrganization;
    private String strSenderDepartment;
    private String strSenderTitle;
    private String strSenderGiveName;
    private String strSenderMiddleName;
    private String strSenderFamilyName;
    private String strSenderStreetAddress;
    private String strSenderCity;
    private String strSenderState;
    private String strSenderPostCode;
    private String strSenderCountryCode;
    private String strSenderTel;
    private String strSenderTelExtension;
    private String strSenderTelCountryCode;
    private String strSenderFax;
    private String strSenderFaxExtension;
    private String strSenderFaxcountryCode;
    private String strSenderEmailAddress;

    // Constructors
    /**
     * Default constructor that creates a new instance of SenderTag.
     * This constructor initializes its member variables to empty string.
     */
    public SenderTag() {
        this.strSenderType = "";
        this.strSenderOrganization = "";
        this.strSenderDepartment = "";
        this.strSenderTitle = "";
        this.strSenderGiveName = "";
        this.strSenderMiddleName = "";
        this.strSenderFamilyName = "";
        this.strSenderStreetAddress = "";
        this.strSenderCity = "";
        this.strSenderState = "";
        this.strSenderPostCode = "";
        this.strSenderCountryCode = "";
        this.strSenderTel = "";
        this.strSenderTelExtension = "";
        this.strSenderTelCountryCode = "";
        this.strSenderFax = "";
        this.strSenderFaxExtension = "";
        this.strSenderFaxcountryCode = "";
        this.strSenderEmailAddress = "";
    }

    /**
     * Constructor that creates a new instance of SenderTag.
     *
     * @param SenderType           <code><b>String</b></code> value to set &lt;sendertype&gt; tag.
     * @param SenderOrganization   <code><b>String</b></code> value to set &lt;senderorganization&gt; tag.
     * @param SenderDepartment     <code><b>String</b></code> value to set &lt;senderdepartment&gt; tag.
     * @param SenderTitle          <code><b>String</b></code> value to set &lt;sendertitle&gt; tag.
     * @param SenderGiveName       <code><b>String</b></code> value to set &lt;sendergivename&gt; tag.
     * @param SenderMiddleName     <code><b>String</b></code> value to set &lt;sendermiddlename&gt; tag.
     * @param SenderFamilyName     <code><b>String</b></code> value to set &lt;senderfamilyname&gt; tag.
     * @param SenderStreetAddress  <code><b>String</b></code> value to set &lt;senderstreetaddress&gt; tag.
     * @param SenderCity           <code><b>String</b></code> value to set &lt;sendercity&gt; tag.
     * @param SenderState          <code><b>String</b></code> value to set &lt;senderstate&gt; tag.
     * @param SenderPostCode       <code><b>String</b></code> value to set &lt;senderpostcode&gt; tag.
     * @param SenderCountryCode    <code><b>String</b></code> value to set &lt;sendercountrycode&gt; tag.
     * @param SenderTel            <code><b>String</b></code> value to set &lt;sendertel&gt; tag.
     * @param SenderTelExtension   <code><b>String</b></code> value to set &lt;sendertelextension&gt; tag.
     * @param SenderTelCountryCode <code><b>String</b></code> value to set &lt;sendertelcountrycode&gt; tag.
     * @param SenderFax            <code><b>String</b></code> value to set &lt;senderfax&gt; tag.
     * @param SenderFaxExtension   <code><b>String</b></code> value to set &lt;senderfaxextension&gt; tag.
     * @param SenderFaxcountryCode <code><b>String</b></code> value to set &lt;senderfaxcountrycode&gt; tag.
     * @param SenderEmailAddress   <code><b>String</b></code> value to set &lt;senderemailaddress&gt; tag.
     * @deprecated
     */
    @Deprecated
    public SenderTag(
            String SenderType,
            String SenderOrganization,
            String SenderDepartment,
            String SenderTitle,
            String SenderGiveName,
            String SenderMiddleName,
            String SenderFamilyName,
            String SenderStreetAddress,
            String SenderCity,
            String SenderState,
            String SenderPostCode,
            String SenderCountryCode,
            String SenderTel,
            String SenderTelExtension,
            String SenderTelCountryCode,
            String SenderFax,
            String SenderFaxExtension,
            String SenderFaxcountryCode,
            String SenderEmailAddress) {
        this.strSenderType = SenderType;
        this.strSenderOrganization = SenderOrganization;
        this.strSenderDepartment = SenderDepartment;
        this.strSenderTitle = SenderTitle;
        this.strSenderGiveName = SenderGiveName;
        this.strSenderMiddleName = SenderMiddleName;
        this.strSenderFamilyName = SenderFamilyName;
        this.strSenderStreetAddress = SenderStreetAddress;
        this.strSenderCity = SenderCity;
        this.strSenderState = SenderState;
        this.strSenderPostCode = SenderPostCode;
        this.strSenderCountryCode = SenderCountryCode;
        this.strSenderTel = SenderTel;
        this.strSenderTelExtension = SenderTelExtension;
        this.strSenderTelCountryCode = SenderTelCountryCode;
        this.strSenderFax = SenderFax;
        this.strSenderFaxExtension = SenderFaxExtension;
        this.strSenderFaxcountryCode = SenderFaxcountryCode;
        this.strSenderEmailAddress = SenderEmailAddress;
    }

    /**
     * Generates valid and well formed &lt;sender&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;sender&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<sender> \n");
            sb.append("    <sendertype>" + strSenderType + "</sendertype> \n");
            sb.append("    <senderorganization>" + strSenderOrganization + "</senderorganization> \n");
            sb.append("    <senderdepartment>" + strSenderDepartment + "</senderdepartment> \n");
            sb.append("    <sendertitle>" + strSenderTitle + "</sendertitle> \n");
            sb.append("    <sendergivename>" + strSenderGiveName + "</sendergivename> \n");
            sb.append("    <sendermiddlename>" + strSenderMiddleName + "</sendermiddlename> \n");
            sb.append("    <senderfamilyname>" + strSenderFamilyName + "</senderfamilyname> \n");
            sb.append("    <senderstreetaddress>" + strSenderStreetAddress + "</senderstreetaddress> \n");
            sb.append("    <sendercity>" + strSenderCity + "</sendercity> \n");
            sb.append("    <senderstate>" + strSenderState + "</senderstate> \n");
            sb.append("    <senderpostcode>" + strSenderPostCode + "</senderpostcode> \n");
            sb.append("    <sendercountrycode>" + strSenderCountryCode + "</sendercountrycode> \n");
            sb.append("    <sendertel>" + strSenderTel + "</sendertel> \n");
            sb.append("    <sendertelextension>" + strSenderTelExtension + "</sendertelextension> \n");
            sb.append("    <sendertelcountrycode>" + strSenderTelCountryCode + "</sendertelcountrycode> \n");
            sb.append("    <senderfax>" + strSenderFax + "</senderfax> \n");
            sb.append("    <senderfaxextension>" + strSenderFaxExtension + "</senderfaxextension> \n");
            sb.append("    <senderfaxcountrycode>" + strSenderFaxcountryCode + "</senderfaxcountrycode> \n");
            sb.append("    <senderemailaddress>" + strSenderEmailAddress + "</senderemailaddress> \n");
            sb.append("</sender> \n");

            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1] Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {

        if (strSenderType.length() > MAXLEN_SENDERTYPE) {
            throw new InvalidDataException("Length of SenderType must be less than " + MAXLEN_SENDERTYPE);
        }
        if (strSenderOrganization.length() > MAXLEN_SENDERORGANIZATION) {
            throw new InvalidDataException("Length of SenderOrganization must be less than " + MAXLEN_SENDERORGANIZATION);
        }
        if (strSenderDepartment.length() > MAXLEN_SENDERDEPARTMENT) {
            throw new InvalidDataException("Length of SenderDepartment must be less than " + MAXLEN_SENDERDEPARTMENT);
        }
        if (strSenderTitle.length() > MAXLEN_SENDERTITLE) {
            throw new InvalidDataException("Length of SenderTitle must be less than " + MAXLEN_SENDERTITLE);
        }
        if (strSenderGiveName.length() > MAXLEN_SENDERGIVENAME) {
            throw new InvalidDataException("Length of SenderGiveName must be less than " + MAXLEN_SENDERGIVENAME);
        }
        if (strSenderMiddleName.length() > MAXLEN_SENDERMIDDLENAME) {
            throw new InvalidDataException("Length of SenderMiddleName must be less than " + MAXLEN_SENDERMIDDLENAME);
        }
        if (strSenderFamilyName.length() > MAXLEN_SENDERFAMILYNAME) {
            throw new InvalidDataException("Length of SenderFamilyName must be less than " + MAXLEN_SENDERFAMILYNAME);
        }
        if (strSenderStreetAddress.length() > MAXLEN_SENDERSTREETADDRESS) {
            throw new InvalidDataException("Length of SenderStreetAddress must be less than " + MAXLEN_SENDERSTREETADDRESS);
        }
        if (strSenderCity.length() > MAXLEN_SENDERCITY) {
            throw new InvalidDataException("Length of SenderCity must be less than " + MAXLEN_SENDERCITY);
        }
        if (strSenderState.length() > MAXLEN_SENDERSTATE) {
            throw new InvalidDataException("Length of SenderState must be less than " + MAXLEN_SENDERSTATE);
        }
        if (strSenderPostCode.length() > MAXLEN_SENDERPOSTCODE) {
            throw new InvalidDataException("Length of SenderPostCode must be less than " + MAXLEN_SENDERPOSTCODE);
        }
        if (strSenderCountryCode.length() > MAXLEN_SENDERCOUNTRYCODE) {
            throw new InvalidDataException("Length of SenderCountryCode must be less than " + MAXLEN_SENDERCOUNTRYCODE);
        }
        if (strSenderTel.length() > MAXLEN_SENDERTEL) {
            throw new InvalidDataException("Length of SenderTel must be less than " + MAXLEN_SENDERTEL);
        }
        if (strSenderTelExtension.length() > MAXLEN_SENDERTELEXTENSION) {
            throw new InvalidDataException("Length of SenderTelExtension must be less than " + MAXLEN_SENDERTELEXTENSION);
        }
        if (strSenderTelCountryCode.length() > MAXLEN_SENDERTELCOUNTRYCODE) {
            throw new InvalidDataException("Length of SenderTelCountryCode must be less than " + MAXLEN_SENDERTELCOUNTRYCODE);
        }
        if (strSenderFax.length() > MAXLEN_SENDERFAX) {
            throw new InvalidDataException("Length of SenderFax must be less than " + MAXLEN_SENDERFAX);
        }
        if (strSenderFaxExtension.length() > MAXLEN_SENDERFAXEXTENSION) {
            throw new InvalidDataException("Length of SenderFaxExtension must be less than " + MAXLEN_SENDERFAXEXTENSION);
        }
        if (strSenderFaxcountryCode.length() > MAXLEN_SENDERFAXCOUNTRYCODE) {
            throw new InvalidDataException("Length of SenderFaxcountryCode must be less than " + MAXLEN_SENDERFAXCOUNTRYCODE);
        }
        if (strSenderEmailAddress.length() > MAXLEN_SENDEREMAILADDRESS) {
            throw new InvalidDataException("Length of SenderEmailAddress must be less than " + MAXLEN_SENDEREMAILADDRESS);
        }

        return true;
    }

    @Override
    public void fetchData() {
        this.strSenderType = "";
        this.strSenderOrganization = "";
        this.strSenderDepartment = "";
        this.strSenderTitle = "";
        if (TagData.getInstance().aCase.getComp_Rep_Info() != null) {
            this.strSenderGiveName = TagData.getInstance().aCase.getComp_Rep_Info();
        }
        this.strSenderMiddleName = "";
        this.strSenderFamilyName = "";
        this.strSenderStreetAddress = "";
        this.strSenderCity = "";
        this.strSenderState = "";
        this.strSenderPostCode = "";
        this.strSenderCountryCode = "";
        this.strSenderTel = "";
        this.strSenderTelExtension = "";
        this.strSenderTelCountryCode = "";
        this.strSenderFax = "";
        this.strSenderFaxExtension = "";
        this.strSenderFaxcountryCode = "";
        this.strSenderEmailAddress = "";
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strSenderType = null;
        this.strSenderOrganization = null;
        this.strSenderDepartment = null;
        this.strSenderTitle = null;
        this.strSenderGiveName = null;
        this.strSenderMiddleName = null;
        this.strSenderFamilyName = null;
        this.strSenderStreetAddress = null;
        this.strSenderCity = null;
        this.strSenderState = null;
        this.strSenderPostCode = null;
        this.strSenderCountryCode = null;
        this.strSenderTel = null;
        this.strSenderTelExtension = null;
        this.strSenderTelCountryCode = null;
        this.strSenderFax = null;
        this.strSenderFaxExtension = null;
        this.strSenderFaxcountryCode = null;
        this.strSenderEmailAddress = null;
    }
}
