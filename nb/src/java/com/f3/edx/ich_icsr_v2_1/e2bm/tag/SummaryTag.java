package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class SummaryTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 7920898685766027938L;
    private String strNarrativeIncludeClinical = "";
    private String strReporterComment = "";
    private String strSenderDiagnosisMedDraVersion = "v. 2.3";
    private String strSenderDiagnosis = "";
    private String strSenderComment = "";

    // Constructors
    /**
     * Default constructor that creates a new instance of SummaryTag.
     * This constructor initializes its member variables to empty string.
     */
    public SummaryTag() {
        this.strNarrativeIncludeClinical = "";
        this.strReporterComment = "";
        this.strSenderDiagnosisMedDraVersion = "v. 2.3";
        this.strSenderDiagnosis = "";
        this.strSenderComment = "";
    }

    /**
     * Constructor that creates a new instance of <code><b>SummaryTag</b></code>.
     * 
     * @param NarrativeIncludeClinical <code><b>String</b></code> value to set &lt;narrativeincludeclinical&gt; tag.
     * @param ReporterComment <code><b>String</b></code> value to set &lt;reportercomment&gt; tag.
     * @param SenderDiagnosisMedDraVersion <code><b>String</b></code> value to set &lt;senderdiagnosismeddraversion&gt; tag.
     * @param SenderDiagnosis <code><b>String</b></code> value to set &lt;senderdiagnosis&gt; tag.
     * @param SenderComment <code><b>String</b></code> value to set &lt;sendercomment&gt; tag.
     * @deprecated 
     */
    @Deprecated
    public SummaryTag(
            String NarrativeIncludeClinical,
            String ReporterComment,
            String SenderDiagnosisMedDraVersion,
            String SenderDiagnosis,
            String SenderComment) {
        this.strNarrativeIncludeClinical = NarrativeIncludeClinical;
        this.strReporterComment = ReporterComment;
        this.strSenderDiagnosisMedDraVersion = "v. 2.3";
        this.strSenderDiagnosis = SenderDiagnosis;
        this.strSenderComment = SenderComment;
    }

    /**
     * Generates valid and well formed &lt;summary&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;summary&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<summary> \n");
            sb.append("    <narrativeincludeclinical>" + strNarrativeIncludeClinical + "</narrativeincludeclinical> \n");
            sb.append("    <reportercomment>" + strReporterComment + "</reportercomment> \n");
            sb.append("    <senderdiagnosismeddraversion>" + strSenderDiagnosisMedDraVersion + "</senderdiagnosismeddraversion> \n");
            sb.append("    <senderdiagnosis>" + strSenderDiagnosis + "</senderdiagnosis> \n");
            sb.append("    <sendercomment>" + strSenderComment + "</sendercomment> \n");
            sb.append("</summary> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // No mandatory fields. So
        return true;
    }

    @Override
    public void fetchData() {
        if (TagData.getInstance().aCase.getShort_Comment() != null) {
            this.strNarrativeIncludeClinical = TagData.getInstance().aCase.getShort_Comment();
        }
        this.strReporterComment = "";
        this.strSenderDiagnosisMedDraVersion = "v. 2.3";
        this.strSenderDiagnosis = "";
        this.strSenderComment = "";
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strNarrativeIncludeClinical = null;
        this.strReporterComment = null;
        this.strSenderDiagnosisMedDraVersion = null;
        this.strSenderDiagnosis = null;
        this.strSenderComment = null;
    }
}
