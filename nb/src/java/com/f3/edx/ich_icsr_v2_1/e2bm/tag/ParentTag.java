package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.util.ArrayList;

/**
 *
 * @author Anoop Varma
 */
public class ParentTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -6482389221059407712L;
    private final int MAXLEN_PARENTIDENTIFICATION = 10;
    private final int MAXLEN_PARENTBIRTHDATEFORMAT = 3;
    private final int MAXLEN_PARENTBIRTHDATE = 8;
    private final int MAXLEN_PARENTAGE = 2;
    private final int MAXLEN_PARENTAGEUNIT = 3;
    private final int MAXLEN_PARENTLASTMENSTRUALDATEFORMAT = 3;
    private final int MAXLEN_PARENTLASTMENSTRUALDATE = 8;
    private final int MAXLEN_PARENTWEIGHT = 6;
    private final int MAXLEN_PARENTHEIGHT = 3;
    private final int MAXLEN_PARENTSEX = 1;
    private final int MAXLEN_PARENTMEDICALRELEVANTTEXT = 10000;
    //////////////////////////////////////////////////////
    private String strParentIdentification;
    private String strParentBirthdateFormat;
    private String strParentBirthDate;
    private String strParentAge;
    private String strParentAgeUnit;
    private String strParentLastMenstrualDateFormat;
    private String strParentLastMenstrualDate;
    private String strParentWeight;
    private String strParentHeight;
    private String strParentSex;
    private String strParentMedicalRelevantText;
    private ArrayList<ParentMedicalHistoryEpisodeTag> alParentMedicalHistoryEpisode;
    private ArrayList<ParentPastDrugTherapyTag> alParentPastDrugTherapy;

    /**
     * Generates valid and well formed &lt;parent&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;parent&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<parent> \n");
            sb.append("    <parentidentification>" + strParentIdentification + "</parentidentification> \n");
            sb.append("    <parentage>" + strParentAge + "</parentage> \n");
            sb.append("    <parentageunit>" + strParentAgeUnit + "</parentageunit> \n");
            sb.append("    <parentlastmenstrualdateformat>" + strParentLastMenstrualDateFormat + "</parentlastmenstrualdateformat> \n");
            sb.append("    <parentlastmenstrualdate>" + strParentLastMenstrualDate + "</parentlastmenstrualdate> \n");
            sb.append("    <parentweight>" + strParentWeight + "</parentweight> \n");
            sb.append("    <parentheight>" + strParentHeight + "</parentheight> \n");
            sb.append("    <parentsex>" + strParentSex + "</parentsex> \n");
            sb.append("    <parentmedicalrelevanttext>" + strParentMedicalRelevantText + "</parentmedicalrelevanttext> \n");

            if (alParentMedicalHistoryEpisode != null) {
                for (int i = 0; i < alParentMedicalHistoryEpisode.size(); i++) {
                    sb.append("    <!-- ParentMedicalHistoryEpisode tag : " + i + " of " + alParentMedicalHistoryEpisode.size() + " --> \n");
                    sb.append("    " + alParentMedicalHistoryEpisode.get(i).getTag());
                }
            }

            if (alParentPastDrugTherapy != null) {
                for (int i = 0; i < alParentPastDrugTherapy.size(); i++) {
                    sb.append("    <!-- ParentPastDrugTherapy tag : " + i + " of " + alParentPastDrugTherapy.size() + " --> \n");
                    sb.append("    " + alParentPastDrugTherapy.get(i).getTag());
                }
            }

            sb.append("</parent> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // no mandatory fields. So, no InvalidTagException

        if (strParentIdentification.length() > MAXLEN_PARENTIDENTIFICATION) {
            throw new InvalidDataException("Length of ParentIdentification must be less than " + MAXLEN_PARENTIDENTIFICATION);
        }
        if (strParentBirthdateFormat.length() > MAXLEN_PARENTBIRTHDATEFORMAT) {
            throw new InvalidDataException("Length of ParentBirthdateFormat must be less than " + MAXLEN_PARENTBIRTHDATEFORMAT);
        }
        if (strParentBirthDate.length() > MAXLEN_PARENTBIRTHDATE) {
            throw new InvalidDataException("Length of ParentBirthDate must be less than " + MAXLEN_PARENTBIRTHDATE);
        }
        if (strParentAge.length() > MAXLEN_PARENTAGE) {
            throw new InvalidDataException("Length of ParentAge must be less than " + MAXLEN_PARENTAGE);
        }
        if (strParentAgeUnit.length() > MAXLEN_PARENTAGEUNIT) {
            throw new InvalidDataException("Length of ParentAgeUnit must be less than " + MAXLEN_PARENTAGEUNIT);
        }
        if (strParentLastMenstrualDateFormat.length() > MAXLEN_PARENTLASTMENSTRUALDATEFORMAT) {
            throw new InvalidDataException("Length of ParentLastMenstrualDateFormat must be less than " + MAXLEN_PARENTLASTMENSTRUALDATEFORMAT);
        }
        if (strParentLastMenstrualDate.length() > MAXLEN_PARENTLASTMENSTRUALDATE) {
            throw new InvalidDataException("Length of ParentLastMenstrualDate must be less than " + MAXLEN_PARENTLASTMENSTRUALDATE);
        }
        if (strParentWeight.length() > MAXLEN_PARENTWEIGHT) {
            throw new InvalidDataException("Length of ParentWeight must be less than " + MAXLEN_PARENTWEIGHT);
        }
        if (strParentHeight.length() > MAXLEN_PARENTHEIGHT) {
            throw new InvalidDataException("Length of ParentHeight must be less than " + MAXLEN_PARENTHEIGHT);
        }
        if (strParentSex.length() > MAXLEN_PARENTSEX) {
            throw new InvalidDataException("Length of ParentSex must be less than " + MAXLEN_PARENTSEX);
        }
        if (strParentMedicalRelevantText.length() > MAXLEN_PARENTMEDICALRELEVANTTEXT) {
            throw new InvalidDataException("Length of ParentMedicalRelevantText must be less than " + MAXLEN_PARENTMEDICALRELEVANTTEXT);
        }

        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strParentIdentification = null;
        this.strParentBirthdateFormat = null;
        this.strParentBirthDate = null;
        this.strParentAge = null;
        this.strParentAgeUnit = null;
        this.strParentLastMenstrualDateFormat = null;
        this.strParentLastMenstrualDate = null;
        this.strParentWeight = null;
        this.strParentHeight = null;
        this.strParentSex = null;
        this.strParentMedicalRelevantText = null;
        this.alParentMedicalHistoryEpisode = null;
        this.alParentPastDrugTherapy = null;
    }
}
