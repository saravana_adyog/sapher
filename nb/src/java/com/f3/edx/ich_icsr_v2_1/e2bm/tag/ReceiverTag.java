package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class ReceiverTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 6870961248970514258L;
    private int MAXLEN_RECEIVERTYPE = 1;
    private int MAXLEN_RECEIVERORGANIZATION = 60;
    private int MAXLEN_RECEIVERDEPARTMENT = 60;
    private int MAXLEN_RECEIVERTITLE = 10;
    private int MAXLEN_RECEIVERGIVENAME = 35;
    private int MAXLEN_RECEIVERMIDDLENAME = 15;
    private int MAXLEN_RECEIVERFAMILYNAME = 35;
    private int MAXLEN_RECEIVERSTREETADDRESS = 100;
    private int MAXLEN_RECEIVERCITY = 35;
    private int MAXLEN_RECEIVERSTATE = 40;
    private int MAXLEN_RECEIVERPOSTCODE = 15;
    private int MAXLEN_RECEIVERCOUNTRYCODE = 2;
    private int MAXLEN_RECEIVERTEL = 10;
    private int MAXLEN_RECEIVERTELEXTENSION = 5;
    private int MAXLEN_RECEIVERTELCOUNTRYCODE = 3;
    private int MAXLEN_RECEIVERFAX = 10;
    private int MAXLEN_RECEIVERFAXEXTENSION = 5;
    private int MAXLEN_RECEIVERFAXCOUNTRYCODE = 3;
    private int MAXLEN_RECEIVEREMAILADDRESS = 100;
    /////////////////////////////////////
    private String strReceiverType;
    private String strReceiverOrganization;
    private String strReceiverDepartment;
    private String strReceiverTitle;
    private String strReceiverGiveName;
    private String strReceiverMiddleName;
    private String strReceiverFamilyName;
    private String strReceiverStreetAddress;
    private String strReceiverCity;
    private String strReceiverState;
    private String strReceiverPostCode;
    private String strReceiverCountryCode;
    private String strReceiverTel;
    private String strReceiverTelExtension;
    private String strReceiverTelCountryCode;
    private String strReceiverFax;
    private String strReceiverFaxExtension;
    private String strReceiverFaxCountryCode;
    private String strReceiverEmailAddress;

    /**
     * Default constructor for ReceiverTag.
     * This constructor initializes its member variables to empty string.
     */
    public ReceiverTag() {
        this.strReceiverType = "";
        this.strReceiverOrganization = "";
        this.strReceiverDepartment = "";
        this.strReceiverTitle = "";
        this.strReceiverGiveName = "";
        this.strReceiverMiddleName = "";
        this.strReceiverFamilyName = "";
        this.strReceiverStreetAddress = "";
        this.strReceiverCity = "";
        this.strReceiverState = "";
        this.strReceiverPostCode = "";
        this.strReceiverCountryCode = "";
        this.strReceiverTel = "";
        this.strReceiverTelExtension = "";
        this.strReceiverTelCountryCode = "";
        this.strReceiverFax = "";
        this.strReceiverFaxExtension = "";
        this.strReceiverFaxCountryCode = "";
        this.strReceiverEmailAddress = "";
    }

    /**
     *
     * @param ReceiverType           <code><b>String</b></code> value to set &lt;receivertype&gt; tag.
     * @param ReceiverOrganization   <code><b>String</b></code> value to set &lt;receiverorganization&gt; tag.
     * @param ReceiverDepartment     <code><b>String</b></code> value to set &lt;receiverdepartment&gt; tag.
     * @param ReceiverTitle          <code><b>String</b></code> value to set &lt;receivertitle&gt; tag.
     * @param ReceiverGiveName       <code><b>String</b></code> value to set &lt;receivergivename&gt; tag.
     * @param ReceiverMiddleName     <code><b>String</b></code> value to set &lt;receivermiddlename&gt; tag.
     * @param ReceiverFamilyName     <code><b>String</b></code> value to set &lt;receiverfamilyname&gt; tag.
     * @param ReceiverStreetAddress  <code><b>String</b></code> value to set &lt;receiverstreetaddress&gt; tag.
     * @param ReceiverCity           <code><b>String</b></code> value to set &lt;receivercity&gt; tag.
     * @param ReceiverState          <code><b>String</b></code> value to set &lt;receiverstate&gt; tag.
     * @param ReceiverPostCode       <code><b>String</b></code> value to set &lt;receiverpostcode&gt; tag.
     * @param ReceiverCountryCode    <code><b>String</b></code> value to set &lt;receivercountrycode&gt; tag.
     * @param ReceiverTel            <code><b>String</b></code> value to set &lt;receivertel&gt; tag.
     * @param ReceiverTelExtension   <code><b>String</b></code> value to set &lt;receivertelextension&gt; tag.
     * @param ReceiverTelCountryCode <code><b>String</b></code> value to set &lt;receivertelcountrycode&gt; tag.
     * @param ReceiverFax            <code><b>String</b></code> value to set &lt;receiverfax&gt; tag.
     * @param ReceiverFaxExtension   <code><b>String</b></code> value to set &lt;receiverfaxextension&gt; tag.
     * @param ReceiverFaxCountryCode <code><b>String</b></code> value to set &lt;receiverfaxcountrycode&gt; tag.
     * @param ReceiverEmailAddress   <code><b>String</b></code> value to set &lt;receiveremailaddress&gt; tag.
     * @deprecated
     */
    @Deprecated
    public ReceiverTag(
            String ReceiverType,
            String ReceiverOrganization,
            String ReceiverDepartment,
            String ReceiverTitle,
            String ReceiverGiveName,
            String ReceiverMiddleName,
            String ReceiverFamilyName,
            String ReceiverStreetAddress,
            String ReceiverCity,
            String ReceiverState,
            String ReceiverPostCode,
            String ReceiverCountryCode,
            String ReceiverTel,
            String ReceiverTelExtension,
            String ReceiverTelCountryCode,
            String ReceiverFax,
            String ReceiverFaxExtension,
            String ReceiverFaxCountryCode,
            String ReceiverEmailAddress) {
        this.strReceiverType = ReceiverType;
        this.strReceiverOrganization = ReceiverOrganization;
        this.strReceiverDepartment = ReceiverDepartment;
        this.strReceiverTitle = ReceiverTitle;
        this.strReceiverGiveName = ReceiverGiveName;
        this.strReceiverMiddleName = ReceiverMiddleName;
        this.strReceiverFamilyName = ReceiverFamilyName;
        this.strReceiverStreetAddress = ReceiverStreetAddress;
        this.strReceiverCity = ReceiverCity;
        this.strReceiverState = ReceiverState;
        this.strReceiverPostCode = ReceiverPostCode;
        this.strReceiverCountryCode = ReceiverCountryCode;
        this.strReceiverTel = ReceiverTel;
        this.strReceiverTelExtension = ReceiverTelExtension;
        this.strReceiverTelCountryCode = ReceiverTelCountryCode;
        this.strReceiverFax = ReceiverFax;
        this.strReceiverFaxExtension = ReceiverFaxExtension;
        this.strReceiverFaxCountryCode = ReceiverFaxCountryCode;
        this.strReceiverEmailAddress = ReceiverEmailAddress;
    }

    /**
     * Generates valid and well formed &lt;receiver&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;receiver&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<receiver> \n");
            sb.append("    <receivertype>" + strReceiverType + "</receivertype> \n");
            sb.append("    <receiverorganization>" + strReceiverOrganization + "</receiverorganization> \n");
            sb.append("    <receiverdepartment>" + strReceiverDepartment + "</receiverdepartment> \n");
            sb.append("    <receivertitle>" + strReceiverTitle + "</receivertitle> \n");
            sb.append("    <receivergivename>" + strReceiverGiveName + "</receivergivename> \n");
            sb.append("    <receivermiddlename>" + strReceiverMiddleName + "</receivermiddlename> \n");
            sb.append("    <receiverfamilyname>" + strReceiverFamilyName + "</receiverfamilyname> \n");
            sb.append("    <receiverstreetaddress>" + strReceiverStreetAddress + "</receiverstreetaddress> \n");
            sb.append("    <receivercity>" + strReceiverCity + "</receivercity> \n");
            sb.append("    <receiverstate>" + strReceiverState + "</receiverstate> \n");
            sb.append("    <receiverpostcode>" + strReceiverPostCode + "</receiverpostcode> \n");
            sb.append("    <receivercountrycode>" + strReceiverCountryCode + "</receivercountrycode> \n");
            sb.append("    <receivertel>" + strReceiverTel + "</receivertel> \n");
            sb.append("    <receivertelextension>" + strReceiverTelExtension + "</receivertelextension> \n");
            sb.append("    <receivertelcountrycode>" + strReceiverTelCountryCode + "</receivertelcountrycode> \n");
            sb.append("    <receiverfax>" + strReceiverFax + "</receiverfax> \n");
            sb.append("    <receiverfaxextension>" + strReceiverFaxExtension + "</receiverfaxextension> \n");
            sb.append("    <receiverfaxcountrycode>" + strReceiverFaxCountryCode + "</receiverfaxcountrycode> \n");
            sb.append("    <receiveremailaddress>" + strReceiverEmailAddress + "</receiveremailaddress> \n");
            sb.append("</receiver> \n");

            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1] Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {
        ////
        if (strReceiverType.length() > MAXLEN_RECEIVERTYPE) {
            throw new InvalidDataException("Length of ReceiverType must be less than " + MAXLEN_RECEIVERTYPE);
        }
        if (strReceiverOrganization.length() > MAXLEN_RECEIVERORGANIZATION) {
            throw new InvalidDataException("Length of ReceiverOrganization must be less than " + MAXLEN_RECEIVERORGANIZATION);
        }
        if (strReceiverDepartment.length() > MAXLEN_RECEIVERDEPARTMENT) {
            throw new InvalidDataException("Length of ReceiverDepartment must be less than " + MAXLEN_RECEIVERDEPARTMENT);
        }
        if (strReceiverTitle.length() > MAXLEN_RECEIVERTITLE) {
            throw new InvalidDataException("Length of ReceiverTitle must be less than " + MAXLEN_RECEIVERTITLE);
        }
        if (strReceiverGiveName.length() > MAXLEN_RECEIVERGIVENAME) {
            throw new InvalidDataException("Length of ReceiverGiveName must be less than " + MAXLEN_RECEIVERGIVENAME);
        }
        if (strReceiverMiddleName.length() > MAXLEN_RECEIVERMIDDLENAME) {
            throw new InvalidDataException("Length of ReceiverMiddleName must be less than " + MAXLEN_RECEIVERMIDDLENAME);
        }
        if (strReceiverFamilyName.length() > MAXLEN_RECEIVERFAMILYNAME) {
            throw new InvalidDataException("Length of ReceiverFamilyName must be less than " + MAXLEN_RECEIVERFAMILYNAME);
        }
        if (strReceiverStreetAddress.length() > MAXLEN_RECEIVERSTREETADDRESS) {
            throw new InvalidDataException("Length of ReceiverStreetAddress must be less than " + MAXLEN_RECEIVERSTREETADDRESS);
        }
        if (strReceiverCity.length() > MAXLEN_RECEIVERCITY) {
            throw new InvalidDataException("Length of ReceiverCity must be less than " + MAXLEN_RECEIVERCITY);
        }
        if (strReceiverState.length() > MAXLEN_RECEIVERSTATE) {
            throw new InvalidDataException("Length of ReceiverState must be less than " + MAXLEN_RECEIVERSTATE);
        }
        if (strReceiverPostCode.length() > MAXLEN_RECEIVERPOSTCODE) {
            throw new InvalidDataException("Length of ReceiverPostCode must be less than " + MAXLEN_RECEIVERPOSTCODE);
        }
        if (strReceiverCountryCode.length() > MAXLEN_RECEIVERCOUNTRYCODE) {
            throw new InvalidDataException("Length of ReceiverCountryCode must be less than " + MAXLEN_RECEIVERCOUNTRYCODE);
        }
        if (strReceiverTel.length() > MAXLEN_RECEIVERTEL) {
            throw new InvalidDataException("Length of ReceiverTel must be less than " + MAXLEN_RECEIVERTEL);
        }
        if (strReceiverTelExtension.length() > MAXLEN_RECEIVERTELEXTENSION) {
            throw new InvalidDataException("Length of ReceiverTelExtension must be less than " + MAXLEN_RECEIVERTELEXTENSION);
        }
        if (strReceiverTelCountryCode.length() > MAXLEN_RECEIVERTELCOUNTRYCODE) {
            throw new InvalidDataException("Length of ReceiverTelCountryCode must be less than " + MAXLEN_RECEIVERTELCOUNTRYCODE);
        }
        if (strReceiverFax.length() > MAXLEN_RECEIVERFAX) {
            throw new InvalidDataException("Length of ReceiverFax must be less than " + MAXLEN_RECEIVERFAX);
        }
        if (strReceiverFaxExtension.length() > MAXLEN_RECEIVERFAXEXTENSION) {
            throw new InvalidDataException("Length of ReceiverFaxExtension must be less than " + MAXLEN_RECEIVERFAXEXTENSION);
        }
        if (strReceiverFaxCountryCode.length() > MAXLEN_RECEIVERFAXCOUNTRYCODE) {
            throw new InvalidDataException("Length of ReceiverFaxCountryCode must be less than " + MAXLEN_RECEIVERFAXCOUNTRYCODE);
        }
        if (strReceiverEmailAddress.length() > MAXLEN_RECEIVEREMAILADDRESS) {
            throw new InvalidDataException("Length of ReceiverEmailAddress must be less than " + MAXLEN_RECEIVEREMAILADDRESS);
        }
        return true;

    }

    @Override
    public void fetchData() {
        strReceiverType = "";
        strReceiverOrganization = "";
        strReceiverDepartment = "";
        strReceiverTitle = "";
        if (TagData.getInstance().aCase.getDcompany() != null) {
            strReceiverGiveName = TagData.getInstance().aCase.getDcompany();
        }
        strReceiverMiddleName = "";
        strReceiverFamilyName = "";
        strReceiverStreetAddress = "";
        strReceiverCity = "";
        strReceiverState = "";
        strReceiverPostCode = "";
        strReceiverCountryCode = "";
        strReceiverTel = "";
        strReceiverTelExtension = "";
        strReceiverTelCountryCode = "";
        strReceiverFax = "";
        strReceiverFaxExtension = "";
        strReceiverFaxCountryCode = "";
        strReceiverEmailAddress = "";
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strReceiverType = null;
        this.strReceiverOrganization = null;
        this.strReceiverDepartment = null;
        this.strReceiverTitle = null;
        this.strReceiverGiveName = null;
        this.strReceiverMiddleName = null;
        this.strReceiverFamilyName = null;
        this.strReceiverStreetAddress = null;
        this.strReceiverCity = null;
        this.strReceiverState = null;
        this.strReceiverPostCode = null;
        this.strReceiverCountryCode = null;
        this.strReceiverTel = null;
        this.strReceiverTelExtension = null;
        this.strReceiverTelCountryCode = null;
        this.strReceiverFax = null;
        this.strReceiverFaxExtension = null;
        this.strReceiverFaxCountryCode = null;
        this.strReceiverEmailAddress = null;
    }
}
