package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Anoop Varma
 */
public class IchIcsrMessageHeaderTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -566421609139818246L;
    /** M.1.1 Message Type  as <code>String</code> */
    private String strMessageType = "ichicsr";
    /** M.1.2 Message Format Version  as <code>String</code> */
    private String strMessageFormatVersion = "2.1";
    /** M.1.3 Message Format Release  as <code>String</code> */
    private String strMessageFormatRelease = "1.0";
    /** M.1.4 Message Number  as <code>String</code> */
    private String strstrMessageNumb = "";
    /** M.1.5 Message Sender Identifier  as <code>String</code> */
    private String strMessageSenderIdentifier = "";
    /** M.1.6 Message Receiver Identifier  as <code>String</code> */
    private String strMessageReceiverIdentifier = "";
    /** M.1.7a Message Date Format  as <code>String</code> */
    private String strMessageDateFormat = "";
    /** M.1.7b Message Date  as <code>String</code> */
    private String strMessageDate = "";

    /**
     * Creates a new instance of IchIcsrMessageHeaderTag
     *
     * @param messagetype               <code><b>String</b></code> value to set &lt;messagetype&gt; tag.
     * @param messageformatversion      <code><b>String</b></code> value to set &lt;messageformatversion&gt; tag.
     * @param MessageFormatRelease      <code><b>String</b></code> value to set &lt;messageformatrelease&gt; tag.
     * @param MessageNumb               <code><b>String</b></code> value to set &lt;messagenumb&gt; tag.
     * @param MessageSenderIdentifier   <code><b>String</b></code> value to set &lt;messagesenderidentifier&gt; tag.
     * @param MessageReceiverIdentifier <code><b>String</b></code> value to set &lt;messagereceiveridentifier&gt; tag.
     * @param MessageDateFormat         <code><b>String</b></code> value to set &lt;messagedateformat&gt; tag.
     * @param MessageDate               <code><b>String</b></code> value to set &lt;messagedate&gt; tag.
     *
     * @deprecated
     */
    @Deprecated
    public IchIcsrMessageHeaderTag(
            String messagetype,
            String messageformatversion,
            String MessageFormatRelease,
            String MessageNumb,
            String MessageSenderIdentifier,
            String MessageReceiverIdentifier,
            String MessageDateFormat,
            String MessageDate) {
        this.strMessageType = messagetype;
        this.strMessageFormatVersion = messageformatversion;
        this.strMessageFormatRelease = MessageFormatRelease;
        this.strstrMessageNumb = MessageNumb;
        this.strMessageSenderIdentifier = MessageSenderIdentifier;
        this.strMessageReceiverIdentifier = MessageReceiverIdentifier;
        this.strMessageDateFormat = MessageDateFormat;
        this.strMessageDate = MessageDate;
    }

    /**
     * Default constructor that creates a new instance of IchIcsrMessageHeaderTag.
     * This constructor initializes its member variables to empty string.
     */
    public IchIcsrMessageHeaderTag() {
        this.strMessageType = "";
        this.strMessageFormatVersion = "";
        this.strMessageFormatRelease = "";
        this.strstrMessageNumb = "";
        this.strMessageSenderIdentifier = "";
        this.strMessageReceiverIdentifier = "";
        this.strMessageDateFormat = "";
        this.strMessageDate = "";
    }

    /**
     * Generates valid and well formed &lt;ichicsrmessageheader&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;ichicsrmessageheader&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<ichicsrmessageheader> \n");
            sb.append("    <messagetype>" + strMessageType + "</messagetype> \n");
            sb.append("    <messageformatversion>" + strMessageFormatVersion + "</messageformatversion> \n");
            sb.append("    <messageformatrelease>" + strMessageFormatRelease + "</messageformatrelease> \n");
            sb.append("    <messagenumb>" + strstrMessageNumb + "</messagenumb> \n");
            sb.append("    <messagesenderidentifier>" + strMessageSenderIdentifier + "</messagesenderidentifier> \n");
            sb.append("    <messagereceiveridentifier>" + strMessageReceiverIdentifier + "</messagereceiveridentifier> \n");
            sb.append("    <messagedateformat>" + strMessageDateFormat + "</messagedateformat> \n");
            sb.append("    <messagedate>" + strMessageDate + "</messagedate> \n");
            sb.append("</ichicsrmessageheader> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    @Override
    public void fetchData() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", java.util.Locale.US);
        this.strMessageType = "";
        this.strMessageFormatVersion = "";
        this.strMessageFormatRelease = "";
        this.strstrMessageNumb = "";
        this.strMessageSenderIdentifier = "";
        this.strMessageReceiverIdentifier = "";
        this.strMessageDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strMessageDate = sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code>TRUE</code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // No mandatory fields. So,
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strMessageType = null;
        this.strMessageFormatVersion = null;
        this.strMessageFormatRelease = null;
        this.strstrMessageNumb = null;
        this.strMessageSenderIdentifier = null;
        this.strMessageReceiverIdentifier = null;
        this.strMessageDateFormat = null;
        this.strMessageDate = null;
    }
}
