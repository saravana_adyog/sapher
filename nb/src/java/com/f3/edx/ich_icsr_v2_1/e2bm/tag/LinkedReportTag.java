package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class LinkedReportTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -7662932084458259553L;
    private int MAXLEN_LINKREPORTNUMB = 100;
    ///////////////////////////////////////
    private String strLinkReportNumb = "";

    // Constructors
    public LinkedReportTag() {
        this.strLinkReportNumb = "";
    }

    /**
     * 
     * @param linkreportnumbas <code><b>String</b></code>
     * @deprecated 
     */
    @Deprecated
    public LinkedReportTag(String linkreportnumb) {
        this.strLinkReportNumb = linkreportnumb;
    }

    /**
     * Generates valid and well formed &lt;linkedreport&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;linkedreport&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<linkedreport> \n");
            sb.append("    <linkreportnumb>" + strLinkReportNumb + "</linkreportnumb> \n");
            sb.append("</linkedreport> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    @Override
    public void fetchData() {
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {
        if (strLinkReportNumb.length() > MAXLEN_LINKREPORTNUMB) {
            throw new InvalidDataException("Length of LinkReportNumb must be less than " + MAXLEN_LINKREPORTNUMB);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strLinkReportNumb = null;
    }
}
