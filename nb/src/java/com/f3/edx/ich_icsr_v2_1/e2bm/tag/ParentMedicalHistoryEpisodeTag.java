package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 * 
 * @author Anoop Varma
 */
public class ParentMedicalHistoryEpisodeTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 5017847271304719995L;
    private String strParentEpisodeNameMeddraVersion = "";
    private String strParentEpisodeName = "";
    private String strParentMedicalStartDateFormat = "";
    private String strParentMedicalStartDate = "";
    private String strParentMedicalContinue = "";
    private String strParentMedicalEndDateFormat = "";
    private String strParentMedicalEndDate = "";
    private String strParentMedicalComment = "";

    /**
     * Generates valid and well formed &lt;parentmedicalhistoryepisode&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;parentmedicalhistoryepisode&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<parentmedicalhistoryepisode> \n");
            sb.append("    <parentepisodenamemeddraversion>" + strParentEpisodeNameMeddraVersion + "</parentepisodenamemeddraversion>  \n");
            sb.append("    <parentepisodename>" + strParentEpisodeName + "</parentepisodename>  \n");
            sb.append("    <parentmedicalstartdateformat>" + strParentMedicalStartDateFormat + "</parentmedicalstartdateformat>  \n");
            sb.append("    <parentmedicalstartdate>" + strParentMedicalStartDate + "</parentmedicalstartdate>  \n");
            sb.append("    <parentmedicalcontinue>" + strParentMedicalContinue + "</parentmedicalcontinue>  \n");
            sb.append("    <parentmedicalenddateformat>" + strParentMedicalEndDateFormat + "</parentmedicalenddateformat>  \n");
            sb.append("    <parentmedicalenddate>" + strParentMedicalEndDate + "</parentmedicalenddate>  \n");
            sb.append("    <parentmedicalcomment>" + strParentMedicalComment + "</parentmedicalcomment>  \n");
            sb.append("</parentmedicalhistoryepisode> \n");

            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }

    }

    // Constructors
    /**
     * Default constructor that creates a new instance of ParentMedicalHistoryEpisodeTag.
     * This constructor initializes its member variables to empty string.
     */
    public ParentMedicalHistoryEpisodeTag() {
        this.strParentEpisodeNameMeddraVersion = "";
        this.strParentEpisodeName = "";
        this.strParentMedicalStartDateFormat = "";
        this.strParentMedicalStartDate = "";
        this.strParentMedicalContinue = "";
        this.strParentMedicalEndDateFormat = "";
        this.strParentMedicalEndDate = "";
        this.strParentMedicalComment = "";
    }

    /**
     *
     * @param ParentEpisodeNameMeddraVersion as <code><b>String</b></code>
     * @param ParentEpisodeName              as <code><b>String</b></code>
     * @param ParentMedicalStartDateFormat   as <code><b>String</b></code>
     * @param ParentMedicalStartDate         as <code><b>String</b></code>
     * @param ParentMedicalContinue          as <code><b>String</b></code>
     * @param ParentMedicalEndDateFormat     as <code><b>String</b></code>
     * @param ParentMedicalEndDate           as <code><b>String</b></code>
     * @param ParentMedicalComment           as <code><b>String</b></code>
     * @deprecated
     */
    @Deprecated
    public ParentMedicalHistoryEpisodeTag(
            String ParentEpisodeNameMeddraVersion,
            String ParentEpisodeName,
            String ParentMedicalStartDateFormat,
            String ParentMedicalStartDate,
            String ParentMedicalContinue,
            String ParentMedicalEndDateFormat,
            String ParentMedicalEndDate,
            String ParentMedicalComment) {
        this.strParentEpisodeNameMeddraVersion = ParentEpisodeNameMeddraVersion;
        this.strParentEpisodeName = ParentEpisodeName;
        this.strParentMedicalStartDateFormat = ParentMedicalStartDateFormat;
        this.strParentMedicalStartDate = ParentMedicalStartDate;
        this.strParentMedicalContinue = ParentMedicalContinue;
        this.strParentMedicalEndDateFormat = ParentMedicalEndDateFormat;
        this.strParentMedicalEndDate = ParentMedicalEndDate;
        this.strParentMedicalComment = ParentMedicalComment;
    }

    @Override
    public void fetchData() {
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strParentEpisodeNameMeddraVersion = null;
        this.strParentEpisodeName = null;
        this.strParentMedicalStartDateFormat = null;
        this.strParentMedicalStartDate = null;
        this.strParentMedicalContinue = null;
        this.strParentMedicalEndDateFormat = null;
        this.strParentMedicalEndDate = null;
        this.strParentMedicalComment = null;
    }
}
