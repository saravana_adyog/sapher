package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class DrugRecurrenceTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -7034714122683422963L;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for DrugRecurActionMedDraVersion : <code><b>8</b></code></p>
     */
    private final int MAXLEN_DRUG_RECURACTION_MEDDRA_VERSION = 8;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for DrugRecurAction : <code><b>250</b></code></p>
     */
    private final int MAXLEN_DRUG_RECURACTION = 250;
    private String strDrugRecurActionMedDraVersion = "v. 2.3";  // 0 or 1 occurance
    private String strDrugRecurAction = "";                     // 0 or 1 occurance

    /**
     * Generates valid and well formed &lt;drugrecurrence&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;drugrecurrence&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<drugrecurrence> \n");
            sb.append("    <drugrecuractionmeddraversion>" + strDrugRecurActionMedDraVersion + "</drugrecuractionmeddraversion> \n");
            sb.append("    <drugrecuraction>" + strDrugRecurAction + "</drugrecuraction> \n");
            sb.append("</drugrecurrence> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    @Override
    public void fetchData() {
        this.strDrugRecurActionMedDraVersion = "v. 2.3";
        if (TagData.getInstance().aCase.getIndication() != null) {
            this.strDrugRecurAction = TagData.getInstance().aCase.getIndication();
        }
    }

    //Constructors
    /**
     * Default constructor that creates a new instance of DrugRecurrenceTag.
     * This constructor initializes its member variables to empty string.
     */
    public DrugRecurrenceTag() {
        this.strDrugRecurActionMedDraVersion = "v. 2.3";
        this.strDrugRecurAction = "";
    }

    /**
     *
     * @param DrugRecurActionMedDraVersion <code><b>String</b></code> value to set &lt;drugrecuractionmeddraversion&gt; tag.
     * @param DrugRecurAction              <code><b>String</b></code> value to set &lt;drugrecuraction&gt; tag.
     *
     * @deprecated
     */
    @Deprecated
    public DrugRecurrenceTag(String DrugRecurActionMedDraVersion,
            String DrugRecurAction) {
        this.strDrugRecurActionMedDraVersion = "v. 2.3";
        this.strDrugRecurAction = DrugRecurAction;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        if (this.strDrugRecurActionMedDraVersion.length() > MAXLEN_DRUG_RECURACTION_MEDDRA_VERSION) {
            throw new InvalidDataException("Length of DrugRecurActionMedDraVersion must be less than " + MAXLEN_DRUG_RECURACTION_MEDDRA_VERSION);
        }
        if (this.strDrugRecurAction.length() > MAXLEN_DRUG_RECURACTION) {
            throw new InvalidDataException("Length of DrugRecurAction must be less than " + MAXLEN_DRUG_RECURACTION);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strDrugRecurActionMedDraVersion = null;
        this.strDrugRecurAction = null;
    }
}
