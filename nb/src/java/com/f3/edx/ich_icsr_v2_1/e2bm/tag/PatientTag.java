package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * B.1 Patient characteristics
 * <p>Ref. <b>ich-icsr-v2_1.dtd</b></p>
 * <p>This class is to handle <b>B.1 Patient</b></p>
 * 
 * @author Anoop Varma
 */
public class PatientTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 8545828465449469872L;
    private final int MAXLEN_PATIENTINITIAL = 10;
    private final int MAXLEN_PATIENTGPMEDICALRECORDNUMB = 20;
    private final int MAXLEN_PATIENTSPECIALISTRECORDNUMB = 20;
    private final int MAXLEN_PATIENTHOSPITALRECORDNUMB = 20;
    private final int MAXLEN_PATIENTINVESTIGATIONNUMB = 20;
    private final int MAXLEN_PATIENTBIRTHDATEFORMAT = 3;
    private final int MAXLEN_PATIENTBIRTHDATE = 8;
    private final int MAXLEN_PATIENTONSETAGE = 5;
    private final int MAXLEN_PATIENTONSETAGEUNIT = 3;
    private final int MAXLEN_GESTATIONPERIOD = 3;
    private final int MAXLEN_GESTATIONPERIODUNIT = 3;
    private final int MAXLEN_PATIENTAGEGROUP = 1;
    private final int MAXLEN_PATIENTWEIGHT = 6;
    private final int MAXLEN_PATIENTHEIGHT = 3;
    private final int MAXLEN_PATIENTSEX = 1;
    private final int MAXLEN_LASTMENSTRUALDATEFORMAT = 3;
    private final int MAXLEN_PATIENTLASTMENSTRUALDATE = 8;
    private final int MAXLEN_PATIENTMEDICALHISTORYTEXT = 10000;
    private final int MAXLEN_RESULTSTESTSPROCEDURES = 2000;
    /////////////////////////////////////////////////////
    private String strPatientInitial;
    private String strPatientGpMedicalRecordNumb;
    private String strPatientSpecialistRecordNumb;
    private String strPatientHospitalRecordNumb;
    private String strPatientInvestigationNumb;
    private String strPatientBirthDateFormat;
    private String strPatientBirthDate;
    private String strPatientOnsetAge;
    private String strPatientOnsetAgeUnit;
    private String strGestationPeriod;
    private String strGestationPeriodUnit;
    private String strPatientAgeGroup;
    private String strPatientWeight;
    private String strPatientHeight;
    private String strPatientSex;
    private String strLastmenstrualDateFormat;
    private String strPatientLastMenstrualDate;
    private String strPatientMedicalHistoryText;
    private String strResultsTestsProcedures;
    private ArrayList<MedicalHistoryEpisodeTag> alMedicalHistoryEpisode;
    private ArrayList<PatientPastDrugTherapyTag> alPatientPastDrugTherapy;
    private ArrayList<PatientDeathTag> alPatientDeath;
    private ArrayList<ParentTag> alParent;
    private ArrayList<ReactionTag> alReaction;
    private ArrayList<TestTag> alTest;
    private ArrayList<DrugTag> alDrug;
    private ArrayList<SummaryTag> alSummary;

    // Constructors
    /**
     * Default constructor that creates a new instance of <code><b>PatientTag</b></code>.
     * This constructor initializes its member variables to empty string.
     */
    public PatientTag() {
        this.strPatientInitial = "";
        this.strPatientGpMedicalRecordNumb = "";
        this.strPatientSpecialistRecordNumb = "";
        this.strPatientHospitalRecordNumb = "";
        this.strPatientInvestigationNumb = "";
        this.strPatientBirthDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strPatientBirthDate = "";
        this.strPatientOnsetAge = "";
        this.strPatientOnsetAgeUnit = "";
        this.strGestationPeriod = "";
        this.strGestationPeriodUnit = "";
        this.strPatientAgeGroup = "";
        this.strPatientWeight = "";
        this.strPatientHeight = "";
        this.strPatientSex = "";
        this.strLastmenstrualDateFormat = "";
        this.strPatientLastMenstrualDate = "";
        this.strPatientMedicalHistoryText = "";
        this.strResultsTestsProcedures = "";

        this.alMedicalHistoryEpisode = null;
        this.alPatientPastDrugTherapy = null;
        this.alPatientDeath = null;
        this.alParent = null;
        this.alReaction = null;
        this.alTest = null;
        this.alDrug = null;
        this.alSummary = null;
    }

    /**
     * 
     * @param PatientInitial <code><b>String</b></code> value to set &lt;patientinitial&gt; tag.
     * @param PatientGpMedicalRecordNumb <code><b>String</b></code> value to set &lt;patientgpmedicalrecordnumb&gt; tag.
     * @param PatientSpecialistRecordNumb <code><b>String</b></code> value to set &lt;patientspecialistrecordnumb&gt; tag.
     * @param PatientHospitalRecordNumb <code><b>String</b></code> value to set &lt;patienthospitalrecordnumb&gt; tag.
     * @param PatientInvestigationNumb <code><b>String</b></code> value to set &lt;patientinvestigationnumb&gt; tag.
     * @param PatientBirthDateFormat <code><b>String</b></code> value to set &lt;patientbirthdateformat&gt; tag.
     * @param PatientBirthDate <code><b>String</b></code> value to set &lt;patientbirthdate&gt; tag.
     * @param PatientOnsetAge <code><b>String</b></code> value to set &lt;patientonsetage&gt; tag.
     * @param PatientOnsetAgeUnit <code><b>String</b></code> value to set &lt;patientonsetageunit&gt; tag.
     * @param GestationPeriod <code><b>String</b></code> value to set &lt;gestationperiod&gt; tag.
     * @param GestationPeriodUnit <code><b>String</b></code> value to set &lt;gestationperiodunit&gt; tag.
     * @param PatientAgeGroup <code><b>String</b></code> value to set &lt;patientagegroup&gt; tag.
     * @param PatientWeight <code><b>String</b></code> value to set &lt;patientweight&gt; tag.
     * @param PatientHeight <code><b>String</b></code> value to set &lt;patientheight&gt; tag.
     * @param PatientSex <code><b>String</b></code> value to set &lt;patientsex&gt; tag.
     * @param LastmenstrualDateFormat <code><b>String</b></code> value to set &lt;lastmenstrualdateformat&gt; tag.
     * @param PatientLastMenstrualDate <code><b>String</b></code> value to set &lt;patientLastmenstrualdate&gt; tag.
     * @param PatientMedicalHistoryText <code><b>String</b></code> value to set &lt;patientmedicalhistorytext&gt; tag.
     * @param ResultsTestsProcedures <code><b>String</b></code> value to set &lt;resultstestsprocedures&gt; tag.
     * @param MedicalHistoryEpisode <code><b>String</b></code> value to set &lt;medicalhistoryepisode&gt; tag.
     * @param PatientPastDrugTherapy <code><b>ArrayList</b></code> value(s) to set &lt;patientpastdrugtherapy&gt; tag(s).
     * @param PatientDeath <code><b>ArrayList</b></code> value(s) to set &lt;patientdeath&gt; tag(s).
     * @param Parent <code><b>ArrayList</b></code> value(s) to set &lt;parent&gt; tag(s).
     * @param Reaction <code><b>ArrayList</b></code> value(s) to set &lt;reaction&gt; tag(s).
     * @param Test <code><b>ArrayList</b></code> value(s) to set &lt;test&gt; tag(s).
     * @param Drug <code><b>ArrayList</b></code> value(s) to set &lt;drug&gt; tag(s).
     * @param Summary <code><b>ArrayList</b></code> value(s) to set &lt;summary&gt; tag(s).
     * @deprecated 
     */
    @Deprecated
    public PatientTag(
            String PatientInitial,
            String PatientGpMedicalRecordNumb,
            String PatientSpecialistRecordNumb,
            String PatientHospitalRecordNumb,
            String PatientInvestigationNumb,
            String PatientBirthDateFormat,
            String PatientBirthDate,
            String PatientOnsetAge,
            String PatientOnsetAgeUnit,
            String GestationPeriod,
            String GestationPeriodUnit,
            String PatientAgeGroup,
            String PatientWeight,
            String PatientHeight,
            String PatientSex,
            String LastmenstrualDateFormat,
            String PatientLastMenstrualDate,
            String PatientMedicalHistoryText,
            String ResultsTestsProcedures,
            ArrayList<MedicalHistoryEpisodeTag> MedicalHistoryEpisode,
            ArrayList<PatientPastDrugTherapyTag> PatientPastDrugTherapy,
            ArrayList<PatientDeathTag> PatientDeath,
            ArrayList<ParentTag> Parent,
            ArrayList<ReactionTag> Reaction,
            ArrayList<TestTag> Test,
            ArrayList<DrugTag> Drug,
            ArrayList<SummaryTag> Summary) {
        this.strPatientInitial = PatientInitial;
        this.strPatientGpMedicalRecordNumb = PatientGpMedicalRecordNumb;
        this.strPatientSpecialistRecordNumb = PatientSpecialistRecordNumb;
        this.strPatientHospitalRecordNumb = PatientHospitalRecordNumb;
        this.strPatientInvestigationNumb = PatientInvestigationNumb;
        this.strPatientBirthDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strPatientBirthDate = PatientBirthDate;
        this.strPatientOnsetAge = PatientOnsetAge;
        this.strPatientOnsetAgeUnit = PatientOnsetAgeUnit;
        this.strGestationPeriod = GestationPeriod;
        this.strGestationPeriodUnit = GestationPeriodUnit;
        this.strPatientAgeGroup = PatientAgeGroup;
        this.strPatientWeight = PatientWeight;
        this.strPatientHeight = PatientHeight;
        this.strPatientSex = PatientSex;
        this.strLastmenstrualDateFormat = LastmenstrualDateFormat;
        this.strPatientLastMenstrualDate = PatientLastMenstrualDate;
        this.strPatientMedicalHistoryText = PatientMedicalHistoryText;
        this.strResultsTestsProcedures = ResultsTestsProcedures;

        this.alMedicalHistoryEpisode = MedicalHistoryEpisode;
        this.alPatientPastDrugTherapy = PatientPastDrugTherapy;
        this.alPatientDeath = PatientDeath;
        this.alParent = Parent;
        this.alReaction = Reaction;
        this.alTest = Test;
        this.alDrug = Drug;
        this.alSummary = Summary;
    }

    /**
     * Generates valid and well formed &lt;patient&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;patient&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<patient> \n");
            sb.append("	   <patientinitial>" + strPatientInitial + "</patientinitial> \n");
            sb.append("	   <patientgpmedicalrecordnumb>" + strPatientGpMedicalRecordNumb + "</patientgpmedicalrecordnumb> \n");
            sb.append("	   <patientspecialistrecordnumb>" + strPatientSpecialistRecordNumb + "</patientspecialistrecordnumb> \n");
            sb.append("	   <patienthospitalrecordnumb>" + strPatientHospitalRecordNumb + "</patienthospitalrecordnumb> \n");
            sb.append("	   <patientinvestigationnumb>" + strPatientInvestigationNumb + "</patientinvestigationnumb> \n");
            sb.append("	   <patientbirthdateformat>" + strPatientBirthDateFormat + "</patientbirthdateformat> \n");
            sb.append("	   <patientbirthdate>" + strPatientBirthDate + "</patientbirthdate> \n");
            sb.append("	   <patientonsetage>" + strPatientOnsetAge + "</patientonsetage> \n");
            sb.append("	   <patientonsetageunit>" + strPatientOnsetAgeUnit + "</patientonsetageunit> \n");
            sb.append("	   <gestationperiod>" + strGestationPeriod + "</gestationperiod> \n");
            sb.append("	   <gestationperiodunit>" + strGestationPeriodUnit + "</gestationperiodunit> \n");
            sb.append("	   <patientagegroup>" + strPatientAgeGroup + "</patientagegroup> \n");
            sb.append("	   <patientweight>" + strPatientWeight + "</patientweight> \n");
            sb.append("	   <patientheight>" + strPatientHeight + "</patientheight> \n");
            sb.append("	   <patientsex>" + strPatientSex + "</patientsex> \n");
            sb.append("	   <lastmenstrualdateformat>" + strLastmenstrualDateFormat + "</lastmenstrualdateformat> \n");
            sb.append("	   <patientlastmenstrualdate>" + strPatientLastMenstrualDate + "</patientlastmenstrualdate> \n");
            sb.append("	   <patientmedicalhistorytext>" + strPatientMedicalHistoryText + "</patientmedicalhistorytext> \n");
            sb.append("	   <resultstestsprocedures>" + strResultsTestsProcedures + "</resultstestsprocedures> \n");

            if (alMedicalHistoryEpisode != null) {
                for (int i = 0; i < alMedicalHistoryEpisode.size(); i++) {
                    sb.append("    <!-- MedicalHistoryEpisode tag : " + (i + 1) + " of " + alMedicalHistoryEpisode.size() + " --> \n");
                    sb.append("    " + alMedicalHistoryEpisode.get(i).getTag());
                }
            }

            if (alPatientPastDrugTherapy != null) {
                for (int i = 0; i < alPatientPastDrugTherapy.size(); i++) {
                    sb.append("    <!-- PatientPastDrugTherapy tag : " + (i + 1) + " of " + alPatientPastDrugTherapy.size() + " --> \n");
                    sb.append("    " + alPatientPastDrugTherapy.get(i).getTag());
                }
            }

            if (alPatientDeath != null) {
                for (int i = 0; i < alPatientDeath.size(); i++) {
                    sb.append("    <!-- PatientDeath tag : " + (i + 1) + " of " + alPatientDeath.size() + " --> \n");
                    sb.append("    " + alPatientDeath.get(i).getTag());
                }
            }

            if (alParent != null) {
                for (int i = 0; i < alParent.size(); i++) {
                    sb.append("    <!-- Parent tag : " + (i + 1) + " of " + alParent.size() + " --> \n");
                    sb.append("    " + alParent.get(i).getTag());
                }
            }

            if (alReaction != null) {
                for (int i = 0; i < alReaction.size(); i++) {
                    sb.append("    <!-- Reaction tag : " + (i + 1) + " of " + alReaction.size() + " --> \n");
                    sb.append("    " + alReaction.get(i).getTag());
                }
            }

            if (alTest != null) {
                for (int i = 0; i < alTest.size(); i++) {
                    sb.append("    <!-- Test tag : " + (i + 1) + " of " + alTest.size() + " --> \n");
                    sb.append("    " + alTest.get(i).getTag());
                }
            }

            if (alDrug != null) {
                for (int i = 0; i < alDrug.size(); i++) {
                    sb.append("    <!-- Drug tag : " + (i + 1) + " of " + alDrug.size() + " --> \n");
                    sb.append("    " + alDrug.get(i).getTag());
                }
            }

            if (alSummary != null) {
                for (int i = 0; i < alSummary.size(); i++) {
                    sb.append("    <!-- Summary tag : " + (i + 1) + " of " + alSummary.size() + " --> \n");
                    sb.append("    " + alSummary.get(i).getTag());
                }
            }
            sb.append("</patient> \n");

            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        if (alReaction.size() == 0) {
            throw new InvalidTagException("Atleast one valid &lt;reaction&gt; tag is required inside &lt;patient&gt;");
        }
        if (alDrug.size() == 0) {
            throw new InvalidTagException("Atleast one valid &lt;drug&gt; tag is required inside &lt;patient&gt;");
        }
        if (alPatientDeath.size() > 1) {
            throw new InvalidTagException(alPatientDeath.size() + "Maximum one valid &lt;patientdeath&gt; tag is allowed inside &lt;patient&gt;");
        }
        if (alParent.size() > 1) {
            throw new InvalidTagException("Maximum one valid &lt;parent&gt; tag is allowed inside &lt;patient&gt;");
        }
        if (alSummary.size() > 1) {
            throw new InvalidTagException("Maximum one valid &lt;summary&gt; tag is allowed inside &lt;patient&gt;");
        }
        if (strPatientInitial.length() > MAXLEN_PATIENTINITIAL) {
            throw new InvalidDataException("Length of PatientInitial must be less than " + MAXLEN_PATIENTINITIAL);
        }
        if (strPatientGpMedicalRecordNumb.length() > MAXLEN_PATIENTGPMEDICALRECORDNUMB) {
            throw new InvalidDataException("Length of PatientGpMedicalRecordNumb must be less than " + MAXLEN_PATIENTGPMEDICALRECORDNUMB);
        }
        if (strPatientSpecialistRecordNumb.length() > MAXLEN_PATIENTSPECIALISTRECORDNUMB) {
            throw new InvalidDataException("Length of PatientSpecialistRecordNumb must be less than " + MAXLEN_PATIENTSPECIALISTRECORDNUMB);
        }
        if (strPatientHospitalRecordNumb.length() > MAXLEN_PATIENTHOSPITALRECORDNUMB) {
            throw new InvalidDataException("Length of PatientHospitalRecordNumb must be less than " + MAXLEN_PATIENTHOSPITALRECORDNUMB);
        }
        if (strPatientInvestigationNumb.length() > MAXLEN_PATIENTINVESTIGATIONNUMB) {
            throw new InvalidDataException("Length of PatientInvestigationNumb must be less than " + MAXLEN_PATIENTINVESTIGATIONNUMB);
        }
        if (strPatientBirthDateFormat.length() > MAXLEN_PATIENTBIRTHDATEFORMAT) {
            throw new InvalidDataException("Length of PatientBirthDateFormat must be less than " + MAXLEN_PATIENTBIRTHDATEFORMAT);
        }
        if (strPatientBirthDate.length() > MAXLEN_PATIENTBIRTHDATE) {
            throw new InvalidDataException("Length of PatientBirthDate must be less than " + MAXLEN_PATIENTBIRTHDATE);
        }
        if (strPatientOnsetAge.length() > MAXLEN_PATIENTONSETAGE) {
            throw new InvalidDataException("Length of PatientOnsetAge must be less than " + MAXLEN_PATIENTONSETAGE);
        }
        if (strPatientOnsetAgeUnit.length() > MAXLEN_PATIENTONSETAGEUNIT) {
            throw new InvalidDataException("Length of PatientOnsetAgeUnit must be less than " + MAXLEN_PATIENTONSETAGEUNIT);
        }
        if (strGestationPeriod.length() > MAXLEN_GESTATIONPERIOD) {
            throw new InvalidDataException("Length of GestationPeriod must be less than " + MAXLEN_GESTATIONPERIOD);
        }
        if (strGestationPeriodUnit.length() > MAXLEN_GESTATIONPERIODUNIT) {
            throw new InvalidDataException("Length of GestationPeriodUnit must be less than " + MAXLEN_GESTATIONPERIODUNIT);
        }
        if (strPatientAgeGroup.length() > MAXLEN_PATIENTAGEGROUP) {
            throw new InvalidDataException("Length of PatientAgeGroup must be less than " + MAXLEN_PATIENTAGEGROUP);
        }
        if (strPatientWeight.length() > MAXLEN_PATIENTWEIGHT) {
            throw new InvalidDataException("Length of PatientWeight must be less than " + MAXLEN_PATIENTWEIGHT);
        }
        if (strPatientHeight.length() > MAXLEN_PATIENTHEIGHT) {
            throw new InvalidDataException("Length of PatientHeight must be less than " + MAXLEN_PATIENTHEIGHT);
        }
        if (strPatientSex.length() > MAXLEN_PATIENTSEX) {
            throw new InvalidDataException("Length of PatientSex must be less than " + MAXLEN_PATIENTSEX);
        }
        if (strLastmenstrualDateFormat.length() > MAXLEN_LASTMENSTRUALDATEFORMAT) {
            throw new InvalidDataException("Length of LastmenstrualDateFormat must be less than " + MAXLEN_LASTMENSTRUALDATEFORMAT);
        }
        if (strPatientLastMenstrualDate.length() > MAXLEN_PATIENTLASTMENSTRUALDATE) {
            throw new InvalidDataException("Length of PatientLastMenstrualDate must be less than " + MAXLEN_PATIENTLASTMENSTRUALDATE);
        }
        if (strPatientMedicalHistoryText.length() > MAXLEN_PATIENTMEDICALHISTORYTEXT) {
            throw new InvalidDataException("Length of PatientMedicalHistoryText must be less than " + MAXLEN_PATIENTMEDICALHISTORYTEXT);
        }
        if (strResultsTestsProcedures.length() > MAXLEN_RESULTSTESTSPROCEDURES) {
            throw new InvalidDataException("Length of ResultsTestsProcedures must be less than " + MAXLEN_RESULTSTESTSPROCEDURES);
        }
        return true;
    }

    @Override
    public void fetchData() {
        if (TagData.getInstance().aCase.getP_Initials() != null) {
            this.strPatientInitial = TagData.getInstance().aCase.getP_Initials();
        }
        this.strPatientGpMedicalRecordNumb = "";
        this.strPatientSpecialistRecordNumb = "";
        this.strPatientHospitalRecordNumb = "";
        this.strPatientInvestigationNumb = "";
        this.strPatientBirthDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", java.util.Locale.US);
        if (TagData.getInstance().aCase.getDate_Of_Birth() != null) {
            this.strPatientBirthDate = sdf.format(TagData.getInstance().aCase.getDate_Of_Birth());
        }
        if (TagData.getInstance().aCase.getAge_Reported() != -1) {
            this.strPatientOnsetAge = "" + TagData.getInstance().aCase.getAge_Reported();
        }
        this.strPatientOnsetAgeUnit = AGE_UNIT_YEAR;
        //
        if (TagData.getInstance().aCase.getPregnancy_Week() != -1) {
            this.strGestationPeriod = "" + TagData.getInstance().aCase.getPregnancy_Week();
        }
        this.strGestationPeriodUnit = AGE_UNIT_WEEK;
        this.strPatientAgeGroup = "";
        if (TagData.getInstance().aCase.getWeight() != -1) {
            this.strPatientWeight = "" + TagData.getInstance().aCase.getWeight();
        }
        if (TagData.getInstance().aCase.getHeight() != -1) {
            this.strPatientHeight = "" + TagData.getInstance().aCase.getHeight();
        }
        if (TagData.getInstance().aCase.getSexcode() != null) {
            if (TagData.getInstance().aCase.getSexcode().equalsIgnoreCase("M")) {
                this.strPatientSex = SEX_CODE_MALE;
            } else {
                this.strPatientSex = SEX_CODE_FEMALE;
            }
        }
        this.strLastmenstrualDateFormat = "";
        this.strPatientLastMenstrualDate = "";

        if (TagData.getInstance().aCase.getShort_History() != null) {
            this.strPatientMedicalHistoryText = TagData.getInstance().aCase.getShort_History();
        }
        this.strResultsTestsProcedures = "";

        this.alMedicalHistoryEpisode = new ArrayList<MedicalHistoryEpisodeTag>();
        MedicalHistoryEpisodeTag mhet = new MedicalHistoryEpisodeTag();
        mhet.fetchData();
        this.alMedicalHistoryEpisode.add(mhet);

        this.alPatientPastDrugTherapy = new ArrayList<PatientPastDrugTherapyTag>();
        PatientPastDrugTherapyTag ppdtt;
        sdf = new SimpleDateFormat("yyyyMMdd", java.util.Locale.US);
        for (int i = 0; i < TagData.getInstance().alCM.size(); i++) {
            ppdtt = new PatientPastDrugTherapyTag(
                    TagData.getInstance().alCM.get(i).getBrand_Nm() == null ? "" : TagData.getInstance().alCM.get(i).getBrand_Nm(),
                    DATE_FORMAT_CODE_CCYYMMDD,
                    TagData.getInstance().alCM.get(i).getTreat_Start() == null ? "" : sdf.format(TagData.getInstance().alCM.get(i).getTreat_Start()),
                    DATE_FORMAT_CODE_CCYYMMDD,
                    TagData.getInstance().alCM.get(i).getTreat_Stop() == null ? "" : sdf.format(TagData.getInstance().alCM.get(i).getTreat_Stop()),
                    "",
                    TagData.getInstance().alCM.get(i).getIndication() == null ? "" : TagData.getInstance().alCM.get(i).getIndication(),
                    "",
                    ""/*No reaction data from CM*/);
            this.alPatientPastDrugTherapy.add(ppdtt);
        }

        this.alPatientDeath = new ArrayList<PatientDeathTag>();
        PatientDeathTag pdt = new PatientDeathTag();
        pdt.fetchData();
        this.alPatientDeath.add(pdt);

        //INFO: Sapher currently don't have this data.
        this.alParent = new ArrayList<ParentTag>();

        this.alReaction = new ArrayList<ReactionTag>();
        ReactionTag rt;
        sdf = new SimpleDateFormat("yyyyMMdd", java.util.Locale.US);
        for (int i = 0; i < TagData.getInstance().alAE.size(); i++) {
            rt = new ReactionTag(
                    "",
                    "",
                    TagData.getInstance().alAE.get(i).getLlt_Desc() == null ? "" : TagData.getInstance().alAE.get(i).getLlt_Desc(),
                    "",
                    TagData.getInstance().alAE.get(i).getDiag_Desc() == null ? "" : TagData.getInstance().alAE.get(i).getDiag_Desc(),
                    "",
                    DATE_FORMAT_CODE_CCYYMMDD,
                    TagData.getInstance().alAE.get(i).getDate_Start() == null ? "" : sdf.format(TagData.getInstance().alAE.get(i).getDate_Start()),
                    DATE_FORMAT_CODE_CCYYMMDD,
                    TagData.getInstance().alAE.get(i).getDate_Stop() == null ? "" : sdf.format(TagData.getInstance().alAE.get(i).getDate_Stop()),
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    /*TODO TagData.getInstance().alAE.get(i).getOutcome() == null ? "" : TagData.getInstance().alAE.get(i).getOutcome()*/
                    "");
            this.alReaction.add(rt);
        }

        //INFO: Sapher currently don't have this data.
        this.alTest = new ArrayList<TestTag>();

        this.alDrug = new ArrayList<DrugTag>();
        DrugTag dt = new DrugTag();
        dt.fetchData();
        this.alDrug.add(dt);

        this.alSummary = new ArrayList<SummaryTag>();
        SummaryTag st = new SummaryTag();
        st.fetchData();
        this.alSummary.add(st);
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPatientInitial = null;
        this.strPatientGpMedicalRecordNumb = null;
        this.strPatientSpecialistRecordNumb = null;
        this.strPatientHospitalRecordNumb = null;
        this.strPatientInvestigationNumb = null;
        this.strPatientBirthDateFormat = null;
        this.strPatientBirthDate = null;
        this.strPatientOnsetAge = null;
        this.strPatientOnsetAgeUnit = null;
        this.strGestationPeriod = null;
        this.strGestationPeriodUnit = null;
        this.strPatientAgeGroup = null;
        this.strPatientWeight = null;
        this.strPatientHeight = null;
        this.strPatientSex = null;
        this.strLastmenstrualDateFormat = null;
        this.strPatientLastMenstrualDate = null;
        this.strPatientMedicalHistoryText = null;
        this.strResultsTestsProcedures = null;

        this.alMedicalHistoryEpisode = null;
        this.alPatientPastDrugTherapy = null;
        this.alPatientDeath = null;
        this.alParent = null;
        this.alReaction = null;
        this.alTest = null;
        this.alDrug = null;
        this.alSummary = null;
    }
}
