package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 * 
 * @author Anoop Varma
 */
public class TestTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 1620604451678099596L;
    private final int MAXLEN_TESTDATEFORMAT = 3;
    private final int MAXLEN_TESTDATE = 8;
    private final int MAXLEN_TESTNAME = 100;
    private final int MAXLEN_TESTRESULT = 50;
    private final int MAXLEN_TESTUNIT = 35;
    private final int MAXLEN_LOWTESTRANGE = 50;
    private final int MAXLEN_HIGHTESTRANGE = 50;
    private final int MAXLEN_MOREINFORMATION = 1;
    /////////////////////////////////////////////
    private String strTestDateFormat = "";
    private String strTestDate = "";
    private String strTestName = "";
    private String strTestResult = "";
    private String strTestUnit = "";
    private String strLowTestRange = "";
    private String strHighTestRange = "";
    private String strMoreInformation = "";

    @Override
    public void fetchData() {
    }

    // Constructors
    /**
     * Default constructor that creates a new instance of TestTag.
     * This constructor initializes its member variables to empty string.
     */
    public TestTag() {
        this.strTestDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strTestDate = "";
        this.strTestName = "";
        this.strTestResult = "";
        this.strTestUnit = "";
        this.strLowTestRange = "";
        this.strHighTestRange = "";
        this.strMoreInformation = "";
    }

    /**
     * Constructor that creates a new instance of TestTag.
     *
     * @param TestDateFormat  <code><b>String</b></code> value to set &lt;TestDateFormat&gt; tag.
     * @param TestDate        <code><b>String</b></code> value to set &lt;TestDate&gt; tag.
     * @param TestName        <code><b>String</b></code> value to set &lt;TestName&gt; tag.
     * @param TestResult      <code><b>String</b></code> value to set &lt;TestResult&gt; tag.
     * @param TestUnit        <code><b>String</b></code> value to set &lt;TestUnit&gt; tag.
     * @param LowTestRange    <code><b>String</b></code> value to set &lt;LowTestRange&gt; tag.
     * @param HighTestRange   <code><b>String</b></code> value to set &lt;HighTestRange&gt; tag.
     * @param MoreInformation <code><b>String</b></code> value to set &lt;MoreInformation&gt; tag.
     * @deprecated
     */
    @Deprecated
    public TestTag(
            String TestDateFormat,
            String TestDate,
            String TestName,
            String TestResult,
            String TestUnit,
            String LowTestRange,
            String HighTestRange,
            String MoreInformation) {
        this.strTestDateFormat = TestDateFormat;
        this.strTestDate = TestDate;
        this.strTestName = TestName;
        this.strTestResult = TestResult;
        this.strTestUnit = TestUnit;
        this.strLowTestRange = LowTestRange;
        this.strHighTestRange = HighTestRange;
        this.strMoreInformation = MoreInformation;
    }

    /**
     * Generates valid and well formed &lt;test&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;test&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<test> \n");
            sb.append("    <testdateformat>" + strTestDateFormat + "</testdateformat> \n");
            sb.append("    <testdate>" + strTestDate + "</testdate> \n");
            sb.append("    <testname>" + strTestName + "</testname> \n");
            sb.append("    <testresult>" + strTestResult + "</testresult> \n");
            sb.append("    <testunit>" + strTestUnit + "</testunit> \n");
            sb.append("    <lowtestrange>" + strLowTestRange + "</lowtestrange> \n");
            sb.append("    <hightestrange>" + strHighTestRange + "</hightestrange> \n");
            sb.append("    <moreinformation>" + strMoreInformation + "</moreinformation> \n");
            sb.append("</test> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // No mandatory fields. So, no InvalidTagException

        if (strTestDateFormat.length() > MAXLEN_TESTDATEFORMAT) {
            throw new InvalidDataException("Length of TestDateFormat must be less than " + MAXLEN_TESTDATEFORMAT);
        }
        if (strTestDate.length() > MAXLEN_TESTDATE) {
            throw new InvalidDataException("Length of TestDate must be less than " + MAXLEN_TESTDATE);
        }
        if (strTestName.length() > MAXLEN_TESTNAME) {
            throw new InvalidDataException("Length of TestName must be less than " + MAXLEN_TESTNAME);
        }
        if (strTestResult.length() > MAXLEN_TESTRESULT) {
            throw new InvalidDataException("Length of TestResult must be less than " + MAXLEN_TESTRESULT);
        }
        if (strTestUnit.length() > MAXLEN_TESTUNIT) {
            throw new InvalidDataException("Length of TestUnit must be less than " + MAXLEN_TESTUNIT);
        }
        if (strLowTestRange.length() > MAXLEN_LOWTESTRANGE) {
            throw new InvalidDataException("Length of LowTestRange must be less than " + MAXLEN_LOWTESTRANGE);
        }
        if (strHighTestRange.length() > MAXLEN_HIGHTESTRANGE) {
            throw new InvalidDataException("Length of HighTestRange must be less than " + MAXLEN_HIGHTESTRANGE);
        }
        if (strMoreInformation.length() > MAXLEN_MOREINFORMATION) {
            throw new InvalidDataException("Length of MoreInformation must be less than " + MAXLEN_MOREINFORMATION);
        }

        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strTestDateFormat = null;
        this.strTestDate = null;
        this.strTestName = null;
        this.strTestResult = null;
        this.strTestUnit = null;
        this.strLowTestRange = null;
        this.strHighTestRange = null;
        this.strMoreInformation = null;
    }
}
