package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.util.ArrayList;

/**
 *
 * @author Anoop Varma
 */
public class IcsrMessage implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -3835083465177441278L;
    //private final String XML_DIR = "\\XML\\";
    /**
     * IchIcsrMessageHeaderTag
     * Only one tag in ICSR Message.
     */
    private IchIcsrMessageHeaderTag ichIcsrMessageHeaderTag = null;
    /**
     * SafetyReportTag
     * Atleast one tag required in ICSR Message.
     */
    private ArrayList<SafetyReportTag> alSafetyReportTag = null;

//    /**
//     * Default constructor that creates a new instance of <code><b>IcsrMessage</b></code>
//     */
//    public IcsrMessage() {
//        initData("");
//        // for testing
//        this.ichIcsrMessageHeaderTag = new IchIcsrMessageHeaderTag();
//        this.alSafetyReportTag = new ArrayList<SafetyReportTag>();
//        alSafetyReportTag.add(new SafetyReportTag());
//    }
    /**
     * Constructor that creates a new instance of <code><b>IcsrMessage</b></code>
     * @param ReportNumber 
     */
    public IcsrMessage(String cpnyName, String ReportNumber) {
        initData(cpnyName, ReportNumber);

        this.ichIcsrMessageHeaderTag = new IchIcsrMessageHeaderTag();
        this.alSafetyReportTag = new ArrayList<SafetyReportTag>();
        alSafetyReportTag.add(new SafetyReportTag());
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        if (alSafetyReportTag.size() == 0) {
            throw new InvalidTagException("");
        }
        return true;
    }

    /**
     *
     * @param cpnyName
     * @param ReportNum
     */
    public void initData(String cpnyName, String ReportNum) {
        TagData td = TagData.getInstance();
        td.setRepNumber(cpnyName, ReportNum);
    }

    /**
     * Generates valid and well formed &lt;icsrmessage&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;icsrmessage&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();

            sb.append("<ichicsr lang=\"en\"> \n");
            sb.append("    " + ichIcsrMessageHeaderTag.getTag());

            if (alSafetyReportTag != null) {
                for (int i = 0; i < alSafetyReportTag.size(); i++) {
                    sb.append("    <!-- SafetyReportTag tag : " + (i + 1) + " of " + alSafetyReportTag.size() + " --> \n");
                    sb.append("    " + alSafetyReportTag.get(i).getTag());
                }
            }

            sb.append("</ichicsr> \n");

            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            //throw new Exception(ite.getMessage());
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            //throw new Exception(ide.getMessage());
            throw ide;
        }
    }

    /**
     *
     */
    @Override
    public void fetchData() {
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.ichIcsrMessageHeaderTag = null;
        this.alSafetyReportTag = null;
    }
}
