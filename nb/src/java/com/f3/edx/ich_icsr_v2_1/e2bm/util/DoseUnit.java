package com.f3.edx.ich_icsr_v2_1.e2bm.util;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Anoop Varma.
 */
public class DoseUnit implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = -3327679187276213623L;
    private static DoseUnit me = null;
    private ArrayList<Dose> alDose;

    /**
     * DoseUnit constructor that creates all Dose Unit options as per the ICH ICSR specification. [Ref: ICH ICSR V2.1]
     */
    public DoseUnit() {
        alDose = new ArrayList<Dose>();

        alDose.add(new Dose("001", "kg", "kilogram(s)"));
        alDose.add(new Dose("002", "G", "gram(s)"));
        alDose.add(new Dose("003", "Mg", "milligram(s)"));
        alDose.add(new Dose("004", "�g", "microgram(s)"));
        alDose.add(new Dose("005", "ng", "nanogram(s)"));
        alDose.add(new Dose("006", "pg", "picogram(s)"));
        alDose.add(new Dose("007", "mg/kg", "milligram(s)/kilogram"));
        alDose.add(new Dose("008", "�g/kg", "microgram(s)/kilogram"));
        alDose.add(new Dose("009", "mg/m2", "milligram(s)/sq. meter"));
        alDose.add(new Dose("010", "�g/m2", "microgram(s)/ sq. Meter"));
        alDose.add(new Dose("011", "l", "litre(s)"));
        alDose.add(new Dose("012", "ml", "millilitre(s)"));
        alDose.add(new Dose("013", "�l", "microlitre(s)"));
        alDose.add(new Dose("014", "Bq", "becquerel(s)"));
        alDose.add(new Dose("015", "GBq", "gigabecquerel(s)"));
        alDose.add(new Dose("016", "MBq", "megabecquerel(s)"));
        alDose.add(new Dose("017", "Kbq", "kilobecquerel(s)"));
        alDose.add(new Dose("018", "Ci", "curie(s)"));
        alDose.add(new Dose("019", "MCi", "millicurie(s)"));
        alDose.add(new Dose("020", "�Ci", "microcurie(s)"));
        alDose.add(new Dose("021", "NCi", "nanocurie(s)"));
        alDose.add(new Dose("022", "Mol", "mole(s)"));
        alDose.add(new Dose("023", "Mmol", "millimole(s)"));
        alDose.add(new Dose("024", "�mol", "micromole(s)"));
        alDose.add(new Dose("025", "Iu", "international unit(s)"));
        alDose.add(new Dose("026", "Kiu", "iu(1000s)"));
        alDose.add(new Dose("027", "Miu", "iu(1,000,000s)"));
        alDose.add(new Dose("028", "iu/kg", "iu/kilogram"));
        alDose.add(new Dose("029", "Meq", "milliequivalent(s)"));
        alDose.add(new Dose("030", "%", "percent"));
        alDose.add(new Dose("031", "Gtt", "drop(s)"));
        alDose.add(new Dose("032", "DF", "dosage form"));
    }

    /**
     * <code><b>DoseUnit</b></code> is a singleton class. So, <code><b>getInstance()</b></code> must be used to get te instance of <code><b>DoseUnit</b></code>.
     * @return <code><b>DoseUnit</b></code> If there is no instance of <code><b>DoseUnit</b></code>, returns a new instance.
     */
    public DoseUnit getInstance() {
        if (me == null) {
            return new DoseUnit();
        }
        return me;
    }

    /**
     * 
     * @param shortDesc
     * @return
     */
    public String getDoseUnitCode(String shortDesc) {
        Dose d = null;
        String str = "";
        for (int i = 0; i < alDose.size(); i++) {
            d = alDose.get(i);
            if (d.getShortDesc().contains(shortDesc)) {
                str = d.getCode();
                break;
            }
        }

        d = null;
        return str;
    }
}

/**
 * Class that manages a Dose, while preparing XML for EMEA submission.
 *
 * @author Anoop Varma
 */
class Dose {

    private String strDoseCode;
    private String strDoseShortDesc;
    private String strDoseDesc;

    /**
     * Default constructor for the <code><b>Dose</b></code> class
     */
    public Dose() {
        strDoseCode = "";
        strDoseShortDesc = "";
        strDoseDesc = "";
    }

    /**
     * Constructor for the <code><b>Dose</b></code> class
     * 
     * @param doseCode     <code><b>String</b></code>
     * @param shortDesc as <code><b>String</b></code>
     * @param desc         <code><b>String</b></code>
     */
    public Dose(String doseCode, String shortDesc, String desc) {
        strDoseCode = doseCode;
        strDoseShortDesc = shortDesc;
        strDoseDesc = desc;
    }

    /**
     * Function to return the Code of a specific dose.
     * @return Dose Code as <code><b>String</b></code>
     */
    public String getCode() {
        return strDoseCode;
    }

    /**
     * Function to return the short description of a specific dose.
     * @return Short description of a code as <code><b>String</b></code>
     */
    public String getShortDesc() {
        return strDoseShortDesc;
    }

    /**
     * Function to return the detailed description of a specific dose.
     * @return Description of a code as <code><b>String</b></code>
     */
    public String getDesc() {
        return strDoseDesc;
    }
}
