package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Anoop Varma
 */
public class MedicalHistoryEpisodeTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 496616065355623040L;
    private final int MAXLEN_PATIENTEPISODENAMEMEDDRAVERSION = 8;
    private final int MAXLEN_PATIENTEPISODENAME = 250;
    private final int MAXLEN_PATIENTMEDICALSTARTDATEFORMAT = 3;
    private final int MAXLEN_PATIENTMEDICALSTARTDATE = 8;
    private final int MAXLEN_PATIENTMEDICALCONTINUE = 1;
    private final int MAXLEN_PATIENTMEDICALENDDATEFORMAT = 3;
    private final int MAXLEN_PATIENTMEDICALENDDATE = 8;
    private final int MAXLEN_PATIENTMEDICALCOMMENT = 100;
    /////////////////////////////////////////////////////////
    private String strPatientEpisodeNameMeddraVersion = "";
    private String strPatientEpisodeName = "";
    private String strPatientMedicalStartDateFormat = "";
    private String strPatientMedicalStartDate = "";
    private String strPatientMedicalContinue = "";
    private String strPatientMedicalEndDateFormat = "";
    private String strPatientMedicalEndDate = "";
    private String strPatientMedicalComment = "";

    /**
     * Generates valid and well formed &lt;medicalhistoryepisode&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;medicalhistoryepisode&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<medicalhistoryepisode> \n");
            sb.append("    <patientepisodenamemeddraversion>" + strPatientEpisodeNameMeddraVersion + "</patientepisodenamemeddraversion>  \n");
            sb.append("    <patientepisodename>" + strPatientEpisodeName + "</patientepisodename>  \n");
            sb.append("    <patientmedicalstartdateformat>" + strPatientMedicalStartDateFormat + "</patientmedicalstartdateformat>  \n");
            sb.append("    <patientmedicalstartdate>" + strPatientMedicalStartDate + "</patientmedicalstartdate>  \n");
            sb.append("    <patientmedicalcontinue>" + strPatientMedicalContinue + "</patientmedicalcontinue>  \n");
            sb.append("    <patientmedicalenddateformat>" + strPatientMedicalEndDateFormat + "</patientmedicalenddateformat>  \n");
            sb.append("    <patientmedicalenddate>" + strPatientMedicalEndDate + "</patientmedicalenddate>  \n");
            sb.append("    <patientmedicalcomment>" + strPatientMedicalComment + "</patientmedicalcomment>  \n");
            sb.append("</medicalhistoryepisode> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    // Constructors
    /**
     * Default constructor that creates a new instance of MedicalHistoryEpisodeTag.
     * This constructor initializes its member variables to empty string.
     */
    public MedicalHistoryEpisodeTag() {
        this.strPatientEpisodeNameMeddraVersion = "";
        this.strPatientEpisodeName = "";
        this.strPatientMedicalStartDateFormat = "";
        this.strPatientMedicalStartDate = "";
        this.strPatientMedicalContinue = "";
        this.strPatientMedicalEndDateFormat = "";
        this.strPatientMedicalEndDate = "";
        this.strPatientMedicalComment = "";
    }

    /**
     *
     * @param PatientEpisodeNameMeddraVersion as <code><b>String</b></code>
     * @param PatientEpisodeName              as <code><b>String</b></code>
     * @param PatientMedicalStartDateFormat   as <code><b>String</b></code>
     * @param PatientMedicalStartDate         as <code><b>String</b></code>
     * @param PatientMedicalContinue          as <code><b>String</b></code>
     * @param PatientMedicalEndDateFormat     as <code><b>String</b></code>
     * @param PatientMedicalEndDate           as <code><b>String</b></code>
     * @param PatientMedicalComment           as <code><b>String</b></code>
     * @deprecated
     */
    @Deprecated
    public MedicalHistoryEpisodeTag(
            String PatientEpisodeNameMeddraVersion,
            String PatientEpisodeName,
            String PatientMedicalStartDateFormat,
            String PatientMedicalStartDate,
            String PatientMedicalContinue,
            String PatientMedicalEndDateFormat,
            String PatientMedicalEndDate,
            String PatientMedicalComment) {
        this.strPatientEpisodeNameMeddraVersion = PatientEpisodeNameMeddraVersion;
        this.strPatientEpisodeName = PatientEpisodeName;
        this.strPatientMedicalStartDateFormat = PatientMedicalStartDateFormat;
        this.strPatientMedicalStartDate = PatientMedicalStartDate;
        this.strPatientMedicalContinue = PatientMedicalContinue;
        this.strPatientMedicalEndDateFormat = PatientMedicalEndDateFormat;
        this.strPatientMedicalEndDate = PatientMedicalEndDate;
        this.strPatientMedicalComment = PatientMedicalComment;
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
        this.strPatientEpisodeNameMeddraVersion = "";
        this.strPatientEpisodeName = "";
        this.strPatientMedicalStartDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", java.util.Locale.US);
        if (TagData.getInstance().aCase.getTreat_Start() != null) {
            this.strPatientMedicalStartDate = sdf.format(TagData.getInstance().aCase.getTreat_Start());
        }
        this.strPatientMedicalContinue = "";
        this.strPatientMedicalEndDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        if (TagData.getInstance().aCase.getTreat_Stop() != null) {
            this.strPatientMedicalEndDate = sdf.format(TagData.getInstance().aCase.getTreat_Stop());
        }
        this.strPatientMedicalComment = "";
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {

        if (strPatientEpisodeNameMeddraVersion.length() > MAXLEN_PATIENTEPISODENAMEMEDDRAVERSION) {
            throw new InvalidDataException("Length of PatientEpisodeNameMeddraVersion must be less than " + MAXLEN_PATIENTEPISODENAMEMEDDRAVERSION);
        }
        if (strPatientEpisodeName.length() > MAXLEN_PATIENTEPISODENAME) {
            throw new InvalidDataException("Length of PatientEpisodeName must be less than " + MAXLEN_PATIENTEPISODENAME);
        }
        if (strPatientMedicalStartDateFormat.length() > MAXLEN_PATIENTMEDICALSTARTDATEFORMAT) {
            throw new InvalidDataException("Length of PatientMedicalStartDateFormat must be less than " + MAXLEN_PATIENTMEDICALSTARTDATEFORMAT);
        }
        if (strPatientMedicalStartDate.length() > MAXLEN_PATIENTMEDICALSTARTDATE) {
            throw new InvalidDataException("Length of PatientMedicalStartDate must be less than " + MAXLEN_PATIENTMEDICALSTARTDATE);
        }
        if (strPatientMedicalContinue.length() > MAXLEN_PATIENTMEDICALCONTINUE) {
            throw new InvalidDataException("Length of PatientMedicalContinue must be less than " + MAXLEN_PATIENTMEDICALCONTINUE);
        }
        if (strPatientMedicalEndDateFormat.length() > MAXLEN_PATIENTMEDICALENDDATEFORMAT) {
            throw new InvalidDataException("Length of PatientMedicalEndDateFormat must be less than " + MAXLEN_PATIENTMEDICALENDDATEFORMAT);
        }
        if (strPatientMedicalEndDate.length() > MAXLEN_PATIENTMEDICALENDDATE) {
            throw new InvalidDataException("Length of PatientMedicalEndDate must be less than " + MAXLEN_PATIENTMEDICALENDDATE);
        }
        if (strPatientMedicalComment.length() > MAXLEN_PATIENTMEDICALCOMMENT) {
            throw new InvalidDataException("Length of PatientMedicalComment must be less than " + MAXLEN_PATIENTMEDICALCOMMENT);
        }

        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPatientEpisodeNameMeddraVersion = null;
        this.strPatientEpisodeName = null;
        this.strPatientMedicalStartDateFormat = null;
        this.strPatientMedicalStartDate = null;
        this.strPatientMedicalContinue = null;
        this.strPatientMedicalEndDateFormat = null;
        this.strPatientMedicalEndDate = null;
        this.strPatientMedicalComment = null;
    }
}
