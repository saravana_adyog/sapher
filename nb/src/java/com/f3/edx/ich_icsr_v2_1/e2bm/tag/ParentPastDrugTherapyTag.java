package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 * 
 * @author Anoop Varma
 */
public class ParentPastDrugTherapyTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 7589788718027218639L;
    private String strParentDrugName = "";
    private String strParentDrugStartDateFormat = "";
    private String strParentDrugStartDate = "";
    private String strParentDrugEndDateFormat = "";
    private String strParentDrugEndDate = "";
    private String strParentIndicationMedDraVersion = "";
    private String strParentDrugIndication = "";
    private String strParentDrgReactionMedDraVersion = "";
    private String strParentDrugReaction = "";

    /**
     * Generates valid and well formed &lt;parentpastdrugtherapy&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;parentpastdrugtherapy&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<Parentpastdrugtherapy> \n");
            sb.append("    <parentdrugname>" + strParentDrugName + "</parentdrugname> \n");
            sb.append("    <parentdrugstartdateformat>" + strParentDrugStartDateFormat + "</parentdrugstartdateformat> \n");
            sb.append("    <parentdrugstartdate>" + strParentDrugStartDate + "</parentdrugstartdate> \n");
            sb.append("    <parentdrugenddateformat>" + strParentDrugEndDateFormat + "</parentdrugenddateformat> \n");
            sb.append("    <parentdrugenddate>" + strParentDrugEndDate + "</parentdrugenddate> \n");
            sb.append("    <parentindicationmeddraversion>" + strParentIndicationMedDraVersion + "</parentindicationmeddraversion> \n");
            sb.append("    <parentdrugindication>" + strParentDrugIndication + "</parentdrugindication> \n");
            sb.append("    <parentdrgreactionmeddraversion>" + strParentDrgReactionMedDraVersion + "</parentdrgreactionmeddraversion> \n");
            sb.append("    <parentdrugreaction>" + strParentDrugReaction + "</parentdrugreaction> \n");
            sb.append("</parentpastdrugtherapy> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    //Constructors
    /**
     * Default constructor for ParentPastDrugTherapyTag.
     * This constructor initializes its member variables to empty string.
     */
    public ParentPastDrugTherapyTag() {
        this.strParentDrugName = "";
        this.strParentDrugStartDateFormat = "";
        this.strParentDrugStartDate = "";
        this.strParentDrugEndDateFormat = "";
        this.strParentDrugEndDate = "";
        this.strParentIndicationMedDraVersion = "";
        this.strParentDrugIndication = "";
        this.strParentDrgReactionMedDraVersion = "";
        this.strParentDrugReaction = "";
    }

    /**
     * Constructor for ParentPastDrugTherapyTag.
     *
     * @param ParentDrugName                 as <code><b>String</b></code>
     * @param ParentDrugStartDateFormat      as <code><b>String</b></code>
     * @param ParentDrugStartDate            as <code><b>String</b></code>
     * @param ParentDrugEndDateFormat        as <code><b>String</b></code>
     * @param ParentDrugEndDate              as <code><b>String</b></code>
     * @param ParentIndicationMedDraVersion  as <code><b>String</b></code>
     * @param ParentDrugIndication           as <code><b>String</b></code>
     * @param ParentDrgReactionMedDraVersion as <code><b>String</b></code>
     * @param ParentDrugReaction             as <code><b>String</b></code>
     * @deprecated
     */
    @Deprecated
    public ParentPastDrugTherapyTag(
            String ParentDrugName,
            String ParentDrugStartDateFormat,
            String ParentDrugStartDate,
            String ParentDrugEndDateFormat,
            String ParentDrugEndDate,
            String ParentIndicationMedDraVersion,
            String ParentDrugIndication,
            String ParentDrgReactionMedDraVersion,
            String ParentDrugReaction) {
        this.strParentDrugName = ParentDrugName;
        this.strParentDrugStartDateFormat = ParentDrugStartDateFormat;
        this.strParentDrugStartDate = ParentDrugStartDate;
        this.strParentDrugEndDateFormat = ParentDrugEndDateFormat;
        this.strParentDrugEndDate = ParentDrugEndDate;
        this.strParentIndicationMedDraVersion = ParentIndicationMedDraVersion;
        this.strParentDrugIndication = ParentDrugIndication;
        this.strParentDrgReactionMedDraVersion = ParentDrgReactionMedDraVersion;
        this.strParentDrugReaction = ParentDrugReaction;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1] 
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        return true;
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strParentDrugName = null;
        this.strParentDrugStartDateFormat = null;
        this.strParentDrugStartDate = null;
        this.strParentDrugEndDateFormat = null;
        this.strParentDrugEndDate = null;
        this.strParentIndicationMedDraVersion = null;
        this.strParentDrugIndication = null;
        this.strParentDrgReactionMedDraVersion = null;
        this.strParentDrugReaction = null;
    }
}
