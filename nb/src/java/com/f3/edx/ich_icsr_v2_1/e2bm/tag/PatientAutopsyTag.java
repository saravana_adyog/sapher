package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class PatientAutopsyTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 44255500302989975L;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientDetermAutopsMedDraVersion : <code><b>8</b></code></p>
     */
    private final int MAXLEN_PATIENTDETERMAUTOPSMEDDRAVERSION = 8;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientDetermineAutopsy : <code><b>250</b></code></p>
     */
    private final int MAXLEN_PATIENTDETERMINEAUTOPSY = 250;
    private String strPatientDetermAutopsMedDraVersion = "";
    private String strPatientDetermineAutopsy = "";

    /**
     * Generates valid and well formed &lt;patientautopsy&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;patientautopsy&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<patientautopsy>  \n");
            sb.append("    <patientdetermautopsmeddraversion>" + strPatientDetermAutopsMedDraVersion + "</patientdetermautopsmeddraversion>  \n");
            sb.append("    <patientdetermineautopsy>" + strPatientDetermineAutopsy + "</patientdetermineautopsy>  \n");
            sb.append("</patientautopsy>  \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    // Constructors
    /**
     * Default constructor for PatientAutopsyTag.
     * This constructor initializes its member variables to empty string.
     */
    public PatientAutopsyTag() {
        this.strPatientDetermAutopsMedDraVersion = "";
        this.strPatientDetermineAutopsy = "";
    }

    /**
     * Constructor for PatientAutopsyTag.
     * 
     * @param PatientDetermAutopsMedDraVersion as <code><b>String</b></code>
     * @param PatientDetermineAutopsy          as <code><b>String</b></code>
     * @deprecated 
     */
    @Deprecated
    public PatientAutopsyTag(
            String PatientDetermAutopsMedDraVersion,
            String PatientDetermineAutopsy) {
        this.strPatientDetermAutopsMedDraVersion = PatientDetermAutopsMedDraVersion;
        this.strPatientDetermineAutopsy = PatientDetermineAutopsy;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {
        if (this.strPatientDetermAutopsMedDraVersion.length() > MAXLEN_PATIENTDETERMAUTOPSMEDDRAVERSION) {
            throw new InvalidDataException("Length of PatientDetermAutopsMedDraVersion must be less than " + MAXLEN_PATIENTDETERMAUTOPSMEDDRAVERSION);
        }
        if (this.strPatientDetermineAutopsy.length() > MAXLEN_PATIENTDETERMINEAUTOPSY) {
            throw new InvalidDataException("Length of PatientDetermineAutopsy must be less than " + MAXLEN_PATIENTDETERMINEAUTOPSY);
        }
        return true;
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
        this.strPatientDetermAutopsMedDraVersion = "";
        this.strPatientDetermineAutopsy = "";
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPatientDetermAutopsMedDraVersion = null;
        this.strPatientDetermineAutopsy = null;
    }
}
