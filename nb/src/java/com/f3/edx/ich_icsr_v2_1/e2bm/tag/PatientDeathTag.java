package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.util.ArrayList;

/**
 *
 * @author Anoop Varma
 */
public class PatientDeathTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 2499083606552192344L;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientDeathDateFormat : <code><b>3</b></code></p>
     */
    private final int MAXLEN_PATIENT_DEATH_DATE_FORMAT = 3;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientDeathDate : <code><b>8</b></code></p>
     */
    private final int MAXLEN_PATIENT_DEATH_DATE = 8;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientAutopsyYesNo : <code><b>1</b></code></p>
     */
    private final int MAXLEN_PATIENT_AUTOPSY_YESNO = 1;
    private String strPatientDeathDateFormat = "";
    private String strPatientDeathDate = "";
    private String strPatientAutopsyYesNo = "";
    private ArrayList<PatientDeathCauseTag> alPatientDeathCause = null;
    private ArrayList<PatientAutopsyTag> alPatientAutopsy = null;

    /**
     * Generates valid and well formed &lt;patientdeath&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;patientdeath&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();
            
            StringBuffer sb = new StringBuffer();
            sb.append("<patientdeath> \n");
            sb.append("    <patientdeathdateformat>" + strPatientDeathDateFormat + "</patientdeathdateformat> \n");
            sb.append("    <patientdeathdate>" + strPatientDeathDate + "</patientdeathdate> \n");
            sb.append("    <patientautopsyyesno>" + strPatientAutopsyYesNo + "</patientautopsyyesno> \n");

            if (alPatientDeathCause != null) {
                for (int i = 0; i < alPatientDeathCause.size(); i++) {
                    sb.append("    " + alPatientDeathCause.get(i).getTag());
                }
            }

            if (alPatientAutopsy != null) {
                for (int i = 0; i < alPatientAutopsy.size(); i++) {
                    sb.append("    " + alPatientAutopsy.get(i).getTag());
                }
            }
            sb.append("</patientdeath> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
        this.strPatientDeathDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strPatientDeathDate = "";
        this.strPatientAutopsyYesNo = "";
        this.alPatientDeathCause = null;
        this.alPatientAutopsy = null;
    }

    // Constructors
    /**
     * Default constructor for PatientDeathTag.
     * This constructor initializes its member variables to empty string.
     */
    public PatientDeathTag() {
        this.strPatientDeathDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strPatientDeathDate = "";
        if (TagData.getInstance().aCase.getAutopsy() != null) {
            if (TagData.getInstance().aCase.getAutopsy().equals("0")) {
                this.strPatientAutopsyYesNo = AUTOPSY_STATUS_NO;
            } else if (TagData.getInstance().aCase.getAutopsy().equals("1")) {
                this.strPatientAutopsyYesNo = AUTOPSY_STATUS_YES;
            }
        }

        this.alPatientDeathCause = new ArrayList<PatientDeathCauseTag>();
        PatientDeathCauseTag pdct = new PatientDeathCauseTag();
        pdct.fetchData();
        this.alPatientDeathCause.add(pdct);

        this.alPatientAutopsy = new ArrayList<PatientAutopsyTag>();
        PatientAutopsyTag pat = new PatientAutopsyTag();
        pat.fetchData();
        this.alPatientAutopsy.add(pat);
    }

    /**
     * Constructor for PatientDeathTag.
     *
     * @param Patientdeathdateformat as <code><b>String</b></code>
     * @param Patientdeathdate       as <code><b>String</b></code>
     * @param Patientautopsyyesno    as <code><b>String</b></code>
     * @param alPatientDeathCause    as <code><b>ArrayList</b></code>
     * @param alPatientAutopsy       as <code><b>ArrayList</b></code>
     * @deprecated
     */
    @Deprecated
    public PatientDeathTag(String Patientdeathdateformat,
            String Patientdeathdate,
            String Patientautopsyyesno,
            ArrayList<PatientDeathCauseTag> alPatientDeathCause,
            ArrayList<PatientAutopsyTag> alPatientAutopsy) {
        this.strPatientDeathDateFormat = Patientdeathdateformat;
        this.strPatientDeathDate = Patientdeathdate;
        this.strPatientAutopsyYesNo = Patientautopsyyesno;
        this.alPatientDeathCause = alPatientDeathCause;
        this.alPatientAutopsy = alPatientAutopsy;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed
     * as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1] 
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {
        if (this.strPatientDeathDateFormat.length() > MAXLEN_PATIENT_DEATH_DATE_FORMAT) {
            throw new InvalidDataException("Length of PatientDeathDateFormat must be less than " + MAXLEN_PATIENT_DEATH_DATE_FORMAT);
        }
        if (this.strPatientDeathDate.length() > MAXLEN_PATIENT_DEATH_DATE) {
            throw new InvalidDataException("Length of PatientDeathDate must be less than " + MAXLEN_PATIENT_DEATH_DATE);
        }
        if (this.strPatientAutopsyYesNo.length() > MAXLEN_PATIENT_AUTOPSY_YESNO) {
            throw new InvalidDataException("Length of PatientAutopsyYesNo must be less than " + MAXLEN_PATIENT_AUTOPSY_YESNO);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPatientDeathDateFormat = null;
        this.strPatientDeathDate = null;
        this.strPatientAutopsyYesNo = null;
        this.alPatientDeathCause = null;
        this.alPatientAutopsy = null;
    }
}
