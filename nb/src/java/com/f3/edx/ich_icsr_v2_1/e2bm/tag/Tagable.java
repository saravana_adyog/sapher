package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.io.Serializable;

/**
 *
 * @author Anoop Varma
 */
public interface Tagable extends Serializable {

    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>CCYYMMDD Date Format Code <code><b>102</b></code></p>
     */
    public String DATE_FORMAT_CODE_CCYYMMDD = "102";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>CCYY Date Format Code <code><b>602</b></code></p>
     */
    public String DATE_FORMAT_CODE_CCYY = "602";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>CCYYMM Date Format Code <code><b>610</b></code></p>
     */
    public String DATE_FORMAT_CODE_CCYYMM = "610";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>CCYYMMDDHHMM Date Format Code <code><b>203</b></code></p>
     */
    public String DATE_FORMAT_CODE_CCYYMMDDHHMM = "203";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>CCYYMMDDHHMMSS Date Format Code <code><b>204</b></code></p>
     */
    public String DATE_FORMAT_CODE_CCYYMMDDHHMMSS = "204";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Male Sex Code [ISO 5218] <code><b>1</b></code></p>
     */
    public String SEX_CODE_MALE = "1";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Female Sex Code [ISO 5218] <code><b>2</b></code></p>
     */
    public String SEX_CODE_FEMALE = "2";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Age Unit Code for Decade <code><b>800</b></code></p>
     */
    public String AGE_UNIT_DECADE = "800";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Age Unit Code for Year <code><b>801</b></code></p>
     */
    public String AGE_UNIT_YEAR = "801";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Age Unit Code for Month <code><b>802</b></code></p>
     */
    public String AGE_UNIT_MONTH = "802";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Age Unit Code for Week <code><b>803</b></code></p>
     */
    public String AGE_UNIT_WEEK = "803";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Age Unit Code for Day <code><b>804</b></code></p>
     */
    public String AGE_UNIT_DAY = "804";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Age Unit Code for Hour <code><b>805</b></code></p>
     */
    public String AGE_UNIT_HOUR = "805";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Autopsy status for Yes <code><b>1</b></code></p>
     */
    public String AUTOPSY_STATUS_YES = "1";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Autopsy status for No <code><b>2</b></code></p>
     */
    public String AUTOPSY_STATUS_NO = "2";
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Autopsy status for Unknown <code><b>3</b></code></p>
     */
    public String AUTOPSY_STATUS_UNKNOWN = "3";
    // A.2.1.4 Qualification codes
    public String QUALIF_CODE_PHYSICIAN = "1";
    public String QUALIF_CODE_PHARMACIST = "2";
    public String QUALIF_CODE_OTHER_HEALTH_PROFESSIONAL = "3";
    public String QUALIF_CODE_LAWYER = "4";
    public String QUALIF_CODE_CONSUMER_OR_OTHER_NON_HEALTH_PROFESSIONAL = "5";
    // B.4.k.1 Characterization of drug role
    public String DRUG_CHAR_CODE_SUSPECT = "1";
    public String DRUG_CHAR_CODE_CONCOMITANT = "2";
    public String DRUG_CHAR_CODE_INTERACTING = "3";
    public String YES = "1";
    public String NO = "2";

    public StringBuffer getTag() throws InvalidTagException, InvalidDataException;

    public boolean validateTag() throws InvalidTagException, InvalidDataException;

    @Override
    public String toString();

    public void fetchData();
}
