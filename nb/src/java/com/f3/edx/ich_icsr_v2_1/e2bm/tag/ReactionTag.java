package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class ReactionTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -7271040954155871608L;
    private final int MAXLEN_PRIMARYSOURCEREACTION = 200;
    private final int MAXLEN_REACTIONMEDDRAVERSIONLLT = 8;
    private final int MAXLEN_REACTIONMEDDRALLT = 250;
    private final int MAXLEN_REACTIONMEDDRAVERSIONPT = 8;
    private final int MAXLEN_REACTIONMEDDRAPT = 250;
    private final int MAXLEN_TERMHIGHLIGHTED = 1;
    private final int MAXLEN_REACTIONSTARTDATEFORMAT = 3;
    private final int MAXLEN_REACTIONSTARTDATE = 12;
    private final int MAXLEN_REACTIONENDDATEFORMAT = 3;
    private final int MAXLEN_REACTIONENDDATE = 12;
    private final int MAXLEN_REACTIONDURATION = 5;
    private final int MAXLEN_REACTIONDURATIONUNIT = 3;
    private final int MAXLEN_REACTIONFIRSTTIME = 5;
    private final int MAXLEN_REACTIONFIRSTTIMEUNIT = 3;
    private final int MAXLEN_REACTIONLASTTIME = 5;
    private final int MAXLEN_REACTIONLASTTIMEUNIT = 3;
    private final int MAXLEN_REACTIONOUTCOME = 1;
    //////////////////////////////////////////////////////
    private String strPrimarySourceReaction = "";
    private String strReactionMedDraversionLlt = "v. 2.3";
    private String strReactionMedDraLlt = "";
    private String strReactionMedDraVersionPt = "v. 2.3";
    private String strReactionMedDraPt = "";
    private String strTermHighlighted = "";
    private String strReactionStartDateFormat = "";
    private String strReactionStartDate = "";
    private String strReactionEndDateFormat = "";
    private String strReactionEndDate = "";
    private String strReactionDuration = "";
    private String strReactionDurationUnit = "";
    private String strReactionFirstTime = "";
    private String strReactionFirstTimeUnit = "";
    private String strReactionLastTime = "";
    private String strReactionLastTimeUnit = "";
    private String strReactionOutcome = "";

    /**
     * Generates valid and well formed &lt;reaction&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;reaction&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<reaction> \n");
            sb.append("     <primarysourcereaction>" + strPrimarySourceReaction + "</primarysourcereaction> \n");
            sb.append("     <reactionmeddraversionllt>" + strReactionMedDraversionLlt + "</reactionmeddraversionllt> \n");
            sb.append("     <reactionmeddrallt>" + strReactionMedDraLlt + "</reactionmeddrallt> \n");
            sb.append("     <reactionmeddraversionpt>" + strReactionMedDraVersionPt + "</reactionmeddraversionpt> \n");
            sb.append("     <reactionmeddrapt>" + strReactionMedDraPt + "</reactionmeddrapt> \n");
            sb.append("     <termhighlighted>" + strTermHighlighted + "</termhighlighted> \n");
            sb.append("     <reactionstartdateformat>" + strReactionStartDateFormat + "</reactionstartdateformat> \n");
            sb.append("     <reactionstartdate>" + strReactionStartDate + "</reactionstartdate> \n");
            sb.append("     <reactionenddateformat>" + strReactionEndDateFormat + "</reactionenddateformat> \n");
            sb.append("     <reactionenddate>" + strReactionEndDate + "</reactionenddate> \n");
            sb.append("     <reactionduration>" + strReactionDuration + "</reactionduration> \n");
            sb.append("     <reactiondurationunit>" + strReactionDurationUnit + "</reactiondurationunit> \n");
            sb.append("     <reactionfirsttime>" + strReactionFirstTime + "</reactionfirsttime> \n");
            sb.append("     <reactionfirsttimeunit>" + strReactionFirstTimeUnit + "</reactionfirsttimeunit> \n");
            sb.append("     <reactionlasttime>" + strReactionLastTime + "</reactionlasttime> \n");
            sb.append("     <reactionlasttimeunit>" + strReactionLastTimeUnit + "</reactionlasttimeunit> \n");
            sb.append("     <reactionoutcome>" + strReactionOutcome + "</reactionoutcome> \n");
            sb.append("</reaction> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    @Override
    public void fetchData() {
    }

    // Constructors
    /**
     * Default constructor for ReactionTag.
     * This constructor initializes its member variables to empty string.
     * The version number variables always will be <code>"v. 2.3"</code>
     */
    public ReactionTag() {
        this.strPrimarySourceReaction = "";
        this.strReactionMedDraversionLlt = "v. 2.3";
        this.strReactionMedDraLlt = "";
        this.strReactionMedDraVersionPt = "v. 2.3";
        this.strReactionMedDraPt = "";
        this.strTermHighlighted = "";
        this.strReactionStartDateFormat = "";
        this.strReactionStartDate = "";
        this.strReactionEndDateFormat = "";
        this.strReactionEndDate = "";
        this.strReactionDuration = "";
        this.strReactionDurationUnit = "";
        this.strReactionFirstTime = "";
        this.strReactionFirstTimeUnit = "";
        this.strReactionLastTime = "";
        this.strReactionLastTimeUnit = "";
        this.strReactionOutcome = "";
    }

    /**
     * Constructor for ReactionTag.
     * The version number variables always will be <code>"v. 2.3"</code>
     * 
     * @param Primarysourcereaction <code><b>String</b></code> value to set &lt;primarysourcereaction&gt; tag.
     * @param ReactionMedDraversionLlt <code><b>String</b></code> value to set &lt;reactionmeddraversionllt&gt; tag.
     * @param ReactionMedDraLlt <code><b>String</b></code> value to set &lt;reactionmeddrallt&gt; tag.
     * @param ReactionMedDraVersionPt <code><b>String</b></code> value to set &lt;reactionmeddraversionpt&gt; tag.
     * @param ReactionMedDraPt <code><b>String</b></code> value to set &lt;reactionmeddrapt&gt; tag.
     * @param TermHighlighted <code><b>String</b></code> value to set &lt;termhighlighted&gt; tag.
     * @param ReactionStartDateFormat <code><b>String</b></code> value to set &lt;reactionstartdateformat&gt; tag.
     * @param ReactionStartDate <code><b>String</b></code> value to set &lt;reactionstartdate&gt; tag.
     * @param ReactionEndDateFormat <code><b>String</b></code> value to set &lt;reactionenddateformat&gt; tag.
     * @param ReactionEndDate <code><b>String</b></code> value to set &lt;reactionenddate&gt; tag.
     * @param ReactionDuration <code><b>String</b></code> value to set &lt;reactionduration&gt; tag.
     * @param ReactionDurationUnit <code><b>String</b></code> value to set &lt;reactiondurationunit&gt; tag.
     * @param ReactionFirstTime <code><b>String</b></code> value to set &lt;reactionfirsttime&gt; tag.
     * @param ReactionFirstTimeUnit <code><b>String</b></code> value to set &lt;reactionfirsttimeunit&gt; tag.
     * @param ReactionLastTime <code><b>String</b></code> value to set &lt;reactionlasttime&gt; tag.
     * @param ReactionLastTimeUnit <code><b>String</b></code> value to set &lt;reactionlasttimeunit&gt; tag.
     * @param ReactionOutcome <code><b>String</b></code> value to set &lt;reactionoutcome&gt; tag.
     */
    public ReactionTag(
            String Primarysourcereaction,
            String ReactionMedDraversionLlt,
            String ReactionMedDraLlt,
            String ReactionMedDraVersionPt,
            String ReactionMedDraPt,
            String TermHighlighted,
            String ReactionStartDateFormat,
            String ReactionStartDate,
            String ReactionEndDateFormat,
            String ReactionEndDate,
            String ReactionDuration,
            String ReactionDurationUnit,
            String ReactionFirstTime,
            String ReactionFirstTimeUnit,
            String ReactionLastTime,
            String ReactionLastTimeUnit,
            String ReactionOutcome) {
        this.strPrimarySourceReaction = Primarysourcereaction;
        this.strReactionMedDraversionLlt = "v. 2.3";
        this.strReactionMedDraLlt = ReactionMedDraLlt;
        this.strReactionMedDraVersionPt = "v. 2.3";
        this.strReactionMedDraPt = ReactionMedDraPt;
        this.strTermHighlighted = TermHighlighted;
        this.strReactionStartDateFormat = ReactionStartDateFormat;
        this.strReactionStartDate = ReactionStartDate;
        this.strReactionEndDateFormat = ReactionEndDateFormat;
        this.strReactionEndDate = ReactionEndDate;
        this.strReactionDuration = ReactionDuration;
        this.strReactionDurationUnit = ReactionDurationUnit;
        this.strReactionFirstTime = ReactionFirstTime;
        this.strReactionFirstTimeUnit = ReactionFirstTimeUnit;
        this.strReactionLastTime = ReactionLastTime;
        this.strReactionLastTimeUnit = ReactionLastTimeUnit;
        this.strReactionOutcome = ReactionOutcome;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // No mandatory fields. So, no InvalidTagException

        if (strPrimarySourceReaction.length() > MAXLEN_PRIMARYSOURCEREACTION) {
            throw new InvalidDataException("Length of PrimarySourceReaction must be less than " + MAXLEN_PRIMARYSOURCEREACTION);
        }
        if (strReactionMedDraversionLlt.length() > MAXLEN_REACTIONMEDDRAVERSIONLLT) {
            throw new InvalidDataException("Length of ReactionMedDraversionLlt must be less than " + MAXLEN_REACTIONMEDDRAVERSIONLLT);
        }
        if (strReactionMedDraLlt.length() > MAXLEN_REACTIONMEDDRALLT) {
            throw new InvalidDataException("Length of ReactionMedDraLlt must be less than " + MAXLEN_REACTIONMEDDRALLT);
        }
        if (strReactionMedDraVersionPt.length() > MAXLEN_REACTIONMEDDRAVERSIONPT) {
            throw new InvalidDataException("Length of ReactionMedDraVersionPt must be less than " + MAXLEN_REACTIONMEDDRAVERSIONPT);
        }
        if (strReactionMedDraPt.length() > MAXLEN_REACTIONMEDDRAPT) {
            throw new InvalidDataException("Length of ReactionMedDraPt must be less than " + MAXLEN_REACTIONMEDDRAPT);
        }
        if (strTermHighlighted.length() > MAXLEN_TERMHIGHLIGHTED) {
            throw new InvalidDataException("Length of TermHighlighted must be less than " + MAXLEN_TERMHIGHLIGHTED);
        }
        if (strReactionStartDateFormat.length() > MAXLEN_REACTIONSTARTDATEFORMAT) {
            throw new InvalidDataException("Length of ReactionStartDateFormat must be less than " + MAXLEN_REACTIONSTARTDATEFORMAT);
        }
        if (strReactionStartDate.length() > MAXLEN_REACTIONSTARTDATE) {
            throw new InvalidDataException("Length of ReactionStartDate must be less than " + MAXLEN_REACTIONSTARTDATE);
        }
        if (strReactionEndDateFormat.length() > MAXLEN_REACTIONENDDATEFORMAT) {
            throw new InvalidDataException("Length of ReactionEndDateFormat must be less than " + MAXLEN_REACTIONENDDATEFORMAT);
        }
        if (strReactionEndDate.length() > MAXLEN_REACTIONENDDATE) {
            throw new InvalidDataException("Length of ReactionEndDate must be less than " + MAXLEN_REACTIONENDDATE);
        }
        if (strReactionDuration.length() > MAXLEN_REACTIONDURATION) {
            throw new InvalidDataException("Length of ReactionDuration must be less than " + MAXLEN_REACTIONDURATION);
        }
        if (strReactionDurationUnit.length() > MAXLEN_REACTIONDURATIONUNIT) {
            throw new InvalidDataException("Length of ReactionDurationUnit must be less than " + MAXLEN_REACTIONDURATIONUNIT);
        }
        if (strReactionFirstTime.length() > MAXLEN_REACTIONFIRSTTIME) {
            throw new InvalidDataException("Length of ReactionFirstTime must be less than " + MAXLEN_REACTIONFIRSTTIME);
        }
        if (strReactionFirstTimeUnit.length() > MAXLEN_REACTIONFIRSTTIMEUNIT) {
            throw new InvalidDataException("Length of ReactionFirstTimeUnit must be less than " + MAXLEN_REACTIONFIRSTTIMEUNIT);
        }
        if (strReactionLastTime.length() > MAXLEN_REACTIONLASTTIME) {
            throw new InvalidDataException("Length of ReactionLastTime must be less than " + MAXLEN_REACTIONLASTTIME);
        }
        if (strReactionLastTimeUnit.length() > MAXLEN_REACTIONLASTTIMEUNIT) {
            throw new InvalidDataException("Length of ReactionLastTimeUnit must be less than " + MAXLEN_REACTIONLASTTIMEUNIT);
        }
        if (strReactionOutcome.length() > MAXLEN_REACTIONOUTCOME) {
            throw new InvalidDataException("Length of ReactionOutcome must be less than " + MAXLEN_REACTIONOUTCOME);
        }

        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPrimarySourceReaction = null;
        this.strReactionMedDraversionLlt = null;
        this.strReactionMedDraLlt = null;
        this.strReactionMedDraVersionPt = null;
        this.strReactionMedDraPt = null;
        this.strTermHighlighted = null;
        this.strReactionStartDateFormat = null;
        this.strReactionStartDate = null;
        this.strReactionEndDateFormat = null;
        this.strReactionEndDate = null;
        this.strReactionDuration = null;
        this.strReactionDurationUnit = null;
        this.strReactionFirstTime = null;
        this.strReactionFirstTimeUnit = null;
        this.strReactionLastTime = null;
        this.strReactionLastTimeUnit = null;
        this.strReactionOutcome = null;
    }
}
