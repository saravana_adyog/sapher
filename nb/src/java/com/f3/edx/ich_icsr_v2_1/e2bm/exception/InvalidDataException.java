package com.f3.edx.ich_icsr_v2_1.e2bm.exception;

/**
 *
 * @author Anoop Varma
 */
public class InvalidDataException extends Exception {

    /** */
    private static final long serialVersionUID = -3212610599880443923L;

    /**
     * InvalidDataException constructor
     * 
     * @param strErrMsg as <code><b>String</b></code>
     */
    public InvalidDataException(String strErrMsg) {
        super(strErrMsg);
    }
}
