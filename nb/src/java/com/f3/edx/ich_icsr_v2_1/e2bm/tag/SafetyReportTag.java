package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import com.f3.iso.country.ISO3166;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Anoop Varma
 */
public class SafetyReportTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 4703266068069535789L;
    private final int MAXLEN_SAFETYREPORTVERSION = 3;
    private final int MAXLEN_SAFETYREPORTID = 100;
    private final int MAXLEN_PRIMARYSOURCECOUNTRY = 2;
    private final int MAXLEN_OCCURCOUNTRY = 2;
    private final int MAXLEN_TRANSMISSIONDATEFORMAT = 3;
    private final int MAXLEN_TRANSMISSIONDATE = 8;
    private final int MAXLEN_REPORTTYPE = 1;
    private final int MAXLEN_SERIOUS = 1;
    private final int MAXLEN_SERIOUSNESSDEATH = 1;
    private final int MAXLEN_SERIOUSNESSLIFETHREATENING = 1;
    private final int MAXLEN_SERIOUSNESSHOSPITALIZATION = 1;
    private final int MAXLEN_SERIOUSNESSDISABLING = 1;
    private final int MAXLEN_SERIOUSNESSCONGENITALANOMALI = 1;
    private final int MAXLEN_SERIOUSNESSOTHER = 1;
    private final int MAXLEN_RECEIVEDATEFORMAT = 3;
    private final int MAXLEN_RECEIVEDATE = 8;
    private final int MAXLEN_RECEIPTDATEFORMAT = 3;
    private final int MAXLEN_RECEIPTDATE = 8;
    private final int MAXLEN_ADDITIONALDOCUMENT = 1;
    private final int MAXLEN_DOCUMENTLIST = 100;
    private final int MAXLEN_FULFILLEXPEDITECRITERIA = 1;
    private final int MAXLEN_AUTHORITYNUMB = 100;
    private final int MAXLEN_COMPANYNUMB = 100;
    private final int MAXLEN_DUPLICATE = 1;
    private final int MAXLEN_CASENULLIFICATION = 1;
    private final int MAXLEN_NULLIFICATIONREASON = 200;
    private final int MAXLEN_MEDICALLYCONFIRM = 1;
    //////////////////////////////////////////////////////
    private String strSafetyReportVersion = "1.0";
    private String strSafetyReportId;
    private String strPrimarySourceCountry;
    private String strOccurCountry;
    private String strTransmissionDateFormat;
    private String strTransmissionDate;
    private String strReportType;
    private String strSerious;
    private String strSeriousnessDeath;
    private String strSeriousnessLifeThreatening;
    private String strSeriousnessHospitalization;
    private String strSeriousnessDisabling;
    private String strSeriousnessCongenitalAnomali;
    private String strSeriousnessOther;
    private String strReceiveDateFormat;
    private String strReceiveDate;
    private String strReceiptDateFormat;
    private String strReceiptDate;
    private String strAdditionalDocument;
    private String strDocumentList;
    private String strFulfillExpediteCriteria;
    private String strAuthorityNumb;
    private String strCompanyNumb;
    private String strDuplicate;
    private String strCaseNullification;
    private String strNullificationReason;
    private String strMedicallyConfirm;
    private ArrayList<ReportDuplicateTag> alReportDuplicate;
    private ArrayList<LinkedReportTag> alLinkedReport;
    private ArrayList<PrimarySourceTag> alPrimarySource;
    private ArrayList<SenderTag> alSender;
    private ArrayList<ReceiverTag> alReceiver;
    private ArrayList<PatientTag> alPatient;

    /**
     * Default constructor that creates a new instance of <code><b>SafetyReportTag</b></code>.
     * This constructor initializes its member variables to empty string.
     */
    public SafetyReportTag() {
        this.strSafetyReportVersion = "1.0";
        this.strSafetyReportId = "";
        this.strPrimarySourceCountry = "";
        this.strOccurCountry = "";
        this.strTransmissionDateFormat = "";
        this.strTransmissionDate = "";
        this.strReportType = "";
        this.strSerious = "";
        this.strSeriousnessDeath = "";
        this.strSeriousnessLifeThreatening = "";
        this.strSeriousnessHospitalization = "";
        this.strSeriousnessDisabling = "";
        this.strSeriousnessCongenitalAnomali = "";
        this.strSeriousnessOther = "";
        this.strReceiveDateFormat = "";
        this.strReceiveDate = "";
        this.strReceiptDateFormat = "";
        this.strReceiptDate = "";
        this.strAdditionalDocument = "";
        this.strDocumentList = "";
        this.strFulfillExpediteCriteria = "";
        this.strAuthorityNumb = "";
        this.strCompanyNumb = "";
        this.strDuplicate = "";
        this.strCaseNullification = "";
        this.strNullificationReason = "";
        this.strMedicallyConfirm = "";
        this.alReportDuplicate = null;
        this.alLinkedReport = null;
        this.alPrimarySource = null;
        this.alSender = null;
        this.alReceiver = null;
        this.alPatient = null;
    }

    /**
     * Constructor that creates a new instance of <code><b>SafetyReportTag</b></code>.
     * 
     * @param SafetyReportVersion <code><b>String</b></code> value to set &lt;safetyreportversion&gt; tag.
     * @param SafetyReportId <code><b>String</b></code> value to set &lt;safetyreportid&gt; tag.
     * @param PrimarySourceCountry <code><b>String</b></code> value to set &lt;primarysourcecountry&gt; tag.
     * @param OccurCountry <code><b>String</b></code> value to set &lt;occurcountry&gt; tag.
     * @param TransmissionDateFormat <code><b>String</b></code> value to set &lt;transmissiondateformat&gt; tag.
     * @param TransmissionDate <code><b>String</b></code> value to set &lt;transmissiondate&gt; tag.
     * @param ReportType <code><b>String</b></code> value to set &lt;reporttype&gt; tag.
     * @param Serious <code><b>String</b></code> value to set &lt;serious&gt; tag.
     * @param SeriousnessDeath <code><b>String</b></code> value to set &lt;seriousnessdeath&gt; tag.
     * @param SeriousnessLifeThreatening <code><b>String</b></code> value to set &lt;seriousnesslifethreatening&gt; tag.
     * @param SeriousnessHospitalization <code><b>String</b></code> value to set &lt;seriousnesshospitalization&gt; tag.
     * @param SeriousnessDisabling <code><b>String</b></code> value to set &lt;seriousnessdisabling&gt; tag.
     * @param SeriousnessCongenitalAnomali <code><b>String</b></code> value to set &lt;seriousnesscongenitalanomali&gt; tag.
     * @param SeriousnessOther <code><b>String</b></code> value to set &lt;seriousnessother&gt; tag.
     * @param ReceiveDateFormat <code><b>String</b></code> value to set &lt;receivedateformat&gt; tag.
     * @param ReceiveDate <code><b>String</b></code> value to set &lt;receivedate&gt; tag.
     * @param ReceiptDateFormat <code><b>String</b></code> value to set &lt;receiptdateformat&gt; tag.
     * @param ReceiptDate <code><b>String</b></code> value to set &lt;receiptdate&gt; tag.
     * @param AdditionalDocument <code><b>String</b></code> value to set &lt;additionaldocument&gt; tag.
     * @param DocumentList <code><b>String</b></code> value to set &lt;documentlist&gt; tag.
     * @param FulfillExpediteCriteria <code><b>String</b></code> value to set &lt;fulfillexpeditecriteria&gt; tag.
     * @param AuthorityNumb <code><b>String</b></code> value to set &lt;authoritynumb&gt; tag.
     * @param CompanyNumb <code><b>String</b></code> value to set &lt;companynumb&gt; tag.
     * @param Duplicate <code><b>String</b></code> value to set &lt;duplicate&gt; tag.
     * @param CaseNullification <code><b>String</b></code> value to set &lt;casenullification&gt; tag.
     * @param NullificationReason <code><b>String</b></code> value to set &lt;nullificationreason&gt; tag.
     * @param MedicallyConfirm <code><b>String</b></code> value to set &lt;medicallyconfirm&gt; tag.
     * @param ReportDuplicate <code><b>ArrayList</b></code> value to set &lt;reportduplicate&gt; tag.
     * @param LinkedReport <code><b>ArrayList</b></code> value to set &lt;linkedreport&gt; tag.
     * @param PrimarySource <code><b>ArrayList</b></code> value to set &lt;primarysource&gt; tag.
     * @param Sender <code><b>ArrayList</b></code> value to set &lt;sender&gt; tag.
     * @param Receiver <code><b>ArrayList</b></code> value to set &lt;receiver&gt; tag.
     * @param Patient <code><b>ArrayList</b></code> value to set &lt;patient&gt; tag.
     * @deprecated 
     */
    @Deprecated
    public SafetyReportTag(
            String SafetyReportVersion,
            String SafetyReportId,
            String PrimarySourceCountry,
            String OccurCountry,
            String TransmissionDateFormat,
            String TransmissionDate,
            String ReportType,
            String Serious,
            String SeriousnessDeath,
            String SeriousnessLifeThreatening,
            String SeriousnessHospitalization,
            String SeriousnessDisabling,
            String SeriousnessCongenitalAnomali,
            String SeriousnessOther,
            String ReceiveDateFormat,
            String ReceiveDate,
            String ReceiptDateFormat,
            String ReceiptDate,
            String AdditionalDocument,
            String DocumentList,
            String FulfillExpediteCriteria,
            String AuthorityNumb,
            String CompanyNumb,
            String Duplicate,
            String CaseNullification,
            String NullificationReason,
            String MedicallyConfirm,
            ArrayList<ReportDuplicateTag> ReportDuplicate,
            ArrayList<LinkedReportTag> LinkedReport,
            ArrayList<PrimarySourceTag> PrimarySource,
            ArrayList<SenderTag> Sender,
            ArrayList<ReceiverTag> Receiver,
            ArrayList<PatientTag> Patient) {
        this.strSafetyReportVersion = "1.0";
        this.strSafetyReportId = SafetyReportId;
        this.strPrimarySourceCountry = PrimarySourceCountry;
        this.strOccurCountry = OccurCountry;
        this.strTransmissionDateFormat = TransmissionDateFormat;
        this.strTransmissionDate = TransmissionDate;
        this.strReportType = ReportType;
        this.strSerious = Serious;
        this.strSeriousnessDeath = SeriousnessDeath;
        this.strSeriousnessLifeThreatening = SeriousnessLifeThreatening;
        this.strSeriousnessHospitalization = SeriousnessHospitalization;
        this.strSeriousnessDisabling = SeriousnessDisabling;
        this.strSeriousnessCongenitalAnomali = SeriousnessCongenitalAnomali;
        this.strSeriousnessOther = SeriousnessOther;
        this.strReceiveDateFormat = ReceiveDateFormat;
        this.strReceiveDate = ReceiveDate;
        this.strReceiptDateFormat = ReceiptDateFormat;
        this.strReceiptDate = ReceiptDate;
        this.strAdditionalDocument = AdditionalDocument;
        this.strDocumentList = DocumentList;
        this.strFulfillExpediteCriteria = FulfillExpediteCriteria;
        this.strAuthorityNumb = AuthorityNumb;
        this.strCompanyNumb = CompanyNumb;
        this.strDuplicate = Duplicate;
        this.strCaseNullification = CaseNullification;
        this.strNullificationReason = NullificationReason;
        this.strMedicallyConfirm = MedicallyConfirm;
        this.alReportDuplicate = ReportDuplicate;
        this.alLinkedReport = LinkedReport;
        this.alPrimarySource = PrimarySource;
        this.alSender = Sender;
        this.alReceiver = Receiver;
        this.alPatient = Patient;
    }

    @Override
    public void fetchData() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", java.util.Locale.US);
        this.strSafetyReportVersion = "1.0";
        this.strSafetyReportId = "" + TagData.getInstance().aCase.getRep_Num();
        if (TagData.getInstance().aCase.getCountry_Nm() != null) {
            this.strPrimarySourceCountry = ISO3166.getInstance().getCountryCode(TagData.getInstance().aCase.getCountry_Nm());
        }
        if (TagData.getInstance().aCase.getCountry_Nm() != null) {
            this.strOccurCountry = ISO3166.getInstance().getCountryCode(TagData.getInstance().aCase.getCountry_Nm());
        }
        this.strTransmissionDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        this.strTransmissionDate = sdf.format(Calendar.getInstance().getTime());
        if (TagData.getInstance().aCase.getRtype() != null) {
            this.strReportType = TagData.getInstance().aCase.getRtype();
        }
        this.strSerious = "";
        this.strSeriousnessDeath = "";
        this.strSeriousnessLifeThreatening = "";
        this.strSeriousnessHospitalization = "";
        this.strSeriousnessDisabling = "";
        this.strSeriousnessCongenitalAnomali = "";
        this.strSeriousnessOther = "";
        this.strReceiveDateFormat = "";
        this.strReceiveDate = "";
        this.strReceiptDateFormat = DATE_FORMAT_CODE_CCYYMMDD;
        if (TagData.getInstance().aCase.getDate_Recv() != null) {
            this.strReceiptDate = sdf.format(TagData.getInstance().aCase.getDate_Recv());
        }
        this.strAdditionalDocument = "";
        this.strDocumentList = "";
        this.strFulfillExpediteCriteria = "";
        if (TagData.getInstance().aCase.getAuthorization_No() != null) {
            this.strAuthorityNumb = TagData.getInstance().aCase.getAuthorization_No();
        }
        if (TagData.getInstance().aCase.getRep_Xref_Num() != null) {
            this.strCompanyNumb = TagData.getInstance().aCase.getRep_Xref_Num();
        }
        this.strDuplicate = "";
        if (TagData.getInstance().aCase.getIs_Suspended() == 1) {
            this.strCaseNullification = "1";
        } else {
            this.strCaseNullification = "0";
        }
        this.strNullificationReason = "";
        this.strMedicallyConfirm = "";

        this.alReportDuplicate = new ArrayList<ReportDuplicateTag>();
        ReportDuplicateTag rdt = new ReportDuplicateTag();
        rdt.fetchData();
        this.alReportDuplicate.add(rdt);

        this.alLinkedReport = new ArrayList<LinkedReportTag>();
        LinkedReportTag lrt = new LinkedReportTag();
        lrt.fetchData();
        this.alLinkedReport.add(lrt);

        this.alPrimarySource = new ArrayList<PrimarySourceTag>();
        PrimarySourceTag pst = new PrimarySourceTag();
        pst.fetchData();
        this.alPrimarySource.add(pst);

        this.alSender = new ArrayList<SenderTag>();
        SenderTag st = new SenderTag();
        st.fetchData();
        this.alSender.add(st);

        this.alReceiver = new ArrayList<ReceiverTag>();
        ReceiverTag rt = new ReceiverTag();
        rt.fetchData();
        this.alReceiver.add(rt);

        this.alPatient = new ArrayList<PatientTag>();
        PatientTag pt = new PatientTag();
        pt.fetchData();
        this.alPatient.add(pt);

    }

    /**
     * Generates valid and well formed &lt;safetyreport&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;safetyreport&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            fetchData();
            validateTag();

            // Hardcoding for ICH ICSR DTD Spec A.1.5.1 & A.1.5.2 set.
            setSeriousnesInfo();

            StringBuffer sb = new StringBuffer();
            sb.append("<safetyreport> \n");
            sb.append("    <safetyreportversion>" + strSafetyReportVersion + "</safetyreportversion> \n");
            sb.append("    <safetyreportid>" + strSafetyReportId + "</safetyreportid> \n");
            sb.append("    <primarysourcecountry>" + strPrimarySourceCountry + "</primarysourcecountry> \n");
            sb.append("    <occurcountry>" + strOccurCountry + "</occurcountry> \n");
            sb.append("    <transmissiondateformat>" + strTransmissionDateFormat + "</transmissiondateformat> \n");
            sb.append("    <transmissiondate>" + strTransmissionDate + "</transmissiondate> \n");
            sb.append("    <reporttype>" + strReportType + "</reporttype> \n");
            sb.append("    <serious>" + strSerious + "</serious> \n");
            sb.append("    <seriousnessdeath>" + strSeriousnessDeath + "</seriousnessdeath> \n");
            sb.append("    <seriousnesslifethreatening>" + strSeriousnessLifeThreatening + "</seriousnesslifethreatening> \n");
            sb.append("    <seriousnesshospitalization>" + strSeriousnessHospitalization + "</seriousnesshospitalization> \n");
            sb.append("    <seriousnessdisabling>" + strSeriousnessDisabling + "</seriousnessdisabling> \n");
            sb.append("    <seriousnesscongenitalanomali>" + strSeriousnessCongenitalAnomali + "</seriousnesscongenitalanomali> \n");
            sb.append("    <seriousnessother>" + strSeriousnessOther + "</seriousnessother> \n");
            sb.append("    <receivedateformat>" + strReceiveDateFormat + "</receivedateformat> \n");
            sb.append("    <receivedate>" + strReceiveDate + "</receivedate> \n");
            sb.append("    <receiptdateformat>" + strReceiptDateFormat + "</receiptdateformat> \n");
            sb.append("    <receiptdate>" + strReceiptDate + "</receiptdate> \n");
            sb.append("    <additionaldocument>" + strAdditionalDocument + "</additionaldocument> \n");
            sb.append("    <documentlist>" + strDocumentList + "</documentlist> \n");
            sb.append("    <fulfillexpeditecriteria>" + strFulfillExpediteCriteria + "</fulfillexpeditecriteria> \n");
            sb.append("    <authoritynumb>" + strAuthorityNumb + "</authoritynumb> \n");
            sb.append("    <companynumb>" + strCompanyNumb + "</companynumb> \n");
            sb.append("    <duplicate>" + strDuplicate + "</duplicate> \n");
            sb.append("    <casenullification>" + strCaseNullification + "</casenullification> \n");
            sb.append("    <nullificationreason>" + strNullificationReason + "</nullificationreason> \n");
            sb.append("    <medicallyconfirm>" + strMedicallyConfirm + "</medicallyconfirm> \n");

            if (alReportDuplicate != null) {
                for (int i = 0; i < alReportDuplicate.size(); i++) {
                    sb.append("    <!-- ReportDuplicate tag : " + (i + 1) + " of " + alReportDuplicate.size() + " --> \n");
                    sb.append("    " + alReportDuplicate.get(i).getTag());
                }
            }

            if (alLinkedReport != null) {
                for (int i = 0; i < alLinkedReport.size(); i++) {
                    sb.append("    <!-- LinkedReport tag : " + (i + 1) + " of " + alLinkedReport.size() + " --> \n");
                    sb.append("    " + alLinkedReport.get(i).getTag());
                }
            }

            if (alPrimarySource != null) {
                for (int i = 0; i < alPrimarySource.size(); i++) {
                    sb.append("    <!-- PrimarySource tag : " + (i + 1) + " of " + alPrimarySource.size() + " --> \n");
                    sb.append("    " + alPrimarySource.get(i).getTag());
                }
            }

            if (alSender != null) {
                for (int i = 0; i < alSender.size(); i++) {
                    sb.append("    <!-- Sender tag : " + (i + 1) + " of " + alSender.size() + " --> \n");
                    sb.append("    " + alSender.get(i).getTag());
                }
            }

            if (alReceiver != null) {
                for (int i = 0; i < alReceiver.size(); i++) {
                    sb.append("    <!-- Receiver tag : " + (i + 1) + " of " + alReceiver.size() + " --> \n");
                    sb.append("    " + alReceiver.get(i).getTag());
                }
            }

            if (alPatient != null) {
                for (int i = 0; i < alPatient.size(); i++) {
                    sb.append("    <!-- Patient tag : " + (i + 1) + " of " + alPatient.size() + " --> \n");
                    sb.append("    " + alPatient.get(i).getTag());
                }
            }

            sb.append("</safetyreport> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1] 
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        if (alPrimarySource.size() == 0) {
            throw new InvalidTagException("There must be atleast one <primarysource> tag in <safetyreport>.");
        }

        if (strSafetyReportVersion.length() > MAXLEN_SAFETYREPORTVERSION) {
            throw new InvalidDataException("Length of SafetyReportVersion must be less than " + MAXLEN_SAFETYREPORTVERSION);
        }

        if (strSafetyReportId.length() > MAXLEN_SAFETYREPORTID) {
            throw new InvalidDataException("Length of  must be less than " + MAXLEN_SAFETYREPORTID);
        }
        if (strPrimarySourceCountry.length() > MAXLEN_PRIMARYSOURCECOUNTRY) {
            throw new InvalidDataException("Length of PrimarySourceCountry must be less than " + MAXLEN_PRIMARYSOURCECOUNTRY);
        }
        if (strOccurCountry.length() > MAXLEN_OCCURCOUNTRY) {
            throw new InvalidDataException("Length of OccurCountry must be less than " + MAXLEN_OCCURCOUNTRY);
        }
        if (strTransmissionDateFormat.length() > MAXLEN_TRANSMISSIONDATEFORMAT) {
            throw new InvalidDataException("Length of TransmissionDateFormat must be less than " + MAXLEN_TRANSMISSIONDATEFORMAT);
        }
        if (strTransmissionDate.length() > MAXLEN_TRANSMISSIONDATE) {
            throw new InvalidDataException("Length of TransmissionDate must be less than " + MAXLEN_TRANSMISSIONDATE);
        }
        if (strReportType.length() > MAXLEN_REPORTTYPE) {
            throw new InvalidDataException("Length of ReportType must be less than " + MAXLEN_REPORTTYPE);
        }
        if (strSerious.length() > MAXLEN_SERIOUS) {
            throw new InvalidDataException("Length of Serious must be less than " + MAXLEN_SERIOUS);
        }
        if (strSeriousnessDeath.length() > MAXLEN_SERIOUSNESSDEATH) {
            throw new InvalidDataException("Length of SeriousnessDeath must be less than " + MAXLEN_SERIOUSNESSDEATH);
        }
        if (strSeriousnessLifeThreatening.length() > MAXLEN_SERIOUSNESSLIFETHREATENING) {
            throw new InvalidDataException("Length of SeriousnessLifeThreatening must be less than " + MAXLEN_SERIOUSNESSLIFETHREATENING);
        }
        if (strSeriousnessHospitalization.length() > MAXLEN_SERIOUSNESSHOSPITALIZATION) {
            throw new InvalidDataException("Length of SeriousnessHospitalization must be less than " + MAXLEN_SERIOUSNESSHOSPITALIZATION);
        }
        if (strSeriousnessDisabling.length() > MAXLEN_SERIOUSNESSDISABLING) {
            throw new InvalidDataException("Length of SeriousnessDisabling must be less than " + MAXLEN_SERIOUSNESSDISABLING);
        }
        if (strSeriousnessCongenitalAnomali.length() > MAXLEN_SERIOUSNESSCONGENITALANOMALI) {
            throw new InvalidDataException("Length of SeriousnessCongenitalAnomali must be less than " + MAXLEN_SERIOUSNESSCONGENITALANOMALI);
        }
        if (strSeriousnessOther.length() > MAXLEN_SERIOUSNESSOTHER) {
            throw new InvalidDataException("Length of SeriousnessOther must be less than " + MAXLEN_SERIOUSNESSOTHER);
        }
        if (strReceiveDateFormat.length() > MAXLEN_RECEIVEDATEFORMAT) {
            throw new InvalidDataException("Length of ReceiveDateFormat must be less than " + MAXLEN_RECEIVEDATEFORMAT);
        }
        if (strReceiveDate.length() > MAXLEN_RECEIVEDATE) {
            throw new InvalidDataException("Length of ReceiveDate must be less than " + MAXLEN_RECEIVEDATE);
        }
        if (strReceiptDateFormat.length() > MAXLEN_RECEIPTDATEFORMAT) {
            throw new InvalidDataException("Length of ReceiptDateFormat must be less than " + MAXLEN_RECEIPTDATEFORMAT);
        }
        if (strReceiptDate.length() > MAXLEN_RECEIPTDATE) {
            throw new InvalidDataException("Length of ReceiptDate must be less than " + MAXLEN_RECEIPTDATE);
        }
        if (strAdditionalDocument.length() > MAXLEN_ADDITIONALDOCUMENT) {
            throw new InvalidDataException("Length of AdditionalDocument must be less than " + MAXLEN_ADDITIONALDOCUMENT);
        }
        if (strDocumentList.length() > MAXLEN_DOCUMENTLIST) {
            throw new InvalidDataException("Length of DocumentList must be less than " + MAXLEN_DOCUMENTLIST);
        }
        if (strFulfillExpediteCriteria.length() > MAXLEN_FULFILLEXPEDITECRITERIA) {
            throw new InvalidDataException("Length of FulfillExpediteCriteria must be less than " + MAXLEN_FULFILLEXPEDITECRITERIA);
        }
        if (strAuthorityNumb.length() > MAXLEN_AUTHORITYNUMB) {
            throw new InvalidDataException("Length of  must be less than " + MAXLEN_AUTHORITYNUMB);
        }
        if (strCompanyNumb.length() > MAXLEN_COMPANYNUMB) {
            throw new InvalidDataException("Length of CompanyNumb must be less than " + MAXLEN_COMPANYNUMB);
        }
        if (strDuplicate.length() > MAXLEN_DUPLICATE) {
            throw new InvalidDataException("Length of Duplicate must be less than " + MAXLEN_DUPLICATE);
        }
        if (strCaseNullification.length() > MAXLEN_CASENULLIFICATION) {
            throw new InvalidDataException("Length of CaseNullification must be less than " + MAXLEN_CASENULLIFICATION);
        }
        if (strNullificationReason.length() > MAXLEN_NULLIFICATIONREASON) {
            throw new InvalidDataException("Length of NullificationReason must be less than " + MAXLEN_NULLIFICATIONREASON);
        }
        if (strMedicallyConfirm.length() > MAXLEN_MEDICALLYCONFIRM) {
            throw new InvalidDataException("Length of MedicallyConfirm must be less than " + MAXLEN_MEDICALLYCONFIRM);
        }

        return true;
    }

    /**
     * Function creates data for the following 7 tags:
     * <p>
     * &nbsp;&nbsp;&nbsp;&lt;serious&gt;,<br>
     * &nbsp;&nbsp;&nbsp;&lt;seriousnessdeath&gt;,<br>
     * &nbsp;&nbsp;&nbsp;&lt;seriousnesslifethreatening&gt;,<br>
     * &nbsp;&nbsp;&nbsp;&lt;seriousnesshospitalization&gt;,<br>
     * &nbsp;&nbsp;&nbsp;&lt;seriousnessdisabling&gt;,<br>
     * &nbsp;&nbsp;&nbsp;&lt;seriousnesscongenitalanomali&gt;,<br>
     * &nbsp;&nbsp;&nbsp;&lt;seriousnessother&gt;
     * </p>
     * <br>Hardcoding for ICH ICSR DTD Spec A.1.5.1 & A.1.5.2 set.
     */
    private void setSeriousnesInfo() {
        strSerious = NO;
        strSeriousnessDeath = NO;
        strSeriousnessLifeThreatening = NO;
        strSeriousnessHospitalization = NO;
        strSeriousnessDisabling = NO;
        strSeriousnessCongenitalAnomali = NO;
        strSeriousnessOther = NO;

        for (int i = 0; i < TagData.getInstance().alAE.size(); i++) {
            if (TagData.getInstance().alAE.get(i).getClassification_Desc() != null) {
                if (TagData.getInstance().alAE.get(i).getClassification_Desc().equalsIgnoreCase("dt")) {
                    strSeriousnessDeath = YES;
                    strSerious = YES;
                } else if (TagData.getInstance().alAE.get(i).getClassification_Desc().equalsIgnoreCase("lt")) {
                    strSeriousnessLifeThreatening = YES;
                    strSerious = YES;
                } else if (TagData.getInstance().alAE.get(i).getClassification_Desc().equalsIgnoreCase("hl") ||
                        TagData.getInstance().alAE.get(i).getClassification_Desc().equalsIgnoreCase("hd")) {
                    strSeriousnessHospitalization = YES;
                    strSerious = YES;
                } else if (TagData.getInstance().alAE.get(i).getClassification_Desc().equalsIgnoreCase("dl")) {
                    strSeriousnessDisabling = YES;
                    strSerious = YES;
                } else if (TagData.getInstance().alAE.get(i).getClassification_Desc().equalsIgnoreCase("ca")) {
                    strSeriousnessCongenitalAnomali = YES;
                    strSerious = YES;
                } else {
                    strSeriousnessOther = YES;
                    strSerious = YES;
                }
            }
        }
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strSafetyReportVersion = null;
        this.strSafetyReportId = null;
        this.strPrimarySourceCountry = null;
        this.strOccurCountry = null;
        this.strTransmissionDateFormat = null;
        this.strTransmissionDate = null;
        this.strReportType = null;
        this.strSerious = null;
        this.strSeriousnessDeath = null;
        this.strSeriousnessLifeThreatening = null;
        this.strSeriousnessHospitalization = null;
        this.strSeriousnessDisabling = null;
        this.strSeriousnessCongenitalAnomali = null;
        this.strSeriousnessOther = null;
        this.strReceiveDateFormat = null;
        this.strReceiveDate = null;
        this.strReceiptDateFormat = null;
        this.strReceiptDate = null;
        this.strAdditionalDocument = null;
        this.strDocumentList = null;
        this.strFulfillExpediteCriteria = null;
        this.strAuthorityNumb = null;
        this.strCompanyNumb = null;
        this.strDuplicate = null;
        this.strCaseNullification = null;
        this.strNullificationReason = null;
        this.strMedicallyConfirm = null;

        this.alReportDuplicate = null;
        this.alLinkedReport = null;
        this.alPrimarySource = null;
        this.alSender = null;
        this.alReceiver = null;
        this.alPatient = null;
    }
}
