package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;
import java.util.ArrayList;

/**
 *
 * @author Anoop Varma
 */
public class DrugTag implements Tagable {

    /** Serial version UID */
    static final long serialVersionUID = 3319238575989896524L;
    private final int MAXLEN_DRUGCHARACTERIZATION = 1;
    private final int MAXLEN_MEDICINALPRODUCT = 70;
    private final int MAXLEN_OBTAINDRUGCOUNTRY = 2;
    private final int MAXLEN_DRUGBATCHNUMB = 35;
    private final int MAXLEN_DRUGAUTHORIZATIONNUMB = 35;
    private final int MAXLEN_DRUGAUTHORIZATIONCOUNTRY = 2;
    private final int MAXLEN_DRUGAUTHORIZATIONHOLDER = 60;
    private final int MAXLEN_DRUGSTRUCTUREDOSAGENUMB = 8;
    private final int MAXLEN_DRUGSTRUCTUREDOSAGEUNIT = 3;
    private final int MAXLEN_DRUGSEPARATEDOSAGENUMB = 3;
    private final int MAXLEN_DRUGINTERVALDOSAGEUNITNUMB = 3;
    private final int MAXLEN_DRUGINTERVALDOSAGEDEFINITION = 3;
    private final int MAXLEN_DRUGCUMULATIVEDOSAGENUMB = 10;
    private final int MAXLEN_DRUGCUMULATIVEDOSAGEUNIT = 3;
    private final int MAXLEN_DRUGDOSAGETEXT = 100;
    private final int MAXLEN_DRUGDOSAGEFORM = 50;
    private final int MAXLEN_DRUGADMINISTRATIONROUTE = 3;
    private final int MAXLEN_DRUGPARADMINISTRATION = 3;
    private final int MAXLEN_REACTIONGESTATIONPERIOD = 3;
    private final int MAXLEN_REACTIONGESTATIONPERIODUNIT = 3;
    private final int MAXLEN_DRUGINDICATIONMEDDRAVERSION = 8;
    private final int MAXLEN_DRUGINDICATION = 250;
    private final int MAXLEN_DRUGSTARTDATEFORMAT = 3;
    private final int MAXLEN_DRUGSTARTDATE = 8;
    private final int MAXLEN_DRUGSTARTPERIOD = 5;
    private final int MAXLEN_DRUGSTARTPERIODUNIT = 3;
    private final int MAXLEN_DRUGLASTPERIOD = 5;
    private final int MAXLEN_DRUGLASTPERIODUNIT = 3;
    private final int MAXLEN_DRUGENDDATEFORMAT = 3;
    private final int MAXLEN_DRUGENDDATE = 8;
    private final int MAXLEN_DRUGTREATMENTDURATION = 5;
    private final int MAXLEN_DRUGTREATMENTDURATIONUNIT = 3;
    private final int MAXLEN_ACTIONDRUG = 1;
    private final int MAXLEN_DRUGRECURREADMINISTRATION = 1;
    private final int MAXLEN_DRUGADDITIONAL = 100;
    ///////////////////////////////////////////
    private String strDrugCharacterization = "";
    private String strMedicinalProduct = "";
    private String strObtainDrugCountry = "";
    private String strDrugBatchNumb = "";
    private String strDrugAuthorizationNumb = "";
    private String strDrugAuthorizationCountry = "";
    private String strDrugAuthorizationHolder = "";
    private String strDrugStructureDosageNumb = "";
    private String strDrugStructureDosageUnit = "";
    private String strDrugSeparateDosageNumb = "";
    private String strDrugIntervalDosageUnitNumb = "";
    private String strDrugIntervalDosageDefinition = "";
    private String strDrugCumulativeDosageNumb = "";
    private String strDrugCumulativeDosageUnit = "";
    private String strDrugDosageText = "";
    private String strDrugDosageForm = "";
    private String strDrugAdministrationRoute = "";
    private String strDrugParAdministration = "";
    private String strReactionGestationPeriod = "";
    private String strReactionGestationPeriodUnit = "";
    private String strDrugIndicationMedDraVersion = "v. 2.3";
    private String strDrugIndication = "";
    private String strDrugStartDateFormat = "";
    private String strDrugStartDate = "";
    private String strDrugStartPeriod = "";
    private String strDrugStartPeriodUnit = "";
    private String strDrugLastPeriod = "";
    private String strDrugLastPeriodUnit = "";
    private String strDrugEndDateFormat = "";
    private String strDrugEndDate = "";
    private String strDrugTreatmentDuration = "";
    private String strDrugTreatmentDurationUnit = "";
    private String strActionDrug = "";
    private String strDrugRecurReadministration = "";
    private String strDrugAdditional = "";
    private ArrayList<ActiveSubstanceTag> alActiveSubstance = null;
    private ArrayList<DrugRecurrenceTag> alDrugRecurrence = null;
    private ArrayList<DrugReactionRelatednessTag> alDrugReactionRelatedness = null;

    /**
     * Generates valid and well formed &lt;drug&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;drug&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<drug> \n");
            sb.append("    <drugcharacterization>" + strDrugCharacterization + "</drugcharacterization> \n");
            sb.append("    <medicinalproduct>" + strMedicinalProduct + "</medicinalproduct> \n");
            sb.append("    <obtaindrugcountry>" + strObtainDrugCountry + "</obtaindrugcountry> \n");
            sb.append("    <drugbatchnumb>" + strDrugBatchNumb + "</drugbatchnumb> \n");
            sb.append("    <drugauthorizationnumb>" + strDrugAuthorizationNumb + "</drugauthorizationnumb> \n");
            sb.append("    <drugauthorizationcountry>" + strDrugAuthorizationCountry + "</drugauthorizationcountry> \n");
            sb.append("    <drugauthorizationholder>" + strDrugAuthorizationHolder + "</drugauthorizationholder> \n");
            sb.append("    <drugstructuredosagenumb>" + strDrugStructureDosageNumb + "</drugstructuredosagenumb> \n");
            sb.append("    <drugstructuredosageunit>" + strDrugStructureDosageUnit + "</drugstructuredosageunit> \n");
            sb.append("    <drugseparatedosagenumb>" + strDrugSeparateDosageNumb + "</drugseparatedosagenumb> \n");
            sb.append("    <drugintervaldosageunitnumb>" + strDrugIntervalDosageUnitNumb + "</drugintervaldosageunitnumb> \n");
            sb.append("    <drugintervaldosagedefinition>" + strDrugIntervalDosageDefinition + "</drugintervaldosagedefinition> \n");
            sb.append("    <drugcumulativedosagenumb>" + strDrugCumulativeDosageNumb + "</drugcumulativedosagenumb> \n");
            sb.append("    <drugcumulativedosageunit>" + strDrugCumulativeDosageUnit + "</drugcumulativedosageunit> \n");
            sb.append("    <drugdosagetext>" + strDrugDosageText + "</drugdosagetext> \n");
            sb.append("    <drugdosageform>" + strDrugDosageForm + "</drugdosageform> \n");
            sb.append("    <drugadministrationroute>" + strDrugAdministrationRoute + "</drugadministrationroute> \n");
            sb.append("    <drugparadministration>" + strDrugParAdministration + "</drugparadministration> \n");
            sb.append("    <reactiongestationperiod>" + strReactionGestationPeriod + "</reactiongestationperiod> \n");
            sb.append("    <reactiongestationperiodunit>" + strReactionGestationPeriodUnit + "</reactiongestationperiodunit> \n");
            sb.append("    <drugindicationmeddraversion>" + strDrugIndicationMedDraVersion + "</drugindicationmeddraversion> \n");
            sb.append("    <drugindication>" + strDrugIndication + "</drugindication> \n");
            sb.append("    <drugstartdateformat>" + strDrugStartDateFormat + "</drugstartdateformat> \n");
            sb.append("    <drugstartdate>" + strDrugStartDate + "</drugstartdate> \n");
            sb.append("    <drugstartperiod>" + strDrugStartPeriod + "</drugstartperiod> \n");
            sb.append("    <drugstartperiodunit>" + strDrugStartPeriodUnit + "</drugstartperiodunit> \n");
            sb.append("    <druglastperiod>" + strDrugLastPeriod + "</druglastperiod> \n");
            sb.append("    <druglastperiodunit>" + strDrugLastPeriodUnit + "</druglastperiodunit> \n");
            sb.append("    <drugenddateformat>" + strDrugEndDateFormat + "</drugenddateformat> \n");
            sb.append("    <drugenddate>" + strDrugEndDate + "</drugenddate> \n");
            sb.append("    <drugtreatmentduration>" + strDrugTreatmentDuration + "</drugtreatmentduration> \n");
            sb.append("    <drugtreatmentdurationunit>" + strDrugTreatmentDurationUnit + "</drugtreatmentdurationunit> \n");
            sb.append("    <actiondrug>" + strActionDrug + "</actiondrug> \n");
            sb.append("    <drugrecurreadministration>" + strDrugRecurReadministration + "</drugrecurreadministration> \n");
            sb.append("    <drugadditional>" + strDrugAdditional + "</drugadditional> \n");

            if (alActiveSubstance != null) {
                for (int i = 0; i < alActiveSubstance.size(); i++) {
                    sb.append("    <!-- ActiveSubstance tag : " + (i + 1) + " of " + alActiveSubstance.size() + " --> \n");
                    sb.append("    " + alActiveSubstance.get(i).getTag());
                }
            }

            if (alDrugRecurrence != null) {
                for (int i = 0; i < alDrugRecurrence.size(); i++) {
                    sb.append("    <!-- DrugRecurrence tag : " + (i + 1) + " of " + alDrugRecurrence.size() + " --> \n");
                    sb.append("    " + alDrugRecurrence.get(i).getTag());
                }
            }

            if (alDrugReactionRelatedness != null) {
                for (int i = 0; i < alDrugReactionRelatedness.size(); i++) {
                    sb.append("    <!-- DrugReactionRelatedness tag : " + (i + 1) + " of " + alDrugReactionRelatedness.size() + " --> \n");
                    sb.append("    " + alDrugReactionRelatedness.get(i).getTag());
                }
            }
            sb.append("</drug> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    /**
     * The function fills all the fields from TagData class.
     */
    @Override
    public void fetchData() {
        this.strDrugCharacterization = "";
        if (TagData.getInstance().aCase.getProdcode() != null) {
            this.strMedicinalProduct = TagData.getInstance().aCase.getProdcode();
        }
        this.strObtainDrugCountry = "";
        if (TagData.getInstance().aCase.getBatch_Num() != null) {
            this.strDrugBatchNumb = TagData.getInstance().aCase.getBatch_Num();
        }
        if (TagData.getInstance().aCase.getAuthorization_No() != null) {
            this.strDrugAuthorizationNumb = TagData.getInstance().aCase.getAuthorization_No();
        }
        this.strDrugAuthorizationCountry = "";
        this.strDrugAuthorizationHolder = "";
        float fDose = 0f;
        if (TagData.getInstance().aCase.getDose() != -1f) {
            this.strDrugStructureDosageNumb = "" + TagData.getInstance().aCase.getDose();
            fDose = TagData.getInstance().aCase.getDose();
        }
        if (TagData.getInstance().aCase.getUnitcode() != null) {
            this.strDrugStructureDosageUnit = TagData.getInstance().aCase.getUnitcode();
        }
        int nNoOfDosage = 0;
        if (TagData.getInstance().aCase.getNoOfDosage() != -1) {
            this.strDrugSeparateDosageNumb = "" + TagData.getInstance().aCase.getNoOfDosage();
            nNoOfDosage = TagData.getInstance().aCase.getNoOfDosage();
        }
//        if (TagData.getInstance().aCase.getDose() != -1f &&
//                TagData.getInstance().aCase.getNoOfDosage() != -1) {
//            System.out.println("" + (fDose * nNoOfDosage));
        this.strDrugIntervalDosageUnitNumb = "" /*+ (fDose * nNoOfDosage)*/; //5.3* 5.1
//        }
        if (TagData.getInstance().aCase.getDosageInterval() != -1) {
            this.strDrugIntervalDosageDefinition = "" + TagData.getInstance().aCase.getDosageInterval();
        }
        this.strDrugCumulativeDosageNumb = "";
        this.strDrugCumulativeDosageUnit = "";
        this.strDrugDosageText = "";
        this.strDrugDosageForm = "";
        this.strDrugAdministrationRoute = "";
        this.strDrugParAdministration = "";
        this.strReactionGestationPeriod = "";
        this.strReactionGestationPeriodUnit = "";
        this.strDrugIndicationMedDraVersion = "v. 2.3";
        this.strDrugIndication = "";
        this.strDrugStartDateFormat = "";
        this.strDrugStartDate = "";
        this.strDrugStartPeriod = "";
        this.strDrugStartPeriodUnit = "";
        this.strDrugLastPeriod = "";
        this.strDrugLastPeriodUnit = "";
        this.strDrugEndDateFormat = "";
        this.strDrugEndDate = "";
        this.strDrugTreatmentDuration = "";
        this.strDrugTreatmentDurationUnit = "";
        this.strActionDrug = "";
        this.strDrugRecurReadministration = "";
        this.strDrugAdditional = "";

        //INFO: Sapher currently don't have this data.
        this.alActiveSubstance = null;

        this.alDrugRecurrence = new ArrayList<DrugRecurrenceTag>();
        DrugRecurrenceTag drt = new DrugRecurrenceTag();
        drt.fetchData();
        this.alDrugRecurrence.add(drt);

        // Prepare 2 drugreactionrelatedness tags,
        // One for Company Assessment of Causality and
        // other for Physician's Assessment of Causality.
        this.alDrugReactionRelatedness = new ArrayList<DrugReactionRelatednessTag>();
        DrugReactionRelatednessTag drrt = new DrugReactionRelatednessTag(
                "v. 2.3", /* MedDRA ver*/
                "",
                "COMPANY",
                "",
                TagData.getInstance().aCase.getCo_Assess_Flg() == null ? "" : TagData.getInstance().aCase.getCo_Assess_Flg());
        drrt.fetchData();
        this.alDrugReactionRelatedness.add(drrt);

        drrt = new DrugReactionRelatednessTag(
                "v. 2.3", /* MedDRA ver*/
                "",
                "PHYSICIAN",
                "",
                TagData.getInstance().aCase.getPhy_Assess_Flg() == null ? "" : TagData.getInstance().aCase.getPhy_Assess_Flg());
        drrt.fetchData();
        this.alDrugReactionRelatedness.add(drrt);
    }

    // Constructors
    /**
     * Default constructor that creates a new instance of DrugTag.
     * This constructor initializes its member variables to empty string.
     */
    public DrugTag() {
        this.strDrugCharacterization = "";
        this.strMedicinalProduct = "";
        this.strObtainDrugCountry = "";
        this.strDrugBatchNumb = "";
        this.strDrugAuthorizationNumb = "";
        this.strDrugAuthorizationCountry = "";
        this.strDrugAuthorizationHolder = "";
        this.strDrugStructureDosageNumb = "";
        this.strDrugStructureDosageUnit = "";
        this.strDrugSeparateDosageNumb = "";
        this.strDrugIntervalDosageUnitNumb = "";
        this.strDrugIntervalDosageDefinition = "";
        this.strDrugCumulativeDosageNumb = "";
        this.strDrugCumulativeDosageUnit = "";
        this.strDrugDosageText = "";
        this.strDrugDosageForm = "";
        this.strDrugAdministrationRoute = "";
        this.strDrugParAdministration = "";
        this.strReactionGestationPeriod = "";
        this.strReactionGestationPeriodUnit = "";
        this.strDrugIndicationMedDraVersion = "v. 2.3";
        this.strDrugIndication = "";
        this.strDrugStartDateFormat = "";
        this.strDrugStartDate = "";
        this.strDrugStartPeriod = "";
        this.strDrugStartPeriodUnit = "";
        this.strDrugLastPeriod = "";
        this.strDrugLastPeriodUnit = "";
        this.strDrugEndDateFormat = "";
        this.strDrugEndDate = "";
        this.strDrugTreatmentDuration = "";
        this.strDrugTreatmentDurationUnit = "";
        this.strActionDrug = "";
        this.strDrugRecurReadministration = "";
        this.strDrugAdditional = "";
        this.alActiveSubstance = null;
        this.alDrugRecurrence = null;
        this.alDrugReactionRelatedness = null;
    }

    /**
     *
     * @param DrugCharacterization         <code><b>String</b></code> value to set &lt;drugcharacterization&gt; tag.
     * @param MedicinalProduct             <code><b>String</b></code> value to set &lt;medicinalproduct&gt; tag.
     * @param ObtainDrugCountry            <code><b>String</b></code> value to set &lt;obtaindrugcountry&gt; tag.
     * @param DrugBatchNumb                <code><b>String</b></code> value to set &lt;drugbatchnumb&gt; tag.
     * @param DrugAuthorizationNumb        <code><b>String</b></code> value to set &lt;drugauthorizationnumb&gt; tag.
     * @param DrugAuthorizationCountry     <code><b>String</b></code> value to set &lt;drugauthorizationcountry&gt; tag.
     * @param DrugAuthorizationHolder      <code><b>String</b></code> value to set &lt;drugauthorizationholder&gt; tag.
     * @param DrugStructureDosageNumb      <code><b>String</b></code> value to set &lt;drugstructuredosagenumb&gt; tag.
     * @param DrugStructureDosageUnit      <code><b>String</b></code> value to set &lt;drugstructuredosageunit&gt; tag.
     * @param DrugSeparateDosageNumb       <code><b>String</b></code> value to set &lt;drugseparatedosagenumb&gt; tag.
     * @param DrugIntervalDosageUnitNumb   <code><b>String</b></code> value to set &lt;drugintervaldosageunitnumb&gt; tag.
     * @param DrugIntervalDosagedefinition <code><b>String</b></code> value to set &lt;drugintervaldosagedefinition&gt; tag.
     * @param DrugCumulativeDosageNumb     <code><b>String</b></code> value to set &lt;drugcumulativedosagenumb&gt; tag.
     * @param DrugCumulativeDosageUnit     <code><b>String</b></code> value to set &lt;drugcumulativedosageunit&gt; tag.
     * @param DrugDosageText               <code><b>String</b></code> value to set &lt;drugdosagetext&gt; tag.
     * @param DrugDosageForm               <code><b>String</b></code> value to set &lt;drugdosageform&gt; tag.
     * @param DrugAdministrationRoute      <code><b>String</b></code> value to set &lt;drugadministrationroute&gt; tag.
     * @param DrugParAdministration        <code><b>String</b></code> value to set &lt;drugparadministration&gt; tag.
     * @param ReactionGestationPeriod      <code><b>String</b></code> value to set &lt;reactiongestationperiod&gt; tag.
     * @param ReactionGestationPeriodUnit  <code><b>String</b></code> value to set &lt;reactiongestationperiodunit&gt; tag.
     * @param DrugIndicationMedDraVersion  <code><b>String</b></code> value to set &lt;drugindicationmeddraversion&gt; tag.
     * @param DrugIndication               <code><b>String</b></code> value to set &lt;drugindication&gt; tag.
     * @param DrugStartDateFormat          <code><b>String</b></code> value to set &lt;drugstartdateformat&gt; tag.
     * @param DrugStartDate                <code><b>String</b></code> value to set &lt;drugstartdate&gt; tag.
     * @param DrugStartPeriod              <code><b>String</b></code> value to set &lt;drugstartperiod&gt; tag.
     * @param DrugStartPeriodUnit          <code><b>String</b></code> value to set &lt;drugstartperiodunit&gt; tag.
     * @param DrugLastPeriod               <code><b>String</b></code> value to set &lt;druglastperiod&gt; tag.
     * @param DrugLastPeriodUnit           <code><b>String</b></code> value to set &lt;druglastperiodunit&gt; tag.
     * @param DrugEndDateFormat            <code><b>String</b></code> value to set &lt;drugenddateformat&gt; tag.
     * @param DrugEndDate                  <code><b>String</b></code> value to set &lt;drugenddate&gt; tag.
     * @param DrugTreatmentDuration        <code><b>String</b></code> value to set &lt;drugtreatmentduration&gt; tag.
     * @param DrugTreatmentDurationUnit    <code><b>String</b></code> value to set &lt;drugtreatmentdurationunit&gt; tag.
     * @param ActionDrug                   <code><b>String</b></code> value to set &lt;actiondrug&gt; tag.
     * @param DrugRecurReadministration    <code><b>String</b></code> value to set &lt;drugrecurreadministration&gt; tag.
     * @param DrugAdditional               <code><b>String</b></code> value to set &lt;drugadditional&gt; tag.
     * @param ActiveSubstance              <code><b>ArrayList</b></code> value to set &lt;ActiveSubstance&gt; tag.
     * @param DrugRecurrence               <code><b>ArrayList</b></code> value to set &lt;DrugRecurrence&gt; tag.
     * @param DrugReactionRelatedness      <code><b>ArrayList</b></code> value to set &lt;DrugReactionRelatedness&gt; tag.
     * @deprecated
     */
    @Deprecated
    public DrugTag(
            String DrugCharacterization,
            String MedicinalProduct,
            String ObtainDrugCountry,
            String DrugBatchNumb,
            String DrugAuthorizationNumb,
            String DrugAuthorizationCountry,
            String DrugAuthorizationHolder,
            String DrugStructureDosageNumb,
            String DrugStructureDosageUnit,
            String DrugSeparateDosageNumb,
            String DrugIntervalDosageUnitNumb,
            String DrugIntervalDosagedefinition,
            String DrugCumulativeDosageNumb,
            String DrugCumulativeDosageUnit,
            String DrugDosageText,
            String DrugDosageForm,
            String DrugAdministrationRoute,
            String DrugParAdministration,
            String ReactionGestationPeriod,
            String ReactionGestationPeriodUnit,
            String DrugIndicationMedDraVersion,
            String DrugIndication,
            String DrugStartDateFormat,
            String DrugStartDate,
            String DrugStartPeriod,
            String DrugStartPeriodUnit,
            String DrugLastPeriod,
            String DrugLastPeriodUnit,
            String DrugEndDateFormat,
            String DrugEndDate,
            String DrugTreatmentDuration,
            String DrugTreatmentDurationUnit,
            String ActionDrug,
            String DrugRecurReadministration,
            String DrugAdditional,
            ArrayList<ActiveSubstanceTag> ActiveSubstance,
            ArrayList<DrugRecurrenceTag> DrugRecurrence,
            ArrayList<DrugReactionRelatednessTag> DrugReactionRelatedness) {
        this.strDrugCharacterization = DrugCharacterization;
        this.strMedicinalProduct = MedicinalProduct;
        this.strObtainDrugCountry = ObtainDrugCountry;
        this.strDrugBatchNumb = DrugBatchNumb;
        this.strDrugAuthorizationNumb = DrugAuthorizationNumb;
        this.strDrugAuthorizationCountry = DrugAuthorizationCountry;
        this.strDrugAuthorizationHolder = DrugAuthorizationHolder;
        this.strDrugStructureDosageNumb = DrugStructureDosageNumb;
        this.strDrugStructureDosageUnit = DrugStructureDosageUnit;
        this.strDrugSeparateDosageNumb = DrugSeparateDosageNumb;
        this.strDrugIntervalDosageUnitNumb = DrugIntervalDosageUnitNumb;
        this.strDrugIntervalDosageDefinition = DrugIntervalDosagedefinition;
        this.strDrugCumulativeDosageNumb = DrugCumulativeDosageNumb;
        this.strDrugCumulativeDosageUnit = DrugCumulativeDosageUnit;
        this.strDrugDosageText = DrugDosageText;
        this.strDrugDosageForm = DrugDosageForm;
        this.strDrugAdministrationRoute = DrugAdministrationRoute;
        this.strDrugParAdministration = DrugParAdministration;
        this.strReactionGestationPeriod = ReactionGestationPeriod;
        this.strReactionGestationPeriodUnit = ReactionGestationPeriodUnit;
        this.strDrugIndicationMedDraVersion = "v. 2.3";
        this.strDrugIndication = DrugIndication;
        this.strDrugStartDateFormat = DrugStartDateFormat;
        this.strDrugStartDate = DrugStartDate;
        this.strDrugStartPeriod = DrugStartPeriod;
        this.strDrugStartPeriodUnit = DrugStartPeriodUnit;
        this.strDrugLastPeriod = DrugLastPeriod;
        this.strDrugLastPeriodUnit = DrugLastPeriodUnit;
        this.strDrugEndDateFormat = DrugEndDateFormat;
        this.strDrugEndDate = DrugEndDate;
        this.strDrugTreatmentDuration = DrugTreatmentDuration;
        this.strDrugTreatmentDurationUnit = DrugTreatmentDurationUnit;
        this.strActionDrug = ActionDrug;
        this.strDrugRecurReadministration = DrugRecurReadministration;
        this.strDrugAdditional = DrugAdditional;
        this.alActiveSubstance = ActiveSubstance;
        this.alDrugRecurrence = DrugRecurrence;
        this.alDrugReactionRelatedness = DrugReactionRelatedness;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code>TRUE</code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // No mandatory fields. So, no InvalidTagException

        if (strDrugCharacterization.length() > MAXLEN_DRUGCHARACTERIZATION) {
            throw new InvalidDataException("Length of DrugCharacterization must be less than " + MAXLEN_DRUGCHARACTERIZATION);
        }
        if (strMedicinalProduct.length() > MAXLEN_MEDICINALPRODUCT) {
            throw new InvalidDataException("Length of MedicinalProduct must be less than " + MAXLEN_MEDICINALPRODUCT);
        }
        if (strObtainDrugCountry.length() > MAXLEN_OBTAINDRUGCOUNTRY) {
            throw new InvalidDataException("Length of ObtainDrugCountry must be less than " + MAXLEN_OBTAINDRUGCOUNTRY);
        }
        if (strDrugBatchNumb.length() > MAXLEN_DRUGBATCHNUMB) {
            throw new InvalidDataException("Length of DrugBatchNumb must be less than " + MAXLEN_DRUGBATCHNUMB);
        }
        if (strDrugAuthorizationNumb.length() > MAXLEN_DRUGAUTHORIZATIONNUMB) {
            throw new InvalidDataException("Length of DrugAuthorizationNumb must be less than " + MAXLEN_DRUGAUTHORIZATIONNUMB);
        }
        if (strDrugAuthorizationCountry.length() > MAXLEN_DRUGAUTHORIZATIONCOUNTRY) {
            throw new InvalidDataException("Length of DrugAuthorizationCountry must be less than " + MAXLEN_DRUGAUTHORIZATIONCOUNTRY);
        }
        if (strDrugAuthorizationHolder.length() > MAXLEN_DRUGAUTHORIZATIONHOLDER) {
            throw new InvalidDataException("Length of DrugAuthorizationHolder must be less than " + MAXLEN_DRUGAUTHORIZATIONHOLDER);
        }
        if (strDrugStructureDosageNumb.length() > MAXLEN_DRUGSTRUCTUREDOSAGENUMB) {
            throw new InvalidDataException("Length of DrugStructureDosageNumb must be less than " + MAXLEN_DRUGSTRUCTUREDOSAGENUMB);
        }
        if (strDrugStructureDosageUnit.length() > MAXLEN_DRUGSTRUCTUREDOSAGEUNIT) {
            throw new InvalidDataException("Length of DrugStructureDosageUnit must be less than " + MAXLEN_DRUGSTRUCTUREDOSAGEUNIT);
        }
        if (strDrugSeparateDosageNumb.length() > MAXLEN_DRUGSEPARATEDOSAGENUMB) {
            throw new InvalidDataException("Length of DrugSeparateDosageNumb must be less than " + MAXLEN_DRUGSEPARATEDOSAGENUMB);
        }
        if (strDrugIntervalDosageUnitNumb.length() > MAXLEN_DRUGINTERVALDOSAGEUNITNUMB) {
            throw new InvalidDataException("Length of DrugIntervalDosageUnitNumb must be less than " + MAXLEN_DRUGINTERVALDOSAGEUNITNUMB);
        }
        if (strDrugIntervalDosageDefinition.length() > MAXLEN_DRUGINTERVALDOSAGEDEFINITION) {
            throw new InvalidDataException("Length of DrugIntervalDosageDefinition must be less than " + MAXLEN_DRUGINTERVALDOSAGEDEFINITION);
        }
        if (strDrugCumulativeDosageNumb.length() > MAXLEN_DRUGCUMULATIVEDOSAGENUMB) {
            throw new InvalidDataException("Length of DrugCumulativeDosageNumb must be less than " + MAXLEN_DRUGCUMULATIVEDOSAGENUMB);
        }
        if (strDrugCumulativeDosageUnit.length() > MAXLEN_DRUGCUMULATIVEDOSAGEUNIT) {
            throw new InvalidDataException("Length of DrugCumulativeDosageUnit must be less than " + MAXLEN_DRUGCUMULATIVEDOSAGEUNIT);
        }
        if (strDrugDosageText.length() > MAXLEN_DRUGDOSAGETEXT) {
            throw new InvalidDataException("Length of DrugDosageText must be less than " + MAXLEN_DRUGDOSAGETEXT);
        }
        if (strDrugDosageForm.length() > MAXLEN_DRUGDOSAGEFORM) {
            throw new InvalidDataException("Length of DrugDosageForm must be less than " + MAXLEN_DRUGDOSAGEFORM);
        }
        if (strDrugAdministrationRoute.length() > MAXLEN_DRUGADMINISTRATIONROUTE) {
            throw new InvalidDataException("Length of DrugAdministrationRoute must be less than " + MAXLEN_DRUGADMINISTRATIONROUTE);
        }
        if (strDrugParAdministration.length() > MAXLEN_DRUGPARADMINISTRATION) {
            throw new InvalidDataException("Length of DrugParAdministration must be less than " + MAXLEN_DRUGPARADMINISTRATION);
        }
        if (strReactionGestationPeriod.length() > MAXLEN_REACTIONGESTATIONPERIOD) {
            throw new InvalidDataException("Length of ReactionGestationPeriod must be less than " + MAXLEN_REACTIONGESTATIONPERIOD);
        }
        if (strReactionGestationPeriodUnit.length() > MAXLEN_REACTIONGESTATIONPERIODUNIT) {
            throw new InvalidDataException("Length of ReactionGestationPeriodUnit must be less than " + MAXLEN_REACTIONGESTATIONPERIODUNIT);
        }
        if (strDrugIndicationMedDraVersion.length() > MAXLEN_DRUGINDICATIONMEDDRAVERSION) {
            throw new InvalidDataException("Length of DrugIndicationMedDraVersion must be less than " + MAXLEN_DRUGINDICATIONMEDDRAVERSION);
        }
        if (strDrugIndication.length() > MAXLEN_DRUGINDICATION) {
            throw new InvalidDataException("Length of DrugIndication must be less than " + MAXLEN_DRUGINDICATION);
        }
        if (strDrugStartDateFormat.length() > MAXLEN_DRUGSTARTDATEFORMAT) {
            throw new InvalidDataException("Length of DrugStartDateFormat must be less than " + MAXLEN_DRUGSTARTDATEFORMAT);
        }
        if (strDrugStartDate.length() > MAXLEN_DRUGSTARTDATE) {
            throw new InvalidDataException("Length of DrugStartDate must be less than " + MAXLEN_DRUGSTARTDATE);
        }
        if (strDrugStartPeriod.length() > MAXLEN_DRUGSTARTPERIOD) {
            throw new InvalidDataException("Length of DrugStartPeriod must be less than " + MAXLEN_DRUGSTARTPERIOD);
        }
        if (strDrugStartPeriodUnit.length() > MAXLEN_DRUGSTARTPERIODUNIT) {
            throw new InvalidDataException("Length of  must be less than " + MAXLEN_DRUGSTARTPERIODUNIT);
        }
        if (strDrugLastPeriod.length() > MAXLEN_DRUGLASTPERIOD) {
            throw new InvalidDataException("Length of DrugLastPeriod must be less than " + MAXLEN_DRUGLASTPERIOD);
        }
        if (strDrugLastPeriodUnit.length() > MAXLEN_DRUGLASTPERIODUNIT) {
            throw new InvalidDataException("Length of DrugLastPeriodUnit must be less than " + MAXLEN_DRUGLASTPERIODUNIT);
        }
        if (strDrugEndDateFormat.length() > MAXLEN_DRUGENDDATEFORMAT) {
            throw new InvalidDataException("Length of DrugEndDateFormat must be less than " + MAXLEN_DRUGENDDATEFORMAT);
        }
        if (strDrugEndDate.length() > MAXLEN_DRUGENDDATE) {
            throw new InvalidDataException("Length of DrugEndDate must be less than " + MAXLEN_DRUGENDDATE);
        }
        if (strDrugTreatmentDuration.length() > MAXLEN_DRUGTREATMENTDURATION) {
            throw new InvalidDataException("Length of DrugTreatmentDuration must be less than " + MAXLEN_DRUGTREATMENTDURATION);
        }
        if (strDrugTreatmentDurationUnit.length() > MAXLEN_DRUGTREATMENTDURATIONUNIT) {
            throw new InvalidDataException("Length of DrugTreatmentDurationUnit must be less than " + MAXLEN_DRUGTREATMENTDURATIONUNIT);
        }
        if (strActionDrug.length() > MAXLEN_ACTIONDRUG) {
            throw new InvalidDataException("Length of ActionDrug must be less than " + MAXLEN_ACTIONDRUG);
        }
        if (strDrugRecurReadministration.length() > MAXLEN_DRUGRECURREADMINISTRATION) {
            throw new InvalidDataException("Length of DrugRecurReadministration must be less than " + MAXLEN_DRUGRECURREADMINISTRATION);
        }
        if (strDrugAdditional.length() > MAXLEN_DRUGADDITIONAL) {
            throw new InvalidDataException("Length of DrugAdditional must be less than " + MAXLEN_DRUGADDITIONAL);
        }

        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strDrugCharacterization = null;
        this.strMedicinalProduct = null;
        this.strObtainDrugCountry = null;
        this.strDrugBatchNumb = null;
        this.strDrugAuthorizationNumb = null;
        this.strDrugAuthorizationCountry = null;
        this.strDrugAuthorizationHolder = null;
        this.strDrugStructureDosageNumb = null;
        this.strDrugStructureDosageUnit = null;
        this.strDrugSeparateDosageNumb = null;
        this.strDrugIntervalDosageUnitNumb = null;
        this.strDrugIntervalDosageDefinition = null;
        this.strDrugCumulativeDosageNumb = null;
        this.strDrugCumulativeDosageUnit = null;
        this.strDrugDosageText = null;
        this.strDrugDosageForm = null;
        this.strDrugAdministrationRoute = null;
        this.strDrugParAdministration = null;
        this.strReactionGestationPeriod = null;
        this.strReactionGestationPeriodUnit = null;
        this.strDrugIndicationMedDraVersion = null;
        this.strDrugIndication = null;
        this.strDrugStartDateFormat = null;
        this.strDrugStartDate = null;
        this.strDrugStartPeriod = null;
        this.strDrugStartPeriodUnit = null;
        this.strDrugLastPeriod = null;
        this.strDrugLastPeriodUnit = null;
        this.strDrugEndDateFormat = null;
        this.strDrugEndDate = null;
        this.strDrugTreatmentDuration = null;
        this.strDrugTreatmentDurationUnit = null;
        this.strActionDrug = null;
        this.strDrugRecurReadministration = null;
        this.strDrugAdditional = null;
        this.alActiveSubstance = null;
        this.alDrugRecurrence = null;
        this.alDrugReactionRelatedness = null;
    }
}
