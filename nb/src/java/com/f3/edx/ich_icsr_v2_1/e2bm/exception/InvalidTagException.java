package com.f3.edx.ich_icsr_v2_1.e2bm.exception;

/**
 *
 * @author Anoop Varma
 */
public class InvalidTagException extends Exception {

    /** */
    private static final long serialVersionUID = 519153989154022615L;

    /**
     * InvalidTagException constructor
     * 
     * @param strErrMsg as <code><b>String</b></code>
     */
    public InvalidTagException(String strErrMsg) {
        super(strErrMsg);
    }
}
