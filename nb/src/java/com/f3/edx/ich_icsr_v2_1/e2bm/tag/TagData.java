package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import bl.sapher.Adverse_Event;
import bl.sapher.CaseDetails;
import bl.sapher.Conc_Medication;
import bl.sapher.general.DBConnectionException;
import bl.sapher.general.GenericException;
import bl.sapher.general.RecordNotFoundException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anoop Varma
 */
public class TagData implements Serializable {

    /** Serial version UID */
    private static final long serialVersionUID = 5991037646078476539L;
    private static TagData me = null;
    int nRepNumber;
    public CaseDetails aCase;
    public ArrayList<Adverse_Event> alAE;
    public ArrayList<Conc_Medication> alCM;

    public static TagData getInstance() {
        if (me == null) {
            me = new TagData();
        }
        return me;
    }

    /**
     * 
     */
    public void init(String cpnyName) {
        try {
            aCase = new CaseDetails(cpnyName, "y", nRepNumber);
            alAE = Adverse_Event.listAEventFromView(cpnyName, nRepNumber);
            alCM = Conc_Medication.listConc_MedicationFromView(cpnyName, nRepNumber);

        } catch (DBConnectionException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
                Logger.getLogger(TagData.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (GenericException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
                Logger.getLogger(TagData.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (RecordNotFoundException ex) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ex.printStackTrace();
                Logger.getLogger(TagData.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void setRepNumber(String cpnyName, String repnum) {
        this.nRepNumber = Integer.parseInt(repnum);
        init(cpnyName);
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        TagData.me = null;
        this.nRepNumber = 0;
        this.aCase = null;
        this.alAE = null;
        this.alCM = null;
    }
}
