package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class PatientDeathCauseTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 1354976912961446664L;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientDeathReportMedDraVersion : <code><b>8</b></code></p>
     */
    private final int MAXLEN_PATIENTDEATHREPORTMEDDRAVERSION = 8;
    /**
     * <b>ICH ICSR Specification v2.3</b>
     * <p>Maximum lenght allowed for PatientDeathReport : <code><b>250</b></code></p>
     */
    private final int MAXLEN_PATIENTDEATHREPORT = 250;
    private String strPatientDeathReportMedDraVersion = "";
    private String strPatientDeathReport = "";

    /**
     * Generates valid and well formed &lt;patientdeathcause&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;patientdeathcause&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<patientdeathcause> \n");
            sb.append("    <patientdeathreportmeddraversion>" + strPatientDeathReportMedDraVersion + "</patientdeathreportmeddraversion> \n");
            sb.append("    <patientdeathreport>" + strPatientDeathReport + "</patientdeathreport> \n");
            sb.append("</patientdeathcause> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
        this.strPatientDeathReportMedDraVersion = "";
        if (TagData.getInstance().aCase.getAutopsy_Result() != null) {
            this.strPatientDeathReport = TagData.getInstance().aCase.getAutopsy_Result();
        }
    }

    // Constructor
    /**
     * Default constructor for ParentPastDrugTherapyTag.
     * This constructor initializes its member variables to empty string.
     */
    public PatientDeathCauseTag() {
        this.strPatientDeathReportMedDraVersion = "";
        this.strPatientDeathReport = "";
    }

    /**
     * Constructor for ParentPastDrugTherapyTag.
     * 
     * @param PatientDeathReportMedDraVersion as <code><b>String</b></code>
     * @param PatientDeathReport              as <code><b>String</b></code>
     * @deprecated 
     */
    @Deprecated
    public PatientDeathCauseTag(
            String PatientDeathReportMedDraVersion,
            String PatientDeathReport) {
        this.strPatientDeathReportMedDraVersion = PatientDeathReportMedDraVersion;
        this.strPatientDeathReport = PatientDeathReport;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        if (this.strPatientDeathReportMedDraVersion.length() > MAXLEN_PATIENTDEATHREPORTMEDDRAVERSION) {
            throw new InvalidDataException("Length of PatientDeathReportMedDraVersion must be less than " + MAXLEN_PATIENTDEATHREPORTMEDDRAVERSION);
        }
        if (this.strPatientDeathReport.length() > MAXLEN_PATIENTDEATHREPORT) {
            throw new InvalidDataException("Length of PatientDeathReport must be less than " + MAXLEN_PATIENTDEATHREPORT);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPatientDeathReportMedDraVersion = null;
        this.strPatientDeathReport = null;
    }
}
