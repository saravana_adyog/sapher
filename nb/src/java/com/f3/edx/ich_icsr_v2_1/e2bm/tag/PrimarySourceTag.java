package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 * <b>primarysource</b> is a mandatory tag.
 * 
 * @author Anoop Varma
 */
public class PrimarySourceTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 7681194164766990550L;
    private int MAXLEN_REPORTERTITLE = 50;
    private int MAXLEN_REPORTERGIVENAME = 35;
    private int MAXLEN_REPORTERMIDDLENAME = 15;
    private int MAXLEN_REPORTERFAMILYNAME = 50;
    private int MAXLEN_REPORTERORGANIZATION = 60;
    private int MAXLEN_REPORTERDEPARTMENT = 60;
    private int MAXLEN_REPORTERSTREET = 100;
    private int MAXLEN_REPORTERCITY = 35;
    private int MAXLEN_REPORTERSTATE = 40;
    private int MAXLEN_REPORTERPOSTCODE = 15;
    private int MAXLEN_REPORTERCOUNTRY = 2;
    private int MAXLEN_QUALIFICATION = 1;
    private int MAXLEN_LITERATUREREFERENCE = 500;
    private int MAXLEN_STUDYNAME = 100;
    private int MAXLEN_SPONSORSTUDYNUMB = 35;
    private int MAXLEN_OBSERVESTUDYTYPE = 1;
    /////////////////////////////////////////
    private String strReporterTitle = "";           // 0 or 1 occurance
    private String strReporterGiveName = "";        // 0 or 1 occurance
    private String strReporterMiddleName = "";      // 0 or 1 occurance
    private String strReporterFamilyName = "";      // 0 or 1 occurance
    private String strReporterOrganization = "";    // 0 or 1 occurance
    private String strReporterDepartment = "";      // 0 or 1 occurance
    private String strReporterStreet = "";          // 0 or 1 occurance
    private String strReporterCity = "";            // 0 or 1 occurance
    private String strReporterState = "";           // 0 or 1 occurance
    private String strReporterPostCode = "";        // 0 or 1 occurance
    private String strReporterCountry = "";         // 0 or 1 occurance
    private String strQualification = "";           // 0 or 1 occurance
    private String strLiteratureReference = "";     // 0 or 1 occurance
    private String strStudyName = "";               // 0 or 1 occurance
    private String strSponsorStudynumb = "";        // 0 or 1 occurance
    private String strObserveStudytype = "";        // 0 or 1 occurance

    /**
     * Generates valid and well formed &lt;primarysource&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;primarysource&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<primarysource> \n");
            sb.append("    <reportertitle>" + strReporterTitle + "</reportertitle> \n");
            sb.append("    <reportergivename>" + strReporterGiveName + "</reportergivename> \n");
            sb.append("    <reportermiddlename>" + strReporterMiddleName + "</reportermiddlename> \n");
            sb.append("    <reporterfamilyname>" + strReporterFamilyName + "</reporterfamilyname> \n");
            sb.append("    <reporterorganization>" + strReporterOrganization + "</reporterorganization> \n");
            sb.append("    <reporterdepartment>" + strReporterDepartment + "</reporterdepartment> \n");
            sb.append("    <reporterstreet>" + strReporterStreet + "</reporterstreet> \n");
            sb.append("    <reportercity>" + strReporterCity + "</reportercity> \n");
            sb.append("    <reporterstate>" + strReporterState + "</reporterstate> \n");
            sb.append("    <reporterpostcode>" + strReporterPostCode + "</reporterpostcode> \n");
            sb.append("    <reportercountry>" + strReporterCountry + "</reportercountry> \n");
            sb.append("    <qualification>" + strQualification + "</qualification> \n");
            sb.append("    <literaturereference>" + strLiteratureReference + "</literaturereference> \n");
            sb.append("    <studyname>" + strStudyName + "</studyname> \n");
            sb.append("    <sponsorstudynumb>" + strSponsorStudynumb + "</sponsorstudynumb> \n");
            sb.append("    <observestudytype>" + strObserveStudytype + "</observestudytype> \n");
            sb.append("</primarysource> \n");
            return sb;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        }
    }

    @Override
    public void fetchData() {
        this.strReporterTitle = "";
        if (TagData.getInstance().aCase.getClin_Rep_Info() != null) {
            this.strReporterGiveName = TagData.getInstance().aCase.getClin_Rep_Info();
        }
        this.strReporterMiddleName = "";
        this.strReporterFamilyName = "";
        if (TagData.getInstance().aCase.getHosp_Nm() != null) {
            this.strReporterOrganization = TagData.getInstance().aCase.getHosp_Nm();
        }
        this.strReporterDepartment = "";
        this.strReporterStreet = "";
        this.strReporterCity = "";
        this.strReporterState = "";
        this.strReporterPostCode = "";
        this.strReporterCountry = "";
        this.strQualification = "";
        this.strLiteratureReference = "";
        this.strStudyName = "";
        this.strSponsorStudynumb = "";
        this.strObserveStudytype = "";
    }

    // Constructors
    /**
     * Default constructor for PrimarySourceTag.
     * This constructor initializes its member variables to empty string.
     */
    public PrimarySourceTag() {
        this.strReporterTitle = "";
        this.strReporterGiveName = "";
        this.strReporterMiddleName = "";
        this.strReporterFamilyName = "";
        this.strReporterOrganization = "";
        this.strReporterDepartment = "";
        this.strReporterStreet = "";
        this.strReporterCity = "";
        this.strReporterState = "";
        this.strReporterPostCode = "";
        this.strReporterCountry = "";
        this.strQualification = "";
        this.strLiteratureReference = "";
        this.strStudyName = "";
        this.strSponsorStudynumb = "";
        this.strObserveStudytype = "";
    }

    /**
     * Constructor for PrimarySourceTag.
     *
     * @param ReporterTitle        <code><b>String</b></code> value to set &lt;reportertitle&gt; tag.
     * @param ReporterGiveName     <code><b>String</b></code> value to set &lt;reportergivename&gt; tag.
     * @param ReporterMiddleName   <code><b>String</b></code> value to set &lt;reportermiddlename&gt; tag.
     * @param ReporterFamilyName   <code><b>String</b></code> value to set &lt;reporterfamilyname&gt; tag.
     * @param ReporterOrganization <code><b>String</b></code> value to set &lt;reporterorganization&gt; tag.
     * @param ReporterDepartment   <code><b>String</b></code> value to set &lt;reporterdepartment&gt; tag.
     * @param ReporterStreet       <code><b>String</b></code> value to set &lt;reporterstreet&gt; tag.
     * @param ReporterCity         <code><b>String</b></code> value to set &lt;reportercity&gt; tag.
     * @param ReporterState        <code><b>String</b></code> value to set &lt;reporterstate&gt; tag.
     * @param ReporterPostCode     <code><b>String</b></code> value to set &lt;reporterpostcode&gt; tag.
     * @param ReporterCountry      <code><b>String</b></code> value to set &lt;reportercountry&gt; tag.
     * @param Qualification        <code><b>String</b></code> value to set &lt;qualification&gt; tag.
     * @param LiteratureReference  <code><b>String</b></code> value to set &lt;literaturereference&gt; tag.
     * @param StudyName            <code><b>String</b></code> value to set &lt;studyname&gt; tag.
     * @param SponsorStudynumb     <code><b>String</b></code> value to set &lt;sponsorstudynumb&gt; tag.
     * @param ObserveStudytype     <code><b>String</b></code> value to set &lt;observestudytype&gt; tag.
     * @deprecated
     */
    @Deprecated
    public PrimarySourceTag(
            String ReporterTitle,
            String ReporterGiveName,
            String ReporterMiddleName,
            String ReporterFamilyName,
            String ReporterOrganization,
            String ReporterDepartment,
            String ReporterStreet,
            String ReporterCity,
            String ReporterState,
            String ReporterPostCode,
            String ReporterCountry,
            String Qualification,
            String LiteratureReference,
            String StudyName,
            String SponsorStudynumb,
            String ObserveStudytype) {
        this.strReporterTitle = ReporterTitle;
        this.strReporterGiveName = ReporterGiveName;
        this.strReporterMiddleName = ReporterMiddleName;
        this.strReporterFamilyName = ReporterFamilyName;
        this.strReporterOrganization = ReporterOrganization;
        this.strReporterDepartment = ReporterDepartment;
        this.strReporterStreet = ReporterStreet;
        this.strReporterCity = ReporterCity;
        this.strReporterState = ReporterState;
        this.strReporterPostCode = ReporterPostCode;
        this.strReporterCountry = ReporterCountry;
        this.strQualification = Qualification;
        this.strLiteratureReference = LiteratureReference;
        this.strStudyName = StudyName;
        this.strSponsorStudynumb = SponsorStudynumb;
        this.strObserveStudytype = ObserveStudytype;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1] Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException
     */
    @Override
    public boolean validateTag() throws InvalidDataException, InvalidTagException {
        // No validation needed as all fields can be null

        if (strReporterTitle.length() > MAXLEN_REPORTERTITLE) {
            throw new InvalidDataException("Length of ReporterTitle must be less than " + MAXLEN_REPORTERTITLE);
        }
        if (strReporterGiveName.length() > MAXLEN_REPORTERGIVENAME) {
            throw new InvalidDataException("Length of ReporterGiveName must be less than " + MAXLEN_REPORTERGIVENAME);
        }
        if (strReporterMiddleName.length() > MAXLEN_REPORTERMIDDLENAME) {
            throw new InvalidDataException("Length of ReporterMiddleName must be less than " + MAXLEN_REPORTERMIDDLENAME);
        }
        if (strReporterFamilyName.length() > MAXLEN_REPORTERFAMILYNAME) {
            throw new InvalidDataException("Length of ReporterFamilyName must be less than " + MAXLEN_REPORTERFAMILYNAME);
        }
        if (strReporterOrganization.length() > MAXLEN_REPORTERORGANIZATION) {
            throw new InvalidDataException("Length of ReporterOrganization must be less than " + MAXLEN_REPORTERORGANIZATION);
        }
        if (strReporterDepartment.length() > MAXLEN_REPORTERDEPARTMENT) {
            throw new InvalidDataException("Length of ReporterDepartment must be less than " + MAXLEN_REPORTERDEPARTMENT);
        }
        if (strReporterStreet.length() > MAXLEN_REPORTERSTREET) {
            throw new InvalidDataException("Length of ReporterStreet must be less than " + MAXLEN_REPORTERSTREET);
        }
        if (strReporterCity.length() > MAXLEN_REPORTERCITY) {
            throw new InvalidDataException("Length of ReporterCity must be less than " + MAXLEN_REPORTERCITY);
        }
        if (strReporterState.length() > MAXLEN_REPORTERSTATE) {
            throw new InvalidDataException("Length of ReporterState must be less than " + MAXLEN_REPORTERSTATE);
        }
        if (strReporterPostCode.length() > MAXLEN_REPORTERPOSTCODE) {
            throw new InvalidDataException("Length of ReporterPostCode must be less than " + MAXLEN_REPORTERPOSTCODE);
        }
        if (strReporterCountry.length() > MAXLEN_REPORTERCOUNTRY) {
            throw new InvalidDataException("Length of ReporterCountry must be less than " + MAXLEN_REPORTERCOUNTRY);
        }
        if (strQualification.length() > MAXLEN_QUALIFICATION) {
            throw new InvalidDataException("Length of Qualification must be less than " + MAXLEN_QUALIFICATION);
        }
        if (strLiteratureReference.length() > MAXLEN_LITERATUREREFERENCE) {
            throw new InvalidDataException("Length of LiteratureReference must be less than " + MAXLEN_LITERATUREREFERENCE);
        }
        if (strStudyName.length() > MAXLEN_STUDYNAME) {
            throw new InvalidDataException("Length of StudyName must be less than " + MAXLEN_STUDYNAME);
        }
        if (strSponsorStudynumb.length() > MAXLEN_SPONSORSTUDYNUMB) {
            throw new InvalidDataException("Length of SponsorStudynumb must be less than " + MAXLEN_SPONSORSTUDYNUMB);
        }
        if (strObserveStudytype.length() > MAXLEN_OBSERVESTUDYTYPE) {
            throw new InvalidDataException("Length of ObserveStudytype must be less than " + MAXLEN_OBSERVESTUDYTYPE);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strReporterTitle = null;
        this.strReporterGiveName = null;
        this.strReporterMiddleName = null;
        this.strReporterFamilyName = null;
        this.strReporterOrganization = null;
        this.strReporterDepartment = null;
        this.strReporterStreet = null;
        this.strReporterCity = null;
        this.strReporterState = null;
        this.strReporterPostCode = null;
        this.strReporterCountry = null;
        this.strQualification = null;
        this.strLiteratureReference = null;
        this.strStudyName = null;
        this.strSponsorStudynumb = null;
        this.strObserveStudytype = null;
    }
}
