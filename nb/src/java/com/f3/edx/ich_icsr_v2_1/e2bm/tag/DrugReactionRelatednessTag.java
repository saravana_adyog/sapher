package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class DrugReactionRelatednessTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 364506387074710777L;
    private final int MAXLEN_DRUGREACTIONASSESMEDDRAVERSION = 8;
    private final int MAXLEN_DRUGREACTIONASSES = 250;
    private final int MAXLEN_DRUGASSESSMENTSOURCE = 60;
    private final int MAXLEN_DRUGASSESSMENTMETHOD = 35;
    private final int MAXLEN_DRUGRESULT = 35;
    //////////////////////////////////////////////////////////////////
    private String strDrugReactionAssesMedDraVersion = "v. 2.3";    // 0 or 1 occurance
    private String strDrugReactionAsses = "";                       // 0 or 1 occurance
    private String strDrugAssessmentSource = "";                    // 0 or 1 occurance
    private String strDrugAssessmentMethod = "";                    // 0 or 1 occurance
    private String strDrugResult = "";                              // 0 or 1 occurance

    /**
     * Generates valid and well formed &lt;drugreactionrelatedness&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;drugreactionrelatedness&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<drugreactionrelatedness> \n");
            sb.append("     <drugreactionassesmeddraversion>" + strDrugReactionAssesMedDraVersion + "</drugreactionassesmeddraversion> \n");
            sb.append("     <drugreactionasses>" + strDrugReactionAsses + "</drugreactionasses> \n");
            sb.append("     <drugassessmentsource>" + strDrugAssessmentSource + "</drugassessmentsource> \n");
            sb.append("     <drugassessmentmethod>" + strDrugAssessmentMethod + "</drugassessmentmethod> \n");
            sb.append("     <drugresult>" + strDrugResult + "</drugresult> \n");
            sb.append("</drugreactionrelatedness> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    /**
     * 
     */
    @Override
    public void fetchData() {
    }

    // Constructors
    /**
     * Default constructor that creates a new instance of DrugReactionRelatednessTag.
     * This constructor initializes its member variables to empty string.
     */
    public DrugReactionRelatednessTag() {
        this.strDrugReactionAssesMedDraVersion = "v. 2.3";
        this.strDrugReactionAsses = "";
        this.strDrugAssessmentSource = "";
        this.strDrugAssessmentMethod = "";
        this.strDrugResult = "";
    }

    /**
     *
     * @param DrugReactionAssesMedDraVersion <code><b>String</b></code> value to set &lt;drugreactionassesmeddraversion&gt; tag.
     * @param DrugReactionAsses              <code><b>String</b></code> value to set &lt;drugreactionasses&gt; tag.
     * @param DrugAssessmentSource           <code><b>String</b></code> value to set &lt;drugassessmentsource&gt; tag.
     * @param DrugAssessmentMethod           <code><b>String</b></code> value to set &lt;drugassessmentmethod&gt; tag.
     * @param DrugResult                     <code><b>String</b></code> value to set &lt;drugresult&gt; tag.
     */
    public DrugReactionRelatednessTag(
            String DrugReactionAssesMedDraVersion,
            String DrugReactionAsses,
            String DrugAssessmentSource,
            String DrugAssessmentMethod,
            String DrugResult) {
        this.strDrugReactionAssesMedDraVersion = "v. 2.3";
        this.strDrugReactionAsses = DrugReactionAsses;
        this.strDrugAssessmentSource = DrugAssessmentSource;
        this.strDrugAssessmentMethod = DrugAssessmentMethod;
        this.strDrugResult = DrugResult;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {

        if (strDrugReactionAssesMedDraVersion.length() > MAXLEN_DRUGREACTIONASSESMEDDRAVERSION) {
            throw new InvalidDataException("Length of DrugReactionAssesMedDraVersion must be less than " + MAXLEN_DRUGREACTIONASSESMEDDRAVERSION);
        }
        if (strDrugReactionAsses.length() > MAXLEN_DRUGREACTIONASSES) {
            throw new InvalidDataException("Length of DrugReactionAsses must be less than " + MAXLEN_DRUGREACTIONASSES);
        }
        if (strDrugAssessmentSource.length() > MAXLEN_DRUGASSESSMENTSOURCE) {
            throw new InvalidDataException("Length of DrugAssessmentSource must be less than " + MAXLEN_DRUGASSESSMENTSOURCE);
        }
        if (strDrugAssessmentMethod.length() > MAXLEN_DRUGASSESSMENTMETHOD) {
            throw new InvalidDataException("Length of DrugAssessmentMethod must be less than " + MAXLEN_DRUGASSESSMENTMETHOD);
        }
        if (strDrugResult.length() > MAXLEN_DRUGRESULT) {
            throw new InvalidDataException("Length of DrugResult must be less than " + MAXLEN_DRUGRESULT);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strDrugReactionAssesMedDraVersion = null;
        this.strDrugReactionAsses = null;
        this.strDrugAssessmentSource = null;
        this.strDrugAssessmentMethod = null;
        this.strDrugResult = null;
    }
}
