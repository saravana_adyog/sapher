package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 * 
 * @author Anoop Varma
 */
public class PatientPastDrugTherapyTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -3299322041984049527L;
    private final int MAXLEN_PATIENTDRUGNAME = 100;
    private final int MAXLEN_PATIENTDRUGSTARTDATEFORMAT = 3;
    private final int MAXLEN_PATIENTDRUGSTARTDATE = 8;
    private final int MAXLEN_PATIENTDRUGENDDATEFORMAT = 3;
    private final int MAXLEN_PATIENTDRUGENDDATE = 8;
    private final int MAXLEN_PATIENTINDICATIONMEDDRAVERSION = 8;
    private final int MAXLEN_PATIENTDRUGINDICATION = 250;
    private final int MAXLEN_PATIENTDRGREACTIONMEDDRAVERSION = 8;
    private final int MAXLEN_PATIENTDRUGREACTION = 250;
    ////////////////////////////////////////////
    private String strPatientDrugName = "";
    private String strPatientDrugStartDateFormat = "";
    private String strPatientDrugStartDate = "";
    private String strPatientDrugEndDateFormat = "";
    private String strPatientDrugEndDate = "";
    private String strPatientIndicationMedDraVersion = "";
    private String strPatientDrugIndication = "";
    private String strPatientDrgReactionMedDraVersion = "";
    private String strPatientDrugReaction = "";

    /**
     * Generates valid and well formed &lt;patientpastdrugtherapy&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;patientpastdrugtherapy&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<patientpastdrugtherapy>  \n");
            sb.append("    <patientdrugname>" + strPatientDrugName + "</patientdrugname>  \n");
            sb.append("    <patientdrugstartdateformat>" + strPatientDrugStartDateFormat + "</patientdrugstartdateformat>  \n");
            sb.append("    <patientdrugstartdate>" + strPatientDrugStartDate + "</patientdrugstartdate>  \n");
            sb.append("    <patientdrugenddateformat>" + strPatientDrugEndDateFormat + "</patientdrugenddateformat>  \n");
            sb.append("    <patientdrugenddate>" + strPatientDrugEndDate + "</patientdrugenddate>  \n");
            sb.append("    <patientindicationmeddraversion>" + strPatientIndicationMedDraVersion + "</patientindicationmeddraversion>  \n");
            sb.append("    <patientdrugindication>" + strPatientDrugIndication + "</patientdrugindication>  \n");
            sb.append("    <patientdrgreactionmeddraversion>" + strPatientDrgReactionMedDraVersion + "</patientdrgreactionmeddraversion>  \n");
            sb.append("    <patientdrugreaction>" + strPatientDrugReaction + "</patientdrugreaction>  \n");
            sb.append("</patientpastdrugtherapy>  \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    @Override
    public void fetchData() {
    }

    // Constructors
    /**
     * Default constructor for PatientPastDrugTherapyTag.
     * This constructor initializes its member variables to empty string.
     */
    public PatientPastDrugTherapyTag() {
        this.strPatientDrugName = "";
        this.strPatientDrugStartDateFormat = "";
        this.strPatientDrugStartDate = "";
        this.strPatientDrugEndDateFormat = "";
        this.strPatientDrugEndDate = "";
        this.strPatientIndicationMedDraVersion = "";
        this.strPatientDrugIndication = "";
        this.strPatientDrgReactionMedDraVersion = "";
        this.strPatientDrugReaction = "";
    }

    /**
     * Constructor for PatientPastDrugTherapyTag.
     *
     * @param PatientDrugName <code><b>String</b></code> value to set &lt;patientdrugname&gt; tag.
     * @param PatientDrugStartDateFormat <code><b>String</b></code> value to set &lt;patientdrugstartdateformat&gt; tag.
     * @param PatientDrugStartDate <code><b>String</b></code> value to set &lt;patientdrugstartdate&gt; tag.
     * @param PatientDrugEndDateFormat <code><b>String</b></code> value to set &lt;patientdrugenddateformat&gt; tag.
     * @param PatientDrugEndDate <code><b>String</b></code> value to set &lt;patientdrugenddate&gt; tag.
     * @param PatientIndicationMedDraVersion <code><b>String</b></code> value to set &lt;patientindicationmeddraversion&gt; tag.
     * @param PatientDrugIndication <code><b>String</b></code> value to set &lt;patientdrugindication&gt; tag.
     * @param PatientDrgReactionMedDraVersion <code><b>String</b></code> value to set &lt;patientdrgreactionmeddraversion&gt; tag.
     * @param PatientDrugReaction <code><b>String</b></code> value to set &lt;patientdrugreaction&gt; tag.
     */
    public PatientPastDrugTherapyTag(
            String PatientDrugName,
            String PatientDrugStartDateFormat,
            String PatientDrugStartDate,
            String PatientDrugEndDateFormat,
            String PatientDrugEndDate,
            String PatientIndicationMedDraVersion,
            String PatientDrugIndication,
            String PatientDrgReactionMedDraVersion,
            String PatientDrugReaction) {
        this.strPatientDrugName = PatientDrugName;
        this.strPatientDrugStartDateFormat = PatientDrugStartDateFormat;
        this.strPatientDrugStartDate = PatientDrugStartDate;
        this.strPatientDrugEndDateFormat = PatientDrugEndDateFormat;
        this.strPatientDrugEndDate = PatientDrugEndDate;
        this.strPatientIndicationMedDraVersion = PatientIndicationMedDraVersion;
        this.strPatientDrugIndication = PatientDrugIndication;
        this.strPatientDrgReactionMedDraVersion = PatientDrgReactionMedDraVersion;
        this.strPatientDrugReaction = PatientDrugReaction;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        // No mandatory fields. So, no InvalidTagException.

        if (strPatientDrugName.length() > MAXLEN_PATIENTDRUGNAME) {
            throw new InvalidDataException("Length of PatientDrugName must be less than " + MAXLEN_PATIENTDRUGNAME);
        }
        if (strPatientDrugStartDateFormat.length() > MAXLEN_PATIENTDRUGSTARTDATEFORMAT) {
            throw new InvalidDataException("Length of PatientDrugStartDateFormat must be less than " + MAXLEN_PATIENTDRUGSTARTDATEFORMAT);
        }
        if (strPatientDrugStartDate.length() > MAXLEN_PATIENTDRUGSTARTDATE) {
            throw new InvalidDataException("Length of PatientDrugStartDate must be less than " + MAXLEN_PATIENTDRUGSTARTDATE);
        }
        if (strPatientDrugEndDateFormat.length() > MAXLEN_PATIENTDRUGENDDATEFORMAT) {
            throw new InvalidDataException("Length of PatientDrugEndDateFormat must be less than " + MAXLEN_PATIENTDRUGENDDATEFORMAT);
        }
        if (strPatientDrugEndDate.length() > MAXLEN_PATIENTDRUGENDDATE) {
            throw new InvalidDataException("Length of PatientDrugEndDate must be less than " + MAXLEN_PATIENTDRUGENDDATE);
        }
        if (strPatientIndicationMedDraVersion.length() > MAXLEN_PATIENTINDICATIONMEDDRAVERSION) {
            throw new InvalidDataException("Length of PatientIndicationMedDraVersion must be less than " + MAXLEN_PATIENTINDICATIONMEDDRAVERSION);
        }
        if (strPatientDrugIndication.length() > MAXLEN_PATIENTDRUGINDICATION) {
            throw new InvalidDataException("Length of PatientDrugIndication must be less than " + MAXLEN_PATIENTDRUGINDICATION);
        }
        if (strPatientDrgReactionMedDraVersion.length() > MAXLEN_PATIENTDRGREACTIONMEDDRAVERSION) {
            throw new InvalidDataException("Length of PatientDrgReactionMedDraVersion must be less than " + MAXLEN_PATIENTDRGREACTIONMEDDRAVERSION);
        }
        if (strPatientDrugReaction.length() > MAXLEN_PATIENTDRUGREACTION) {
            throw new InvalidDataException("Length of PatientDrugReaction must be less than " + MAXLEN_PATIENTDRUGREACTION);
        }

        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strPatientDrugName = null;
        this.strPatientDrugStartDateFormat = null;
        this.strPatientDrugStartDate = null;
        this.strPatientDrugEndDateFormat = null;
        this.strPatientDrugEndDate = null;
        this.strPatientIndicationMedDraVersion = null;
        this.strPatientDrugIndication = null;
        this.strPatientDrgReactionMedDraVersion = null;
        this.strPatientDrugReaction = null;
    }
}
