package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class ReportDuplicateTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = -1772911369646036177L;
    private int MAXLEN_DUPLICATESOURCE = 50;
    private int MAXLEN_DUPLICATENUMB = 100;
    ///////////////////////////////////////
    private String strDuplicateSource = "";
    private String strDuplicateNumb = "";

    // Constructors
    /**
     * Default constructor that creates a new instance of ReportDuplicateTag.
     * This constructor initializes its member variables to empty string.
     */
    public ReportDuplicateTag() {
        this.strDuplicateSource = "";
        this.strDuplicateNumb = "";
    }

    /**
     * Constructor that creates a new instance of ReportDuplicateTag.
     *
     * @param DuplicateSource <code><b>String</b></code> value to set &lt;duplicatesource&gt; tag.
     * @param DuplicateNumb   <code><b>String</b></code> value to set &lt;duplicatenumb&gt; tag.
     * @deprecated
     */
    @Deprecated
    public ReportDuplicateTag(
            String DuplicateSource,
            String DuplicateNumb) {
        strDuplicateSource = DuplicateSource;
        strDuplicateNumb = DuplicateNumb;
    }

    /**
     * Generates valid and well formed &lt;reportduplicate&gt; tag. [Ref: ICH ICSR V2.1]
     * @return Properly formed &lt;reportduplicate&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<reportduplicate> \n");
            sb.append("    <duplicatesource>" + strDuplicateSource + "</duplicatesource> \n");
            sb.append("    <duplicatenumb>" + strDuplicateNumb + "</duplicatenumb> \n");
            sb.append("</reportduplicate> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {
        ////
        if (strDuplicateSource.length() > MAXLEN_DUPLICATESOURCE) {
            throw new InvalidDataException("Length of DuplicateSource must be less than " + MAXLEN_DUPLICATESOURCE);
        }
        if (strDuplicateNumb.length() > MAXLEN_DUPLICATENUMB) {
            throw new InvalidDataException("Length of DuplicateNumb must be less than " + MAXLEN_DUPLICATENUMB);
        }
        return true;
    }

    @Override
    public void fetchData() {
        this.strDuplicateSource = "";
        this.strDuplicateNumb = "";
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strDuplicateSource = null;
        this.strDuplicateNumb = null;
    }
}
