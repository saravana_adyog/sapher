package com.f3.edx.ich_icsr_v2_1.e2bm.tag;

import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException;
import com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException;

/**
 *
 * @author Anoop Varma
 */
public class ActiveSubstanceTag implements Tagable {

    /** Serial version UID */
    private static final long serialVersionUID = 5938684982760272265L;
    private final int MAXLEN_ACTIVESUBSTANCENAME = 3;
    ///////////////////////////////////////////
    private String strActiveSubstanceName = "";     // 0 or more occurance

    /**
     * Generates valid and well formed &lt;activesubstance&gt; tag. [ICH ICSR V2.1]
     * @return Properly formed &lt;activesubstance&gt; tag as <code><b>StringBuffer</b></code>.
     * @throws java.lang.Exception Raised when there is any issue with tag(s)/sub tag(s). [Ref: ICH ICSR V2.1]
     */
    @Override
    public StringBuffer getTag() throws InvalidDataException, InvalidTagException {
        try {
            validateTag();

            StringBuffer sb = new StringBuffer();
            sb.append("<activesubstance> \n");
            sb.append("    <activesubstancename>" + strActiveSubstanceName + "</activesubstancename> \n");
            sb.append("</activesubstance> \n");
            return sb;
        } catch (InvalidTagException ite) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ite.printStackTrace();
            }
            throw ite;
        } catch (InvalidDataException ide) {
            if (bl.sapher.general.GenConf.isDebugMode()) {
                ide.printStackTrace();
            }
            throw ide;
        }
    }

    @Override
    public void fetchData() {
    }

    // Constructors
    /**
     * Default constructor that creates a new instance of ActiveSubstanceTag.
     * This constructor initializes its member variables to empty string.
     */
    public ActiveSubstanceTag() {
        this.strActiveSubstanceName = "";
    }

    /**
     * 
     * @param ActiveSubstanceNameas <code><b>String</b></code> value to set &lt;activesubstancenameas&gt; tag.
     * @deprecated 
     */
    @Deprecated
    public ActiveSubstanceTag(String ActiveSubstanceName) {
        this.strActiveSubstanceName = ActiveSubstanceName;
    }

    /**
     * Function to check whether the tag and it's sub tags are valid and well formed as per the <b>ICH ICSR Specification v2.3</b>
     * 
     * @return <code><b>true</b></code> if the tag is valid and well formed.
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidTagException Raised when any mandatory tag(s)/sub tag(s) are missing. [Ref: ICH ICSR V2.1]
     * @throws com.f3.edx.ich_icsr_v2_1.e2bm.exception.InvalidDataException Raised when there is any issue with tag data. [Ref: ICH ICSR V2.1]
     */
    @Override
    public boolean validateTag() throws InvalidTagException, InvalidDataException {

        if (strActiveSubstanceName.length() > MAXLEN_ACTIVESUBSTANCENAME) {
            throw new InvalidDataException("Length of ActiveSubstanceName must be less than " + MAXLEN_ACTIVESUBSTANCENAME);
        }
        return true;
    }

    /**
     * finalize method for this class.
     * @throws java.lang.Throwable Raised if any error occurs.
     */
    @Override
    protected void finalize() throws Throwable {
        this.strActiveSubstanceName = null;
    }
}
