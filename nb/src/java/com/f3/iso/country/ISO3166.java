package com.f3.iso.country;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Utility singleton class to handle the <b>ISO3166</b> standard for countries and their codes.
 * @author Anoop Varma
 */
public class ISO3166 implements Serializable {

    /** */
    private static final long serialVersionUID = -6712756643884706820L;
    /** ArrayList that holds the list of <code><b>Country</b></code> objects */
    private ArrayList<Country> alCountry;
    /** private static <code><b>ISO3166</b></code> object to make this class singleton. */
    private static ISO3166 me = null;

    /**
     * This function returns the instance of <code><b>ISO3166</b></code>.
     * @return Instance of <code><b>ISO3166</b></code>
     */
    public static ISO3166 getInstance() {
        if (me == null) {
            me = new ISO3166();
        }
        return me;
    }

    /**
     * Default constructor that creates an instance of <code><b>ISO3166</b></code>.
     * <p>The ArrayList will be initialized with all countries and their codes.</p>
     */
    public ISO3166() {
        alCountry = new ArrayList<Country>();

        alCountry.add(new Country("AFGHANISTAN", "AF"));
        alCountry.add(new Country("ALAND ISLANDS", "AX"));
        alCountry.add(new Country("ALBANIA", "AL"));
        alCountry.add(new Country("ALGERIA", "DZ"));
        alCountry.add(new Country("AMERICAN SAMOA", "AS"));
        alCountry.add(new Country("ANDORRA", "AD"));
        alCountry.add(new Country("ANGOLA", "AO"));
        alCountry.add(new Country("ANGUILLA", "AI"));
        alCountry.add(new Country("ANTARCTICA", "AQ"));
        alCountry.add(new Country("ANTIGUA AND BARBUDA", "AG"));
        alCountry.add(new Country("ARGENTINA", "AR"));
        alCountry.add(new Country("ARMENIA", "AM"));
        alCountry.add(new Country("ARUBA", "AW"));
        alCountry.add(new Country("AUSTRALIA", "AU"));
        alCountry.add(new Country("AUSTRIA", "AT"));
        alCountry.add(new Country("AZERBAIJAN", "AZ"));
        alCountry.add(new Country("BAHAMAS", "BS"));
        alCountry.add(new Country("BAHRAIN", "BH"));
        alCountry.add(new Country("BANGLADESH", "BD"));
        alCountry.add(new Country("BARBADOS", "BB"));
        alCountry.add(new Country("BELARUS", "BY"));
        alCountry.add(new Country("BELGIUM", "BE"));
        alCountry.add(new Country("BELIZE", "BZ"));
        alCountry.add(new Country("BENIN", "BJ"));
        alCountry.add(new Country("BERMUDA", "BM"));
        alCountry.add(new Country("BHUTAN", "BT"));
        alCountry.add(new Country("BOLIVIA", "BO"));
        alCountry.add(new Country("BOSNIA AND HERZEGOVINA", "BA"));
        alCountry.add(new Country("BOTSWANA", "BW"));
        alCountry.add(new Country("BOUVET ISLAND", "BV"));
        alCountry.add(new Country("BRAZIL", "BR"));
        alCountry.add(new Country("BRITISH INDIAN OCEAN TERRITORY", "IO"));
        alCountry.add(new Country("BRUNEI DARUSSALAM", "BN"));
        alCountry.add(new Country("BULGARIA", "BG"));
        alCountry.add(new Country("BURKINA FASO", "BF"));
        alCountry.add(new Country("BURUNDI", "BI"));
        alCountry.add(new Country("CAMBODIA", "KH"));
        alCountry.add(new Country("CAMEROON", "CM"));
        alCountry.add(new Country("CANADA", "CA"));
        alCountry.add(new Country("CAPE VERDE", "CV"));
        alCountry.add(new Country("CAYMAN ISLANDS", "KY"));
        alCountry.add(new Country("CENTRAL AFRICAN REPUBLIC", "CF"));
        alCountry.add(new Country("CHAD", "TD"));
        alCountry.add(new Country("CHILE", "CL"));
        alCountry.add(new Country("CHINA", "CN"));
        alCountry.add(new Country("CHRISTMAS ISLAND", "CX"));
        alCountry.add(new Country("COCOS (KEELING) ISLANDS", "CC"));
        alCountry.add(new Country("COLOMBIA", "CO"));
        alCountry.add(new Country("COMOROS", "KM"));
        alCountry.add(new Country("CONGO", "CG"));
        alCountry.add(new Country("CONGO, THE DEMOCRATIC REPUBLIC OF THE", "CD"));
        alCountry.add(new Country("COOK ISLANDS", "CK"));
        alCountry.add(new Country("COSTA RICA", "CR"));
        alCountry.add(new Country("C�TE D'IVOIRE", "CI"));
        alCountry.add(new Country("CROATIA", "HR"));
        alCountry.add(new Country("CUBA", "CU"));
        alCountry.add(new Country("CYPRUS", "CY"));
        alCountry.add(new Country("CZECH REPUBLIC", "CZ"));
        alCountry.add(new Country("DENMARK", "DK"));
        alCountry.add(new Country("DJIBOUTI", "DJ"));
        alCountry.add(new Country("DOMINICA", "DM"));
        alCountry.add(new Country("DOMINICAN REPUBLIC", "DO"));
        alCountry.add(new Country("ECUADOR", "EC"));
        alCountry.add(new Country("EGYPT", "EG"));
        alCountry.add(new Country("EL SALVADOR", "SV"));
        alCountry.add(new Country("EQUATORIAL GUINEA", "GQ"));
        alCountry.add(new Country("ERITREA", "ER"));
        alCountry.add(new Country("ESTONIA", "EE"));
        alCountry.add(new Country("ETHIOPIA", "ET"));
        alCountry.add(new Country("FALKLAND ISLANDS (MALVINAS)", "FK"));
        alCountry.add(new Country("FAROE ISLANDS", "FO"));
        alCountry.add(new Country("FIJI", "FJ"));
        alCountry.add(new Country("FINLAND", "FI"));
        alCountry.add(new Country("FRANCE", "FR"));
        alCountry.add(new Country("FRENCH GUIANA", "GF"));
        alCountry.add(new Country("FRENCH POLYNESIA", "PF"));
        alCountry.add(new Country("FRENCH SOUTHERN TERRITORIES", "TF"));
        alCountry.add(new Country("GABON", "GA"));
        alCountry.add(new Country("GAMBIA", "GM"));
        alCountry.add(new Country("GEORGIA", "GE"));
        alCountry.add(new Country("GERMANY", "DE"));
        alCountry.add(new Country("GHANA", "GH"));
        alCountry.add(new Country("GIBRALTAR", "GI"));
        alCountry.add(new Country("GREECE", "GR"));
        alCountry.add(new Country("GREENLAND", "GL"));
        alCountry.add(new Country("GRENADA", "GD"));
        alCountry.add(new Country("GUADELOUPE", "GP"));
        alCountry.add(new Country("GUAM", "GU"));
        alCountry.add(new Country("GUATEMALA", "GT"));
        alCountry.add(new Country("GUERNSEY", "GG"));
        alCountry.add(new Country("GUINEA", "GN"));
        alCountry.add(new Country("GUINEA-BISSAU", "GW"));
        alCountry.add(new Country("GUYANA", "GY"));
        alCountry.add(new Country("HAITI", "HT"));
        alCountry.add(new Country("HEARD ISLAND AND MCDONALD ISLANDS", "HM"));
        alCountry.add(new Country("HOLY SEE (VATICAN CITY STATE)", "VA"));
        alCountry.add(new Country("HONDURAS", "HN"));
        alCountry.add(new Country("HONG KONG", "HK"));
        alCountry.add(new Country("HUNGARY", "HU"));
        alCountry.add(new Country("ICELAND", "IS"));
        alCountry.add(new Country("INDIA", "IN"));
        alCountry.add(new Country("INDONESIA", "ID"));
        alCountry.add(new Country("IRAN, ISLAMIC REPUBLIC OF", "IR"));
        alCountry.add(new Country("IRAQ", "IQ"));
        alCountry.add(new Country("IRELAND", "IE"));
        alCountry.add(new Country("ISLE OF MAN", "IM"));
        alCountry.add(new Country("ISRAEL", "IL"));
        alCountry.add(new Country("ITALY", "IT"));
        alCountry.add(new Country("JAMAICA", "JM"));
        alCountry.add(new Country("JAPAN", "JP"));
        alCountry.add(new Country("JERSEY", "JE"));
        alCountry.add(new Country("JORDAN", "JO"));
        alCountry.add(new Country("KAZAKHSTAN", "KZ"));
        alCountry.add(new Country("KENYA", "KE"));
        alCountry.add(new Country("KIRIBATI", "KI"));
        alCountry.add(new Country("KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF", "KP"));
        alCountry.add(new Country("KOREA, REPUBLIC OF", "KR"));
        alCountry.add(new Country("KUWAIT", "KW"));
        alCountry.add(new Country("KYRGYZSTAN", "KG"));
        alCountry.add(new Country("LAO PEOPLE'S DEMOCRATIC REPUBLIC", "LA"));
        alCountry.add(new Country("LATVIA", "LV"));
        alCountry.add(new Country("LEBANON", "LB"));
        alCountry.add(new Country("LESOTHO", "LS"));
        alCountry.add(new Country("LIBERIA", "LR"));
        alCountry.add(new Country("LIBYAN ARAB JAMAHIRIYA", "LY"));
        alCountry.add(new Country("LIECHTENSTEIN", "LI"));
        alCountry.add(new Country("LITHUANIA", "LT"));
        alCountry.add(new Country("LUXEMBOURG", "LU"));
        alCountry.add(new Country("MACAO", "MO"));
        alCountry.add(new Country("MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF", "MK"));
        alCountry.add(new Country("MADAGASCAR", "MG"));
        alCountry.add(new Country("MALAWI", "MW"));
        alCountry.add(new Country("MALAYSIA", "MY"));
        alCountry.add(new Country("MALDIVES", "MV"));
        alCountry.add(new Country("MALI", "ML"));
        alCountry.add(new Country("MALTA", "MT"));
        alCountry.add(new Country("MARSHALL ISLANDS", "MH"));
        alCountry.add(new Country("MARTINIQUE", "MQ"));
        alCountry.add(new Country("MAURITANIA", "MR"));
        alCountry.add(new Country("MAURITIUS", "MU"));
        alCountry.add(new Country("MAYOTTE", "YT"));
        alCountry.add(new Country("MEXICO", "MX"));
        alCountry.add(new Country("MICRONESIA, FEDERATED STATES OF", "FM"));
        alCountry.add(new Country("MOLDOVA", "MD"));
        alCountry.add(new Country("MONACO", "MC"));
        alCountry.add(new Country("MONGOLIA", "MN"));
        alCountry.add(new Country("MONTENEGRO", "ME"));
        alCountry.add(new Country("MONTSERRAT", "MS"));
        alCountry.add(new Country("MOROCCO", "MA"));
        alCountry.add(new Country("MOZAMBIQUE", "MZ"));
        alCountry.add(new Country("MYANMAR", "MM"));
        alCountry.add(new Country("NAMIBIA", "NA"));
        alCountry.add(new Country("NAURU", "NR"));
        alCountry.add(new Country("NEPAL", "NP"));
        alCountry.add(new Country("NETHERLANDS", "NL"));
        alCountry.add(new Country("NETHERLANDS ANTILLES", "AN"));
        alCountry.add(new Country("NEW CALEDONIA", "NC"));
        alCountry.add(new Country("NEW ZEALAND", "NZ"));
        alCountry.add(new Country("NICARAGUA", "NI"));
        alCountry.add(new Country("NIGER", "NE"));
        alCountry.add(new Country("NIGERIA", "NG"));
        alCountry.add(new Country("NIUE", "NU"));
        alCountry.add(new Country("NORFOLK ISLAND", "NF"));
        alCountry.add(new Country("NORTHERN MARIANA ISLANDS", "MP"));
        alCountry.add(new Country("NORWAY", "NO"));
        alCountry.add(new Country("OMAN", "OM"));
        alCountry.add(new Country("PAKISTAN", "PK"));
        alCountry.add(new Country("PALAU", "PW"));
        alCountry.add(new Country("PALESTINIAN TERRITORY, OCCUPIED", "PS"));
        alCountry.add(new Country("PANAMA", "PA"));
        alCountry.add(new Country("PAPUA NEW GUINEA", "PG"));
        alCountry.add(new Country("PARAGUAY", "PY"));
        alCountry.add(new Country("PERU", "PE"));
        alCountry.add(new Country("PHILIPPINES", "PH"));
        alCountry.add(new Country("PITCAIRN", "PN"));
        alCountry.add(new Country("POLAND", "PL"));
        alCountry.add(new Country("PORTUGAL", "PT"));
        alCountry.add(new Country("PUERTO RICO", "PR"));
        alCountry.add(new Country("QATAR", "QA"));
        alCountry.add(new Country("R�UNION", "RE"));
        alCountry.add(new Country("ROMANIA", "RO"));
        alCountry.add(new Country("RUSSIAN FEDERATION", "RU"));
        alCountry.add(new Country("RWANDA", "RW"));
        alCountry.add(new Country("SAINT BARTH�LEMY", "BL"));
        alCountry.add(new Country("SAINT HELENA", "SH"));
        alCountry.add(new Country("SAINT KITTS AND NEVIS", "KN"));
        alCountry.add(new Country("SAINT LUCIA", "LC"));
        alCountry.add(new Country("SAINT MARTIN", "MF"));
        alCountry.add(new Country("SAINT PIERRE AND MIQUELON", "PM"));
        alCountry.add(new Country("SAINT VINCENT AND THE GRENADINES", "VC"));
        alCountry.add(new Country("SAMOA", "WS"));
        alCountry.add(new Country("SAN MARINO", "SM"));
        alCountry.add(new Country("SAO TOME AND PRINCIPE", "ST"));
        alCountry.add(new Country("SAUDI ARABIA", "SA"));
        alCountry.add(new Country("SENEGAL", "SN"));
        alCountry.add(new Country("SERBIA", "RS"));
        alCountry.add(new Country("SEYCHELLES", "SC"));
        alCountry.add(new Country("SIERRA LEONE", "SL"));
        alCountry.add(new Country("SINGAPORE", "SG"));
        alCountry.add(new Country("SLOVAKIA", "SK"));
        alCountry.add(new Country("SLOVENIA", "SI"));
        alCountry.add(new Country("SOLOMON ISLANDS", "SB"));
        alCountry.add(new Country("SOMALIA", "SO"));
        alCountry.add(new Country("SOUTH AFRICA", "ZA"));
        alCountry.add(new Country("SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS", "GS"));
        alCountry.add(new Country("SPAIN", "ES"));
        alCountry.add(new Country("SRI LANKA", "LK"));
        alCountry.add(new Country("SUDAN", "SD"));
        alCountry.add(new Country("SURINAME", "SR"));
        alCountry.add(new Country("SVALBARD AND JAN MAYEN", "SJ"));
        alCountry.add(new Country("SWAZILAND", "SZ"));
        alCountry.add(new Country("SWEDEN", "SE"));
        alCountry.add(new Country("SWITZERLAND", "CH"));
        alCountry.add(new Country("SYRIAN ARAB REPUBLIC", "SY"));
        alCountry.add(new Country("TAIWAN, PROVINCE OF CHINA", "TW"));
        alCountry.add(new Country("TAJIKISTAN", "TJ"));
        alCountry.add(new Country("TANZANIA, UNITED REPUBLIC OF", "TZ"));
        alCountry.add(new Country("THAILAND", "TH"));
        alCountry.add(new Country("TIMOR-LESTE", "TL"));
        alCountry.add(new Country("TOGO", "TG"));
        alCountry.add(new Country("TOKELAU", "TK"));
        alCountry.add(new Country("TONGA", "TO"));
        alCountry.add(new Country("TRINIDAD AND TOBAGO", "TT"));
        alCountry.add(new Country("TUNISIA", "TN"));
        alCountry.add(new Country("TURKEY", "TR"));
        alCountry.add(new Country("TURKMENISTAN", "TM"));
        alCountry.add(new Country("TURKS AND CAICOS ISLANDS", "TC"));
        alCountry.add(new Country("TUVALU", "TV"));
        alCountry.add(new Country("UGANDA", "UG"));
        alCountry.add(new Country("UKRAINE", "UA"));
        alCountry.add(new Country("UNITED ARAB EMIRATES", "AE"));
        alCountry.add(new Country("UNITED KINGDOM", "GB"));
        alCountry.add(new Country("UNITED STATES", "US"));
        alCountry.add(new Country("UNITED STATES MINOR OUTLYING ISLANDS", "UM"));
        alCountry.add(new Country("URUGUAY", "UY"));
        alCountry.add(new Country("UZBEKISTAN", "UZ"));
        alCountry.add(new Country("VANUATU", "VU"));
        alCountry.add(new Country("VATICAN CITY STATE", "VA"));
        alCountry.add(new Country("VENEZUELA", "VE"));
        alCountry.add(new Country("VIET NAM", "VN"));
        alCountry.add(new Country("VIRGIN ISLANDS, BRITISH", "VG"));
        alCountry.add(new Country("VIRGIN ISLANDS, U.S.", "VI"));
        alCountry.add(new Country("WALLIS AND FUTUNA", "WF"));
        alCountry.add(new Country("WESTERN SAHARA", "EH"));
        alCountry.add(new Country("YEMEN", "YE"));
        alCountry.add(new Country("ZAMBIA", "ZM"));
        alCountry.add(new Country("ZIMBABWE", "ZW"));
    }

    /**
     * 
     * @param countryname Country name as <code><b>String</b></code> to get its code.
     * @return Country code of matching entry
     */
    public String getCountryCode(String countryname) {
        Country c = null;
        String str = "";
        for (int i = 0; i < alCountry.size(); i++) {
            c = alCountry.get(i);
            if (c.getName().contains(countryname.toUpperCase())) {
                str = c.getCode();
                break;
            }
        }

        c = null;
        return str;
    }
}

/**
 * Class that represents a country.
 * It has a name and code.
 * This class is used by <code><b>ISO3166</b></code>.
 * 
 * @author Anoop Varma
 */
class Country {

    /** <code><b>String</b></code> variable to hold country name. */
    private String strName;
    /** <code><b>String</b></code> variable to hold country code. */
    private String strCode;

    /**
     * Default constructor that creates an instance of <code><b>Country</b></code>.
     * 
     */
    public Country() {
        strName = "";
        strCode = "";
    }

    public Country(String name, String code) {
        strName = name;
        strCode = code;
    }

    public String getName() {
        return strName;
    }

    public String getCode() {
        return strCode;
    }
}
