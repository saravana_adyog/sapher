spool Sapher_Meddra.log


prompt MEDDRA BROWSER UPDATE
prompt =====================

prompt
prompt Creating temporary table Tmp_Tree_List
prompt ======================================
prompt

CREATE GLOBAL TEMPORARY TABLE Tmp_Tree_List (	
Lvl_No NUMBER(1,0), 
Soc_Code VARCHAR2(12), 
Soc_Desc VARCHAR2(500), 
Hlgt_Code VARCHAR2(12), 
Hlgt_Desc VARCHAR2(500), 
Hlt_Code VARCHAR2(12), 
Hlt_Desc VARCHAR2(500), 
Pt_Code VARCHAR2(12), 
Pt_Desc VARCHAR2(500), 
Llt_Code VARCHAR2(12), 
Llt_Desc VARCHAR2(500), 
Code VARCHAR2(12), 
Dsc VARCHAR2(500)
) ON COMMIT PRESERVE ROWS ;


prompt
prompt Creating table Meddra_Soc
prompt ==========================
prompt

CREATE TABLE Meddra_Soc (	
Body_Sys_Num	VARCHAR2(12) NOT NULL, 
Who_Art_Code	VARCHAR2(100), 
Harts_Code		VARCHAR2(100), 
Costart_Sym		VARCHAR2(100), 
Icd9_Code		VARCHAR2(100), 
Icd9_Cm_Code	VARCHAR2(100), 
Icd10_Code		VARCHAR2(100), 
Jart_Code		VARCHAR2(100)
)
;
ALTER TABLE Meddra_Soc ADD CONSTRAINT Pk_Meddra_Soc PRIMARY KEY (Body_Sys_Num) USING INDEX TABLESPACE &&1
;
ALTER TABLE Meddra_Soc ADD CONSTRAINT Fk_Meddra_Soc_Body_System FOREIGN KEY (Body_Sys_Num) REFERENCES Body_System (Body_Sys_Num)
;

prompt
prompt Creating table MEDDRA_HLGT
prompt ==========================
prompt
create table MEDDRA_HLGT
(
  HLGT_CODE VARCHAR2(12) not null,
  HLGT_DESC VARCHAR2(500) not null,
  WHO_ART_CODE VARCHAR2(100),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  JART_CODE VARCHAR2(100)
)
;
alter table MEDDRA_HLGT
  add constraint PK_MEDDRA_HLGT primary key (HLGT_CODE) using index tablespace &&1;
alter table MEDDRA_HLGT
  add constraint UK_MEDDRA_HLGT unique (HLGT_DESC) using index tablespace &&1;

prompt
prompt Creating table MEDDRA_SOC_HLGT
prompt ==============================
prompt
create table MEDDRA_SOC_HLGT
(
  BODY_SYS_NUM VARCHAR2(12) not null,
  HLGT_CODE VARCHAR2(12) not null
)
;
alter table MEDDRA_SOC_HLGT
  add constraint PK_MEDDRA_SOC_HLGT primary key (BODY_SYS_NUM, HLGT_CODE) using index tablespace &&1;
alter table MEDDRA_SOC_HLGT
  add constraint FK_MEDDRA_SOC_HLGT_BODY_SYSTEM foreign key (BODY_SYS_NUM) 
  references BODY_SYSTEM (BODY_SYS_NUM);
alter table MEDDRA_SOC_HLGT
  add constraint FK_MEDDRA_SOC_HLGT_MEDDRA_HLGT foreign key (HLGT_CODE) 
  references MEDDRA_HLGT (HLGT_CODE);

prompt
prompt Creating table MEDDRA_HLT
prompt =========================
prompt
create table MEDDRA_HLT
(
  HLT_CODE VARCHAR2(12) not null,
  HLT_DESC VARCHAR2(500) not null,
  WHO_ART_CODE VARCHAR2(100),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  JART_CODE VARCHAR2(100)
)
;
alter table MEDDRA_HLT
  add constraint PK_MEDDRA_HLT primary key (HLT_CODE) using index tablespace &&1;
alter table MEDDRA_HLT
  add constraint UK_MEDDRA_HLT unique (HLT_DESC) using index tablespace &&1;

prompt
prompt Creating table MEDDRA_HLGT_HLT
prompt ==============================
prompt
create table MEDDRA_HLGT_HLT
(
  HLGT_CODE VARCHAR2(12) not null,
  HLT_CODE VARCHAR2(12) not null
)
;
alter table MEDDRA_HLGT_HLT
  add constraint PK_MEDDRA_HLGT_HLT primary key (HLGT_CODE, HLT_CODE) using index tablespace &&1;
alter table MEDDRA_HLGT_HLT
  add constraint FK_MEDDRA_HLGT_HLT_MEDDRA_HLGT foreign key (HLGT_CODE) 
  references MEDDRA_HLGT (HLGT_CODE);
alter table MEDDRA_HLGT_HLT
  add constraint FK_MEDDRA_HLGT_HLT_MEDDRA_HLT foreign key (HLT_CODE) 
  references MEDDRA_HLT (HLT_CODE);

prompt
prompt Creating table Meddra_Diag
prompt ==========================
prompt

CREATE TABLE Meddra_Diag (	
Diag_Code		VARCHAR2(12) NOT NULL, 
Primary_Soc		VARCHAR2(12), 
Who_Art_Code	VARCHAR2(100), 
Harts_Code		VARCHAR2(100), 
Costart_Sym		VARCHAR2(100), 
Icd9_Code		VARCHAR2(100), 
Icd9_Cm_Code	VARCHAR2(100), 
Icd10_Code		VARCHAR2(100), 
Jart_Code		VARCHAR2(100)
)
;
ALTER TABLE Meddra_Diag ADD CONSTRAINT Pk_Meddra_Diag PRIMARY KEY (Diag_Code) USING INDEX TABLESPACE &&1
;
ALTER TABLE Meddra_Diag ADD CONSTRAINT Fk_Meddra_Diag_Body_System FOREIGN KEY (Primary_Soc) REFERENCES Body_System (Body_Sys_Num)
;

prompt
prompt Creating table MEDDRA_HLT_PT
prompt ============================
prompt
create table MEDDRA_HLT_PT
(
  HLT_CODE VARCHAR2(12) not null,
  DIAG_CODE VARCHAR2(12) not null
)
;
alter table MEDDRA_HLT_PT
  add constraint PK_MEDDRA_HLT_PT primary key (HLT_CODE, DIAG_CODE) using index tablespace &&1;
alter table MEDDRA_HLT_PT
  add constraint FK_MEDDRA_HLT_PT_MEDDRA_HLT foreign key (HLT_CODE) 
  references MEDDRA_HLT (HLT_CODE);
alter table MEDDRA_HLT_PT
  add constraint FK_MEDDRA_HLT_PT_AE_DIAG foreign key (DIAG_CODE) 
  references AE_DIAG (DIAG_CODE);

prompt
prompt Creating table Meddra_Llt 
prompt ==========================
prompt

CREATE TABLE Meddra_Llt (	
Llt_Code		VARCHAR2(12) NOT NULL, 
Diag_Code		VARCHAR2(12),
Who_Art_Code	VARCHAR2(100), 
Harts_Code		VARCHAR2(100), 
Costart_Sym		VARCHAR2(100), 
Icd9_Code		VARCHAR2(100), 
Icd9_Cm_Code	VARCHAR2(100), 
Icd10_Code		VARCHAR2(100), 
Jart_Code		VARCHAR2(100)
)
;
ALTER TABLE Meddra_Llt ADD CONSTRAINT Pk_Meddra_Llt PRIMARY KEY (Llt_Code) USING INDEX TABLESPACE &&1
;
ALTER TABLE Meddra_Llt ADD CONSTRAINT Fk_Meddra_Llt_Ae_Llt FOREIGN KEY (Llt_Code) REFERENCES Ae_Llt (Llt_Code)
;
ALTER TABLE Meddra_Llt ADD CONSTRAINT Fk_Meddra_Llt_Ae_Diag FOREIGN KEY (Diag_Code) REFERENCES Ae_Diag (Diag_Code)
;

/*	MEDDRA_PKG	*/

create or replace PACKAGE            "MEDDRA_PKG" IS
PROCEDURE MedDRA_Update;
PROCEDURE Get_Meddra_Listing(
	p_level_no		IN INT,
	p_code			IN MEDDRA_HLGT.Hlgt_Code%TYPE,
	p_soc			IN MEDDRA_HLGT.Hlgt_Code%TYPE,
    cur_MListing	OUT SYS_REFCURSOR);
PROCEDURE Get_Meddra_Soc(
	p_soc_code		IN BODY_SYSTEM.Body_Sys_Num%TYPE,
	cur_soc			OUT SYS_REFCURSOR);
PROCEDURE Get_Meddra_Hlgt(
	p_hlgt_code		IN MEDDRA_HLGT.Hlgt_Code%TYPE,
	cur_hlgt			OUT SYS_REFCURSOR);
PROCEDURE Get_Meddra_Hlt(
	p_hlt_code		IN MEDDRA_HLT.Hlt_Code%TYPE,
	cur_hlt			OUT SYS_REFCURSOR);
PROCEDURE Get_Meddra_Pt(
	p_pt_code		IN Ae_Diag.Diag_Code%TYPE,
	cur_pt			OUT SYS_REFCURSOR);
PROCEDURE Get_Meddra_Llt(
	p_llt_code		IN Ae_Llt.Llt_Code%TYPE,
	cur_llt			OUT SYS_REFCURSOR);
PROCEDURE Meddra_Search_List(
	p_lvl_no		IN VARCHAR2,
	p_desc			IN MEDDRA_HLGT.Hlgt_Desc%TYPE,
	cur_listing		OUT SYS_REFCURSOR);
END MEDDRA_PKG;
/

create or replace PACKAGE BODY            "MEDDRA_PKG" IS
--------------------------------------------
PROCEDURE MedDRA_Update
 IS
BEGIN

	execute immediate 'alter table MEDDRA_DIAG disable constraint Fk_Meddra_Diag_Body_System';
	execute immediate 'alter table MEDDRA_LLT disable constraint PK_MEDDRA_LLT';
	execute immediate 'alter table MEDDRA_LLT disable constraint FK_MEDDRA_LLT_AE_LLT';
	execute immediate 'alter table MEDDRA_LLT disable constraint FK_MEDDRA_LLT_AE_DIAG';
	execute immediate 'alter table ADVERSE_EVENT disable constraint FK_AE_LLT';
	execute immediate 'alter table AE_LLT disable constraint PK_AE_LLT';
	execute immediate 'alter table AE_LLT disable constraint UK_AE_LLT';
	execute immediate 'alter table ADVERSE_EVENT disable constraint FK_AE_DIAG';
	execute immediate 'alter table MEDDRA_HLT_PT disable constraint FK_MEDDRA_HLT_PT_AE_DIAG';
	execute immediate 'alter table MEDDRA_DIAG disable constraint PK_MEDDRA_DIAG';
	execute immediate 'alter table AE_DIAG disable constraint PK_AE_DIAG';
	execute immediate 'alter table AE_DIAG disable constraint UK_AE_DIAG';
	execute immediate 'alter table MEDDRA_HLGT_HLT disable constraint PK_MEDDRA_HLGT_HLT';
	execute immediate 'alter table MEDDRA_HLT_PT disable constraint PK_MEDDRA_HLT_PT';
	execute immediate 'alter table MEDDRA_HLT_PT disable constraint FK_MEDDRA_HLT_PT_MEDDRA_HLT';
	execute immediate 'alter table MEDDRA_HLGT_HLT disable constraint FK_MEDDRA_HLGT_HLT_MEDDRA_HLGT';
	execute immediate 'alter table MEDDRA_HLGT_HLT disable constraint FK_MEDDRA_HLGT_HLT_MEDDRA_HLT';
	execute immediate 'alter table MEDDRA_HLT disable constraint PK_MEDDRA_HLT';
	execute immediate 'alter table MEDDRA_HLT disable constraint UK_MEDDRA_HLT';
	execute immediate 'alter table MEDDRA_SOC_HLGT disable constraint FK_MEDDRA_SOC_HLGT_BODY_SYSTEM';
	execute immediate 'alter table MEDDRA_SOC_HLGT disable constraint FK_MEDDRA_SOC_HLGT_MEDDRA_HLGT';
  execute immediate 'alter table MEDDRA_HLGT disable constraint PK_MEDDRA_HLGT';
	execute immediate 'alter table MEDDRA_HLGT disable constraint UK_MEDDRA_HLGT';
	execute immediate 'alter table MEDDRA_SOC_HLGT disable constraint PK_MEDDRA_SOC_HLGT';
  execute immediate 'alter table ADVERSE_EVENT disable constraint FK_AE_BODYSYSTEM';
	execute immediate 'alter table MEDDRA_SOC disable constraint PK_MEDDRA_SOC';
	execute immediate 'alter table MEDDRA_SOC disable constraint FK_MEDDRA_SOC_BODY_SYSTEM';
	execute immediate 'alter table BODY_SYSTEM disable constraint PK_BODY_SYSTEM';
	execute immediate 'alter table BODY_SYSTEM disable constraint UK_BODY_SYSTEM';
		
	DELETE FROM MEDDRA_HLGT_HLT;
	INSERT INTO MEDDRA_HLGT_HLT SELECT * FROM EXT_MEDDRA_HLGT_HLT;
	DELETE FROM MEDDRA_HLT_PT;
	INSERT INTO MEDDRA_HLT_PT SELECT * FROM EXT_MEDDRA_HLT_PT;
	DELETE FROM MEDDRA_SOC_HLGT;
	INSERT INTO MEDDRA_SOC_HLGT SELECT * FROM EXT_MEDDRA_SOC_HLGT;
	DELETE FROM MEDDRA_HLGT;
	INSERT INTO MEDDRA_HLGT SELECT * FROM EXT_MEDDRA_HLGT;
	DELETE FROM MEDDRA_HLT;
	INSERT INTO MEDDRA_HLT SELECT * FROM EXT_MEDDRA_HLT;
	DELETE FROM MEDDRA_LLT WHERE LLT_CODE IN(SELECT LLT_CODE FROM EXT_MEDDRA_LLT);
	UPDATE MEDDRA_LLT SET DIAG_CODE = NULL;
	INSERT INTO MEDDRA_LLT(LLT_CODE,DIAG_CODE,WHO_ART_CODE,HARTS_CODE,COSTART_SYM,ICD9_CODE,ICD9_CM_CODE,ICD10_CODE,JART_CODE)
	SELECT LLT_CODE,PT_CODE,WHO_ART_CODE,HARTS_CODE,COSTART_SYM,ICD9_CODE,ICD9_CM_CODE,ICD10_CODE,JART_CODE FROM EXT_MEDDRA_LLT;
	DELETE FROM AE_LLT WHERE LLT_CODE IN(SELECT LLT_CODE FROM EXT_MEDDRA_LLT);
	UPDATE AE_LLT SET IS_VALID = 0;
	INSERT INTO AE_LLT(LLT_CODE,LLT_DESC,IS_VALID) 
	SELECT LLT_CODE,LLT_DESC,CASE LLT_CURRENCY WHEN 'Y' THEN 1 WHEN 'N' THEN 0 END FROM EXT_MEDDRA_LLT;
	DELETE FROM MEDDRA_DIAG WHERE DIAG_CODE IN(SELECT PT_CODE FROM EXT_MEDDRA_PT);
	UPDATE MEDDRA_DIAG SET PRIMARY_SOC = NULL;
	INSERT INTO MEDDRA_DIAG(DIAG_CODE,PRIMARY_SOC,WHO_ART_CODE,HARTS_CODE,COSTART_SYM,ICD9_CODE,ICD9_CM_CODE,ICD10_CODE,JART_CODE) 
	SELECT PT_CODE,PRIMARY_SOC,WHO_ART_CODE,HARTS_CODE,COSTART_SYM,ICD9_CODE,ICD9_CM_CODE,ICD10_CODE,JART_CODE FROM EXT_MEDDRA_PT;
	DELETE FROM AE_DIAG WHERE DIAG_CODE IN(SELECT PT_CODE FROM EXT_MEDDRA_PT);
	UPDATE AE_DIAG SET IS_VALID = 0;
	INSERT INTO AE_DIAG(DIAG_CODE,DIAG_DESC,IS_VALID)
	SELECT PT_CODE,PT_DESC,1 FROM EXT_MEDDRA_PT;
	DELETE FROM MEDDRA_SOC WHERE BODY_SYS_NUM IN(SELECT SOC_CODE FROM EXT_MEDDRA_SOC);
	INSERT INTO MEDDRA_SOC(BODY_SYS_NUM,WHO_ART_CODE,HARTS_CODE,COSTART_SYM,ICD9_CODE,ICD9_CM_CODE,ICD10_CODE,JART_CODE)
	SELECT SOC_CODE,WHO_ART_CODE,HARTS_CODE,COSTART_SYM,ICD9_CODE,ICD9_CM_CODE,ICD10_CODE,JART_CODE FROM EXT_MEDDRA_SOC;
	DELETE FROM BODY_SYSTEM WHERE BODY_SYS_NUM IN(SELECT SOC_CODE FROM EXT_MEDDRA_SOC);
	UPDATE BODY_SYSTEM SET IS_VALID = 0;
	INSERT INTO BODY_SYSTEM(BODY_SYS_NUM,BODY_SYSTEM,IS_VALID)
	SELECT SOC_CODE,SOC_DESC,1 FROM EXT_MEDDRA_SOC;

	execute immediate 'alter table BODY_SYSTEM enable constraint PK_BODY_SYSTEM';
	execute immediate 'alter table BODY_SYSTEM enable constraint UK_BODY_SYSTEM';
	execute immediate 'alter table MEDDRA_SOC enable constraint PK_MEDDRA_SOC';
	execute immediate 'alter table MEDDRA_SOC enable constraint FK_MEDDRA_SOC_BODY_SYSTEM';
	execute immediate 'alter table MEDDRA_HLGT enable constraint PK_MEDDRA_HLGT';
	execute immediate 'alter table MEDDRA_HLGT enable constraint UK_MEDDRA_HLGT';
	execute immediate 'alter table MEDDRA_HLT enable constraint PK_MEDDRA_HLT';
	execute immediate 'alter table MEDDRA_HLT enable constraint UK_MEDDRA_HLT';
	execute immediate 'alter table AE_LLT enable constraint PK_AE_LLT';
	execute immediate 'alter table AE_LLT enable constraint UK_AE_LLT';
	execute immediate 'alter table MEDDRA_LLT enable constraint PK_MEDDRA_LLT';
	execute immediate 'alter table MEDDRA_LLT enable constraint FK_MEDDRA_LLT_AE_LLT';
	execute immediate 'alter table MEDDRA_LLT enable constraint FK_MEDDRA_LLT_AE_DIAG';
	execute immediate 'alter table AE_DIAG enable constraint PK_AE_DIAG';
	execute immediate 'alter table AE_DIAG enable constraint UK_AE_DIAG';
	execute immediate 'alter table MEDDRA_DIAG enable constraint PK_MEDDRA_DIAG';
	execute immediate 'alter table MEDDRA_HLGT_HLT enable constraint PK_MEDDRA_HLGT_HLT';
	execute immediate 'alter table MEDDRA_HLGT_HLT enable constraint PK_MEDDRA_HLGT_HLT';
	execute immediate 'alter table MEDDRA_HLT_PT enable constraint PK_MEDDRA_HLT_PT';
	execute immediate 'alter table MEDDRA_DIAG enable constraint FK_AE_DIAG_BODY_SYSTEM';
END meddra_update;
--------------------------------------------
PROCEDURE Get_Meddra_Listing(
	p_level_no		IN INT,
	p_code			IN MEDDRA_HLGT.HLGT_CODE%TYPE,
	p_soc			IN MEDDRA_HLGT.HLGT_CODE%TYPE,
    cur_MListing	OUT SYS_REFCURSOR)
IS
BEGIN
IF (p_level_no=1)
THEN
	  OPEN cur_MListing FOR
	SELECT Body_Sys_Num,Body_System
	  FROM Body_System;
ELSIF (p_level_no=2)
THEN
	  OPEN cur_MListing FOR
	SELECT mh.Hlgt_Code,mh.Hlgt_Desc
	  FROM Meddra_Hlgt mh,Meddra_Soc_Hlgt msh
	 WHERE mh.Hlgt_Code=msh.Hlgt_Code
	   AND msh.Body_Sys_Num=p_code;
ELSIF (p_level_no=3)
THEN
	  OPEN cur_MListing FOR
	SELECT mh.Hlt_Code,mh.Hlt_Desc
	  FROM Meddra_Hlt mh,Meddra_Hlgt_Hlt mhh
	 WHERE mh.Hlt_Code=mhh.Hlt_Code
	   AND mhh.Hlgt_Code=p_code;
ELSIF (p_level_no=4)
THEN
	  OPEN cur_MListing FOR
	SELECT ad.Diag_Code,ad.Diag_Desc,
	       (CASE WHEN (SELECT Primary_Soc FROM Meddra_Diag WHERE Diag_Code=ad.Diag_Code)=p_soc THEN 1 ELSE 0 END) "PT"
	  FROM Ae_Diag ad,Meddra_Hlt_Pt mhp
	 WHERE ad.Diag_Code=mhp.Diag_Code
	   AND mhp.Hlt_Code=p_code;
ELSIF (p_level_no=5)
THEN
	  OPEN cur_MListing FOR
	SELECT al.Llt_Code,al.Llt_Desc,al.Is_Valid
	  FROM Ae_Llt al,Ae_Diag ad,Meddra_Llt ml
	 WHERE al.Llt_Code=ml.Llt_Code
	   AND ad.Diag_Code=ml.Diag_Code
	   AND ad.Diag_Code=p_code;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Meddra_Soc(
	p_soc_code		IN BODY_SYSTEM.Body_Sys_Num%TYPE,
	cur_soc			OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN cur_soc FOR
SELECT bs.Body_Sys_Num,bs.Body_System,bs.Is_Valid,ms.Who_Art_Code,ms.Harts_Code,ms.Costart_Sym,ms.Icd9_Code,ms.Icd9_Cm_Code,ms.Icd10_Code,ms.Jart_Code
  FROM Body_System bs, Meddra_Soc ms
 WHERE bs.Body_Sys_Num=ms.Body_Sys_Num
   AND bs.Body_Sys_Num=p_soc_code;
END;
--------------------------------------------
PROCEDURE Get_Meddra_Hlgt(
	p_hlgt_code		IN MEDDRA_HLGT.Hlgt_Code%TYPE,
	cur_hlgt		OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN cur_hlgt FOR
SELECT *
  FROM Meddra_Hlgt
 WHERE Hlgt_Code=p_hlgt_code;
END;
--------------------------------------------
PROCEDURE Get_Meddra_Hlt(
	p_hlt_code		IN MEDDRA_HLT.Hlt_Code%TYPE,
	cur_hlt			OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN cur_hlt FOR
SELECT *
  FROM MEDDRA_HLT
 WHERE Hlt_Code=p_hlt_code;
END;
--------------------------------------------
PROCEDURE Get_Meddra_Pt(
	p_pt_code		IN Ae_Diag.Diag_Code%TYPE,
	cur_pt			OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN cur_pt FOR
SELECT ad.Diag_Code,ad.Diag_Desc,ad.Is_Valid,md.Primary_Soc,md.Who_Art_Code,md.Harts_Code,md.Costart_Sym,md.Icd9_Code,md.Icd9_Cm_Code,md.Icd10_Code,md.Jart_Code
  FROM Ae_Diag ad,Meddra_Diag md
 WHERE ad.Diag_Code=p_pt_code
   AND ad.Diag_Code=md.Diag_Code;
END;
--------------------------------------------
PROCEDURE Get_Meddra_Llt(
	p_llt_code		IN Ae_Llt.Llt_Code%TYPE,
	cur_llt			OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN cur_llt FOR
SELECT al.Llt_Code,al.Llt_Desc,al.Is_Valid,ml.Diag_Code,ml.Who_Art_Code,ml.Harts_Code,ml.Costart_Sym,ml.Icd9_Code,ml.Icd9_Cm_Code,ml.Icd10_Code,ml.Jart_Code
  FROM Ae_Llt al,Meddra_Llt ml
 WHERE al.Llt_Code=p_llt_code
   AND al.Llt_Code=ml.Llt_Code;
END;
--------------------------------------------
PROCEDURE Meddra_Search_List(
	p_lvl_no		IN VARCHAR2,
	p_desc			IN MEDDRA_HLGT.Hlgt_Desc%TYPE,
	cur_listing		OUT SYS_REFCURSOR)
IS

CURSOR cur_tmp_tree_list IS 
SELECT Lvl_No,Soc_Code,Soc_Desc,Hlgt_Code,Hlgt_Desc,Hlt_Code,Hlt_Desc,Pt_Code,Pt_Desc,Llt_Code,Llt_Desc,Code,Dsc
  FROM Tmp_Tree_List;
  
BEGIN

DELETE FROM Tmp_Tree_List;

IF SUBSTR(p_lvl_no,1,1)='1'
THEN
	INSERT INTO Tmp_Tree_List (Lvl_No,Soc_Code,Soc_Desc,Code,Dsc)
	SELECT 1,Body_Sys_Num,Body_System,Body_Sys_Num,Body_System
	  FROM Body_System
	 WHERE UPPER(Body_System) LIKE UPPER('%'||p_desc||'%');
END IF;

IF SUBSTR(p_lvl_no,2,1)='1'
THEN
	INSERT INTO Tmp_Tree_List (Lvl_No,Hlgt_Code,Hlgt_Desc,Code,Dsc)
	SELECT 2,Hlgt_Code,Hlgt_Desc,Hlgt_Code,Hlgt_Desc
	  FROM Meddra_Hlgt
	 WHERE UPPER(Hlgt_Desc) LIKE UPPER('%'||p_desc||'%');
END IF;

IF SUBSTR(p_lvl_no,3,1)='1'
THEN
	INSERT INTO Tmp_Tree_List (Lvl_No,Hlt_Code,Hlt_Desc,Code,Dsc)
	SELECT 3,Hlt_Code,Hlt_Desc,Hlt_Code,Hlt_Desc
	  FROM Meddra_Hlt
	 WHERE UPPER(Hlt_Desc) LIKE UPPER('%'||p_desc||'%');
END IF;

IF SUBSTR(p_lvl_no,4,1)='1'
THEN
	INSERT INTO Tmp_Tree_List (Lvl_No,Pt_Code,Pt_Desc,Code,Dsc)
	SELECT 4,Diag_Code,Diag_Desc,Diag_Code,Diag_Desc
	  FROM Ae_Diag
	 WHERE UPPER(Diag_Desc) LIKE UPPER('%'||p_desc||'%');
END IF;

IF SUBSTR(p_lvl_no,5,1)='1'
THEN
	INSERT INTO Tmp_Tree_List (Lvl_No,Llt_Code,Llt_Desc,Code,Dsc)
	SELECT 5,Llt_Code,Llt_Desc,Llt_Code,Llt_Desc
	  FROM Ae_Llt
	 WHERE UPPER(Llt_Desc) LIKE UPPER('%'||p_desc||'%');
END IF;

FOR v1 IN cur_tmp_tree_list
LOOP

	IF v1.Lvl_No=2
	THEN
		INSERT INTO Tmp_Tree_List (Lvl_No,Soc_Code,Soc_Desc,Code,Dsc)
		SELECT 2,msh.Body_Sys_Num,
		       (SELECT Body_System FROM Body_System WHERE Body_Sys_Num=msh.Body_Sys_Num),
		       v1.Code,v1.Dsc
		  FROM Meddra_Soc_Hlgt msh
		 WHERE msh.Hlgt_Code=v1.Code;
		 
		DELETE FROM Tmp_Tree_List
		 WHERE Code=v1.Code
		   AND Soc_Code IS NULL;
		   
	END IF;

	IF v1.Lvl_No=3
	THEN
		
		INSERT INTO Tmp_Tree_List (Lvl_No,Soc_Code,Soc_Desc,Hlgt_Code,Hlgt_Desc,Code,Dsc)
		SELECT 3,msh.Body_Sys_Num,
		       (SELECT Body_System FROM Body_System WHERE Body_Sys_Num=msh.Body_Sys_Num),
		       mhh.Hlgt_Code,
		       (SELECT Hlgt_Desc FROM Meddra_Hlgt WHERE Hlgt_Code=mhh.Hlgt_Code),
		       v1.Code,v1.Dsc
		  FROM Meddra_Soc_Hlgt msh, Meddra_Hlgt_Hlt mhh
		 WHERE msh.Hlgt_Code=mhh.Hlgt_Code
		   AND mhh.Hlt_Code=v1.Code;
		   
		DELETE FROM Tmp_Tree_List
		 WHERE Code=v1.Code
		   AND Soc_Code IS NULL
		   AND Hlgt_Code IS NULL;
	
	 END IF;
	
	IF v1.Lvl_No=4
	THEN
		
		INSERT INTO Tmp_Tree_List (Lvl_No,Soc_Code,Soc_Desc,Hlgt_Code,Hlgt_Desc,Hlt_Code,Hlt_Desc,Code,Dsc)
		SELECT 4,msh.Body_Sys_Num,
		       (SELECT Body_System FROM Body_System WHERE Body_Sys_Num=msh.Body_Sys_Num),
		       mhh.Hlgt_Code,
		       (SELECT Hlgt_Desc FROM Meddra_Hlgt WHERE Hlgt_Code=mhh.Hlgt_Code),
		       mhp.Hlt_Code,
		       (SELECT Hlt_Desc FROM Meddra_Hlt WHERE Hlt_Code=mhp.Hlt_Code),
		       v1.Code,v1.Dsc
		  FROM Meddra_Soc_Hlgt msh, Meddra_Hlgt_Hlt mhh,Meddra_Hlt_Pt mhp
		 WHERE msh.Hlgt_Code=mhh.Hlgt_Code
		   AND mhh.Hlt_Code=mhp.Hlt_Code
		   AND mhp.Diag_Code=v1.Code;
		   
		DELETE FROM Tmp_Tree_List
		 WHERE Code=v1.Code
		   AND Soc_Code IS NULL
		   AND Hlgt_Code IS NULL
		   AND Hlt_Code IS NULL;
	
	 END IF;
	 
	IF v1.Lvl_No=5
	THEN
		
		INSERT INTO Tmp_Tree_List (Lvl_No,Soc_Code,Soc_Desc,Hlgt_Code,Hlgt_Desc,Hlt_Code,Hlt_Desc,Pt_Code,Pt_Desc,Code,Dsc)
		SELECT 5,msh.Body_Sys_Num,
		       (SELECT Body_System FROM Body_System WHERE Body_Sys_Num=msh.Body_Sys_Num),
		       mhh.Hlgt_Code,
		       (SELECT Hlgt_Desc FROM Meddra_Hlgt WHERE Hlgt_Code=mhh.Hlgt_Code),
		       mhp.Hlt_Code,
		       (SELECT Hlt_Desc FROM Meddra_Hlt WHERE Hlt_Code=mhp.Hlt_Code),
		       ad.Diag_Code,
		       (SELECT Diag_Desc FROM Ae_Diag WHERE Diag_Code=ad.Diag_Code),
		       v1.Code,v1.Dsc
		  FROM Meddra_Soc_Hlgt msh, Meddra_Hlgt_Hlt mhh,Meddra_Hlt_Pt mhp,Ae_Diag ad,Ae_Llt al
		 WHERE msh.Hlgt_Code=mhh.Hlgt_Code
		   AND mhh.Hlt_Code=mhp.Hlt_Code
		   AND mhp.Diag_Code=ad.Diag_Code
		   AND ad.Diag_Code=(SELECT Diag_Code FROM Meddra_Llt WHERE Llt_Code=al.Llt_Code)
		   AND al.Llt_Code=v1.code;
		   
		DELETE FROM Tmp_Tree_List
		 WHERE Code=v1.Code
		   AND Soc_Code IS NULL
		   AND Hlgt_Code IS NULL
		   AND Hlt_Code IS NULL
		   AND Pt_Code IS NULL;
	
	 END IF;
	 
END LOOP;

  OPEN cur_listing FOR
SELECT *
  FROM Tmp_Tree_List;
  
COMMIT;
END;
--------------------------------------------
END MEDDRA_PKG;
/


exit

spool off
