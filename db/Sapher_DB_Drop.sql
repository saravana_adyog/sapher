--------------------------------------------
-- DB Create scripts for user SAPHER      --
-- Created by Arun P. Jose on 05-MAR-2008 --
--------------------------------------------

set echo on
set verify on
set serveroutput on size 100000

spool Sapher_DB_Drop.log
alter session set "_oracle_script"=true;
select * from global_name;
sho user;

--------------------------------------------
-- Drop the App user for Sapher           --
--------------------------------------------

drop user &&1 cascade;

drop tablespace &&2 including contents and datafiles;

drop tablespace &&3 including contents and datafiles;

drop tablespace &&4 including contents and datafiles cascade constraints;

drop tablespace &&6 including contents and datafiles;

--------------------------------------------
-- Drop the Admin user for Sapher         --
--------------------------------------------

drop user &&5 cascade;

drop tablespace &&7 including contents and datafiles;


exit

spool off
