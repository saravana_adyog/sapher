-------------------------------------------
-- PLSQL scripts for user SAPHER          --
-- Created by Arun P. Jose on 05-MAR-2008 --
--------------------------------------------

set echo on
set verify on
set serveroutput on size 100000

spool Sapher_DB_PLSQL.log

select * from global_name;
sho user;

prompt
prompt Creating package SAPHER_PKG
prompt ===========================
prompt
create or replace PACKAGE            "SAPHER_PKG" IS
PROCEDURE Delete_AuditTrial(
    p_Chg_No IN AUDIT_TRIAL.Chg_No%TYPE);
PROCEDURE Save_Role_List(
    p_Save_Flag IN CHAR,
    p_Role_Name IN ROLE_LIST.Role_Name%TYPE,
    p_Is_Locked IN ROLE_LIST.Is_Locked%TYPE);
PROCEDURE Delete_Role_List(
    p_Role_Name IN ROLE_LIST.Role_Name%TYPE);
PROCEDURE Update_Role_Permissions(
    p_Inv_Id IN ROLE_PERMISSIONS.Inv_Id%TYPE,
    p_Role_Name IN ROLE_PERMISSIONS.Role_Name%TYPE,
    p_Pview IN ROLE_PERMISSIONS.Pview%TYPE,
    p_Padd IN ROLE_PERMISSIONS.Padd%TYPE,
    p_Pedit IN ROLE_PERMISSIONS.Pedit%TYPE,
    p_Pdelete IN ROLE_PERMISSIONS.Pdelete%TYPE);
PROCEDURE Delete_User_List(
    p_Username IN USER_LIST.Username%TYPE);
PROCEDURE Save_User_List(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Role_Name IN USER_LIST.Role_Name%TYPE,
    p_Password IN USER_LIST.PASSWORD%TYPE,
    p_Pwd_Change_Flag IN USER_LIST.Pwd_Change_Flag%TYPE,
    p_Pwd_Mistake_Count IN USER_LIST.Pwd_Mistake_Count%TYPE,
    p_Is_Locked IN USER_LIST.Is_Locked%TYPE);
PROCEDURE Save_Country(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Country_Code IN COUNTRY.Country_Code%TYPE,
    p_Country_Nm IN COUNTRY.Country_Nm%TYPE,
    p_Is_Valid IN COUNTRY.Is_Valid%TYPE);
PROCEDURE Delete_Country(
    p_Username IN USER_LIST.Username%TYPE,
    p_Country_Code IN COUNTRY.Country_Code%TYPE);
PROCEDURE Delete_LHA(
    p_Username IN USER_LIST.Username%TYPE,
    p_LHA_Code IN LHA.LHA_Code%TYPE);
PROCEDURE Save_LHA(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_LHA_Code IN LHA.LHA_Code%TYPE,
    p_LHA_Desc IN LHA.LHA_Desc%TYPE,
    p_Country_Code IN LHA.Country_Code%TYPE,
    p_Is_Valid IN LHA.Is_Valid%TYPE);
PROCEDURE Delete_Adverse_Event(
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN ADVERSE_EVENT.Rep_Num%TYPE,
    p_Llt_Code IN ADVERSE_EVENT.Llt_Code%TYPE);
PROCEDURE Delete_AE_Diag(
    p_Username IN USER_LIST.Username%TYPE,
    p_Diag_Code IN AE_DIAG.Diag_Code%TYPE);
PROCEDURE Delete_AE_LLT(
    p_Username IN USER_LIST.Username%TYPE,
    p_LLT_Code IN AE_LLT.Llt_Code%TYPE);
PROCEDURE Delete_AE_Outcome(
    p_Username IN USER_LIST.Username%TYPE,
    p_Outcome_Code IN AE_OUTCOME.Outcome_Code%TYPE);
PROCEDURE Delete_Assessment_Causality(
    p_Username IN USER_LIST.Username%TYPE,
    p_Assessment_Code IN ASSESSMENT_CAUSALITY.Assessment_Code%TYPE);
PROCEDURE Delete_Body_System(
    p_Username IN USER_LIST.Username%TYPE,
    p_Body_Sys_Num IN BODY_SYSTEM.Body_Sys_Num%TYPE);
PROCEDURE Delete_Case_Details(
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CASE_DETAILS.Rep_Num%TYPE);
PROCEDURE Delete_Conc_Medications(
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CONC_MEDICATIONS.Rep_Num%TYPE,
    p_Product_Code IN CONC_MEDICATIONS.Product_Code%TYPE);
PROCEDURE Delete_Subm_Company(
    p_Username IN USER_LIST.Username%TYPE,
    p_Company_Nm IN SUBM_COMPANY.Company_Nm%TYPE);
PROCEDURE Delete_Dose_Frequency(
    p_Username IN USER_LIST.Username%TYPE,
    p_DFreq_Code IN DOSE_FREQUENCY.Dfreq_Code%TYPE);
PROCEDURE Delete_Dose_Regimen(
    p_Username IN USER_LIST.Username%TYPE,
    p_DRegimen_Code IN DOSE_REGIMEN.Dregimen_Code%TYPE);
PROCEDURE Delete_Dose_Units(
    p_Username IN USER_LIST.Username%TYPE,
    p_Unit_Code IN DOSE_UNITS.Unit_Code%TYPE);
PROCEDURE Delete_Ethnic_Origin(
    p_Username IN USER_LIST.Username%TYPE,
    p_Ethnic_Code IN ETHNIC_ORIGIN.Ethnic_Code%TYPE);
PROCEDURE Delete_Event_Severity(
    p_Username IN USER_LIST.Username%TYPE,
    p_ESeverity_Code IN EVENT_SEVERITY.Eseverity_Code%TYPE);
PROCEDURE Delete_Formulation(
    p_Username IN USER_LIST.Username%TYPE,
    p_Formulation_Code IN FORMULATION.Formulation_Code%TYPE);
PROCEDURE Delete_Indication_Treatment(
    p_Username IN USER_LIST.Username%TYPE,
    p_Indication_Num IN INDICATION_TREATMENT.Indication_Num%TYPE);
PROCEDURE Delete_Manufacturer(
    p_Username IN USER_LIST.Username%TYPE,
    p_M_Code IN MANUFACTURER.m_Code%TYPE);
PROCEDURE Delete_Product(
    p_Username IN USER_LIST.Username%TYPE,
    p_Product_Code IN PRODUCT.Product_Code%TYPE);
PROCEDURE Delete_Rechallenge_Result(
    p_Username IN USER_LIST.Username%TYPE,
    p_Result_Code IN RECHALLENGE_RESULT.Result_Code%TYPE);
PROCEDURE Delete_Reporter_Stat(
    p_Username IN USER_LIST.Username%TYPE,
    p_Status_Code IN REPORTER_STAT.Status_Code%TYPE);
PROCEDURE Delete_Rep_Type(
    p_Username IN USER_LIST.Username%TYPE,
    p_Type_Code IN REP_TYPE.Type_Code%TYPE);
PROCEDURE Delete_Route(
    p_Username IN USER_LIST.Username%TYPE,
    p_Route_Code IN ROUTE.Route_Code%TYPE);
PROCEDURE Delete_Seriousness(
    p_Username IN USER_LIST.Username%TYPE,
    p_Classification_Code IN CLASSIFICATION_SERIOUSNESS.Classification_Code%TYPE);
PROCEDURE Delete_Sex(
    p_Username IN USER_LIST.Username%TYPE,
    p_Sex_Code IN SEX.Sex_Code%TYPE);
PROCEDURE Delete_Source_Info(
    p_Username IN USER_LIST.Username%TYPE,
    p_Source_Code IN SOURCE_INFO.Source_Code%TYPE);
PROCEDURE Delete_Trial_Num(
    p_Username IN USER_LIST.Username%TYPE,
    p_Trial_Num IN TRIAL_NUM.TRIAL_NUM%TYPE);
PROCEDURE Save_Adverse_Event(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN ADVERSE_EVENT.Rep_Num%TYPE,
  	p_Diag_Code IN ADVERSE_EVENT.Diag_Code%TYPE,
  	p_LLT_Code IN ADVERSE_EVENT.Llt_Code%TYPE,
  	p_Outcome_Code IN ADVERSE_EVENT.Outcome_Code%TYPE,
  	p_Body_Sys_Num IN ADVERSE_EVENT.Body_Sys_Num%TYPE,
  	p_Classification_Code IN ADVERSE_EVENT.Classification_Code%TYPE,
  	p_ESeverity_Code IN ADVERSE_EVENT.Eseverity_Code%TYPE,
    p_Reported_To_LHA IN ADVERSE_EVENT.Reported_To_Lha%TYPE,
  	p_LHA_Code IN ADVERSE_EVENT.Lha_Code%TYPE,
  	p_Report_Date IN ADVERSE_EVENT.Report_Date%TYPE,
    p_Date_Start IN ADVERSE_EVENT.Date_Start%TYPE,
  	p_Date_Stop IN ADVERSE_EVENT.Date_Stop%TYPE,
  	p_Labelling IN ADVERSE_EVENT.Labelling%TYPE,
  	p_Prev_Report_Flg IN ADVERSE_EVENT.Prev_Report_Flg%TYPE,
  	p_Prev_Report IN ADVERSE_EVENT.Prev_Report%TYPE,
  	p_Event_Treatment IN ADVERSE_EVENT.Event_Treatment%TYPE,
  	p_Event_Abate_On_Stop IN ADVERSE_EVENT.Event_Abate_On_Stop%TYPE,
  	p_Aggr_Of_Diag IN ADVERSE_EVENT.Aggr_Of_Diag%TYPE,
  	p_Aggr_Of_LLT IN ADVERSE_EVENT.Aggr_Of_Llt%TYPE);
PROCEDURE Save_AE_Diag(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Diag_Code IN AE_DIAG.Diag_Code%TYPE,
    p_AE_Diag_Desc IN AE_DIAG.Diag_Desc%TYPE,
    p_Is_Valid IN AE_DIAG.Is_Valid%TYPE);
PROCEDURE Save_AE_LLT(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_LLT_Code IN AE_LLT.Llt_Code%TYPE,
    p_LLT_Desc IN AE_LLT.Llt_Desc%TYPE,
    p_Is_Valid IN AE_LLT.Is_Valid%TYPE);
PROCEDURE Save_AE_Outcome(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Outcome_Code IN AE_OUTCOME.Outcome_Code%TYPE,
    p_Outcome IN AE_OUTCOME.Outcome%TYPE,
    p_Is_Valid IN AE_OUTCOME.Is_Valid%TYPE);
PROCEDURE Save_Assessment_Causality(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Assessment_Code IN ASSESSMENT_CAUSALITY.Assessment_Code%TYPE,
    p_Assessment IN ASSESSMENT_CAUSALITY.Assessment%TYPE,
    p_Assessment_Flag IN ASSESSMENT_CAUSALITY.Assessment_Flag%TYPE,
    p_Is_Valid IN ASSESSMENT_CAUSALITY.Is_Valid%TYPE);
PROCEDURE Save_Audit(
    p_Username IN AUDIT_TRIAL.Username%TYPE,
    p_Tgt_Type IN AUDIT_TRIAL.Tgt_Type%TYPE,
    p_Txn_Type IN AUDIT_TRIAL.Txn_Type%TYPE,
    p_Inv_Id IN AUDIT_TRIAL.Inv_Id%TYPE,
    p_PK_Col_Val IN AUDIT_TRIAL.Pk_Col_Value%TYPE,
    p_Inv_Col_Id IN AUDIT_TRIAL.Inv_Col_Id%TYPE,
    p_Old_Val IN AUDIT_TRIAL.Old_Val%TYPE,
    p_New_Val IN AUDIT_TRIAL.New_Val%TYPE);
PROCEDURE Save_Body_System(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Body_Sys_Num IN BODY_SYSTEM.Body_Sys_Num%TYPE,
    p_Body_System IN BODY_SYSTEM.BODY_SYSTEM%TYPE,
    p_Is_Valid IN BODY_SYSTEM.Is_Valid%TYPE);
PROCEDURE Save_Case_Details(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN OUT CASE_DETAILS.Rep_Num%TYPE,
  	p_Type_Code IN CASE_DETAILS.Type_Code%TYPE,
  	p_Trial_Num IN CASE_DETAILS.TRIAL_NUM%TYPE,
  	p_Country_Code IN CASE_DETAILS.Country_Code%TYPE,
  	p_Status_Code IN CASE_DETAILS.Status_Code%TYPE,
  	p_Source_Code IN CASE_DETAILS.Source_Code%TYPE,
  	p_M_Code IN CASE_DETAILS.M_Code%TYPE,
  	p_Formulation_Code IN CASE_DETAILS.Formulation_Code%TYPE,
  	p_Unit_Code IN CASE_DETAILS.Unit_Code%TYPE,
  	p_DRegimen_Code IN CASE_DETAILS.Dregimen_Code%TYPE,
  	p_DFreq_Code IN CASE_DETAILS.Dfreq_Code%TYPE,
  	p_Route_Code IN CASE_DETAILS.Route_Code%TYPE,
  	p_Indication_Num IN CASE_DETAILS.Indication_Num%TYPE,
  	p_Sex_Code IN CASE_DETAILS.Sex_Code%TYPE,
  	p_Ethnic_Code IN CASE_DETAILS.Ethnic_Code%TYPE,
  	p_Outcome_Code IN CASE_DETAILS.Outcome_Code%TYPE,
  	p_Product_Code IN CASE_DETAILS.Product_Code%TYPE,
  	p_Phy_Assessment_Code IN CASE_DETAILS.Phy_Assessment_Code%TYPE,
  	p_Co_Assessment_Code IN CASE_DETAILS.Co_Assessment_Code%TYPE,
  	p_Result_Code IN CASE_DETAILS.Result_Code%TYPE,
  	p_Subm_Company_Nm IN CASE_DETAILS.Subm_Company_Nm%TYPE,
  	p_Subm_LHA_Code IN CASE_DETAILS.Subm_Lha_Code%TYPE,
  	p_Date_Entry IN CASE_DETAILS.Date_Entry%TYPE,
  	p_Recv_By_At_Hq IN CASE_DETAILS.Recv_By_At_Hq%TYPE,
  	p_Date_Recv IN CASE_DETAILS.Date_Recv%TYPE,
  	p_Rep_Num_Local IN CASE_DETAILS.Rep_Num_Local%TYPE,
  	p_Rep_Xref_Num IN CASE_DETAILS.Rep_Xref_Num%TYPE,
  	p_Clin_Rep_Info IN CASE_DETAILS.Clin_Rep_Info%TYPE,
  	p_Comp_Rep_Info IN CASE_DETAILS.Comp_Rep_Info%TYPE,
  	p_Patient_Num IN CASE_DETAILS.Patient_Num%TYPE,
  	p_Hosp_Nm IN CASE_DETAILS.Hosp_Nm%TYPE,
  	p_Batch_Num IN CASE_DETAILS.Batch_Num%TYPE,
  	p_Dose IN CASE_DETAILS.Dose%TYPE,
  	p_Treat_Start IN CASE_DETAILS.Treat_Start%TYPE,
  	p_Treat_Stop IN CASE_DETAILS.Treat_Stop%TYPE,
  	p_Rechallenge IN CASE_DETAILS.Rechallenge%TYPE,
  	p_Authorization_No IN CASE_DETAILS.Authorization_No%TYPE,
  	p_P_Initials IN CASE_DETAILS.p_Initials%TYPE,
  	p_Date_Of_Birth IN CASE_DETAILS.Date_Of_Birth%TYPE,
  	p_Age_Reported IN CASE_DETAILS.Age_Reported%TYPE,
  	p_Weight IN CASE_DETAILS.Weight%TYPE,
  	p_Height IN CASE_DETAILS.Height%TYPE,
  	p_Pregnancy IN CASE_DETAILS.Pregnancy%TYPE,
  	p_Pregnancy_Week IN CASE_DETAILS.Pregnancy_Week%TYPE,
  	p_Short_History IN CASE_DETAILS.Short_History%TYPE,
  	p_Outcome_Rep_Date IN CASE_DETAILS.Outcome_Rep_Date%TYPE,
  	p_Autopsy IN CASE_DETAILS.Autopsy%TYPE,
  	p_Autopsy_Result IN CASE_DETAILS.Autopsy_Result%TYPE,
  	p_Phy_Assessment_Date IN CASE_DETAILS.Phy_Assessment_Date%TYPE,
  	p_Co_Assessment_Date IN CASE_DETAILS.Co_Assessment_Date%TYPE,
  	p_Phy_Assessment_Reason IN CASE_DETAILS.Phy_Assessment_Reason%TYPE,
  	p_Co_Assessment_Reason IN CASE_DETAILS.Co_Assessment_Reason%TYPE,
  	p_Contributing_Factors IN CASE_DETAILS.Contributing_Factors%TYPE,
  	p_Rechallenge_Date IN CASE_DETAILS.Rechallenge_Date%TYPE,
  	p_Rechallenge_Dose IN CASE_DETAILS.Rechallenge_Dose%TYPE,
  	p_Verbatim IN CASE_DETAILS.Verbatim%TYPE,
  	p_SHORT_COMMENT IN CASE_DETAILS.Short_Comment%TYPE,
  	p_Distbn_Date IN CASE_DETAILS.Distbn_Date%TYPE,
  	p_Submission_Date IN CASE_DETAILS.Submission_Date%TYPE,
  	p_Further_Commn IN CASE_DETAILS.Further_Commn%TYPE,
    p_AE_Desc IN CASE_DETAILS.AE_Desc%TYPE,
  	p_Is_Suspended IN CASE_DETAILS.Is_Suspended%TYPE);
PROCEDURE Save_Case_AE_Details(
	p_Username IN USER_LIST.Username%TYPE,
	p_Rep_Num IN OUT CASE_DETAILS.Rep_Num%TYPE, 
	p_Verbatim IN CASE_DETAILS.Verbatim%TYPE, 
	p_AE_Desc IN CASE_DETAILS.AE_Desc%TYPE);
PROCEDURE Save_Case_CM_Details(
	p_Username IN USER_LIST.Username%TYPE,
	p_Rep_Num IN OUT CASE_DETAILS.Rep_Num%TYPE,
	p_Short_History IN CASE_DETAILS.Short_History%TYPE, 
	p_SHORT_COMMENT IN CASE_DETAILS.Short_Comment%TYPE);
PROCEDURE Save_Case_Details_Mgr(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CASE_DETAILS.Rep_Num%TYPE,
  	p_Type_Code IN CASE_DETAILS.Type_Code%TYPE,
  	p_Trial_Num IN CASE_DETAILS.TRIAL_NUM%TYPE,
  	p_Country_Code IN CASE_DETAILS.Country_Code%TYPE,
  	p_Status_Code IN CASE_DETAILS.Status_Code%TYPE,
  	p_Source_Code IN CASE_DETAILS.Source_Code%TYPE,
  	p_M_Code IN CASE_DETAILS.M_Code%TYPE,
  	p_Formulation_Code IN CASE_DETAILS.Formulation_Code%TYPE,
  	p_Unit_Code IN CASE_DETAILS.Unit_Code%TYPE,
  	p_DRegimen_Code IN CASE_DETAILS.Dregimen_Code%TYPE,
  	p_DFreq_Code IN CASE_DETAILS.Dfreq_Code%TYPE,
  	p_Route_Code IN CASE_DETAILS.Route_Code%TYPE,
  	p_Indication_Num IN CASE_DETAILS.Indication_Num%TYPE,
  	p_Sex_Code IN CASE_DETAILS.Sex_Code%TYPE,
  	p_Ethnic_Code IN CASE_DETAILS.Ethnic_Code%TYPE,
  	p_Outcome_Code IN CASE_DETAILS.Outcome_Code%TYPE,
  	p_Product_Code IN CASE_DETAILS.Product_Code%TYPE,
  	p_Phy_Assessment_Code IN CASE_DETAILS.Phy_Assessment_Code%TYPE,
  	p_Co_Assessment_Code IN CASE_DETAILS.Co_Assessment_Code%TYPE,
  	p_Result_Code IN CASE_DETAILS.Result_Code%TYPE,
  	p_Subm_Company_Nm IN CASE_DETAILS.Subm_Company_Nm%TYPE,
  	p_Subm_LHA_Code IN CASE_DETAILS.Subm_Lha_Code%TYPE,
  	p_Date_Entry IN CASE_DETAILS.Date_Entry%TYPE,
  	p_Recv_By_At_Hq IN CASE_DETAILS.Recv_By_At_Hq%TYPE,
  	p_Date_Recv IN CASE_DETAILS.Date_Recv%TYPE,
  	p_Rep_Num_Local IN CASE_DETAILS.Rep_Num_Local%TYPE,
  	p_Rep_Xref_Num IN CASE_DETAILS.Rep_Xref_Num%TYPE,
  	p_Clin_Rep_Info IN CASE_DETAILS.Clin_Rep_Info%TYPE,
  	p_Comp_Rep_Info IN CASE_DETAILS.Comp_Rep_Info%TYPE,
  	p_Patient_Num IN CASE_DETAILS.Patient_Num%TYPE,
  	p_Hosp_Nm IN CASE_DETAILS.Hosp_Nm%TYPE,
  	p_Batch_Num IN CASE_DETAILS.Batch_Num%TYPE,
  	p_Dose IN CASE_DETAILS.Dose%TYPE,
  	p_Treat_Start IN CASE_DETAILS.Treat_Start%TYPE,
  	p_Treat_Stop IN CASE_DETAILS.Treat_Stop%TYPE,
  	p_Rechallenge IN CASE_DETAILS.Rechallenge%TYPE,
  	p_Authorization_No IN CASE_DETAILS.Authorization_No%TYPE,
  	p_P_Initials IN CASE_DETAILS.p_Initials%TYPE,
  	p_Date_Of_Birth IN CASE_DETAILS.Date_Of_Birth%TYPE,
  	p_Age_Reported IN CASE_DETAILS.Age_Reported%TYPE,
  	p_Weight IN CASE_DETAILS.Weight%TYPE,
  	p_Height IN CASE_DETAILS.Height%TYPE,
  	p_Pregnancy IN CASE_DETAILS.Pregnancy%TYPE,
  	p_Pregnancy_Week IN CASE_DETAILS.Pregnancy_Week%TYPE,
  	p_Short_History IN CASE_DETAILS.Short_History%TYPE,
  	p_Outcome_Rep_Date IN CASE_DETAILS.Outcome_Rep_Date%TYPE,
  	p_Autopsy IN CASE_DETAILS.Autopsy%TYPE,
  	p_Autopsy_Result IN CASE_DETAILS.Autopsy_Result%TYPE,
  	p_Phy_Assessment_Date IN CASE_DETAILS.Phy_Assessment_Date%TYPE,
  	p_Co_Assessment_Date IN CASE_DETAILS.Co_Assessment_Date%TYPE,
  	p_Phy_Assessment_Reason IN CASE_DETAILS.Phy_Assessment_Reason%TYPE,
  	p_Co_Assessment_Reason IN CASE_DETAILS.Co_Assessment_Reason%TYPE,
  	p_Contributing_Factors IN CASE_DETAILS.Contributing_Factors%TYPE,
  	p_Rechallenge_Date IN CASE_DETAILS.Rechallenge_Date%TYPE,
  	p_Rechallenge_Dose IN CASE_DETAILS.Rechallenge_Dose%TYPE,
  	p_Verbatim IN CASE_DETAILS.Verbatim%TYPE,
  	p_SHORT_COMMENT IN CASE_DETAILS.Short_Comment%TYPE,
  	p_Distbn_Date IN CASE_DETAILS.Distbn_Date%TYPE,
  	p_Submission_Date IN CASE_DETAILS.Submission_Date%TYPE,
  	p_Further_Commn IN CASE_DETAILS.Further_Commn%TYPE,
    p_AE_Desc IN CASE_DETAILS.AE_Desc%TYPE,
  	p_Is_Suspended IN CASE_DETAILS.Is_Suspended%TYPE);
PROCEDURE Save_Conc_Medications(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CONC_MEDICATIONS.Rep_Num%TYPE,
    p_Product_Code IN CONC_MEDICATIONS.Product_Code%TYPE,
    p_Indication_Num IN CONC_MEDICATIONS.Indication_Num%TYPE,
    p_Treat_Start IN CONC_MEDICATIONS.Treat_Start%TYPE,
    p_Treat_Stop IN CONC_MEDICATIONS.Treat_Stop%TYPE);
PROCEDURE Save_Subm_Company(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Company_Nm IN SUBM_COMPANY.Company_Nm%TYPE,
    p_Country_Code IN SUBM_COMPANY.Country_Code%TYPE,
    p_Is_Valid IN SUBM_COMPANY.Is_Valid%TYPE);
PROCEDURE Save_Dose_Frequency(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_DFreq_Code IN DOSE_FREQUENCY.Dfreq_Code%TYPE,
    p_DFreq_Desc IN DOSE_FREQUENCY.Dfreq_Desc%TYPE,
    p_Is_Valid IN DOSE_FREQUENCY.Is_Valid%TYPE);
PROCEDURE Save_Dose_Regimen(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_DRegimen_Code IN DOSE_REGIMEN.Dregimen_Code%TYPE,
    p_DRegimen_Desc IN DOSE_REGIMEN.Dregimen_Desc%TYPE,
    p_Is_Valid IN DOSE_REGIMEN.Is_Valid%TYPE,
    p_No_Of_Dosage IN DOSE_REGIMEN.No_Of_Dosage%TYPE,
    p_Dosage_Interval IN DOSE_REGIMEN.Dosage_Interval%TYPE);
PROCEDURE Save_Dose_Units(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Unit_Code IN DOSE_UNITS.Unit_Code%TYPE,
    p_Unit_Desc IN DOSE_UNITS.Unit_Desc%TYPE,
    p_Is_Valid IN DOSE_UNITS.Is_Valid%TYPE);
PROCEDURE Save_Event_Severity(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_ESeverity_Code IN EVENT_SEVERITY.Eseverity_Code%TYPE,
    p_ESeverity_Desc IN EVENT_SEVERITY.Eseverity_Desc%TYPE,
    p_Is_Valid IN EVENT_SEVERITY.Is_Valid%TYPE);
PROCEDURE Save_Formulation(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Formulation_Code IN FORMULATION.Formulation_Code%TYPE,
    p_Formulation_Desc IN FORMULATION.Formulation_Desc%TYPE,
    p_Is_Valid IN FORMULATION.Is_Valid%TYPE);
PROCEDURE Save_Indication_Treatment(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Indication_Num IN INDICATION_TREATMENT.Indication_Num%TYPE,
    p_Indication IN INDICATION_TREATMENT.Indication%TYPE,
    p_Is_Valid IN INDICATION_TREATMENT.Is_Valid%TYPE);
PROCEDURE Save_Inventory(
    p_Save_Flag IN CHAR, 
    p_inv_id IN INVENTORY.Inv_Id%TYPE, 
    p_inv_desc IN INVENTORY.Inv_Desc%TYPE,
    p_inv_type IN INVENTORY.Inv_Type%TYPE,
    p_ins_audit_flag IN INVENTORY.Ins_Audit_Flag%TYPE,
    p_del_audit_flag IN INVENTORY.Del_Audit_Flag%TYPE);
PROCEDURE Save_Manufacturer(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_M_Code IN MANUFACTURER.M_Code%TYPE,
    p_M_Name IN MANUFACTURER.M_Name%TYPE,
    p_M_Addr1 IN MANUFACTURER.M_Addr1%TYPE,
    p_M_Addr2 IN MANUFACTURER.M_Addr2%TYPE,
    p_M_Addr3 IN MANUFACTURER.M_Addr3%TYPE,
    p_M_Addr4 IN MANUFACTURER.M_Addr4%TYPE,
    p_M_Phone IN MANUFACTURER.M_Phone%TYPE,
    p_M_Fax IN MANUFACTURER.M_Fax%TYPE,
    p_M_Email IN MANUFACTURER.M_Email%TYPE,
    p_Is_Valid IN MANUFACTURER.Is_Valid%TYPE);
PROCEDURE Save_Product(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Product_Code IN PRODUCT.Product_Code%TYPE,
    p_Generic_Nm IN PRODUCT.Generic_Nm%TYPE,
    p_Brand_Nm IN PRODUCT.Brand_Nm%TYPE,
    p_Is_Valid IN PRODUCT.Is_Valid%TYPE);
PROCEDURE Save_Rechallenge_Result(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Result_Code IN RECHALLENGE_RESULT.Result_Code%TYPE,
    p_Result_Desc IN RECHALLENGE_RESULT.Result_Desc%TYPE,
    p_Is_Valid IN RECHALLENGE_RESULT.Is_Valid%TYPE);
PROCEDURE Save_Reporter_Stat(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Status_Code IN REPORTER_STAT.Status_Code%TYPE,
    p_Status_Desc IN REPORTER_STAT.Status_Desc%TYPE,
    p_Is_Valid IN REPORTER_STAT.Is_Valid%TYPE);
PROCEDURE Save_Rep_Type(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Type_Code IN REP_TYPE.Type_Code%TYPE,
    p_Type_Desc IN REP_TYPE.Type_Desc%TYPE,
    p_Is_Valid IN REP_TYPE.Is_Valid%TYPE);
PROCEDURE Save_Route(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Route_Code IN ROUTE.Route_Code%TYPE,
    p_Route_Desc IN ROUTE.Route_Desc%TYPE,
    p_Is_Valid IN ROUTE.Is_Valid%TYPE);
PROCEDURE Save_Seriousness(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Classification_Code IN CLASSIFICATION_SERIOUSNESS.Classification_Code%TYPE,
    p_Classification_Desc IN CLASSIFICATION_SERIOUSNESS.Classification_Desc%TYPE,
    p_Is_Valid IN CLASSIFICATION_SERIOUSNESS.Is_Valid%TYPE);
PROCEDURE Save_Sex(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Sex_Code IN SEX.Sex_Code%TYPE,
    p_Sex_Desc IN SEX.Sex_Desc%TYPE,
    p_Is_Valid IN SEX.Is_Valid%TYPE);
PROCEDURE Save_Source_Info(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Source_Code IN SOURCE_INFO.Source_Code%TYPE,
    p_Source_Desc IN SOURCE_INFO.Source_Desc%TYPE,
    p_Is_Valid IN SOURCE_INFO.Is_Valid%TYPE);
PROCEDURE Save_Trial_Num(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Trial_Num IN TRIAL_NUM.TRIAL_NUM%TYPE,
    p_Is_Valid IN TRIAL_NUM.Is_Valid%TYPE);
PROCEDURE Save_Ethnic_Origin(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Ethnic_Code IN ETHNIC_ORIGIN.Ethnic_Code%TYPE,
    p_Ethnic_Desc IN ETHNIC_ORIGIN.Ethnic_Desc%TYPE,
    p_Is_Valid IN ETHNIC_ORIGIN.Is_Valid%TYPE);
PROCEDURE Sumtab_Label(
    p_Product    IN CASE_DETAILS.PRODUCT_CODE%TYPE,
    p_Frm_Date   IN VARCHAR2,
    p_To_Date    IN VARCHAR2,
    p_Status     IN Case_Details.is_Suspended%TYPE DEFAULT NULL,
    p_curSumtabL OUT SYS_REFCURSOR);
PROCEDURE Sumtab_RType(
    p_Product    IN CASE_DETAILS.PRODUCT_CODE%TYPE,
    p_Frm_Date   IN VARCHAR2,
    p_To_Date    IN VARCHAR2,
    p_Status     IN Case_Details.is_Suspended%TYPE DEFAULT NULL,
    p_curSumtabR OUT SYS_REFCURSOR);
PROCEDURE Cioms2_Proc (
          p_Product   IN  Case_Details.Product_Code%TYPE DEFAULT NULL,
          p_Frm_Date  IN  VARCHAR2 DEFAULT NULL,
          p_To_Date   IN  VARCHAR2 DEFAULT NULL,
          p_Status    IN  Case_Details.is_Suspended%TYPE DEFAULT NULL,
          p_curCioms2 OUT SYS_REFCURSOR);
PROCEDURE ICH_Proc (
          p_Product   IN Case_Details.Product_Code%TYPE DEFAULT NULL,
          p_Frm_Date  IN VARCHAR2 DEFAULT NULL,
          p_To_Date   IN VARCHAR2 DEFAULT NULL,
          p_Status    IN Case_Details.is_Suspended%TYPE DEFAULT NULL,
          p_curICH   OUT SYS_REFCURSOR);
PROCEDURE Query_Tool_Proc (
    p_Product_Code        IN CASE_DETAILS.PRODUCT_CODE%TYPE,
    p_Status_Code         IN CASE_DETAILS.STATUS_CODE%TYPE,
    p_Country_Code        IN CASE_DETAILS.COUNTRY_CODE%TYPE,
    p_Source_Code         IN CASE_DETAILS.SOURCE_CODE%TYPE,
    p_Classification_Code IN CLASSIFICATION_SERIOUSNESS.CLASSIFICATION_CODE%TYPE,
    p_Unit_Code           IN DOSE_UNITS.UNIT_CODE%TYPE,
    p_Dose                IN CASE_DETAILS.DOSE%TYPE,
    p_Indication_Num      IN INDICATION_TREATMENT.INDICATION_NUM%TYPE,
    p_Llt_Code            IN AE_LLT.LLT_CODE%TYPE,
    p_Outcome_Code        IN AE_OUTCOME.OUTCOME_CODE%TYPE,
    p_Diag_Code           IN AE_DIAG.DIAG_CODE%TYPE,
    p_Co_Assessment_Code  IN ASSESSMENT_CAUSALITY.ASSESSMENT_CODE%TYPE,
    p_Body_Sys_Num        IN BODY_SYSTEM.BODY_SYS_NUM%TYPE,
    p_Sex_Code            IN SEX.SEX_CODE%TYPE,
    p_Phy_Assessment_Code IN ASSESSMENT_CAUSALITY.ASSESSMENT_CODE%TYPE,
    p_M_Code              IN MANUFACTURER.M_CODE%TYPE,
    p_Patient_Num         IN CASE_DETAILS.PATIENT_NUM%TYPE,
    p_Frm_Rep_num         IN CASE_DETAILS.REP_NUM%TYPE,
    p_To_Rep_Num          IN CASE_DETAILS.REP_NUM%TYPE,
    p_Frm_Age             IN NUMBER,
    p_To_Age              IN NUMBER,
    p_Frm_Treat           IN NUMBER,
    p_To_Treat            IN NUMBER,
    p_Frm_Event           IN NUMBER,
    p_To_Event            IN NUMBER,
    p_Frm_Date_Recv       IN VARCHAR2,
    p_to_Date_Recv        IN VARCHAR2,
    p_Status              IN Case_Details.Is_Suspended%TYPE,
    p_CurQTool           OUT SYS_REFCURSOR);
    
PROCEDURE Update_Inv_Col(
          p_Inv_Col_Id   IN INVENTORY_COLS.INV_COL_ID%TYPE,
          p_Audit_Flag   IN INVENTORY_COLS.AUDIT_FLAG%TYPE);
          
PROCEDURE Del_Cases;
PROCEDURE Clear_Db;
PROCEDURE Register_User(
   p_UserCount IN number,
      p_Username  IN User_List.Username%TYPE,
      p_RemoteIp  IN User_Session.Remoteip%TYPE,
      p_SessionId OUT User_Session.Session_Id%TYPE);
PROCEDURE UnRegister_User(
      p_SessionId IN User_Session.Session_Id%TYPE);
PROCEDURE Get_Interval(
    p_Interval_Code  IN Interval.Interval_Code%TYPE,
    p_Interval       IN Interval.Interval%TYPE,
    p_Active         IN Interval.Is_Valid%TYPE,
    cur_Interval    OUT SYS_REFCURSOR);
PROCEDURE Get_User(
    p_Username     IN User_List.Username%TYPE,
    p_RoleName     IN User_List.Role_Name%TYPE,
    p_Is_Locked    IN User_list.Is_Locked%TYPE,
    p_Search_Type  IN Number,
    cur_User       OUT SYS_REFCURSOR);
PROCEDURE Get_User_IgnoreCase(
    p_Username  IN User_List.Username%TYPE,
    p_RoleName  IN User_List.Role_Name%TYPE,
    p_Is_Locked IN User_list.Is_Locked%TYPE,
    cur_User    OUT SYS_REFCURSOR);
PROCEDURE Get_User_Session(
    p_Username  IN User_List.Username%TYPE,
    p_Session   IN User_Session.Session_Id%TYPE,
    cur_User    OUT SYS_REFCURSOR);
PROCEDURE Get_Role(
    p_RoleName  IN Role_List.Role_Name%TYPE,
    p_Is_Locked IN Role_list.Is_Locked%TYPE,
    cur_Role    OUT SYS_REFCURSOR);
PROCEDURE Get_Role_Permissions(
    p_RoleName    IN Role_Permissions.Role_Name%TYPE,
    p_InventoryId IN Role_Permissions.Inv_Id%TYPE,
    p_InvType     IN Inventory.Inv_Type%TYPE,
    cur_Perm      OUT SYS_REFCURSOR);
PROCEDURE Get_Audit_Trial(
    p_ChgNo       IN Audit_Trial.Chg_No%TYPE,
    p_Username    IN Audit_Trial.Username%TYPE,
    p_InventoryId IN Audit_Trial.Inv_Id%TYPE,
    p_Txn_Type    IN Audit_Trial.Txn_Type%TYPE,
    p_Tgt_Type    IN Audit_Trial.Tgt_Type%TYPE,
    p_From        IN VARCHAR2,
    p_To          IN VARCHAR2,
    p_RefVal      IN Audit_Trial.Pk_Col_Value%TYPE,
    cur_Audit     OUT SYS_REFCURSOR);
PROCEDURE Get_Inventory(
    p_InvId    	   IN Inventory.Inv_Id%TYPE,
    p_InvDesc  	   IN Inventory.Inv_Desc%TYPE,
    p_InvType  	   IN Inventory.Inv_Type%TYPE,
    p_Search_Type  IN Number,
    cur_Inv    	   OUT SYS_REFCURSOR);
PROCEDURE Get_Inventory_Cols(
    p_InvId   IN Inventory_Cols.Inv_Id%TYPE,
    p_InvCol  IN Inventory_Cols.Inv_Col_Id%TYPE,
    p_Field   IN Inventory_Cols.Field_Name%TYPE,
    cur_Inv   OUT SYS_REFCURSOR);
PROCEDURE Get_Case(
    p_RepNum       IN Case_Details.Rep_Num%TYPE,
    p_Diag_Code      IN Adverse_Event.Diag_Code%TYPE,
    p_Trial_Num    IN Case_Details.Trial_Num%TYPE,
    p_Prod_Code    IN Case_Details.Product_Code%TYPE,
    p_Frm          IN VARCHAR2,
    p_To           IN VARCHAR2,
    p_Is_Suspended IN Case_Details.Is_Suspended%TYPE,
    cur_Case       OUT SYS_REFCURSOR);
PROCEDURE Get_Country(
    p_Code    	   IN Country.Country_Code%TYPE,
    p_Name    	   IN Country.Country_Nm%TYPE,
    p_Active  	   IN Country.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Ctry       OUT SYS_REFCURSOR);
PROCEDURE Get_Outcome(
    p_Code         IN AE_Outcome.Outcome_Code%TYPE,
    p_Desc         IN AE_Outcome.Outcome%TYPE,
    p_Active       IN AE_Outcome.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Outcome    OUT SYS_REFCURSOR);
PROCEDURE Get_LLT(
    p_Code   	   IN AE_LLT.LLT_CODE%TYPE,
    p_Desc   	   IN AE_LLT.LLT_DESC%TYPE,
    p_Active 	   IN AE_LLT.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_LLT        OUT SYS_REFCURSOR);
PROCEDURE Get_Diag(
    p_Code   	   IN AE_DIAG.DIAG_CODE%TYPE,
    p_Desc   	   IN AE_Diag.Diag_Desc%TYPE,
    p_Active 	   IN Ae_Diag.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Diag 	   OUT SYS_REFCURSOR);
PROCEDURE Get_Assessment(
    p_Code   	   IN Assessment_Causality.Assessment_Code%TYPE,
    p_Desc   	   IN Assessment_Causality.Assessment%TYPE,
    p_Active 	   IN Assessment_Causality.Is_Valid%TYPE,
    p_Flag   	   IN Assessment_Causality.Assessment_Flag%TYPE,
    p_Search_Type  IN Number,
    cur_Asmt 	   OUT SYS_REFCURSOR);
PROCEDURE Get_Seriousness(
    p_Code    	   IN Classification_Seriousness.Classification_Code%TYPE,
    p_Desc    	   IN Classification_Seriousness.Classification_Desc%TYPE,
    p_Active  	   IN Classification_Seriousness.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Clasf 	   OUT SYS_REFCURSOR);
PROCEDURE Get_DFreq(
    p_Code    	   IN Dose_Frequency.Dfreq_Code%TYPE,
    p_Desc    	   IN Dose_Frequency.Dfreq_Desc%TYPE,
    p_Active  	   IN Dose_Frequency.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_DFreq 	   OUT SYS_REFCURSOR);
PROCEDURE Get_DRgmn(
    p_Code    		  IN Dose_Regimen.Dregimen_Code%TYPE,
    p_Desc    		  IN Dose_Regimen.Dregimen_Desc%TYPE,
    p_Active  		  IN Dose_Regimen.Is_Valid%TYPE,
    p_No_Of_Dosage 	  IN Dose_Regimen.No_Of_Dosage%TYPE,
    p_Dosage_Interval IN Dose_Regimen.Dosage_Interval%TYPE,
    p_Search_Type  	  IN Number,
    cur_DRgmn 		  OUT SYS_REFCURSOR);
PROCEDURE Get_DUnit(
    p_Code    	   IN Dose_Units.Unit_Code%TYPE,
    p_Desc    	   IN Dose_Units.Unit_Desc%TYPE,
    p_Active  	   IN Dose_Units.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_DUnit 	   OUT SYS_REFCURSOR);
PROCEDURE Get_EOrigin(
    p_Code    	   IN Ethnic_Origin.Ethnic_Code%TYPE,
    p_Desc    	   IN Ethnic_Origin.Ethnic_Desc%TYPE,
    p_Active  	   IN Ethnic_Origin.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_EOrgn 	   OUT SYS_REFCURSOR);
PROCEDURE Get_ESvty(
    p_Code    	   IN Event_Severity.Eseverity_Code%TYPE,
    p_Desc    	   IN Event_Severity.Eseverity_Desc%TYPE,
    p_Active  	   IN Event_Severity.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_ESvty 	   OUT SYS_REFCURSOR);
PROCEDURE Get_Formulation(
    p_Code    	   IN Event_Severity.Eseverity_Code%TYPE,
    p_Desc    	   IN Event_Severity.Eseverity_Desc%TYPE,
    p_Active  	   IN Event_Severity.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Frmln 	   OUT SYS_REFCURSOR);
PROCEDURE Get_Product(
    p_Code   	   IN Product.Product_Code%TYPE,
    p_GNm    	   IN Product.Generic_Nm%TYPE,
    p_BNm    	   IN Product.Brand_Nm%TYPE,
    p_Active 	   IN Product.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Pdt  	   OUT SYS_REFCURSOR);
PROCEDURE Get_Sex(
    p_Code    	   IN Sex.Sex_Code%TYPE,
    p_Desc    	   IN Sex.Sex_Desc%TYPE,
    p_Active  	   IN Sex.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Sex   	   OUT SYS_REFCURSOR);
PROCEDURE Get_Indication(
    p_Code    	   IN Indication_Treatment.Indication_Num%TYPE,
    p_Desc    	   IN Indication_Treatment.Indication%TYPE,
    p_Active  	   IN Indication_Treatment.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Ind   	   OUT SYS_REFCURSOR);
PROCEDURE Get_Manufacturer(
    p_Code    	   IN Manufacturer.m_Code%TYPE,
    p_Name    	   IN Manufacturer.m_Name%TYPE,
    p_Active  	   IN Manufacturer.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Mfr   	   OUT SYS_REFCURSOR);
PROCEDURE Get_RechResult(
    p_Code    	   IN Rechallenge_Result.Result_Code%TYPE,
    p_Desc    	   IN Rechallenge_Result.Result_Desc%TYPE,
    p_Active  	   IN Rechallenge_Result.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Res   	   OUT SYS_REFCURSOR);
PROCEDURE Get_RepType(
    p_Code    	   IN Rep_Type.Type_Code%TYPE,
    p_Desc    	   IN Rep_Type.Type_Desc%TYPE,
    p_Active  	   IN Rep_Type.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Rep   	   OUT SYS_REFCURSOR);
PROCEDURE Get_RepSrc(
    p_Code    	   IN Source_Info.Source_Code%TYPE,
    p_Desc    	   IN Source_Info.Source_Desc%TYPE,
    p_Active  	   IN Source_Info.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_RSrc  	   OUT SYS_REFCURSOR);
PROCEDURE Get_RepStat(
    p_Code    	   IN Reporter_Stat.Status_Code%TYPE,
    p_Desc    	   IN Reporter_Stat.Status_Desc%TYPE,
    p_Active  	   IN Reporter_Stat.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_RStat 	   OUT SYS_REFCURSOR);
PROCEDURE Get_LHA(
    p_Code    	   IN LHA.LHA_CODE%TYPE,
    p_Desc    	   IN LHA.LHA_DESC%TYPE,
    p_Ctry    	   IN Country.Country_Nm%TYPE,
    p_Active  	   IN LHA.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_LHA   	   OUT SYS_REFCURSOR);
PROCEDURE Get_SCo(
    p_Name    	   IN Subm_Company.Company_Nm%TYPE,
    p_Ctry    	   IN Country.Country_Nm%TYPE,
    p_Active  	   IN Subm_Company.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_SCo   	   OUT SYS_REFCURSOR);
PROCEDURE Get_Trial(
    p_Code    	   IN Trial_Num.Trial_Num%TYPE,
    p_Active  	   IN Trial_Num.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Trial 	   OUT SYS_REFCURSOR);
PROCEDURE Get_BodySys(
    p_Code    	   IN Body_System.Body_Sys_Num%TYPE,
    p_Desc    	   IN Body_System.Body_System%TYPE,
    p_Active  	   IN Body_System.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_BSys  	   OUT SYS_REFCURSOR);
PROCEDURE Get_Route(
    p_Code    	   IN Route.Route_Code%TYPE,
    p_Desc    	   IN Route.Route_Desc%TYPE,
    p_Active  	   IN Route.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Route 	   OUT SYS_REFCURSOR);
PROCEDURE List_Pat_Num(
    cur_PatNum OUT SYS_REFCURSOR);
PROCEDURE List_Rep_Num(
    cur_RepNum OUT SYS_REFCURSOR);

PROCEDURE Save_License(
    p_License_Key IN LICENSE.License_Key%TYPE);
PROCEDURE Delete_License(
    p_License_Key IN LICENSE.License_Key%TYPE);
PROCEDURE Get_License(
    p_License_Key IN LICENSE.License_Key%TYPE,
    cur_License OUT SYS_REFCURSOR);
PROCEDURE Get_Case_Duplicate(
    p_Date_Recv       		IN 	Case_Details.Date_Recv%TYPE,
    p_Clin_Rep_Info   		IN 	Case_Details.Clin_Rep_Info%TYPE,
    p_Country_Code    		IN 	Case_Details.Country_Code%TYPE,
    p_Product_Code    		IN 	Case_Details.Product_Code%TYPE,
    p_P_Initials			IN 	Case_Details.P_Initials%TYPE,
    p_Age_Reported			IN 	Case_Details.Age_Reported%TYPE,
    p_Sex_Code				IN 	Case_Details.Sex_Code%TYPE,
    p_Trial_Num				IN 	Case_Details.Trial_Num%TYPE,
    p_Patient_Num			IN 	Case_Details.Patient_Num%TYPE,
    p_Phy_Assessment_Code	IN 	Case_Details.Phy_Assessment_Code%TYPE,
    p_Co_Assessment_Code	IN 	Case_Details.Co_Assessment_Code%TYPE,
    p_Verbatim				IN 	Case_Details.Verbatim%TYPE,
    p_Diag_Code				IN	Adverse_Event.Diag_Code%TYPE,
    p_Aggr_Of_Diag			IN	Adverse_Event.Aggr_Of_Diag%TYPE,
    p_Llt_Code				IN	Adverse_Event.Llt_Code%TYPE,
    p_Aggr_Of_Llt			IN	Adverse_Event.Aggr_Of_Llt%TYPE,
    p_Body_Sys_Num			IN	Adverse_Event.Body_Sys_Num%TYPE,
    p_Classification_Code	IN	Adverse_Event.Classification_Code%TYPE,
    p_Outcome_Code			IN	Adverse_Event.Outcome_Code%TYPE,
    cur_Case       			OUT SYS_REFCURSOR);
PROCEDURE Get_CM(
    p_Rep_Num        IN CONC_MEDICATIONS.Rep_Num%TYPE,
    p_Prod_Code      IN CONC_MEDICATIONS.Product_Code%TYPE,
    cur_Case_CM      OUT SYS_REFCURSOR);
PROCEDURE Get_AE(
    p_Rep_Num        IN ADVERSE_EVENT.Rep_Num%TYPE,
    p_Llt_Code       IN ADVERSE_EVENT.llt_Code%TYPE,
    cur_Case_AE      OUT SYS_REFCURSOR);
PROCEDURE Copy_Case(
    p_Username		IN USER_LIST.Username%TYPE,
    p_Rep_Num		IN CASE_DETAILS.Rep_Num%TYPE,
    p_Rep_Num_New	OUT CASE_DETAILS.Rep_Num%TYPE);    
END sapher_pkg;
/
prompt
prompt Creating package body SAPHER_PKG
prompt ================================
prompt
create or replace PACKAGE BODY            "SAPHER_PKG" IS
--------------------------------------------
PROCEDURE Delete_AuditTrial(
    p_Chg_No IN AUDIT_TRIAL.Chg_No%TYPE)
AS
BEGIN
    DELETE
      FROM AUDIT_TRIAL
     WHERE Chg_No = p_Chg_No;
END;
--------------------------------------------
PROCEDURE Save_Role_List(
    p_Save_Flag IN CHAR,
    p_Role_Name IN ROLE_LIST.Role_Name%TYPE,
    p_Is_Locked IN ROLE_LIST.Is_Locked%TYPE)
AS
    CURSOR Fetch_Inventory IS
    SELECT Inv_Id
      FROM INVENTORY;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO ROLE_LIST
               (Role_Name, Is_Locked)
        VALUES (UPPER(p_Role_Name),p_Is_Locked);
        FOR Val IN Fetch_Inventory
        LOOP
           INSERT INTO ROLE_PERMISSIONS VALUES(Val.Inv_Id,UPPER(p_Role_Name),0,0,0,0);
        END LOOP;
    END;
    ELSIF p_Save_Flag='U' THEN
    BEGIN
         UPDATE ROLE_LIST
            SET Is_Locked = p_Is_Locked
          WHERE Role_Name = p_Role_Name;
    END;
    END IF;
END;
--------------------------------------------
PROCEDURE Delete_Role_List(
    p_Role_Name IN ROLE_LIST.Role_Name%TYPE)
AS
BEGIN
    DELETE
      FROM ROLE_LIST
     WHERE Role_Name = p_Role_Name;
END;
--------------------------------------------
PROCEDURE Update_Role_Permissions(
    p_Inv_Id IN ROLE_PERMISSIONS.Inv_Id%TYPE,
    p_Role_Name IN ROLE_PERMISSIONS.Role_Name%TYPE,
    p_Pview IN ROLE_PERMISSIONS.Pview%TYPE,
    p_Padd IN ROLE_PERMISSIONS.Padd%TYPE,
    p_Pedit IN ROLE_PERMISSIONS.Pedit%TYPE,
    p_Pdelete IN ROLE_PERMISSIONS.Pdelete%TYPE)
AS
BEGIN
   UPDATE ROLE_PERMISSIONS
      SET Pview = p_Pview,
          Padd = p_Padd,
          Pedit = p_Pedit,
          Pdelete = p_Pdelete
    WHERE Inv_Id = p_Inv_Id
      AND Role_Name = p_Role_Name;
END;
--------------------------------------------
PROCEDURE Delete_User_List(
    p_Username IN USER_LIST.Username%TYPE)
AS
BEGIN
    DELETE
      FROM USER_LIST
     WHERE Username = p_Username;
END;
--------------------------------------------
PROCEDURE Save_User_List(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Role_Name IN USER_LIST.Role_Name%TYPE,
    p_Password IN USER_LIST.PASSWORD%TYPE,
    p_Pwd_Change_Flag IN USER_LIST.Pwd_Change_Flag%TYPE,
    p_Pwd_Mistake_Count IN USER_LIST.Pwd_Mistake_Count%TYPE,
    p_Is_Locked IN USER_LIST.Is_Locked%TYPE)
AS
l_User User_List%ROWTYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO USER_LIST
               (Username, Role_Name, PASSWORD, Pwd_Change_Flag, Pwd_Mistake_Count, Pwd_Change_Date, Is_Locked)
        VALUES (UPPER(p_Username),p_Role_Name,p_Password,p_Pwd_Change_Flag,p_Pwd_Mistake_Count,SYSDATE,p_Is_Locked);
    END;
    ELSIF p_Save_Flag = 'U' THEN
    BEGIN

      SELECT *
        INTO l_User
        FROM User_List
       WHERE Username = p_Username;

    		UPDATE User_List
           SET Role_Name = (CASE WHEN p_Role_Name IS NULL THEN l_User.Role_Name ELSE p_Role_Name END),
               Password = (CASE WHEN p_Password IS NULL THEN l_User.Password ELSE p_Password END),
               Pwd_Change_Date = (CASE WHEN p_Password IS NULL THEN l_User.Pwd_Change_Date ELSE SYSDATE END),
               Pwd_Change_Flag = (CASE p_Pwd_Change_Flag WHEN -1 THEN l_User.Pwd_Change_Flag ELSE p_Pwd_Change_Flag END),
               Pwd_Mistake_Count = (CASE p_Pwd_Mistake_Count WHEN -1 THEN l_User.Pwd_Mistake_Count ELSE p_Pwd_Mistake_Count END),
               Is_Locked = (CASE p_Is_Locked WHEN -1 THEN l_User.Is_Locked ELSE p_Is_Locked END)
         WHERE Username = p_Username;

    END;
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Country(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Country_Code IN COUNTRY.Country_Code%TYPE,
    p_Country_Nm IN COUNTRY.Country_Nm%TYPE,
    p_Is_Valid IN COUNTRY.Is_Valid%TYPE)
AS
    l_Country_Nm COUNTRY.Country_Nm%TYPE;
    l_Is_Valid COUNTRY.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO COUNTRY
               (Country_Code, Country_Nm, Is_Valid)
        VALUES (UPPER(p_Country_Code),p_Country_Nm,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL7',UPPER(p_Country_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN
        SELECT Country_Nm,Is_Valid
          INTO l_Country_Nm, l_Is_Valid
          FROM COUNTRY
         WHERE Country_Code = p_Country_Code;

        UPDATE COUNTRY
           SET Country_Nm = p_Country_Nm,
               Is_Valid = p_Is_Valid
         WHERE Country_Code = p_Country_Code;

        Save_Audit(p_Username,'CODE','U','CDL7',p_Country_Code,
              'CDL7.2',l_Country_Nm,p_Country_Nm);
        Save_Audit(p_Username,'CODE','U','CDL7',p_Country_Code,
              'CDL7.3',
               CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
               CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
     END IF;
END;
--------------------------------------------
PROCEDURE Delete_Country(
    p_Username IN USER_LIST.Username%TYPE,
    p_Country_Code IN COUNTRY.Country_Code%TYPE)
AS
BEGIN
    DELETE
      FROM COUNTRY
     WHERE Country_Code = p_Country_Code;
    Save_Audit(p_Username,'CODE','D','CDL7',p_Country_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_LHA(
    p_Username IN USER_LIST.Username%TYPE,
    p_LHA_Code IN LHA.LHA_Code%TYPE)
AS
BEGIN
    DELETE
      FROM LHA
     WHERE LHA_Code = p_LHA_Code;
    Save_Audit(p_Username,'CODE','D','CDL15',p_LHA_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Save_LHA(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_LHA_Code IN LHA.LHA_Code%TYPE,
    p_LHA_Desc IN LHA.LHA_Desc%TYPE,
    p_Country_Code IN LHA.Country_Code%TYPE,
    p_Is_Valid IN LHA.Is_Valid%TYPE)
AS
    l_LHA_Desc LHA.LHA_Desc%TYPE;
    l_Country_Code LHA.Country_Code%TYPE;
    l_Is_Valid LHA.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO LHA
               (LHA_Code, LHA_Desc, Is_Valid, Country_Code)
        VALUES (UPPER(p_LHA_Code),p_LHA_Desc,p_Is_Valid,p_Country_Code);
        Save_Audit(p_Username,'CODE','I','CDL15',UPPER(p_LHA_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT LHA_Desc, Country_Code, Is_Valid
          INTO l_LHA_Desc, l_Country_Code, l_Is_Valid
          FROM LHA
         WHERE LHA_Code = p_LHA_Code;

        UPDATE LHA
           SET LHA_Desc = p_LHA_Desc,
               Country_Code = p_Country_Code,
               Is_Valid = p_Is_Valid
          WHERE LHA_Code = p_LHA_Code;

        Save_Audit(p_Username,'CODE','U','CDL15',p_LHA_Code,
                   'CDL15.3',l_LHA_Desc,p_LHA_Desc);
        Save_Audit(p_Username,'CODE','U','CDL15',p_LHA_Code,
                   'CDL15.2',l_Country_Code,p_Country_Code);
        Save_Audit(p_Username,'CODE','U','CDL15',p_LHA_Code,
                   'CDL15.4',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Delete_Adverse_Event(
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN ADVERSE_EVENT.Rep_Num%TYPE,
    p_Llt_Code IN ADVERSE_EVENT.Llt_Code%TYPE)
AS
BEGIN
    DELETE
      FROM ADVERSE_EVENT
     WHERE Rep_Num = p_Rep_Num
       AND Llt_Code = p_Llt_Code;
    Save_Audit(p_Username,'CASE','D','CSD2',p_Rep_Num||','||p_Llt_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_AE_Diag(
    p_Username IN USER_LIST.Username%TYPE,
    p_Diag_Code IN AE_DIAG.Diag_Code%TYPE)
AS
BEGIN
    DELETE
      FROM AE_DIAG
     WHERE Diag_Code = p_Diag_Code;
    Save_Audit(p_Username,'CODE','D','CDL3',p_Diag_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_AE_LLT(
    p_Username IN USER_LIST.Username%TYPE,
    p_LLT_Code IN AE_LLT.Llt_Code%TYPE)
AS
BEGIN
    DELETE
      FROM AE_LLT
     WHERE LLT_Code = p_LLT_Code;
    Save_Audit(p_Username,'CODE','D','CDL1',p_LLT_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_AE_Outcome(
    p_Username IN USER_LIST.Username%TYPE,
    p_Outcome_Code IN AE_OUTCOME.Outcome_Code%TYPE)
AS
BEGIN
    DELETE
      FROM AE_OUTCOME
     WHERE Outcome_Code = p_Outcome_Code;
    Save_Audit(p_Username,'CODE','D','CDL2',p_Outcome_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Assessment_Causality(
    p_Username IN USER_LIST.Username%TYPE,
    p_Assessment_Code IN ASSESSMENT_CAUSALITY.Assessment_Code%TYPE)
AS
BEGIN
    DELETE
      FROM ASSESSMENT_CAUSALITY
     WHERE Assessment_Code = p_Assessment_Code;
    Save_Audit(p_Username,'CODE','D','CDL4',p_Assessment_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Body_System(
    p_Username IN USER_LIST.Username%TYPE,
    p_Body_Sys_Num IN BODY_SYSTEM.Body_Sys_Num%TYPE)
AS
BEGIN
    DELETE
      FROM BODY_SYSTEM
     WHERE Body_Sys_Num = p_Body_Sys_Num;
    Save_Audit(p_Username,'CODE','D','CDL24',p_Body_Sys_Num,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Case_Details(
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CASE_DETAILS.Rep_Num%TYPE)
AS
BEGIN
    DELETE
      FROM CASE_DETAILS
     WHERE Rep_Num = p_Rep_Num;
    Save_Audit(p_Username,'CASE','D','CSD1',p_Rep_Num,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Conc_Medications(
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CONC_MEDICATIONS.Rep_Num%TYPE,
    p_Product_Code IN CONC_MEDICATIONS.Product_Code%TYPE)
AS
BEGIN
    DELETE
      FROM CONC_MEDICATIONS
     WHERE Rep_Num = p_Rep_Num
       AND Product_Code = p_Product_Code;
    Save_Audit(p_Username,'CASE','D','CSD3',p_Rep_Num||','||p_Product_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Subm_Company(
    p_Username IN USER_LIST.Username%TYPE,
    p_Company_Nm IN SUBM_COMPANY.Company_Nm%TYPE)
AS
BEGIN
    DELETE
      FROM SUBM_COMPANY
     WHERE Company_Nm = p_Company_Nm;
    Save_Audit(p_Username,'CODE','D','CDL23',p_Company_Nm,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Dose_Frequency(
    p_Username IN USER_LIST.Username%TYPE,
    p_DFreq_Code IN DOSE_FREQUENCY.Dfreq_Code%TYPE)
AS
BEGIN
    DELETE
      FROM DOSE_FREQUENCY
     WHERE DFreq_Code = p_DFreq_Code;
    Save_Audit(p_Username,'CODE','D','CDL8',p_DFreq_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Dose_Regimen(
    p_Username IN USER_LIST.Username%TYPE,
    p_DRegimen_Code IN DOSE_REGIMEN.Dregimen_Code%TYPE)
AS
BEGIN
    DELETE
      FROM DOSE_REGIMEN
     WHERE DRegimen_Code = p_DRegimen_Code;
    Save_Audit(p_Username,'CODE','D','CDL9',p_DRegimen_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Dose_Units(
    p_Username IN USER_LIST.Username%TYPE,
    p_Unit_Code IN DOSE_UNITS.Unit_Code%TYPE)
AS
BEGIN
    DELETE
      FROM DOSE_UNITS
     WHERE Unit_Code = p_Unit_Code;
    Save_Audit(p_Username,'CODE','D','CDL10',p_Unit_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Ethnic_Origin(
    p_Username IN USER_LIST.Username%TYPE,
    p_Ethnic_Code IN ETHNIC_ORIGIN.Ethnic_Code%TYPE)
AS
BEGIN
    DELETE
      FROM ETHNIC_ORIGIN
     WHERE Ethnic_Code = p_Ethnic_Code;
    Save_Audit(p_Username,'CODE','D','CDL17',p_Ethnic_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Event_Severity(
    p_Username IN USER_LIST.Username%TYPE,
    p_ESeverity_Code IN EVENT_SEVERITY.Eseverity_Code%TYPE)
AS
BEGIN
    DELETE
      FROM EVENT_SEVERITY
     WHERE ESeverity_Code = p_ESeverity_Code;
    Save_Audit(p_Username,'CODE','D','CDL12',p_ESeverity_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Formulation(
    p_Username IN USER_LIST.Username%TYPE,
    p_Formulation_Code IN FORMULATION.Formulation_Code%TYPE)
AS
BEGIN
    DELETE
      FROM FORMULATION
     WHERE Formulation_Code = p_Formulation_Code;
    Save_Audit(p_Username,'CODE','D','CDL13',p_Formulation_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Indication_Treatment(
    p_Username IN USER_LIST.Username%TYPE,
    p_Indication_Num IN INDICATION_TREATMENT.Indication_Num%TYPE)
AS
BEGIN
    DELETE
      FROM INDICATION_TREATMENT
     WHERE Indication_Num = p_Indication_Num;
    Save_Audit(p_Username,'CODE','D','CDL14',p_Indication_Num,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Manufacturer(
    p_Username IN USER_LIST.Username%TYPE,
    p_M_Code IN MANUFACTURER.m_Code%TYPE)
AS
BEGIN
    DELETE
      FROM MANUFACTURER
     WHERE M_Code = p_M_Code;
    Save_Audit(p_Username,'CODE','D','CDL6',p_M_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Product(
    p_Username IN USER_LIST.Username%TYPE,
    p_Product_Code IN PRODUCT.Product_Code%TYPE)
AS
BEGIN
    DELETE
      FROM PRODUCT
     WHERE Product_Code = p_Product_Code;
    Save_Audit(p_Username,'CODE','D','CDL16',p_Product_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Rechallenge_Result(
    p_Username IN USER_LIST.Username%TYPE,
    p_Result_Code IN RECHALLENGE_RESULT.Result_Code%TYPE)
AS
BEGIN
    DELETE
      FROM RECHALLENGE_RESULT
     WHERE Result_Code = p_Result_Code;
    Save_Audit(p_Username,'CODE','D','CDL17',p_Result_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Reporter_Stat(
    p_Username IN USER_LIST.Username%TYPE,
    p_Status_Code IN REPORTER_STAT.Status_Code%TYPE)
AS
BEGIN
    DELETE
      FROM REPORTER_STAT
     WHERE Status_Code = p_Status_Code;
    Save_Audit(p_Username,'CODE','D','CDL20',p_Status_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Rep_Type(
    p_Username IN USER_LIST.Username%TYPE,
    p_Type_Code IN REP_TYPE.Type_Code%TYPE)
AS
BEGIN
    DELETE
      FROM REP_TYPE
     WHERE Type_Code = p_Type_Code;
    Save_Audit(p_Username,'CODE','D','CDL18',p_Type_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Route(
    p_Username IN USER_LIST.Username%TYPE,
    p_Route_Code IN ROUTE.Route_Code%TYPE)
AS
BEGIN
    DELETE
      FROM ROUTE
     WHERE Route_Code = p_Route_Code;
    Save_Audit(p_Username,'CODE','D','CDL25',p_Route_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Seriousness(
    p_Username IN USER_LIST.Username%TYPE,
    p_Classification_Code IN CLASSIFICATION_SERIOUSNESS.Classification_Code%TYPE)
AS
BEGIN
    DELETE
      FROM CLASSIFICATION_SERIOUSNESS
     WHERE Classification_Code = p_Classification_Code;
    Save_Audit(p_Username,'CODE','D','CDL5',p_Classification_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Sex(
    p_Username IN USER_LIST.Username%TYPE,
    p_Sex_Code IN SEX.Sex_Code%TYPE)
AS
BEGIN
    DELETE
      FROM SEX
     WHERE Sex_Code = p_Sex_Code;
    Save_Audit(p_Username,'CODE','D','CDL21',p_Sex_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Source_Info(
    p_Username IN USER_LIST.Username%TYPE,
    p_Source_Code IN SOURCE_INFO.Source_Code%TYPE)
AS
BEGIN
    DELETE
      FROM SOURCE_INFO
     WHERE Source_Code = p_Source_Code;
    Save_Audit(p_Username,'CODE','D','CDL19',p_Source_Code,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Delete_Trial_Num(
    p_Username IN USER_LIST.Username%TYPE,
    p_Trial_Num IN TRIAL_NUM.TRIAL_NUM%TYPE)
AS
BEGIN
    DELETE
      FROM TRIAL_NUM
     WHERE TRIAL_NUM = p_Trial_Num;
    Save_Audit(p_Username,'CODE','D','CDL22',p_Trial_Num,NULL,NULL,NULL);
END;
--------------------------------------------
PROCEDURE Save_Adverse_Event(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN ADVERSE_EVENT.Rep_Num%TYPE,
  	p_Diag_Code IN ADVERSE_EVENT.Diag_Code%TYPE,
  	p_LLT_Code IN ADVERSE_EVENT.Llt_Code%TYPE,
  	p_Outcome_Code IN ADVERSE_EVENT.Outcome_Code%TYPE,
  	p_Body_Sys_Num IN ADVERSE_EVENT.Body_Sys_Num%TYPE,
  	p_Classification_Code IN ADVERSE_EVENT.Classification_Code%TYPE,
  	p_ESeverity_Code IN ADVERSE_EVENT.Eseverity_Code%TYPE,
    p_Reported_To_LHA IN ADVERSE_EVENT.Reported_To_Lha%TYPE,
  	p_LHA_Code IN ADVERSE_EVENT.Lha_Code%TYPE,
  	p_Report_Date IN ADVERSE_EVENT.Report_Date%TYPE,
    p_Date_Start IN ADVERSE_EVENT.Date_Start%TYPE,
  	p_Date_Stop IN ADVERSE_EVENT.Date_Stop%TYPE,
  	p_Labelling IN ADVERSE_EVENT.Labelling%TYPE,
  	p_Prev_Report_Flg IN ADVERSE_EVENT.Prev_Report_Flg%TYPE,
  	p_Prev_Report IN ADVERSE_EVENT.Prev_Report%TYPE,
  	p_Event_Treatment IN ADVERSE_EVENT.Event_Treatment%TYPE,
  	p_Event_Abate_On_Stop IN ADVERSE_EVENT.Event_Abate_On_Stop%TYPE,
  	p_Aggr_Of_Diag IN ADVERSE_EVENT.Aggr_Of_Diag%TYPE,
  	p_Aggr_Of_LLT IN ADVERSE_EVENT.Aggr_Of_Llt%TYPE)
AS
  	l_Diag_Code ADVERSE_EVENT.Diag_Code%TYPE;
  	l_Outcome_Code ADVERSE_EVENT.Outcome_Code%TYPE;
  	l_Body_Sys_Num ADVERSE_EVENT.Body_Sys_Num%TYPE;
  	l_Classification_Code ADVERSE_EVENT.Classification_Code%TYPE;
  	l_ESeverity_Code ADVERSE_EVENT.ESeverity_Code%TYPE;
  	l_LHA_Code ADVERSE_EVENT.LHA_Code%TYPE;
  	l_Report_Date ADVERSE_EVENT.Report_Date%TYPE;
  	l_Date_Start ADVERSE_EVENT.Date_Start%TYPE;
  	l_Date_Stop ADVERSE_EVENT.Date_Stop%TYPE;
  	l_Labelling ADVERSE_EVENT.Labelling%TYPE;
  	l_Prev_Report_Flg ADVERSE_EVENT.Prev_Report_Flg%TYPE;
  	l_Prev_Report ADVERSE_EVENT.Prev_Report%TYPE;
  	l_Event_Treatment ADVERSE_EVENT.Event_Treatment%TYPE;
  	l_Event_Abate_On_Stop ADVERSE_EVENT.Event_Abate_On_Stop%TYPE;
  	l_Aggr_Of_Diag ADVERSE_EVENT.Aggr_Of_Diag%TYPE;
  	l_Aggr_Of_LLT ADVERSE_EVENT.Aggr_Of_LLT%TYPE;
    l_Reported_To_LHA ADVERSE_EVENT.Reported_To_Lha%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO ADVERSE_EVENT
               (Rep_Num, Diag_Code, LLT_Code, Outcome_Code, Body_Sys_Num, Classification_Code, ESeverity_Code,Reported_To_LHA,
                LHA_Code, Date_Start, Date_Stop, Labelling, Prev_Report_Flg, Prev_Report, Event_Treatment,
                Event_Abate_On_Stop, Aggr_Of_Diag, Aggr_Of_LLT,Report_Date)
        VALUES (p_Rep_Num, p_Diag_Code, p_LLT_Code, p_Outcome_Code, p_Body_Sys_Num, p_Classification_Code, p_ESeverity_Code,p_Reported_To_LHA,
                p_LHA_Code, p_Date_Start, p_Date_Stop, p_Labelling, p_Prev_Report_Flg, p_Prev_Report, p_Event_Treatment,
                p_Event_Abate_On_Stop, p_Aggr_Of_Diag, p_Aggr_Of_LLT, p_Report_Date);
        Save_Audit(p_Username,'CASE','I','CSD2',p_Rep_Num||','||p_LLT_Code,NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN
    BEGIN
            SELECT Diag_Code, Outcome_Code, Body_Sys_Num, Classification_Code,
                   ESeverity_Code, Reported_To_LHA, LHA_Code, Report_Date,
                   Date_Start, Date_Stop, Labelling, Prev_Report_Flg,
                   Prev_Report, Event_Treatment, Event_Abate_On_Stop, Aggr_Of_Diag,
                   Aggr_Of_LLT
              INTO l_Diag_Code, l_Outcome_Code, l_Body_Sys_Num, l_Classification_Code,
                   l_ESeverity_Code, l_Reported_To_LHA, l_LHA_Code, l_Report_Date,
                   l_Date_Start, l_Date_Stop, l_Labelling, l_Prev_Report_Flg,
                   l_Prev_Report, l_Event_Treatment, l_Event_Abate_On_Stop,
                   l_Aggr_Of_Diag, l_Aggr_Of_LLT
              FROM ADVERSE_EVENT
             WHERE Rep_Num = p_Rep_Num
               AND LLT_Code = p_LLT_Code;

            UPDATE ADVERSE_EVENT
               SET Diag_Code = p_Diag_Code, Outcome_Code = p_Outcome_Code,
                   Body_Sys_Num = p_Body_Sys_Num, Classification_Code = p_Classification_Code,
                   ESeverity_Code = p_ESeverity_Code, Reported_To_LHA = p_Reported_To_LHA,
                   LHA_Code = p_LHA_Code, Report_Date = p_Report_Date, Date_Start = p_Date_Start,
                   Date_Stop = p_Date_Stop, Labelling = p_Labelling, Prev_Report_Flg = p_Prev_Report_Flg,
                   Prev_Report = p_Prev_Report, Event_Treatment = p_Event_Treatment,
                   Event_Abate_On_Stop = p_Event_Abate_On_Stop, Aggr_Of_Diag = p_Aggr_Of_Diag,
                   Aggr_Of_LLT = p_Aggr_Of_LLT
             WHERE Rep_Num = p_Rep_Num
               AND Llt_Code = p_Llt_Code;

            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.66',p_Llt_Code || '-'||l_Diag_Code,p_Llt_Code || '-'||p_Diag_Code);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.67',p_Llt_Code || '-'||l_Outcome_Code,p_Llt_Code || '-'||p_Outcome_Code);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.68',p_Llt_Code || '-'||l_Body_Sys_Num,p_Llt_Code || '-'||p_Body_Sys_Num);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.69',p_Llt_Code || '-'||l_Classification_Code,p_Llt_Code || '-'||p_Classification_Code);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.70',p_Llt_Code || '-'||l_ESeverity_Code,p_Llt_Code || '-'||p_ESeverity_Code);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.87',p_Llt_Code ||'-'||l_Reported_To_LHA, p_Llt_Code || '-'|| p_Reported_To_LHA);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.71',p_Llt_Code || '-'||l_LHA_Code,p_Llt_Code || '-'||p_LHA_Code);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.72',p_Llt_Code || '-'||l_Report_Date,p_Llt_Code || '-'||p_Report_Date);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.73',p_Llt_Code || '-'||l_Date_Start,p_Llt_Code || '-'||p_Date_Start);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.74',p_Llt_Code || '-'||l_Date_Stop,p_Llt_Code || '-'||p_Date_Stop);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.75',p_Llt_Code || '-'||l_Labelling,p_Llt_Code || '-'||p_Labelling);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.76',p_Llt_Code || '-'||l_Prev_Report_Flg,p_Llt_Code || '-'||p_Prev_Report_Flg);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.77',p_Llt_Code || '-'||l_Prev_Report,p_Llt_Code || '-'||p_Prev_Report);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.78',p_Llt_Code || '-'||l_Event_Treatment,p_Llt_Code || '-'||p_Event_Treatment);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.79',p_Llt_Code || '-'||l_Event_Abate_On_Stop,p_Llt_Code || '-'||p_Event_Abate_On_Stop);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.80',p_Llt_Code || '-'||l_Aggr_Of_Diag,p_Llt_Code || '-'||p_Aggr_Of_Diag);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.81',p_Llt_Code || '-'||l_Aggr_Of_LLT,p_Llt_Code || '-'||p_Aggr_Of_LLT);
    END;
    END IF;
END;
--------------------------------------------
PROCEDURE Save_AE_Diag(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Diag_Code IN AE_DIAG.Diag_Code%TYPE,
    p_AE_Diag_Desc IN AE_DIAG.Diag_Desc%TYPE,
    p_Is_Valid IN AE_DIAG.Is_Valid%TYPE)
AS
    l_AE_Diag_Desc AE_DIAG.Diag_Desc%TYPE;
    l_Is_Valid AE_DIAG.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO AE_DIAG
               (Diag_Code, Diag_Desc, Is_Valid)
        VALUES (UPPER(p_Diag_Code),p_AE_Diag_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL3',UPPER(p_Diag_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

      SELECT Diag_Desc, Is_Valid
        INTO l_AE_Diag_Desc, l_Is_Valid
        FROM AE_DIAG
       WHERE Diag_Code = p_Diag_Code;

      UPDATE AE_DIAG
         SET Diag_Desc = p_AE_Diag_Desc,
             Is_Valid = p_Is_Valid
        WHERE Diag_Code = p_Diag_Code;

      Save_Audit(p_Username,'CODE','U','CDL3',p_Diag_Code,
                 'CDL3.2',l_AE_Diag_Desc,p_AE_Diag_Desc);
      Save_Audit(p_Username,'CODE','U','CDL3',p_Diag_Code,
                 'CDL3.3',
                 CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                 CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_AE_LLT(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_LLT_Code IN AE_LLT.Llt_Code%TYPE,
    p_LLT_Desc IN AE_LLT.Llt_Desc%TYPE,
    p_Is_Valid IN AE_LLT.Is_Valid%TYPE)
AS
    l_LLT_Desc AE_LLT.LLT_Desc%TYPE;
    l_Is_Valid AE_LLT.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO AE_LLT
               (LLT_Code, LLT_Desc, Is_Valid)
        VALUES (UPPER(p_LLT_Code),p_LLT_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL1',UPPER(p_LLT_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

      SELECT LLT_Desc, Is_Valid
        INTO l_LLT_Desc, l_Is_Valid
        FROM AE_LLT
       WHERE LLT_Code = p_LLT_Code;

      UPDATE AE_LLT
         SET LLT_Desc = p_LLT_Desc,
             Is_Valid = p_Is_Valid
        WHERE LLT_Code = p_LLT_Code;

      Save_Audit(p_Username,'CODE','U','CDL1',p_LLT_Code,
                 'CDL1.2',l_LLT_Desc,p_LLT_Desc);
      Save_Audit(p_Username,'CODE','U','CDL1',p_LLT_Code,
                 'CDL1.3',
                 CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                 CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_AE_Outcome(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Outcome_Code IN AE_OUTCOME.Outcome_Code%TYPE,
    p_Outcome IN AE_OUTCOME.Outcome%TYPE,
    p_Is_Valid IN AE_OUTCOME.Is_Valid%TYPE)
AS
    l_Outcome AE_OUTCOME.Outcome%TYPE;
    l_Is_Valid AE_OUTCOME.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO AE_OUTCOME
               (Outcome_Code, Outcome, Is_Valid)
        VALUES (UPPER(p_Outcome_Code),p_Outcome,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL2',UPPER(p_Outcome_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

      SELECT Outcome, Is_Valid
        INTO l_Outcome, l_Is_Valid
        FROM AE_OUTCOME
       WHERE Outcome_Code = p_Outcome_Code;

      UPDATE AE_OUTCOME
         SET Outcome = p_Outcome,
             Is_Valid = p_Is_Valid
        WHERE Outcome_Code = p_Outcome_Code;

      Save_Audit(p_Username,'CODE','U','CDL2',p_Outcome_Code ,
                 'CDL2.2',l_Outcome,p_Outcome);
      Save_Audit(p_Username,'CODE','U','CDL2',p_Outcome_Code,
             'CDL2.3',
             CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
             CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Assessment_Causality(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Assessment_Code IN ASSESSMENT_CAUSALITY.Assessment_Code%TYPE,
    p_Assessment IN ASSESSMENT_CAUSALITY.Assessment%TYPE,
    p_Assessment_Flag IN ASSESSMENT_CAUSALITY.Assessment_Flag%TYPE,
    p_Is_Valid IN ASSESSMENT_CAUSALITY.Is_Valid%TYPE)
AS
    l_Assessment ASSESSMENT_CAUSALITY.Assessment%TYPE;
    l_Assessment_Flag ASSESSMENT_CAUSALITY.Assessment_Flag%TYPE;
    l_Is_Valid ASSESSMENT_CAUSALITY.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO ASSESSMENT_CAUSALITY
               (Assessment_Code, Assessment, Is_Valid, Assessment_Flag)
        VALUES (UPPER(p_Assessment_Code),p_Assessment,p_Is_Valid,p_Assessment_Flag);
        Save_Audit(p_Username,'CODE','I','CDL4',UPPER(p_Assessment_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

    SELECT Assessment,Assessment_Flag,Is_Valid
      INTO l_Assessment, l_Assessment_Flag, l_Is_Valid
      FROM ASSESSMENT_CAUSALITY
     WHERE Assessment_Code = p_Assessment_Code;

    UPDATE ASSESSMENT_CAUSALITY
       SET Assessment = p_Assessment,
           Assessment_Flag = p_Assessment_Flag,
           Is_Valid = p_Is_Valid
     WHERE Assessment_Code = p_Assessment_Code;

    Save_Audit(p_Username,'CODE','U','CDL4',p_Assessment_Code ,
               'CDL4.2',l_Assessment,p_Assessment);
    Save_Audit(p_Username,'CODE','U','CDL4',p_Assessment_Code ,
               'CDL4.3',l_Assessment_Flag,p_Assessment_Flag);
    Save_Audit(p_Username,'CODE','U','CDL4',p_Assessment_Code,
               'CDL4.4',
               CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                 CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Audit(
    p_Username IN AUDIT_TRIAL.Username%TYPE,
    p_Tgt_Type IN AUDIT_TRIAL.Tgt_Type%TYPE,
    p_Txn_Type IN AUDIT_TRIAL.Txn_Type%TYPE,
    p_Inv_Id IN AUDIT_TRIAL.Inv_Id%TYPE,
    p_PK_Col_Val IN AUDIT_TRIAL.Pk_Col_Value%TYPE,
    p_Inv_Col_Id IN AUDIT_TRIAL.Inv_Col_Id%TYPE,
    p_Old_Val IN AUDIT_TRIAL.Old_Val%TYPE,
    p_New_Val IN AUDIT_TRIAL.New_Val%TYPE)
AS
    l_Audit_Flag INVENTORY_COLS.Audit_Flag%TYPE;
    l_Inv_Ins_Flag INVENTORY.Ins_Audit_Flag%TYPE;
    l_Inv_Del_Flag INVENTORY.Del_Audit_Flag%TYPE;
BEGIN
    IF p_Txn_Type='U'
    THEN
     SELECT Audit_Flag INTO l_Audit_Flag FROM Inventory_Cols WHERE Inv_Col_Id = p_Inv_Col_Id;
      IF (l_Audit_Flag=1 AND p_Old_Val <> p_New_Val)
      THEN
          INSERT INTO AUDIT_TRIAL
          (Chg_No,Username,Tgt_Type,Txn_Type,Inv_Id,Chg_Time,PK_Col_Value,Inv_Col_Id,Old_Val,New_Val)
          VALUES (TO_CHAR(SYSDATE,'YYYY')||Seq_Audit.NEXTVAL,p_Username,p_Tgt_Type,p_Txn_Type,
          p_Inv_Id,SYSDATE,p_PK_Col_Val,p_Inv_Col_Id,p_Old_Val,p_New_Val);
      END IF;
    ELSIF p_Txn_Type='I'
     THEN
	     SELECT Ins_Audit_Flag INTO l_Inv_Ins_Flag FROM INVENTORY WHERE Inv_Id = p_Inv_Id;
	      IF (l_Inv_Ins_Flag=1)
	      THEN
	          INSERT INTO AUDIT_TRIAL
	          (Chg_No,Username,Tgt_Type,Txn_Type,Inv_Id,Chg_Time,PK_Col_Value,Inv_Col_Id,Old_Val,New_Val)
	          VALUES (TO_CHAR(SYSDATE,'YYYY')||Seq_Audit.NEXTVAL,p_Username,p_Tgt_Type,p_Txn_Type,
	          p_Inv_Id,SYSDATE,p_PK_Col_Val,p_Inv_Col_Id,p_Old_Val,p_New_Val);
	      END IF;
    ELSIF p_Txn_Type='D'
     THEN
	     SELECT Del_Audit_Flag INTO l_Inv_Del_Flag FROM INVENTORY WHERE Inv_Id = p_Inv_Id;
	      IF (l_Inv_Del_Flag=1)
	      THEN
	          INSERT INTO AUDIT_TRIAL
	          (Chg_No,Username,Tgt_Type,Txn_Type,Inv_Id,Chg_Time,PK_Col_Value,Inv_Col_Id,Old_Val,New_Val)
	          VALUES (TO_CHAR(SYSDATE,'YYYY')||Seq_Audit.NEXTVAL,p_Username,p_Tgt_Type,p_Txn_Type,
	          p_Inv_Id,SYSDATE,p_PK_Col_Val,p_Inv_Col_Id,p_Old_Val,p_New_Val);
	      END IF;
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Body_System(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Body_Sys_Num IN BODY_SYSTEM.Body_Sys_Num%TYPE,
    p_Body_System IN BODY_SYSTEM.BODY_SYSTEM%TYPE,
    p_Is_Valid IN BODY_SYSTEM.Is_Valid%TYPE)
AS
    l_Body_System BODY_SYSTEM.BODY_SYSTEM%TYPE;
    l_Is_Valid BODY_SYSTEM.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO BODY_SYSTEM
               (Body_Sys_Num, BODY_SYSTEM, Is_Valid)
        VALUES (UPPER(p_Body_Sys_Num),p_Body_System,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL24',UPPER(p_Body_Sys_Num),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT BODY_SYSTEM, Is_Valid
          INTO l_Body_System, l_Is_Valid
          FROM BODY_SYSTEM
         WHERE Body_Sys_Num = p_Body_Sys_Num;

        UPDATE BODY_SYSTEM
           SET BODY_SYSTEM = p_Body_System,
               Is_Valid = p_Is_Valid
          WHERE Body_Sys_Num = p_Body_Sys_Num;

        Save_Audit(p_Username,'CODE','U','CDL24',p_Body_Sys_Num,
                   'CDL24.2',l_Body_System,p_Body_System);
        Save_Audit(p_Username,'CODE','U','CDL24',p_Body_Sys_Num,
                   'CDL24.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Case_Details(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN OUT CASE_DETAILS.Rep_Num%TYPE,
  	p_Type_Code IN CASE_DETAILS.Type_Code%TYPE,
  	p_Trial_Num IN CASE_DETAILS.TRIAL_NUM%TYPE,
  	p_Country_Code IN CASE_DETAILS.Country_Code%TYPE,
  	p_Status_Code IN CASE_DETAILS.Status_Code%TYPE,
  	p_Source_Code IN CASE_DETAILS.Source_Code%TYPE,
  	p_M_Code IN CASE_DETAILS.M_Code%TYPE,
  	p_Formulation_Code IN CASE_DETAILS.Formulation_Code%TYPE,
  	p_Unit_Code IN CASE_DETAILS.Unit_Code%TYPE,
  	p_DRegimen_Code IN CASE_DETAILS.Dregimen_Code%TYPE,
  	p_DFreq_Code IN CASE_DETAILS.Dfreq_Code%TYPE,
  	p_Route_Code IN CASE_DETAILS.Route_Code%TYPE,
  	p_Indication_Num IN CASE_DETAILS.Indication_Num%TYPE,
  	p_Sex_Code IN CASE_DETAILS.Sex_Code%TYPE,
  	p_Ethnic_Code IN CASE_DETAILS.Ethnic_Code%TYPE,
  	p_Outcome_Code IN CASE_DETAILS.Outcome_Code%TYPE,
  	p_Product_Code IN CASE_DETAILS.Product_Code%TYPE,
  	p_Phy_Assessment_Code IN CASE_DETAILS.Phy_Assessment_Code%TYPE,
  	p_Co_Assessment_Code IN CASE_DETAILS.Co_Assessment_Code%TYPE,
  	p_Result_Code IN CASE_DETAILS.Result_Code%TYPE,
  	p_Subm_Company_Nm IN CASE_DETAILS.Subm_Company_Nm%TYPE,
  	p_Subm_LHA_Code IN CASE_DETAILS.Subm_Lha_Code%TYPE,
  	p_Date_Entry IN CASE_DETAILS.Date_Entry%TYPE,
  	p_Recv_By_At_Hq IN CASE_DETAILS.Recv_By_At_Hq%TYPE,
  	p_Date_Recv IN CASE_DETAILS.Date_Recv%TYPE,
  	p_Rep_Num_Local IN CASE_DETAILS.Rep_Num_Local%TYPE,
  	p_Rep_Xref_Num IN CASE_DETAILS.Rep_Xref_Num%TYPE,
  	p_Clin_Rep_Info IN CASE_DETAILS.Clin_Rep_Info%TYPE,
  	p_Comp_Rep_Info IN CASE_DETAILS.Comp_Rep_Info%TYPE,
  	p_Patient_Num IN CASE_DETAILS.Patient_Num%TYPE,
  	p_Hosp_Nm IN CASE_DETAILS.Hosp_Nm%TYPE,
  	p_Batch_Num IN CASE_DETAILS.Batch_Num%TYPE,
  	p_Dose IN CASE_DETAILS.Dose%TYPE,
  	p_Treat_Start IN CASE_DETAILS.Treat_Start%TYPE,
  	p_Treat_Stop IN CASE_DETAILS.Treat_Stop%TYPE,
  	p_Rechallenge IN CASE_DETAILS.Rechallenge%TYPE,
  	p_Authorization_No IN CASE_DETAILS.Authorization_No%TYPE,
  	p_P_Initials IN CASE_DETAILS.p_Initials%TYPE,
  	p_Date_Of_Birth IN CASE_DETAILS.Date_Of_Birth%TYPE,
  	p_Age_Reported IN CASE_DETAILS.Age_Reported%TYPE,
  	p_Weight IN CASE_DETAILS.Weight%TYPE,
  	p_Height IN CASE_DETAILS.Height%TYPE,
  	p_Pregnancy IN CASE_DETAILS.Pregnancy%TYPE,
  	p_Pregnancy_Week IN CASE_DETAILS.Pregnancy_Week%TYPE,
  	p_Short_History IN CASE_DETAILS.Short_History%TYPE,
  	p_Outcome_Rep_Date IN CASE_DETAILS.Outcome_Rep_Date%TYPE,
  	p_Autopsy IN CASE_DETAILS.Autopsy%TYPE,
  	p_Autopsy_Result IN CASE_DETAILS.Autopsy_Result%TYPE,
  	p_Phy_Assessment_Date IN CASE_DETAILS.Phy_Assessment_Date%TYPE,
  	p_Co_Assessment_Date IN CASE_DETAILS.Co_Assessment_Date%TYPE,
  	p_Phy_Assessment_Reason IN CASE_DETAILS.Phy_Assessment_Reason%TYPE,
  	p_Co_Assessment_Reason IN CASE_DETAILS.Co_Assessment_Reason%TYPE,
  	p_Contributing_Factors IN CASE_DETAILS.Contributing_Factors%TYPE,
  	p_Rechallenge_Date IN CASE_DETAILS.Rechallenge_Date%TYPE,
  	p_Rechallenge_Dose IN CASE_DETAILS.Rechallenge_Dose%TYPE,
  	p_Verbatim IN CASE_DETAILS.Verbatim%TYPE,
  	p_SHORT_COMMENT IN CASE_DETAILS.Short_Comment%TYPE,
  	p_Distbn_Date IN CASE_DETAILS.Distbn_Date%TYPE,
  	p_Submission_Date IN CASE_DETAILS.Submission_Date%TYPE,
  	p_Further_Commn IN CASE_DETAILS.Further_Commn%TYPE,
  	p_AE_Desc IN CASE_DETAILS.AE_Desc%TYPE,
  	p_Is_Suspended IN CASE_DETAILS.Is_Suspended%TYPE)
AS
  	l_Type_Code CASE_DETAILS.Type_Code%TYPE;
  	l_Trial_Num CASE_DETAILS.TRIAL_NUM%TYPE;
  	l_Country_Code CASE_DETAILS.Country_Code%TYPE;
  	l_Status_Code CASE_DETAILS.Status_Code%TYPE;
  	l_Source_Code CASE_DETAILS.Source_Code%TYPE;
  	l_M_Code CASE_DETAILS.M_Code%TYPE;
  	l_Formulation_Code CASE_DETAILS.Formulation_Code%TYPE;
  	l_Unit_Code CASE_DETAILS.Unit_Code%TYPE;
  	l_DRegimen_Code CASE_DETAILS.DRegimen_Code%TYPE;
  	l_DFreq_Code CASE_DETAILS.DFreq_Code%TYPE;
  	l_Route_Code CASE_DETAILS.Route_Code%TYPE;
  	l_Indication_Num CASE_DETAILS.Indication_Num%TYPE;
  	l_Sex_Code CASE_DETAILS.Sex_Code%TYPE;
  	l_Ethnic_Code CASE_DETAILS.Ethnic_Code%TYPE;
  	l_Outcome_Code CASE_DETAILS.Outcome_Code%TYPE;
  	l_Product_Code CASE_DETAILS.Product_Code%TYPE;
  	l_Phy_Assessment_Code CASE_DETAILS.Phy_Assessment_Code%TYPE;
  	l_Co_Assessment_Code CASE_DETAILS.Co_Assessment_Code%TYPE;
  	l_Result_Code CASE_DETAILS.Result_Code%TYPE;
  	l_Subm_Company_Nm CASE_DETAILS.Subm_Company_Nm%TYPE;
  	l_Subm_LHA_Code CASE_DETAILS.Subm_LHA_Code%TYPE;
  	l_Date_Entry CASE_DETAILS.Date_Entry%TYPE;
  	l_Recv_By_At_Hq CASE_DETAILS.Recv_By_At_Hq%TYPE;
  	l_Date_Recv CASE_DETAILS.Date_Recv%TYPE;
  	l_Rep_Num_Local CASE_DETAILS.Rep_Num_Local%TYPE;
  	l_Rep_Xref_Num CASE_DETAILS.Rep_Xref_Num%TYPE;
  	l_Clin_Rep_Info CASE_DETAILS.Clin_Rep_Info%TYPE;
  	l_Comp_Rep_Info CASE_DETAILS.Comp_Rep_Info%TYPE;
  	l_Patient_Num CASE_DETAILS.Patient_Num%TYPE;
  	l_Hosp_Nm CASE_DETAILS.Hosp_Nm%TYPE;
  	l_Batch_Num CASE_DETAILS.Batch_Num%TYPE;
  	l_Dose CASE_DETAILS.Dose%TYPE;
  	l_Treat_Start CASE_DETAILS.Treat_Start%TYPE;
  	l_Treat_Stop CASE_DETAILS.Treat_Stop%TYPE;
  	l_Rechallenge CASE_DETAILS.Rechallenge%TYPE;
  	l_Authorization_No CASE_DETAILS.Authorization_No%TYPE;
  	l_P_Initials CASE_DETAILS.P_Initials%TYPE;
  	l_Date_Of_Birth CASE_DETAILS.Date_Of_Birth%TYPE;
  	l_Age_Reported CASE_DETAILS.Age_Reported%TYPE;
  	l_Weight CASE_DETAILS.Weight%TYPE;
  	l_Height CASE_DETAILS.Height%TYPE;
  	l_Pregnancy CASE_DETAILS.Pregnancy%TYPE;
  	l_Pregnancy_Week CASE_DETAILS.Pregnancy_Week%TYPE;
  	l_Outcome_Rep_Date CASE_DETAILS.Outcome_Rep_Date%TYPE;
  	l_Autopsy CASE_DETAILS.Autopsy%TYPE;
  	l_Autopsy_Result CASE_DETAILS.Autopsy_Result%TYPE;
  	l_Phy_Assessment_Date CASE_DETAILS.Phy_Assessment_Date%TYPE;
  	l_Co_Assessment_Date CASE_DETAILS.Co_Assessment_Date%TYPE;
  	l_Phy_Assessment_Reason CASE_DETAILS.Phy_Assessment_Reason%TYPE;
  	l_Co_Assessment_Reason CASE_DETAILS.Co_Assessment_Reason%TYPE;
  	l_Contributing_Factors CASE_DETAILS.Contributing_Factors%TYPE;
  	l_Rechallenge_Date CASE_DETAILS.Rechallenge_Date%TYPE;
  	l_Rechallenge_Dose CASE_DETAILS.Rechallenge_Dose%TYPE;
  	l_Distbn_Date CASE_DETAILS.Distbn_Date%TYPE;
  	l_Submission_Date CASE_DETAILS.Submission_Date%TYPE;
  	l_Further_Commn CASE_DETAILS.Further_Commn%TYPE;
    l_Is_Suspended CASE_DETAILS.Is_Suspended%TYPE;
    l_Max_No MAX_NO.MAX_NO%TYPE;
    l_cnt NUMBER;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN

        p_Rep_Num := TO_CHAR(SYSDATE,'YYYY');

        SELECT COUNT(*)
          INTO l_cnt
          FROM MAX_NO
         WHERE no_type = 'C'
           AND yr = p_Rep_Num;

        IF l_cnt <> 0 THEN
          UPDATE MAX_NO m
             SET m.MAX_NO = m.MAX_NO + 1
           WHERE m.no_type = 'C'
             AND m.yr = p_Rep_Num;
        ELSE
          INSERT INTO MAX_NO VALUES ('C',p_Rep_Num,1);
        END IF;

        SELECT m.MAX_NO
          INTO l_Max_No
          FROM MAX_NO m
         WHERE m.no_type = 'C'
           AND m.yr = p_Rep_Num;

        p_Rep_Num := p_Rep_Num || lpad(l_Max_No,5,'0');

        INSERT
          INTO CASE_DETAILS
               (Age_Reported, Phy_Assessment_Code, Co_Assessment_Code, Authorization_No, Autopsy,
               Autopsy_Result, Batch_Num, Clin_Rep_Info, Comp_Rep_Info, Contributing_Factors,
               Country_Code, Co_Assessment_Date, Co_Assessment_Reason, Date_Entry, Date_Of_Birth,
               Date_Recv, Dfreq_Code, Subm_Company_Nm, Distbn_Date, Dose, Dregimen_Code, Ethnic_Code,
               Formulation_Code, Further_Commn, Height, Hosp_Nm, Indication_Num, Is_Suspended, M_Code,
               Outcome_Code, Outcome_Rep_Date, Patient_Num, Phy_Assessment_Date, Phy_Assessment_Reason,
               Pregnancy, Pregnancy_Week, Product_Code, P_Initials, Rechallenge_Dose, Rechallenge,
               Rechallenge_Date, Recv_By_At_Hq, Rep_Num, Rep_Num_Local, Rep_Xref_Num,
               Result_Code, Route_Code, Sex_Code, Short_Comment, Short_History, Source_Code,
               Status_Code, Submission_Date, Subm_Lha_Code, Treat_Start, Treat_Stop, TRIAL_NUM,
               Type_Code, Unit_Code, Verbatim, Weight, AE_Desc)
        VALUES (CASE P_Age_Reported WHEN -1 THEN NULL ELSE P_Age_Reported END, P_Phy_Assessment_Code,
                P_Co_Assessment_Code, P_Authorization_No, P_Autopsy, P_Autopsy_Result, P_Batch_Num,
                P_Clin_Rep_Info, P_Comp_Rep_Info, P_Contributing_Factors,
                P_Country_Code, P_Co_Assessment_Date, P_Co_Assessment_Reason, P_Date_Entry, P_Date_Of_Birth,
                P_Date_Recv, P_Dfreq_Code, P_Subm_Company_Nm, P_Distbn_Date, CASE P_Dose WHEN -1 THEN NULL ELSE p_Dose END,
                P_Dregimen_Code, P_Ethnic_Code, P_Formulation_Code, P_Further_Commn,
                CASE P_Height WHEN -1 THEN NULL ELSE P_Height END, P_Hosp_Nm, P_Indication_Num,
                P_Is_Suspended, P_M_Code, P_Outcome_Code, P_Outcome_Rep_Date, P_Patient_Num,
                P_Phy_Assessment_Date, P_Phy_Assessment_Reason, P_Pregnancy,
                CASE P_Pregnancy_Week WHEN -1 THEN NULL ELSE P_Pregnancy_Week END,
                P_Product_Code, P_P_Initials, P_Rechallenge_Dose, P_Rechallenge, P_Rechallenge_Date,
                 P_Recv_By_At_Hq, P_Rep_Num, P_Rep_Num_Local, P_Rep_Xref_Num, P_Result_Code,
                P_Route_Code, P_Sex_Code, P_Short_Comment, P_Short_History, P_Source_Code, P_Status_Code,
                P_Submission_Date, P_Subm_Lha_Code, P_Treat_Start, P_Treat_Stop, P_Trial_Num, P_Type_Code,
                P_Unit_Code, P_Verbatim, CASE P_Weight WHEN -1 THEN NULL ELSE P_Weight END, p_AE_Desc);
        Save_Audit(p_Username,'CASE','I','CSD1',p_Rep_Num,NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN
        BEGIN
            SELECT WEIGHT, UNIT_CODE, TYPE_CODE, TRIAL_NUM,
                   TREAT_STOP, TREAT_START, SUBM_LHA_CODE, SUBMISSION_DATE,
                   STATUS_CODE, SOURCE_CODE,
                   SEX_CODE, ROUTE_CODE, RESULT_CODE, REP_XREF_NUM, REP_NUM_LOCAL,
                   RECV_BY_AT_HQ, RECHALLENGE_DATE, RECHALLENGE, RECHALLENGE_DOSE,
                   P_INITIALS, PRODUCT_CODE, PREGNANCY_WEEK, PREGNANCY,
                   PHY_ASSESSMENT_REASON, PHY_ASSESSMENT_DATE, PATIENT_NUM,
                   OUTCOME_REP_DATE, OUTCOME_CODE, M_CODE, IS_SUSPENDED,
                   INDICATION_NUM, HOSP_NM, HEIGHT, FURTHER_COMMN, FORMULATION_CODE,
                   ETHNIC_CODE, DREGIMEN_CODE, DOSE, DISTBN_DATE, SUBM_COMPANY_NM,
                   DFREQ_CODE, DATE_RECV, DATE_OF_BIRTH, DATE_ENTRY, CO_ASSESSMENT_REASON,
                   CO_ASSESSMENT_DATE, COUNTRY_CODE, CONTRIBUTING_FACTORS, COMP_REP_INFO,
                   CLIN_REP_INFO, BATCH_NUM, AUTOPSY_RESULT, AUTOPSY, AUTHORIZATION_NO,
                   CO_ASSESSMENT_CODE, PHY_ASSESSMENT_CODE, AGE_REPORTED
              INTO l_WEIGHT, l_UNIT_CODE, l_TYPE_CODE, l_TRIAL_NUM, l_TREAT_STOP,
                   l_TREAT_START, l_SUBM_LHA_CODE, l_SUBMISSION_DATE, l_STATUS_CODE,
                   l_SOURCE_CODE, l_SEX_CODE, l_ROUTE_CODE,
                   l_RESULT_CODE, l_REP_XREF_NUM, l_REP_NUM_LOCAL, l_RECV_BY_AT_HQ,
                   l_RECHALLENGE_DATE, l_RECHALLENGE, l_RECHALLENGE_DOSE, l_P_INITIALS,
                   l_PRODUCT_CODE, l_PREGNANCY_WEEK, l_PREGNANCY, l_PHY_ASSESSMENT_REASON,
                   l_PHY_ASSESSMENT_DATE, l_PATIENT_NUM, l_OUTCOME_REP_DATE, l_OUTCOME_CODE,
                   l_M_CODE, l_IS_SUSPENDED, l_INDICATION_NUM, l_HOSP_NM, l_HEIGHT,
                   l_FURTHER_COMMN, l_FORMULATION_CODE, l_ETHNIC_CODE, l_DREGIMEN_CODE,
                   l_DOSE, l_DISTBN_DATE, l_SUBM_COMPANY_NM, l_DFREQ_CODE, l_DATE_RECV,
                   l_DATE_OF_BIRTH, l_DATE_ENTRY, l_CO_ASSESSMENT_REASON, l_CO_ASSESSMENT_DATE,
                   l_COUNTRY_CODE, l_CONTRIBUTING_FACTORS, l_COMP_REP_INFO, l_CLIN_REP_INFO,
                   l_BATCH_NUM, l_AUTOPSY_RESULT, l_AUTOPSY, l_AUTHORIZATION_NO,
                   l_CO_ASSESSMENT_CODE, l_PHY_ASSESSMENT_CODE, l_AGE_REPORTED
              FROM CASE_DETAILS
             WHERE Rep_Num = p_Rep_Num;

            UPDATE CASE_DETAILS
               SET WEIGHT = p_WEIGHT,
                   UNIT_CODE = p_UNIT_CODE,
                   TYPE_CODE = p_TYPE_CODE,
                   TRIAL_NUM = p_TRIAL_NUM,
                   TREAT_STOP = p_TREAT_STOP,
                   TREAT_START = p_TREAT_START,
                   SUBM_LHA_CODE = p_SUBM_LHA_CODE,
                   SUBMISSION_DATE = p_SUBMISSION_DATE,
                   STATUS_CODE = p_STATUS_CODE,
                   SOURCE_CODE = p_SOURCE_CODE,
                   SEX_CODE = p_SEX_CODE,
                   ROUTE_CODE = p_ROUTE_CODE,
                   RESULT_CODE = p_RESULT_CODE,
                   REP_XREF_NUM = p_REP_XREF_NUM,
                   REP_NUM_LOCAL = p_REP_NUM_LOCAL,
                   RECV_BY_AT_HQ = p_RECV_BY_AT_HQ,
                   RECHALLENGE_DATE = p_RECHALLENGE_DATE,
                   RECHALLENGE = p_RECHALLENGE,
                   RECHALLENGE_DOSE = p_RECHALLENGE_DOSE,
                   P_INITIALS = p_P_INITIALS,
                   PRODUCT_CODE = p_PRODUCT_CODE,
                   PREGNANCY_WEEK = p_PREGNANCY_WEEK,
                   PREGNANCY = p_PREGNANCY,
                   PHY_ASSESSMENT_REASON = p_PHY_ASSESSMENT_REASON,
                   PHY_ASSESSMENT_DATE = p_PHY_ASSESSMENT_DATE,
                   PATIENT_NUM = p_PATIENT_NUM,
                   OUTCOME_REP_DATE = p_OUTCOME_REP_DATE,
                   OUTCOME_CODE = p_OUTCOME_CODE,
                   M_CODE = p_M_CODE,
                   IS_SUSPENDED = p_IS_SUSPENDED,
                   INDICATION_NUM = p_INDICATION_NUM,
                   HOSP_NM = p_HOSP_NM,
                   HEIGHT = p_HEIGHT,
                   FURTHER_COMMN = p_FURTHER_COMMN,
                   FORMULATION_CODE = p_FORMULATION_CODE,
                   ETHNIC_CODE = p_ETHNIC_CODE,
                   DREGIMEN_CODE = p_DREGIMEN_CODE,
                   DOSE = p_DOSE,
                   DISTBN_DATE = p_DISTBN_DATE,
                   SUBM_COMPANY_NM = p_SUBM_COMPANY_NM,
                   DFREQ_CODE = p_DFREQ_CODE,
                   DATE_RECV = p_DATE_RECV,
                   DATE_OF_BIRTH = p_DATE_OF_BIRTH,
                   DATE_ENTRY = p_DATE_ENTRY,
                   CO_ASSESSMENT_REASON = p_CO_ASSESSMENT_REASON,
                   CO_ASSESSMENT_DATE = p_CO_ASSESSMENT_DATE,
                   COUNTRY_CODE = p_COUNTRY_CODE,
                   CONTRIBUTING_FACTORS = p_CONTRIBUTING_FACTORS,
                   COMP_REP_INFO = p_COMP_REP_INFO,
                   CLIN_REP_INFO = p_CLIN_REP_INFO,
                   BATCH_NUM = p_BATCH_NUM,
                   AUTOPSY_RESULT = p_AUTOPSY_RESULT,
                   AUTOPSY = p_AUTOPSY,
                   AUTHORIZATION_NO = p_AUTHORIZATION_NO,
                   CO_ASSESSMENT_CODE = p_CO_ASSESSMENT_CODE,
                   PHY_ASSESSMENT_CODE = p_PHY_ASSESSMENT_CODE,
                   AGE_REPORTED = p_AGE_REPORTED
             WHERE Rep_Num = p_Rep_Num;

            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.41',l_WEIGHT,p_WEIGHT);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.9',l_UNIT_CODE,p_UNIT_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.2',l_TYPE_CODE,p_TYPE_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.3',l_TRIAL_NUM,p_TRIAL_NUM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.35',l_TREAT_STOP,p_TREAT_STOP);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.34',l_TREAT_START,p_TREAT_START);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.23',l_SUBM_LHA_CODE,p_SUBM_LHA_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.59',l_SUBMISSION_DATE,p_SUBMISSION_DATE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.5',l_STATUS_CODE,p_STATUS_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.6',l_SOURCE_CODE,p_SOURCE_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.14',l_SEX_CODE,p_SEX_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.12',l_ROUTE_CODE,p_ROUTE_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.20',l_RESULT_CODE,p_RESULT_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.27',l_REP_XREF_NUM,p_REP_XREF_NUM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.26',l_REP_NUM_LOCAL,p_REP_NUM_LOCAL);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.63',l_RECV_BY_AT_HQ,p_RECV_BY_AT_HQ);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.54',l_RECHALLENGE_DATE,p_RECHALLENGE_DATE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.36',l_RECHALLENGE,p_RECHALLENGE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.55',l_RECHALLENGE_DOSE,p_RECHALLENGE_DOSE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.38',l_P_INITIALS,p_P_INITIALS);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.17',l_PRODUCT_CODE,p_PRODUCT_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.44',l_PREGNANCY_WEEK,p_PREGNANCY_WEEK);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.43',l_PREGNANCY,p_PREGNANCY);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.51',l_PHY_ASSESSMENT_REASON,p_PHY_ASSESSMENT_REASON);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.49',l_PHY_ASSESSMENT_DATE,p_PHY_ASSESSMENT_DATE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.30',l_PATIENT_NUM,p_PATIENT_NUM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.46',l_OUTCOME_REP_DATE,p_OUTCOME_REP_DATE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.16',l_OUTCOME_CODE,p_OUTCOME_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.7',l_M_CODE,p_M_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.62',
                       CASE l_IS_SUSPENDED WHEN 1 THEN 'Suspended' WHEN 0 THEN 'Active' END,
                       CASE p_IS_SUSPENDED WHEN 1 THEN 'Suspended' WHEN 0 THEN 'Active' END);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.13',l_INDICATION_NUM,p_INDICATION_NUM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.31',l_HOSP_NM,p_HOSP_NM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.42',l_HEIGHT,p_HEIGHT);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.60',l_FURTHER_COMMN,p_FURTHER_COMMN);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.8',l_FORMULATION_CODE,p_FORMULATION_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.15',l_ETHNIC_CODE,p_ETHNIC_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.10',l_DREGIMEN_CODE,p_DREGIMEN_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.33',l_DOSE,p_DOSE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.58',l_DISTBN_DATE,p_DISTBN_DATE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.21',l_SUBM_COMPANY_NM,p_SUBM_COMPANY_NM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.11',l_DFREQ_CODE,p_DFREQ_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.25',l_DATE_RECV,p_DATE_RECV);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.39',l_DATE_OF_BIRTH,p_DATE_OF_BIRTH);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.24',l_DATE_ENTRY,p_DATE_ENTRY);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.19',l_CO_ASSESSMENT_REASON,p_CO_ASSESSMENT_REASON);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.50',l_CO_ASSESSMENT_DATE,p_CO_ASSESSMENT_DATE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.4',l_COUNTRY_CODE,p_COUNTRY_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.53',l_CONTRIBUTING_FACTORS,p_CONTRIBUTING_FACTORS);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.29',l_COMP_REP_INFO,p_COMP_REP_INFO);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.28',l_CLIN_REP_INFO,p_CLIN_REP_INFO);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.32',l_BATCH_NUM,p_BATCH_NUM);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.48',l_AUTOPSY_RESULT,p_AUTOPSY_RESULT);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.47',l_AUTOPSY,p_AUTOPSY);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.37',l_AUTHORIZATION_NO,p_AUTHORIZATION_NO);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.19',l_CO_ASSESSMENT_CODE,p_CO_ASSESSMENT_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.18',l_PHY_ASSESSMENT_CODE,p_PHY_ASSESSMENT_CODE);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.40',l_AGE_REPORTED,p_AGE_REPORTED);

        END;
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Case_AE_Details(
	p_Username IN USER_LIST.Username%TYPE,
	p_Rep_Num IN OUT CASE_DETAILS.Rep_Num%TYPE,
	p_Verbatim IN CASE_DETAILS.Verbatim%TYPE,
	p_AE_Desc IN CASE_DETAILS.AE_Desc%TYPE)
AS
	l_Verbatim CASE_DETAILS.Verbatim%TYPE;
	l_AE_Desc CASE_DETAILS.AE_Desc%TYPE;
BEGIN
	SELECT Verbatim, AE_Desc
	  INTO l_Verbatim, l_AE_Desc
	  FROM CASE_DETAILS
	 WHERE Rep_Num = p_Rep_Num;

	UPDATE CASE_DETAILS
	   SET Verbatim = p_Verbatim,
	   	   AE_Desc = p_AE_Desc
	 WHERE Rep_Num = p_Rep_Num;

	Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.56',l_VERBATIM,p_VERBATIM);
    Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.61',l_AE_Desc,p_AE_Desc);
END;
--------------------------------------------
PROCEDURE Save_Case_CM_Details(
	p_Username IN USER_LIST.Username%TYPE,
	p_Rep_Num IN OUT CASE_DETAILS.Rep_Num%TYPE,
	p_Short_History IN CASE_DETAILS.Short_History%TYPE,
	p_SHORT_COMMENT IN CASE_DETAILS.Short_Comment%TYPE)
AS
	l_Short_History CASE_DETAILS.Short_History%TYPE;
	l_SHORT_COMMENT CASE_DETAILS.Short_Comment%TYPE;
BEGIN
	SELECT Short_History, Short_Comment
	  INTO l_Short_History, l_Short_Comment
	  FROM CASE_DETAILS
	 WHERE Rep_Num = p_Rep_Num;

	UPDATE CASE_DETAILS
	   SET Short_History = p_Short_History,
	   	   Short_Comment = p_Short_Comment
	 WHERE Rep_Num = p_Rep_Num;

	Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.45',l_SHORT_HISTORY,p_SHORT_HISTORY);
    Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.57',l_SHORT_COMMENT,p_SHORT_COMMENT);

END;
--------------------------------------------
PROCEDURE Save_Case_Details_Mgr(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CASE_DETAILS.Rep_Num%TYPE,
  	p_Type_Code IN CASE_DETAILS.Type_Code%TYPE,
  	p_Trial_Num IN CASE_DETAILS.TRIAL_NUM%TYPE,
  	p_Country_Code IN CASE_DETAILS.Country_Code%TYPE,
  	p_Status_Code IN CASE_DETAILS.Status_Code%TYPE,
  	p_Source_Code IN CASE_DETAILS.Source_Code%TYPE,
  	p_M_Code IN CASE_DETAILS.M_Code%TYPE,
  	p_Formulation_Code IN CASE_DETAILS.Formulation_Code%TYPE,
  	p_Unit_Code IN CASE_DETAILS.Unit_Code%TYPE,
  	p_DRegimen_Code IN CASE_DETAILS.Dregimen_Code%TYPE,
  	p_DFreq_Code IN CASE_DETAILS.Dfreq_Code%TYPE,
  	p_Route_Code IN CASE_DETAILS.Route_Code%TYPE,
  	p_Indication_Num IN CASE_DETAILS.Indication_Num%TYPE,
  	p_Sex_Code IN CASE_DETAILS.Sex_Code%TYPE,
  	p_Ethnic_Code IN CASE_DETAILS.Ethnic_Code%TYPE,
  	p_Outcome_Code IN CASE_DETAILS.Outcome_Code%TYPE,
  	p_Product_Code IN CASE_DETAILS.Product_Code%TYPE,
  	p_Phy_Assessment_Code IN CASE_DETAILS.Phy_Assessment_Code%TYPE,
  	p_Co_Assessment_Code IN CASE_DETAILS.Co_Assessment_Code%TYPE,
  	p_Result_Code IN CASE_DETAILS.Result_Code%TYPE,
  	p_Subm_Company_Nm IN CASE_DETAILS.Subm_Company_Nm%TYPE,
  	p_Subm_LHA_Code IN CASE_DETAILS.Subm_Lha_Code%TYPE,
  	p_Date_Entry IN CASE_DETAILS.Date_Entry%TYPE,
  	p_Recv_By_At_Hq IN CASE_DETAILS.Recv_By_At_Hq%TYPE,
  	p_Date_Recv IN CASE_DETAILS.Date_Recv%TYPE,
  	p_Rep_Num_Local IN CASE_DETAILS.Rep_Num_Local%TYPE,
  	p_Rep_Xref_Num IN CASE_DETAILS.Rep_Xref_Num%TYPE,
  	p_Clin_Rep_Info IN CASE_DETAILS.Clin_Rep_Info%TYPE,
  	p_Comp_Rep_Info IN CASE_DETAILS.Comp_Rep_Info%TYPE,
  	p_Patient_Num IN CASE_DETAILS.Patient_Num%TYPE,
  	p_Hosp_Nm IN CASE_DETAILS.Hosp_Nm%TYPE,
  	p_Batch_Num IN CASE_DETAILS.Batch_Num%TYPE,
  	p_Dose IN CASE_DETAILS.Dose%TYPE,
  	p_Treat_Start IN CASE_DETAILS.Treat_Start%TYPE,
  	p_Treat_Stop IN CASE_DETAILS.Treat_Stop%TYPE,
  	p_Rechallenge IN CASE_DETAILS.Rechallenge%TYPE,
  	p_Authorization_No IN CASE_DETAILS.Authorization_No%TYPE,
  	p_P_Initials IN CASE_DETAILS.p_Initials%TYPE,
  	p_Date_Of_Birth IN CASE_DETAILS.Date_Of_Birth%TYPE,
  	p_Age_Reported IN CASE_DETAILS.Age_Reported%TYPE,
  	p_Weight IN CASE_DETAILS.Weight%TYPE,
  	p_Height IN CASE_DETAILS.Height%TYPE,
  	p_Pregnancy IN CASE_DETAILS.Pregnancy%TYPE,
  	p_Pregnancy_Week IN CASE_DETAILS.Pregnancy_Week%TYPE,
  	p_Short_History IN CASE_DETAILS.Short_History%TYPE,
  	p_Outcome_Rep_Date IN CASE_DETAILS.Outcome_Rep_Date%TYPE,
  	p_Autopsy IN CASE_DETAILS.Autopsy%TYPE,
  	p_Autopsy_Result IN CASE_DETAILS.Autopsy_Result%TYPE,
  	p_Phy_Assessment_Date IN CASE_DETAILS.Phy_Assessment_Date%TYPE,
  	p_Co_Assessment_Date IN CASE_DETAILS.Co_Assessment_Date%TYPE,
  	p_Phy_Assessment_Reason IN CASE_DETAILS.Phy_Assessment_Reason%TYPE,
  	p_Co_Assessment_Reason IN CASE_DETAILS.Co_Assessment_Reason%TYPE,
  	p_Contributing_Factors IN CASE_DETAILS.Contributing_Factors%TYPE,
  	p_Rechallenge_Date IN CASE_DETAILS.Rechallenge_Date%TYPE,
  	p_Rechallenge_Dose IN CASE_DETAILS.Rechallenge_Dose%TYPE,
  	p_Verbatim IN CASE_DETAILS.Verbatim%TYPE,
  	p_SHORT_COMMENT IN CASE_DETAILS.Short_Comment%TYPE,
  	p_Distbn_Date IN CASE_DETAILS.Distbn_Date%TYPE,
  	p_Submission_Date IN CASE_DETAILS.Submission_Date%TYPE,
  	p_Further_Commn IN CASE_DETAILS.Further_Commn%TYPE,
  	p_AE_Desc IN CASE_DETAILS.AE_Desc%TYPE,
  	p_Is_Suspended IN CASE_DETAILS.Is_Suspended%TYPE)
AS
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN

        INSERT
          INTO CASE_DETAILS
               (Age_Reported, Phy_Assessment_Code, Co_Assessment_Code, Authorization_No, Autopsy,
               Autopsy_Result, Batch_Num, Clin_Rep_Info, Comp_Rep_Info, Contributing_Factors,
               Country_Code, Co_Assessment_Date, Co_Assessment_Reason, Date_Entry, Date_Of_Birth,
               Date_Recv, Dfreq_Code, Subm_Company_Nm, Distbn_Date, Dose, Dregimen_Code, Ethnic_Code,
               Formulation_Code, Further_Commn, Height, Hosp_Nm, Indication_Num, Is_Suspended, M_Code,
               Outcome_Code, Outcome_Rep_Date, Patient_Num, Phy_Assessment_Date, Phy_Assessment_Reason,
               Pregnancy, Pregnancy_Week, Product_Code, P_Initials, Rechallenge_Dose, Rechallenge,
               Rechallenge_Date, Recv_By_At_Hq, Rep_Num, Rep_Num_Local, Rep_Xref_Num,
               Result_Code, Route_Code, Sex_Code, Short_Comment, Short_History, Source_Code,
               Status_Code, Submission_Date, Subm_Lha_Code, Treat_Start, Treat_Stop, TRIAL_NUM,
               Type_Code, Unit_Code, Verbatim, Weight, AE_Desc)
        VALUES (CASE P_Age_Reported WHEN -1 THEN NULL ELSE P_Age_Reported END, P_Phy_Assessment_Code,
                P_Co_Assessment_Code, P_Authorization_No, P_Autopsy, P_Autopsy_Result, P_Batch_Num,
                P_Clin_Rep_Info, P_Comp_Rep_Info, P_Contributing_Factors,
                P_Country_Code, P_Co_Assessment_Date, P_Co_Assessment_Reason, P_Date_Entry, P_Date_Of_Birth,
                P_Date_Recv, P_Dfreq_Code, P_Subm_Company_Nm, P_Distbn_Date, CASE P_Dose WHEN -1 THEN NULL ELSE p_Dose END,
                P_Dregimen_Code, P_Ethnic_Code, P_Formulation_Code, P_Further_Commn,
                CASE P_Height WHEN -1 THEN NULL ELSE P_Height END, P_Hosp_Nm, P_Indication_Num,
                P_Is_Suspended, P_M_Code, P_Outcome_Code, P_Outcome_Rep_Date, P_Patient_Num,
                P_Phy_Assessment_Date, P_Phy_Assessment_Reason, P_Pregnancy,
                CASE P_Pregnancy_Week WHEN -1 THEN NULL ELSE P_Pregnancy_Week END,
                P_Product_Code, P_P_Initials, P_Rechallenge_Dose, P_Rechallenge, P_Rechallenge_Date,
                 P_Recv_By_At_Hq, P_Rep_Num, P_Rep_Num_Local, P_Rep_Xref_Num, P_Result_Code,
                P_Route_Code, P_Sex_Code, P_Short_Comment, P_Short_History, P_Source_Code, P_Status_Code,
                P_Submission_Date, P_Subm_Lha_Code, P_Treat_Start, P_Treat_Stop, P_Trial_Num, P_Type_Code,
                P_Unit_Code, P_Verbatim, CASE P_Weight WHEN -1 THEN NULL ELSE P_Weight END, p_AE_Desc);
        Save_Audit(p_Username,'CASE','I','CSD1',p_Rep_Num,NULL,NULL,NULL);
    END;
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Conc_Medications(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Rep_Num IN CONC_MEDICATIONS.Rep_Num%TYPE,
    p_Product_Code IN CONC_MEDICATIONS.Product_Code%TYPE,
    p_Indication_Num IN CONC_MEDICATIONS.Indication_Num%TYPE,
    p_Treat_Start IN CONC_MEDICATIONS.Treat_Start%TYPE,
    p_Treat_Stop IN CONC_MEDICATIONS.Treat_Stop%TYPE)
AS
    l_Indication_Num CONC_MEDICATIONS.Indication_Num%TYPE;
    l_Treat_Start CONC_MEDICATIONS.Treat_Start%TYPE;
    l_Treat_Stop CONC_MEDICATIONS.Treat_Stop%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO CONC_MEDICATIONS
               (Rep_Num, Product_Code, Indication_Num, Treat_Start, Treat_Stop)
        VALUES (p_Rep_Num,p_Product_Code,p_Indication_Num,p_Treat_Start,p_Treat_Stop);
        Save_Audit(p_Username,'CASE','I','CSD3',p_Rep_Num||','||p_Product_Code,NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN
        BEGIN
            SELECT Indication_Num, Treat_Start, Treat_Stop
              INTO l_Indication_Num, l_Treat_Start, l_Treat_Stop
              FROM CONC_MEDICATIONS
             WHERE Rep_Num = p_Rep_Num
               AND Product_code = p_Product_Code;
            UPDATE CONC_MEDICATIONS
               SET Indication_Num = p_Indication_Num, Treat_Start = p_Treat_Start, Treat_Stop = p_Treat_Stop
              WHERE Rep_Num = p_Rep_Num
                AND Product_code = p_Product_Code;
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.84',l_Indication_Num,p_Indication_Num);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.85',l_Treat_Start,p_Treat_Start);
            Save_Audit(p_Username,'CASE','U','CSD1',p_Rep_Num,
                       'CSD1.86',l_Treat_Stop,p_Treat_Stop);
        END;
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Subm_Company(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Company_Nm IN SUBM_COMPANY.Company_Nm%TYPE,
    p_Country_Code IN SUBM_COMPANY.Country_Code%TYPE,
    p_Is_Valid IN SUBM_COMPANY.Is_Valid%TYPE)
AS
    l_Country_Code SUBM_COMPANY.Country_Code%TYPE;
    l_Is_Valid SUBM_COMPANY.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO SUBM_COMPANY
               (Company_Nm, Country_Code, Is_Valid)
        VALUES (UPPER(p_Company_Nm),p_Country_Code,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL23',UPPER(p_Company_Nm),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Country_Code, Is_Valid
          INTO l_Country_Code, l_Is_Valid
          FROM SUBM_COMPANY
         WHERE Company_Nm = p_Company_Nm;

        UPDATE SUBM_COMPANY
           SET Country_Code = p_Country_Code,
               Is_Valid = p_Is_Valid
          WHERE Company_Nm = p_Company_Nm;

        Save_Audit(p_Username,'CODE','U','CDL23',p_Company_Nm ,
                   'CDL23.4',l_Country_Code,p_Country_Code);
        Save_Audit(p_Username,'CODE','U','CDL23',p_Company_Nm,
                   'CDL23.5',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Dose_Frequency(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_DFreq_Code IN DOSE_FREQUENCY.Dfreq_Code%TYPE,
    p_DFreq_Desc IN DOSE_FREQUENCY.Dfreq_Desc%TYPE,
    p_Is_Valid IN DOSE_FREQUENCY.Is_Valid%TYPE)
AS
    l_DFreq_Desc DOSE_FREQUENCY.DFreq_Desc%TYPE;
    l_Is_Valid DOSE_FREQUENCY.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO DOSE_FREQUENCY
               (DFreq_Code, DFreq_Desc, Is_Valid)
        VALUES (UPPER(p_DFreq_Code),p_DFreq_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL8',UPPER(p_DFreq_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT DFreq_Desc, Is_Valid
          INTO l_DFreq_Desc, l_Is_Valid
          FROM DOSE_FREQUENCY
         WHERE DFreq_Code = p_DFreq_Code;

        UPDATE DOSE_FREQUENCY
           SET DFreq_Desc = p_DFreq_Desc,
               Is_Valid = p_Is_Valid
          WHERE DFreq_Code = p_DFreq_Code;

        Save_Audit(p_Username,'CODE','U','CDL8',p_DFreq_Code ,
                   'CDL8.2',l_DFreq_Desc,p_DFreq_Desc);
        Save_Audit(p_Username,'CODE','U','CDL8',p_DFreq_Code,
                   'CDL8.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Dose_Regimen(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_DRegimen_Code IN DOSE_REGIMEN.Dregimen_Code%TYPE,
    p_DRegimen_Desc IN DOSE_REGIMEN.Dregimen_Desc%TYPE,
    p_Is_Valid IN DOSE_REGIMEN.Is_Valid%TYPE,
    p_No_Of_Dosage IN DOSE_REGIMEN.No_Of_Dosage%TYPE,
    p_Dosage_Interval IN DOSE_REGIMEN.Dosage_Interval%TYPE)
AS
    l_DRegimen_Desc DOSE_REGIMEN.DRegimen_Desc%TYPE;
    l_Is_Valid DOSE_REGIMEN.Is_Valid%TYPE;
    l_No_Of_Dosage DOSE_REGIMEN.No_Of_Dosage%TYPE;
    l_Dosage_Interval DOSE_REGIMEN.Dosage_Interval%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO DOSE_REGIMEN
               (DRegimen_Code, DRegimen_Desc, Is_Valid, No_Of_Dosage, Dosage_Interval)
        VALUES (UPPER(p_DRegimen_Code),p_DRegimen_Desc,p_Is_Valid,p_No_Of_Dosage,p_Dosage_Interval);
        Save_Audit(p_Username,'CODE','I','CDL9',UPPER(p_DRegimen_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT DRegimen_Desc, Is_Valid, No_Of_Dosage, Dosage_Interval
          INTO l_DRegimen_Desc, l_Is_Valid, l_No_Of_Dosage, l_Dosage_Interval
          FROM DOSE_REGIMEN
         WHERE DRegimen_Code = p_DRegimen_Code;

        UPDATE DOSE_REGIMEN
           SET DRegimen_Desc = p_DRegimen_Desc,
               Is_Valid = p_Is_Valid,
               No_Of_Dosage = p_No_Of_Dosage,
               Dosage_Interval = p_Dosage_Interval
          WHERE DRegimen_Code = p_DRegimen_Code;

        Save_Audit(p_Username,'CODE','U','CDL9',p_DRegimen_Code ,
                   'CDL9.2',l_DRegimen_Desc,p_DRegimen_Desc);
        Save_Audit(p_Username,'CODE','U','CDL9',p_DRegimen_Code,
                   'CDL9.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
        Save_Audit(p_Username,'CODE','U','CDL9',p_DRegimen_Code ,
                   'CDL9.4',l_No_Of_Dosage,p_No_Of_Dosage);
        Save_Audit(p_Username,'CODE','U','CDL9',p_DRegimen_Code ,
                   'CDL9.5',l_Dosage_Interval,p_Dosage_Interval);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Dose_Units(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Unit_Code IN DOSE_UNITS.Unit_Code%TYPE,
    p_Unit_Desc IN DOSE_UNITS.Unit_Desc%TYPE,
    p_Is_Valid IN DOSE_UNITS.Is_Valid%TYPE)
AS
    l_Unit_Desc DOSE_UNITS.Unit_Desc%TYPE;
    l_Is_Valid DOSE_UNITS.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO DOSE_UNITS
               (Unit_Code, Unit_Desc, Is_Valid)
        VALUES (UPPER(p_Unit_Code),p_Unit_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL10',UPPER(p_Unit_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Unit_Desc, Is_Valid
          INTO l_Unit_Desc, l_Is_Valid
          FROM DOSE_UNITS
         WHERE Unit_Code = p_Unit_Code;

        UPDATE DOSE_UNITS
           SET Unit_Desc = p_Unit_Desc,
               Is_Valid = p_Is_Valid
          WHERE Unit_Code = p_Unit_Code;

        Save_Audit(p_Username,'CODE','U','CDL10',p_Unit_Code ,
                   'CDL10.2',l_Unit_Desc,p_Unit_Desc);
        Save_Audit(p_Username,'CODE','U','CDL10',p_Unit_Code,
                   'CDL10.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Ethnic_Origin(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Ethnic_Code IN ETHNIC_ORIGIN.Ethnic_Code%TYPE,
    p_Ethnic_Desc IN ETHNIC_ORIGIN.Ethnic_Desc%TYPE,
    p_Is_Valid IN ETHNIC_ORIGIN.Is_Valid%TYPE)
AS
    l_Ethnic_Desc ETHNIC_ORIGIN.Ethnic_Desc%TYPE;
    l_Is_Valid ETHNIC_ORIGIN.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO ETHNIC_ORIGIN
               (Ethnic_Code, Ethnic_Desc, Is_Valid)
        VALUES (UPPER(p_Ethnic_Code),p_Ethnic_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL11',UPPER(p_Ethnic_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Ethnic_Desc, Is_Valid
          INTO l_Ethnic_Desc, l_Is_Valid
          FROM ETHNIC_ORIGIN
         WHERE Ethnic_Code = p_Ethnic_Code;

        UPDATE ETHNIC_ORIGIN
           SET Ethnic_Desc = p_Ethnic_Desc,
               Is_Valid = p_Is_Valid
          WHERE Ethnic_Code = p_Ethnic_Code;

        Save_Audit(p_Username,'CODE','U','CDL11',p_Ethnic_Code,
                   'CDL11.5',l_Ethnic_Desc,p_Ethnic_Desc);
        Save_Audit(p_Username,'CODE','U','CDL11',p_Ethnic_Code,
                   'CDL11.6',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Event_Severity(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_ESeverity_Code IN EVENT_SEVERITY.Eseverity_Code%TYPE,
    p_ESeverity_Desc IN EVENT_SEVERITY.Eseverity_Desc%TYPE,
    p_Is_Valid IN EVENT_SEVERITY.Is_Valid%TYPE)
AS
    l_ESeverity_Desc EVENT_SEVERITY.ESeverity_Desc%TYPE;
    l_Is_Valid EVENT_SEVERITY.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO EVENT_SEVERITY
               (ESeverity_Code, ESeverity_Desc, Is_Valid)
        VALUES (UPPER(p_ESeverity_Code),p_ESeverity_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL12',UPPER(p_ESeverity_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT ESeverity_Desc, Is_Valid
          INTO l_ESeverity_Desc, l_Is_Valid
          FROM EVENT_SEVERITY
         WHERE ESeverity_Code = p_ESeverity_Code;

        UPDATE EVENT_SEVERITY
           SET ESeverity_Desc = p_ESeverity_Desc,
               Is_Valid = p_Is_Valid
          WHERE ESeverity_Code = p_ESeverity_Code;

        Save_Audit(p_Username,'CODE','U','CDL12',p_ESeverity_Code,
                   'CDL12.2',l_ESeverity_Desc,p_ESeverity_Desc);
        Save_Audit(p_Username,'CODE','U','CDL12',p_ESeverity_Code,
                   'CDL12.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Formulation(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Formulation_Code IN FORMULATION.Formulation_Code%TYPE,
    p_Formulation_Desc IN FORMULATION.Formulation_Desc%TYPE,
    p_Is_Valid IN FORMULATION.Is_Valid%TYPE)
AS
    l_Formulation_Desc FORMULATION.Formulation_Desc%TYPE;
    l_Is_Valid FORMULATION.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO FORMULATION
               (Formulation_Code, Formulation_Desc, Is_Valid)
        VALUES (UPPER(p_Formulation_Code),p_Formulation_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL13',UPPER(p_Formulation_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Formulation_Desc, Is_Valid
          INTO l_Formulation_Desc, l_Is_Valid
          FROM FORMULATION
         WHERE Formulation_Code = p_Formulation_Code;

        UPDATE FORMULATION
           SET Formulation_Desc = p_Formulation_Desc,
               Is_Valid = p_Is_Valid
          WHERE Formulation_Code = p_Formulation_Code;

        Save_Audit(p_Username,'CODE','U','CDL13',p_Formulation_Code ,
                   'CDL13.5',l_Formulation_Desc,p_Formulation_Desc);
        Save_Audit(p_Username,'CODE','U','CDL13',p_Formulation_Code,
                   'CDL13.6',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Indication_Treatment(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Indication_Num IN INDICATION_TREATMENT.Indication_Num%TYPE,
    p_Indication IN INDICATION_TREATMENT.Indication%TYPE,
    p_Is_Valid IN INDICATION_TREATMENT.Is_Valid%TYPE)
AS
    l_Indication INDICATION_TREATMENT.Indication%TYPE;
    l_Is_Valid INDICATION_TREATMENT.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO INDICATION_TREATMENT
               (Indication_Num, Indication, Is_Valid)
        VALUES (UPPER(p_Indication_Num),p_Indication,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL14',UPPER(p_Indication_Num),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Indication, Is_Valid
          INTO l_Indication, l_Is_Valid
          FROM INDICATION_TREATMENT
         WHERE Indication_Num = p_Indication_Num;

        UPDATE INDICATION_TREATMENT
           SET Indication = p_Indication,
               Is_Valid = p_Is_Valid
          WHERE Indication_Num = p_Indication_Num;

        Save_Audit(p_Username,'CODE','U','CDL14',p_Indication_Num ,
                   'CDL14.2',l_Indication,p_Indication);
        Save_Audit(p_Username,'CODE','U','CDL14',p_Indication_Num,
                   'CDL14.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Inventory(
    p_Save_Flag IN CHAR,
    p_inv_id IN INVENTORY.Inv_Id%TYPE,
    p_inv_desc IN INVENTORY.Inv_Desc%TYPE,
    p_inv_type IN INVENTORY.Inv_Type%TYPE,
    p_ins_audit_flag IN INVENTORY.Ins_Audit_Flag%TYPE,
    p_del_audit_flag IN INVENTORY.Del_Audit_Flag%TYPE)
AS
BEGIN
  IF p_Save_Flag = 'I' THEN
  BEGIN
    INSERT
      INTO INVENTORY
           (Inv_Id, Inv_Desc, Inv_Type, Ins_Audit_Flag, Del_Audit_Flag)
    VALUES (UPPER(p_inv_id), p_inv_desc, p_inv_type, p_ins_audit_flag, p_del_audit_flag);
  END;
  ELSIF p_Save_Flag = 'U' THEN
  BEGIN
    UPDATE INVENTORY
       SET Ins_Audit_Flag = p_ins_audit_flag,
           Del_Audit_Flag = p_del_audit_flag
     WHERE Inv_Id = p_inv_id;
  END;
  END IF;
END;
--------------------------------------------
PROCEDURE Save_Manufacturer(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_M_Code IN MANUFACTURER.M_Code%TYPE,
    p_M_Name IN MANUFACTURER.M_Name%TYPE,
    p_M_Addr1 IN MANUFACTURER.M_Addr1%TYPE,
    p_M_Addr2 IN MANUFACTURER.M_Addr2%TYPE,
    p_M_Addr3 IN MANUFACTURER.M_Addr3%TYPE,
    p_M_Addr4 IN MANUFACTURER.M_Addr4%TYPE,
    p_M_Phone IN MANUFACTURER.M_Phone%TYPE,
    p_M_Fax IN MANUFACTURER.M_Fax%TYPE,
    p_M_Email IN MANUFACTURER.M_Email%TYPE,
    p_Is_Valid IN MANUFACTURER.Is_Valid%TYPE)
AS
    l_M_Name MANUFACTURER.M_Name%TYPE;
    l_M_Addr1 MANUFACTURER.M_Addr1%TYPE;
    l_M_Addr2 MANUFACTURER.M_Addr2%TYPE;
    l_M_Addr3 MANUFACTURER.M_Addr3%TYPE;
    l_M_Addr4 MANUFACTURER.M_Addr4%TYPE;
    l_M_Phone MANUFACTURER.M_Phone%TYPE;
    l_M_Fax MANUFACTURER.M_Fax%TYPE;
    l_M_Email MANUFACTURER.M_Email%TYPE;
    l_Is_Valid MANUFACTURER.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO MANUFACTURER
               (M_Code, M_Name, Is_Valid, M_Addr1, M_Addr2, M_Addr3, M_Addr4, M_Phone, M_Fax, M_Email)
        VALUES (UPPER(p_M_Code),p_M_Name,p_Is_Valid,p_M_Addr1,p_M_Addr2,p_M_Addr3,p_M_Addr4,p_M_Phone,p_M_Fax,p_M_Email);
        Save_Audit(p_Username,'CODE','I','CDL6',UPPER(p_M_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT M_Email,M_Fax,M_Phone,M_Addr4,M_Addr3,M_Addr2,M_Addr1,M_Name,Is_Valid
          INTO l_M_Email, l_M_Fax,l_M_Phone, l_M_Addr4,l_M_Addr3,l_M_Addr2,l_M_Addr1,l_M_Name, l_Is_Valid
          FROM MANUFACTURER
         WHERE M_Code = p_M_Code;

        UPDATE MANUFACTURER
           SET M_Email = p_M_Email,
               m_Fax = p_M_Fax,
               m_Phone = p_M_Phone,
               m_addr4 = p_M_Addr4,
               M_Addr3 = p_M_Addr3,
               M_Addr2 = p_M_Addr2,
               M_Addr1 = p_M_Addr1,
               M_Name = p_M_Name,
               Is_Valid = p_Is_Valid
          WHERE M_Code = p_M_Code;

        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.9',l_M_Email,p_M_Email);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.8',l_M_Fax,p_M_Fax);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.7',l_M_Phone,p_M_Phone);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.6',l_M_Addr4,p_M_addr4);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.5',l_M_Addr3,p_M_addr3);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.4',l_M_Addr2,p_M_addr2);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.3',l_M_Addr1,p_M_addr1);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code ,
                   'CDL6.2',l_M_Name,p_M_Name);
        Save_Audit(p_Username,'CODE','U','CDL6',p_M_Code,
                   'CDL6.10',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Product(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Product_Code IN PRODUCT.Product_Code%TYPE,
    p_Generic_Nm IN PRODUCT.Generic_Nm%TYPE,
    p_Brand_Nm IN PRODUCT.Brand_Nm%TYPE,
    p_Is_Valid IN PRODUCT.Is_Valid%TYPE)
AS
    l_Generic_Nm PRODUCT.Generic_Nm%TYPE;
    l_Brand_Nm PRODUCT.Brand_Nm%TYPE;
    l_Is_Valid PRODUCT.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO PRODUCT
               (Product_Code, Generic_Nm, Is_Valid, Brand_Nm)
        VALUES (UPPER(p_Product_Code),p_Generic_Nm,p_Is_Valid,p_Brand_Nm);
        Save_Audit(p_Username,'CODE','I','CDL16',UPPER(p_Product_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Generic_Nm, Brand_Nm, Is_Valid
          INTO l_Generic_Nm, l_Brand_Nm, l_Is_Valid
          FROM PRODUCT
         WHERE Product_Code = p_Product_Code;

        UPDATE PRODUCT
           SET Generic_Nm = p_Generic_Nm,
               Brand_Nm = p_Brand_Nm,
               Is_Valid = p_Is_Valid
          WHERE Product_Code = p_Product_Code;

        Save_Audit(p_Username,'CODE','U','CDL16',p_Product_Code ,
                   'CDL16.2',l_Generic_Nm,p_Generic_Nm);
        Save_Audit(p_Username,'CODE','U','CDL16',p_Product_Code ,
                   'CDL16.3',l_Brand_Nm,p_Brand_Nm);
        Save_Audit(p_Username,'CODE','U','CDL16',p_Product_Code,
                   'CDL16.4',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Rechallenge_Result(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Result_Code IN RECHALLENGE_RESULT.Result_Code%TYPE,
    p_Result_Desc IN RECHALLENGE_RESULT.Result_Desc%TYPE,
    p_Is_Valid IN RECHALLENGE_RESULT.Is_Valid%TYPE)
AS
    l_Result_Desc RECHALLENGE_RESULT.Result_Desc%TYPE;
    l_Is_Valid RECHALLENGE_RESULT.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO RECHALLENGE_RESULT
               (Result_Code, Result_Desc, Is_Valid)
        VALUES (UPPER(p_Result_Code),p_Result_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL17',UPPER(p_Result_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Result_Desc, Is_Valid
          INTO l_Result_Desc, l_Is_Valid
          FROM RECHALLENGE_RESULT
         WHERE Result_Code = p_Result_Code;

        UPDATE RECHALLENGE_RESULT
           SET Result_Desc = p_Result_Desc,
               Is_Valid = p_Is_Valid
          WHERE Result_Code = p_Result_Code;

        Save_Audit(p_Username,'CODE','U','CDL17',p_Result_Code ,
                   'CDL17.2',l_Result_Desc,p_Result_Desc);
        Save_Audit(p_Username,'CODE','U','CDL17',p_Result_Code,
                   'CDL17.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Reporter_Stat(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Status_Code IN REPORTER_STAT.Status_Code%TYPE,
    p_Status_Desc IN REPORTER_STAT.Status_Desc%TYPE,
    p_Is_Valid IN REPORTER_STAT.Is_Valid%TYPE)
AS
    l_Status_Desc REPORTER_STAT.Status_Desc%TYPE;
    l_Is_Valid REPORTER_STAT.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO REPORTER_STAT
               (Status_Code, Status_Desc, Is_Valid)
        VALUES (UPPER(p_Status_Code),p_Status_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL20',UPPER(p_Status_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Status_Desc, Is_Valid
          INTO l_Status_Desc, l_Is_Valid
          FROM REPORTER_STAT
         WHERE Status_Code = p_Status_Code;

        UPDATE REPORTER_STAT
           SET Status_Desc = p_Status_Desc,
               Is_Valid = p_Is_Valid
          WHERE Status_Code = p_Status_Code;

        Save_Audit(p_Username,'CODE','U','CDL20',p_Status_Code ,
                   'CDL20.2',l_Status_Desc,p_Status_Desc);
        Save_Audit(p_Username,'CODE','U','CDL20',p_Status_Code,
                   'CDL20.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Rep_Type(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Type_Code IN REP_TYPE.Type_Code%TYPE,
    p_Type_Desc IN REP_TYPE.Type_Desc%TYPE,
    p_Is_Valid IN REP_TYPE.Is_Valid%TYPE)
AS
    l_Type_Desc REP_TYPE.Type_Desc%TYPE;
    l_Is_Valid REP_TYPE.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO REP_TYPE
               (Type_Code, Type_Desc, Is_Valid)
        VALUES (UPPER(p_Type_Code),p_Type_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL18',UPPER(p_Type_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Type_Desc, Is_Valid
          INTO l_Type_Desc, l_Is_Valid
          FROM REP_TYPE
         WHERE Type_Code = p_Type_Code;

        UPDATE REP_TYPE
           SET Type_Desc = p_Type_Desc,
               Is_Valid = p_Is_Valid
          WHERE Type_Code = p_Type_Code;

        Save_Audit(p_Username,'CODE','U','CDL18',p_Type_Code ,
                   'CDL18.2',l_Type_Desc,p_Type_Desc);
        Save_Audit(p_Username,'CODE','U','CDL18',p_Type_Code,
                   'CDL18.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);

    END IF;
END;
--------------------------------------------
PROCEDURE Save_Route(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Route_Code IN ROUTE.Route_Code%TYPE,
    p_Route_Desc IN ROUTE.Route_Desc%TYPE,
    p_Is_Valid IN ROUTE.Is_Valid%TYPE)
AS
    l_Route_Desc ROUTE.Route_Desc%TYPE;
    l_Is_Valid ROUTE.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO ROUTE
               (Route_Code, Route_Desc, Is_Valid)
        VALUES (UPPER(p_Route_Code),p_Route_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL25',UPPER(p_Route_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Route_Desc, Is_Valid
          INTO l_Route_Desc, l_Is_Valid
          FROM ROUTE
         WHERE Route_Code = p_Route_Code;

        UPDATE ROUTE
           SET Route_Desc = p_Route_Desc,
               Is_Valid = p_Is_Valid
          WHERE Route_Code = p_Route_Code;

        Save_Audit(p_Username,'CODE','U','CDL25',p_Route_Code ,
                   'CDL25.2',l_Route_Desc,p_Route_Desc);
        Save_Audit(p_Username,'CODE','U','CDL25',p_Route_Code,
                   'CDL25.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Seriousness(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Classification_Code IN CLASSIFICATION_SERIOUSNESS.Classification_Code%TYPE,
    p_Classification_Desc IN CLASSIFICATION_SERIOUSNESS.Classification_Desc%TYPE,
    p_Is_Valid IN CLASSIFICATION_SERIOUSNESS.Is_Valid%TYPE)
AS
    l_Classification_Desc CLASSIFICATION_SERIOUSNESS.Classification_Desc%TYPE;
    l_Is_Valid CLASSIFICATION_SERIOUSNESS.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO CLASSIFICATION_SERIOUSNESS
               (Classification_Code, Classification_Desc, Is_Valid)
        VALUES (UPPER(p_Classification_Code),p_Classification_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL5',UPPER(p_Classification_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

            SELECT Classification_Desc, Is_Valid
              INTO l_Classification_Desc, l_Is_Valid
              FROM CLASSIFICATION_SERIOUSNESS
             WHERE Classification_Code = p_Classification_Code;

            UPDATE CLASSIFICATION_SERIOUSNESS
               SET Classification_Desc = p_Classification_Desc,
                   Is_Valid = p_Is_Valid
              WHERE Classification_Code = p_Classification_Code;

            Save_Audit(p_Username,'CODE','U','CDL5',p_Classification_Code,
                       'CDL5.2',l_Classification_Desc,p_Classification_Desc);
            Save_Audit(p_Username,'CODE','U','CDL5',p_Classification_Code,
                       'CDL5.3',
                       CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                       CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Sex(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Sex_Code IN SEX.Sex_Code%TYPE,
    p_Sex_Desc IN SEX.Sex_Desc%TYPE,
    p_Is_Valid IN SEX.Is_Valid%TYPE)
AS
    l_Sex_Desc SEX.Sex_Desc%TYPE;
    l_Is_Valid SEX.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO SEX
               (Sex_Code, Sex_Desc, Is_Valid)
        VALUES (UPPER(p_Sex_Code),p_Sex_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL21',UPPER(p_Sex_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Sex_Desc, Is_Valid
          INTO l_Sex_Desc, l_Is_Valid
          FROM SEX
         WHERE Sex_Code = p_Sex_Code;

        UPDATE SEX
           SET Sex_Desc = p_Sex_Desc,
               Is_Valid = p_Is_Valid
          WHERE Sex_Code = p_Sex_Code;

        Save_Audit(p_Username,'CODE','U','CDL21',p_Sex_Code ,
                   'CDL21.2',l_Sex_Desc,p_Sex_Desc);
        Save_Audit(p_Username,'CODE','U','CDL21',p_Sex_Code,
                   'CDL21.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Source_Info(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Source_Code IN SOURCE_INFO.Source_Code%TYPE,
    p_Source_Desc IN SOURCE_INFO.Source_Desc%TYPE,
    p_Is_Valid IN SOURCE_INFO.Is_Valid%TYPE)
AS
    l_Source_Desc SOURCE_INFO.Source_Desc%TYPE;
    l_Is_Valid SOURCE_INFO.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO SOURCE_INFO
               (Source_Code, Source_Desc, Is_Valid)
        VALUES (UPPER(p_Source_Code),p_Source_Desc,p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL19',UPPER(p_Source_Code),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Source_Desc, Is_Valid
          INTO l_Source_Desc, l_Is_Valid
          FROM SOURCE_INFO
         WHERE Source_Code = p_Source_Code;

        UPDATE SOURCE_INFO
           SET Source_Desc = p_Source_Desc,
               Is_Valid = p_Is_Valid
          WHERE Source_Code = p_Source_Code;

        Save_Audit(p_Username,'CODE','U','CDL19',p_Source_Code ,
                   'CDL19.2',l_Source_Desc,p_Source_Desc);
        Save_Audit(p_Username,'CODE','U','CDL19',p_Source_Code,
                   'CDL19.3',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Save_Trial_Num(
    p_Save_Flag IN CHAR ,
    p_Username IN USER_LIST.Username%TYPE,
    p_Trial_Num IN TRIAL_NUM.TRIAL_NUM%TYPE,
    p_Is_Valid IN TRIAL_NUM.Is_Valid%TYPE)
AS
    l_Is_Valid TRIAL_NUM.Is_Valid%TYPE;
BEGIN
    IF p_Save_Flag = 'I' THEN
    BEGIN
        INSERT
          INTO TRIAL_NUM
               (TRIAL_NUM, Is_Valid)
        VALUES (UPPER(p_Trial_Num), p_Is_Valid);
        Save_Audit(p_Username,'CODE','I','CDL22',UPPER(p_Trial_Num),NULL,NULL,NULL);
    END;
    ELSIF p_Save_Flag = 'U' THEN

        SELECT Is_Valid
          INTO l_Is_Valid
          FROM TRIAL_NUM
         WHERE TRIAL_NUM = p_Trial_Num;

        UPDATE TRIAL_NUM
           SET Is_Valid = p_Is_Valid
          WHERE TRIAL_NUM = p_Trial_Num;

        Save_Audit(p_Username,'CODE','U','CDL22',p_Trial_Num,
                   'CDL22.2',
                   CASE l_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END,
                   CASE p_Is_Valid WHEN 1 THEN 'Active' WHEN 0 THEN 'Inactive' END);
    END IF;
END;
--------------------------------------------
PROCEDURE Sumtab_Label(
    p_Product    IN CASE_DETAILS.PRODUCT_CODE%TYPE,
    p_Frm_Date   IN VARCHAR2,
    p_To_Date    IN VARCHAR2,
    p_Status     IN Case_Details.is_Suspended%TYPE DEFAULT NULL,
    p_curSumtabL OUT SYS_REFCURSOR)
     AS
  BEGIN
   OPEN p_curSumtabL FOR
SELECT vae.Body_Sys_Num, vae.Body_System, vae.Diag_Code, vae.Diag_Desc,
       SUM(CASE WHEN vae.Labelling='U' AND vae.Classification_Code IS NOT NULL THEN 1 ELSE 0 END) "UL_SEV",
       SUM(CASE WHEN vae.Labelling='E' AND vae.Classification_Code IS NOT NULL THEN 1 ELSE 0 END) "L_SEV",
       SUM(CASE WHEN vae.Labelling='U' AND vae.Classification_Code IS NULL THEN 1 ELSE 0 END) "UL_NON_SEV",
       SUM(CASE WHEN vae.Labelling='E' AND vae.Classification_Code IS NULL THEN 1 ELSE 0 END) "L_NON_SEV",
       SUM(CASE WHEN vae.Labelling IN ('U','E') THEN 1 ELSE 0 END) "TOTAL"
  FROM v_Adverse_Event vae, v_Case_Details vcd
 WHERE vae.Rep_Num = vcd.Rep_Num
   AND (p_Frm_Date IS NULL OR vcd.Date_Recv >= TO_DATE(p_Frm_Date,'DD-MON-YYYY'))
   AND (p_To_Date IS NULL OR vcd.Date_Recv <= TO_DATE(p_To_Date,'DD-MON-YYYY'))
   AND (p_Product IS NULL OR vcd.Product_Code = p_Product)
   AND (p_Status = -1 OR vcd.Is_Suspended = p_Status)
 GROUP BY (vae.Body_Sys_Num, vae.Body_System, vae.Diag_Code, vae.Diag_Desc)
HAVING vae.Body_Sys_Num IS NOT NULL
   AND vae.Body_System IS NOT NULL
   AND vae.Diag_Code IS NOT NULL AND vae.Diag_Desc IS NOT NULL
 ORDER BY vae.Body_Sys_Num,vae.Diag_Code;
   END ;
--------------------------------------------
PROCEDURE Sumtab_RType(
    p_Product    IN CASE_DETAILS.PRODUCT_CODE%TYPE,
    p_Frm_Date   IN VARCHAR2,
    p_To_Date    IN VARCHAR2,
    p_Status     IN Case_Details.is_Suspended%TYPE DEFAULT NULL,
    p_curSumtabR OUT SYS_REFCURSOR)
     AS
  BEGIN
   OPEN p_curSumtabR FOR
SELECT vae.Body_Sys_Num, vae.Body_System,vae.Diag_Code, vae.Diag_Desc,
       SUM(CASE WHEN vcd.Type_Code='C' THEN 1 ELSE 0 END) "CLINICAL_TRIAL",
       SUM(CASE WHEN vcd.Type_Code='1' THEN 1 ELSE 0 END) "SPONTANEOUS",
       SUM(CASE WHEN vcd.Type_Code='R' THEN 1 ELSE 0 END) "REGULATORY",
       SUM(CASE WHEN vcd.Type_Code='2' THEN 1 ELSE 0 END) "LITERATURE",
       SUM(CASE WHEN vcd.Type_Code NOT IN ('C','1','R','2') OR vcd.Type_Code IS NULL THEN 1 ELSE 0 END) "OTHERS",
       SUM(CASE WHEN vcd.Type_Code IS NOT NULL THEN 1 ELSE 0 END) "TOTAL"
  FROM v_Adverse_Event vae, v_Case_Details vcd
 WHERE vae.Rep_Num = vcd.Rep_Num
   AND (p_Frm_Date IS NULL OR vcd.Date_Recv >= TO_DATE(p_Frm_Date,'DD-MON-YYYY'))
   AND (p_To_Date IS NULL OR vcd.Date_Recv <= TO_DATE(p_To_Date,'DD-MON-YYYY'))
   AND (p_Product IS NULL OR vcd.Product_Code = p_Product)
   AND (p_Status = -1 OR vcd.Is_Suspended = p_Status)
 GROUP BY (vae.Body_Sys_Num, vae.Body_System,vae.Diag_Code, vae.Diag_Desc)
HAVING vae.Body_Sys_Num IS NOT NULL
   AND vae.Body_System IS NOT NULL
   AND vae.Diag_Code IS NOT NULL AND vae.Diag_Desc IS NOT NULL
 ORDER BY vae.Body_Sys_Num, vae.Diag_Code;
   END ;
--------------------------------------------
PROCEDURE Cioms2_Proc (
          p_Product   IN  Case_Details.Product_Code%TYPE DEFAULT NULL,
          p_Frm_Date  IN  VARCHAR2 DEFAULT NULL,
          p_To_Date   IN  VARCHAR2 DEFAULT NULL,
          p_Status    IN  Case_Details.is_Suspended%TYPE DEFAULT NULL,
          p_curCioms2 OUT SYS_REFCURSOR)
       AS
    BEGIN
            OPEN p_curCioms2 FOR
          SELECT vae.Body_Sys_Num, vae.Body_System, vcd.Rep_Num, vcd.Country_Nm, vcd.Source_Desc, vcd.Sex_Code,vcd.Sex_Desc,
                 vcd.Age, vcd.Unit_Code, vcd.Duration, vcd.Outcome,vcd.Dose,
                 vae.Llt_Code, (CASE Aggr_Of_LLT WHEN 'Y' THEN 'Aggr. of ' || vae.Llt_Desc ELSE vae.Llt_Desc END) as LLT_Desc,
                 vae.Diag_Code, (CASE Aggr_Of_Diag WHEN 'Y' THEN 'Aggr. of ' || vae.Diag_Desc ELSE vae.Diag_Desc END) as Diag_Desc,
                 vae.Diag_Desc, vae.Classification_Code,
                 vae.Labelling, vcd.Short_Comment
            FROM V_Case_Details vcd, V_Adverse_Event vae
           WHERE vcd.Rep_Num = vae.Rep_Num(+)
             AND (p_Frm_Date IS NULL OR vcd.Date_Recv >= TO_DATE(p_Frm_Date,'DD-MON-YYYY'))
             AND (p_To_Date IS NULL OR vcd.Date_Recv <= TO_DATE(p_To_Date,'DD-MON-YYYY'))
             AND (p_Product IS NULL OR vcd.Product_Code = p_Product)
             AND (p_Status = -1 OR vcd.Is_Suspended = p_Status)
           ORDER BY vae.Body_Sys_Num, vcd.Rep_Num, vae.Ae_Order;
      END;
--------------------------------------------
PROCEDURE ICH_Proc (
          p_Product   IN Case_Details.Product_Code%TYPE DEFAULT NULL,
          p_Frm_Date  IN VARCHAR2 DEFAULT NULL,
          p_To_Date   IN VARCHAR2 DEFAULT NULL,
          p_Status    IN Case_Details.is_Suspended%TYPE DEFAULT NULL,
          p_curICH   OUT SYS_REFCURSOR)
       AS
    BEGIN
            OPEN p_curICH FOR
          SELECT vae.Body_Sys_Num, vae.Body_System, vcd.Rep_Num, vcd.Country_Nm, vcd.source_desc,
                 vcd.Sex_Code, vcd.AGE, vcd.Unit_Code, to_char(vcd.Treat_Start,'DD_MON-YYYY') as Treat_Start, to_Char(vcd.Treat_Stop,'DD-MON-YYYY') as Treat_Stop, vcd.Duration,
                 vcd.Outcome, to_char(vcd.Date_Recv,'DD-MON-YYYY') as Date_Recv, vae.Classification_Code, vae.Labelling, vcd.Short_Comment,
                 vae.Llt_Code, (CASE Aggr_Of_LLT WHEN 'Y' THEN 'Aggr. of ' || vae.Llt_Desc ELSE vae.Llt_Desc END) as LLT_Desc,
                 vae.Diag_Code, (CASE Aggr_Of_Diag WHEN 'Y' THEN 'Aggr. of ' || vae.Diag_Desc ELSE vae.Diag_Desc END) as Diag_Desc
            FROM v_Case_Details VCD, v_Adverse_Event vae
           WHERE vcd.Rep_Num = vae.Rep_Num(+)
             AND (p_Frm_Date IS NULL OR vcd.Date_Recv >= TO_DATE(p_Frm_Date,'DD-MON-YYYY'))
             AND (p_To_Date IS NULL OR vcd.Date_Recv <= TO_DATE(p_To_Date,'DD-MON-YYYY'))
             AND (p_Product IS NULL OR vcd.Product_Code = p_Product)
             AND (p_Status = -1 OR vcd.Is_Suspended = p_Status)
           ORDER BY vae.Body_Sys_Num, vcd.Rep_Num, vae.Ae_Order;
      END;
--------------------------------------------
PROCEDURE Query_Tool_Proc (
    p_Product_Code        IN CASE_DETAILS.PRODUCT_CODE%TYPE,
    p_Status_Code         IN CASE_DETAILS.STATUS_CODE%TYPE,
    p_Country_Code        IN CASE_DETAILS.COUNTRY_CODE%TYPE,
    p_Source_Code         IN CASE_DETAILS.SOURCE_CODE%TYPE,
    p_Classification_Code IN CLASSIFICATION_SERIOUSNESS.CLASSIFICATION_CODE%TYPE,
    p_Unit_Code           IN DOSE_UNITS.UNIT_CODE%TYPE,
    p_Dose                IN CASE_DETAILS.DOSE%TYPE,
    p_Indication_Num      IN INDICATION_TREATMENT.INDICATION_NUM%TYPE,
    p_Llt_Code            IN AE_LLT.LLT_CODE%TYPE,
    p_Outcome_Code        IN AE_OUTCOME.OUTCOME_CODE%TYPE,
    p_Diag_Code           IN AE_DIAG.DIAG_CODE%TYPE,
    p_Co_Assessment_Code  IN ASSESSMENT_CAUSALITY.ASSESSMENT_CODE%TYPE,
    p_Body_Sys_Num        IN BODY_SYSTEM.BODY_SYS_NUM%TYPE,
    p_Sex_Code            IN SEX.SEX_CODE%TYPE,
    p_Phy_Assessment_Code IN ASSESSMENT_CAUSALITY.ASSESSMENT_CODE%TYPE,
    p_M_Code              IN MANUFACTURER.M_CODE%TYPE,
    p_Patient_Num         IN CASE_DETAILS.PATIENT_NUM%TYPE,
    p_Frm_Rep_num         IN CASE_DETAILS.REP_NUM%TYPE,
    p_To_Rep_Num          IN CASE_DETAILS.REP_NUM%TYPE,
    p_Frm_Age             IN NUMBER,
    p_To_Age              IN NUMBER,
    p_Frm_Treat           IN NUMBER,
    p_To_Treat            IN NUMBER,
    p_Frm_Event           IN NUMBER,
    p_To_Event            IN NUMBER,
    p_Frm_Date_Recv       IN VARCHAR2,
    p_to_Date_Recv        IN VARCHAR2,
    p_Status              IN Case_Details.Is_Suspended%TYPE,
    p_CurQTool           OUT SYS_REFCURSOR)
     AS
  BEGIN
   OPEN p_CurQTool FOR
 SELECT vcd.Rep_Num, vcd.Brand_Nm, vae.Llt_Code,
        (CASE Aggr_Of_LLT WHEN 'Y' THEN 'Aggr. of ' || vae.Llt_Desc ELSE vae.Llt_Desc END) as LLT_Desc,
        vae.Diag_Code,
        (CASE Aggr_Of_Diag WHEN 'Y' THEN 'Aggr. of ' || vae.Diag_Desc ELSE vae.Diag_Desc END) as Diag_Desc,
        vcd.Country_Nm, vcd.Sex_Code, vcd.Age, vcd.Duration, vcd.Phy_Assessment_Code, vcd.Co_Assessment_Code,
        vcd.Outcome
   FROM V_Case_Details vcd, V_Adverse_Event vae
  WHERE vcd.Rep_Num = vae.Rep_Num(+)
    AND (p_Product_Code IS NULL OR vcd.Product_Code = p_Product_Code)
    AND (p_Status_Code IS NULL OR vcd.Status_Code = p_Status_Code)
    AND (p_Country_Code IS NULL OR vcd.Country_Code = p_Country_Code)
    AND (p_Source_Code IS NULL OR vcd.Source_Code = p_Source_Code)
    AND (p_Classification_Code IS NULL OR vae.Classification_Code = p_Classification_Code)
    AND (p_Unit_Code IS NULL OR vcd.Unit_Code = p_Unit_Code)
    AND (p_Dose = -1 OR vcd.Dose = p_Dose)
    AND (p_Indication_Num IS NULL OR vcd.Indication_Num = p_Indication_Num)
    AND (p_Llt_Code IS NULL OR vae.Llt_Code = p_Llt_Code)
    AND (p_Outcome_Code IS NULL OR vcd.Outcome_Code = p_Outcome_Code)
    AND (p_Diag_Code IS NULL OR vae.Diag_Code = p_Diag_Code)
    AND (p_Co_Assessment_Code IS NULL OR vcd.Co_Assessment_Code = p_Co_Assessment_Code)
    AND (p_Body_Sys_Num IS NULL OR vae.Body_Sys_Num = p_Body_Sys_Num)
    AND (p_Sex_Code IS NULL OR vcd.Sex_Code = p_Sex_Code)
    AND (p_Phy_Assessment_Code IS NULL OR vcd.Phy_Assessment_Code = p_Phy_Assessment_Code)
    AND (p_M_Code IS NULL OR vcd.M_Code = p_M_Code)
    AND (p_Patient_Num IS NULL OR vcd.Patient_Num = p_Patient_Num)
    AND (p_Frm_Rep_num = -1 OR vcd.Rep_Num >= p_Frm_Rep_num)
    AND (p_To_Rep_Num = -1 OR vcd.Rep_Num <= p_To_Rep_Num)
    AND (p_Frm_Age = -1 OR VCD.Age >= p_Frm_Age)
    AND (p_To_Age = -1 OR VCD.Age <= p_To_Age)
    AND (p_Frm_Treat = -1 OR vcd.DURATION >= p_Frm_Treat)
    AND (p_To_Treat = -1 OR vcd.DURATION <= p_To_Treat)
    AND (p_Frm_Event = -1 OR vae.EVENT_DURATION >= p_Frm_Event)
    AND (p_To_Event = -1 OR vae.EVENT_DURATION <= p_Frm_Event)
    AND (p_Frm_Date_Recv IS NULL OR vcd.Date_Recv >= TO_DATE(p_Frm_Date_Recv,'DD-MON-YYYY'))
    AND (p_to_Date_Recv IS NULL OR vcd.Date_Recv <= TO_DATE(p_to_Date_Recv,'DD-MON-YYYY'))
    AND (p_Status = -1 OR vcd.Is_Suspended <= p_Status)
  ORDER BY vcd.Rep_Num, vae.Ae_Order;
    END ;
--------------------------------------------
PROCEDURE Update_Inv_Col(
          p_Inv_Col_Id   IN INVENTORY_COLS.INV_COL_ID%TYPE,
          p_Audit_Flag   IN INVENTORY_COLS.AUDIT_FLAG%TYPE)
AS
BEGIN
 UPDATE Inventory_Cols
    SET Audit_Flag = p_Audit_Flag
  WHERE Inv_Col_Id = p_Inv_Col_Id;
END;
--------------------------------------------

---------------------------------------------
-- Delete All Cases
---------------------------------------------
PROCEDURE Del_Cases
 AS
BEGIN
 DELETE FROM Case_Details;
 COMMIT ;
END;
--------------------------------------------

---------------------------------------------
-- Clear Database
---------------------------------------------
PROCEDURE Clear_Db
 AS
BEGIN
 DELETE FROM Case_Details;
 COMMIT;
 DELETE FROM LHA;
 DELETE FROM SUBM_COMPANY;
 DELETE FROM AE_LLT;
 DELETE FROM AE_DIAG;
 DELETE FROM BODY_SYSTEM;
 COMMIT;
 DELETE FROM AE_OUTCOME;
 DELETE FROM ASSESSMENT_CAUSALITY;
 DELETE FROM CLASSIFICATION_SERIOUSNESS;
 DELETE FROM COUNTRY;
 DELETE FROM DOSE_FREQUENCY;
 COMMIT;
 DELETE FROM DOSE_REGIMEN;
 DELETE FROM DOSE_UNITS;
 DELETE FROM ETHNIC_ORIGIN;
 DELETE FROM EVENT_SEVERITY;
 DELETE FROM FORMULATION;
 COMMIT;
 DELETE FROM INDICATION_TREATMENT;
 DELETE FROM MANUFACTURER;
 DELETE FROM PRODUCT;
 DELETE FROM RECHALLENGE_RESULT;
 DELETE FROM REPORTER_STAT;
 DELETE FROM REP_TYPE;
 DELETE FROM ROUTE;
 COMMIT;
 DELETE FROM SEX;
 DELETE FROM SOURCE_INFO;
 DELETE FROM TRIAL_NUM;
 COMMIT;
END;
--------------------------------------------
PROCEDURE Register_User(
      p_UserCount IN NUMBER,
      p_Username  IN User_List.Username%TYPE,
      p_RemoteIp  IN User_Session.Remoteip%TYPE,
      p_SessionId OUT User_Session.Session_Id%TYPE)
AS
BEGIN
   p_SessionId := p_Username||'_'||to_Char(SYSDATE,'YYYYMMDDHH24MISS');
 
   INSERT INTO User_Session(Session_Id,Username, Login_Time, LogIn, RemoteIp)
   VALUES (p_SessionId, p_Username, SYSDATE, 1, p_RemoteIp);
END;
--------------------------------------------
PROCEDURE UnRegister_User(
      p_SessionId IN User_Session.Session_Id%TYPE)
AS
BEGIN
   UPDATE User_Session
      SET LogIn = 0,
          Logout_Time = SYSDATE
    WHERE Session_Id = p_SessionId;
END;
--------------------------------------------
PROCEDURE Get_Interval(
    p_Interval_Code  IN Interval.Interval_Code%TYPE,
    p_Interval       IN Interval.Interval%TYPE,
    p_Active         IN Interval.Is_Valid%TYPE,
    cur_Interval    OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_Interval FOR
SELECT Interval_Code, Interval, Is_Valid
  FROM Interval
 WHERE (p_Interval_Code IS NULL OR UPPER(Interval_Code) LIKE UPPER(p_Interval_Code)||'%')
   AND (p_Interval IS NULL OR UPPER(Interval) LIKE UPPER(p_Interval)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Interval_Code;
END;
--------------------------------------------
PROCEDURE Get_User(
    p_Username     IN User_List.Username%TYPE,
    p_RoleName     IN User_List.Role_Name%TYPE,
    p_Is_Locked    IN User_list.Is_Locked%TYPE,
    p_Search_Type  IN Number,
    cur_User       OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

    OPEN cur_User FOR
  SELECT u.Username as u_Username,
         r.Role_Name as r_Role_Name, r.Is_Locked as r_Is_Locked, u.Password as u_Password,
         u.Pwd_Change_Flag as u_Pwd_Change_Flag,  u.Pwd_Mistake_Count as u_Pwd_Mistake_Count,
         u.Pwd_Change_Date as u_Pwd_Change_Date,
         (CASE WHEN u.Is_Locked =1 OR r.Is_Locked = 1 THEN 1 ELSE 0 END) as u_Is_Locked,
         u.Is_Locked as u_Lock
    FROM User_List u, Role_List r
   WHERE u.Role_Name = r.Role_Name
     AND (p_Username IS NULL OR u.Username = p_Username)
     AND (p_RoleName IS NULL OR UPPER(r.Role_Name) LIKE UPPER(p_RoleName||'%'))
     AND (p_Is_Locked = -1 OR u.Is_Locked = p_Is_Locked)
   ORDER BY u.Username;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

    OPEN cur_User FOR
  SELECT u.Username as u_Username,
         r.Role_Name as r_Role_Name, r.Is_Locked as r_Is_Locked, u.Password as u_Password,
         u.Pwd_Change_Flag as u_Pwd_Change_Flag,  u.Pwd_Mistake_Count as u_Pwd_Mistake_Count,
         u.Pwd_Change_Date as u_Pwd_Change_Date,
         (CASE WHEN u.Is_Locked =1 OR r.Is_Locked = 1 THEN 1 ELSE 0 END) as u_Is_Locked,
         u.Is_Locked as u_Lock
    FROM User_List u, Role_List r
   WHERE u.Role_Name = r.Role_Name
     AND (p_Username IS NULL OR u.Username = p_Username)
      OR (p_RoleName IS NULL OR UPPER(r.Role_Name) LIKE UPPER(p_RoleName||'%'))
     AND (p_Is_Locked = -1 OR u.Is_Locked = p_Is_Locked)
   ORDER BY u.Username;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_User_IgnoreCase(
    p_Username  IN User_List.Username%TYPE,
    p_RoleName  IN User_List.Role_Name%TYPE,
    p_Is_Locked IN User_list.Is_Locked%TYPE,
    cur_User    OUT SYS_REFCURSOR)
AS
BEGIN
    OPEN cur_User FOR
  SELECT u.Username as u_Username,
         r.Role_Name as r_Role_Name, r.Is_Locked as r_Is_Locked, u.Password as u_Password,
         u.Pwd_Change_Flag as u_Pwd_Change_Flag,  u.Pwd_Mistake_Count as u_Pwd_Mistake_Count,
         u.Pwd_Change_Date as u_Pwd_Change_Date,
         (CASE WHEN u.Is_Locked =1 OR r.Is_Locked = 1 THEN 1 ELSE 0 END) as u_Is_Locked,
         u.Is_Locked as u_Lock
    FROM User_List u, Role_List r
   WHERE u.Role_Name = r.Role_Name
     AND (p_Username IS NULL OR UPPER(u.Username) = UPPER(p_Username))
     AND (p_RoleName IS NULL OR UPPER(r.Role_Name) LIKE UPPER(p_RoleName||'%'))
     AND (p_Is_Locked = -1 OR u.Is_Locked = p_Is_Locked)
   ORDER BY u.Username;
END;
--------------------------------------------
PROCEDURE Get_User_Session(
    p_Username  IN User_List.Username%TYPE,
    p_Session   IN User_Session.Session_Id%TYPE,
    cur_User    OUT SYS_REFCURSOR)
AS
BEGIN
    OPEN cur_User FOR
  SELECT r.Role_Name as r_Role_Name, r.Is_Locked as r_Is_Locked, u.Password as u_Password,
         u.Pwd_Change_Flag as u_Pwd_Change_Flag,  u.Pwd_Mistake_Count as u_Pwd_Mistake_Count,
         u.Pwd_Change_Date as u_Pwd_Change_Date,
         (CASE WHEN u.Is_Locked =1 OR r.Is_Locked = 1 THEN 1 ELSE 0 END) as u_Is_Locked,
         u.Is_Locked as u_Lock
    FROM User_List u, Role_List r, User_Session us
   WHERE u.Role_Name = r.Role_Name
     AND u.Username = us.Username
     AND UPPER(us.Session_Id) = UPPER(p_Session)
     AND UPPER(u.Username) = UPPER(p_Username)
   ORDER BY u.username;
END;
--------------------------------------------
PROCEDURE Get_Role(
    p_RoleName  IN Role_List.Role_Name%TYPE,
    p_Is_Locked IN Role_list.Is_Locked%TYPE,
    cur_Role    OUT SYS_REFCURSOR)
AS
BEGIN
    OPEN cur_Role FOR
  SELECT Role_Name, Is_Locked
    FROM Role_List
   WHERE (p_RoleName IS NULL OR UPPER(Role_Name) LIKE UPPER(p_RoleName)||'%')
     AND (p_Is_Locked = -1 OR Is_Locked = p_Is_Locked)
   ORDER BY Role_Name;
END;
--------------------------------------------
PROCEDURE Get_Role_Permissions(
    p_RoleName    IN Role_Permissions.Role_Name%TYPE,
    p_InventoryId IN Role_Permissions.Inv_Id%TYPE,
    p_InvType     IN Inventory.Inv_Type%TYPE,
    cur_Perm      OUT SYS_REFCURSOR)
AS
BEGIN
    OPEN cur_Perm FOR
  SELECT i.Inv_Id, i.Inv_Desc, i.Inv_Type, rp.pView, rp.pAdd, rp.pEdit, rp.pDelete
    FROM Inventory i, Role_Permissions rp
   WHERE i.Inv_Id = rp.Inv_Id
     AND UPPER(rp.Role_Name) = UPPER(p_RoleName)
     AND (p_InventoryId IS NULL OR UPPER(rp.Inv_Id) LIKE UPPER(p_InventoryId)||'%')
     AND (p_InvType IS NULL OR UPPER(i.Inv_Type) = UPPER(p_InvType))
   ORDER BY i.Inv_Id;
END;
--------------------------------------------
PROCEDURE Get_Audit_Trial(
    p_ChgNo       IN Audit_Trial.Chg_No%TYPE,
    p_Username    IN Audit_Trial.Username%TYPE,
    p_InventoryId IN Audit_Trial.Inv_Id%TYPE,
    p_Txn_Type    IN Audit_Trial.Txn_Type%TYPE,
    p_Tgt_Type    IN Audit_Trial.Tgt_Type%TYPE,
    p_From        IN VARCHAR2,
    p_To          IN VARCHAR2,
    p_RefVal      IN Audit_Trial.Pk_Col_Value%TYPE,
    cur_Audit     OUT SYS_REFCURSOR)
AS
BEGIN
    OPEN cur_Audit FOR
  SELECT a.Chg_No as a_Chg_No, a.Username as a_Username, i.Inv_Id as i_Inv_Id, i.Inv_Desc as i_Inv_Desc,
         i.Inv_Type as i_Inv_Type, i.Ins_Audit_Flag as i_Ins_Audit_Flag, i.Del_Audit_Flag as i_Del_Audit_Flag, a.Tgt_Type as a_Tgt_Type,
         a.Txn_Type as a_Txn_Type, a.Chg_Time as a_Chg_Time, a.Pk_Col_Value as a_Pk_Col_Value,
         ic.Inv_Col_Id as ic_Inv_Col_Id, ic.Inv_Id as ic_Inv_Id, ic.Field_Name as ic_Field_Name,
         ic.Audit_Flag as ic_Audit_Flag, a.Old_Val as a_Old_Val, a.New_Val as a_New_Val
    FROM Audit_Trial a, Inventory i, Inventory_Cols ic
   WHERE a.Inv_Id = i.Inv_Id
     AND a.Inv_Col_Id = ic.Inv_Col_Id(+)
     AND (p_ChgNo = -1 OR Chg_No = p_ChgNo)
     AND (p_Username IS NULL OR UPPER(a.Username) LIKE UPPER(p_Username))
     AND (p_InventoryId IS NULL OR UPPER(a.Inv_Id) LIKE UPPER(p_InventoryId))
     AND (p_Txn_Type IS NULL OR UPPER(a.Txn_Type) = UPPER(p_Txn_Type))
     AND (p_Tgt_Type IS NULL OR UPPER(a.Tgt_Type) = UPPER(p_Tgt_Type))
     AND (p_From IS NULL OR a.Chg_Time >= TO_DATE(p_From,'DD-MON-YYYY HH24:MI'))
     AND (p_To IS NULL OR a.Chg_Time <= TO_DATE(p_To,'DD-MON-YYYY HH24:MI'))
     AND (p_RefVal IS NULL OR UPPER(a.Pk_Col_Value) LIKE UPPER(p_RefVal))
   ORDER BY a.Chg_No;
END;
--------------------------------------------
PROCEDURE Get_Inventory(
    p_InvId    	   IN Inventory.Inv_Id%TYPE,
    p_InvDesc  	   IN Inventory.Inv_Desc%TYPE,
    p_InvType  	   IN Inventory.Inv_Type%TYPE,
    p_Search_Type  IN Number,
    cur_Inv    	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Inv FOR
SELECT Inv_Id, Inv_Desc, Inv_Type, Ins_Audit_Flag, Del_Audit_Flag
  FROM Inventory
 WHERE (p_InvId IS NULL OR UPPER(Inv_Id) LIKE  UPPER(p_InvId)||'%')
   AND (p_InvDesc IS NULL OR UPPER(Inv_Desc) LIKE '%'||UPPER(p_InvDesc)||'%')
   AND (p_InvType IS NULL OR UPPER(Inv_Type) = UPPER(p_InvType))
 ORDER BY Inv_Id;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Inv FOR
SELECT Inv_Id, Inv_Desc, Inv_Type, Ins_Audit_Flag, Del_Audit_Flag
  FROM Inventory
 WHERE (p_InvId IS NULL OR UPPER(Inv_Id) LIKE  UPPER(p_InvId)||'%')
    OR (p_InvDesc IS NULL OR UPPER(Inv_Desc) LIKE '%'||UPPER(p_InvDesc)||'%')
   AND (p_InvType IS NULL OR UPPER(Inv_Type) = UPPER(p_InvType))
 ORDER BY Inv_Id;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Inventory_Cols(
    p_InvId   IN Inventory_Cols.Inv_Id%TYPE,
    p_InvCol  IN Inventory_Cols.Inv_Col_Id%TYPE,
    p_Field   IN Inventory_Cols.Field_Name%TYPE,
    cur_Inv   OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_Inv FOR
SELECT i.Inv_Id as i_Inv_Id, i.Inv_Desc as i_Inv_Desc, i.Inv_Type as i_Inv_Type,
       i.Ins_Audit_Flag as i_Ins_Audit_Flag, i.Del_Audit_Flag as i_Del_Audit_Flag,ic.Inv_Col_Id as ic_Inv_Col_Id, ic.Field_Name as ic_Field_Name, ic.Audit_Flag as ic_Audit_Flag
  FROM Inventory_Cols ic, Inventory i
 WHERE ic.Inv_Id = i.Inv_Id
   AND (p_InvCol IS NULL OR UPPER(ic.Inv_Col_Id) = UPPER(p_InvCol))
   AND (p_InvId IS NULL OR UPPER(ic.Inv_Id) =  UPPER(p_InvId))
   AND (p_Field IS NULL OR UPPER(ic.Field_Name) = UPPER(p_Field)||'%')
 ORDER BY i.Inv_ID, ic.Field_Name;
END;
--------------------------------------------
PROCEDURE Get_Case(
    p_RepNum       IN Case_Details.Rep_Num%TYPE,
    p_Diag_Code    IN Adverse_Event.Diag_Code%TYPE,
    p_Trial_Num    IN Case_Details.Trial_Num%TYPE,
    p_Prod_Code    IN Case_Details.Product_Code%TYPE,
    p_Frm          IN VARCHAR2,
    p_To           IN VARCHAR2,
    p_Is_Suspended IN Case_Details.Is_Suspended%TYPE,
    cur_Case       OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_Case FOR
SELECT vcd.Rep_Num as cd_Rep_Num,
       vcd.Date_Entry as cd_Date_Entry,
       vcd.Recv_By_At_Hq as cd_Recv_By_At_Hq,
       vcd.Date_Recv as cd_Date_Recv,
       vcd.Rep_Num_Local as cd_Rep_Num_Local,
       vcd.Rep_Xref_Num as cd_Rep_Xref_Num,
       vcd.Clin_Rep_Info as cd_Clin_Rep_Info,
       vcd.Comp_Rep_Info as cd_Comp_Rep_Info,
       vcd.Patient_Num as cd_Patient_Num,
       vcd.Hosp_Nm as cd_Hosp_Nm,
       vcd.Batch_Num as cd_Batch_Num,
       vcd.Dose as cd_Dose,
       vcd.Treat_Start as cd_Treat_Start,
       vcd.Treat_Stop as cd_Treat_Stop,
       vcd.Rechallenge as cd_Rechallenge,
       vcd.Authorization_No as cd_Authorization_No,
       vcd.P_Initials as cd_P_Initials,
       vcd.Date_Of_Birth as cd_Date_Of_Birth,
       vcd.Age_Reported as cd_Age_Reported,
       vcd.Weight as cd_Weight,
       vcd.Height as cd_Height,
       vcd.Pregnancy as cd_Pregnancy,
       vcd.Pregnancy_Week as cd_Pregnancy_Week,
       vcd.Short_History as cd_Short_History,
       vcd.Outcome_Rep_Date as cd_Outcome_Rep_Date,
       vcd.Autopsy as cd_Autopsy,
       vcd.Autopsy_Result as cd_Autopsy_Result,
       vcd.Phy_Assessment_Date as cd_Phy_Assessment_Date,
       vcd.Co_Assessment_Date as cd_Co_Assessment_Date,
       vcd.Phy_Assessment_Reason as cd_Phy_Assessment_Reason,
       vcd.Co_Assessment_Reason as cd_Co_Assessment_Reason,
       vcd.Contributing_Factors as cd_Contributing_Factors,
       vcd.Rechallenge_Date as cd_Rechallenge_Date,
       vcd.Rechallenge_Dose as cd_Rechallenge_Dose,
       vcd.Verbatim as cd_Verbatim,
       vcd.Short_Comment as cd_Short_Comment,
       vcd.Distbn_Date as cd_Distbn_Date,
       vcd.Submission_Date as cd_Submission_Date,
       vcd.Further_Commn as cd_Further_Commn,
       vcd.AE_Desc as cd_AE_Desc,
       vcd.Is_Suspended as cd_Is_Suspended,
       vcd.subm_lha_code as cd_Lha_Code,
       vcd.subm_company_nm as cd_Subm_Company_Nm,
       vcd.M_Code as cd_M_Code,
       vcd.Result_Code as cd_Result_Code,
       vcd.phy_Assessment_Code as pac_Assessment_Code,
       vcd.co_assessment_code as cac_Assessment_Code,
       vcd.Product_Code as cd_Product_Code,
       vcd.Ethnic_Code as cd_Ethnic_Code,
       vcd.Outcome_Code as cd_Outcome_Code,
       vcd.Indication_Num as cd_Indication_Num,
       vcd.Sex_Code as cd_Sex_Code,
       vcd.Unit_Code as cd_Unit_Code,
       vcd.Dregimen_Code as cd_Dregimen_Code,
       vcd.Dfreq_Code as cd_Dfreq_Code,
       vcd.Route_Code as cd_Route_Code,
       vcd.Status_Code as cd_Status_Code,
       vcd.Source_Code as cd_Source_code,
       vcd.Formulation_Code as cd_Formulation_Code,
	     vcd.Type_Code as cd_Type_Code,
       vcd.Trial_Num as cd_Trial_Num,
       vcd.Country_Code as cd_Country_Code,
       vcd.Type_Desc as cd_Type_Desc,
       vcd.Country_Nm as cd_Country_Nm,
       vcd.Status_Desc as cd_Status_Desc,
       vcd.Source_Desc as cd_Source_Desc,
       vcd.M_Name as cd_M_Name,
       vcd.Formulation_Desc as cd_Formulation_Desc,
       vcd.Unit_Desc as cd_Unit_Desc,
       vcd.Dregimen_Desc as cd_Dregimen_Desc,
       vcd.Dfreq_Desc as cd_Dfreq_Desc,
       vcd.Route_Desc as cd_Route_Desc,
       vcd.Indication as cd_Indication,
       vcd.Sex_Desc as cd_Sex_Desc,
       vcd.Ethnic_Desc as cd_Ethnic_Desc,
       vcd.Outcome as cd_Outcome,
       vcd.Generic_Nm as cd_Generic_Nm,
       vcd.Brand_Nm as cd_Brand_Nm,
       vcd.Phy_assessment as cd_Phy_assessment,
       vcd.Phy_assess_Flg as cd_Phy_assess_Flg,
       vcd.Co_assessment as cd_Co_assessment,
       vcd.Co_assess_Flg as cd_Co_assess_Flg,
       vcd.Result_Desc as cd_Result_Desc,
       vcd.Lha_Desc as cd_Lha_Desc,
       vcd.Duration as cd_Duration,
       vcd.No_Of_Dosage as cd_No_Of_Dosage,
       vcd.Dosage_Interval as cd_Dosage_Interval,
       vcd.Interval as cd_Interval,
       vcd.PRI_SERIOUSNESS_CODE as cd_PRI_SERIOUSNESS_CODE,
       vcd.PRI_SERIOUSNESS as cd_PRI_SERIOUSNESS,
       vcd.PRI_REACTION_ONSET as cd_PRI_REACTION_ONSET,
       vcd.PRI_ABATE_AFTER_STOP as cd_PRI_ABATE_AFTER_STOP,
       vcd.COUNT_CM as cd_COUNT_CM,
       vcd.M_ADDR1 as cd_M_ADDR1, 
       vcd.M_ADDR2 as cd_M_ADDR2,
       vcd.M_ADDR3 as cd_M_ADDR3,
       vcd.M_ADDR4 as cd_M_ADDR4,
       vcd.M_PHONE as cd_M_PHONE,
       vcd.M_FAX as cd_M_FAX,
       vcd.M_EMAIL as cd_M_EMAIL
  FROM V_Case_Details vcd
 WHERE (p_RepNum = -1 OR to_char(vcd.rep_num) LIKE p_RepNum||'%')
   AND (p_Trial_Num IS NULL OR vcd.Trial_Num LIKE p_Trial_Num||'%')
   AND (p_Prod_Code IS NULL OR vcd.Product_Code LIKE p_Prod_Code||'%')
   AND (p_Frm IS NULL OR vcd.Date_Recv >= TO_DATE(p_Frm,'DD-MON-YYYY'))
   AND (p_To IS NULL OR vcd.Date_Recv <= TO_DATE(p_To,'DD-MON-YYYY'))
   AND (p_Is_Suspended = -1 OR vcd.Is_Suspended = p_Is_Suspended)
   AND (p_Diag_Code IS NULL OR vcd.rep_num IN (SELECT ae.rep_num FROM Adverse_Event ae WHERE Diag_Code=p_Diag_Code))
 ORDER BY vcd.Rep_Num DESC;
END;
--------------------------------------------
PROCEDURE Get_Country(
    p_Code    	   IN Country.Country_Code%TYPE,
    p_Name    	   IN Country.Country_Nm%TYPE,
    p_Active 	   IN Country.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Ctry  OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  	OPEN cur_Ctry FOR
  SELECT Country_Code, Country_Nm, Is_Valid
    FROM Country
   WHERE (p_Code IS NULL OR UPPER(Country_Code) LIKE UPPER(p_Code)||'%')
     AND (p_Name IS NULL OR UPPER(Country_Nm) LIKE '%'||UPPER(p_Name)||'%')
     AND (p_Active = -1 OR Is_Valid = p_Active)
   ORDER BY Country_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  	OPEN cur_Ctry FOR
  SELECT Country_Code, Country_Nm, Is_Valid
    FROM Country
   WHERE (p_Code IS NULL OR UPPER(Country_Code) LIKE UPPER(p_Code)||'%')
      OR (p_Name IS NULL OR UPPER(Country_Nm) LIKE '%'||UPPER(p_Name)||'%')
     AND (p_Active = -1 OR Is_Valid = p_Active)
   ORDER BY Country_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Outcome(
    p_Code         IN AE_Outcome.Outcome_Code%TYPE,
    p_Desc         IN AE_Outcome.Outcome%TYPE,
    p_Active       IN AE_Outcome.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Outcome    OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Outcome FOR
SELECT Outcome_Code, Outcome, Is_Valid
  FROM Ae_Outcome
 WHERE (p_Code IS NULL OR UPPER(Outcome_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Outcome) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Outcome_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Outcome FOR
SELECT Outcome_Code, Outcome, Is_Valid
  FROM Ae_Outcome
 WHERE (p_Code IS NULL OR UPPER(Outcome_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Outcome) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Outcome_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_LLT(
    p_Code   	   IN AE_LLT.LLT_CODE%TYPE,
    p_Desc   	   IN AE_LLT.LLT_DESC%TYPE,
    p_Active 	   IN AE_LLT.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_LLT  	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_LLT FOR
SELECT LLT_Code, LLT_Desc, Is_Valid
  FROM Ae_LLT
 WHERE (p_Code IS NULL OR UPPER(LLT_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(LLT_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY LLT_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_LLT FOR
SELECT LLT_Code, LLT_Desc, Is_Valid
  FROM Ae_LLT
 WHERE (p_Code IS NULL OR UPPER(LLT_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(LLT_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY LLT_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Diag(
    p_Code   	   IN AE_DIAG.DIAG_CODE%TYPE,
    p_Desc   	   IN AE_Diag.Diag_Desc%TYPE,
    p_Active 	   IN Ae_Diag.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Diag 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Diag FOR
SELECT Diag_Code, Diag_Desc, Is_Valid
  FROM Ae_Diag
 WHERE (p_Code IS NULL OR UPPER(Diag_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Diag_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Diag_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Diag FOR
SELECT Diag_Code, Diag_Desc, Is_Valid
  FROM Ae_Diag
 WHERE (p_Code IS NULL OR UPPER(Diag_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Diag_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Diag_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Assessment(
    p_Code   	   IN Assessment_Causality.Assessment_Code%TYPE,
    p_Desc   	   IN Assessment_Causality.Assessment%TYPE,
    p_Active 	   IN Assessment_Causality.Is_Valid%TYPE,
    p_Flag   	   IN Assessment_Causality.Assessment_Flag%TYPE,
    p_Search_Type  IN Number,
    cur_Asmt 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Asmt FOR
SELECT Assessment_Code, Assessment, Assessment_Flag, Is_Valid
  FROM Assessment_Causality
 WHERE (p_Code IS NULL OR UPPER(Assessment_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Assessment) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
   AND (p_Flag IS NULL OR UPPER(Assessment_Flag) = UPPER(p_Flag))
 ORDER BY Assessment_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Asmt FOR
SELECT Assessment_Code, Assessment, Assessment_Flag, Is_Valid
  FROM Assessment_Causality
 WHERE (p_Code IS NULL OR UPPER(Assessment_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Assessment) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
    OR (p_Flag IS NULL OR UPPER(Assessment_Flag) = UPPER(p_Flag))
 ORDER BY Assessment_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Seriousness(
    p_Code    	   IN Classification_Seriousness.Classification_Code%TYPE,
    p_Desc    	   IN Classification_Seriousness.Classification_Desc%TYPE,
    p_Active  	   IN Classification_Seriousness.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Clasf 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Clasf FOR
SELECT Classification_Code, Classification_Desc, Is_Valid
  FROM Classification_Seriousness
 WHERE (p_Code IS NULL OR UPPER(Classification_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Classification_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Classification_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Clasf FOR
SELECT Classification_Code, Classification_Desc, Is_Valid
  FROM Classification_Seriousness
 WHERE (p_Code IS NULL OR UPPER(Classification_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Classification_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Classification_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_DFreq(
    p_Code    	   IN Dose_Frequency.Dfreq_Code%TYPE,
    p_Desc    	   IN Dose_Frequency.Dfreq_Desc%TYPE,
    p_Active  	   IN Dose_Frequency.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_DFreq 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_DFreq FOR
SELECT Dfreq_Code, Dfreq_Desc, Is_Valid
  FROM Dose_Frequency
 WHERE (p_Code IS NULL OR UPPER(Dfreq_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Dfreq_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Dfreq_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_DFreq FOR
SELECT Dfreq_Code, Dfreq_Desc, Is_Valid
  FROM Dose_Frequency
 WHERE (p_Code IS NULL OR UPPER(Dfreq_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Dfreq_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Dfreq_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_DRgmn(
    p_Code    		  IN Dose_Regimen.Dregimen_Code%TYPE,
    p_Desc    		  IN Dose_Regimen.Dregimen_Desc%TYPE,
    p_Active  		  IN Dose_Regimen.Is_Valid%TYPE,
    p_No_Of_Dosage    IN Dose_Regimen.No_Of_Dosage%TYPE,
    p_Dosage_Interval IN Dose_Regimen.Dosage_Interval%TYPE,
    p_Search_Type     IN Number,
    cur_DRgmn 		  OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_DRgmn FOR
SELECT d.Dregimen_Code as d_Dregimen_Code, d.Dregimen_Desc as d_Dregimen_Desc, d.Is_Valid as d_Is_Valid, d.No_Of_Dosage as d_No_Of_Dosage, d.Dosage_Interval as d_Dosage_Interval, i.Interval as i_Interval
  FROM Dose_Regimen d, Interval i
 WHERE d.Dosage_Interval = i.Interval_Code
   AND (p_Code IS NULL OR UPPER(d.Dregimen_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(d.Dregimen_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR d.Is_Valid = p_Active)
   AND (p_No_Of_Dosage = -1 OR d.No_Of_Dosage = p_No_Of_Dosage)
   AND (p_Dosage_Interval = -1 OR d.Dosage_Interval = p_Dosage_Interval)
 ORDER BY d.Dregimen_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_DRgmn FOR
SELECT d_Dregimen_Code, d_Dregimen_Desc, d_Is_Valid, d_No_Of_Dosage, d_Dosage_Interval, i_Interval
  FROM (SELECT d.Dregimen_Code as d_Dregimen_Code, d.Dregimen_Desc as d_Dregimen_Desc, d.Is_Valid as d_Is_Valid, d.No_Of_Dosage as d_No_Of_Dosage, d.Dosage_Interval as d_Dosage_Interval, i.Interval as i_Interval
          FROM Dose_Regimen d, Interval i
         WHERE d.Dosage_Interval = i.Interval_Code)
 WHERE (p_Code IS NULL OR UPPER(d_Dregimen_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(d_Dregimen_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR d_Is_Valid = p_Active)
   AND (p_No_Of_Dosage = -1 OR d_No_Of_Dosage = p_No_Of_Dosage)
   AND (p_Dosage_Interval = -1 OR d_Dosage_Interval = p_Dosage_Interval)
 ORDER BY d_Dregimen_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_DUnit(
    p_Code    	   IN Dose_Units.Unit_Code%TYPE,
    p_Desc    	   IN Dose_Units.Unit_Desc%TYPE,
    p_Active  	   IN Dose_Units.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_DUnit 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_DUnit FOR
SELECT Unit_Code, Unit_Desc, Is_Valid
  FROM Dose_Units
 WHERE (p_Code IS NULL OR UPPER(Unit_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Unit_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Unit_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_DUnit FOR
SELECT Unit_Code, Unit_Desc, Is_Valid
  FROM Dose_Units
 WHERE (p_Code IS NULL OR UPPER(Unit_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Unit_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Unit_Code;


	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_EOrigin(
    p_Code    	   IN Ethnic_Origin.Ethnic_Code%TYPE,
    p_Desc    	   IN Ethnic_Origin.Ethnic_Desc%TYPE,
    p_Active  	   IN Ethnic_Origin.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_EOrgn 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_EOrgn FOR
SELECT Ethnic_Code, Ethnic_Desc, Is_Valid
  FROM Ethnic_Origin
 WHERE (p_Code IS NULL OR UPPER(Ethnic_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Ethnic_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Ethnic_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_EOrgn FOR
SELECT Ethnic_Code, Ethnic_Desc, Is_Valid
  FROM Ethnic_Origin
 WHERE (p_Code IS NULL OR UPPER(Ethnic_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Ethnic_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Ethnic_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_ESvty(
    p_Code    	   IN Event_Severity.Eseverity_Code%TYPE,
    p_Desc    	   IN Event_Severity.Eseverity_Desc%TYPE,
    p_Active  	   IN Event_Severity.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_ESvty 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_ESvty FOR
SELECT ESeverity_Code,ESeverity_Desc, Is_Valid
  FROM Event_Severity
 WHERE (p_Code IS NULL OR UPPER(ESeverity_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(ESeverity_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY ESeverity_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_ESvty FOR
SELECT ESeverity_Code,ESeverity_Desc, Is_Valid
  FROM Event_Severity
 WHERE (p_Code IS NULL OR UPPER(ESeverity_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(ESeverity_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY ESeverity_Code;


	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Formulation(
    p_Code    	   IN Event_Severity.Eseverity_Code%TYPE,
    p_Desc    	   IN Event_Severity.Eseverity_Desc%TYPE,
    p_Active  	   IN Event_Severity.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Frmln 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Frmln FOR
SELECT Formulation_Code, Formulation_Desc, Is_Valid
  FROM Formulation
 WHERE (p_Code IS NULL OR UPPER(Formulation_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Formulation_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Formulation_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Frmln FOR
SELECT Formulation_Code, Formulation_Desc, Is_Valid
  FROM Formulation
 WHERE (p_Code IS NULL OR UPPER(Formulation_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Formulation_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Formulation_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Product(
    p_Code   	   IN Product.Product_Code%TYPE,
    p_GNm    	   IN Product.Generic_Nm%TYPE,
    p_BNm    	   IN Product.Brand_Nm%TYPE,
    p_Active 	   IN Product.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Pdt  	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Pdt FOR
SELECT Product_Code, Generic_Nm, Brand_Nm, Is_Valid
  FROM Product
 WHERE (p_Code IS NULL OR UPPER(Product_Code) LIKE UPPER(p_Code)||'%')
   AND (p_GNm IS NULL OR UPPER(Generic_Nm) LIKE '%'||UPPER(p_GNm)||'%')
   AND (p_BNm IS NULL OR UPPER(Brand_Nm) LIKE '%'||UPPER(p_BNm)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Product_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Pdt FOR
SELECT Product_Code, Generic_Nm, Brand_Nm, Is_Valid
  FROM Product
 WHERE (p_Code IS NULL OR UPPER(Product_Code) LIKE UPPER(p_Code)||'%')
    OR (p_GNm IS NULL OR UPPER(Generic_Nm) LIKE '%'||UPPER(p_GNm)||'%')
    OR (p_BNm IS NULL OR UPPER(Brand_Nm) LIKE '%'||UPPER(p_BNm)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Product_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Sex(
    p_Code    	   IN Sex.Sex_Code%TYPE,
    p_Desc    	   IN Sex.Sex_Desc%TYPE,
    p_Active  	   IN Sex.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Sex   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Sex FOR
SELECT Sex_Code, Sex_Desc, Is_Valid
  FROM Sex
 WHERE (p_Code IS NULL OR UPPER(Sex_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Sex_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Sex_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Sex FOR
SELECT Sex_Code, Sex_Desc, Is_Valid
  FROM Sex
 WHERE (p_Code IS NULL OR UPPER(Sex_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Sex_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Sex_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Indication(
    p_Code    	   IN Indication_Treatment.Indication_Num%TYPE,
    p_Desc    	   IN Indication_Treatment.Indication%TYPE,
    p_Active  	   IN Indication_Treatment.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Ind   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Ind FOR
SELECT Indication_Num, Indication, Is_Valid
  FROM Indication_Treatment
 WHERE (p_Code IS NULL OR UPPER(Indication_Num) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Indication) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Indication_Num;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Ind FOR
SELECT Indication_Num, Indication, Is_Valid
  FROM Indication_Treatment
 WHERE (p_Code IS NULL OR UPPER(Indication_Num) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Indication) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Indication_Num;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Manufacturer(
    p_Code    	   IN Manufacturer.m_Code%TYPE,
    p_Name    	   IN Manufacturer.m_Name%TYPE,
    p_Active  	   IN Manufacturer.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Mfr   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Mfr FOR
SELECT M_Code, M_Name, M_Addr1, M_Addr2, M_Addr3, M_Addr4,
       M_Phone, M_Fax, M_Email, Is_Valid
  FROM Manufacturer
 WHERE (p_Code IS NULL OR UPPER(M_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Name IS NULL OR UPPER(M_Name) LIKE '%'||UPPER(p_Name)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY M_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Mfr FOR
SELECT M_Code, M_Name, M_Addr1, M_Addr2, M_Addr3, M_Addr4,
       M_Phone, M_Fax, M_Email, Is_Valid
  FROM Manufacturer
 WHERE (p_Code IS NULL OR UPPER(M_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Name IS NULL OR UPPER(M_Name) LIKE '%'||UPPER(p_Name)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY M_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_RechResult(
    p_Code    	   IN Rechallenge_Result.Result_Code%TYPE,
    p_Desc    	   IN Rechallenge_Result.Result_Desc%TYPE,
    p_Active  	   IN Rechallenge_Result.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Res   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Res FOR
SELECT Result_Code, Result_Desc, Is_Valid
  FROM Rechallenge_Result
 WHERE (p_Code IS NULL OR UPPER(Result_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Result_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Result_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Res FOR
SELECT Result_Code, Result_Desc, Is_Valid
  FROM Rechallenge_Result
 WHERE (p_Code IS NULL OR UPPER(Result_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Result_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Result_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_RepType(
    p_Code    	   IN Rep_Type.Type_Code%TYPE,
    p_Desc    	   IN Rep_Type.Type_Desc%TYPE,
    p_Active  	   IN Rep_Type.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Rep   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Rep FOR
SELECT Type_Code, Type_Desc, Is_Valid
  FROM Rep_Type
 WHERE (p_Code IS NULL OR UPPER(Type_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Type_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Type_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Rep FOR
SELECT Type_Code, Type_Desc, Is_Valid
  FROM Rep_Type
 WHERE (p_Code IS NULL OR UPPER(Type_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Type_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Type_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_RepSrc(
    p_Code    	   IN Source_Info.Source_Code%TYPE,
    p_Desc    	   IN Source_Info.Source_Desc%TYPE,
    p_Active  	   IN Source_Info.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_RSrc  	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_RSrc FOR
SELECT Source_Code, Source_Desc, Is_Valid
  FROM Source_Info
 WHERE (p_Code IS NULL OR UPPER(Source_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Source_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Source_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_RSrc FOR
SELECT Source_Code, Source_Desc, Is_Valid
  FROM Source_Info
 WHERE (p_Code IS NULL OR UPPER(Source_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Source_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Source_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_RepStat(
    p_Code    	   IN Reporter_Stat.Status_Code%TYPE,
    p_Desc    	   IN Reporter_Stat.Status_Desc%TYPE,
    p_Active  	   IN Reporter_Stat.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_RStat 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_RStat FOR
SELECT Status_Code, Status_Desc, Is_Valid
  FROM Reporter_Stat
 WHERE (p_Code IS NULL OR UPPER(Status_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Status_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Status_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_RStat FOR
SELECT Status_Code, Status_Desc, Is_Valid
  FROM Reporter_Stat
 WHERE (p_Code IS NULL OR UPPER(Status_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Status_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Status_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_LHA(
    p_Code    	   IN LHA.LHA_CODE%TYPE,
    p_Desc    	   IN LHA.LHA_DESC%TYPE,
    p_Ctry    	   IN Country.Country_Nm%TYPE,
    p_Active  	   IN LHA.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_LHA   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_LHA FOR
SELECT c.Country_Code as c_Country_Code, c.Country_Nm as c_Country_Nm,
       c.Is_Valid as c_Is_Valid, l.LHA_Code as l_LHA_Code,
       l.LHA_Desc as l_LHA_Desc, l.Is_Valid as l_Is_Valid
  FROM LHA l, Country c
 WHERE l.Country_Code = c.Country_Code
   AND (p_Code IS NULL OR UPPER(l.LHA_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(l.LHA_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Ctry IS NULL OR UPPER(c.Country_Nm) LIKE UPPER(p_Ctry)||'%')
   AND (p_Active = -1 OR l.Is_Valid = p_Active)
 ORDER BY l.LHA_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_LHA FOR
SELECT c_Country_Code, c_Country_Nm, c_Is_Valid, l_LHA_Code, l_LHA_Desc, l_Is_Valid
  FROM (SELECT c.Country_Code as c_Country_Code, c.Country_Nm as c_Country_Nm,
                c.Is_Valid as c_Is_Valid, l.LHA_Code as l_LHA_Code,
                l.LHA_Desc as l_LHA_Desc, l.Is_Valid as l_Is_Valid
        FROM LHA l, Country c
        WHERE l.Country_Code = c.Country_Code)
 WHERE (p_Code IS NULL OR UPPER(l_LHA_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(l_LHA_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Ctry IS NULL OR UPPER(c_Country_Nm) LIKE UPPER(p_Ctry)||'%')
   AND (p_Active = -1 OR l_Is_Valid = p_Active)
 ORDER BY l_LHA_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_SCo(
    p_Name    	   IN Subm_Company.Company_Nm%TYPE,
    p_Ctry    	   IN Country.Country_Nm%TYPE,
    p_Active  	   IN Subm_Company.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_SCo   	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_SCo FOR
SELECT s.Company_Nm as s_Company_Nm, c.Country_Code as c_Country_Code,
       c.Country_Nm as c_Country_Nm, c.Is_Valid as c_Is_Valid,
       s.Is_Valid as s_Is_Valid
  FROM Subm_Company s, Country c
 WHERE s.Country_Code = c.Country_Code
   AND (p_Name IS NULL OR UPPER(s.Company_Nm) LIKE '%'||UPPER(p_Name)||'%')
   AND (p_Ctry IS NULL OR UPPER(c.Country_Nm) LIKE '%'||UPPER(p_Ctry)||'%')
   AND (p_Active = -1 OR s.Is_Valid = p_Active)
 ORDER BY s.Company_Nm;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_SCo FOR
SELECT s_Company_Nm, c_Country_Code, c_Country_Nm, c_Is_Valid, s_Is_Valid
  FROM (SELECT s.Company_Nm as s_Company_Nm, c.Country_Code as c_Country_Code,
                c.Country_Nm as c_Country_Nm, c.Is_Valid as c_Is_Valid,
                s.Is_Valid as s_Is_Valid
          FROM Subm_Company s, Country c
         WHERE s.Country_Code = c.Country_Code)
 WHERE (p_Name IS NULL OR UPPER(s_Company_Nm) LIKE '%'||UPPER(p_Name)||'%')
 -- OR (p_Ctry IS NULL OR UPPER(c_Country_Nm) LIKE '%'||UPPER(p_Ctry)||'%')
   AND (p_Active = -1 OR s_Is_Valid = p_Active)
 ORDER BY s_Company_Nm;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Trial(
    p_Code         IN Trial_Num.Trial_Num%TYPE,
    p_Active       IN Trial_Num.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Trial      OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Trial FOR
SELECT Trial_Num, Is_Valid
  FROM Trial_Num
 WHERE (p_Code IS NULL OR UPPER(Trial_Num) LIKE UPPER(p_Code)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Trial_Num;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Trial FOR
SELECT Trial_Num, Is_Valid
  FROM Trial_Num
 WHERE (p_Code IS NULL OR UPPER(Trial_Num) LIKE '%'||UPPER(p_Code)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Trial_Num;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_BodySys(
    p_Code    	   IN Body_System.Body_Sys_Num%TYPE,
    p_Desc    	   IN Body_System.Body_System%TYPE,
    p_Active  	   IN Body_System.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_BSys  	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_BSys FOR
SELECT Body_Sys_Num, Body_System, Is_Valid
  FROM Body_System
 WHERE (p_Code IS NULL OR UPPER(Body_Sys_Num) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Body_System) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Body_Sys_Num;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_BSys FOR
SELECT Body_Sys_Num, Body_System, Is_Valid
  FROM Body_System
 WHERE (p_Code IS NULL OR UPPER(Body_Sys_Num) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Body_System) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Body_Sys_Num;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE Get_Route(
    p_Code    	   IN Route.Route_Code%TYPE,
    p_Desc    	   IN Route.Route_Desc%TYPE,
    p_Active  	   IN Route.Is_Valid%TYPE,
    p_Search_Type  IN Number,
    cur_Route 	   OUT SYS_REFCURSOR)
AS
BEGIN
IF p_Search_Type = '1' THEN
	BEGIN

  OPEN cur_Route FOR
SELECT Route_Code,Route_Desc, Is_Valid
  FROM Route
 WHERE (p_Code IS NULL OR UPPER(Route_Code) LIKE UPPER(p_Code)||'%')
   AND (p_Desc IS NULL OR UPPER(Route_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Route_Code;

 	END;
ELSIF p_Search_Type = '0' THEN
	BEGIN

  OPEN cur_Route FOR
SELECT Route_Code,Route_Desc, Is_Valid
  FROM Route
 WHERE (p_Code IS NULL OR UPPER(Route_Code) LIKE UPPER(p_Code)||'%')
    OR (p_Desc IS NULL OR UPPER(Route_Desc) LIKE '%'||UPPER(p_Desc)||'%')
   AND (p_Active = -1 OR Is_Valid = p_Active)
 ORDER BY Route_Code;

	END;
END IF;
END;
--------------------------------------------
PROCEDURE List_Pat_Num(
    cur_PatNum OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_PatNum FOR
SELECT DISTINCT Patient_Num
  FROM Case_Details
 ORDER BY Patient_Num;
END;
--------------------------------------------
PROCEDURE List_Rep_Num(
    cur_RepNum OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_RepNum FOR
SELECT DISTINCT Rep_Num
  FROM Case_Details
 ORDER BY Rep_Num;
END;
--------------------------------------------

PROCEDURE Save_License(
    p_License_Key IN LICENSE.License_Key%TYPE)
AS
BEGIN
    INSERT
      INTO LICENSE
           (License_Key)
    VALUES (UPPER(p_License_Key));
END;
--------------------------------------------
PROCEDURE Delete_License(
    p_License_Key IN LICENSE.License_Key%TYPE)
AS
BEGIN
    DELETE
      FROM LICENSE
     WHERE License_Key = p_License_Key;
END;
--------------------------------------------
PROCEDURE Get_License(
    p_License_Key IN LICENSE.License_Key%TYPE,
    cur_License OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_License FOR
SELECT License_Key
  FROM LICENSE
 WHERE (p_License_Key IS NULL OR UPPER(p_License_Key) LIKE UPPER(p_License_Key)||'%')
 ORDER BY License_Key;
END;
--------------------------------------------
PROCEDURE Get_Case_Duplicate(
    p_Date_Recv       		IN Case_Details.Date_Recv%TYPE,
    p_Clin_Rep_Info   		IN Case_Details.Clin_Rep_Info%TYPE,
    p_Country_Code    		IN Case_Details.Country_Code%TYPE,
    p_Product_Code    		IN Case_Details.Product_Code%TYPE,
    p_P_Initials		IN Case_Details.P_Initials%TYPE,
    p_Age_Reported		IN Case_Details.Age_Reported%TYPE,
    p_Sex_Code			IN Case_Details.Sex_Code%TYPE,
    p_Trial_Num			IN Case_Details.Trial_Num%TYPE,
    p_Patient_Num		IN Case_Details.Patient_Num%TYPE,
    p_Phy_Assessment_Code	IN Case_Details.Phy_Assessment_Code%TYPE,
    p_Co_Assessment_Code	IN Case_Details.Co_Assessment_Code%TYPE,
    p_Verbatim			IN Case_Details.Verbatim%TYPE,
    p_Diag_Code			IN Adverse_Event.Diag_Code%TYPE,
    p_Aggr_Of_Diag		IN Adverse_Event.Aggr_Of_Diag%TYPE,
    p_Llt_Code			IN Adverse_Event.Llt_Code%TYPE,
    p_Aggr_Of_Llt		IN Adverse_Event.Aggr_Of_Llt%TYPE,
    p_Body_Sys_Num		IN Adverse_Event.Body_Sys_Num%TYPE,
    p_Classification_Code	IN Adverse_Event.Classification_Code%TYPE,
    p_Outcome_Code		IN Adverse_Event.Outcome_Code%TYPE,
    cur_Case       		OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_Case FOR
SELECT distinct cd.Rep_Num, cd.Trial_Num, cd.Date_Recv, cd.IS_SUSPENDED
  FROM Case_Details cd
  LEFT JOIN Adverse_Event ae
    ON cd.Rep_Num = ae.Rep_Num
 WHERE (p_Date_Recv IS NULL OR cd.Date_Recv = p_Date_Recv)
   AND (p_Clin_Rep_Info IS NULL OR cd.Clin_Rep_Info = p_Clin_Rep_Info)
   AND (p_Country_Code IS NULL OR cd.Country_Code = p_Country_Code)
   AND (p_Product_Code IS NULL OR cd.Product_Code = p_Product_Code)
   AND (p_P_Initials IS NULL OR cd.P_Initials = p_P_Initials)
   AND (p_Age_Reported = -1 OR cd.Age_Reported = p_Age_Reported)
   AND (p_Sex_Code IS NULL OR cd.Sex_Code = p_Sex_Code)
   AND (p_Trial_Num IS NULL OR cd.Trial_Num = p_Trial_Num)
   AND (p_Patient_Num IS NULL OR cd.Patient_Num = p_Patient_Num)
   AND (p_Phy_Assessment_Code IS NULL OR cd.Phy_Assessment_Code = p_Phy_Assessment_Code)
   AND (p_Co_Assessment_Code IS NULL OR cd.Co_Assessment_Code = p_Co_Assessment_Code)
   AND (p_Verbatim IS NULL OR cd.Verbatim = p_Verbatim)
   AND (p_Diag_Code IS NULL OR ae.Diag_Code = p_Diag_Code)
   AND (p_Aggr_Of_Diag IS NULL OR ae.Aggr_Of_Diag = p_Aggr_Of_Diag)
   AND (p_Llt_Code IS NULL OR ae.Llt_Code = p_Llt_Code)
   AND (p_Aggr_Of_Llt IS NULL OR ae.Aggr_Of_Llt = p_Aggr_Of_Llt)
   AND (p_Body_Sys_Num IS NULL OR ae.Body_Sys_Num = p_Body_Sys_Num)
   AND (p_Classification_Code IS NULL OR ae.Classification_Code = p_Classification_Code)
   AND (p_Outcome_Code IS NULL OR ae.Outcome_Code = p_Outcome_Code)
 ORDER BY cd.Rep_Num DESC;
END;
--------------------------------------------
PROCEDURE Get_CM(
    p_Rep_Num        IN CONC_MEDICATIONS.Rep_Num%TYPE,
    p_Prod_Code      IN CONC_MEDICATIONS.Product_Code%TYPE,
    cur_Case_CM      OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_Case_CM FOR
SELECT *
  FROM v_conc_medications vcm
 WHERE (p_Rep_Num IS NULL OR vcm.Rep_Num = p_Rep_Num)
   AND (p_Prod_Code IS NULL OR vcm.Product_Code = p_Prod_Code)
 ORDER BY vcm.Rep_Num, vcm.cm_order;
END;
--------------------------------------------
PROCEDURE Get_AE(
    p_Rep_Num        IN ADVERSE_EVENT.Rep_Num%TYPE,
    p_Llt_Code       IN ADVERSE_EVENT.llt_Code%TYPE,
    cur_Case_AE      OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN cur_Case_AE FOR
SELECT *
  FROM v_adverse_event vae
 WHERE (p_Rep_Num IS NULL OR vae.Rep_Num = p_Rep_Num)
   AND (p_Llt_Code IS NULL OR vae.llt_Code = p_Llt_Code)
 ORDER BY vae.Rep_Num, vae.ae_order;
END;
--------------------------------------------
PROCEDURE Copy_Case(
    p_Username		IN USER_LIST.Username%TYPE,
    p_Rep_Num		IN CASE_DETAILS.Rep_Num%TYPE,
    p_Rep_Num_New	OUT CASE_DETAILS.Rep_Num%TYPE)
AS
  	l_Type_Code CASE_DETAILS.Type_Code%TYPE;
  	l_Trial_Num CASE_DETAILS.TRIAL_NUM%TYPE;
  	l_Country_Code CASE_DETAILS.Country_Code%TYPE;
  	l_Status_Code CASE_DETAILS.Status_Code%TYPE;
  	l_Source_Code CASE_DETAILS.Source_Code%TYPE;
  	l_M_Code CASE_DETAILS.M_Code%TYPE;
  	l_Formulation_Code CASE_DETAILS.Formulation_Code%TYPE;
  	l_Unit_Code CASE_DETAILS.Unit_Code%TYPE;
  	l_DRegimen_Code CASE_DETAILS.DRegimen_Code%TYPE;
  	l_DFreq_Code CASE_DETAILS.DFreq_Code%TYPE;
  	l_Route_Code CASE_DETAILS.Route_Code%TYPE;
  	l_Indication_Num CASE_DETAILS.Indication_Num%TYPE;
  	l_Sex_Code CASE_DETAILS.Sex_Code%TYPE;
  	l_Ethnic_Code CASE_DETAILS.Ethnic_Code%TYPE;
  	l_Outcome_Code CASE_DETAILS.Outcome_Code%TYPE;
  	l_Product_Code CASE_DETAILS.Product_Code%TYPE;
  	l_Phy_Assessment_Code CASE_DETAILS.Phy_Assessment_Code%TYPE;
  	l_Co_Assessment_Code CASE_DETAILS.Co_Assessment_Code%TYPE;
  	l_Result_Code CASE_DETAILS.Result_Code%TYPE;
  	l_Subm_Company_Nm CASE_DETAILS.Subm_Company_Nm%TYPE;
  	l_Subm_LHA_Code CASE_DETAILS.Subm_LHA_Code%TYPE;
  	l_Date_Entry CASE_DETAILS.Date_Entry%TYPE;
  	l_Recv_By_At_Hq CASE_DETAILS.Recv_By_At_Hq%TYPE;
  	l_Date_Recv CASE_DETAILS.Date_Recv%TYPE;
  	l_Rep_Num_Local CASE_DETAILS.Rep_Num_Local%TYPE;
  	l_Rep_Xref_Num CASE_DETAILS.Rep_Xref_Num%TYPE;
  	l_Clin_Rep_Info CASE_DETAILS.Clin_Rep_Info%TYPE;
  	l_Comp_Rep_Info CASE_DETAILS.Comp_Rep_Info%TYPE;
  	l_Patient_Num CASE_DETAILS.Patient_Num%TYPE;
  	l_Hosp_Nm CASE_DETAILS.Hosp_Nm%TYPE;
  	l_Batch_Num CASE_DETAILS.Batch_Num%TYPE;
  	l_Dose CASE_DETAILS.Dose%TYPE;
  	l_Treat_Start CASE_DETAILS.Treat_Start%TYPE;
  	l_Treat_Stop CASE_DETAILS.Treat_Stop%TYPE;
  	l_Rechallenge CASE_DETAILS.Rechallenge%TYPE;
  	l_Authorization_No CASE_DETAILS.Authorization_No%TYPE;
  	l_P_Initials CASE_DETAILS.P_Initials%TYPE;
  	l_Date_Of_Birth CASE_DETAILS.Date_Of_Birth%TYPE;
  	l_Age_Reported CASE_DETAILS.Age_Reported%TYPE;
  	l_Weight CASE_DETAILS.Weight%TYPE;
  	l_Height CASE_DETAILS.Height%TYPE;
  	l_Pregnancy CASE_DETAILS.Pregnancy%TYPE;
  	l_Pregnancy_Week CASE_DETAILS.Pregnancy_Week%TYPE;
  	l_Outcome_Rep_Date CASE_DETAILS.Outcome_Rep_Date%TYPE;
  	l_Autopsy CASE_DETAILS.Autopsy%TYPE;
  	l_Autopsy_Result CASE_DETAILS.Autopsy_Result%TYPE;
  	l_Phy_Assessment_Date CASE_DETAILS.Phy_Assessment_Date%TYPE;
  	l_Co_Assessment_Date CASE_DETAILS.Co_Assessment_Date%TYPE;
  	l_Phy_Assessment_Reason CASE_DETAILS.Phy_Assessment_Reason%TYPE;
  	l_Co_Assessment_Reason CASE_DETAILS.Co_Assessment_Reason%TYPE;
  	l_Contributing_Factors CASE_DETAILS.Contributing_Factors%TYPE;
  	l_Rechallenge_Date CASE_DETAILS.Rechallenge_Date%TYPE;
  	l_Rechallenge_Dose CASE_DETAILS.Rechallenge_Dose%TYPE;
  	l_Distbn_Date CASE_DETAILS.Distbn_Date%TYPE;
  	l_Submission_Date CASE_DETAILS.Submission_Date%TYPE;
  	l_Further_Commn CASE_DETAILS.Further_Commn%TYPE;
    l_Is_Suspended CASE_DETAILS.Is_Suspended%TYPE;
    l_Short_Comment CASE_DETAILS.Short_Comment%TYPE;
    l_Short_History CASE_DETAILS.Short_History%TYPE;
    l_Verbatim CASE_DETAILS.Verbatim%TYPE;
    l_AE_Desc CASE_DETAILS.AE_Desc%TYPE;
    l_Max_No MAX_NO.MAX_NO%TYPE;
    l_cnt NUMBER;
    
    CURSOR cur_Ae IS
    SELECT Diag_Code,Llt_Code,Outcome_Code,Body_Sys_Num,Classification_Code,Eseverity_Code,Reported_To_Lha,
           Lha_Code,Report_Date,Date_Start,Date_Stop,Labelling,Prev_Report_Flg,Prev_Report,Event_Treatment,
	       Event_Abate_On_Stop,Aggr_Of_Diag,Aggr_Of_Llt
	  FROM Adverse_Event
	 WHERE Rep_Num=p_Rep_Num;
	 
    CURSOR cur_Cm IS
    SELECT Product_Code,Indication_Num,Treat_Start,Treat_Stop
	  FROM Conc_Medications
	 WHERE Rep_Num=p_Rep_Num;
	 
BEGIN

	SELECT Age_Reported, Phy_Assessment_Code, Co_Assessment_Code, Authorization_No, Autopsy,Autopsy_Result, 
	       Batch_Num, Clin_Rep_Info, Comp_Rep_Info, Contributing_Factors,Country_Code, Co_Assessment_Date,
	       Co_Assessment_Reason, Date_Entry, Date_Of_Birth,Date_Recv, Dfreq_Code, Subm_Company_Nm, 
	       Distbn_Date, Dose, Dregimen_Code, Ethnic_Code,Formulation_Code, Further_Commn, 
	       Height, Hosp_Nm, Indication_Num, Is_Suspended, M_Code,Outcome_Code, Outcome_Rep_Date, 
	       Patient_Num, Phy_Assessment_Date, Phy_Assessment_Reason,Pregnancy, Pregnancy_Week, 
	       Product_Code, P_Initials, Rechallenge_Dose, Rechallenge,Rechallenge_Date, 
	       Recv_By_At_Hq, Rep_Num_Local, Rep_Xref_Num,Result_Code, Route_Code, 
	       Sex_Code, Short_Comment, Short_History, Source_Code,Status_Code, Submission_Date, 
	       Subm_Lha_Code, Treat_Start, Treat_Stop, TRIAL_NUM,Type_Code, Unit_Code, Verbatim, 
	       Weight, AE_Desc
	  INTO l_Age_Reported, l_Phy_Assessment_Code, l_Co_Assessment_Code, l_Authorization_No, l_Autopsy,
	       l_Autopsy_Result, l_Batch_Num, l_Clin_Rep_Info, l_Comp_Rep_Info, l_Contributing_Factors,
	       l_Country_Code, l_Co_Assessment_Date, l_Co_Assessment_Reason, l_Date_Entry, l_Date_Of_Birth,
	       l_Date_Recv, l_Dfreq_Code, l_Subm_Company_Nm, l_Distbn_Date, l_Dose, l_Dregimen_Code, 
	       l_Ethnic_Code,l_Formulation_Code, l_Further_Commn, l_Height, l_Hosp_Nm, l_Indication_Num, 
	       l_Is_Suspended, l_M_Code,l_Outcome_Code, l_Outcome_Rep_Date, l_Patient_Num, 
	       l_Phy_Assessment_Date, l_Phy_Assessment_Reason,l_Pregnancy, l_Pregnancy_Week, 
	       l_Product_Code, l_P_Initials,l_Rechallenge_Dose, l_Rechallenge,l_Rechallenge_Date, 
	       l_Recv_By_At_Hq, l_Rep_Num_Local, l_Rep_Xref_Num,l_Result_Code, 
	       l_Route_Code, l_Sex_Code, l_Short_Comment, l_Short_History, l_Source_Code,
	       l_Status_Code,l_Submission_Date, l_Subm_Lha_Code, l_Treat_Start, l_Treat_Stop, 
	       l_TRIAL_NUM,l_Type_Code, l_Unit_Code, l_Verbatim,l_Weight, l_AE_Desc
	  FROM Case_Details
	 WHERE Rep_Num=p_Rep_Num;
	       
	Save_Case_Details('I', p_Username, p_Rep_Num_New, l_Type_Code, l_Trial_Num, l_Country_Code, l_Status_Code, l_Source_Code, l_M_Code, l_Formulation_Code, l_Unit_Code, l_DRegimen_Code, l_DFreq_Code, l_Route_Code, l_Indication_Num, l_Sex_Code, l_Ethnic_Code, l_Outcome_Code, l_Product_Code, l_Phy_Assessment_Code, l_Co_Assessment_Code, l_Result_Code, l_Subm_Company_Nm, l_Subm_LHA_Code, l_Date_Entry, l_Recv_By_At_Hq, l_Date_Recv, l_Rep_Num_Local, l_Rep_Xref_Num, l_Clin_Rep_Info, l_Comp_Rep_Info, l_Patient_Num, l_Hosp_Nm, l_Batch_Num, l_Dose, l_Treat_Start, l_Treat_Stop, l_Rechallenge, l_Authorization_No, l_P_Initials, l_Date_Of_Birth, l_Age_Reported, l_Weight, l_Height, l_Pregnancy, l_Pregnancy_Week, l_Short_History, l_Outcome_Rep_Date, l_Autopsy, l_Autopsy_Result, l_Phy_Assessment_Date, l_Co_Assessment_Date, l_Phy_Assessment_Reason, l_Co_Assessment_Reason, l_Contributing_Factors, l_Rechallenge_Date, l_Rechallenge_Dose, l_Verbatim, l_SHORT_COMMENT, l_Distbn_Date, l_Submission_Date, l_Further_Commn, l_AE_Desc, l_Is_Suspended);
	
	FOR fetch_Ae IN cur_Ae
	LOOP
		Save_Adverse_Event('I',p_Username,p_Rep_Num_New,fetch_Ae.Diag_Code,Fetch_Ae.LLT_Code,Fetch_Ae.Outcome_Code,Fetch_Ae.Body_Sys_Num,Fetch_Ae.Classification_Code,Fetch_Ae.ESeverity_Code,Fetch_Ae.Reported_To_LHA,Fetch_Ae.LHA_Code,Fetch_Ae.Report_Date,Fetch_Ae.Date_Start,Fetch_Ae.Date_Stop,Fetch_Ae.Labelling,Fetch_Ae.Prev_Report_Flg,Fetch_Ae.Prev_Report,Fetch_Ae.Event_Treatment,Fetch_Ae.Event_Abate_On_Stop,Fetch_Ae.Aggr_Of_Diag,Fetch_Ae.Aggr_Of_LLT);
	END LOOP;
	
	FOR fetch_Cm IN cur_Cm
	LOOP
		Save_Conc_Medications('I',p_Username,p_Rep_Num_New,fetch_Cm.Product_Code,fetch_Cm.Indication_Num,fetch_Cm.Treat_Start,fetch_Cm.Treat_Stop);
	END LOOP;

END;
--------------------------------------------
END sapher_pkg;
/

prompt
prompt Creating trigger UPDATE_AE_ORDER
prompt ================================
prompt
CREATE OR REPLACE TRIGGER UPDATE_AE_ORDER 
BEFORE INSERT ON ADVERSE_EVENT 
FOR EACH ROW 
declare
begin
  SELECT MAX(NVL(AE_Order,0)) + 1
    INTO :New.Ae_Order
    FROM Adverse_Event
   WHERE Rep_Num = :NEW.Rep_Num;
end;
/

prompt
prompt Creating trigger UPDATE_CM_ORDER
prompt ================================
prompt
create or replace trigger Update_Cm_Order
  before insert on Conc_Medications  
  for each row
declare
begin
  SELECT MAX(NVL(CM_Order,0)) + 1
    INTO :New.CM_Order
    FROM Conc_Medications
   WHERE Rep_Num = :NEW.Rep_Num;
end;
/

exit

spool off
