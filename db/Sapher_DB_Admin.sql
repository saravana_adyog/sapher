--------------------------------------------
-- DB Create scripts for user SAPHER ADMIN--
-- Created by Arun P. Jose on 13-MAR-2008 --
--------------------------------------------

set echo on
set verify on
set serveroutput on size 100000

spool Sapher_DB_Admin.log

select * from global_name;
sho user;
--------------------------------------------
create or replace directory &&2 as '&&1';
--------------------------------------------
create global temporary table TMP_LIST(
  CONTENT VARCHAR2(2000)
)on commit preserve rows;
--------------------------------------------
create or replace and compile java source named dirlist as
import java.io.*;
import java.sql.*;

public class DirList
{
 public static void getList(String directory) throws SQLException
 {
  File path = new File (directory);
  String[] list = path.list();
  String element;
  
  for (int i=0; i<list.length; i++)
  {
   element = list[i];
   #sql { INSERT INTO TMP_LIST (CONTENT) VALUES (:element) };
  }
 }
}
/
--------------------------------------------
create or replace and compile java source named readlogfile as
import java.io.*;
import java.sql.*;

public class ReadLogFile
{
 public static void readFile(String filename) throws SQLException,FileNotFoundException, IOException
 {
    BufferedReader input =  new BufferedReader(new FileReader(new File(filename)));
    String line = null;
    while (( line = input.readLine()) != null){
      #sql { INSERT INTO TMP_LIST (CONTENT) VALUES (:line) };
    }
    input.close();
  }
}
/
--------------------------------------------
create or replace
PACKAGE Sapadmin_Pkg  AUTHID CURRENT_USER IS
PROCEDURE Imp_User(
	P_Dmp_File	IN VARCHAR2,
	P_FromUser	IN VARCHAR2,
	P_ToUser		IN VARCHAR2);
PROCEDURE Exp_User(
	P_Username		IN VARCHAR2);
 PROCEDURE Get_Dir_List_J(
  p_Directory IN VARCHAR2);
PROCEDURE Get_Dir_List(
        p_Type    IN VARCHAR2,
        p_From    IN VARCHAR2,
        p_To      IN VARCHAR2,
        p_DirList OUT SYS_REFCURSOR);
PROCEDURE Read_Log_File_J(p_FileName IN VARCHAR2);
PROCEDURE Read_Log_File(p_FileName IN VARCHAR2, p_FileContent OUT SYS_REFCURSOR);
PROCEDURE Save_Scheduled_Backup(p_Save_Flag IN CHAR, p_job_name IN VARCHAR2, p_user_name IN VARCHAR2, p_start_date IN DATE, p_end_date IN DATE, p_repeat_interval IN VARCHAR2);
PROCEDURE Get_Scheduled_Backup(p_jobName IN VARCHAR2, Cur_Backup_List OUT SYS_REFCURSOR);
PROCEDURE Delete_Scheduled_Backup(p_job_name IN VARCHAR2);
PROCEDURE Get_Backup_Dir_Name(Cur_Dir_Name OUT SYS_REFCURSOR);
END;
/
--------------------------------------------
create or replace
PACKAGE BODY Sapadmin_Pkg IS
---------------------------------------------
-- IMPORTING
---------------------------------------------
PROCEDURE Imp_User(
	P_Dmp_File	IN VARCHAR2,
	P_FromUser	IN VARCHAR2,
	P_ToUser	IN VARCHAR2)
 AS
    TYPE typ_cur IS REF CURSOR;
    cr_ToUserSession typ_cur;
    var_sid     NUMBER;
    var_serial  NUMBER;
	l_Handle 	NUMBER;
    l_job_state VARCHAR2(30);
    l_stat      ku$_Status;
    l_cnt       NUMBER(4);
BEGIN
  SELECT COUNT(*)
    INTO l_cnt
    FROM all_users
   WHERE UPPER(Username) = UPPER(P_ToUser);
  IF l_cnt <> 0 THEN
       OPEN cr_ToUserSession FOR
    SELECT sid, serial# FROM v$session WHERE username = UPPER(P_ToUser);
       LOOP
            FETCH cr_ToUserSession INTO var_sid,var_serial;
             EXIT WHEN cr_ToUserSession%NOTFOUND;
          EXECUTE IMMEDIATE 'ALTER SYSTEM KILL SESSION '''||var_sid||','||var_serial||'''';
        END LOOP;
    EXECUTE IMMEDIATE 'DROP USER ' || p_ToUser || ' CASCADE';
    NULL;
  END IF;

 	l_Handle := DBMS_DATAPUMP.OPEN('IMPORT','SCHEMA');
	DBMS_DATAPUMP.ADD_FILE(l_Handle,P_Dmp_File,'&&2');
	DBMS_DATAPUMP.ADD_FILE(handle => l_Handle,filename => TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')||'_'||P_ToUser||'_RESTORE.LOG',DIRECTORY => '&&2',filetype => DBMS_DATAPUMP.KU$_FILE_TYPE_LOG_FILE);
	dbms_datapump.metadata_remap(l_Handle,'REMAP_SCHEMA',p_FromUser,p_ToUSer); 
	DBMS_DATAPUMP.SET_PARAMETER(l_Handle,'TABLE_EXISTS_ACTION','REPLACE');
	DBMS_DATAPUMP.START_JOB(l_Handle);
  l_job_state := 'UNDEFINED';
  WHILE (l_job_state != 'COMPLETED') AND (l_job_state != 'STOPPED') LOOP
    dbms_datapump.get_status(l_Handle, dbms_datapump.ku$_status_job_error +
    dbms_datapump.ku$_status_job_status +
    dbms_datapump.ku$_status_wip, -1, l_job_state, l_stat);
  END LOOP;
	DBMS_DATAPUMP.DETACH(l_Handle);
END;
---------------------------------------------
-- EXPORTING
---------------------------------------------
PROCEDURE Exp_User(
	P_Username		IN VARCHAR2)
 AS
	L_Dp_Handle	NUMBER;
  l_job_state VARCHAR2(30);
  l_stat      ku$_Status;
  l_Filename VARCHAR2(100) := TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')||'_'||P_Username;
BEGIN
  L_Dp_Handle := DBMS_DATAPUMP.OPEN('EXPORT','SCHEMA');
  DBMS_DATAPUMP.ADD_FILE(handle => L_Dp_Handle,filename => l_Filename||'.DMP',DIRECTORY => '&&2');
  DBMS_DATAPUMP.ADD_FILE(handle => L_Dp_Handle,filename => l_Filename||'.LOG',DIRECTORY => '&&2',filetype => DBMS_DATAPUMP.KU$_FILE_TYPE_LOG_FILE);
  DBMS_DATAPUMP.METADATA_FILTER(handle => L_Dp_Handle,name   => 'SCHEMA_EXPR',value  => '= '''||UPPER(P_Username)||'''');
  DBMS_DATAPUMP.START_JOB(L_Dp_Handle);
  l_job_state := 'UNDEFINED';
  WHILE (l_job_state != 'COMPLETED') AND (l_job_state != 'STOPPED') LOOP
    dbms_datapump.get_status(L_Dp_Handle, dbms_datapump.ku$_status_job_error +
    dbms_datapump.ku$_status_job_status +
    dbms_datapump.ku$_status_wip, -1, l_job_state, l_stat);
  END LOOP;
  DBMS_DATAPUMP.DETACH(L_Dp_Handle);
END;
---------------------------------------------
-- DIR CONTENTS
---------------------------------------------
PROCEDURE Get_Dir_List_J(p_Directory IN VARCHAR2)
  AS LANGUAGE JAVA
  NAME 'DirList.getList(java.lang.String)';
---------------------------------------------
PROCEDURE Get_Dir_List(
        p_Type    IN VARCHAR2,
        p_From    IN VARCHAR2,
        p_To      IN VARCHAR2,
        p_DirList OUT SYS_REFCURSOR)
AS
BEGIN
  DELETE FROM TMP_LIST;
  Get_Dir_List_J('&&1');
    OPEN p_DirList FOR
  SELECT Content
    FROM Tmp_List
   WHERE (p_Type IS NULL
      OR Instr(Content,p_Type) > 1)
     AND (p_From IS NULL
      OR to_date(Substr(Content,0,12),'YYYYMMDDHH24MI') >= to_date(p_From,'DD-MON-YYYY HH24:MI'))
     AND (p_To IS NULL
      OR to_date(Substr(Content,0,12),'YYYYMMDDHH24MI') <= to_date(p_To,'DD-MON-YYYY HH24:MI'))
   ORDER BY Content DESC;
  COMMIT;
END;
---------------------------------------------
PROCEDURE Get_Backup_Dir_Name(Cur_Dir_Name OUT SYS_REFCURSOR)
AS
BEGIN
	OPEN Cur_Dir_Name FOR
  SELECT Directory_Path 
    FROM All_Directories 
   WHERE Directory_Name = '&&2';
END;
---------------------------------------------
-- READ LOG FILE
---------------------------------------------
PROCEDURE Read_Log_File_J(p_FileName IN VARCHAR2)
  AS LANGUAGE JAVA
  NAME 'ReadLogFile.readFile(java.lang.String)';
---------------------------------------------
PROCEDURE Read_Log_File(p_FileName IN VARCHAR2, p_FileContent OUT SYS_REFCURSOR)
AS
BEGIN
 DELETE FROM TMP_LIST;
  Read_Log_File_J('&&1\'||p_FileName);
    OPEN p_FileContent FOR
  SELECT Content
    FROM TMP_LIST;
  COMMIT;
END;
---------------------------------------------
-- Procedures for Backup Schedule 
---------------------------------------------
PROCEDURE Save_Scheduled_Backup(p_Save_Flag IN CHAR, p_job_name IN VARCHAR2, p_user_name IN VARCHAR2, p_start_date IN DATE, p_end_date IN DATE, p_repeat_interval IN VARCHAR2) AS
l_job_name VARCHAR2(50);                                                                                                                                                         
BEGIN                                                                                                                                                                            
  IF p_Save_Flag = 'I' THEN                                                                                                                                                      
  BEGIN                                                                                                                                                                          
    l_job_name :='&&3'||'_'||to_char(sysdate,'YYYYMMDDHH24MISS');                                                                                                          
    DBMS_SCHEDULER.CREATE_JOB(                                                                                                                                                   
      job_name        => l_job_name,                                                                                                                                             
      job_type        => 'STORED_PROCEDURE',                                                                                                                                     
      job_action      => 'SAPADMIN_PKG.Exp_User',                                                                                                                                
      number_of_arguments => 1,                                                                                                                                                  
      start_date      => p_start_date,                                                                                                                                           
      repeat_interval => p_repeat_interval,                                                                                                                                      
      end_date        => p_end_date,                                                                                                                                             
      enabled         => FALSE,                                                                                                                                                  
      comments        => 'Sapher scheduled backup');                                                                                                                             
                                                                                                                                                                                 
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE(                                                                                                                                       
      job_name                => l_job_name,                                                                                                                                     
      argument_position       => 1,                                                                                                                                              
      argument_value          => p_user_name);                                                                                                                                   
                                                                                                                                                                                 
    DBMS_SCHEDULER.enable(l_job_name);                                                                                                                                           
  END;                                                                                                                                                                           
  ELSIF p_Save_Flag = 'U' THEN                                                                                                                                                   
  BEGIN                                                                                                                                                                          
    DBMS_SCHEDULER.DROP_JOB(job_name=>p_job_name);                                                                                                                               
    DBMS_SCHEDULER.CREATE_JOB(                                                                                                                                                   
      job_name        => p_job_name,                                                                                                                                             
      job_type        => 'STORED_PROCEDURE',                                                                                                                                     
      job_action      => 'SAPADMIN_PKG.Exp_User',                                                                                                                                
      number_of_arguments => 1,                                                                                                                                                  
      start_date      => p_start_date,                                                                                                                                           
      repeat_interval => p_repeat_interval,                                                                                                                                      
      end_date        => p_end_date,                                                                                                                                             
      enabled         => FALSE,                                                                                                                                                  
      comments        => 'Sapher scheduled backup');                                                                                                                             
                                                                                                                                                                                 
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE(                                                                                                                                       
      job_name                => p_job_name,                                                                                                                                     
      argument_position       => 1,                                                                                                                                              
      argument_value          => p_user_name);                                                                                                                                   
                                                                                                                                                                                 
    DBMS_SCHEDULER.enable(p_job_name);                                                                                                                                           
  END;                                                                                                                                                                           
  END IF;                                                                                                                                                                        
END;
---------------------------------------------
PROCEDURE Get_Scheduled_Backup(p_jobName IN VARCHAR2, Cur_Backup_List OUT SYS_REFCURSOR) AS
BEGIN
    OPEN Cur_Backup_List FOR
  SELECT a.job_name, b.VALUE, a.start_date, a.repeat_interval, a.end_date, to_char(a.last_start_date, 'DD-MON-YYYY') as last_start_date, to_char(a.next_run_date, 'DD-MON-YYYY') as next_run_date, a.state
    FROM DBA_SCHEDULER_JOBS a, DBA_SCHEDULER_JOB_ARGS b
   WHERE a.job_name LIKE '&&3%'
     AND a.job_name = b.job_name
     AND (p_jobName IS NULL OR a.job_Name = p_jobName);
END;
---------------------------------------------
PROCEDURE Delete_Scheduled_Backup(p_job_name IN VARCHAR2) AS
BEGIN
  DBMS_SCHEDULER.DROP_JOB(job_name=>p_job_name);
END;
---------------------------------------------
END Sapadmin_Pkg;
/

exit;



spool off
