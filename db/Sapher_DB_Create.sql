--------------------------------------------
-- DB Create scripts for user SAPHER      --
-- Created by Arun P. Jose on 05-MAR-2008 --
--------------------------------------------

set echo on
set verify on
set serveroutput on size 100000

spool Sapher_DB_Create.log
alter session set "_oracle_script"=true;
select * from global_name;
sho user;

--------------------------------------------
-- Create the App user for Sapher         --
--------------------------------------------

create tablespace &&3
datafile
     '&&10\&&3..f1.dbf'
    size 100M
  autoextend on
        next 10M
        maxsize UNLIMITED,
     '&&10\&&3..f2.dbf'
    size 100M
  autoextend on
        next 10M
        maxsize UNLIMITED
  extent management local;        

create tablespace &&4
datafile
     '&&10\&&4..dbf'
    size 100M
  autoextend on
        next 10M
        maxsize UNLIMITED
  extent management local;        


create tablespace &&11
datafile
     '&&10\&&11..dbf'
    size 100M
  autoextend on
        next 10M
        maxsize UNLIMITED
  extent management local;
  
create temporary tablespace &&5
tempfile 
  '&&10\&&5..dbf'
  size 50M 
  autoextend on 
  next 10M 
  maxsize 2048M
  extent management local;

create user &&1
identified by &&2
default tablespace &&3
temporary tablespace &&5;

grant connect, resource, create view, create any directory to &&1;

--------------------------------------------
-- Create the Admin user for Sapher       --
--------------------------------------------

create tablespace &&8
datafile
     '&&10\&&8..dbf'
    size 100M
  autoextend on
        next 10M
        maxsize UNLIMITED
  extent management local;        

create user &&6
identified by &&7
default tablespace &&8
temporary tablespace &&5;

grant connect, resource, dba to &&6;

grant select on V_$SESSION to &&6;
grant select on DBA_SCHEDULER_JOBS to &&6;
grant select on DBA_SCHEDULER_JOB_ARGS to &&6;

begin
dbms_java.grant_permission('&&6','SYS:java.io.FilePermission','&&9\*','read');
end;
/

begin
dbms_java.grant_permission('&&6','SYS:java.io.FilePermission','&&9','read');
end;
/

--------------------------------------------

exit

spool off
