@ECHO off

REM ******************************************
REM *****   Sapher database Creation     *****
REM *****   Arun P. Jose, Focal3         *****
REM *****   05-MAR-2008                  *****
REM ******************************************

REM ******************************************
REM *****  Variables Set here            *****
REM ******************************************

SET SAdminUser=SAPBETAADMN
SET SAdminPwd=SAPBETAADMN
SET SAdminTbs=SAPBETAADMN_TBS
SET SDocumentTbs=SAPHER_DOCS_TBS

SET SUser=SAPBETA
SET SPwd=SAPBETA
SET STbs=SAPBETA_TBS
SET SIdx_Tbs=SAPBETA_IDX

SET STemp_Tbs=SAPBETA_TMP
SET DirName=SAPBKPBETA
SET SDir='E:\Source\SAPHER\bkp'
SET JobName=SAPHERJOB
SET DocDirName=SAPHER_DOCS_DIR
SET DocDir='E:\Source\SAPHER\docs'

SET DB_Con=
SET SYSDBA=sys/Password88
SET ORAHOME=C:\app\sapbeta\oradata\orcl

SET License=ABCDE

REM ******************************************
REM *****  Script executed here          *****
REM ******************************************

choice /C:YN /M "Execute Sapher_DB_Drop.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SYSDBA%%DB_Con% as sysdba @Sapher_DB_Drop.sql %SUser% %STbs% %SIdx_Tbs% %STemp_Tbs% %SAdminUser% %SDocumentTbs% %SAdminTbs%
:endscript

choice /C:YN /M "Execute Sapher_DB_Create.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SYSDBA%%DB_Con% as sysdba @Sapher_DB_Create.sql %SUser% %SPwd% %STbs% %SIdx_Tbs% %STemp_Tbs% %SAdminUser% %SAdminPwd% %SAdminTbs% %SDir% %ORAHOME% %SDocumentTbs%
:endscript

choice /C:YN /M "Execute Sapher_DB_Admin.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SAdminUser%/%SAdminPwd%%DB_Con% @Sapher_DB_Admin.sql %SDir% %DirName% %JobName%
:endscript

choice /C:YN /M "Execute Sapher_DB_Objects.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SUser%/%SPwd%%DB_Con% @Sapher_DB_Objects.sql %SIdx_Tbs% 
:endscript

choice /C:YN /M "Execute Sapher_DB_PLSQL.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SUser%/%SPwd%%DB_Con% @Sapher_DB_PLSQL.sql
:endscript

choice /C:YN /M "Execute Sapher_DB_DefValues.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SUser%/%SPwd%%DB_Con% @Sapher_DB_DefValues.sql %License%
:endscript

choice /C:YN /M "Execute Sapher_Documents.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SUser%/%SPwd%%DB_Con% @Sapher_Documents.sql %SDocumentTbs% %DocDirName% %DocDir% %SIdx_Tbs% 
:endscript


REM ******************************************
REM *****  MEDDRA UPDATES executed here   ****
REM ******************************************

SET MeddraDIR=MEDDRA
SET MeddraDIRName='E:\Source\SAPHER\meddra'

choice /C:YN /M "Execute Sapher_MEDDRA_ADMN.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SAdminUser%/%SAdminPwd%%DB_Con% @Sapher_MEDDRA_ADMN.sql %MeddraDIRName% %MeddraDIR%
:endscript

choice /C:YN /M "Execute Sapher_Meddra.sql"
IF %ERRORLEVEL% ==1 goto startscript
IF %ERRORLEVEL% ==2 goto endscript

:startscript
sqlplus %SUser%/%SPwd%%DB_Con% @Sapher_Meddra.sql %SIdx_Tbs%
:endscript

exit

