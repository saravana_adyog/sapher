---------------------------------------------
-- Document manipulation script            --
-- Created by Sujith Sundar on 05-JUN-2009 --
---------------------------------------------

set echo on
set verify on
set serveroutput on size 100000

spool Sapher_Documents.log

select * from global_name;
show user;


-----------------------------------------------------------------
create or replace directory &&2 as '&&3';
-----------------------------------------------------------------


prompt
prompt Creating table SAPHER_DOCS
prompt ======================
prompt
CREATE TABLE Sapher_Docs 
		(Rep_Num  	NUMBER,
		Doc_Id   	NUMBER,
		Doc_Title 	VARCHAR2(255),
		Doc_Type  	VARCHAR2(10),
		Doc_Size 	VARCHAR2(20),
		Doc_Desc 	VARCHAR2(100),
		Doc_Blob  	BLOB DEFAULT EMPTY_BLOB(),
		Is_Valid  	NUMBER(1))
TABLESPACE &&1;
		
ALTER TABLE Sapher_Docs ADD CONSTRAINT PK_SAPHER_DOCS PRIMARY KEY (doc_id, rep_num) USING INDEX TABLESPACE &&4;

alter table Sapher_Docs 
  add constraint FK_Sapher_Docs_CASE_DETAILS foreign key (Rep_Num)
  references CASE_DETAILS(rep_num) on delete cascade;


create or replace
PACKAGE Sapher_Docs_Pkg as
PROCEDURE save_doc(
	p_Username 	IN USER_LIST.Username%TYPE,
	p_rep_num 	IN SAPHER_DOCS.Rep_Num%TYPE, 
	p_doc_title IN SAPHER_DOCS.Doc_Title%TYPE, 
	p_doc_desc 	IN SAPHER_DOCS.Doc_Desc%TYPE, 
	p_is_valid 	IN SAPHER_DOCS.Is_Valid%TYPE);
	
PROCEDURE get_doc(
	p_rep_num 	IN SAPHER_DOCS.Rep_Num%TYPE,
	p_doc_id 	IN SAPHER_DOCS.Doc_Id%TYPE, 
	p_doc_title IN SAPHER_DOCS.Doc_Title%TYPE, 
	p_doc_type  IN SAPHER_DOCS.Doc_Type%TYPE,
	p_doc_desc	IN SAPHER_DOCS.Doc_Desc%TYPE,
	p_is_valid	IN SAPHER_DOCS.Is_Valid%TYPE,
	cur_doc 	OUT SYS_REFCURSOR);
	
PROCEDURE del_doc(
	p_Username 	IN USER_LIST.Username%TYPE,
	p_rep_num IN SAPHER_DOCS.Rep_Num%TYPE, 
	p_doc_id IN SAPHER_DOCS.Doc_Id%TYPE);
END Sapher_Docs_Pkg;
/

create or replace
PACKAGE BODY Sapher_Docs_Pkg AS
--------------------------------------------------------
PROCEDURE save_doc(
	p_Username 	IN USER_LIST.Username%TYPE,
	p_rep_num 	IN SAPHER_DOCS.Rep_Num%TYPE, 
	p_doc_title IN SAPHER_DOCS.Doc_Title%TYPE, 
	p_doc_desc 	IN SAPHER_DOCS.Doc_Desc%TYPE, 
	p_is_valid 	IN SAPHER_DOCS.Is_Valid%TYPE) 
IS
	v_doc_id	SAPHER_DOCS.Doc_Id%TYPE;
	v_doc_type	SAPHER_DOCS.Doc_Type%TYPE;
	v_doc_size	SAPHER_DOCS.Doc_Size%TYPE;
	dest_loc    BLOB := empty_blob();
	src_loc	    BFILE;
BEGIN
	SELECT NVL(MAX(DOC_ID),0) INTO v_doc_id
      FROM Sapher_Docs
     WHERE rep_num = p_rep_num;

	v_doc_id := v_doc_id + 1;
  
  	v_doc_type := UPPER(SUBSTR(p_doc_title,INSTR(p_doc_title,'.',-1)+1));
  	src_loc := BFILENAME('&&2', p_doc_title);
  	v_doc_size := ROUND(DBMS_LOB.GETLENGTH(src_loc)/1024, 2)||' KB';
  	
  	INSERT INTO Sapher_Docs(rep_num, doc_id, doc_title, doc_type, doc_size, doc_desc, is_valid) 
    VALUES (p_rep_num, v_doc_id, p_doc_title, v_doc_type, v_doc_size, p_doc_desc, p_is_valid);
  	
    SELECT doc_blob INTO dest_loc 
      FROM Sapher_Docs 
     WHERE doc_id = v_doc_id AND rep_num = p_rep_num
       FOR UPDATE;
       
  	DBMS_LOB.OPEN(src_loc, DBMS_LOB.LOB_READONLY);
  	DBMS_LOB.OPEN(dest_loc, DBMS_LOB.LOB_READWRITE);
  	DBMS_LOB.LOADFROMFILE(dest_loc, src_loc, DBMS_LOB.getLength(src_loc));
  	DBMS_LOB.CLOSE(dest_loc);
  	DBMS_LOB.CLOSE(src_loc);
  	
	SAPHER_PKG.Save_Audit(p_Username,'CASE','I','CSD4',p_rep_num||','||p_doc_title,NULL,NULL,NULL);
	COMMIT;
END save_doc;
--------------------------------------------------------
PROCEDURE get_doc(
	p_rep_num 	IN SAPHER_DOCS.Rep_Num%TYPE,
	p_doc_id 	IN SAPHER_DOCS.Doc_Id%TYPE, 
	p_doc_title IN SAPHER_DOCS.Doc_Title%TYPE, 
	p_doc_type  IN SAPHER_DOCS.Doc_Type%TYPE,
	p_doc_desc	IN SAPHER_DOCS.Doc_Desc%TYPE,
	p_is_valid	IN SAPHER_DOCS.Is_Valid%TYPE,
	cur_doc 	OUT SYS_REFCURSOR) 
IS
BEGIN
    OPEN cur_doc FOR
  SELECT rep_num, doc_id, doc_title, doc_type, Doc_Size, Doc_Desc, doc_blob, Is_Valid
    FROM Sapher_Docs 
   WHERE rep_num = p_rep_num
     AND (p_doc_id = -1 OR doc_id = p_doc_id)
     AND (p_doc_title IS NULL OR UPPER(doc_title) = UPPER(p_doc_title))
     AND (p_doc_type IS NULL OR UPPER(doc_type) = UPPER(p_doc_type))
     AND (p_doc_desc IS NULL OR UPPER(doc_desc) LIKE '%'||UPPER(p_doc_desc)||'%')
     AND (p_is_valid = -1 OR Is_Valid = p_is_valid)
   ORDER BY rep_num, doc_id;
END get_doc;
--------------------------------------------------------
PROCEDURE del_doc(
	p_Username 	IN USER_LIST.Username%TYPE,
	p_rep_num 	IN SAPHER_DOCS.Rep_Num%TYPE, 
	p_doc_id 	IN SAPHER_DOCS.Doc_Id%TYPE) 
IS
	v_doc_title SAPHER_DOCS.Doc_Title%TYPE;
BEGIN
	SELECT Doc_Title INTO v_doc_title
	  FROM Sapher_Docs
	 WHERE rep_num = p_rep_num AND doc_id = p_doc_id;
	
	DELETE Sapher_Docs
	 WHERE rep_num = p_rep_num AND doc_id = p_doc_id;
	 
	SAPHER_PKG.Save_Audit(p_Username,'CASE','D','CSD4',p_rep_num||','||v_doc_title,NULL,NULL,NULL);
    COMMIT;
END del_doc;
--------------------------------------------------------
END Sapher_Docs_Pkg;
/

exit;
spool off
