--------------------------------------------
-- DB Objects for user SAPHER             --
-- Created by Arun P. Jose on 05-MAR-2008 --
--------------------------------------------

set echo on
set verify on
set serveroutput on size 100000

spool Sapher_DB_Objects.log

select * from global_name;
sho user;


prompt
prompt Creating table AE_DIAG
prompt ======================
prompt
create table AE_DIAG
(
  DIAG_CODE VARCHAR2(12) not null,
  DIAG_DESC VARCHAR2(500) not null,
  IS_VALID  NUMBER(1)
)
;
alter table AE_DIAG
  add constraint PK_AE_DIAG primary key (DIAG_CODE) using index tablespace &&1;
alter table AE_DIAG
  add constraint UK_AE_DIAG unique (DIAG_DESC) using index tablespace &&1;
alter table AE_DIAG
  add constraint CK_IS_VALID_DIAG
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table AE_LLT
prompt =====================
prompt
create table AE_LLT
(
  LLT_CODE VARCHAR2(12) not null,
  LLT_DESC VARCHAR2(500) not null,
  IS_VALID NUMBER(1)
)
;
alter table AE_LLT
  add constraint PK_AE_LLT primary key (LLT_CODE) using index tablespace &&1;
alter table AE_LLT
  add constraint UK_AE_LLT unique (LLT_DESC) using index tablespace &&1;
alter table AE_LLT
  add constraint CK_IS_VALID_LLT
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table AE_OUTCOME
prompt =========================
prompt
create table AE_OUTCOME
(
  OUTCOME_CODE VARCHAR2(3) not null,
  OUTCOME      VARCHAR2(50) not null,
  IS_VALID     NUMBER(1)
)
;
alter table AE_OUTCOME
  add constraint PK_AE_OUTCOME primary key (OUTCOME_CODE) using index tablespace &&1;
alter table AE_OUTCOME
  add constraint UK_AE_OUTCOME unique (OUTCOME) using index tablespace &&1;
alter table AE_OUTCOME
  add constraint CK_IS_VALID_OUTCOME
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table BODY_SYSTEM
prompt ==========================
prompt
create table BODY_SYSTEM
(
  BODY_SYS_NUM VARCHAR2(12) not null,
  BODY_SYSTEM  VARCHAR2(500) not null,
  IS_VALID     NUMBER(1)
)
;
alter table BODY_SYSTEM
  add constraint PK_BODY_SYSTEM primary key (BODY_SYS_NUM) using index tablespace &&1;
alter table BODY_SYSTEM
  add constraint UK_BODY_SYSTEM unique (BODY_SYSTEM) using index tablespace &&1;
alter table BODY_SYSTEM
  add constraint CK_IS_VALID_BODYSYS
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table COUNTRY
prompt ======================
prompt
create table COUNTRY
(
  COUNTRY_CODE VARCHAR2(3) not null,
  COUNTRY_NM   VARCHAR2(50) not null,
  IS_VALID     NUMBER(1)
)
;
alter table COUNTRY
  add constraint PK_COUNTRY primary key (COUNTRY_CODE) using index tablespace &&1;
alter table COUNTRY
  add constraint UK_COUNTRY_NM unique (COUNTRY_NM) using index tablespace &&1;
alter table COUNTRY
  add constraint CK_IS_VALID_COUNTRY
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table DOSE_FREQUENCY
prompt =============================
prompt
create table DOSE_FREQUENCY
(
  DFREQ_CODE VARCHAR2(4) not null,
  DFREQ_DESC VARCHAR2(50) not null,
  IS_VALID   NUMBER(1)
)
;
alter table DOSE_FREQUENCY
  add constraint PK_DOSE_FREQUENCY primary key (DFREQ_CODE) using index tablespace &&1;
alter table DOSE_FREQUENCY
  add constraint UK_DOSE_FREQUENCY unique (DFREQ_DESC) using index tablespace &&1;
alter table DOSE_FREQUENCY
  add constraint CK_IS_VALID_DFREQ
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table INTERVAL
prompt =======================
prompt
create table INTERVAL
(
  INTERVAL_CODE NUMBER(3) not null,
  INTERVAL      VARCHAR2(20) not null,
  IS_VALID      NUMBER(1)
)
;

alter table INTERVAL
  add constraint PK_INTERVAL primary key (INTERVAL_CODE) using index tablespace &&1;
alter table INTERVAL
  add constraint CK_IS_VALID_INTERVAL check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table DOSE_REGIMEN
prompt ===========================
prompt
create table DOSE_REGIMEN
(
  DREGIMEN_CODE VARCHAR2(2) not null,
  DREGIMEN_DESC VARCHAR2(50) not null,
  IS_VALID      NUMBER(1),
  NO_OF_DOSAGE    NUMBER(3) not null,
  DOSAGE_INTERVAL NUMBER(3) not null
)
;

alter table DOSE_REGIMEN
  add constraint PK_DOSE_REGIMEN primary key (DREGIMEN_CODE) using index tablespace &&1;
alter table DOSE_REGIMEN
  add constraint AK_UK_DOSE_REGIMEN_DOSE_REG unique (DREGIMEN_DESC) using index tablespace &&1;
alter table DOSE_REGIMEN
  add constraint CK_IS_VALID_DREGIMEN
  check (IS_VALID is null or (IS_VALID in (1,0)));
alter table DOSE_REGIMEN
  add constraint FK_DOSAGE_INTERVAL_DR foreign key (DOSAGE_INTERVAL) 
  references INTERVAL (INTERVAL_CODE);
  
prompt
prompt Creating table DOSE_UNITS
prompt =========================
prompt
create table DOSE_UNITS
(
  UNIT_CODE VARCHAR2(3) not null,
  UNIT_DESC VARCHAR2(50) not null,
  IS_VALID  NUMBER(1) not null
)
;
alter table DOSE_UNITS
  add constraint PK_DOSE_UNITS primary key (UNIT_CODE) using index tablespace &&1;
alter table DOSE_UNITS
  add constraint UK_DOSE_UNITS unique (UNIT_DESC) using index tablespace &&1;
alter table DOSE_UNITS
  add constraint CK_IS_VALID_DOSEUNIT
  check (IS_VALID in (1,0));

prompt
prompt Creating table ETHNIC_ORIGIN
prompt ============================
prompt
create table ETHNIC_ORIGIN
(
  ETHNIC_CODE VARCHAR2(2) not null,
  ETHNIC_DESC VARCHAR2(50) not null,
  IS_VALID    NUMBER(1)
)
;
alter table ETHNIC_ORIGIN
  add constraint PK_ETHNIC_ORIGIN primary key (ETHNIC_CODE) using index tablespace &&1;
alter table ETHNIC_ORIGIN
  add constraint UK_ETHNIC_ORIGIN unique (ETHNIC_DESC) using index tablespace &&1;
alter table ETHNIC_ORIGIN
  add constraint CK_IS_VALID_EORIGIN
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table FORMULATION
prompt ==========================
prompt
create table FORMULATION
(
  FORMULATION_CODE VARCHAR2(4) not null,
  FORMULATION_DESC VARCHAR2(50) not null,
  IS_VALID         NUMBER(1)
)
;
alter table FORMULATION
  add constraint PK_FORMULATION primary key (FORMULATION_CODE) using index tablespace &&1;
alter table FORMULATION
  add constraint UK_FORMULATION unique (FORMULATION_DESC) using index tablespace &&1;
alter table FORMULATION
  add constraint CK_IS_VALID_FORMULATION
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table INDICATION_TREATMENT
prompt ===================================
prompt
create table INDICATION_TREATMENT
(
  INDICATION_NUM VARCHAR2(8) not null,
  INDICATION     VARCHAR2(50) not null,
  IS_VALID       NUMBER(1)
)
;
alter table INDICATION_TREATMENT
  add constraint PK_INDICATION_TREATMENT primary key (INDICATION_NUM) using index tablespace &&1;
alter table INDICATION_TREATMENT
  add constraint UK_INDICATION unique (INDICATION) using index tablespace &&1;
alter table INDICATION_TREATMENT
  add constraint CK_IS_VALID_INDICATION
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table LHA
prompt ==================
prompt
create table LHA
(
  LHA_CODE     VARCHAR2(12) not null,
  COUNTRY_CODE VARCHAR2(3) not null,
  LHA_DESC     VARCHAR2(50) not null,
  IS_VALID     NUMBER(1)
)
;
alter table LHA
  add constraint PK_LHA primary key (LHA_CODE) using index tablespace &&1;
alter table LHA
  add constraint UK_LHA unique (LHA_DESC) using index tablespace &&1;
alter table LHA
  add constraint FK_LHA_COUNTRY foreign key (COUNTRY_CODE)
  references COUNTRY (COUNTRY_CODE);
alter table LHA
  add constraint CK_IS_VALID_LHA
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table MANUFACTURER
prompt ===========================
prompt
create table MANUFACTURER
(
  M_CODE   VARCHAR2(5) not null,
  M_NAME   VARCHAR2(50) not null,
  M_ADDR1  VARCHAR2(50),
  M_ADDR2  VARCHAR2(50),
  M_ADDR3  VARCHAR2(50),
  M_ADDR4  VARCHAR2(50),
  M_PHONE  VARCHAR2(20),
  M_FAX    VARCHAR2(20),
  M_EMAIL  VARCHAR2(80),
  IS_VALID NUMBER(1)
)
;
alter table MANUFACTURER
  add constraint PK_MANUFACTURER primary key (M_CODE) using index tablespace &&1;
alter table MANUFACTURER
  add constraint CK_IS_VALID_MANU
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table PRODUCT
prompt ======================
prompt
create table PRODUCT
(
  PRODUCT_CODE VARCHAR2(12) not null,
  GENERIC_NM   VARCHAR2(50) not null,
  BRAND_NM     VARCHAR2(50) not null,
  IS_VALID     NUMBER(1)
)
;
alter table PRODUCT
  add constraint PK_PRODUCT primary key (PRODUCT_CODE) using index tablespace &&1;
alter table PRODUCT
  add constraint CK_IS_VALID_PDT
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table RECHALLENGE_RESULT
prompt =================================
prompt
create table RECHALLENGE_RESULT
(
  RESULT_CODE VARCHAR2(2) not null,
  RESULT_DESC VARCHAR2(50) not null,
  IS_VALID    NUMBER(1)
)
;
alter table RECHALLENGE_RESULT
  add constraint PK_RECHALLENGE_RESULT primary key (RESULT_CODE) using index tablespace &&1;
alter table RECHALLENGE_RESULT
  add constraint UK_RECHALLENGE_RESULT unique (RESULT_DESC) using index tablespace &&1;
alter table RECHALLENGE_RESULT
  add constraint CK_IS_VALID_RECHRES
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table REPORTER_STAT
prompt ============================
prompt
create table REPORTER_STAT
(
  STATUS_CODE VARCHAR2(2) not null,
  STATUS_DESC VARCHAR2(50) not null,
  IS_VALID    NUMBER(1)
)
;
alter table REPORTER_STAT
  add constraint PK_REPORTER_STAT primary key (STATUS_CODE) using index tablespace &&1;
alter table REPORTER_STAT
  add constraint UK_REP_STATUS unique (STATUS_DESC) using index tablespace &&1;
alter table REPORTER_STAT
  add constraint CK_IS_VALID_REPSTAT
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table REP_TYPE
prompt =======================
prompt
create table REP_TYPE
(
  TYPE_CODE CHAR(1) not null,
  TYPE_DESC VARCHAR2(50) not null,
  IS_VALID  NUMBER(1)
)
;
alter table REP_TYPE
  add constraint PK_REP_TYPE primary key (TYPE_CODE) using index tablespace &&1;
alter table REP_TYPE
  add constraint UK_REP_TYPE unique (TYPE_DESC) using index tablespace &&1;
alter table REP_TYPE
  add constraint CK_IS_VALID_RTYPE
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table ROUTE
prompt ====================
prompt
create table ROUTE
(
  ROUTE_CODE VARCHAR2(3) not null,
  ROUTE_DESC VARCHAR2(50) not null,
  IS_VALID   NUMBER(1)
)
;
alter table ROUTE
  add constraint PK_ROUTE primary key (ROUTE_CODE) using index tablespace &&1;
alter table ROUTE
  add constraint UK_ROUTE unique (ROUTE_DESC) using index tablespace &&1;
alter table ROUTE
  add constraint CK_IS_VALID_ROUTE
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table SEX
prompt ==================
prompt
create table SEX
(
  SEX_CODE CHAR(1) not null,
  SEX_DESC VARCHAR2(20) not null,
  IS_VALID NUMBER(1)
)
;
alter table SEX
  add constraint PK_SEX primary key (SEX_CODE) using index tablespace &&1;
alter table SEX
  add constraint UK_SEX unique (SEX_DESC) using index tablespace &&1;
alter table SEX
  add constraint CK_IS_VALID_SEX
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table SOURCE_INFO
prompt ==========================
prompt
create table SOURCE_INFO
(
  SOURCE_CODE VARCHAR2(2) not null,
  SOURCE_DESC VARCHAR2(50) not null,
  IS_VALID    NUMBER(1)
)
;
alter table SOURCE_INFO
  add constraint PK_SOURCE_INFO primary key (SOURCE_CODE) using index tablespace &&1;
alter table SOURCE_INFO
  add constraint UK_SOURCE_INFO unique (SOURCE_DESC) using index tablespace &&1;
alter table SOURCE_INFO
  add constraint CK_IS_VALID_SRC
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table SUBM_COMPANY
prompt ===========================
prompt
create table SUBM_COMPANY
(
  COMPANY_NM   VARCHAR2(50) not null,
  COUNTRY_CODE VARCHAR2(3),
  IS_VALID     NUMBER(1)
)
;
alter table SUBM_COMPANY
  add constraint PK_SUBM_COMPANY primary key (COMPANY_NM) using index tablespace &&1;
alter table SUBM_COMPANY
  add constraint FK_SUB_CONF_COUNTRY foreign key (COUNTRY_CODE)
  references COUNTRY (COUNTRY_CODE);
alter table SUBM_COMPANY
  add constraint CK_IS_VALID_DISTBNCO
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table TRIAL_NUM
prompt ========================
prompt
create table TRIAL_NUM
(
  TRIAL_NUM VARCHAR2(20) not null,
  IS_VALID  NUMBER(1)
)
;
alter table TRIAL_NUM
  add constraint PK_TRIAL_NUM primary key (TRIAL_NUM) using index tablespace &&1;
alter table TRIAL_NUM
  add constraint CK_IS_VALID_TRIAL
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table ASSESSMENT_CAUSALITY
prompt ===================================
prompt
create table ASSESSMENT_CAUSALITY
(
  ASSESSMENT_CODE VARCHAR2(2) not null,
  ASSESSMENT      VARCHAR2(50) not null,
  ASSESSMENT_FLAG CHAR(1) not null,
  IS_VALID        NUMBER(1)
)
;
alter table ASSESSMENT_CAUSALITY
  add constraint PK_ASSESSMENT_CAUSALITY primary key (ASSESSMENT_CODE) using index tablespace &&1;
alter table ASSESSMENT_CAUSALITY
  add constraint UK_ASSESSMENT_CAUSALITY unique (ASSESSMENT) using index tablespace &&1;
alter table ASSESSMENT_CAUSALITY
  add constraint CK_ASSESSMENT_FLAG
  check (ASSESSMENT_FLAG in ('Y','N'));
alter table ASSESSMENT_CAUSALITY
  add constraint CK_IS_VALID_ASSESSMENT
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table CASE_DETAILS
prompt ===========================
prompt
create table CASE_DETAILS
(
  REP_NUM               NUMBER(9) not null,
  TYPE_CODE             CHAR(1),
  TRIAL_NUM             VARCHAR2(20),
  COUNTRY_CODE          VARCHAR2(3),
  STATUS_CODE           VARCHAR2(2),
  SOURCE_CODE           VARCHAR2(2),
  M_CODE                VARCHAR2(5),
  FORMULATION_CODE      VARCHAR2(4),
  UNIT_CODE             VARCHAR2(3),
  DREGIMEN_CODE         VARCHAR2(2),
  DFREQ_CODE            VARCHAR2(4),
  ROUTE_CODE            VARCHAR2(3),
  INDICATION_NUM        VARCHAR2(8),
  SEX_CODE              CHAR(1),
  ETHNIC_CODE           VARCHAR2(2),
  OUTCOME_CODE          VARCHAR2(3),
  PRODUCT_CODE          VARCHAR2(12),
  PHY_ASSESSMENT_CODE   VARCHAR2(2),
  CO_ASSESSMENT_CODE    VARCHAR2(2),
  RESULT_CODE           VARCHAR2(2),
  SUBM_COMPANY_NM       VARCHAR2(50),
  SUBM_LHA_CODE         VARCHAR2(12),
  DATE_ENTRY            DATE,
  RECV_BY_AT_HQ         VARCHAR2(20),
  DATE_RECV             DATE,
  REP_NUM_LOCAL         VARCHAR2(10),
  REP_XREF_NUM          VARCHAR2(10),
  CLIN_REP_INFO         VARCHAR2(100),
  COMP_REP_INFO         VARCHAR2(50),
  PATIENT_NUM           VARCHAR2(20),
  HOSP_NM               VARCHAR2(50),
  BATCH_NUM             VARCHAR2(20),
  DOSE                  NUMBER(6,2),
  TREAT_START           DATE,
  TREAT_STOP            DATE,
  RECHALLENGE           CHAR(1),
  AUTHORIZATION_NO      VARCHAR2(20),
  P_INITIALS            VARCHAR2(4),
  DATE_OF_BIRTH         DATE,
  AGE_REPORTED          NUMBER(3),
  WEIGHT                NUMBER(5,2),
  HEIGHT                NUMBER(3),
  PREGNANCY             CHAR(1),
  PREGNANCY_WEEK        NUMBER(2),
  SHORT_HISTORY         VARCHAR2(1000),
  OUTCOME_REP_DATE      DATE,
  AUTOPSY               CHAR(1),
  AUTOPSY_RESULT        VARCHAR2(500),
  PHY_ASSESSMENT_DATE   DATE,
  CO_ASSESSMENT_DATE    DATE,
  PHY_ASSESSMENT_REASON VARCHAR2(1000),
  CO_ASSESSMENT_REASON  VARCHAR2(1000),
  CONTRIBUTING_FACTORS  VARCHAR2(100),
  RECHALLENGE_DATE      DATE,
  RECHALLENGE_DOSE      VARCHAR2(10),
  VERBATIM              VARCHAR2(1000),
  SHORT_COMMENT         VARCHAR2(500),
  DISTBN_DATE           DATE,
  SUBMISSION_DATE       DATE,
  FURTHER_COMMN         VARCHAR2(500),
  AE_DESC               VARCHAR2(4000),
  IS_SUSPENDED          NUMBER(1)
)
;
alter table CASE_DETAILS
  add constraint PK_CASE_DETAILS primary key (REP_NUM) using index tablespace &&1;
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_COUNTRY foreign key (COUNTRY_CODE)
  references COUNTRY (COUNTRY_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_CO_ASSESSMENT foreign key (CO_ASSESSMENT_CODE)
  references ASSESSMENT_CAUSALITY (ASSESSMENT_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_DCOMP foreign key (SUBM_COMPANY_NM)
  references SUBM_COMPANY (COMPANY_NM);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_DFREQ foreign key (DFREQ_CODE)
  references DOSE_FREQUENCY (DFREQ_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_DOSEUNIT foreign key (UNIT_CODE)
  references DOSE_UNITS (UNIT_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_DREGIMEN foreign key (DREGIMEN_CODE)
  references DOSE_REGIMEN (DREGIMEN_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_ETHNIC foreign key (ETHNIC_CODE)
  references ETHNIC_ORIGIN (ETHNIC_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_FORMULATION foreign key (FORMULATION_CODE)
  references FORMULATION (FORMULATION_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_INDICATION foreign key (INDICATION_NUM)
  references INDICATION_TREATMENT (INDICATION_NUM);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_LHA foreign key (SUBM_LHA_CODE)
  references LHA (LHA_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_MANUFACTURER foreign key (M_CODE)
  references MANUFACTURER (M_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_OUTCOME foreign key (OUTCOME_CODE)
  references AE_OUTCOME (OUTCOME_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_PHY_ASSESSMENT foreign key (PHY_ASSESSMENT_CODE)
  references ASSESSMENT_CAUSALITY (ASSESSMENT_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_PRODUCT foreign key (PRODUCT_CODE)
  references PRODUCT (PRODUCT_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_RCH_RESULT foreign key (RESULT_CODE)
  references RECHALLENGE_RESULT (RESULT_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_REPSTAT foreign key (STATUS_CODE)
  references REPORTER_STAT (STATUS_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_REPTYPE foreign key (TYPE_CODE)
  references REP_TYPE (TYPE_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_ROUTE foreign key (ROUTE_CODE)
  references ROUTE (ROUTE_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_SEX foreign key (SEX_CODE)
  references SEX (SEX_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_SOURCE foreign key (SOURCE_CODE)
  references SOURCE_INFO (SOURCE_CODE);
alter table CASE_DETAILS
  add constraint FK_CASEDETAILS_TRIALNUM foreign key (TRIAL_NUM)
  references TRIAL_NUM (TRIAL_NUM);
alter table CASE_DETAILS
  add constraint CK_AUTOPSY
  check (AUTOPSY is null or (AUTOPSY in ('Y','N')));
alter table CASE_DETAILS
  add constraint CK_RECHALLENGE
  check (RECHALLENGE is null or (RECHALLENGE in ('Y','N')));
alter table CASE_DETAILS
  add constraint CK_STATUS_CASEDETAILS
  check (IS_SUSPENDED is null or (IS_SUSPENDED in (1,0)));

prompt
prompt Creating table CLASSIFICATION_SERIOUSNESS
prompt =========================================
prompt
create table CLASSIFICATION_SERIOUSNESS
(
  CLASSIFICATION_CODE VARCHAR2(4) not null,
  CLASSIFICATION_DESC VARCHAR2(50) not null,
  IS_VALID            NUMBER(1)
)
;
alter table CLASSIFICATION_SERIOUSNESS
  add constraint PK_CLASSIFICATION_SERIOUSNESS primary key (CLASSIFICATION_CODE) using index tablespace &&1;
alter table CLASSIFICATION_SERIOUSNESS
  add constraint UK_CLASSIFICATION_SERIOUSNESS unique (CLASSIFICATION_DESC) using index tablespace &&1;
alter table CLASSIFICATION_SERIOUSNESS
  add constraint CK_IS_VALID_SERIOUSNESS
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table EVENT_SEVERITY
prompt =============================
prompt
create table EVENT_SEVERITY
(
  ESEVERITY_CODE VARCHAR2(3) not null,
  ESEVERITY_DESC VARCHAR2(50) not null,
  IS_VALID       NUMBER(1)
)
;
alter table EVENT_SEVERITY
  add constraint PK_EVENT_SEVERITY primary key (ESEVERITY_CODE) using index tablespace &&1;
alter table EVENT_SEVERITY
  add constraint UK_ESEVERITY unique (ESEVERITY_DESC) using index tablespace &&1;
alter table EVENT_SEVERITY
  add constraint CK_IS_VALID_SEVERITY
  check (IS_VALID is null or (IS_VALID in (1,0)));

prompt
prompt Creating table ADVERSE_EVENT
prompt ============================
prompt
create table ADVERSE_EVENT
(
  REP_NUM             NUMBER(9) not null,
  LLT_CODE            VARCHAR2(8) not null,
  DIAG_CODE           VARCHAR2(8),
  OUTCOME_CODE        VARCHAR2(3),
  BODY_SYS_NUM        VARCHAR2(8),
  CLASSIFICATION_CODE VARCHAR2(4),
  ESEVERITY_CODE      VARCHAR2(3),
  LHA_CODE            VARCHAR2(12),
  REPORT_DATE         DATE,
  DATE_START          DATE,
  DATE_STOP           DATE,
  LABELLING           CHAR(1),
  PREV_REPORT_FLG     CHAR(1),
  PREV_REPORT         VARCHAR2(50),
  EVENT_TREATMENT     VARCHAR2(100),
  EVENT_ABATE_ON_STOP CHAR(1),
  AGGR_OF_DIAG        CHAR(1),
  AGGR_OF_LLT         CHAR(1),
  AE_ORDER            NUMBER(2),
  REPORTED_TO_LHA     CHAR(1)
)
;
alter table ADVERSE_EVENT
  add constraint PK_ADVERSE_EVENT primary key (REP_NUM, LLT_CODE) using index tablespace &&1;
alter table ADVERSE_EVENT
  add constraint FK_AE_BODYSYSTEM foreign key (BODY_SYS_NUM)
  references BODY_SYSTEM (BODY_SYS_NUM);
alter table ADVERSE_EVENT
  add constraint FK_AE_CASE_DETAILS foreign key (REP_NUM)
  references CASE_DETAILS (REP_NUM) on delete cascade;
alter table ADVERSE_EVENT
  add constraint FK_AE_CLASS_SERIOUSNESS foreign key (CLASSIFICATION_CODE)
  references CLASSIFICATION_SERIOUSNESS (CLASSIFICATION_CODE);
alter table ADVERSE_EVENT
  add constraint FK_AE_DIAG foreign key (DIAG_CODE)
  references AE_DIAG (DIAG_CODE);
alter table ADVERSE_EVENT
  add constraint FK_AE_LHA foreign key (LHA_CODE)
  references LHA (LHA_CODE);
alter table ADVERSE_EVENT
  add constraint FK_AE_LLT foreign key (LLT_CODE)
  references AE_LLT (LLT_CODE);
alter table ADVERSE_EVENT
  add constraint FK_AE_OUTCOME foreign key (OUTCOME_CODE)
  references AE_OUTCOME (OUTCOME_CODE);
alter table ADVERSE_EVENT
  add constraint FK_AE_SEVERITY foreign key (ESEVERITY_CODE)
  references EVENT_SEVERITY (ESEVERITY_CODE);

prompt
prompt Creating table INVENTORY
prompt ========================
prompt
create table INVENTORY
(
  INV_ID         VARCHAR2(10) not null,
  INV_DESC       VARCHAR2(50) not null,
  INV_TYPE       CHAR(3) not null,
  INS_AUDIT_FLAG NUMBER(1) not null,
  DEL_AUDIT_FLAG NUMBER(1) not null
)
;
alter table INVENTORY
  add constraint PK_INVENTORY primary key (INV_ID) using index tablespace &&1;
alter table INVENTORY
  add constraint UK_INVENTORY unique (INV_DESC) using index tablespace &&1;
alter table INVENTORY
  add constraint CK_INV_TYPE
  check (INV_TYPE in ('SCR','REP'));

prompt
prompt Creating table INVENTORY_COLS
prompt =============================
prompt
create table INVENTORY_COLS
(
  INV_COL_ID VARCHAR2(10) not null,
  INV_ID     VARCHAR2(10),
  FIELD_NAME VARCHAR2(50) not null,
  AUDIT_FLAG NUMBER(1) not null
)
;
alter table INVENTORY_COLS
  add constraint PK_INVENTORY_COLS primary key (INV_COL_ID) using index tablespace &&1;
alter table INVENTORY_COLS
  add constraint FK_INVCOLS_INVENTORY foreign key (INV_ID)
  references INVENTORY (INV_ID) on delete cascade;
alter table INVENTORY_COLS
  add constraint CK_AUDIT_FLAG
  check (AUDIT_FLAG in (1,0));

prompt
prompt Creating table ROLE_LIST
prompt ========================
prompt
create table ROLE_LIST
(
  ROLE_NAME VARCHAR2(30) not null,
  IS_LOCKED NUMBER(1)
)
;
alter table ROLE_LIST
  add constraint PK_ROLE_LIST primary key (ROLE_NAME) using index tablespace &&1;
alter table ROLE_LIST
  add constraint CK_IS_LKD_ROLE
  check (IS_LOCKED is null or (IS_LOCKED in (1,0)));

prompt
prompt Creating table USER_LIST
prompt ========================
prompt
create table USER_LIST
(
  USERNAME          VARCHAR2(10) not null,
  ROLE_NAME         VARCHAR2(30),
  PASSWORD          VARCHAR2(25),
  PWD_CHANGE_FLAG   NUMBER(1) not null,
  PWD_MISTAKE_COUNT NUMBER(1) not null,
  PWD_CHANGE_DATE   DATE,
  IS_LOCKED         NUMBER(1)
)
;
alter table USER_LIST
  add constraint PK_USER_LIST primary key (USERNAME) using index tablespace &&1;
alter table USER_LIST
  add constraint FK_USER_ROLE foreign key (ROLE_NAME)
  references ROLE_LIST (ROLE_NAME);
alter table USER_LIST
  add constraint CK_IS_LKD_USER
  check (IS_LOCKED is null or (IS_LOCKED in (1,0)));
alter table USER_LIST
  add constraint CK_PWD_CHANGE_FLAG
  check (PWD_CHANGE_FLAG in (1,0));

prompt
prompt Creating table AUDIT_TRIAL
prompt ==========================
prompt
create table AUDIT_TRIAL
(
  CHG_NO       NUMBER(10) not null,
  USERNAME     VARCHAR2(10) not null,
  INV_ID       VARCHAR2(10),
  INV_COL_ID   VARCHAR2(10),
  TGT_TYPE     CHAR(4) not null,
  TXN_TYPE     CHAR(1) not null,
  CHG_TIME     DATE not null,
  PK_COL_VALUE VARCHAR2(30) not null,
  OLD_VAL      VARCHAR2(4000),
  NEW_VAL      VARCHAR2(4000)
)
;
alter table AUDIT_TRIAL
  add constraint PK_AUDIT_TRIAL primary key (CHG_NO) using index tablespace &&1;
alter table AUDIT_TRIAL
  add constraint FK_AUDITTRIAL_INVENTORY foreign key (INV_ID)
  references INVENTORY (INV_ID);
alter table AUDIT_TRIAL
  add constraint FK_AUDITTRIAL_USERLIST foreign key (USERNAME)
  references USER_LIST (USERNAME);
alter table AUDIT_TRIAL
  add constraint FK_AUDIT_TRIAL_INVCOLS foreign key (INV_COL_ID)
  references INVENTORY_COLS (INV_COL_ID);
alter table AUDIT_TRIAL
  add constraint CK_CHG_TYPE
  check (TXN_TYPE in ('I','U','D'));
alter table AUDIT_TRIAL
  add constraint CK_TGT_TYPE
  check (TGT_TYPE in ('CASE','CODE'));

prompt
prompt Creating table CONC_MEDICATIONS
prompt ===============================
prompt
create table CONC_MEDICATIONS
(
  REP_NUM        NUMBER(9) not null,
  PRODUCT_CODE   VARCHAR2(12) not null,
  INDICATION_NUM VARCHAR2(8),
  TREAT_START    DATE,
  TREAT_STOP     DATE,
  CM_ORDER       NUMBER(2)
)
;
alter table CONC_MEDICATIONS
  add constraint PK_CONC_MEDICATIONS primary key (REP_NUM, PRODUCT_CODE) using index tablespace &&1;
alter table CONC_MEDICATIONS
  add constraint FK_CONC_MEDIC_CASEDETAILS foreign key (REP_NUM)
  references CASE_DETAILS (REP_NUM) on delete cascade;
alter table CONC_MEDICATIONS
  add constraint FK_CONC_MEDIC_INDICATION foreign key (INDICATION_NUM)
  references INDICATION_TREATMENT (INDICATION_NUM);
alter table CONC_MEDICATIONS
  add constraint FK_CONC_MEDIC_PRODUCT foreign key (PRODUCT_CODE)
  references PRODUCT (PRODUCT_CODE);

prompt
prompt Creating table MAX_NO
prompt =====================
prompt
create table MAX_NO
(
  NO_TYPE VARCHAR2(10) not null,
  YR      NUMBER(4) not null,
  MAX_NO  NUMBER(10)
)
;
alter table MAX_NO
  add constraint PK_MAX_NO primary key (NO_TYPE, YR) using index tablespace &&1;

prompt
prompt Creating table ROLE_PERMISSIONS
prompt ===============================
prompt
create table ROLE_PERMISSIONS
(
  INV_ID    VARCHAR2(10) not null,
  ROLE_NAME VARCHAR2(30) not null,
  PVIEW     NUMBER(1) not null,
  PADD      NUMBER(1) not null,
  PEDIT     NUMBER(1) not null,
  PDELETE   NUMBER(1) not null
)
;
alter table ROLE_PERMISSIONS
  add constraint PK_ROLE_PERMISSIONS primary key (INV_ID, ROLE_NAME) using index tablespace &&1;
alter table ROLE_PERMISSIONS
  add constraint FK_PERM_INVENTORY foreign key (INV_ID)
  references INVENTORY (INV_ID);
alter table ROLE_PERMISSIONS
  add constraint FK_PERM_ROLE foreign key (ROLE_NAME)
  references ROLE_LIST (ROLE_NAME) on delete cascade;
alter table ROLE_PERMISSIONS
  add constraint CK_FADD
  check (PADD in (1,0));
alter table ROLE_PERMISSIONS
  add constraint CK_FDELETE
  check (PDELETE in (1,0));
alter table ROLE_PERMISSIONS
  add constraint CK_FEDIT
  check (PEDIT in (1,0));
alter table ROLE_PERMISSIONS
  add constraint CK_FVIEW
  check (PVIEW in (1,0));

prompt
prompt Creating table USER_SESSION
prompt ===============================
prompt
create table USER_SESSION
(
  SESSION_ID  VARCHAR2(100) not null,
  USERNAME    VARCHAR2(50) not null,
  LOGIN_TIME  DATE not null,
  LOGOUT_TIME DATE,
  LOGIN       NUMBER(1) not null,
  REMOTEIP    VARCHAR2(50)
);

alter table USER_SESSION
  add constraint PK_USER_SESSION primary key (SESSION_ID)
  using index 
  tablespace &&1;
  
alter table USER_SESSION
  add constraint FK_USER_SESSION_USER foreign key (USERNAME)
  references USER_LIST (USERNAME) on delete cascade;

prompt
prompt Creating table LICENSE
prompt ======================
prompt
create table LICENSE
(
  LICENSE_KEY VARCHAR2(100) not null
)
;
alter table LICENSE
  add constraint PK_LICENSE primary key (LICENSE_KEY) using index tablespace &&1;

prompt
prompt Creating sequence SEQ_AUDIT
prompt ===========================
prompt
create sequence SEQ_AUDIT
minvalue 1
maxvalue 999999999999999999999999999
start with 1000
increment by 1
cache 20;

prompt
prompt Creating view V_ADVERSE_EVENT
prompt =============================
prompt
CREATE OR REPLACE VIEW V_ADVERSE_EVENT AS
SELECT AE.Rep_Num, AE.Diag_Code, DIAG.Diag_Desc, AE.Llt_Code, LLT.Llt_Desc, AE.Outcome_Code, AOUT.Outcome,
        AE.Body_Sys_Num, BSYS.Body_System, AE.Classification_Code, CLASF.Classification_Desc,
        AE.Eseverity_Code, ESEV.Eseverity_Desc, AE.Lha_Code, LH.Country_Code, LH.Lha_Desc,
        AE.Report_Date, AE.Date_Start, AE.Date_Stop, nvl(round(( AE.Date_Stop-AE.Date_Start)),-1) "EVENT_DURATION",
        AE.Labelling, AE.Prev_Report_Flg,AE.Reported_To_Lha,
        AE.Prev_Report, AE.Event_Treatment, AE.Event_Abate_On_Stop, AE.Aggr_Of_Diag, AE.Aggr_Of_Llt, AE.Ae_Order
   FROM Adverse_Event AE, Ae_Diag DIAG, Ae_Llt LLT, Ae_Outcome AOUT, Body_System BSYS,
        Classification_Seriousness CLASF, Event_Severity ESEV, Lha LH
  WHERE AE.Diag_Code = DIAG.Diag_Code(+)
    AND AE.Llt_Code = LLT.Llt_Code(+)
    AND AE.Outcome_Code = AOUT.Outcome_Code(+)
    AND AE.Body_Sys_Num = BSYS.Body_Sys_Num(+)
    AND AE.Classification_Code = CLASF.Classification_Code(+)
    AND AE.Eseverity_Code = ESEV.Eseverity_Code(+)
    AND AE.Lha_Code = LH.Lha_Code(+)
  ORDER BY AE.Rep_Num,AE.Ae_Order
/

prompt
prompt Creating view V_CASE_DETAILS
prompt ============================
prompt
CREATE OR REPLACE VIEW V_CASE_DETAILS (REP_NUM, TYPE_CODE, TYPE_DESC, TRIAL_NUM, COUNTRY_CODE, COUNTRY_NM, STATUS_CODE, STATUS_DESC, SOURCE_CODE, SOURCE_DESC, M_CODE, M_NAME, FORMULATION_CODE, FORMULATION_DESC, UNIT_CODE, UNIT_DESC, DREGIMEN_CODE, DREGIMEN_DESC, NO_OF_DOSAGE, DOSAGE_INTERVAL, INTERVAL, DFREQ_CODE, DFREQ_DESC, ROUTE_CODE, ROUTE_DESC, INDICATION_NUM, INDICATION, SEX_CODE, SEX_DESC, ETHNIC_CODE, ETHNIC_DESC, OUTCOME_CODE, OUTCOME, PRODUCT_CODE, GENERIC_NM, BRAND_NM, PHY_ASSESSMENT_CODE, PHY_ASSESSMENT, PHY_ASSESS_FLG, CO_ASSESSMENT_CODE, CO_ASSESSMENT, CO_ASSESS_FLG, RESULT_CODE, RESULT_DESC, SUBM_COMPANY_NM, SUBM_LHA_CODE, LHA_DESC, DATE_ENTRY, RECV_BY_AT_HQ, DATE_RECV, REP_NUM_LOCAL, REP_XREF_NUM, CLIN_REP_INFO, COMP_REP_INFO, PATIENT_NUM, HOSP_NM, BATCH_NUM, DOSE, TREAT_START, TREAT_STOP, DURATION, RECHALLENGE, AUTHORIZATION_NO, P_INITIALS, DATE_OF_BIRTH, AGE, AGE_REPORTED, WEIGHT, HEIGHT, PREGNANCY, PREGNANCY_WEEK, SHORT_HISTORY, OUTCOME_REP_DATE, AUTOPSY, AUTOPSY_RESULT, PHY_ASSESSMENT_DATE, CO_ASSESSMENT_DATE, PHY_ASSESSMENT_REASON, CO_ASSESSMENT_REASON, CONTRIBUTING_FACTORS, RECHALLENGE_DATE, RECHALLENGE_DOSE, VERBATIM, SHORT_COMMENT, DISTBN_DATE, SUBMISSION_DATE, FURTHER_COMMN, AE_DESC, IS_SUSPENDED, PRI_SERIOUSNESS_CODE, PRI_SERIOUSNESS, PRI_REACTION_ONSET, PRI_ABATE_AFTER_STOP,COUNT_CM,M_ADDR1, M_ADDR2, M_ADDR3, M_ADDR4, M_PHONE, M_FAX, M_EMAIL) AS 
  SELECT CD.Rep_Num, CD.Type_Code, RTYP.Type_Desc, CD.Trial_Num, CD.Country_Code, CNT.Country_Nm,
        CD.Status_Code, RSTAT.Status_Desc, CD.Source_Code, SINFO.source_desc, CD.M_Code, MAN.M_Name,
        CD.Formulation_Code, FORML.Formulation_Desc, CD.Unit_Code, DUNIT.Unit_Desc,
        CD.Dregimen_Code, DREG.Dregimen_Desc, DREG.No_Of_Dosage, DREG.Dosage_Interval, Int.Interval, CD.Dfreq_Code, DFREQ.Dfreq_Desc,
        CD.Route_Code, RT.Route_Desc, CD.Indication_Num, IND.Indication, CD.Sex_Code, SX.Sex_Desc,
        CD.Ethnic_Code, EORIG.Ethnic_Desc, CD.Outcome_Code, AOUT.Outcome,
        CD.Product_Code, PRD.Generic_Nm, PRD.Brand_Nm,
        CD.Phy_Assessment_Code, ACAUS1.Assessment PHY_ASSESSMENT, ACAUS1.Assessment_Flag PHY_ASSESS_FLG,
        CD.Co_Assessment_Code, ACAUS2.Assessment CO_ASSESSMENT, ACAUS2.Assessment_Flag CO_ASSESS_FLG,
        CD.Result_Code, RRES.Result_Desc, CD.Subm_Company_Nm, CD.Subm_Lha_Code, LH.Lha_Desc,
        CD.Date_Entry, CD.Recv_By_At_Hq, CD.Date_Recv, CD.Rep_Num_Local, CD.Rep_Xref_Num,
        CD.Clin_Rep_Info, CD.Comp_Rep_Info, CD.Patient_Num, CD.Hosp_Nm, CD.Batch_Num, CD.Dose,
        CD.Treat_Start, CD.Treat_Stop,
        nvl(round(CD.Treat_Stop-CD.Treat_Start+1),-1) DURATION, CD.Rechallenge,
        CD.Authorization_No, CD.P_Initials, CD.Date_Of_Birth,
        nvl(round((CD.Date_Recv-CD.Date_Of_Birth)/365),-1) AGE,
        CD.Age_Reported, CD.Weight, CD.Height, CD.Pregnancy, CD.Pregnancy_Week, CD.Short_History,
        CD.Outcome_Rep_Date, CD.Autopsy, CD.Autopsy_Result, CD.Phy_Assessment_Date, CD.Co_Assessment_Date,
        CD.Phy_Assessment_Reason, CD.Co_Assessment_Reason, CD.Contributing_Factors, CD.Rechallenge_Date,
        CD.Rechallenge_Dose, CD.Verbatim, CD.Short_Comment, CD.Distbn_Date, CD.Submission_Date,
        CD.Further_Commn, CD.Ae_Desc, CD.Is_Suspended, AE.CLASSIFICATION_CODE, AE.CLASSIFICATION_DESC, AE.DATE_START, AE.EVENT_ABATE_ON_STOP,
		cm.count_cm, MAN.M_ADDR1, MAN.M_ADDR2, MAN.M_ADDR3, MAN.M_ADDR4, MAN.M_PHONE, MAN.M_FAX, MAN.M_EMAIL
   FROM Case_Details CD, Rep_Type RTYP, Trial_Num TRIAL, Country CNT, Reporter_Stat RSTAT,
        Source_Info SINFO, Manufacturer MAN, Formulation FORML, Dose_Units DUNIT,
        Dose_Regimen DREG, Dose_Frequency DFREQ, Route RT, Indication_Treatment IND,
        Sex SX, Ethnic_Origin EORIG, Ae_Outcome AOUT, Product PRD,
        Assessment_Causality ACAUS1, Assessment_Causality ACAUS2,Rechallenge_Result RRES, Subm_Company SCO,
        Lha LH, INTERVAL Int, 
		(SELECT * FROM v_Adverse_Event WHERE ae_order=1)AE,
		(SELECT rep_num, count(*) count_cm  from Conc_Medications group by rep_num) CM
  WHERE CD.rep_num = AE.rep_num(+)
	AND CD.rep_num = CM.rep_num(+)
    AND CD.Type_Code = RTYP.Type_Code(+)
    AND CD.Trial_Num = TRIAL.Trial_Num(+)
    AND CD.Country_Code = CNT.Country_Code(+)
    AND CD.Status_Code =RSTAT.Status_Code(+)
    AND CD.Source_Code = SINFO.Source_Code(+)
    AND CD.M_Code = MAN.M_Code(+)
    AND CD.Formulation_Code = FORML.Formulation_Code(+)
    AND CD.Unit_Code = DUNIT.Unit_Code(+)
    AND CD.Dregimen_Code = DREG.Dregimen_Code(+)
    AND CD.Dfreq_Code = DFREQ.Dfreq_Code(+)
    AND CD.Route_Code = RT.Route_Code(+)
    AND CD.Indication_Num = IND.Indication_Num(+)
    AND CD.Sex_Code = SX.Sex_Code(+)
    AND CD.Ethnic_Code = EORIG.Ethnic_Code(+)
    AND CD.Outcome_Code = AOUT.Outcome_Code(+)
    AND CD.Product_Code = PRD.Product_Code(+)
    AND CD.Phy_Assessment_Code = ACAUS1.Assessment_Code(+)
    AND CD.Co_Assessment_Code = ACAUS2.Assessment_Code(+)
    AND CD.Result_Code = RRES.Result_Code(+)
    AND CD.Subm_Company_Nm = SCO.Company_Nm(+)
    AND CD.Subm_Lha_Code = LH.Lha_Code(+)
    AND DREG.Dosage_Interval = Int.INTERVAL_CODE(+)
  ORDER BY CD.Rep_Num
/

prompt
prompt Creating view V_CONC_MEDICATIONS
prompt ================================
prompt
CREATE OR REPLACE VIEW V_CONC_MEDICATIONS (REP_NUM, PRODUCT_CODE, GENERIC_NM, BRAND_NM, INDICATION_NUM, INDICATION, TREAT_START, TREAT_STOP, CONC_DURATION, CM_ORDER) AS 
  SELECT CM.Rep_Num, CM.Product_Code, PRD.Generic_Nm, PRD.Brand_Nm,
        CM.Indication_Num, IND.Indication, CM.Treat_Start, CM.Treat_Stop,
        nvl(round((CM.Treat_Stop-CM.Treat_Start)+1),-1) "CONC_DURATION", CM.cm_order
   FROM Conc_Medications CM, Product PRD, Indication_Treatment IND
  WHERE CM.Product_Code = PRD.Product_Code(+)
    AND CM.Indication_Num = IND.Indication_Num(+)
  ORDER BY CM.Rep_Num,CM.cm_order 
/

exit

spool off
