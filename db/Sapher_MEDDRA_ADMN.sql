spool Sapher_MEDDRA_ADMN.log


begin
dbms_java.grant_permission('SAPBETAADMN','SYS:java.io.FilePermission','&&1\*','read,write');
end;
/

begin
dbms_java.grant_permission('SAPBETAADMN','SYS:java.io.FilePermission','&&1','read,write');
end;
/


create or replace directory &&2 as '&&1';

create table EXT_MEDDRA_SOC
(
  SOC_CODE VARCHAR2(12),
  SOC_DESC VARCHAR2(500),
  SOC_ABBR VARCHAR2(50),
  WHO_ART_CODE VARCHAR2(100),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  JART_CODE VARCHAR2(100)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('soc.asc')
)
reject limit unlimited;

create table EXT_MEDDRA_HLGT
(
  HLGT_CODE VARCHAR2(12),
  HLGT_DESC VARCHAR2(500),
  WHO_ART_CODE VARCHAR2(100),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  JART_CODE VARCHAR2(100)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('hlgt.asc')
)
reject limit unlimited;


create table EXT_MEDDRA_SOC_HLGT
(
  SOC_CODE VARCHAR2(12),
  HLGT_CODE VARCHAR2(12)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('soc_hlgt.asc')
)
reject limit unlimited;


create table EXT_MEDDRA_HLT
(
  HLT_CODE VARCHAR2(12) ,
  HLT_DESC VARCHAR2(500) ,
  WHO_ART_CODE VARCHAR2(100),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  JART_CODE VARCHAR2(100)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('hlt.asc')
)
reject limit unlimited;

create table EXT_MEDDRA_HLGT_HLT
(
  HLGT_CODE VARCHAR2(12),
  HLT_CODE VARCHAR2(12)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('hlgt_hlt.asc')
)
reject limit unlimited;

create table EXT_MEDDRA_PT
(
  PT_CODE VARCHAR2(12),
  PT_DESC VARCHAR2(500),
  WHO_ART_CODE VARCHAR2(100),
  PRIMARY_SOC VARCHAR2(12),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  JART_CODE VARCHAR2(100)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('pt.asc')
)
reject limit unlimited;

create table EXT_MEDDRA_HLT_PT
(
  HLT_CODE VARCHAR2(12),
  PT_CODE VARCHAR2(12)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('hlt_pt.asc')
)
reject limit unlimited;

create table EXT_MEDDRA_LLT
(
  LLT_CODE VARCHAR2(12),
  LLT_DESC VARCHAR2(500),
  PT_CODE VARCHAR2(12),
  WHO_ART_CODE VARCHAR2(100),
  HARTS_CODE VARCHAR2(100),
  COSTART_SYM VARCHAR2(100),
  ICD9_CODE VARCHAR2(100),
  ICD9_CM_CODE VARCHAR2(100),
  ICD10_CODE VARCHAR2(100),
  LLT_CURRENCY CHAR(1),
  JART_CODE VARCHAR2(100)
)
organization external (
   type oracle_loader
   default directory &&2
  access parameters (
    records delimited  by newline
    fields  terminated by '$'
    missing field values are null
  )
  location ('llt.asc')
)
reject limit unlimited;


grant select on EXT_MEDDRA_HLGT     to public;
grant select on EXT_MEDDRA_HLGT_HLT to public;
grant select on EXT_MEDDRA_HLT      to public;
grant select on EXT_MEDDRA_HLT_PT   to public;
grant select on EXT_MEDDRA_LLT      to public;
grant select on EXT_MEDDRA_PT       to public;
grant select on EXT_MEDDRA_SOC      to public;
grant select on EXT_MEDDRA_SOC_HLGT to public;

drop public synonym EXT_MEDDRA_HLGT        ;
drop public synonym EXT_MEDDRA_HLGT_HLT    ;
drop public synonym EXT_MEDDRA_HLT         ;
drop public synonym EXT_MEDDRA_HLT_PT      ;
drop public synonym EXT_MEDDRA_LLT         ;
drop public synonym EXT_MEDDRA_PT          ;
drop public synonym EXT_MEDDRA_SOC         ;
drop public synonym EXT_MEDDRA_SOC_HLGT    ;


create public synonym EXT_MEDDRA_HLGT     for EXT_MEDDRA_HLGT    ;
create public synonym EXT_MEDDRA_HLGT_HLT for EXT_MEDDRA_HLGT_HLT;
create public synonym EXT_MEDDRA_HLT      for EXT_MEDDRA_HLT     ;
create public synonym EXT_MEDDRA_HLT_PT   for EXT_MEDDRA_HLT_PT  ;
create public synonym EXT_MEDDRA_LLT      for EXT_MEDDRA_LLT     ;
create public synonym EXT_MEDDRA_PT       for EXT_MEDDRA_PT      ;
create public synonym EXT_MEDDRA_SOC      for EXT_MEDDRA_SOC     ;
create public synonym EXT_MEDDRA_SOC_HLGT for EXT_MEDDRA_SOC_HLGT;

exit

spool off
