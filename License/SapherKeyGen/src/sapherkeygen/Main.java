
package sapherkeygen;

import com.f3.sapher.keygen.SapherKey;

/**
 *
 * @author Anoop Varma
 */
public class Main {

    /** Serial version UID */
    private static final long serialVersionUID = 5938684982760272279L;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SapherKey key = new SapherKey();
        
        System.out.println(args[0]);
        System.out.println(args[1]);
        
        String str = key.generateKey(args[0], args[1]);
        
        System.out.println(str);
        System.out.println("Count: " + SapherKey.calcUserCount(str));
        System.out.println("Client: " + SapherKey.calcCompanyName(str));
    }

}
