package com.f3.sapher.keygen;

/**
 *
 * @author Anoop Varma
 */
public class SapherKey {

    /** Serial version UID */
    private static final long serialVersionUID = -5938684982760272280L;

    public String encrypt(String inStr) {
        String outStr = null;
        char[] cArr = inStr.toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = ((char) (cArr[i] + 8));
        }
        outStr = new String(cArr);
        return outStr;
    }

    public String decrypt(String inStr) {
        String outStr = null;
        char[] cArr = inStr.toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = ((char) (cArr[i] - 8));
        }
        outStr = new String(cArr);
        return outStr;
    }

    /**
     * Function that generates the user license key.
     * @param count The number of users to be allowed with this license, <code><b>int</b></code>
     * @param ClientName Name of the client to whom the license is provided, as <code><b>String</b></code>
     * @return
     */
    public String generateKey(String count, String ClientName) {
        
        char[] cArr = count.toCharArray();
        
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] += 25;
        }
        String str = new String(cArr, 0, 3);

        cArr = ClientName.toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] += 8;
        }
        String strCpny = new String(cArr);
        
        cArr = ("" + System.currentTimeMillis()).toCharArray();
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] += 25;
        }
        String ts = new String(cArr);

        return "SFR-U" + str + "-C" + strCpny + "-T" + ts;

    }

    /**
     * Function that returns the number of users from the license key provided.
     * 
     * @param licenseKey as <code><b>String</b></code>.
     * @return The number of users from the license as <code><b>String</b></code>.
     */
    public static String calcUserCount(String licenseKey) {
        char[] cArr = licenseKey.toCharArray();

        for (int i = 5; i < 8; i++) {
            cArr[i] -= 25;
        }
        String s = new String(cArr, 5, 3);

        return s;
    }
    
    public static String calcCompanyName(String licenseKey) {
        char[] cArr = licenseKey.toCharArray();

        int nDelimiter = licenseKey.indexOf("-T");
        
        for (int i = 10; i < nDelimiter; i++) {
            cArr[i] -= 8;
        }
        String s = new String(cArr, 10, nDelimiter-10);

        return s;
    }
}





















