-----------------------------------------------------------
-- Populating Standard tables in SAPHER                  --
-- Created by Sujith Sundar on 03-APR-2013               --
-----------------------------------------------------------

set echo on
set verify on

spool PopulateStdTables.log

select * from global_name;
show user;

TRUNCATE TABLE REP_TYPE;
TRUNCATE TABLE REPORTER_STAT;
TRUNCATE TABLE DOSE_UNITS;
TRUNCATE TABLE ROUTE;
TRUNCATE TABLE AE_OUTCOME;
TRUNCATE TABLE COUNTRY;
TRUNCATE TABLE SEX;
TRUNCATE TABLE INTERVAL;


insert into REP_TYPE(TYPE_CODE, TYPE_DESC, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'REP_TYPE';
	
insert into REPORTER_STAT(STATUS_CODE, STATUS_DESC, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'REPORTER_STAT'; 

insert into DOSE_UNITS(UNIT_CODE, UNIT_DESC, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'DOSE_UNITS'; 

insert into ROUTE(ROUTE_CODE, ROUTE_DESC, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'ROUTE'; 

insert into AE_OUTCOME(OUTCOME_CODE, OUTCOME, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'AE_OUTCOME'; 

insert into COUNTRY(COUNTRY_CODE, COUNTRY_NM, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'COUNTRY'; 

insert into SEX(SEX_CODE, SEX_DESC, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'SEX'; 

insert into INTERVAL(INTERVAL_CODE, INTERVAL, IS_VALID)
	select trim(STD_CODE), trim(STD_DESC), '1'
	from MAPPING_TABLE
	where STD_REF_TABLE = 'INTERVAL'; 
	

commit;

spool off
exit
