-------------------------------------------------------------
-- Control file to load standard code list in MIGSAPHER    --
-- Created by Sujith Sundar on 29-JUL-2008                 --
-------------------------------------------------------------

LOAD DATA
INFILE 'StdCodeListSapher.txt'
BADFILE 'StdCodeListSapher.bad'

TRUNCATE 
INTO TABLE MAPPING_TABLE
FIELDS TERMINATED BY '|' TRAILING NULLCOLS
(STD_REF_TABLE, CLIENT_CODE, CLIENT_DESC, STD_CODE "(:CLIENT_CODE)", STD_DESC "(:CLIENT_DESC)")
