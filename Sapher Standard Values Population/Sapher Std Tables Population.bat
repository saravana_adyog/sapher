@ECHO off

REM ******************************************
REM *****   Sapher Std Tables            *****
REM *****   Sujith Sundar, Focal3        *****
REM *****   03-APR-2013                  *****
REM ******************************************

REM ******************************************
REM *****  Variables Set here            *****
REM ******************************************

SET SUser=SAPBETA
SET SPwd=SAPBETA

SET DB_Con=sapher
SET SYSDBA=system/wspr123
SET ORAHOME=E:\oracle\product\10.1.0\oradata\SAPHER

REM ******************************************
REM *****  Script executed here          *****
REM ******************************************

sqlplus SUser/SPwd@%DB_Con% @Mapping_Table.sql
sqlldr SUser/SPwd@%DB_Con% control=StdCodeListSapher.ctl log=StdCodeListSapher.log
sqlplus SUser/SPwd@%DB_Con% @PopulateStdTables.sql

pause 
