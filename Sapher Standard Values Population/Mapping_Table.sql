set echo on
set verify on

spool MAPPING_TABLE.log

select * from global_name;
show user;


create table MAPPING_TABLE
(
  STD_REF_TABLE varchar2(30),
  CLIENT_CODE varchar2(35),
  CLIENT_DESC varchar2(150),
  STD_CODE varchar2(35),
  STD_DESC varchar2(150)
);


commit;

spool off
exit
